echo %*
rem @echo OFF
rem ***************************************************************************
rem
rem    Filename           : JT_to_SE-ASM.bat
rem
rem Script to convert a Monolitic JT file into a SE Assembly
rem
rem ***************************************************************************
rem Revision History:
rem
rem REV   AUTHOR      DATE         COMMENT
rem ===   ========    ===========  =======
rem 001   C.Veraa     19-Mrt-2014  Initial Version
rem
rem ***************************************************************************

REM set TC_ROOT To Directory where TC in installed
set TC_ROOT=D:\PDM\Tc11

REM  set TC_DATA To Directory where TC Data is located. Check if TC_DATA is valid.
set TC_DATA=\\BDR-TDVOL11\PDMShare\DVDATA
call "%TC_DATA%\tc_profilevars.bat"

::  Usage: assy_jt_creator -u=<username> -p=<password> -g=<groupname> [-h] [- i= <batchFile.xml>][-log=<log file name>]
::
::
::	[-sc=<sctructure context name] [[-item=<item id> || -key=<key string>] -rev=<revision id> -rev_rule=<revision rule> -var_rule=<saved variant rule> -tmode=<transfer mode>]
::	[-tar_loc=<target location where jt will be created>]
::
::		-jt_type=<the assembly type to be created. 1:Monolithic  2:Assembly  3:COJT>
::
::		[-pin=<to process intermediate nodes>]    1:true  0:false
::
::		[-cof=<continue on failure>]  1:continue  0:stop
::
::		[ [ -de={n|e|a|r} ] [ -dt_type=<dataset type> -dt_rel=<dataset relation> -dt_ref=<named reference> -assy_update=<dataset update mode> ] ]
::
::		-skip_root_dset_with_name=<skip dataset with this name on the root node related by IMAN_Rendering from being exported.>
::
:: -debug={TRUE|FALSE}. The default value is FALSE. To generate Debug log in target location
::
:: If the input xml file is specified, all the other command line parameters are not considered.
:: The log file location, if not specified through commandline, should be mentioned for all the set of parameters in the XML file.
:: If -de is not given as input, then a JT file of -jt_type will be created without Dset

rem echo. > %~n0.job

rem ********************************************************************
rem TRANSLATOR DIRECTORY
rem ********************************************************************
SET TRANSLATE_DIR=%~dp0

rem ********************************************************************
rem SE Template DIRECTORY
rem ********************************************************************
set SE_TEMPLATE=D:\PDM\ST9\Template

rem Remove qoutes
set WORKDIR=%~1
set ITEMID=%~2
set REVID=%~3
rem set FILENAME_OUT=%~4

echo assy_jt_creator  -u=dcproxy -p=dcproxy -g=dba -item=%ITEMID% -rev=%REVID% -de= -tar_loc=%WORKDIR% -log="%WORKDIR%\assy_jt_creator.log" -assy_update=create -jt_assy_type=2 -cof=0 -pin=0 -rev_rule="Latest Working" -dt_type=DirectModel -dt_ref=JTPART -dt_rel=IMAN_Rendering
     assy_jt_creator  -u=dcproxy -p=dcproxy -g=dba -item=%ITEMID% -rev=%REVID% -de= -tar_loc=%WORKDIR% -log="%WORKDIR%\assy_jt_creator.log" -assy_update=create -jt_assy_type=2 -cof=0 -pin=0 -rev_rule="Latest Working" -dt_type=DirectModel -dt_ref=JTPART -dt_rel=IMAN_Rendering


:: Cleanup
if exist *.xml del *.xml
:: if exist *_asm.jt del *_asm.jt

:: for /f "tokens=1" %%a in ('dir /b /ad') do set DIRNAME=%%a
:: rd /s /q %DIRNAME%

set FILENAME_IN=%ITEMID%.jt
for /f "tokens=1" %%a in ('dir /b *.jt') do set JT_FILE=%%a
rename %JT_FILE% %FILENAME_IN%

:: Convert Monolityc JT into an SE assembly file
:process_prt
	echo %TRANSLATE_DIR%\BnlSeBatchConvert -in="%FILENAME_IN%" -out="%WORKDIR%\%ITEMID%.asm" -template_dir="%SE_TEMPLATE%"
	     %TRANSLATE_DIR%\BnlSeBatchConvert -in="%FILENAME_IN%" -out="%WORKDIR%\%ITEMID%.asm" -template_dir="%SE_TEMPLATE%"
	if %ERRORLEVEL% NEQ 0 set RETURNCODE=%ERRORLEVEL% & goto :end

:end
EXIT /B %RETURNCODE%



