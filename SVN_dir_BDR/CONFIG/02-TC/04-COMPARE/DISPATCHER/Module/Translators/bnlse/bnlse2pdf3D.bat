@echo off
rem @<COPYRIGHT>@
rem ============================================================================
rem Copyright 2012.
rem Siemens Product Lifecycle Management Software Inc.
rem All Rights Reserved.
rem ============================================================================
rem @<COPYRIGHT>@
rem
rem          SCRIPT TO RUN SolidEdge TRANSLATOR for SolidEdge
rem  Example : set SOILDEDGE_INSTALL=D:\Program Files\Solid Edge V19\Program\SolidEdgeTranslationServices.exe
rem  =================================================


set SOLIDEDGE_INSTALL=E:\PLM\Solid Edge ST7

@set PARAM1=%1

if x%PARAM1%== x goto help
if %1== -help goto help

goto setenv

:help

echo Usage:
echo  Translator executable name: setranslations.bat
echo  Command String is:
echo    setranslations.bat inputFile -v=false -t=target_extension outputfile
echo  Where seven parameters is passed:
echo    Param 1) is the input argument
echo    Param 2) is the input file
echo    Param 3) is the String '-v' i.e Visualization
echo    Param 4) is the String 'true'
echo    Param 5) is the String '-t' i.e the target extension
echo    Param 6) is the String '.*' i.e extension
echo    Param 7) is the output directory argument
echo    Param 8) is the output directory
echo    Param 9) is the output file
echo    Example:-
echo      setranslations.bat e:\se_data\part.par -v=false -t=jt e:\se_data\part.jt
goto :EOF

:setenv

if "%SOLIDEDGE_INSTALL%"=="CHANGE_ME" (
 echo set SOLIDEDGE_INSTALL to location where SolidEdge is installed
 goto :end
)

echo Vars:
echo  var1= %1
echo  var2= %2
echo  var3= %3
echo  var4= %4
echo  var5= %5
echo  var6= %6
echo  var7= %7
echo  var8= %8
echo  var9= %9

rem Added SPLM/JVi - 20140731
if not exist .\temp mkdir .\temp
set TEMP=.\temp
set TMP=%TEMP%

echo TEMP= %TEMP%

rem create PDF3D
echo "%SOLIDEDGE_INSTALL%/SolidEdgeTranslationServices.exe" %1=%2 %3=%4 %5=pdf %6=%7\%8%9.pdf
"%SOLIDEDGE_INSTALL%/SolidEdgeTranslationServices.exe" %1=%2 %3=%4 %5=pdf %6=%7\%8%9.pdf

set EXITVALUE=%ERRORLEVEL%

EXIT %EXITVALUE%
