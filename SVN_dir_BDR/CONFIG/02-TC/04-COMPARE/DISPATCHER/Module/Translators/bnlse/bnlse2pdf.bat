@echo off
rem Copyright by Siemens PLM Software 2013-2015
rem
rem Start script for the BnlSe2Pdf
rem
rem J. Mansvelders   Created                       01-04-2014
rem D. Tjon          Updated for TC11              16-10-2015
rem
rem ********************************************************************

setlocal

rem ********************************************************************
rem Ghostscript bin directory
rem ********************************************************************
rem Replace CHANGE_ME with correct values as shown in Example
rem Set GS_HOME To the Directory where Ghost is installed
rem Example set GS_HOME=C:\Program Files\gs\gs9.14\bin
set GS_HOME=D:\PDM\Gs9.19\bin

rem ********************************************************************
rem INPUT PARAMETERS
rem ********************************************************************
set INPUTFILE=%~1
set OUTPUTDIR=%~2
set PRINTFILE=%~3
set SYNCFILE=%~4

rem ********************************************************************
rem TRANSLATOR DIRECTORY
rem ********************************************************************
SET TRANSLATE_DIR=%~dp0

rem ********************************************************************
rem RUN SE PDF
rem ********************************************************************
rem                    Available command line options:
rem                    Required:
rem                             -logfile=<logfilename>
rem                             -filename=<part-name>
rem                    Optional:
rem                             -queue=<printer-or-plottername>
rem                             -printfile=<outputfile>
rem                             -color
rem                             -orientation=landscape | portrait
rem                             -numofcopies=<number>
rem                             -macro=<path\macro.exe>
rem                             -syncfile=<path\sync.txt>
rem                             -layerson=layer 1;layer 2;�;layer n
rem                             -layersoff=layer 1;layer 2;�;layer n
rem                             -ignorelayererrors=Y | N

set SE_COMMAND=%TRANSLATE_DIR%\BNL_SE_print.exe "-logfile=%CD%\selog.log" "-filename=%INPUTFILE%" "-queue=Solid Edge Velocity PS Printer 2.0" "-printfile=%OUTPUTDIR%\%PRINTFILE%" "-syncfile=%SYNCFILE%" "-color"

echo Running [%SE_COMMAND%]
%SE_COMMAND%
echo.

FOR /F "TOKENS=*" %%G IN ('dir /b "%OUTPUTDIR%\*.pri"') DO CALL :CreatePDF %%G

set EXITVALUE=%ERRORLEVEL%

goto :End

:CreatePDF
set PSFILE=%~n1
set PDF_COMMAND="%GS_HOME%\gswin64c.exe" -dNOPAUSE -dBATCH -sDEVICE#pdfwrite -sOutputFile="%OUTPUTDIR%\%PSFILE%.pdf" -f "%OUTPUTDIR%\%PSFILE%.pri"

echo Running [%PDF_COMMAND%]
%PDF_COMMAND%
echo.

goto :EOF

:End
endlocal

@echo on
EXIT %EXITVALUE%
