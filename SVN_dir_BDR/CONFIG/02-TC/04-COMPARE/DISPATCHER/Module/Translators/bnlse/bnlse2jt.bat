@echo ON
rem @<COPYRIGHT>@
rem ============================================================================
rem Copyright 2012.
rem Siemens Product Lifecycle Management Software Inc.
rem All Rights Reserved.
rem ============================================================================
rem @<COPYRIGHT>@
rem
rem          SCRIPT TO RUN SolidEdge TRANSLATOR for SolidEdge 
rem # Example : set SOLIDEDGE_INSTALL=D:\Program Files\Solid Edge V19\Program\SolidEdgeTranslationServices.exe
rem  ================================================= 

set TC_ROOT=D:\PDM\Tc11
set TC_DATA=\\BDR-TDVOL11\PDMShare\DVDATA
set SOLIDEDGE_INSTALL=D:\PDM\ST9
set OUTPUT_TYPE=jt

call "%TC_DATA%\tc_profilevars.bat"

@set PARAM1=%1

if x%PARAM1%== x goto help
if %1== -help goto help

goto setenv

:help

echo Usage:
echo  Translator executable name: setranslations.bat
echo  Command String is:
echo    setranslations.bat inputFile -v=false -t=target_extension outputfile
echo  Where seven parameters is passed: 
echo    Param 1) is the input argument 
echo    Param 2) is the input file 
echo    Param 3) is the String '-v' i.e Visualization 
echo    Param 4) is the String 'true'  
echo    Param 5) is the String '-t' i.e the target extension  
echo    Param 6) is the String '.*' i.e extension
echo    Param 7) is the output directory argument
echo    Param 8) is the output directory
echo    Param 9) is the output file 
echo    Example:-
echo      setranslations.bat e:\se_data\part.par -v=false -t=jt e:\se_data\part.jt
goto :EOF

:setenv

if "%SOLIDEDGE_INSTALL%"=="CHANGE_ME" (
 echo set SOLIDEDGE_INSTALL to location where SolidEdge is installed
 goto :end
)

echo Vars:
echo var1= %1
echo var2= %2
echo var3= %3
echo var4= %4
echo var5= %5
echo var6= %6
echo var7= %7
echo var8= %8
echo var9= %9
set item_id=%~8
set rev_id=%~9

rem set user - password - group
SET TRANSLATE_DIR=%~dp0
call %TRANSLATE_DIR%\setuserpwd.bat"


if not exist .\result mkdir .\result

rem Retrieving name of working directory and define temp dir for TranslationService log file
rem Added by CVER - Siemens PLM to resolve issue with TranslationService temp dir
for %%* in (cd) do set WORK_DIR=%%~dp*
set TEMP=%WORK_DIR%Temp
set TMP=%TEMP%
if not exist %TEMP% mkdir %TEMP%
echo TEMP= %TEMP%

rem Create output
echo.
echo "%SOLIDEDGE_INSTALL%\Program\SolidEdgeTranslationServices.exe" -i="%2" -v=false -t=%OUTPUT_TYPE% -o="%7\%8%9.%OUTPUT_TYPE%" 
"%SOLIDEDGE_INSTALL%\Program\SolidEdgeTranslationServices.exe" -i="%2" -v=false -t=%OUTPUT_TYPE% -o="%7\%8%9.%OUTPUT_TYPE%" 
echo.

set EXITVALUE=%ERRORLEVEL%
rem EXIT %EXITVALUE%

