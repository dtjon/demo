@echo OFF
:: ***********************************************************************************
:: Revision history
::
:: REV  AUTHOR         DATE         COMMENT
:: ===  =============  ===========  ==================================================
:: 001  Carol Veraa    20-Sep-2013  Initial Version
:: 002  Carol Veraa    20-Nov-2013  Add return code check to get proper Processed info
:: 003   Derek Tjon    29-Sep-2016  Converted for dispatcher
::   
:: ***************************************************************************
rem echo. > %~n0.job 

rem ********************************************************************
rem TRANSLATOR DIRECTORY
rem ********************************************************************
SET TRANSLATE_DIR=%~dp0

rem ********************************************************************
rem SE Template DIRECTORY
rem ********************************************************************
set SE_TEMPLATE=D:\PDM\ST9\Template

rem Remove qoutes
set FILENAME_IN=%~1
set WORKDIR=%~2
set FILENAME_OUT=%~3.par

rem for /f "tokens=1,2,3 delims=." %%a in ("%FILENAME_IN%") do set FN=%%a&set EX=%%b&set NO=%%c

:process_jt2par
	echo %TRANSLATE_DIR%\BnlSeBatchConvert -in="%FILENAME_IN%" -out="%WORKDIR%\%FILENAME_OUT%" -template_dir="%SE_TEMPLATE%"
	     %TRANSLATE_DIR%\BnlSeBatchConvert -in="%FILENAME_IN%" -out="%WORKDIR%\%FILENAME_OUT%" -template_dir="%SE_TEMPLATE%"
	if %ERRORLEVEL% NEQ 0 set RETURNCODE=%ERRORLEVEL% & goto :end

rem DTJ: remove for TC11 upgrade
rem :process_par2jt
rem	set SE_JTPATH="D:\PDM\ST4\Program"
rem	echo %SE_JTPATH%\sepvadp -g -o %WORKDIR% %WORKDIR%\%FILENAME_OUT%
rem	call %SE_JTPATH%\sepvadp -g -o %WORKDIR% %WORKDIR%\%FILENAME_OUT%	
rem	if %ERRORLEVEL% NEQ 0 set RETURNCODE=%ERRORLEVEL% & goto :end	

:end
EXIT /B %RETURNCODE%

	

