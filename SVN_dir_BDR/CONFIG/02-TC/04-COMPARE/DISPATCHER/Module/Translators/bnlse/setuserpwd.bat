@echo off
rem Use Teamcenter password file instead of password which can be read/viewed
rem TC_PWF_dcproxy variable containing login setting is defined in tc_profilevars.bat

set TC_USER=
set TC_PWD=
set TC_GRP=