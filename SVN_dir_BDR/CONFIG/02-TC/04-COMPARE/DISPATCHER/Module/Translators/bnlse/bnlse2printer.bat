@echo off
rem Copyright by Siemens PLM Software 2013-2015
rem
rem Start script for the BnlSe2Printer
rem
rem J. Mansvelders   Created                       01-04-2014
rem D. Tjon          Updated for TC11              16-10-2015
rem
rem ********************************************************************

setlocal

rem ********************************************************************
rem INPUT PARAMETERS
rem ********************************************************************
set INPUTFILE=%~1
set SYNCFILE=%~2

rem ********************************************************************
rem TRANSLATOR DIRECTORY
rem ********************************************************************
SET TRANSLATE_DIR=%~dp0

rem ********************************************************************
rem RUN SE PRINT
rem ********************************************************************
rem                    Available command line options:
rem                    Required:
rem                             -logfile=<logfilename>
rem                             -filename=<part-name>
rem                    Optional:
rem                             -queue=<printer-or-plottername>
rem                             -printfile=<outputfile>
rem                             -color
rem                             -orientation=landscape | portrait
rem                             -numofcopies=<number>
rem                             -macro=<path\macro.exe>
rem                             -syncfile=<path\sync.txt>
rem                             -layerson=layer 1;layer 2;�;layer n
rem                             -layersoff=layer 1;layer 2;�;layer n
rem                             -ignorelayererrors=Y | N

set SE_COMMAND=%TRANSLATE_DIR%\BNL_SE_print.exe "-logfile=%CD%\selog.log" "-filename=%INPUTFILE%" "-queue=\\nlutcsprint01\Secure Print" "-syncfile=%SYNCFILE%" "-numofcopies=1"

echo Running [%SE_COMMAND%]
%SE_COMMAND%

set EXITVALUE=%ERRORLEVEL%

endlocal

@echo on

EXIT %EXITVALUE%