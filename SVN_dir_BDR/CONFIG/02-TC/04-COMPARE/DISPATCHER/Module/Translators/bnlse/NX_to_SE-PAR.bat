@echo OFF
rem ***************************************************************************
rem
rem    Filename           : NX_to_SE-PAR.bat
rem
rem Script to convert NX modeling file into SE Part
rem
rem ***************************************************************************
rem Revision History:
rem
rem REV   AUTHOR      DATE         COMMENT
rem ===   ========    ===========  =======
rem 001   C.Veraa     20-Sep-2013  Initial Version
rem 002   C.Veraa     20-Nov-2013  Add return code check to get proper Processed info
rem 003   Derek Tjon  29-Sep-2016  Converted for dispatcher
rem
rem ***************************************************************************

rem ********************************************************************
rem TRANSLATOR DIRECTORY
rem ********************************************************************
SET TRANSLATE_DIR=%~dp0

set SE_TEMPLATE=D:\PDM\ST9\Template

rem Remove qoutes
set INPUTPATH=%~1
set OUTPUTDIR=%~2
set ITEMID=%~3
set REVID=%~4
set FILENAME_IN=%ITEMID%.par

for /f "tokens=1,2,3 delims=." %%a in ("%FILENAME_IN%") do set FN=%%a&set EX=%%b

:process_prt2par
	echo %TRANSLATE_DIR%\BnlSeBatchConvert -in="%INPUTPATH%" -out="%OUTPUTDIR%\%ITEMID%.par" -template_dir="%SE_TEMPLATE%"
	     %TRANSLATE_DIR%\BnlSeBatchConvert -in="%INPUTPATH%" -out="%OUTPUTDIR%\%ITEMID%.par" -template_dir="%SE_TEMPLATE%"
	if %ERRORLEVEL% NEQ 0 set RETURNCODE=%ERRORLEVEL% & goto :end

rem DTJ: remove for TC11 upgrade
rem :process_par2jt
rem	set SE_JTPATH="D:\PDM\ST4\Program"
rem	echo %SE_JTPATH%\sepvadp -g -o %INPUTPATH% "%INPUTPATH%\%ITEMID%.par"
rem	call %SE_JTPATH%\sepvadp -g -o %INPUTPATH% "%INPUTPATH%\%ITEMID%.par"
rem	if %ERRORLEVEL% NEQ 0 set RETURNCODE=%ERRORLEVEL% & goto :end

:end
EXIT /B %RETURNCODE%
