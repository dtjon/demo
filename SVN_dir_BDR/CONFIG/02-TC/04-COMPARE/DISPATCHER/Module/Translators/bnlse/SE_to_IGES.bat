echo %*
rem @echo OFF
rem ***************************************************************************
rem
rem    Filename           : SE_to_IGES.bat
rem
rem Script to convert Solid Edge Part to a IGES File
rem
rem ***************************************************************************
rem Revision History:
rem
rem REV   AUTHOR      DATE         COMMENT
rem ===   ========    ===========  =======
rem 001   C.Veraa     11-Dec-2013  Initial Version
rem 002   C.Veraa     15-Jan-2014  Adapted to process also sheetmetal parts and assemblies
rem
rem ***************************************************************************
rem echo. > %~n0.job 

rem Process arguments and remove qoutes
set FILENAME_IN=%~1
set WORKDIR=%~2
set ITEMID=%~3
set REVID=%~4
set OUTPUTFILE=%ITEMID%-%REVID%.igs

for /f "tokens=1,2 delims=." %%a in ("%FILENAME_IN%") do set FILENAME=%%a&set FILETYPE=%%b

if %FILETYPE%==par goto :process_par
if %FILETYPE%==psm goto :process_psm
if %FILETYPE%==asm goto :process_asm
set RETURNCODE=-1 & goto :end

:process_par
	set SE_TOOL=D:\PDM\ST9\Program\SolidEdgeTranslationServices.exe

	echo "%SE_TOOL%" -i="%WORKDIR%\%FILENAME_IN%" -o="%WORKDIR%\result\%OUTPUTFILE%" -t=igs
	call "%SE_TOOL%" -i="%WORKDIR%\%FILENAME_IN%" -o="%WORKDIR%\result\%OUTPUTFILE%" -t=igs
	if %ERRORLEVEL% NEQ 0 set RETURNCODE=%ERRORLEVEL% 
goto :end

:process_psm
	set SE_TOOL=D:\PDM\ST9\Program\SolidEdgeTranslationServices.exe

	echo "%SE_TOOL%" -i="%WORKDIR%\%FILENAME_IN%" -o="%WORKDIR%\result\%OUTPUTFILE%" -t=igs
	call "%SE_TOOL%" -i="%WORKDIR%\%FILENAME_IN%" -o="%WORKDIR%\result\%OUTPUTFILE%" -t=igs
	if %ERRORLEVEL% NEQ 0 set RETURNCODE=%ERRORLEVEL%
goto :end

:process_asm
	echo 	plmxml_export -u=upm -p=upm -g=dba -transfermode=SE_UPM -item=%ITEMID% -rev=%REVID% -export_bom=yes -xml_file=%WORKDIR%\%ITEMID%.xml
	plmxml_export -u=upm -p=upm -g=dba -transfermode=SE_UPM -item=%ITEMID% -rev=%REVID% -export_bom=yes -xml_file=%WORKDIR%\%ITEMID%.xml

	echo Move all files from PLMXML export dir to UPM workdir
	if exist %WORKDIR%\%ITEMID%\*.par move %WORKDIR%\%ITEMID%\*.par %WORKDIR%\result
	if exist %WORKDIR%\%ITEMID%\*.psm move %WORKDIR%\%ITEMID%\*.psm %WORKDIR%\result
	if exist %WORKDIR%\%ITEMID%\*.asm move %WORKDIR%\%ITEMID%\*.asm %WORKDIR%\result

	echo Remove the PLMXML export dir
	rmdir /S /Q %WORKDIR%\%ITEMID%

	set SE_TOOL=D:\PDM\ST9\Program\SolidEdgeTranslationServices.exe

	echo "%SE_TOOL%" -i="%WORKDIR%\%FILENAME_IN%" -o="%WORKDIR%\result\%OUTPUTFILE%" -t=igs
	call "%SE_TOOL%" -i="%WORKDIR%\%FILENAME_IN%" -o="%WORKDIR%\result\%OUTPUTFILE%" -t=igs
	if %ERRORLEVEL% NEQ 0 set RETURNCODE=%ERRORLEVEL%
goto :end

:end
	echo Cleanup
	del /q %WORKDIR%\result\*.par
	del /q %WORKDIR%\result\*.psm
	del /q %WORKDIR%\result\*.asm
	del /q %WORKDIR%\result\*.cfg
	del /q %WORKDIR%\result\*.xml

EXIT /B %RETURNCODE%