@echo OFF
rem Copyright by Siemens PLM Software 2014
rem
rem Start script for the BnlExport.bat
rem
rem Siemens   Created                       01-04-2014
rem
rem
rem ********************************************************************

setlocal

rem ********************************************************************
rem INPUT PARAMETERS
rem ********************************************************************
set PARAM1=%~2
set PARAM2=%~8

echo PARAM1 = %PARAM1%
echo PARAM2 = %PARAM2%

if x%PARAM1%== x goto Help
if x%PARAM2%== x goto Help
if %1== -help goto Help

goto Setenv

:Help

echo Usage:
echo  Translator executable name: bnlnx2cgm.bat
echo  Command String is:
echo    bnlnx2cgm.bat inputFile outputdirectory
echo  Where two parameters are passed:
echo    Param 1) is the input file which has the @DB String
echo    Param 2) is the directory where the result files are going
echo    Example:-
echo      bnlnx2cgm.bat e:\ug_data\part.txt e:\ug_data
goto :EOF

:Setenv

set OUTPUT_FILE=%~2
set ITEM_ID=%~4
set REV_ID=%~6
set OUTPUT_DIR=%~8


for %%i in ("%OUTPUT_FILE%") do (
	echo Exported file %%~ni%%~xi still in the Working dir .. move it to the result dir
	move %%~ni%%~xi .\result > NUL
)

echo Copy %OUTPUT_FILE% to %OUTPUT_DIR%
if not exist %OUTPUT_DIR% md %OUTPUT_DIR%
copy /Y %OUTPUT_FILE% %OUTPUT_DIR%  > NUL

:end
EXIT %EXITVALUE%