@echo OFF
setlocal
rem # ============================================================================= #
rem   SCRIPT TO RUN PLMXML_IMPORT_EXPORT TRANSLATOR
rem # ============================================================================= #

rem Teamcenter Environment settings
set TC_ROOT=D:\PDM\Tc11
set TC_DATA=\\BDR-TDVOL11\PDMShare\DVDATA
set JAVA_HOME=D:\PDM\Java\jre1.8.0_92
set TOOL_DIR=D:\PDM\DP11\Module\Translators\plmxml_export_extra\xml2csv
set XALAN_HOME=%TOOL_DIR%\xalan-j_2_7_1
set classpath=%XALAN_HOME%\xalan.jar;%XALAN_HOME%\serializer.jar;%XALAN_HOME%\xml-apis.jar;%XALAN_HOME%\xercesImpl.jar;%JAVA_HOME%\lib\tools.jar

call %TC_DATA%\tc_profilevars

rem Bypass BNL Customizations...
set TC_PREFERENCES_OVERRIDE_FROM_FILE=TRUE
set TC_PREFERENCES_OVERLAY_FILE=%TC_DATA%\bnl_custom\bypass\TC_BYPASS_ALL.xml

set TC_USERNAME=dcproxy
set TC_PASSWD=dcproxy
set TC_GROUP=dba
set ITEMID=%~3
set REVID=%~4
set MAILID=%~5
set REVNAME=%~6
set REVRULE=Precise Only

rem Export required files; DXF, IGES, STEP and PDF
if exist ".\%ITEMID%_%REVID%" rmdir /S/Q ".\%ITEMID%_%REVID%"
echo "%TC_ROOT%\bin\plmxml_export" -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -item=%ITEMID% -rev=%REVID% -rev_rule="%REVRULE%" -export_bom=yes -xml_file=%ITEMID%_%REVID%.xml -transfermode="DIV_DP" -log=%ITEMID%_%REVID%.log
"%TC_ROOT%\bin\plmxml_export" -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -item=%ITEMID% -rev=%REVID% -rev_rule="%REVRULE%" -export_bom=yes -xml_file=%ITEMID%_%REVID%.xml -transfermode="DIV_DP" -log=%ITEMID%_%REVID%.log
if not exist ".\%ITEMID%_%REVID%" mkdir ".\%ITEMID%_%REVID%"

rem Create BoM Report
set PATH=%TOOL_DIR%\Software;%PATH%
set OLDPATH=%PATH%
echo "%JAVA_HOME%\bin\java" -Xms512m -Xmx1024m org.apache.xalan.xslt.Process -IN "%ITEMID%_%REVID%.xml" -XSL "%TOOL_DIR%\GenRMH_DIV-Report_001.xsl" -OUT "%ITEMID%_%REVID%\%ITEMID%_%REVID%.csv"
"%JAVA_HOME%\bin\java" -Xms512m -Xmx1024m org.apache.xalan.xslt.Process -IN "%ITEMID%_%REVID%.xml" -XSL "%TOOL_DIR%\GenRMH_DIV-Report_001.xsl" -OUT "%ITEMID%_%REVID%\%ITEMID%_%REVID%.csv"
set PATH=%OLDPATH%

rem Create zip file and include the BoM Report
"%TC_ROOT%\bin\zip" -q -j "%ITEMID%_%REVID%.zip" ".\%ITEMID%_%REVID%\*.*"

rem Send mail to workflow initiator
rem set MAIL_SERVER=outbounds200.obsmtp.com  DOET HET NIET !!!
:: set MAIL_SERVER=domino03.domein1.ap
:: set MAIL_SERVER=10.31.1.23
set MAIL_SERVER=domino03.domein1.ap
set MAIL_PORT=25
set MAIL_TO=%MAILID%
:: set MAIL_TO=carol.veraa@bdrthermea.com
set MAIL_SUBJECT=Specifcation Export of %ITEMID%/%REVID%
set MAIL_BODY=mail_body.txt

echo Please find attached the Specification Export data of ItemRevision > %MAIL_BODY%
echo. >> %MAIL_BODY%
echo   %ITEMID%/%REVID%-%REVNAME% >> %MAIL_BODY%
echo. >> %MAIL_BODY%
echo as exported from Teamcenter on %DATE% at %TIME%. >> %MAIL_BODY%
echo. >> %MAIL_BODY%

set MAIL_ATTACHMENTS=mail_attachments.txt
for %%* in (.) do set CURDIR=%%~f*
echo %CURDIR%\%ITEMID%_%REVID%.zip=B > %MAIL_ATTACHMENTS%

echo Sending Mail containing "%ITEMID%_%REVID%.zip" to "%MAIL_TO%"
rem DTJ: 5-oct-2016 temporary disabled upload for testing
rem tc_mail_smtp -to="%MAIL_TO%" -subject="%MAIL_SUBJECT%" -body="%MAIL_BODY%" -attachments="%MAIL_ATTACHMENTS%" -server=%MAIL_SERVER% -port=%MAIL_PORT%

set EXITVALUE=%ERRORLEVEL%

endlocal
@echo on
EXIT %EXITVALUE%
goto :EOF