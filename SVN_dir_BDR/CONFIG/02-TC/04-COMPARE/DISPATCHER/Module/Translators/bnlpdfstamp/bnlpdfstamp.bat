@echo OFF
:: **********************************************************************************
:: File Name: BNL_PDFSTAMP.bat
:: ***********************************************************************************
:: Revision history
::
:: REV  AUTHOR         DATE         COMMENT
:: ===  =============  ===========  ==================================================
:: 001  Carol Veraa    13-Nov-2014  Initial version
:: 002  Derek Tjon     27-Sep-2016  Converted for dispatcher
:: ***********************************************************************************
setlocal

rem ********************************************************************
rem INPUT PARAMETERS
rem ********************************************************************
set PDFFILE=%~1
set OUTPUTDIR=%~2
set STAMP=%~3

rem ********************************************************************
rem TRANSLATOR DIRECTORY
rem ********************************************************************
SET TRANSLATE_DIR=%~dp0

rem ********************************************************************
rem CHECK SHEET SIZE
rem ********************************************************************
:papersize
	set PDFSIZE=
	%TRANSLATE_DIR%\BnlResizePdf.exe -in="%PDFFILE%" > %OUTPUTDIR%\size.txt
	set /p SIZE= < %OUTPUTDIR%\size.txt
        rem del %OUTPUTDIR%\size.txt

	if "%SIZE%"=="" goto :no_size
	if %SIZE:~0,1%==A goto size_%SIZE%
goto :no_size

:size_A4P
	set SIZESPEC=-x=40 -y=0  -textsize=125 -textrotation=55
goto :process

:size_A4
	set SIZESPEC=-x=40 -y=0  -textsize=125 -textrotation=35
goto :process

:size_A3
	set SIZESPEC=-x=50 -y=-50  -textsize=175 -textrotation=35
goto :process

:size_A3P
	set SIZESPEC=-x=40 -y=0  -textsize=175 -textrotation=55
goto :process

:size_A2
	set SIZESPEC=-x=70 -y=-70  -textsize=250 -textrotation=35
goto :process

:size_A1
	set SIZESPEC=-x=50 -y=-80  -textsize=350 -textrotation=35
goto :process

:size_A0
	set SIZESPEC=-x=100 -y=-140  -textsize=500 -textrotation=35
goto :process

:no_size
	set SIZESPEC=-x=50 -y=-50  -textsize=175 -textrotation=35
goto :process

:process
	%TRANSLATE_DIR%\BnlStampPdf -in="%PDFFILE%" -out="%PDFFILE%" %SIZESPEC% -p=CC -text="%STAMP%" -textcolor=#999999 -textalign=CENTER -l=1 -o=0.5
	if %ERRORLEVEL% NEQ 0 set RETURNCODE=%ERRORLEVEL% & goto :end

	rem Remove *.bak.pdf
	if exist *.bak.pdf del *.bak.pdf

:end

rem ********************************************************************
rem Copy stamped file
rem ********************************************************************
copy %PDFFILE% %OUTPUTDIR%

:end
endlocal


EXIT /B %RETURNCODE%