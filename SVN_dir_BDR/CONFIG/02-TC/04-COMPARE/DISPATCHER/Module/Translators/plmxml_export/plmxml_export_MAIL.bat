@echo OFF
setlocal
rem # ============================================================================= #
rem   SCRIPT TO RUN PLMXML_IMPORT_EXPORT TRANSLATOR
rem # ============================================================================= #

rem Teamcenter Environment settings
set TC_ROOT=D:\PDM\Tc83
set TC_DATA=\\BDR-PRVOL01\prod
set JAVA_HOME=%TC_ROOT%\install\install\jre
set TOOL_DIR=D:\PDM\Disp\Module\Translators\plmxml_export\xml2csv
set XALAN_HOME=%TOOL_DIR%\xalan-j_2_7_1
set classpath=%XALAN_HOME%\xalan.jar;%XALAN_HOME%\serializer.jar;%XALAN_HOME%\xml-apis.jar;%XALAN_HOME%\xercesImpl.jar;%JAVA_HOME%\lib\tools.jar

call %TC_DATA%\tc_profilevars

rem Bypass BNL Customizations...
set TC_PREFERENCES_OVERRIDE_FROM_FILE=TRUE
set TC_PREFERENCES_OVERLAY_FILE=%TC_DATA%\bnl_custom\bypass\TC_BYPASS_ALL.xml
@echo ON
set TC_USERNAME=dcproxy
set TC_PASSWD=Te@mcent3r
set TC_GROUP=dba
set ITEMID=%~3
set REVID=%~4
set MAILID=%~5
set REVNAME=%~6
set IRSTATUS=%~7
set REVRULE=Precise Only

rem Export required files; DXF, IGES, STEP and PDF
if exist ".\%ITEMID%_%REVID%" rmdir /S/Q ".\%ITEMID%_%REVID%"
"%TC_ROOT%\bin\plmxml_export" -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -item=%ITEMID% -rev=%REVID% -rev_rule="%REVRULE%" -export_bom=yes -xml_file=%ITEMID%_%REVID%.xml -transfermode="PDF_DP" -log=%ITEMID%_%REVID%.log
if not exist ".\%ITEMID%_%REVID%" mkdir ".\%ITEMID%_%REVID%"

rem Create BoM Report
set PATH=%TOOL_DIR%\Software;%PATH%
set OLDPATH=%PATH%
"%JAVA_HOME%\bin\java" -Xms512m -Xmx1024m org.apache.xalan.xslt.Process -IN "%ITEMID%_%REVID%.xml" -XSL "%TOOL_DIR%\GenRMH_PDF-Report_005.xsl" -OUT "%ITEMID%_%REVID%\%ITEMID%_%REVID%.csv"
set PATH=%OLDPATH%

rem Create zip file and include the BoM Report
"%TC_ROOT%\bin\zip" -q -j "%ITEMID%_%REVID%.zip" ".\%ITEMID%_%REVID%\*.*"

rem Send mail to workflow initiator
:: set MAIL_SERVER=domino03.domein1.ap
:: set MAIL_SERVER=10.31.1.23
set MAIL_SERVER=domino03.domein1.ap
set MAIL_PORT=25
set MAIL_TO=%MAILID%
:: set MAIL_TO=evert-jan.neeleman@bdrthermea.com
:: set MAIL_TO=carol.veraa@bdrthermea.com
set MAIL_SUBJECT=Specifcation Export of %ITEMID%/%REVID%
set MAIL_BODY=mail_body.txt

echo Please find attached the Specification Export data of ItemRevision > %MAIL_BODY%
echo. >> %MAIL_BODY%
echo   %ITEMID%/%REVID%-%REVNAME% >> %MAIL_BODY%
echo. >> %MAIL_BODY%
echo as exported from Teamcenter on %DATE% at %TIME%. >> %MAIL_BODY%
echo. >> %MAIL_BODY%

set MAIL_ATTACHMENTS=mail_attachments.txt
for %%* in (.) do set CURDIR=%%~f*
echo %CURDIR%\%ITEMID%_%REVID%.zip=B > %MAIL_ATTACHMENTS%

echo Sending Mail containing "%ITEMID%_%REVID%.zip" to "%MAIL_TO%"
tc_mail_smtp -to="%MAIL_TO%" -subject="%MAIL_SUBJECT%" -body="%MAIL_BODY%" -attachments="%MAIL_ATTACHMENTS%" -server=%MAIL_SERVER% -port=%MAIL_PORT%

set EXITVALUE=%ERRORLEVEL%

echo Finished Processing ...
endlocal
@echo on
EXIT %EXITVALUE%
goto:EOF


:remove_status
	echo Remove any existing status
	call release_man -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -spec -status=Evaluation -dataset -item=%ITEMID% -rev=%REVID% -relation=IMAN_specification -datasetName=%ITEMID%/%REVID% -datasetType=Zip -unrelease > NUL
	call release_man -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -spec -status=Production -dataset -item=%ITEMID% -rev=%REVID% -relation=IMAN_specification -datasetName=%ITEMID%/%REVID% -datasetType=Zip -unrelease > NUL
goto:EOF

:add_status
	if not "%1" == "" (
		echo Processing status %1

		if "%1" EQU "Evaluation"  (
			call release_man -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -spec -status=Evaluation -dataset -item=%ITEMID% -rev=%REVID% -relation=IMAN_specification -datasetName=%ITEMID%/%REVID% -datasetType=Zip > NUL

			shift
			goto :add_status
		)
	
		if "%1" EQU "Production"  ( 
			call release_man -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -spec -status=Production -dataset -item=%ITEMID% -rev=%REVID% -relation=IMAN_specification -datasetName=%ITEMID%/%REVID% -datasetType=Zip > NUL

			shift
			goto :add_status
		)
		
		shift
		goto :add_status
	)
goto:EOF