rem @echo OFF
setlocal
rem # ============================================================================= #
rem   SCRIPT TO RUN PLMXML_IMPORT_EXPORT TRANSLATOR
rem # ============================================================================= #

rem Teamcenter Environment settings
set TC_ROOT=D:\PDM\Tc11
set TC_DATA=\\BDR-TDVOL11\PDMShare\DVDATA
set JAVA_HOME=D:\PDM\Java\jre1.8.0_92
set TOOL_DIR=D:\PDM\DP11\Module\Translators\plmxml_export_extra\xml2csv
set XALAN_HOME=%TOOL_DIR%\xalan-j_2_7_1
set classpath=%XALAN_HOME%\xalan.jar;%XALAN_HOME%\serializer.jar;%XALAN_HOME%\xml-apis.jar;%XALAN_HOME%\xercesImpl.jar;%JAVA_HOME%\lib\tools.jar

call %TC_DATA%\tc_profilevars

rem Bypass BNL Customizations...
set TC_PREFERENCES_OVERRIDE_FROM_FILE=TRUE
set TC_PREFERENCES_OVERLAY_FILE=%TC_DATA%\bnl_custom\bypass\TC_BYPASS_ALL.xml

set TC_USERNAME=dcproxy
set TC_PASSWD=dcproxy
set TC_GROUP=dba
set ITEMID=%~3
set REVID=%~4
set MAILID=%~5
set REVNAME=%~6
set IRSTATUS=%~7
set REVRULE=Precise Only

rem Export required files; DXF, IGES, STEP and PDF
if exist ".\%ITEMID%_%REVID%" rmdir /S/Q ".\%ITEMID%_%REVID%"
"%TC_ROOT%\bin\plmxml_export" -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -item=%ITEMID% -rev=%REVID% -rev_rule="%REVRULE%" -export_bom=yes -xml_file=%ITEMID%_%REVID%.xml -transfermode="PDF_DP" -log=%ITEMID%_%REVID%.log
if not exist ".\%ITEMID%_%REVID%" mkdir ".\%ITEMID%_%REVID%"

rem Create BoM Report
set PATH=%TOOL_DIR%\Software;%PATH%
set OLDPATH=%PATH%
"%JAVA_HOME%\bin\java" -Xms512m -Xmx1024m org.apache.xalan.xslt.Process -IN "%ITEMID%_%REVID%.xml" -XSL "%TOOL_DIR%\GenRMH_PDF-Report_005.xsl" -OUT "%ITEMID%_%REVID%\%ITEMID%_%REVID%.csv"
set PATH=%OLDPATH%

rem Create zip file and include the BoM Report
"%TC_ROOT%\bin\zip" -q -j "%ITEMID%_%REVID%.zip" ".\%ITEMID%_%REVID%\*.*"

rem Set the IPS tooling environment ...
set EXE_FILE=D:\PDM\IPS_Tools\IPS_EXE\ips_data_upload_tc8_3_nti_8_2_0e.exe
set INPTXT_FILE=%ITEMID%_%REVID%_Input.txt
set OUTLOG_FILE=%ITEMID%_%REVID%_Output.log
set CONFIG_FILE=D:\PDM\IPS_Tools\IPS_Upload_Zip_Datasets\DatasetUpload_config.cfg

echo !~ItemID~RevID~DsetName~DsetType~FileRef~File > %INPTXT_FILE%
echo %ITEMID%~%REVID%~%ITEMID%/%REVID%~Zip~ZIPFILE~%ITEMID%_%REVID%.zip >> %INPTXT_FILE%

rem Import file
rem DTJ: 5-oct-2016 temporary disabled upload for testing
rem %EXE_FILE% -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -cfg=%CONFIG_FILE% -i=%INPTXT_FILE% -v -o=%OUTLOG_FILE% -m=datasets > NUL
set EXITVALUE=%ERRORLEVEL%

rem Status processsing
rem IR Status can be "Empty" "Evaluation", "Production" "Evaluation, Production"  "Production, Evaluation" ... 
rem DS Status can be "Empty" or have any of the statusses from above

rem Because the DS status is unknown start with removing ...
call :remove_status
call :add_status %IRSTATUS%

echo Finished Processing ...

endlocal
@echo on
EXIT %EXITVALUE%
goto:EOF


:remove_status
	echo Remove any existing status
	call release_man -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -spec -status=Evaluation -dataset -item=%ITEMID% -rev=%REVID% -relation=IMAN_specification -datasetName=%ITEMID%/%REVID% -datasetType=Zip -unrelease > NUL
	call release_man -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -spec -status=Production -dataset -item=%ITEMID% -rev=%REVID% -relation=IMAN_specification -datasetName=%ITEMID%/%REVID% -datasetType=Zip -unrelease > NUL
goto:EOF

:add_status
	if not "%1" == "" (
		echo Processing status %1

		if "%1" EQU "Evaluation"  (
			call release_man -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -spec -status=Evaluation -dataset -item=%ITEMID% -rev=%REVID% -relation=IMAN_specification -datasetName=%ITEMID%/%REVID% -datasetType=Zip > NUL

			shift
			goto :add_status
		)
	
		if "%1" EQU "Production"  ( 
			call release_man -u=%TC_USERNAME% -p=%TC_PASSWD% -g=%TC_GROUP% -spec -status=Production -dataset -item=%ITEMID% -rev=%REVID% -relation=IMAN_specification -datasetName=%ITEMID%/%REVID% -datasetType=Zip > NUL

			shift
			goto :add_status
		)
		
		shift
		goto :add_status
	)
goto:EOF