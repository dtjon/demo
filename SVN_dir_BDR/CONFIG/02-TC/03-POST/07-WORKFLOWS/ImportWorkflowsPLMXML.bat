@ECHO OFF
TITLE Import Workflows
COLOR F0

::setting required variables
set TC_USER_ID=infodba
set TC_USER_PASSWORD=infodba

set KIT_DIR=%~dp0
FOR %%g in ("%KIT_DIR%") do SET KIT_DIR=%%~fsg
ECHO.
ECHO Running from current directory = %KIT_DIR%
cd /d %KIT_DIR%

set LOG_DIR=%KIT_DIR%\Logs

rem Exporting single workflow e.g BDR_REnder
rem set TEMPLATE_NAME=BDR_REnder
rem plmxml_import -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -xml_file="%KIT_DIR%\%TEMPLATE_NAME%.xml" -transfermode=workflow_template_overwrite -ignore_originid -log="%LOG_DIR%\import_%TEMPLATE_NAME%.log"

ECHO.
ECHO ****************************
ECHO Importing / overwriting Sub Workflow Templates...

for /F "tokens=* eol=;" %%A in ( %KIT_DIR%\WorkflowReportList.TXT) do  (
        ECHO Processing '%%A'
        plmxml_import -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -transfermode=workflow_template_overwrite -ignore_originid -xml_file="%KIT_DIR%\Workflows\%%A.xml" -log="%LOG_DIR%\import_%%A.log"
)

ECHO ****************************
ECHO.







































