@ECHO OFF
TITLE Export Workflows
COLOR F0

::setting required variables
set TC_USER_ID=infodba
set TC_USER_PASSWORD=infodba

set KIT_DIR=%~dp0
FOR %%g in ("%KIT_DIR%") do SET KIT_DIR=%%~fsg
ECHO.
ECHO Running from current directory = %KIT_DIR%
cd /d %KIT_DIR%

set LOG_DIR=%KIT_DIR%\Logs

rem Exporting single workflow e.g BDR_REnder
rem set TEMPLATE_NAME=BDR_REnder
rem plmxml_export -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -xml_file="%KIT_DIR%\%TEMPLATE_NAME%.xml" -transfermode=workflow_template_mode -template="%TEMPLATE_NAME%" -log="%LOG_DIR%\export_%TEMPLATE_NAME%.log"

ECHO.
ECHO ****************************
ECHO Exporting Workflow Templates...

for /F "tokens=* eol=;" %%A in ( %KIT_DIR%\WorkflowReportList.TXT) do  (
        ECHO Processing '%%A'
        plmxml_export -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -xml_file="%KIT_DIR%\Workflows\%%A.xml" -transfermode=workflow_template_mode -template="%%A" -log="%LOG_DIR%\export_%%A.log"

        cd "%LOG_DIR%
        java -jar ..\WorkflowReport.jar "%KIT_DIR%\Workflows\%%A.xml"
        cd ..
)

ECHO ****************************
ECHO.
