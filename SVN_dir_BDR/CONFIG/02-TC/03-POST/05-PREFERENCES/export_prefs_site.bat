@echo OFF
cls
TITLE Export Site Preferences

::storing current directory in variable
set KIT_DIR=%~dp0
for %%g in ("%KIT_DIR%") do set KIT_DIR=%%~fsg
cd /d %KIT_DIR%

echo.
echo Running from current directory:
echo.
echo   %KIT_DIR%

::Section for preferences_manager
echo.
echo ****************************
for /F "tokens=* eol=;" %%A in ( %KIT_DIR%\_SitePreferenceList.TXT) do  (
        echo Exporting '%%A' Preferences...
        rem preferences_manager %TC_PWF_infodba% -mode=export -scope=SITE -out_file="%KIT_DIR%\Site_Preferences\%%A.xml" -file="%%A.txt"
		preferences_manager %TC_PWF_infodba% -mode=export -scope=SITE -out_file="%KIT_DIR%\Site_Preferences\%%A.xml" -file="%%A.txt"
		echo.
		echo.
)

echo ****************************
echo.

pause
