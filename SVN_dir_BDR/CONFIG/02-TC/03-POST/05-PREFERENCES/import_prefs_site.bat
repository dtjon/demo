@echo OFF
TITLE Import SEEC Site Preferences

::setting required variables
set NR_BYPASS=ON
set BYPASS_RULES=ON
set BMF_BYPASS_ALL_EXTENSION_RULES=ON

::storing current directory in variable
set KIT_DIR=%~dp0
FOR %%g in ("%KIT_DIR%") do SET KIT_DIR=%%~fsg
echo.
echo Running from current directory:
echo.
echo   %KIT_DIR%

:: HiddenPerspectives
::=====================================================================================================================================================

::Section for preferences_manager
echo.
echo ****************************

for /F "tokens=* eol=;" %%A in ( %KIT_DIR%\_SitePreferenceList.TXT) do  (
        echo importing '%%A' Preferences...
        preferences_manager %TC_PWF_infodba% -mode=import -scope=SITE -action=OVERRIDE -file="%KIT_DIR%\%%A.xml"
		echo.
		echo.		
)

echo ****************************
echo.


::=====================================================================================================================================================

pause