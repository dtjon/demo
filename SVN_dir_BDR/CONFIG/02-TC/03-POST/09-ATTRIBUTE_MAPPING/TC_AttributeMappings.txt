# Attribute Synchronisation mapping file for site "VM Dev Environment" (id -1988937320)
# TC_ROOT = E:\PLM\Teamcenter10
# TC_DATA = F:\PLM\plmshare\tcdev_data
# TC_DB_CONNECT = infodba:Mg/h48WSGRvDRI5u7SdQA3v9073PkLEdP9m8U5tgEDg@DATABASE=TCDEV;SERVER=WIN-G2LQ6SFSU7A,1433
# Exported by infodba (infodba) at 2016/01/12 13:45:37
#
# Syntax:
#    <title> : <mapping> [ <qualifiers> ]
#    where <mapping> is one of:
#        IM
#        IRM
#        CONST( <value> )
#        PREF( [ <scope> : ] <value> )
#        <step>.<step>...<property>
#        where <step> is one of:
#            <property>
#            GRM( <relationship-type> [ , <object-type> ] )
#            NR( <named-reference-type> [ , <form-type> ] )
#            SIGNOFF( [ <release-level> , ] <role> )
#            ItemRevision
#            Item
#            Job
#    and <qualifiers> are:
#        /write_once
#        /description=<description>
#        /master=both|cad|iman|none
#        /freezable
#
# Real mappings:
#
# NOTE: for item master form the qualifier master=nnn is reassigned to master=iman
#       where nnn can be cad, both, or none.
#

# (hard-wired) DB_UNITS : "Part Unit of Measure"
# (hard-wired) DB_PART_TYPE : "Part Type"
# (hard-wired) DB_PART_REV : "Part Revision"
# (hard-wired) DB_PART_NO : "Part Number"
# (hard-wired) DB_PART_NAME : "Part Name"
# (hard-wired) DB_PART_DESC : "Part Description"

{ Dataset type="ACADDWF"
}

{ Dataset type="ACADDWG"
}

{ Dataset type="ACADTMPL"
}

{ Dataset type="AdobeIllustrator"
}

{ Dataset type="Bitmap"
}

{ Dataset type="Bitmap_Thumbnail"
}

{ Dataset type="Briefcase"
}

{ Dataset type="CAEAnalysisDS"
}

{ Dataset type="CAEBC"
}

{ Dataset type="CAEConn"
}

{ Dataset type="CAEGeom"
    # (hard-wired) DB_PART_NAME : "Part Name"
    # (hard-wired) DB_PART_DESC : "Part Description"

}

{ Dataset type="CAELoad"
}

{ Dataset type="CAEMesh"
    # (hard-wired) DB_PART_NAME : "Part Name"
    # (hard-wired) DB_PART_DESC : "Part Description"

}

{ Dataset type="CAEResults"
}

{ Dataset type="CAESolution"
    # (hard-wired) DB_PART_NAME : "Part Name"
    # (hard-wired) DB_PART_DESC : "Part Description"

}

{ Dataset type="CAESolver"
}

{ Dataset type="CAEStructureMap"
}

{ Dataset type="CLIPS"
}

{ Dataset type="CME_Report"
}

{ Dataset type="CT22CollectionLogicXML"
}

{ Dataset type="ClearanceDBConnection"
}

{ Dataset type="ClearanceDBReport"
}

{ Dataset type="ClearanceResults"
}

{ Dataset type="ClearanceResultsFilter"
}

{ Dataset type="CrfExcelStylesheet"
}

{ Dataset type="CrfHtmlStylesheet"
}

{ Dataset type="CrfMSWordDocStylesheet"
}

{ Dataset type="CrfOutputExcel"
}

{ Dataset type="CrfOutputHtml"
}

{ Dataset type="CrfOutputMSWord"
}

{ Dataset type="CrfOutputPdf"
}

{ Dataset type="CrfOutputText"
}

{ Dataset type="CrfOutputXml"
}

{ Dataset type="CrfPdfStylesheet"
}

{ Dataset type="CrfTextStylesheet"
}

{ Dataset type="CrfXmlStylesheet"
}

{ Dataset type="DFA_KF"
}

{ Dataset type="DXF"
}

{ Dataset type="DirectModel"
}

{ Dataset type="DirectModel3DMarkup"
}

{ Dataset type="DirectModelAnimation"
}

{ Dataset type="DirectModelAssembly"
}

{ Dataset type="DirectModelFlowChart"
}

{ Dataset type="DirectModelIllustrationBook"
}

{ Dataset type="DirectModelIllustrationSheet"
}

{ Dataset type="DirectModelMarkup"
}

{ Dataset type="DirectModelMotion"
}

{ Dataset type="DirectModelSession"
}

{ Dataset type="DrawingSheet"
}

{ Dataset type="ECSnapshot"
}

{ Dataset type="ExcelReportFormatter"
}

{ Dataset type="Fnd0BMIDEDeployArchive"
}

{ Dataset type="Fnd0BMIDEProjectBackup"
}

{ Dataset type="Fnd0BMIDEResource"
}

{ Dataset type="Fnd0ContextMenuSuppRuleXML"
}

{ Dataset type="Fnd0IconResource"
}

{ Dataset type="Fnd0LaunchPadRendering"
}

{ Dataset type="Fnd0PropertyMap"
}

{ Dataset type="Fnd0ReqMarkup"
}

{ Dataset type="Fnd0SharedMetadataCache"
}

{ Dataset type="Fnd0SpatialHierarchy"
}

{ Dataset type="Fnd0TcVisConstraint"
}

{ Dataset type="Fnd0TcVisFilter"
}

{ Dataset type="Fnd0Visio"
}

{ Dataset type="Fnd0VisioTmplDataset"
}

{ Dataset type="FullText"
}

{ Dataset type="GIF"
}

{ Dataset type="HPGL"
}

{ Dataset type="HTML"
}

{ Dataset type="ICSClassFiles"
}

{ Dataset type="IH4_CadmaticXML"
}

{ Dataset type="IH4_Hydroman"
}

{ Dataset type="IH4_Hydrosym"
}

{ Dataset type="IH4_IGES"
}

{ Dataset type="IH4_STEP"
}

{ Dataset type="IMANReportData"
}

{ Dataset type="IMANReportDesign"
}

{ Dataset type="IPAConfig"
}

{ Dataset type="Image"
}

{ Dataset type="JPEG"
}

{ Dataset type="JPEG_Thumbnail"
}

{ Dataset type="JtSimplification"
}

{ Dataset type="KRX"
}

{ Dataset type="MEMachineDriver"
}

{ Dataset type="MISC"
}

{ Dataset type="MSExcel"
}

{ Dataset type="MSExcelTemplate"
}

{ Dataset type="MSExcelTemplateX"
}

{ Dataset type="MSExcelX"
}

{ Dataset type="MSPowerPoint"
}

{ Dataset type="MSPowerPointTemplate"
}

{ Dataset type="MSPowerPointTemplateX"
}

{ Dataset type="MSPowerPointX"
}

{ Dataset type="MSProject"
}

{ Dataset type="MSWord"
}

{ Dataset type="MSWordTemplate"
}

{ Dataset type="MSWordTemplateX"
}

{ Dataset type="MSWordX"
}

{ Dataset type="Markup"
}

{ Dataset type="Mfg0DMIS"
}

{ Dataset type="Mfg0ME3DPDFInputs"
}

{ Dataset type="Mfg0PlantCarpet"
}

{ Dataset type="NX0ApplicationData"
}

{ Dataset type="NX0BookletDataset"
}

{ Dataset type="NX0RoutingPipePartData"
}

{ Dataset type="NX0RoutingRunData"
}

{ Dataset type="NX0RoutingSpecData"
}

{ Dataset type="NXAFEM"
}

{ Dataset type="NXDerived"
}

{ Dataset type="NXMotion"
}

{ Dataset type="NXRouting"
}

{ Dataset type="NXSimulation"
}

{ Dataset type="Nastran"
}

{ Dataset type="NetworkImage"
}

{ Dataset type="Outlook"
}

{ Dataset type="PCF"
}

{ Dataset type="PDF"
}

{ Dataset type="PDX"
}

{ Dataset type="PNG_Thumbnail"
}

{ Dataset type="Photoshop"
}

{ Dataset type="PortfolioHFT"
}

{ Dataset type="Postscript"
}

{ Dataset type="ProgramView"
}

{ Dataset type="RollupReport"
}

{ Dataset type="SE Assembly"
	{ Item type="IH4_CommodityPrt"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
        "Mass" : ItemRevision.ih4_WeightCAD /master=cad /description="Mass"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
        "CMz" : ItemRevision.ih4_C0GCalZ /master=cad /description="COG - Z"
        "CMy" : ItemRevision.ih4_COGCalY /master=cad /description="COG - Y"
        "CMx" : ItemRevision.ih4_COGCalX /master=cad /description="COG - X"
    }
	
	{ Item type="IH4_DesignedPart"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
        "Mass" : ItemRevision.ih4_WeightCAD /master=cad /description="Mass"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
        "CMz" : ItemRevision.ih4_C0GCalZ /master=cad /description="COG - Z"
        "CMy" : ItemRevision.ih4_COGCalY /master=cad /description="COG - Y"
        "CMx" : ItemRevision.ih4_COGCalX /master=cad /description="COG - X"
    }
	
	{ Item type="IH4_BaseMaterial"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_ManufPart"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_ManufSpool"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_RoutingPart"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_MakeAssy"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }	
}

{ Dataset type="SE Component"
}

{ Dataset type="SE Connection"
}

{ Dataset type="SE Draft"
    { Item type="IH4_ProdDesign"
        "PBS_ITEM_REV_ID" : ItemRevision.ih4_RelToPBS.item_revision_id /master=iman /description="PBS Item Revision ID"
        "PBS_ITEM_NAME" : ItemRevision.ih4_RelToPBS.object_name /master=iman /description="PBS Item Name"
        "PBS_ITEM_ID" : ItemRevision.ih4_RelToPBS.item_id /master=iman /description="PBS Item ID"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
    }

    { Item type="IH4_CommodityPrt"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
    }

    { Item type="IH4_DesignedPart"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
    }

}

{ Dataset type="SE Part"
	{ Item type="IH4_CommodityPrt"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
        "Material" : ItemRevision.ih4_Material /master=cad /description="Material"
        "Mass" : ItemRevision.ih4_WeightCAD /master=cad /description="Mass"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
        "CMz" : ItemRevision.ih4_C0GCalZ /master=cad /description="COG - Z"
        "CMy" : ItemRevision.ih4_COGCalY /master=cad /description="COG - Y"
        "CMx" : ItemRevision.ih4_COGCalX /master=cad /description="COG - X"
    }
	
	{ Item type="IH4_DesignedPart"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
        "Material" : ItemRevision.ih4_Material /master=cad /description="Material"
        "Mass" : ItemRevision.ih4_WeightCAD /master=cad /description="Mass"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
        "CMz" : ItemRevision.ih4_COGCalZ /master=cad /description="COG - Z"
        "CMy" : ItemRevision.ih4_COGCalY /master=cad /description="COG - Y"
        "CMx" : ItemRevision.ih4_COGCalX /master=cad /description="COG - X"
    }
	
	{ Item type="IH4_BaseMaterial"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_ManufPart"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_ManufSpool"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_RoutingPart"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_MakeAssy"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }	
}

{ Dataset type="SE SheetMetal"
	{ Item type="IH4_CommodityPrt"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
        "Material" : ItemRevision.ih4_Material /master=cad /description="Material"
        "Mass" : ItemRevision.ih4_WeightCAD /master=cad /description="Mass"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
        "CMz" : ItemRevision.ih4_C0GCalZ /master=cad /description="COG - Z"
        "CMy" : ItemRevision.ih4_COGCalY /master=cad /description="COG - Y"
        "CMx" : ItemRevision.ih4_COGCalX /master=cad /description="COG - X"
    }
	
	{ Item type="IH4_DesignedPart"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
        "Material" : ItemRevision.ih4_Material /master=cad /description="Material"
        "Mass" : ItemRevision.ih4_WeightCAD /master=cad /description="Mass"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
        "CMz" : ItemRevision.ih4_C0GCalZ /master=cad /description="COG - Z"
        "CMy" : ItemRevision.ih4_COGCalY /master=cad /description="COG - Y"
        "CMx" : ItemRevision.ih4_COGCalX /master=cad /description="COG - X"
    }
	
	{ Item type="IH4_BaseMaterial"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_ManufPart"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_ManufSpool"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_RoutingPart"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_MakeAssy"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }	
}

{ Dataset type="SE Weldment"
	{ Item type="IH4_CommodityPrt"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_DesignedPart"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_BaseMaterial"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_ManufPart"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_ManufSpool"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_RoutingPart"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
	
	{ Item type="IH4_MakeAssy"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }	
}

{ Dataset type="STL"
}

{ Dataset type="Sheet"
}

{ Dataset type="SimulationProcessStudio"
}

{ Dataset type="SnapShotViewData"
}

{ Dataset type="Spatial_Data"
}

{ Dataset type="TCPublishingAliases"
}

{ Dataset type="TCPublishingPage"
}

{ Dataset type="TCPublishingTechnicalPortfolio"
}

{ Dataset type="TIF"
}

{ Dataset type="TechnicalPortfolioXML"
}

{ Dataset type="TempDirectModelMarkup"
}

{ Dataset type="TempMarkup"
}

{ Dataset type="Text"
}

{ Dataset type="UGALTREP"
}

{ Dataset type="UGCAMCLSF"
}

{ Dataset type="UGCAMPTP"
}

{ Dataset type="UGCAMShopDoc"
}

{ Dataset type="UGCAMTemplateData"
}

{ Dataset type="UGENGINEROLL"
}

{ Dataset type="UGMASTER"
    # (hard-wired) DB_PART_NAME : "Part Name"
    # (hard-wired) DB_PART_DESC : "Part Description"

    { Item type="Item"
        "DATE_CREATED" : ItemRevision.creation_date /master=iman /description="DATE CREATED"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
    }

    { Item type="IH4_ProdDesign"
        "DATE_CREATED" : ItemRevision.creation_date /master=iman /description="DATE CREATED"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
    }

    { Item type="IH4_CommodityPrt"
        "DATE_CREATED" : ItemRevision.creation_date /master=iman /description="DATE CREATED"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
		"CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"		
    }

    { Item type="IH4_DesignedPart"
        "DATE_CREATED" : ItemRevision.creation_date /master=iman /description="DATE CREATED"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
		"CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }

    { Item type="IH4_ManufPart"
        "SA_COMPONENT_WIDTH" : ItemRevision.ih4_Width /master=cad /description="SA_COMPONENT_WIDTH"
        "SA_COMPONENT_PARTID" : ItemRevision.ih4_PosNo /master=cad /description="SA_COMPONENT_PARTID"
        "SA_COMPONENT_LENGTH" : ItemRevision.ih4_Length /master=cad /description="SA_COMPONENT_LENGTH"
        "SA_COMPONENT_FORM" : ItemRevision.ih4_ComponentForm /master=cad /description="SA_COMPONENT_FORM"
        "SAW_TYPE" : ItemRevision.ih4_Type /master=cad /description="SAW_TYPE"
        "SAW_THICKNESS" : ItemRevision.ih4_Thickness /master=cad /description="SAW_THICKNESS"
        "SAW_SUB_NAME" : ItemRevision.ih4_IsHull /master=cad /description="SAW_SUB_NAME"
        "SAW_PARAMETER" : ItemRevision.ih4_ProfileType /master=cad /description="SAW_PARAMETER"
        "SAW_MATERIAL_GRADE" : ItemRevision.ih4_MaterialGrade /master=cad /description="SAW_MATERIAL_GRADE"
        "SAW_MASS_D" : ItemRevision.ih4_WeightCAD /master=cad /description="SAW_MASS_D"
        "SECTION" : ItemRevision.ih4_Section /master=iman /description="SECTION"
        "SUB_SECTION" : ItemRevision.ih4_SubSection /master=iman /description="SUB_SECTION"
        "PANEL" : ItemRevision.ih4_Panel /master=iman /description="PANEL"
        "SUB_PANEL" : ItemRevision.ih4_SubPanel /master=iman /description="SUB_PANEL"
		"END_ITEM_ID" : ItemRevision.ih4_EndItemID /master=iman /description="END_ITEM_ID"
		"TRANSPORT" : ItemRevision.ih4_TransportFromBOP /master=iman /description="TRANSPORT"
		"WORKAREA" : ItemRevision.ih4_WorkAreaFromBOP /master=iman /description="WORKAREA"
        "DATE_CREATED" : ItemRevision.creation_date /master=iman /description="DATE CREATED"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
		"CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }

    { Item type="IH4_RoutingPart"
        "MEMBER_NAME" : ItemRevision. ih4_NX_MadeFrom /master=cad /description="MEMBER_NAME"                       
        "LENGTH" : ItemRevision. ih4_NX_CutLength /master=cad /description="LENGHT"
		"CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }
		
	{ Item type="IH4_BaseMaterial"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }	
	
	{ Item type="IH4_ManufSpool"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"

    }
	
	{ Item type="IH4_MakeAssy"
        "CAD Owner" : ItemRevision.ih4_CADOwner /master=cad /description="CAD Owner"
    }	
	
	
}

{ Dataset type="UGPART"
    # (hard-wired) DB_DATASET_REV : "Part File Rev"
    # (hard-wired) DB_DATASET_ID : "Part File ID"

    { Item type="Item"
        "DATE_CREATED" : ItemRevision.creation_date /master=iman /description="DATE CREATED"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
    }

    { Item type="IH4_WeldContainr"
        "DATE_CREATED" : ItemRevision.creation_date /master=iman /description="DATE CREATED"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
    }

    { Item type="IH4_CommodityPrt"
        "DATE_CREATED" : ItemRevision.creation_date /master=iman /description="DATE CREATED"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
    }

    { Item type="IH4_DesignedPart"
        "DATE_CREATED" : ItemRevision.creation_date /master=iman /description="DATE CREATED"
        "CREATOR" : ItemRevision.owning_user.user_name /master=iman /description="CREATOR"
    }

}

{ Dataset type="UGSCENARIO"
}

{ Dataset type="UGTIREOFFSET"
}

{ Dataset type="UGTIRESPINDLE"
}

{ Dataset type="VSAMeasurementDoc"
}

{ Dataset type="VSAProcessDoc"
}

{ Dataset type="ValidationResultLog"
}

{ Dataset type="ValidationRuleSet"
}

{ Dataset type="VisJackEnvironment"
}

{ Dataset type="VisJackFigure"
}

{ Dataset type="VisJackPosture"
}

{ Dataset type="VisJackPsurf"
}

{ Dataset type="VisMovieCapture"
}

{ Dataset type="VisPublishGeometryAsset"
}

{ Dataset type="VisPublishTechnicalIllustration"
}

{ Dataset type="VisPublishTechnicalPortfolio"
}

{ Dataset type="VisPublishVisioStencil"
}

{ Dataset type="VisPublishVisioTemplate"
}

{ Dataset type="VisQualityCluster"
}

{ Dataset type="Vis_Session"
}

{ Dataset type="Vis_Snapshot_2D_View_Data"
}

{ Dataset type="VisualReportDefinition"
}

{ Dataset type="WordPerfect"
}

{ Dataset type="XMLAuditLog"
}

{ Dataset type="XMLRenderingStylesheet"
}

{ Dataset type="XMLReportFormatter"
}

{ Dataset type="Zip"
}

{ Dataset type="Zone"
}

