@ECHO OFF
TITLE Import Attribute Mapping
COLOR F0

::setting required variables
set TC_USER_ID=infodba
set TC_USER_PASSWORD=infodba


::storing current directory in variable
set KIT_DIR=%~dp0
FOR %%g in ("%KIT_DIR%") do SET KIT_DIR=%%~fsg
ECHO.
ECHO Running from current directory = %KIT_DIR%

:: Attribute Mapping
::============================================================================================================
ECHO importing Attribute Mapping...
import_attr_mappings -u=%TC_USER_ID% -p=%TC_USER_PASSWORD% -g=dba -file="%KIT_DIR%\TC_AttributeMappings.txt"
::============================================================================================================


PAUSE