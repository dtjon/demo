@ECHO OFF
TITLE Import All Queries
COLOR F0

::setting required variables
set TC_USER_ID=infodba
set TC_USER_PASSWORD=infodba


::storing current directory in variable
set KIT_DIR=%~dp0
FOR %%g in ("%KIT_DIR%") do SET KIT_DIR=%%~fsg
ECHO.
ECHO Running from current directory = %KIT_DIR%

:: Import All Queries
::=====================================================================================================================================================

::Section for Import All Queries
ECHO.
ECHO ****************************
ECHO importing All Queries...

plmxml_import -u=infodba -p=infodba -g=dba -xml_file=%KIT_DIR%\all_queries.xml -log=%KIT_DIR%\all_queries.log -import_mode=overwrite