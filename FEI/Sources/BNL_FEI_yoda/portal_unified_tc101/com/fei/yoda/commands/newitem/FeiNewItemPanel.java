/***********************************************************************************************************************
 (C) copyright by Siemens PLM Software

 Created by     :   J. Mansvelders 

 Description	:   Custom New Item Panel
 
 Created for    :   FEI

 Comments       :   

 Filename       :   FeiNewItemPanel.java
 
 Using modules  :   

 Version info   :   The number consist of the iman release and then the release
 <release>.<pointrelease>

 
 History of changes                                            
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.1         JM          07-04-2009
 Updated                                                                0.2         JM          10-04-2009
 Updated resetting of IMF and IRMF                                      0.3         JM          02-11-2009
 Reviewed                                                               0.4         JM          19-11-2009
 Enhanced bypass check                                                  0.5         JM          12-03-2010
 Updated for Teamcenter 8.3.*                                           0.6         JM          09-09-2011
 Updated for Teamcenter 10.1.*                                          0.7         JWO         28-06-2016
 ************************************************************************************************************************/

package com.fei.yoda.commands.newitem;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import com.fei.yoda.util.FeiUtils;
import com.teamcenter.rac.commands.newitem.ItemInfoPanel;
import com.teamcenter.rac.commands.newitem.ItemMasterFormPanel;
import com.teamcenter.rac.commands.newitem.ItemRevMasterFormPanel;
import com.teamcenter.rac.commands.newitem.NewItemDialog;
import com.teamcenter.rac.commands.newitem.NewItemPanel;
import com.teamcenter.rac.common.lov.view.components.LOVDisplayer;
import com.teamcenter.rac.form.AbstractTCForm;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.stylesheet.AbstractRendering;
import com.teamcenter.rac.util.MessageBox;

public class FeiNewItemPanel extends NewItemPanel {
	
	private String itemTypeSelected = null;

    private LOVDisplayer uomLovComboBox;
    //private TCComponentListOfValues oldUomLov = null;
    private LOVDisplayer oldUomLov = null;
	private ItemInfoPanel itemInfoP = null;
	private int assigned = 0;
	private int focusFlag = 0;
	private	int masterFormResetted = 0;
	private int masterRevFormResetted = 0;
	
	public FeiNewItemPanel(Frame arg0, TCSession arg1, NewItemDialog arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("deprecation")
	public FeiNewItemPanel(Frame arg0, TCSession arg1, NewItemDialog arg2,
			Boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	
	public Object getUOMComp()
    {
		if (itemInfoP.uomLovComboBox == null)
		{
			System.out.println("FeiNewItemPanel: No combobox.");
			return null;
		}
		
		// Get the selection
		Object obj = itemInfoP.uomLovComboBox.getSelectedValue();
		if (obj == null || obj.toString().length() == 0)
        {
			System.out.println("FeiNewItemPanel: Nothing selected.");
        	return obj;
        }
		else if(obj instanceof TCComponent)
        {
			System.out.println("FeiNewItemPanel: Default uom.");
            return (TCComponent)obj;
        }
        else
        {
			System.out.println("FeiNewItemPanel: Custom uom.");
			
			return obj;

        /*	try
        	{
        		//TCComponent[] lovValues = oldUomLov.getListOfValues().getTagListOfValues();
        		oldUomLov.get
        		
        		System.out.println("FeiNewItemPanel: Looking up the selected uom.");
        		
        		for (int i = 0; i < lovValues.length; i++)
        		{
        			TCComponent comp = lovValues[i];
        			
        			if (obj.toString().equalsIgnoreCase(comp.getProperty("symbol")))
        			{
        				System.out.println("FeiNewItemPanel: UOM found.");
        				return comp;
        			}
        		}
        	}
        	catch (Exception e)
        	{
        		e.printStackTrace();
        	}*/

        	//System.out.println("FeiNewItemPanel: Warning, UOM has not been found. Probably nothing has been selected.");
        	
          //  return null;
        }
    }

	public void finish()
	{
		super.finish();
		System.out.println("After finish");
		masterFormResetted = 0;
		masterRevFormResetted = 0;
	}
	
	public void itemTypeSelected()
	{
		super.itemTypeSelected();
		System.out.println("After itemTypeSelected");
		masterFormResetted = 0;
		masterRevFormResetted = 0;
	}
	
	@SuppressWarnings("deprecation")
	public void afterSetPanel(int i)
	{
		super.afterSetPanel(i);
		
		if (i == getItemMasterFormStep() && masterFormResetted == 0) {
			
			ItemMasterFormPanel itemMasterFormP = (ItemMasterFormPanel)stepPanels.get(i);
			
			TCComponentForm theIMF = (TCComponentForm)FeiUtils.getClassField(itemMasterFormP, "itemMasterForm");
			//TCComponentForm theIMF = itemMasterFormP.itemMasterForm;
			
			AbstractTCForm theIMFPanel = itemMasterFormP.formPanel;
			AbstractRendering theIMFStylesheetPanel = (AbstractRendering)FeiUtils.getClassField(itemMasterFormP, "stylesheetPanel");
			//AbstractRendering theIMFStylesheetPanel = itemMasterFormP.stylesheetPanel;
			
			FeiUtils.resetFormValues(false, theIMF, theIMF, theIMFPanel, theIMFStylesheetPanel, session);
			
			masterFormResetted = 1;
		}
		else if (i == getItemRevMasterFormStep() && masterRevFormResetted == 0) {
			
			ItemRevMasterFormPanel itemRevMasterFormP = (ItemRevMasterFormPanel)stepPanels.get(i);
		
			TCComponentForm theIRMF = (TCComponentForm)FeiUtils.getClassField(itemRevMasterFormP, "itemRevMasterForm");
			//TCComponentForm theIRMF = itemRevMasterFormP.itemRevMasterForm;

			AbstractTCForm theIRMFPanel = itemRevMasterFormP.formPanel;
			AbstractRendering theIRMFStylesheetPanel = (AbstractRendering)FeiUtils.getClassField(itemRevMasterFormP, "stylesheetPanel");
			//AbstractRendering theIRMFStylesheetPanel = itemRevMasterFormP.stylesheetPanel;
			
			FeiUtils.resetFormValues(false, theIRMF, theIRMF, theIRMFPanel, theIRMFStylesheetPanel, session);
			
			masterRevFormResetted = 1;
		}
		 
		// Needs the UOM LOV to be updated?
		if (itemTypeSelected == null || itemTypeSelected.compareTo(getItemType()) != 0)
		{
			masterFormResetted = 0;
			masterRevFormResetted = 0;

			System.out.println("FeiNewItemPanel: Item type has been changed");
		 	itemTypeSelected = getItemType();
		 	
		 	if (oldUomLov != null)
		 	{
		 		//uomLovComboBox.setLovComponent(oldUomLov, null);
		 		uomLovComboBox = oldUomLov;
		 		oldUomLov = null;
			}
		}

		if (i == getItemInfoStep())
		{
			itemInfoP = (ItemInfoPanel)stepPanels.get(i);
		 	
		 	// Update uom
			if (oldUomLov == null)
		 	{
		 		System.out.println("FeiNewItemPanel: Checking if the uom combobox should be updated.");
		 		
		 		// Check for the preference
		 		try
		 		{
		 			// Check for bypass
		 			if (this.session.isUserSystemAdmin() && this.session.hasBypass())
		 			{
		 				System.out.println("FeiNewItemPanel: Bypass has been enabled, skipping UOM update.");
		 			}
		 			else
		 			{
			 			String prefName = "FEI_" + itemTypeSelected + "_UOM_LOV";
			 			System.out.println("FeiNewItemPanel: Looking for preference " + prefName);
			 			
			 			TCPreferenceService prefService = session.getPreferenceService();
			 			String uomLov = prefService.getString(TCPreferenceService.TC_preference_site, prefName);
			 			
			 			System.out.println("FeiNewItemPanel: Found value: " + uomLov);
			 			
			 			if (uomLov != null && uomLov.length() > 0)
			 			{
			 				// Check if configured lov does exist
			 				//TCComponentListOfValues lov = LOVUIComponent.findLOVByName(uomLov);
			 				TCComponentListOfValues lov = TCComponentListOfValuesType.findLOVByName(uomLov);
			 				if (lov == null)
			 				{
			 					System.out.println("FeiNewItemPanel: Warning, failed to update the combobox. The configured lov cannot be found.");
			 				}
			 				else
			 				{
			 					System.out.println("FeiNewItemPanel: Update combobox.");
			 					uomLovComboBox = itemInfoP.uomLovComboBox;
			 					oldUomLov = itemInfoP.uomLovComboBox;
			 					uomLovComboBox.setPropertyData(lov);
			 				//	oldUomLov = itemInfoP.uomLovComboBox.getLovComponent();
			 				//  uomLovComboBox.setLovComponent(lov, null);
			 							 		
			 					System.out.println("FeiNewItemPanel: Combobox has been updated.");
			 				}
			 			}
		 			}
		 		}
		 		catch (Exception e)
		 		{
		 			e.printStackTrace();
		 		}
		 	}
			 
		 	itemInfoP.assignButton.addActionListener(new ActionListener()
		 	{

				public void actionPerformed(ActionEvent e) {
					itemInfoP.idTextField.requestFocus();
					assigned = 1;
				}
		 	});
			 
		 	itemInfoP.nameTextField.setText(itemInfoP.idTextField.getText());
			 
		 	itemInfoP.idTextField.addFocusListener(new FocusListener()
		 	{
				public void focusGained(FocusEvent e)
				{
					// dummy
				}

				public void focusLost(FocusEvent e)
				{
					if (itemInfoP.nameTextField.getText().compareTo(itemInfoP.idTextField.getText()) != 0) {
						System.out.println("FeiNewItemPanel: Setting name field (1).");
						itemInfoP.nameTextField.setText(itemInfoP.idTextField.getText());
					}
				}
		 	});

		 	itemInfoP.nameTextField.addFocusListener(new FocusListener()
		 	{
		 		public void focusGained(FocusEvent e)
		 		{
		 			if (assigned == 1)
		 			{
		 				System.out.println("FeiNewItemPanel: Setting name field (2).");
		 				itemInfoP.nameTextField.setText(itemInfoP.idTextField.getText());
		 				assigned = 0;
		 			}
		 		}
	
		 		public void focusLost(FocusEvent e)
		 		{
		 			// dummy
				}
	 		});

		 	itemInfoP.descTextArea.addFocusListener(new FocusListener()
		 	{
		 		ItemInfoPanel m_itemInfoP = itemInfoP;

				public void focusGained(FocusEvent e)
				{
					// Dummy
				}
				

				public void focusLost(FocusEvent e)
				{
                    if (focusFlag == 0)
                    {
                    	focusFlag = 1;
                    	String theMessage = FeiUtils.isDescriptionOk(
                    			parentFrame, m_itemInfoP.descTextArea.getText(), itemTypeSelected, session);
                    	if (theMessage.length() > 0)
                    	{
                    		MessageBox.post(theMessage, "Error", MessageBox.ERROR);
                    		
                    		m_itemInfoP.requestFocus();
                    		m_itemInfoP.descTextArea.requestFocus();
                    		
                    		focusFlag = 2;
                    	}
                    	else
                    	{
                    		focusFlag = 0;
                    	}
					}
                    else if (focusFlag == 2)
                    {
                    	focusFlag = 0;
                    	m_itemInfoP.requestFocus();
                		m_itemInfoP.descTextArea.requestFocus();
                    }
				}
			});
	     }
	 }
}
