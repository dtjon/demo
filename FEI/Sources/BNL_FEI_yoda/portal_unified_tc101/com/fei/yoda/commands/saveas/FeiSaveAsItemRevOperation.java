/***********************************************************************************************************************
 (C) copyright by Siemens PLM Software

 Created by     :   J. Mansvelders 

 Description	:   Added refresh
 
 Created for    :   FEI

 Comments       :   

 Filename       :   FeiSaveAsItemRevOperation.java
 
 Using modules  :   

 Version info   :   The number consist of the TC release and then the release
 <release>.<pointrelease>

 
 History of changes                                            
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.1         JM          21-07-2009
 Reviewed                                                               0.2         JM          19-11-2009
 Updated for new successor/predecessor behavior                         0.3         JM          31-03-2010
 Updated for Teamcenter 8.3.*                                           0.4         JM          09-09-2011
  
 ************************************************************************************************************************/

package com.fei.yoda.commands.saveas;

import com.fei.yoda.util.FeiUtils;
import com.teamcenter.rac.commands.saveas.SaveAsItemRevCommand;
import com.teamcenter.rac.commands.saveas.SaveAsItemRevDialog;
import com.teamcenter.rac.commands.saveas.SaveAsItemRevOperation;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;

public class FeiSaveAsItemRevOperation extends SaveAsItemRevOperation {

	public FeiSaveAsItemRevOperation(SaveAsItemRevDialog arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public FeiSaveAsItemRevOperation(SaveAsItemRevCommand arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	public void pasteNewComponent() throws Exception
	{
		super.pasteNewComponent();
	}
	
	public void executeOperation() throws Exception
	{
		System.out.println("FeiSaveAsItemRevOperation: Before executeOperation.");

		super.executeOperation();
		
	    System.out.println("FeiSaveAsItemRevOperation: Refreshing forms.");
		FeiUtils.refreshMasterForms(newComp, getSession());

		
		// Csp generation
		try {
			TCUserService service = getSession().getUserService();
        	Object[] ObjtoSend = new Object[1];
	      	ObjtoSend[0] = new String(""); // dummy
	      	
	      	System.out.println("FeiSaveAsItemRevOperation: BNL_check_for_csp.");

	        service.call("BNL_check_for_csp", ObjtoSend);
		}
		catch (TCException e1) {
			e1.printStackTrace();
			MessageBox mb = new MessageBox(desktop, e1);
			mb.setModal(true);
			mb.setVisible(true);
		}
		
		// Create predecessor
		TCComponentItemRevision rev = ((TCComponentItem)newComp).getLatestItemRevision();
		try {
			TCUserService service = getSession().getUserService();
        	Object[] ObjtoSend = new Object[2];
	       	ObjtoSend[0] = rev;
	      	ObjtoSend[1] = new String(""); // dummy
	      	
	      	System.out.println("FeiSaveAsItemRevOperation: BNL_create_predecessor.");

	        service.call("BNL_create_predecessor", ObjtoSend);
		}
		catch (TCException e1) {
			e1.printStackTrace();
			MessageBox mb = new MessageBox(desktop, e1);
			mb.setModal(true);
			mb.setVisible(true);
		}
	}
}
