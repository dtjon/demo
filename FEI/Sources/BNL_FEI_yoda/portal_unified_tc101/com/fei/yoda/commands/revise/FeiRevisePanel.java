/***********************************************************************************************************************
 (C) copyright by Siemens PLM Software

 Created by     :   J. Mansvelders 

 Description	:   Custom Revise Panel
 
 Created for    :   FEI

 Comments       :   

 Filename       :   FeiRevisePanel.java
 
 Using modules  :   

 Version info   :   The number consist of the iman release and then the release
 <release>.<pointrelease>

 
 History of changes                                            
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.1         JM          07-04-2009
 Updated                                                                0.2         JM          20-04-2009
 Updated resetting of IRMF                                              0.3         JM          02-11-2009
 Reviewed                                                               0.4         JM          19-11-2009
 Updated for Teamcenter 8.3.*                                           0.5         JM          09-09-2011
 Updated beforeSetPanel				                                    0.6         JM          13-06-2013
 Updated for IR 7520879                                                 0.7         JWO         21-10-2015
 ************************************************************************************************************************/

package com.fei.yoda.commands.revise;

import java.awt.Frame;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import com.fei.yoda.util.FeiUtils;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.commands.newitem.ItemInfoPanel;
import com.teamcenter.rac.commands.newitem.ItemRevMasterFormPanel;
import com.teamcenter.rac.commands.revise.ReviseDialog;
import com.teamcenter.rac.commands.revise.RevisePanel;
import com.teamcenter.rac.form.AbstractTCForm;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.stylesheet.AbstractRendering;
import com.teamcenter.rac.util.MessageBox;

public class FeiRevisePanel extends RevisePanel {
	
	private ItemInfoPanel itemInfoP = null;
	private String itemTypeSelected = null;
	private int focusFlag = 0;
	private int masterRevFormResetted = 0;
	private boolean isClosingActivated;
	private InterfaceAIFComponent selectedComp = null;

	public FeiRevisePanel(Frame arg0, TCSession arg1, ReviseDialog arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
		itemTypeSelected = arg2.getItemType();
		InterfaceAIFComponent[] comps = arg2.pasteTargets;
		if (comps.length > 0)
		{
			selectedComp = comps[0];
		}
	}

	public FeiRevisePanel(Frame arg0, TCSession arg1, ReviseDialog arg2,
			Boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		isClosingActivated = false;
		// TODO Auto-generated constructor stub
		itemTypeSelected = arg2.getItemType();
		InterfaceAIFComponent[] comps = arg2.pasteTargets;
		if (comps.length > 0)
		{
			selectedComp = comps[0];
		}
	}
	
	public void finish()
	{
		super.finish();
		isClosingActivated = false;
		System.out.println("After finish");
		masterRevFormResetted = 0;
	}
	
	public void close() {
		super.close();
		
		if(isClosingActivated)
            return;
		isClosingActivated = true;
		
		System.out.println("FeiSaveAsItemRevPanel: After close.");

		try {
			TCUserService service = session.getUserService();
        	Object[] ObjtoSend = new Object[2];
	       	ObjtoSend[0] = selectedComp;
	      	ObjtoSend[1] = new String(""); // dummy
	      	
	      	System.out.println("FeiSaveAsItemRevPanel: BNL_fei_remove_from_list.");
	      	
	        service.call("BNL_fei_remove_from_list", ObjtoSend);
		}
		catch (TCException e1) {
			e1.printStackTrace();
			MessageBox mb = new MessageBox(parentFrame, e1);
			mb.setModal(true);
			mb.setVisible(true);
		}
		
	}
	
	public boolean beforeSetPanel(int i)
    {
		if (i == getAltIdInfoStep() || i == getAdditionalAltIdInfoStep() || i == getAdditionalAltRevInfoStep() || i == getAttachmentStep()) {
		//if (i == getAttachmentStep()) {
			
			System.out.println("FeiSaveAsItemRevPanel: getAttachmentStep.");
			
			// Set flag
			try {
				TCUserService service = session.getUserService();
	        	Object[] ObjtoSend = new Object[2];
		       	ObjtoSend[0] = new Boolean(true);
		      	ObjtoSend[1] = new String(""); // dummy
		      	
		      	System.out.println("FeiRevisePanel: BNL_set_in_portal_flag (true).");
		      	
		        service.call("BNL_set_in_portal_flag", ObjtoSend);
			}
			catch (TCException e1) {
				e1.printStackTrace();
				MessageBox mb = new MessageBox(parentFrame, e1);
				mb.setModal(true);
				mb.setVisible(true);
			}
			
			// Set flag
			TCComponentItemRevision rev = (TCComponentItemRevision)selectedComp;
			try {
				TCUserService service = session.getUserService();
	        	Object[] ObjtoSend = new Object[3];
		       	ObjtoSend[0] = rev;
		       	ObjtoSend[1] = new Boolean(true);
		      	ObjtoSend[2] = new String(""); // dummy
		      	
		      	System.out.println("FeiRevisePanel: BNL_set_in_portal_revise_flag (true).");
		      	
		        service.call("BNL_set_in_portal_revise_flag", ObjtoSend);
			}
			catch (TCException e1) {
				e1.printStackTrace();
				MessageBox mb = new MessageBox(parentFrame, e1);
				mb.setModal(true);
				mb.setVisible(true);
			}
		}
		

		boolean flag = super.beforeSetPanel(i);
		
		if (i == getAltIdInfoStep() || i == getAdditionalAltIdInfoStep() || i == getAdditionalAltRevInfoStep() || i == getAttachmentStep()) {
		//if (i == getAttachmentStep()) {
			try {
				TCUserService service = session.getUserService();
	        	Object[] ObjtoSend = new Object[2];
	        	ObjtoSend[0] = new Boolean(false);
		      	ObjtoSend[1] = new String(""); // dummy
		      	
		      	System.out.println("FeiRevisePanel: BNL_set_in_portal_flag (false).");
		      	
		        service.call("BNL_set_in_portal_flag", ObjtoSend);
			}
			catch (TCException e1) {
				e1.printStackTrace();
				MessageBox mb = new MessageBox(parentFrame, e1);
				mb.setModal(true);
				mb.setVisible(true);
			}
			
			// Set flag
			TCComponentItemRevision rev = (TCComponentItemRevision)selectedComp;
			try {
				TCUserService service = session.getUserService();
	        	Object[] ObjtoSend = new Object[3];
		       	ObjtoSend[0] = rev;
		       	ObjtoSend[1] = new Boolean(false);
		      	ObjtoSend[2] = new String(""); // dummy
		      	
		      	System.out.println("FeiRevisePanel: BNL_set_in_portal_revise_flag (true).");
		      	
		        service.call("BNL_set_in_portal_revise_flag", ObjtoSend);
			}
			catch (TCException e1) {
				e1.printStackTrace();
				MessageBox mb = new MessageBox(parentFrame, e1);
				mb.setModal(true);
				mb.setVisible(true);
			}
		}
		
		return flag;
    }

	
	public void afterSetPanel(int i) {
        super.afterSetPanel(i);
        
        if (i == getItemRevMasterFormStep() && masterRevFormResetted == 0) {
        	
        	System.out.println("FeiRevisePanel: Resetting IRMF.");
			
			ItemRevMasterFormPanel itemRevMasterFormP = (ItemRevMasterFormPanel)stepPanels.get(i);
			TCComponentForm theIRMF = (TCComponentForm)FeiUtils.getClassField(itemRevMasterFormP, "itemRevMasterForm");
            //TCComponentForm theIRMF = itemRevMasterFormP.itemRevMasterForm;
			
			AbstractTCForm theIRMFPanel = itemRevMasterFormP.formPanel;
			AbstractRendering theIRMFStylesheetPanel = (AbstractRendering)FeiUtils.getClassField(itemRevMasterFormP, "stylesheetPanel");
			//AbstractRendering theIRMFStylesheetPanel = itemRevMasterFormP.stylesheetPanel;
			
			try {
				TCComponentForm oldForm = (TCComponentForm)((TCComponent)selectedComp).getRelatedComponent("IMAN_master_form_rev");
				FeiUtils.resetFormValues(true, oldForm, theIRMF, theIRMFPanel, theIRMFStylesheetPanel, session);
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			masterRevFormResetted = 1;
		}
        else if (i == getItemInfoStep()) {
        	
			itemInfoP = (ItemInfoPanel)stepPanels.get(i);

        	itemInfoP.descTextArea.addFocusListener(new FocusListener() {
        		
        		ItemInfoPanel m_itemInfoP = itemInfoP;
        		
				public void focusGained(FocusEvent e)
				{
					// Dummy
				}

				public void focusLost(FocusEvent e)
				{
                    if (focusFlag == 0)
                    {
                    	System.out.println("FeiRevisePanel: Checking description.");
                    	focusFlag = 1;
                    	String theMessage = FeiUtils.isDescriptionOk(
                        		parentFrame, m_itemInfoP.descTextArea.getText(), itemTypeSelected, session);
                    	
                    	if (theMessage.length() > 0)
                    	{
                    		MessageBox.post(theMessage, "Error", MessageBox.ERROR);
                    		
                    		m_itemInfoP.requestFocus();
                    		m_itemInfoP.descTextArea.requestFocus();
                    		
                    		focusFlag = 2;
                    	}
                    	else
                    	{
                    		focusFlag = 0;
                    	}
                    }
                    else if (focusFlag == 2)
                    {
                    	focusFlag = 0;
                    	m_itemInfoP.requestFocus();
                		m_itemInfoP.descTextArea.requestFocus();
                    }
				}
			});

			itemInfoP.descTextArea.requestFocus();
	     }
	 }
}
