/***********************************************************************************************************************
 (C) copyright by Siemens PLM Software

 Created by     :   J. Mansvelders 

 Description	:   Added refresh
 
 Created for    :   FEI

 Comments       :   

 Filename       :   FeiReviseOperation.java
 
 Using modules  :   

 Version info   :   The number consist of the iman release and then the release
 <release>.<pointrelease>

 
 History of changes                                            
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.1         JM          21-07-2009
 Reviewed                                                               0.2         JM          19-11-2009
 Updated for Teamcenter 8.3.*                                           0.3         JM          09-09-2011
  
 ************************************************************************************************************************/

package com.fei.yoda.commands.revise;

import com.fei.yoda.util.FeiUtils;
import com.teamcenter.rac.commands.revise.ReviseCommand;
import com.teamcenter.rac.commands.revise.ReviseDialog;
import com.teamcenter.rac.commands.revise.ReviseOperation;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;

public class FeiReviseOperation extends ReviseOperation {

	public FeiReviseOperation(ReviseDialog arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public FeiReviseOperation(ReviseCommand arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	public void executeOperation() throws Exception
	{
		super.executeOperation();
		System.out.println("FeiReviseOperation: Refreshing forms.");
		FeiUtils.refreshMasterForms(newComp, getSession());

		// Csp generation
		try {
			TCUserService service = getSession().getUserService();
        	Object[] ObjtoSend = new Object[1];
	      	ObjtoSend[0] = new String(""); // dummy
	      	
	      	System.out.println("FeiSaveAsItemRevOperation: BNL_check_for_csp.");

	        service.call("BNL_check_for_csp", ObjtoSend);
		}
		catch (TCException e1) {
			e1.printStackTrace();
			MessageBox mb = new MessageBox(desktop, e1);
			mb.setModal(true);
			mb.setVisible(true);
		}
	}

}
