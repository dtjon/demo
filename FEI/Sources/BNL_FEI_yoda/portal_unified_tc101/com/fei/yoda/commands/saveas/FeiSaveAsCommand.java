/***********************************************************************************************************************
 (C) copyright by Siemens PLM Software

 Created by     :   J. Mansvelders 

 Description	:   Custom Save As Command
 
 Created for    :   FEI

 Comments       :   

 Filename       :   FeiSaveAsCommand.java
 
 Using modules  :   

 Version info   :   The number consist of the iman release and then the release
 <release>.<pointrelease>

 
 History of changes                                            
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.1         JM          07-04-2009
 Updated for Teamcenter 8.3.*                                           0.2         JM          09-09-2011

 ************************************************************************************************************************/

package com.fei.yoda.commands.saveas;

import java.awt.Frame;

import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.commands.saveas.SaveAsCommand;
import com.teamcenter.rac.commands.saveas.SaveItemAsDialog;

public class FeiSaveAsCommand extends SaveAsCommand {

	public FeiSaveAsCommand() {
		// TODO Auto-generated constructor stub
	}

	public FeiSaveAsCommand(Frame frame, InterfaceAIFComponent[] ainterfaceaifcomponent,
			Boolean boolean1) {
		super(frame, ainterfaceaifcomponent, boolean1);
		// TODO Auto-generated constructor stub

		// Replace SaveItemAsDialog by FeiSaveItemAsDialog
		if (this.getRunnable() instanceof SaveItemAsDialog) {
			setRunnable(new FeiSaveItemAsDialog(frame, ((InterfaceAIFComponent) (ainterfaceaifcomponent[0]))));
		}
	}
}
