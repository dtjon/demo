/***********************************************************************************************************************
 (C) copyright by Siemens PLM Software

 Created by     :   J. Mansvelders 

 Description	:   Custom Save ItemRev As Panel
 
 Created for    :   FEI

 Comments       :   

 Filename       :   FeiSaveAsItemRevPanel.java
 
 Using modules  :   

 Version info   :   The number consist of the TC release and then the release
 <release>.<pointrelease>

 
 History of changes                                            
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.1         JM          07-04-2009
 Updated                                                                0.2         JM          10-04-2009
 Added check to the description                                         0.3         JM          20-04-2009
 Added resetting of IMF and IRMF                                        0.4         JM          21-07-2009
 Updated resetting of IMF and IRMF                                      0.5         JM          02-11-2009
 Reviewed						                                        0.6         JM          19-11-2009
 Updated for Teamcenter 8.3.*                                           0.7         JM          09-09-2011
 Updated beforeSetPanel				                                    0.6         JM          13-06-2013

 ************************************************************************************************************************/

package com.fei.yoda.commands.saveas;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import com.fei.yoda.util.FeiUtils;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.commands.newitem.ItemInfoPanel;
import com.teamcenter.rac.commands.newitem.ItemMasterFormPanel;
import com.teamcenter.rac.commands.newitem.ItemRevMasterFormPanel;
import com.teamcenter.rac.commands.saveas.SaveAsItemRevDialog;
import com.teamcenter.rac.commands.saveas.SaveAsItemRevPanel;
import com.teamcenter.rac.form.AbstractTCForm;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.stylesheet.AbstractRendering;
import com.teamcenter.rac.util.MessageBox;

public class FeiSaveAsItemRevPanel extends SaveAsItemRevPanel {
	
	private ItemInfoPanel itemInfoP = null;
	private int assigned = 0;
	private String itemTypeSelected = null;
	private int focusFlag = 0;
	private InterfaceAIFComponent selectedComp = null;
	private int masterFormResetted = 0;
	private int masterRevFormResetted = 0;
	private boolean isItemInfoPanelInitialized;
	private boolean isClosingActivated;
	
	public FeiSaveAsItemRevPanel(Frame arg0, TCSession arg1,
			SaveAsItemRevDialog arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
		isItemInfoPanelInitialized = false;
		isClosingActivated = false;
		itemTypeSelected = arg2.getItemType();
		InterfaceAIFComponent[] comps = arg2.pasteTargets;
		if (comps.length > 0)
		{
			selectedComp = comps[0];
		}
	}

	public FeiSaveAsItemRevPanel(Frame arg0, TCSession arg1,
			SaveAsItemRevDialog arg2, Boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
		isItemInfoPanelInitialized = false;
		isClosingActivated = false;
		itemTypeSelected = arg2.getItemType();
		InterfaceAIFComponent[] comps = arg2.pasteTargets;
		if (comps.length > 0)
		{
			selectedComp = comps[0];
		}
	}
	
	public void finish()
	{
		super.finish();
		System.out.println("After finish");
		
		masterFormResetted = 0;
		masterRevFormResetted = 0;
	}
	
	public void close() {
		super.close();
		
		if(isClosingActivated)
            return;
		isClosingActivated = true;
		
		System.out.println("FeiSaveAsItemRevPanel: After close.");
		
		try {
			TCUserService service = session.getUserService();
        	Object[] ObjtoSend = new Object[2];
	       	ObjtoSend[0] = selectedComp;
	      	ObjtoSend[1] = new String(""); // dummy
	      	
	      	System.out.println("FeiSaveAsItemRevPanel: BNL_fei_remove_from_list.");
	      	
	        service.call("BNL_fei_remove_from_list", ObjtoSend);
		}
		catch (TCException e1) {
			e1.printStackTrace();
			MessageBox mb = new MessageBox(parentFrame, e1);
			mb.setModal(true);
			mb.setVisible(true);
		}
		
	}
	
	public boolean beforeSetPanel(int i)
    {
		if (i == getAltIdInfoStep() || i == getAdditionalAltIdInfoStep() || i == getAdditionalAltRevInfoStep() || i == getAttachmentStep()) {
		//if (i == getAttachmentStep()) {
			
			System.out.println("FeiSaveAsItemRevPanel: getAttachmentStep.");
			
			// Set flag
			try {
				TCUserService service = session.getUserService();
	        	Object[] ObjtoSend = new Object[2];
		       	ObjtoSend[0] = new Boolean(true);
		      	ObjtoSend[1] = new String(""); // dummy
		      	
		      	System.out.println("FeiSaveAsItemRevPanel: BNL_set_in_portal_flag (true).");
		      	
		        service.call("BNL_set_in_portal_flag", ObjtoSend);
			}
			catch (TCException e1) {
				e1.printStackTrace();
				MessageBox mb = new MessageBox(parentFrame, e1);
				mb.setModal(true);
				mb.setVisible(true);
			}		
		}

		boolean flag = super.beforeSetPanel(i);
		
		if (i == getAltIdInfoStep() || i == getAdditionalAltIdInfoStep() || i == getAdditionalAltRevInfoStep() || i == getAttachmentStep()) {
		//if (i == getAttachmentStep()) {
			try {
				TCUserService service = session.getUserService();
	        	Object[] ObjtoSend = new Object[2];
	        	ObjtoSend[0] = new Boolean(false);
		      	ObjtoSend[1] = new String(""); // dummy
		      	
		      	System.out.println("FeiSaveAsItemRevPanel: BNL_set_in_portal_flag (false).");
		      	
		        service.call("BNL_set_in_portal_flag", ObjtoSend);
			}
			catch (TCException e1) {
				e1.printStackTrace();
				MessageBox mb = new MessageBox(parentFrame, e1);
				mb.setModal(true);
				mb.setVisible(true);
			}
			
		}
		
		return flag;
    }
	
	public void afterSetPanel(int i) {

		super.afterSetPanel(i);
						
		if (i == getItemMasterFormStep() && masterFormResetted == 0) {
			
			System.out.println("FeiSaveAsItemRevPanel: Resetting IMF.");
			
			ItemMasterFormPanel itemMasterFormP = (ItemMasterFormPanel)stepPanels.get(i);
			
			TCComponentForm theIMF = itemMasterFormP.itemMasterForm;
			
			AbstractTCForm theIMFPanel = itemMasterFormP.formPanel;
			AbstractRendering theIMFStylesheetPanel = itemMasterFormP.stylesheetPanel;
			
			TCComponentItem theOldItem;
			try {
				theOldItem = ((TCComponentItemRevision)selectedComp).getItem();
				TCComponentForm oldForm = (TCComponentForm)theOldItem.getRelatedComponent("IMAN_master_form");
				
				FeiUtils.resetFormValues(false, oldForm, theIMF, theIMFPanel, theIMFStylesheetPanel, session);
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
			masterFormResetted = 1;
		}
		
		if (i == getItemRevMasterFormStep() && masterRevFormResetted == 0) {
			
			System.out.println("FeiSaveAsItemRevPanel: Resetting IRMF.");
			
			ItemRevMasterFormPanel itemRevMasterFormP = (ItemRevMasterFormPanel)stepPanels.get(i);

			TCComponentForm theIRMF = (TCComponentForm)FeiUtils.getClassField(itemRevMasterFormP, "itemRevMasterForm");
			//TCComponentForm theIRMF = itemRevMasterFormP.itemRevMasterForm;
			
			AbstractTCForm theIRMFPanel = itemRevMasterFormP.formPanel;
			AbstractRendering theIRMFStylesheetPanel = (AbstractRendering)FeiUtils.getClassField(itemRevMasterFormP, "stylesheetPanel");
			//AbstractRendering theIRMFStylesheetPanel = itemRevMasterFormP.stylesheetPanel;
			
			TCComponentForm oldForm;
			try {
				oldForm = (TCComponentForm)((TCComponent)selectedComp).getRelatedComponent("IMAN_master_form_rev");
				FeiUtils.resetFormValues(false, oldForm, theIRMF, theIRMFPanel, theIRMFStylesheetPanel, session);
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
									
			masterRevFormResetted = 1;
		}

		if (i == getItemInfoStep()) {
			itemInfoP = (ItemInfoPanel)stepPanels.get(i);
			
			if (!isItemInfoPanelInitialized)
			{
				isItemInfoPanelInitialized = true;
				System.out.println("FeiSaveAsItemRevPanel: Adding predecessor checkbox.");
				
				JCheckBox predecessorCheckBox = new JCheckBox("Create Predecessor");
				itemInfoP.otherPropertyPanel.add("3.1.right.top", new JLabel(""));
				itemInfoP.otherPropertyPanel.add("3.2.right.top", predecessorCheckBox);
				
				predecessorCheckBox.addActionListener(new ActionListener() {
	
					public void actionPerformed(ActionEvent e) {
						
						try {
							TCUserService service = session.getUserService();
				        	Object[] ObjtoSend = new Object[2];
					       	ObjtoSend[0] = selectedComp;
					      	ObjtoSend[1] = new String(""); // dummy
					      	
							// TODO Auto-generated method stub
							if (((JCheckBox)e.getSource()).isSelected())
							{
								System.out.println("FeiSaveAsItemRevPanel: BNL_fei_add_to_list.");
						        service.call("BNL_fei_add_to_list", ObjtoSend);
							}
							else
							{
								System.out.println("FeiSaveAsItemRevPanel: BNL_fei_remove_from_list.");
						        service.call("BNL_fei_remove_from_list", ObjtoSend);
							}
						}
						catch (TCException e1) {
							e1.printStackTrace();
							MessageBox mb = new MessageBox(parentFrame, e1);
							mb.setModal(true);
							mb.setVisible(true);
						}
					}
					
				});
				
				itemInfoP.assignButton.addActionListener(new ActionListener() {
					
					ItemInfoPanel m_itemInfoP = itemInfoP;
	
					public void actionPerformed(ActionEvent e) {
						m_itemInfoP.idTextField.requestFocus();
						assigned = 1;
					}
				});
	
				
				itemInfoP.idTextField.addFocusListener(new FocusListener() {
					
					ItemInfoPanel m_itemInfoP = itemInfoP;
					
					public void focusGained(FocusEvent e) {
						// dummy
					}
	
					public void focusLost(FocusEvent e) {
						if (m_itemInfoP.nameTextField.getText().compareTo(m_itemInfoP.idTextField.getText()) != 0) {
							System.out.println("FeiSaveAsItemRevPanel: Setting name field (1).");
							m_itemInfoP.nameTextField.setText(m_itemInfoP.idTextField.getText());
						}
					}
				});
				
				itemInfoP.nameTextField.addFocusListener(new FocusListener() {
					
					ItemInfoPanel m_itemInfoP = itemInfoP;
					
					public void focusGained(FocusEvent e) {
						if (assigned == 1) {
							System.out.println("FeiSaveAsItemRevPanel: Setting name field (2).");
							m_itemInfoP.nameTextField.setText(m_itemInfoP.idTextField.getText());
							assigned = 0;
						}
					}
	
					public void focusLost(FocusEvent e) {
						// dummy
					}
				});
				
	        	itemInfoP.descTextArea.addFocusListener(new FocusListener() {
	        		
	        		ItemInfoPanel m_itemInfoP = itemInfoP;
	        		
					public void focusGained(FocusEvent e) {
						// Dummy
					}
	
					public void focusLost(FocusEvent e) {
						
	                    if (focusFlag == 0)
	                    {
	                    	focusFlag = 1;
	                    	String theMessage = FeiUtils.isDescriptionOk(
	                        		parentFrame, m_itemInfoP.descTextArea.getText(), itemTypeSelected, session);
	                    	
	                    	if (theMessage.length() > 0)
	                    	{
	                    		MessageBox.post(theMessage, "Error", MessageBox.ERROR);
	                    		
	                    		m_itemInfoP.requestFocus();
	                    		m_itemInfoP.descTextArea.requestFocus();
	                    		
	                    		focusFlag = 2;
	                    	}
	                    	else
	                    	{
	                    		focusFlag = 0;
	                    	}
	                    }
	                    else if (focusFlag == 2)
	                    {
	                    	focusFlag = 0;
	                    	m_itemInfoP.requestFocus();
	                		m_itemInfoP.descTextArea.requestFocus();
	                    }
					}
				});
			}
        	
        	itemInfoP.nameTextField.setText(itemInfoP.idTextField.getText());
			
			itemInfoP.descTextArea.requestFocus();
	     }
	 }
}