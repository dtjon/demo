/***********************************************************************************************************************
 (C) copyright by Siemens PLM Software

 Created by     :   J. Mansvelders 

 Description	:   Added refresh
 
 Created for    :   FEI

 Comments       :   

 Filename       :   FeiNewItemOperation.java
 
 Using modules  :   

 Version info   :   The number consist of the iman release and then the release
 <release>.<pointrelease>

 
 History of changes                                            
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.1         JM          21-07-2009
 Reviewed                                                               0.2         JM          19-11-2009
 Updated for Teamcenter 8.3.*                                           0.3         JM          09-09-2011 
  
 ************************************************************************************************************************/

package com.fei.yoda.commands.newitem;

import com.teamcenter.rac.commands.newitem.NewItemCommand;
import com.teamcenter.rac.commands.newitem.NewItemDialog;
import com.teamcenter.rac.commands.newitem.NewItemOperation;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.fei.yoda.util.FeiUtils;

public class FeiNewItemOperation extends NewItemOperation {

	public FeiNewItemOperation() {
		// TODO Auto-generated constructor stub
	}

	public FeiNewItemOperation(NewItemDialog arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public FeiNewItemOperation(NewItemCommand arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	public void executeOperation() throws Exception
	{
		super.executeOperation();
		
		if(newComp instanceof TCComponentItem)
        {
			// Workaround for IR 1894958
			TCComponentItem item = (TCComponentItem)newComp;
			
			String value = item.getProperty("uom_tag");
			
			if (value != null && uomComp != null)
			{
				if (uomComp.toString().equalsIgnoreCase(value) || (uomComp.toString().length() > 0 && value.equalsIgnoreCase("each")))
				{
					System.out.println(" Update UOM with value: " + uomComp);
					item.setProperty("uom_tag", uomComp.toString());
				}
			}
        }
		
		System.out.println("FeiSaveAsItemRevOperation: Refreshing forms.");
		FeiUtils.refreshMasterForms(newComp, getSession());
	}

}
