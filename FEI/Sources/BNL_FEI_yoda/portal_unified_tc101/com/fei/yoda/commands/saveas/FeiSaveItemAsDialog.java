/***********************************************************************************************************************
 (C) copyright by Siemens PLM Software

 Created by     :   J. Mansvelders 

 Description	:   Custom Save Item As Dialog
 
 Created for    :   FEI

 Comments       :   

 Filename       :   FeiSaveItemAsDialog.java
 
 Using modules  :   

 Version info   :   The number consist of the iman release and then the release
 <release>.<pointrelease>

 
 History of changes                                            
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.1         JM          07-04-2009
 Updated                                                                0.2         JM          10-04-2009
 Updated                                                                0.3         JM          20-04-2009
 Reviewed						                                        0.4         JM          19-11-2009
 Updated for Teamcenter 8.3.*                                           0.5         JM          09-09-2011

 ************************************************************************************************************************/

package com.fei.yoda.commands.saveas;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import com.fei.yoda.util.FeiUtils;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.commands.newitem.ItemInfoPanel;
import com.teamcenter.rac.commands.saveas.SaveItemAsDialog;
import com.teamcenter.rac.util.MessageBox;

public class FeiSaveItemAsDialog extends SaveItemAsDialog {
	
	private int assigned = 0;
	private String itemTypeSelected = null;
	private int focusFlag = 0;
	
	public FeiSaveItemAsDialog(Frame arg0, InterfaceAIFComponent arg1) {
		super(arg0, arg1);

		itemTypeSelected = currentComponent.getType();
		System.out.println("FeiSaveItemAsDialog");
		
		nameField.setText(id.getText());
		
		this.assignButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				id.requestFocus();
				assigned = 1;
			}
		});

		id.addFocusListener(new FocusListener() {
				public void focusGained(FocusEvent e) {
					// dummy
				}

				public void focusLost(FocusEvent e) {
					if (nameField.getText().compareTo(id.getText()) != 0) {
						nameField.setText(id.getText());
					}
				}
		});
		
		nameField.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				if (assigned == 1)
				{
					System.out.println("FeiSaveItemAsDialog: Updating name field.");
					nameField.setText(id.getText());
					assigned = 0;

					validateEntries();
				}
			}

			public void focusLost(FocusEvent e) {
				// dummy
			}
		});
		
    	descArea.addFocusListener(new FocusListener() {
    		
			public void focusGained(FocusEvent e) {
				// Dummy
			}

			public void focusLost(FocusEvent e) {
				
                if (focusFlag == 0)
                {
                	System.out.println("FeiSaveItemAsDialog: Checking description.");
                	focusFlag = 1;
                	String theMessage = FeiUtils.isDescriptionOk(
                    		parent, descArea.getText(), itemTypeSelected, session);
                	
                	if (theMessage.length() > 0)
                	{
                		MessageBox.post(theMessage, "Error", MessageBox.ERROR);
                		
                		descArea.requestFocus();
                		
                		focusFlag = 2;
                	}
                	else
                	{
                		focusFlag = 0;
                	}
                }
                else if (focusFlag == 2)
                {
                	focusFlag = 0;
                	descArea.requestFocus();
                }
			}
		});
		
    	descArea.requestFocus();
	}
}
