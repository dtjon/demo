/***********************************************************************************************************************
 (C) copyright by Siemens PLM Software

 Created by     :   J. Mansvelders 

 Description	:   Utils class
 
 Created for    :   FEI

 Comments       :   

 Filename       :   FeiUtils.java
 
 Using modules  :   

 Version info   :   The number consist of the iman release and then the release
 <release>.<pointrelease>

 
 History of changes                                            
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.1         JM          20-04-2009
 Added refreshMasterForms                                               0.2         JM          21-07-2009
 Added removal from list                                                0.3         JM          23-07-2009
 Added check for illegal chars                                          0.4         JM          29-10-2009
 Reviewed				                                                0.5         JM          19-11-2009
 Added check for bypass                                                 0.6         JM          09-12-2009
 Enhanced bypass check                                                  0.7         JM          11-03-2010
 Updated for Teamcenter 8.3.*                                           0.8         JM          09-09-2011
 Updated for Teamcenter 10.1.*                                          0.9         JWO         28-06-2016
 ************************************************************************************************************************/

package com.fei.yoda.util;

import java.awt.Frame;
import java.lang.reflect.Field;
import java.util.List;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.form.AbstractTCForm;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.stylesheet.AbstractRendering;

public class FeiUtils {
	
	public static String isDescriptionOk(Frame theFrame, String theDesc, String itemTypeSelected, TCSession theSession)
	{
		if (itemTypeSelected.compareTo("Item") == 0 ||
				itemTypeSelected.compareTo("Document") == 0)
		{
			itemTypeSelected = itemTypeSelected + "Revision";
		}
		else
		{
			itemTypeSelected = itemTypeSelected + " Revision";
		}

		try
		{
			// Check for bypass
			if (theSession.isUserSystemAdmin() && theSession.hasBypass())
			{
				System.out.println("FeiUtils: Bypass has been enabled, skipping check [isDescriptionOk].");
				return ""; 
			}

			TCPreferenceService prefService = theSession.getPreferenceService();
			String[] validateTypes = prefService.getStringArray(TCPreferenceService.TC_preference_site, "FEI_VALIDATE_TYPES");
			
			System.out.println("FeiUtils: Searching preference [FEI_VALIDATE_TYPES] for type: " + itemTypeSelected);
			
			// Check if configured
			for (int i = 0; i < validateTypes.length; i++)
			{
				System.out.println("FeiUtils: Configured type: " + validateTypes[i]);
				
				if (validateTypes[i].compareTo(itemTypeSelected) == 0)
				{
					String prefLookup;
					String[] validateFields;
					int ii;
					
					System.out.println("FeiUtils: Type matches");
					
					prefLookup = "FEI_ATTR_LEN_" + itemTypeSelected;
					
					System.out.println("FeiUtils: ** Searching for preference: " + prefLookup);
					
					validateFields = prefService.getStringArray(TCPreferenceService.TC_preference_site, prefLookup);
					for (ii = 0; ii < validateFields.length; ii++)
					{
						System.out.println("FeiUtils: Value: " + validateFields[ii]);

						// Look for object_desc
						String[] splittedFields = validateFields[ii].split(":");

						if (splittedFields.length > 0 && splittedFields[0].compareTo("object_desc") == 0)
						{
							int descLength = Integer.parseInt(splittedFields[1]);
							System.out.println("FeiUtils: Allowed length for object_desc: " + descLength);
							
							if (theDesc.length() > descLength)
							{
								
								String theMessage = "The length [" + 
									theDesc.length() + "] is not allowed for attribute [" + 
									splittedFields[0] + "]. The allowed length is [" + descLength + "].";
								
								return theMessage;
						    }
							
							break;
						}
					}// End of for (int ii = 0; ii < validateFields.length; ii++)
					
					prefLookup = "FEI_ATTR_CHARS_" + itemTypeSelected;
					
					System.out.println("FeiUtils: Searching for preference: " + prefLookup);
					
					validateFields = prefService.getStringArray(TCPreferenceService.TC_preference_site, prefLookup);
					
					for (ii = 0; ii < validateFields.length; ii++)
					{
						System.out.println("FeiUtils: Value: " + validateFields[ii]);

						// Look for object_desc
						String[] splittedFields = validateFields[ii].split(":");

						if (splittedFields.length > 0 && splittedFields[0].compareTo("object_desc") == 0)
						{
							System.out.println("FeiUtils: Checking object_desc for illegal chars: " + splittedFields[1]);
								
							boolean checkFail = false; 
							
							String[] checkChars = splittedFields[1].split("\\|");
							
							for (int c = 0; c < checkChars.length; c++)
							{
								System.out.println("FeiUtils: Checking for: " + checkChars[c]);
								
								if (checkChars[c].equalsIgnoreCase("\\B"))
								{
									System.out.println("FeiUtils: debug1");
									if (theDesc.length() > 0 && theDesc.charAt(0) == ' ')
									{
										System.out.println("FeiUtils: debug2");
										checkFail = true;
									}
								}
								else if (
										checkChars[c].equalsIgnoreCase("\\b")
										|| checkChars[c].equalsIgnoreCase("\\n")
										|| checkChars[c].equalsIgnoreCase("\\t")
										|| checkChars[c].equalsIgnoreCase("&quot;")
										)
								{
									System.out.println("FeiUtils: debug3");
									//if (theDesc.contentEquals("\\b\\n\\t\""))
									if (theDesc.indexOf('\b') != -1
											|| theDesc.indexOf('\n') != -1
											|| theDesc.indexOf('\t') != -1
											|| theDesc.indexOf('\"') != -1)
									{
										System.out.println("FeiUtils: debug4");
										checkFail = true;
									}
								}
								else if (theDesc.indexOf(checkChars[c]) != -1)
								{
									System.out.println("FeiUtils: debug5");
									checkFail = true;
								}
							}

							if (checkFail)
							{
								System.out.println("FeiUtils: debug6");
								String theMessage = "Illegal characters [" + 
								    splittedFields[1] + "] are used in the value of attribute [" + 
									splittedFields[0] + "]. This is not allowed.";
								
								return theMessage;
							}
							
							break;
						}
					}// End of for (int ii = 0; ii < validateFields.length; ii++)
				}// End of if (validateTypes[i].compareTo(itemTypeSelected) == 0)
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
		return "";
	}
	
	public static void refreshMasterForms(TCComponent newComponent, TCSession theSession) throws Exception
	{
		TCComponentItem item;
		TCComponentItemRevision rev;
		TCComponent comp;
		
		TCUserService service = theSession.getUserService();
       	Object[] ObjtoSend = new Object[2];
		
		if(newComponent instanceof TCComponentItem)
        {
			item = (TCComponentItem)newComponent;
			rev = item.getLatestItemRevision();
        } else
        {
            rev = (TCComponentItemRevision)newComponent;
            item = rev.getItem();
        }
		AIFComponentContext imf[] = item.getRelated("IMAN_master_form");
		AIFComponentContext irmf[] = rev.getRelated("IMAN_master_form_rev");
		
		for (int i = 0; i < imf.length; i++)
		{
			comp = ((TCComponent)imf[i].getComponent());
			comp.refresh();
			
			System.out.println("FeiUtils: BNL_remove_form_from_list (IMF).");

	        ObjtoSend[0] = comp; // Form
		    ObjtoSend[1] = new String(""); // dummy
		        service.call("BNL_remove_form_from_list", ObjtoSend);
		}
			
		for (int i = 0; i < irmf.length; i++)
		{
			comp = ((TCComponent)irmf[i].getComponent());
			comp.refresh();
			
			System.out.println("FeiUtils: BNL_remove_form_from_list (IRMF).");
			
			ObjtoSend[0] = comp; // Form
		    ObjtoSend[1] = new String(""); // dummy
		    service.call("BNL_remove_form_from_list", ObjtoSend);
		}
	}
	
	public static void resetFormValues(boolean isRevise, TCComponentForm oldForm, TCComponentForm theForm, AbstractTCForm theFormPanel, AbstractRendering theStylesheetPanel, TCSession theSession)
	{
		// Check for bypass
		try {
			if (theSession.isUserSystemAdmin() && theSession.hasBypass())
			{
				System.out.println("FeiUtils: Bypass has been enabled, skipping action [resetFormValues].");
				return; 
			}
		} catch (TCException e1) {
			e1.printStackTrace();
		}
		
		
		String formType = theForm.getType();
		
     	System.out.println("FeiUtils: Type form: " + formType);
		
    	TCPreferenceService prefService = null;
		String[] types = null;
		prefService = theSession.getPreferenceService();
		types = prefService.getStringArray(TCPreferenceService.TC_preference_site, "FEI_VALIDATE_TYPES");
		if (types.length == 0)
		{
			System.out.println("FeiUtils: Warning, preference [FEI_VALIDATE_TYPES] can not be found.");
		}
		
		for (int ii = 0; ii < types.length; ii++)
		{
			if (types[ii].equalsIgnoreCase(formType))
			{
				String prefName = "FEI_RESET_" + formType;
	 			System.out.println("FeiUtils: Looking for preference " + prefName);
	 			String[] attributes = null;
				attributes = prefService.getStringArray(TCPreferenceService.TC_preference_site, prefName);
				if (attributes.length == 0)
				{
					System.out.println("FeiUtils: Warning, preference [" + prefName + "] can not be found.");
				}
	 			
	 			if (attributes.length > 0)
	 			{
	 				String as1Temp[] = new String[attributes.length];
	 				Object aobjTemp[] = new Object[attributes.length];
	 				int aa = 0;
	 			
		 			for (int a = 0; a < attributes.length; a++)
		 			{
		 				String[] splittedFields = attributes[a].split("\\.");
		 				
		 				System.out.println("FeiUtils: length " + splittedFields.length);
		 				
		 				if (splittedFields.length > 1)
		 				{
		 					if ((isRevise && splittedFields.length > 2 && splittedFields[2].equalsIgnoreCase("%copy%"))
		 							|| splittedFields[1].equalsIgnoreCase("%copy%"))
		 					{
		 						if (isRevise)
		 							System.out.println("FeiUtils: Revise and %copy%, attribute [" + splittedFields[0] + "] will be checked.");
		 						else
		 							System.out.println("FeiUtils: %copy%, attribute [" + splittedFields[0] + "] will be checked.");
		 						
		 						if (!checkPropValue(oldForm, splittedFields[0]))
		 						{
		 							System.out.println("FeiUtils: Current value is not allowed, attribute [" + splittedFields[0] + "] will be cleared.");
		 							as1Temp[aa] = splittedFields[0];
		 							aobjTemp[aa] = getPropValueObject(theForm, splittedFields[0], "");
		 							aa++;
		 						}
		 						else
		 						{
		 							System.out.println("FeiUtils: Current value is allowed, attribute [" + splittedFields[0] + "] will be untouched.");
		 						}
		 					}
		 					else
		 					{
		 						System.out.println("FeiUtils: Attribute [" + splittedFields[0] + "] will get value [" + splittedFields[1] + "]");
		 						
		 						as1Temp[aa] = splittedFields[0];
		 						aobjTemp[aa] = getPropValueObject(theForm, splittedFields[0], splittedFields[1]);
		 						aa++;
		 					}
		 				}
		 				else
		 				{
		 					System.out.println("FeiUtils: Attribute [" + attributes[a] + "] will be cleared.");

		 					as1Temp[aa] = attributes[a];
		 					aobjTemp[aa] = getPropValueObject(theForm, attributes[a], "");
		 					aa++;
		 				}
		 			} // End of for (int a = 0; a < attributes.length; a++)
		 			
		 			String as1[] = new String[aa];
	 				Object aobj[] = new Object[aa];
	 				for (int i = 0; i < aa; i++)
	 				{
	 					as1[i] = as1Temp[i];
	 					aobj[i] = aobjTemp[i];
	 				}

		 			// Update the values
					if (theFormPanel != null)
					{
						System.out.println("FeiUtils: theFormPanel");
						theFormPanel.setValues(as1, aobj);

					}
					else if (theStylesheetPanel != null)
					{
						System.out.println("FeiUtils: theStylesheetPanel");
						theStylesheetPanel.setValues(as1, aobj);
					}
					
					System.out.println("FeiUtils: Attributes have been updated.");
					
					// Attributes have been updated, now add to the list
					try {
						TCUserService service = theSession.getUserService();
						
				       	Object[] ObjtoSend = new Object[2];
				       	
				       	System.out.println("FeiUtils: BNL_add_form_to_list (form).");
			        	
				       	ObjtoSend[0] = theForm; // Form
				      	ObjtoSend[1] = new String(""); // dummy
				        service.call("BNL_add_form_to_list", ObjtoSend);
					} catch (TCException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	 			}
	 			break;
			}
		}
	}
	
	public static Object getPropValueObject(TCComponentForm theForm, String propName, String propValue)
	{
		try {
			TCProperty prop = theForm.getTCProperty(propName);
			if (prop.isNotArray())
			{
				return propValue;
			}
			else
			{
				Object[] array = { propValue };
				return array;
			}
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean reportValues(TCComponentListOfValues theLov, String propValue) throws TCException
	{
		ListOfValuesInfo lovInfo = theLov.getListOfValues();
		Object[] theValue = lovInfo.getListOfValues();
			
		for (int i = 0; i < theValue.length; i++)
		{
			// Check for filter
			TCComponentListOfValues filterLov = lovInfo.getListOfFilterAtIndex(i);
			
			if (filterLov != null)
			{
				if (reportValues(filterLov, propValue) == true)
				{
					return true;
				}
			}
			else
			{
				System.out.println("** Value: " + theValue[i]);
				
				if (theValue[i].equals(propValue))
				{	
					System.out.println("FeiUtils: Match, value [" + theValue[i] + "] is allowed.");
					return true;
				}
			}
		}

		return false;
	}
	
	public static boolean checkPropValue(TCComponentForm theForm, String propName)
	{
		try {
			boolean bAllowed = true;

			TCProperty prop = theForm.getTCProperty(propName);
			String propValue = prop.getUIFValue();
			
			List<String> propValues = prop.getDisplayableValues();

			System.out.println("Value to be checked: " + propValue);
			
			if (propValue.length() > 0)
			{
				TCComponentListOfValues theLov = prop.getLOV();

				if (theLov != null)
				{
					System.out.println("Lov has been attached, checking...");
					
					for (int i = 0; i < propValues.size(); i++)
					{
						String checkVal = "";
						if (i > 0 && propValues.get(i).startsWith(" "))
						{
							checkVal = propValues.get(i).substring(1);
						}
						else
						{
							checkVal = propValues.get(i);
						}
						if (reportValues(theLov, checkVal) == false)
						{
							System.out.println("FeiUtils: value [" + checkVal + "] is not allowed.");
							bAllowed = false;

							break;
						}
					}
					if (bAllowed)
					{
						System.out.println("FeiUtils: value [" + propValue + "] is allowed.");
						return true;
					}
					
					/*if (reportValues(theLov, propValue) == true)
					{
						System.out.println("FeiUtils: value [" + propValue + "] is allowed.");
						return true;
					}*/
				}
				else
				{
					System.out.println("No Lov has been attached, nothing to check.");
					return true;
				}
			}
			else
			{
				System.out.println("Empty value, nothing to check.");
				return true;
			}
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	 /**
	   * Reflection access to protected fields
	   * @param masterInstance Master object with protected field
	   * @param sFieldName Field name
	   * @return Field content as object
	   */
	  public static Object getClassField( Object masterInstance, String sFieldName )
	  {
	    try
	    { // get the protected field
	      Field field = masterInstance.getClass().getDeclaredField(sFieldName);
	      boolean isAccessible = field.isAccessible();
	      field.setAccessible(true);
	      Object obj = field.get( masterInstance );
	      field.setAccessible( isAccessible );
	      return obj;
	    }
	    catch ( SecurityException ex )
	    {
	      ex.printStackTrace();
	    }
	    catch ( NoSuchFieldException ex )
	    {
	      ex.printStackTrace();
	    }
	    catch ( IllegalArgumentException ex )
	    {
	      ex.printStackTrace();
	    }
	    catch ( IllegalAccessException ex )
	    {
	      ex.printStackTrace();
	    }
	    return null;
	  }
}
