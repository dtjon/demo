//=============================================================================
//              Copyright (c) 2003-2008 UGS Corporation
//                 Unpublished - All Rights Reserved
//=============================================================================
//
//     Filename: actiondialog.js
//     Module  : common
//
//     an action dialog is a specialisation of a dialog; it is constructed from a form template
//     and automatically performs an action upon OK Apply (optional) or Cancel being clicked.
//     the parameters passed to the action are taken from the buildURL of the form renderer,
//     with an optional selection path.
//
//     the action script must return an update message.
//
//     the following tasks are examples that fall easily into this paradigm:
//
//     - all five new dialogs
//     - save as
//     - properties
//     - initiate process
//     - check out
//
//     and that's just off workspace.
//
//     TODO:
//
//     - make the button thing a mask, so that you can have any combination of OK / Apply /
//       Cancel.
//
//     - make the selection thing be "none/one/many" rather than "none/one" as it currently is.
//
//     - allow for messages on success (like Carlos' "Job Submitted" box).
//
//     - have the form renderer attach root object tag to the table, and buildURL should take
//     note of this.
//
// 12-Jan-2010  J.Mansvelders           Fix for issue with mandatory fields

// ============================================================================

// urlBuilder is an optional arg specifying the function name of the dialog parsing function
function genericAction( table, popDownOnSuccess, urlBuilder, isProcessTabs )
{
    var actionScript = table.getAttribute("actionScript");
    if(actionScript == null)
    {
        actionScript = table.actionScript;
    }

    var request = "webclient?TC_file=" + actionScript;

    if( urlBuilder )
    {
        // if explicitly set, use it
    }
    // for incremental processing, e.g., formmodify on master form tab for saveas/revise
    else if( table.incremental )
    {
        urlBuilder = "buildURLWithoutDialog";
    }
    // include a parameter array in request
    else if( getDialog( table ).paramarray )
    {
        urlBuilder = "buildURLWithAttrs";
    }
    // old logic for setting incremental... for dialogs where tabs are not processed all at once
    // this condition can be removed once "incremental" is fully applied
    else if( popDownOnSuccess == false && isProcessTabs == null )
    {
        urlBuilder = "buildURLWithoutDialog";
    }
    else
    {
        urlBuilder = "buildURL";
    }

    request += eval( urlBuilder + "( table )"  );

    if( table.takeSelection )
    {

        var parentUID;
        if( table.takeSelection == 'top' )
        {
            parentUID = getTopLine().iman_parent_uid;
        }
        else
        {
            if( firstSelectedRowOrTopLine() == null )
            {
                parentUID = 'AAAAAAAAAAAAAA';  //NULLTAG
            }
            // In createbusinessobject.xml, we contextually insert the newly created objects based on
            // the application from which the new object is created. Hence we needed this special casing.
            else if(((getAppName() == getString("web_pse_appname") || getAppName() == getString("web_rm_appname") ) 
                || getAppName() == getString("web_inbox_appname"))
                && (actionScript.indexOf("createbusinessobject.xml") != -1))
            {
                parentUID = firstSelectedRowOrTopLine().iman_parent_uid;
            }
            else
            {
                parentUID = firstSelectedRowOrTopLine().iman_tag;
            }

            if( table.takeSelection == 'lhn' )
            {
                request += "&lhn=true";
            }
        }
        request += "&parent_uid=" + parentUID;
    }
    this.url = request;
    this.dialog = getDialog(table);

    // Do nothing if callback is disabled
    if( this.dialog && this.dialog.disableCallBack )
    {
        this.callback = doNothingCallback;
    }
    else
    {
        this.callback = updateCallback;
    }

    this.popDownOnSuccess = popDownOnSuccess;
}


//This is dummy callback, which doesn't perform any action, except showing error messages is any.
function doNothingCallback( documentElement )
{
    // Check if the response contains any errorstack, if yes then process it.
    var errorStackNodes = documentElement.getElementsByTagName( "errorstack" );

    if ( errorStackNodes && errorStackNodes[0] )
    {
        errorAlert( errorStackNodes[0] );

        return false;
    }
}

function genericActionCall( table, popDownOnSuccess, urlBuilder, isProcessTabs )
{
     doRequest( new genericAction( table, popDownOnSuccess, urlBuilder, isProcessTabs ), false );
}

function buttonOK( dialog )
{
    // special validator used only by reports so far...
    if ( dialog.dialogValidator )
    {
        if ( !dialog.dialogValidator( dialog ) )
        {
            return false;
        }
    }

    // This flag checks if there is no error in actionCall, then proceed with the showing processing icon and further action.
    var shouldProceed = true;

    var actionScript = dialog.table.actionScript;
    if( actionScript == "common/actions/saveasitemrev.xml" )
    {
        var content0 = document.getElementById("contentsaveasitemrev0");
        var tbody = content0.firstChild.firstChild;

        globalNewItemDialogIM = 0;
        globalNewItemDialogIRM = 0;


        var content1 = document.getElementById("contentsaveasitemrev1");

        // Validate the Item Master form
        if(content1)
        {
            if(validateItemMasterForms(content1.firstChild) == false)
            {
                alertString( 63144 );
                return;
            }
        }

        // Validate the Item Revision Master form
        var content2 = document.getElementById("contentsaveasitemrev2");

        if(content2)
        {
            if(validateItemMasterForms(content2.firstChild) == false)
            {
                alertString( 63145 );
                return;
            }
        }

        var content0 = document.getElementById("contentsaveasitemrev0");
        var tbody = content0.firstChild.firstChild;
        //reaching to the last node in the DOM structure of revise as dialog
        var tbodyToModify = tbody.firstChild.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling;

        //Access the hidden INPUT field for imform,irmform.
        var imform = document.getElementById( "imformuid" );
        var irmform = document.getElementById( "irmformuid" );

        //Checking for the Value attribute of the hidden INPUT field for imform.
        var imformuid = '';
        if ( imform != null )
        {
            imformuid = imform.getAttribute( "value" );
        }
        var irmformuid = '';
        if ( irmform != null )
        {
            irmformuid = irmform.getAttribute( "value" );
        }

        var shouldCheckForIRM = false;
        if(!tbodyToModify || (imformuid == '' && irmformuid == ''))
        {
            // If Finish button is hit without creating IM and IRM form
            updateWithMasterFormUID("imformuid", null, contentName)
            updateWithMasterFormUID("irmformuid", null, contentName)

            globalNewItemDialogIM = 1;
            globalNewItemDialogIRM = 1;
        }
        else
        {
            shouldCheckForIRM = true;
            // For IM form

            var content1 = document.getElementById("contentsaveasitemrev1");

            if(content1.firstChild.firstChild.firstChild)
            {
                // Set the IRM form properties
                dialog.table.actionCall( content1.firstChild, false );
            }
            else
            {
                // If Finish button is hit without creating IM
                updateWithMasterFormUID("imformuid", null, contentName);
                globalNewItemDialogIM = 1;
            }
        }

        if(shouldCheckForIRM == true)
        {
            tbodyToModify = tbodyToModify.nextSibling;
            if( !content2.firstChild.firstChild.firstChild )
            {
                updateWithMasterFormUID("irmformuid", null, contentName)
                globalNewItemDialogIRM = 1;
            }
            else
            {
                // Set the IRM form properties
                callSetAttributesIRMRevise();
            }
        }

        // Set the IRM form properties
        callItemRevisionCreate();
    }


    else
    {
        // Fix for PR#5314337
        // All the dialogs need not be having actionCall function
        // for ex: Dialogs created through createDialog function
        // The if condition is added to overcome the JavaScript error
        if ( getAttr(dialog.table, "actionCall") != null )
        {
            shouldProceed = dialog.table.actionCall( dialog.table, true );
        }
    }

    if(! (shouldProceed == false))
    {
        shouldProceed = true;
    }

    // Do not show processing icon incase of cached dialog.
    //Also do not show processing icon in case of already popped-down dialogs (hidden or non-displayed dialogs).
    //On finish button click do not show Please wait icon when IRD is to be shown 
    //selectedBOData will be populated only incase of BO requires IRD to be shown during create
    var isIRDRequiredOnOK = false;
    
    if ( dialog.table.selectedBoData )
    {
         isIRDRequiredOnOK = isSearchRequiredOnCreate ( dialog.table.selectedBoData.mfkBOName ) ;
    }

    if(   ( isIRDRequiredOnOK == false )  && (!dialog.cached && ((dialog.style.visibility != "hidden") || (dialog.style.display != "none")) && shouldProceed ))
    { 
        var loadingObj = new ShowPageLoadingObj (dialog, "nonmodal");
        dialog.loadingObj = loadingObj;
    }
}

// Apply button
function buttonApply( dialog )
{
    if ( dialog.dialogValidator )
    {
        if ( !dialog.dialogValidator( dialog ) )
        {
            return false;
        }
    }


    // Conditionalproperties fields needs to check if the form has any mandatory fields
    //Check if Conditionalproperties fields  are activated
    if(dialog.CONDITIONALID && dialog.CONDITIONALID == "CONDITIONALPROPERTYFIELDS")
    {
        //Check if dialog has unfilled fields
        if(checkRequiredFieldsInDialog( dialog ))
        {
            //Dialog has unfilled fields - give error message
            alertString( 63138 );
            //Prevent the Apply action from proceeding
            return false;
        }//Check for checkRequiredFieldsInDialog ends
    }//Check for conditional properties ends


    dialog.table.actionCall( dialog.table, false, null, true );
}

// Cancel button
function buttonCancel( dialog )
{
    dialog.popdown();
}

function attachOKApplyCancelButtons( table, applyButton )
{
     //                       tfoot         tr         td
     var buttonCell = table.firstChild.firstChild.firstChild;

     // grabbed from cp PLM39347...
     //added okbtn id, if some one want to remove this line, make sure to add button id.
     buttonCell.innerHTML = "<input type=button id=\"okbtn\" class=\"buttons\" onclick=\"buttonOK(getDialog(this))\" value=\""
                            + getString( "web_ok" ) + "\">";

    if ( applyButton )
    {
        //added apply button id.
        buttonCell.innerHTML +=
            "<input type=button id=\"applybtn\" class=\"buttons\" onclick=\"buttonApply(getDialog(this))\" value=\"" + getString( "web_apply" ) + "\">";
    }

     //added cancel buttun id.
    buttonCell.innerHTML +=
        "<input type=button id=\"cancelbtn\" class=\"buttons\" onclick=\"buttonCancel(getDialog(this))\"  value=\"" + getString( "web_cancel" ) + "\">";

}

function attachApplyButton( table )
{
    //                       tfoot         tr         td
    var buttonCell = table.firstChild.firstChild.firstChild;

    buttonCell.innerHTML =
            "<input type=button id=\"applybtn\" class=\"buttons\" onclick=\"buttonApply(getDialog(this))\" value=\"" + getString( "web_apply" ) + "\">";

}

function createActionDialog( formNode, actionScript, takeSelection, applyButton, numCols,
                             actionCall, renderer, resizable )
{
    // check for custom overrides
    var type = formNode.getAttribute( "type" );

    var overrides = globalCustomDialogType[ type ];
    // set custom overrides
    for( override in overrides )
    {
        eval( override + '= overrides["' + override + '"]');
    }

    // set optional arguments to defaults
    if( !numCols )
    {
        numCols = 2;
    }
    if( !actionCall )
    {
        actionCall = genericActionCall;
    }

    var viewsNode = formNode.getElementsByTagName( "views" );
    var dlgType = formNode.getAttribute("dlgtype");

    //Custom XRT for Summary contains "views" element.
    //For Summary display, create an embedded dialog.
    if( viewsNode.length == 1 || dlgType == "embedded" )
    {
        var dialog = createEmbedDialog( numCols, null, resizable );
    }
    else
    {
        var dialog = createDialog( numCols, null, resizable );
    }

    var table = dialog.table;
    var foot = dialog.foottable;

    if( dlgType == "embedded" )
    {
        renderForm( formNode, dialog.bodyDiv, renderer, "" );
    }
    else
    {
        renderForm( formNode, table, renderer, "" );
    }
    if ( dlgType != "embedded" )
    {
        if ( foot.modifiable )
		{
			if( viewsNode.length == 1 )
			{
				attachApplyButton( foot );
			}
			else
			{
				attachOKApplyCancelButtons( foot, applyButton );
			}
		}
    }

    //calcAndSetHeight( dialog );

    table.takeSelection = takeSelection;
    table.actionScript = actionScript;
    table.actionCall = actionCall;

    return dialog;
}

function addYesnoButton(table)
{
    //                       tfoot         tr         td
    var buttonCell = table.firstChild.firstChild.firstChild;

    buttonCell.innerHTML = "<input type=button onclick=\"buttonOK( getDialog(this))\" value=\"" + getString( "web_yes" ) + "\">";

    buttonCell.innerHTML += "<input type=button onclick=\"buttonCancel(getDialog(this))\" value=\"" + getString( "web_no" ) + "\">";
}

// Handle back button navigation
function buttonBack( dialog )
{
    selectPreviousTab(dialog);
    updateTabNavigationButtons( dialog );
}

// Handle next button navigation
function buttonNext( dialog )
{
    selectNextTab(dialog);
    updateTabNavigationButtons( dialog );
}

function attachBackNextButtons( table )
{
    //                       tfoot         tr         td
    var buttonCell = table.firstChild.firstChild.firstChild;
    buttonCell.innerHTML = "<input  id='back' disabled type=button class=\"buttons\" onclick=\"buttonBack(getDialog(this))\" value=\"" + getString( "web_back" ) + "\">";
    buttonCell.innerHTML += "<input id='next' type=button class=\"buttons\" onclick=\"buttonNext(getDialog(this))\" value=\"" + getString( "web_next" ) + "\">";
    buttonCell.innerHTML +=
        "<input id='finish' type=button class=\"buttons\" onclick=\"buttonOK(getDialog(this))\" value=\"" + getString( "web_finish" ) + "\">";
    buttonCell.innerHTML +=
        "<input type=button class=\"buttons\" onclick=\"buttonCancel(getDialog(this))\" value=\"" + getString( "web_cancel" ) + "\">";
}

//This function will be called when onclick event of Assign button is fired and submits the request.
function buttonAssign( assignBtn )
{
    doRequest(new assignAutoAssignableValues(assignBtn));
}

//This function will call assign.xml TC script which processes request.
function assignAutoAssignableValues(assignBtn)
{
    var assignURL = "webclient?TC_file=common/actions/assign.xml";
    var dialog = getDialog(assignBtn.row);
    assignURL += getAssignURL(assignBtn);
    var loadingObj = new ShowPageLoadingObj (dialog, "nonmodal");
    dialog.loadingObj = loadingObj; 
    
    this.dialog = dialog;
    this.row = assignBtn.row;
    this.url = assignURL;
    this.callback = autoAssignableCallback;
}

// creates tabbed dialog and adds back, next and finish buttons to tabbed dialog.
// Handles navigation among the tabs and buttons.
function createWizardDialog( formNode, actionScript, takeSelection, applyButton, numCols,
                             actionCall, renderer, resizable, tabContentName )
{
    // check for custom overrides
    var type = formNode.getAttribute( "type" );
    var overrides = globalCustomDialogType[ type ];
    // set custom overrides
    for( override in overrides )
    {
        eval( override + '= overrides["' + override + '"]');
    }

    // set optional arguments to defaults
    if( !numCols )
    {
        numCols = 2;
    }
    if( !actionCall )
    {
        actionCall = genericActionCall;
    }

    var dialog = createDialog( numCols, null, resizable );
    dialog.setAttribute( 'type', 'wizard' );
    var table = dialog.table;
    var foot = dialog.foottable;
    renderForm( formNode, table, renderer, tabContentName );

    if ( foot.modifiable )
    {
        attachBackNextButtons( foot );
    }
    // Disable the next button if there is only one tab
    if (formNode.getElementsByTagName("tab_form") && formNode.getElementsByTagName("tab_form").length == 1 && formNode.getElementsByTagName("page") && formNode.getElementsByTagName("page").length == 0  )
    {
        document.getElementById("next").disabled = true;
    }
    if (formNode.getElementsByTagName("page") && formNode.getElementsByTagName("page").length == 1 && formNode.getElementsByTagName("tab_form") && formNode.getElementsByTagName("tab_form").length == 0)
    {
        document.getElementById("next").disabled = true;
    }
    table.takeSelection = takeSelection;
    table.actionScript = actionScript;
    table.actionCall = actionCall;
    return dialog;
}


// Function to validate if all the required fields for the Item Master
// forms have been filled
function validateItemMasterForms( table )
{
    var valid = true;

    var nextRow;
    if( table.firstChild.tagName == "TBODY" )
    {
        //                 tbody       tr
        nextRow = table.firstChild.firstChild;
    }
    else if(table.firstChild.tagName == "THEAD") // THEAD
    {
        //                thead      tbody       tr
        nextRow = table.firstChild.nextSibling.firstChild;
    }
    else
    {
        /* In case of Mozilla,firstChild can be a text node
          if there is next line character of table tag  */

        //                text        thead      tbody       tr
        nextRow = table.firstChild.nextSibling.nextSibling.firstChild;
    }

    while ( nextRow )
    {
        // Check if this is a required field
        // If it is a required field it should have a value set
        var searchStr = nextRow.firstChild.innerHTML;
        if(searchStr.indexOf("*") >= 0)
        {
            // JWO|26-06-2016 
            if (nextRow.firstChild.nextSibling.firstChild == null)
            {
                valid = false;
                return valid;
            }
            
            if (nextRow.firstChild.nextSibling.innerHTML == "")
            {
                // This is a required field. This should have a value set
                var valueOfAttr = queryInputValue(nextRow.firstChild.nextSibling);
                if(valueOfAttr == "" || valueOfAttr == null)
                {
                    valid = false;
                    return valid;
                }
            }
        }

        nextRow = nextRow.nextSibling;
    }

    return valid;
}
//This function is needed to set visibilty of other suggestive LOV field
//This is a part of fix to PR#6296414(LOV Other... field is
//persistent on all the tab pages on create dialog)
//This is not a browser specific. This function will be called
//when user switches back and next on create dialog of tabbed pages
function otherLOVField( dialog )
{
    var tabPanelDivNodeList =  globalYuiInstance.one(dialog).all('.yui3-tab-panel');
    tabPanelDivNodeList.each(function(node){

        var panelDivNode = globalYuiInstance.Node.getDOMNode(node);

        var divs = panelDivNode.getElementsByTagName("DIV");

        for ( i = 0; i < divs.length; i++ )
        {
            if ( divs[i].className == "comboDIV" )
            {
                if ( node.hasClass('yui3-tab-panel-selected') )
                {
                    var selectElem = divs[i].firstChild;
                    if ( selectElem.options[ selectElem.selectedIndex ] != null )
                    {
                        if ( selectElem.options[ selectElem.selectedIndex ].text == getString( "web_lov_other_option" ) )
                        {
                            if ( divs[i].firstChild.nextSibling.nodeName == "INPUT" )
                            {
                                var input = divs[i].firstChild.nextSibling;
                                if ( input.style.visibility == "hidden" )
                                {
                                    input.style.visibility = "visible";
                                }
                            }
                        }
                    }
                }
                else
                {
                    if ( divs[i].firstChild.nextSibling.nodeName == "INPUT" )
                    {
                        var input = divs[i].firstChild.nextSibling;
                        if ( input.style.visibility == "visible" )
                        {
                            input.style.visibility = "hidden";
                        }
                    }
                }
            }
        }

    },this);
}

