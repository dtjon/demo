/**
 (C) copyright by Siemens PLM Software

 Description  :   Dispatcher Request

 Created for  :   FEI

 Comments     :   Every new procedure or function in this mdoules starts
                  with BNL_<module>_

 Filename     :   $Id: bnl_runtimeprops.c 113 2015-02-19 10:26:14Z tjonkonj $

 Version info :   release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 Reason / description                 Version By          Date
 ----------------------------------------------------------------------------------------------------------
 Creation for TC10.1                  0.1     DTJ         06-Jun-2016
*/

#include <stdio.h>

#include <tc/iman_preferences.h>
#include <common/basic_definitions.h>
#include <tccore/aom_prop.h>

#include <BNL_tb_module.h>
#include <BNL_strings_module.h>
#include <BNL_em_module.h>
#include <BNL_fm_module.h>

/* Global declarations */
extern FILE *feilog;
static char BNL_module[] = "BNL_dp_request 0.1";

#define BNL_DP_REQ_CMD_PREF     "FEI_dp_req_cmd" // preference for dispatcher-command

/*****************************************************************************************************
 * int BNL_create_dispatcher_request:
 * Method to get runtimeprop value
 *
 * @param[I]    tItemRev:       Primary object
 * @param[I]    pszPrefName:    Preference to use for request. Eg c:\%BNL_UPM%\NOT_USED_FEI_csp_imf.ini
 * @param[I]    iExtraCols:     number of (additional) request arguments
 * @param[I]    pszExtraCols:   (additional) request arguments value
 *
 * @retval ITK_ok on successful execution of function, Else ITK Error code.
 *
 * NOTES:
 * Function used in TC_yoda AND FEI_process_fru_attr module
 * replace files in both modules if changes are applied
 * create dispatcher request is asynchrone. If error processing is required, add mailing in the batch-file
 *
 * History of changes
 * Date        Author           Description
 * ----------- ---------------- ----------------------------------------------------------------------
 * 06-Jun-2016 DTJ              Created, replacement for obsolete function BNL_FEI_create_plot_request
 ****************************************************************************************************/
int BNL_create_dispatcher_request(tag_t tItemRev, char *pszPrefName, int iExtraCols, char **pszExtraCols)
{
  char        *pszModName   = "BNL_create_dispatcher_request";
  int         iRetCode      = ITK_ok;

  int         i             = 0;
  char        *pszTempExec  = NULL;
  char        *pszExecutable= NULL;
  char        *pszCommand   = NULL;
  char        *pszItemID    = NULL;
  char        *pszItemRevID = NULL;
  char        *pszTempFile  = NULL;
  char        *pszIniFile   = NULL;

  // Executable eg: c:\\temp\\fei_create_dp_rqst.bat
  //BNL_tb_pref_ask_char_value(BNL_DP_REQ_CMD_PREF, IMAN_preference_site, &pszExecutable);
  BNL_tb_pref_ask_char_value(BNL_DP_REQ_CMD_PREF, IMAN_preference_all, &pszTempExec);
  if (pszTempExec == NULL)
  {
    fprintf(feilog, "%s: Preference [%s] not defined. No Dispatcher Request is being created!\n", BNL_module, BNL_DP_REQ_CMD_PREF);
    goto function_exit;
  }
  else
  {
    BNL_tb_replace_env_for_path(pszTempExec, &pszExecutable);
    if (pszExecutable == NULL)
    {
      //pszExecutable = BNL_strings_copy_string(pszTempExec);
      pszExecutable = MEM_string_copy(pszTempExec);
    }

    fprintf(feilog, "%s: Using Dispatcher command [%s]\n", BNL_module, pszExecutable);

    if (!BNL_fm_check_file_or_dir(pszExecutable))
    {
      fprintf(feilog, "%s: Error, the given Dispatcher command [%s] not found.\n" , BNL_module, pszExecutable);
      goto function_exit;
    }
  }

  // -config argument
  BNL_tb_pref_ask_char_value(pszPrefName, IMAN_preference_site, &pszTempFile);
  if (pszTempFile == NULL)
  {
    fprintf(feilog, "%s: Preference [%s] not defined. No Dispatcher Request is being created!\n", BNL_module, pszPrefName);
    goto function_exit;
  }
  else
  {
    BNL_tb_replace_env_for_path(pszTempFile, &pszIniFile);
    if (pszIniFile == NULL)
    {
      //pszIniFile = BNL_strings_copy_string(pszTempFile);
      pszIniFile = MEM_string_copy(pszTempFile);
    }

    fprintf(feilog, "%s: Using ini file [%s]\n", BNL_module, pszIniFile);

    if (!BNL_fm_check_file_or_dir(pszIniFile))
    {
      fprintf(feilog, "%s: Error, the given inifile [%s] not found.\n" , BNL_module, pszIniFile);
      goto function_exit;
    }
  }

  // build command
  pszCommand = BNL_strings_low_strssa(NULL, "start /B "); // run command in background
  pszCommand = BNL_strings_low_strssa(pszCommand, pszExecutable);

  iRetCode = AOM_UIF_ask_value(tItemRev, "item_id", &pszItemID);
  if (BNL_em_error_handler("AOM_UIF_ask_value(item_id)", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;
  pszCommand = BNL_strings_low_strssa(pszCommand, " \"");
  pszCommand = BNL_strings_low_strssa(pszCommand, pszItemID);
  pszCommand = BNL_strings_low_strssa(pszCommand, "\" \"");

  iRetCode = AOM_UIF_ask_value(tItemRev, "item_revision_id", &pszItemRevID);
  if (BNL_em_error_handler("AOM_UIF_ask_value(item_revision_id)", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;
  pszCommand = BNL_strings_low_strssa(pszCommand, pszItemRevID);
  pszCommand = BNL_strings_low_strssa(pszCommand, "\" \"");

  pszCommand = BNL_strings_low_strssa(pszCommand, pszIniFile);
  pszCommand = BNL_strings_low_strssa(pszCommand, "\"");

  for (i = 0; i < iExtraCols; i++)
  {
    pszCommand = BNL_strings_low_strssa(pszCommand, " \"");
    pszCommand = BNL_strings_low_strssa(pszCommand, pszExtraCols[i]);
    pszCommand = BNL_strings_low_strssa(pszCommand, "\"");
  }

  iRetCode = system(pszCommand);
  /* arguments:
   * 1. fullpath batch-file
   * 2. item_id
   * 3. item_revision_id
   * 4. fullpath configuration ini-file
   * 5. etc extra colums
   * Eg: c:\TC10\create_request_tool.cmd "
   */
  fprintf(feilog, "Running command [%d]=[%s]\n", iRetCode, pszCommand);

function_exit:
  if (pszTempExec != NULL) MEM_free(pszTempExec);
  if (pszExecutable != NULL) MEM_free(pszExecutable);
  if (pszItemID != NULL) MEM_free(pszItemID);
  if (pszItemRevID != NULL) MEM_free(pszItemRevID);

  if (pszTempFile != NULL) MEM_free(pszTempFile);
  if (pszIniFile != NULL) MEM_free(pszIniFile);

  if (pszCommand) free(pszCommand);

  // Always return ITK_ok
  return ITK_ok;
}

