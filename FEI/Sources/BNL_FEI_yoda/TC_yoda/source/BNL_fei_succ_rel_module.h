/**
 (C) copyright by Siemens Product Lifecylce Management Software (NL) B.V.


 Description  :  This module header file belongs to the yoda module .c file.
                 
 Created for  :  FEI

 Comments     :  

 Filename     : $Id: BNL_fei_succ_rel_module.h 411 2015-11-10 10:05:15Z tjonkonj $

 Version info :  release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 reason /description                        Version By                  date
 ----------------------------------------------------------------------------------------------------------
 Creation                                   0.1     J. Wijnja      26-03-2009
 Use FEI report preferences                 0.2     J.M.           18-06-2009
 Added new functions                        0.2     J.M.           02-03-2010
 Added BNL_check_for_successor_relation     0.3     J.M.           03-09-2010
 changed DEFAULT_PREDRELATION value         0.4     D.Tjon         04-11-2015
 
*/

#define BNL_FEI_SUCCESSOR_REL          "BNL_succ_relation"
#define BNL_FEI_PREDECESSOR_REL        "BNL_pred_relation"

#define DEFAULT_PREDRELATION           "CMSolutionToImpacted"
#define DEFAULT_SUCCRELATION           "FEI_Successor_relation"

int BNL_FEI_succ_rel_initialise(FILE *pLog);
int BNL_FEI_succ_rel_module_exit();
int BNL_FEI_create_successor(METHOD_message_t *pmMessage,va_list args);
int BNL_FEI_delete_successor(METHOD_message_t *pmMessage,va_list args);
int BNL_assign_successor_relation(tag_t tPrim,tag_t tSecond);
int BNL_delete_successor_relation(tag_t tPrim,tag_t tSecond);
int BNL_check_for_successor_relation(tag_t tPrim, tag_t tSecond, logical *plExist);

//int BNL_FEI_succ_pre_saveas(void *pReturnValue);
//int BNL_FEI_succ_post_saveas(void *pReturnValue);
