/**
 (C) copyright by Siemens Product Lifecylce Management Software (NL) B.V.


 Description    :   BNL FEI yoda module

 Created for    :   FEI

 Comments       :   Every new procedure or function in this modules starts
                    with BNL_

 Filename       :   $Id: BNL_fei_yoda_module.c 449 2016-06-28 09:00:09Z tjonkonj $


 History of changes
 Reason /description                                                    Version     By          Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.1         JM          15-01-2010
 Added csp generation                                                   0.2         JM          22-01-2010
 Fixed issue during Item Creation                                       0.3         JM          26-01-2010
 Updated BNL_fei_yoda_update_relation                                   0.4         JM          12-02-2010
 Updated BNL_FEI_itemrev_csp                                            0.5         JM          15-02-2010
 Added new functions for fru functionality                              0.6         JM          25-02-2010
 Updated fru functionality (use id for desc)                            0.7         JM          01-03-2010
 Fixed issue with setting of status                                     0.8         JM          03-03-2010
 Enhanced bypass check                                                  0.9         JM          11-03-2010
 Updated for new successor/predecessor behavior                         1.0         JM          30-03-2010
 Enhanced bypass check                                                  1.1         JM          31-03-2010
 Unload bv and bvr                                                      1.2         JM          15-04-2010
 Unload created fru item                                                1.3         JM          30-06-2010
 Send E-mail when fru item is being created                             1.4         JM          13-07-2010
 Updated creation of fru item                                           1.5         JM          29-07-2010
 Updated BNL_FEI_create_plot_request                                    1.6         JM          03-09-2010
 Added bvr runtime property                                             1.7         JM          16-09-2010
 Updated BNL_fei_yoda_grm_delete_pre and added saveas actions           1.8         JM          17-09-2010
 Updated BNL_fei_yoda_check_uom                                         1.9         JM          03-11-2010
 Updated csp creation                                                   2.0         JM          15-03-2011
 Updated runtime properties                                             2.1         JM          09-09-2011
 Changed check for the save of bom                                      2.2         JM          09-09-2010
 Added init for 64 bit                                                  2.3         JM          11-11-2010
 Fixed issue with BNL_FEI_create_plot_request                           2.4         JM          11-11-2010
 Added workaround for IR 1875690                                        2.5         JM          12-08-2011
 Updated BNL_fei_yoda_check_uom and reset forms variable                2.6         JM          14-05-2012
 Updated for IR 1983274 and IR 1984828                                  2.7         JM          30-05-2014
 Update for TC 10.1.*                                                   2.8         DTJ         31-05-2016


*/
#include <stdlib.h>

#ifdef  TC10_1
#include <tc/iman.h>
#include <tccore/imantype.h>
#include <ict/ict_userservice.h>
#include <tccore/aom_prop.h>
#include <lov/lov.h>
#else
#include <iman.h>
#include <item.h>
#include <bom.h>
#include <ps.h>
#include <lov.h>
#include <aom_prop.h>
#include <form.h>
#include <user_server_exits.h>
#include <grmtype.h>
#endif

#include <BNL_register_bnllibs.h>
#include <BNL_tb_module.h>
#include <BNL_em_module.h>
#include <BNL_fm_module.h>
#include <BNL_xml_module.h>
#include <BNL_strings_module.h>
#include <BNL_errors.h>
#include <BNL_csp_module.h>

#include <BNL_fei_yoda_module.h>
#include <BNL_register_yoda.h>
#include <BNL_fei_attr_check_module.h>

static char BNL_module[] = "BNL YODA MOD 2.8";

FILE *feilog;

typedef struct sImfsList_s
{
  tag_t *ptImfs;
  char **pszNames;
  int iObjects;
} sImfsList_t;

// Globals
logical glCreateFlag = false;
logical glDeleteFlag = false;

int giComps        = 0;
tag_t *gptComps    = NULL;

tag_t gtFruRev     = NULL_TAG;

sImfsList_t *gpsList       = NULL;
sImfsList_t *gpsListPortal = NULL;

int giMdrMetaAttributes    = 0;
char **gpszMdrMetaAttrNames= NULL;
char **gpszMdrMetaAttrValuesOld= NULL;
tag_t gtCspIMF             = NULL_TAG;
logical glCspIMF           = false;

int giSaveAs               = 0;


#define BNL_BOM_VAL_PREF              "FEI_BOM_VALIDATION"
#define BNL_BOM_VAL_PREF_TOPLEVEL     "FEI_BOM_VALIDATION_TOP_LEVEL_ONLY"
#define FEI_MDR_METADATA_ATTRS_SEND   "FEI_MDR_MetaData_Attributes_to_send"

#define BNL_FEI_UOM           "uom_tag"
#define BNL_UOM_NOT_ALLOWED   (BNL_error_base + 2)

int BNL_fei_yoda_rtp_fill_prop(METHOD_message_t *message, va_list args);
int BNL_fei_yoda_rtp_bvr_fill_prop(METHOD_message_t *message, va_list args);
int BNL_fei_yoda_add_propvalue(tag_t tObject, char *pszProperty, char **pszPropValue);
int BNL_fei_yoda_check_bomlines(tag_t tTopline, int iPrefCount, char **pszPrefValues);
int BNL_fei_yoda_check(tag_t tParent, tag_t tChild, int iPrefCount, char **pszPrefValues, char **pszError);
int BNL_FEI_get_item_from_imf(tag_t tIMF, tag_t *ptItem);
int BNL_FEI_get_imf_from_item(tag_t tItem, tag_t *ptIMF);
int BNL_FEI_create_plot_request(tag_t tItemRev, char *pszType, int iExtraCols, char **pszExtraCols);
int BNL_create_dispatcher_request(tag_t tItemRev, char *pszPreference, int iExtraCols, char **pszExtraCols);

int BNL_FEI_FRU_create_item(char *pszItemType, char *pszDesc, char *pszName, tag_t *ptItem, tag_t *ptRev);
int BNL_FEI_FRU_set_status_change_ownership(tag_t tItem, tag_t tRev, char *pszUser, char *pszGroup, char *pszStatus);
int BNL_FEI_FRU_check_attr_value(tag_t tItem, logical *plResult);
int BNL_FEI_FRU_update_bom(tag_t tItem, tag_t tRev);
int BNL_FEI_get_bomview(tag_t tItem, tag_t tRev, tag_t *ptBv, tag_t *ptBvr, logical *plViewCreated);

int BNL_FEI_create_csp(tag_t tItem);


int BNL_FEI_yoda_initialise(FILE *pLog)
{
  int iRetCode        = 0;

  feilog = pLog;

  return iRetCode;
}


int BNL_FEI_yoda_exit_module()
{
  int iRetCode        = 0;
  int i               = 0;

  if (giComps > 0) MEM_free(gptComps);

  if (gpsList != NULL)
  {
    // Clear memory
    for (i = 0; i < gpsList->iObjects; i++)
    {
      free(gpsList->pszNames[i]);
      gpsList->pszNames[i] = NULL;
    }
    MEM_free(gpsList->pszNames);
    gpsList->pszNames = NULL;
    MEM_free(gpsList->ptImfs);
    gpsList->ptImfs = NULL;
    MEM_free(gpsList);
    gpsList = NULL;
  }

  if (gpsListPortal != NULL)
  {
    // Clear memory
    for (i = 0; i < gpsListPortal->iObjects; i++)
    {
      free(gpsListPortal->pszNames[i]);
      gpsListPortal->pszNames[i] = NULL;
    }
    MEM_free(gpsListPortal->pszNames);
    gpsListPortal->pszNames = NULL;
    MEM_free(gpsListPortal->ptImfs);
    gpsListPortal->ptImfs = NULL;
    MEM_free(gpsListPortal);
    gpsListPortal = NULL;
  }

  if (giMdrMetaAttributes > 0)
  {
    for (i = 0; i < giMdrMetaAttributes; i++)
    {
      if (gpszMdrMetaAttrNames[i] != NULL) free(gpszMdrMetaAttrNames[i]);
      gpszMdrMetaAttrNames[i] = NULL;
      if (gpszMdrMetaAttrValuesOld[i] != NULL) free(gpszMdrMetaAttrValuesOld[i]);
      gpszMdrMetaAttrValuesOld[i] = NULL;
    }
    if (gpszMdrMetaAttrNames != NULL) MEM_free(gpszMdrMetaAttrNames);
    gpszMdrMetaAttrNames =NULL;
    if (gpszMdrMetaAttrValuesOld != NULL) MEM_free(gpszMdrMetaAttrValuesOld);
    gpszMdrMetaAttrValuesOld = NULL;
    giMdrMetaAttributes = 0;
  }

  return iRetCode;
}


/**
int BNL_fei_yoda_rtp_property()

  Description:

  Create the runtime property.

*/
int BNL_fei_yoda_rtp_property()
{
  int iRetCode                = 0;
  int iPrefCount              = 0;
  int i                       = 0;

  char szPropName[129];

  char **pszPrefValues         = NULL;

  METHOD_id_t method_id;


  BNL_tb_pref_ask_char_values("FEI_RTP_names", IMAN_preference_site, &iPrefCount, &pszPrefValues);

  for (i = 0; i < iPrefCount; i++)
  {
    strcpy(szPropName, pszPrefValues[i]);

    /* Add the method that gets the link attribute */
    iRetCode = METHOD_register_prop_method("WorkspaceObject", szPropName, PROP_ask_value_string_msg, BNL_fei_yoda_rtp_fill_prop, 0, &method_id);
    if (BNL_em_error_handler("METHOD_register_prop_method", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    fprintf(feilog, "%s: Runtime property [%s] has been registered for type [WorkspaceObject].\n", BNL_module, szPropName);
  }

function_exit:

  if (iPrefCount > 0) MEM_free(pszPrefValues);

  return iRetCode;
}

int BNL_fei_yoda_rtp_bvr()
{
  int iRetCode                = 0;

  char *pszPropName           = NULL;

  METHOD_id_t method_id;



  BNL_tb_pref_ask_char_value(BNL_FEI_PREF_RTP_BVR, IMAN_preference_site, &pszPropName);

  if (pszPropName != NULL)
  {
    /* Add the method that gets the attribute */
    iRetCode = METHOD_register_prop_method("PSBOMViewRevision", pszPropName, PROP_ask_value_string_msg, BNL_fei_yoda_rtp_bvr_fill_prop, 0, &method_id);
    if (BNL_em_error_handler("METHOD_register_prop_method", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    fprintf(feilog, "%s: Runtime property [%s] has been registered for type [PSBOMViewRevision].\n", BNL_module, pszPropName);
  }

function_exit:

  if (pszPropName != NULL) MEM_free(pszPropName);

  return iRetCode;
}


/**
int BNL_fei_yoda_rtp_property(METHOD_message_t *message, va_list args)

  Description:

  Fill the runtime property.

*/
int BNL_fei_yoda_rtp_fill_prop(METHOD_message_t *message, va_list args)
{
  int iRetCode        = 0;
  int i               = 0;

  char *pszPropValue  = NULL;
  char *pszPropName   = NULL;
  char *pszPref       = NULL;
  char *pszPrefSplitted= NULL;
  char *pszTemp       = NULL;

  char **pszValue     = NULL;

  tag_t tProp         = NULLTAG;
  tag_t tObject       = NULL_TAG;

  char szObjectClass[32+1];
  char szKey[128 + 1];
  char szObjType[256];


  tProp = va_arg(args, tag_t);
  pszValue = va_arg(args, char**);


  iRetCode = PROP_ask_name(tProp, &pszPropName);
  if (BNL_em_error_handler("PROP_ask_name", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  //fprintf(feilog, "%s: Property name [%s].\n", BNL_module, pszPropName);

  iRetCode = PROP_ask_owning_object(tProp, &tObject);
  if (BNL_em_error_handler("PROP_ask_owning_object", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  // Check the class
  iRetCode = BNL_tb_determine_super_class(tObject, szObjectClass);
  if (BNL_em_error_handler("BNL_tb_determine_super_class", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  //fprintf(feilog, "%s: Object is of class [%s].\n", BNL_module, szObjectClass);

  if (strcmp(szObjectClass, "ItemRevision") == 0 || strcmp(szObjectClass, "Item") == 0)
  {
    if (strcmp(szObjectClass, "ItemRevision") == 0)
    {
      // Get the Item
      iRetCode = ITEM_ask_item_of_rev(tObject, &tObject);
      if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;
    }

    // Get the type of the Item
    iRetCode = BNL_tb_determine_object_type(tObject, szObjType);
    if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    // Checking for the preference
    sprintf(szKey, "FEI_RTP_name_%s_%s", pszPropName, szObjType);
    //fprintf(feilog, "%s: Looking for preference [%s].\n", BNL_module, szKey);

    BNL_tb_pref_ask_char_value(szKey, IMAN_preference_site, &pszPref);

    if (pszPref == NULL)
    {
      sprintf(szKey, "FEI_RTP_name_%s_*", pszPropName);
      //fprintf(feilog, "%s: Looking for preference [%s].\n", BNL_module, szKey);

      BNL_tb_pref_ask_char_value(szKey, IMAN_preference_site, &pszPref);
    }

    if (pszPref != NULL)
    {
      char *pszDummy        = NULL;
      char *pszProperty     = NULL;

      //fprintf(feilog, "%s: Value [%s].\n", BNL_module, pszPref);

      pszProperty = pszPref;

      while ((pszDummy = strchr(pszProperty, '-')) != NULL)
      {
        *pszDummy = '\0';

        //fprintf(feilog, "Retrieving property [%s]\n", pszProperty);

        // Get and add property
        BNL_fei_yoda_add_propvalue(tObject, pszProperty, &pszPropValue);

        *pszDummy = '-';
        pszProperty = pszDummy + 1;
      } // End of while ((pszDummy = strchr(pszProperty, '-')) != NULL)
      // Get and add property
      BNL_fei_yoda_add_propvalue(tObject, pszProperty, &pszPropValue);
    } // End of if (pszPref != NULL)
  } // End  of if (strcmp(szObjectClass, "ItemRevision") == 0 || strcmp(szObjectClass, "Item") == 0)

  if (pszPropValue == NULL)
  {
    iRetCode = BNL_tb_get_property(tObject, "object_string", &pszPropValue);
    if (BNL_em_error_handler("BNL_tb_get_property", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;
  }

  //fprintf(feilog, "%s: Value will be set to [%s].\n\n", BNL_module, pszPropValue);

  *pszValue = (char * ) MEM_alloc( sizeof(char) * ((int)strlen(pszPropValue) + 1));
  strcpy(*pszValue, pszPropValue);

function_exit:

  if (pszPropValue != NULL) MEM_free(pszPropValue);
  if (pszPropName != NULL) MEM_free(pszPropName);
  if (pszPref != NULL) MEM_free(pszPref);
  if (pszPrefSplitted != NULL) free(pszPrefSplitted);

  return iRetCode;
}

int BNL_fei_yoda_rtp_bvr_fill_prop(METHOD_message_t *message, va_list args)
{
  int iRetCode        = 0;

  char **pszValue     = NULL;

  tag_t tProp         = NULL_TAG;
  tag_t tObject       = NULL_TAG;
  tag_t tBomView      = NULL_TAG;
  tag_t tItem         = NULL_TAG;
  tag_t tRev          = NULL_TAG;

  char szObjType[256] = { "" };


  tProp = va_arg(args, tag_t);
  pszValue = va_arg(args, char**);


  iRetCode = PROP_ask_owning_object(tProp, &tObject);
  if (BNL_em_error_handler("PROP_ask_owning_object", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  // Get the bomview
  iRetCode = PS_ask_bom_view_of_bvr(tObject, &tBomView);
  if (BNL_em_error_handler("PS_ask_bom_view_of_bvr", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  if (tBomView != NULL_TAG)
  {
    // Get the Item
    iRetCode = PS_ask_item_of_bom_view(tBomView, &tItem);
    if (BNL_em_error_handler("PS_ask_item_of_bom_view", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    if (tItem != NULL_TAG)
    {
      iRetCode = ITEM_ask_latest_rev(tItem, &tRev);
      if (BNL_em_error_handler("ITEM_ask_latest_rev", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

      if (tRev != NULL_TAG)
      {
        iRetCode = BNL_tb_determine_object_type(tRev, szObjType);
        if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;
      }
      else
      {
        fprintf(feilog, "%s: Failed to get the Rev of the Item.\n", BNL_module);
      }
    }
    else
    {
      fprintf(feilog, "%s: Failed to get the Item of the Bomview.\n", BNL_module);
    }
  }
  else
  {
    fprintf(feilog, "%s: Failed to get the Bomview of the BVR.\n", BNL_module);
  }

  *pszValue = (char * ) MEM_alloc( sizeof(char) * ((int)strlen(szObjType) + 1));
  strcpy(*pszValue, szObjType);

function_exit:

  return iRetCode;
}

/**
int BNL_fei_yoda_rtp_property(METHOD_message_t *message, va_list args)

  Description:

  Get and add the values to the runtime property.

*/
int BNL_fei_yoda_add_propvalue(tag_t tObject, char *pszProperty, char **pszPropValue)
{
  int iRetCode      = 0;

  char *pszTemp     = NULL;
  char *pszValue    = NULL;


  pszValue = *pszPropValue;

  iRetCode = BNL_tb_get_property(tObject, pszProperty, &pszTemp);
  if (iRetCode != 0)
  {
    EMH_clear_errors();
    iRetCode = 0;

    //fprintf(feilog, "%s: Skipping [%s]...\n", BNL_module, pszProperty);
  }
  else
  {
    //fprintf(feilog, "%s: Property [%s] has value [%s].\n", BNL_module, pszProperty, pszTemp);

    if (pszValue == NULL)
    {
      pszValue = (char * ) MEM_alloc( sizeof(char) * 1);
      strcpy(pszValue, "");
    }

    if (strlen(pszTemp) > 0)
    {
      if (strlen(pszValue) == 0)
      {
        pszValue = (char *)MEM_realloc(pszValue, ((int)strlen(pszTemp) + 1));
        sprintf(pszValue, "%s", pszTemp);
      }
      else
      {
        pszValue = (char *)MEM_realloc(pszValue, ((int)strlen(pszValue) + (int)strlen(pszTemp) + 2));
        sprintf(pszValue, "%s-%s", pszValue, pszTemp);
      }
    }
  }

  if (pszTemp != NULL) MEM_free(pszTemp);

  *pszPropValue = pszValue;

  return iRetCode;
}


int BNL_fei_yoda_check_bom(METHOD_message_t *pmMessage, va_list args)
{
	int iRetCode				      = 0;
  int iPrefCount            = 0;

  tag_t tBomwindow          = NULL_TAG;
  tag_t tTopline            = NULL_TAG;

  tag_t *ptChilds           = NULL;

  char **pszPrefValues      = NULL;

  // Debug
  clock_t start, finish;
  double  duration;


  fprintf(feilog, "*** In BNL_fei_yoda_check_bom.\n");

  start = clock();

  tBomwindow = va_arg(args, tag_t);

  //fprintf(feilog, "DEBUG: Bomwindow [%d]\n", tBomwindow);

  iRetCode = PREF_set_search_scope(IMAN_preference_site);
  if (BNL_em_error_handler("PREF_set_search_scope", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  // Get the preference
  iRetCode = BNL_tb_pref_ask_char_values(BNL_BOM_VAL_PREF, IMAN_preference_site, &iPrefCount, &pszPrefValues);
  if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  if (iPrefCount == 0)
  {
    fprintf(feilog, "%s: Warning, preference [%s] has not been defined.\n", BNL_module, BNL_BOM_VAL_PREF);
    goto function_exit;
  }

  iRetCode = BOM_ask_window_top_line(tBomwindow, &tTopline);
  if (BNL_em_error_handler("BOM_ask_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  //fprintf(feilog, "DEBUG: tTopline [%d]\n", tTopline);

  // JM|12-08-2011: Workaround for IR 1875690
  if (tTopline != NULL_TAG)
  {
    if ((iRetCode = BNL_fei_yoda_check_bomlines(tTopline, iPrefCount, pszPrefValues)) != 0) goto function_exit;
  }


function_exit:

  finish = clock();
  duration = (double)(finish - start) / CLOCKS_PER_SEC;

  fprintf(feilog, "Time needed for checking: %2.1f seconds.\n", duration);

  /*if (iRetCode == 0)
  {
    *((logical*)pReturnValue) = true;
  }
  else
  {
    *((logical*)pReturnValue) = false;
  }*/

  if (pszPrefValues) MEM_free(pszPrefValues);

	fprintf(feilog,"*** End BNL_fei_yoda_check_bom.\n\n");

	return iRetCode;
}


/**
int BNL_fei_yoda_check_bom(void * pReturnValue)

  Description:

  Check Bom

*/
int BNL_fei_yoda_check_bomlines(tag_t tTopline, int iPrefCount, char **pszPrefValues)
{
  int iRetCode            = 0;
  int iChilds             = 0;
  int i                   = 0;
  int iAttrBvrId          = 0;

  tag_t tBvr              = NULL_TAG;
  tag_t tProp             = NULL_TAG;

  tag_t *ptChilds         = NULL;

  char *pszError          = NULL;
  char *pszTopLevelOnly   = NULL;

  logical lIsBvrChanged   = false;

  logical lOneLevelCheck  = false;


  iRetCode = PREF_set_search_scope(IMAN_preference_site);
  if (BNL_em_error_handler("PREF_set_search_scope", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  // Get the preference
  iRetCode = BNL_tb_pref_ask_char_value(BNL_BOM_VAL_PREF_TOPLEVEL, IMAN_preference_site, &pszTopLevelOnly);
  if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  if (pszTopLevelOnly != NULL && _stricmp(pszTopLevelOnly, "true") == 0)
  {
    fprintf(feilog, "%s: Only top level will be checked.\n", BNL_module);
    lOneLevelCheck = true;
  }
  else
  {
    //fprintf(feilog, "%s: All levels will be checked.\n", BNL_module);
    lOneLevelCheck = false;
  }


  // Will be used for getting the bvr
  iRetCode = BOM_line_look_up_attribute(bomAttr_lineBvrTag, &iAttrBvrId);
  if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  // Get the child lines
  iRetCode = BOM_line_ask_child_lines(tTopline, &iChilds, &ptChilds);
  if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  //fprintf(feilog, "%s: Top line has childs [%d]\n", BNL_module, iChilds);

  if (iChilds > 0)
  {
    iRetCode = BOM_line_ask_attribute_tag(tTopline, iAttrBvrId, &tBvr);
    if (BNL_em_error_handler("BOM_line_ask_attribute_tag", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    iRetCode = PROP_ask_property_by_name(tBvr, "struct_changed", &tProp);
    if (BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    iRetCode = PROP_ask_value_logical(tProp, &lIsBvrChanged);
    if (BNL_em_error_handler("PROP_ask_value_logical", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    for (i = 0; i < iChilds; i++)
    {
      if (lIsBvrChanged)
      {
        fprintf(feilog, "%s: Structure has changed, checking...\n", BNL_module);

        if ((iRetCode = BNL_fei_yoda_check(tTopline, ptChilds[i], iPrefCount, pszPrefValues, &pszError)) != 0
          || pszError != NULL) goto function_exit;
      }
      //else
      //{
        //fprintf(feilog, "%s: Structure has not changed, no need to check.\n", BNL_module);
      //} // End of if (lIsBvrChanged)

      if (!lOneLevelCheck)
      {
        /* Recursive call */
        if ((iRetCode = BNL_fei_yoda_check_bomlines(ptChilds[i], iPrefCount, pszPrefValues)) != 0) goto function_exit;
      }
    } // End of for (i = 0; i < iChilds; i++)
  } // End of if (iChilds > 0)

function_exit:

  if (pszError != NULL)
  {
    // No save allowed
    iRetCode = BNL_error_message + giErrorOffset;
    BNL_em_error_handler_s1(NULL, BNL_module, EMH_severity_error, iRetCode, true, pszError);
  }

  if (pszError) free(pszError);
  if (pszTopLevelOnly) MEM_free(pszTopLevelOnly);
  if (iChilds > 0) MEM_free(ptChilds);

  return iRetCode;
}

/**
int BNL_fei_check_attr_value
(
  tag_t ItemRevision     <I>
  char *pszName          <I>
  char *pszValue         <I>
)

  Description:
    Checks if the attribute value

  Returns:

    -1 => match
    0 => no match
    !ITK_ok

*/
int BNL_fei_check_attr_value(tag_t tRev, char *pszName, char *pszValue)
{
  int iRetCode      = 0;
  int iRels         = 0;
  int iRelsItem     = 0;

  tag_t tRel        = NULL_TAG;
  tag_t tSourceProp = NULL_TAG;
  tag_t tItem       = NULL_TAG;
  tag_t tIMF        = NULL_TAG;
  tag_t tIRMF       = NULL_TAG;

  tag_t *ptRels     = NULL;
  tag_t *ptRelsItem = NULL;

  char *pszAttrValue=NULL;

  // Assumed it's on the ItemRevision Master Form or Item Master Fom
  iRetCode = GRM_find_relation_type("IMAN_master_form", &tRel);
  if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = GRM_list_secondary_objects_only(tRev, tRel, &iRels, &ptRels);
  if (BNL_em_error_handler("GRM_list_secondary_objects_only", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  if (iRels == 1) tIRMF = ptRels[0];

  iRetCode = ITEM_ask_item_of_rev(tRev, &tItem);
  if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = GRM_list_secondary_objects_only(tItem, tRel, &iRelsItem, &ptRelsItem);
  if (BNL_em_error_handler("GRM_list_secondary_objects_only", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  if (iRelsItem == 1) tIMF = ptRelsItem[0];

  // First check the IRMF
  PROP_ask_property_by_name(tIRMF, pszName, &tSourceProp);
  EMH_clear_errors();

  if (tSourceProp == NULL_TAG)
  {
    fprintf(feilog, "Property [%s] not on IRMF, checking IMF\n", pszName);
    PROP_ask_property_by_name(tIMF, pszName, &tSourceProp);
  }

  if (tSourceProp != NULL_TAG)
  {
    // Check the attribute
    iRetCode = PROP_UIF_ask_value(tSourceProp, &pszAttrValue);
    if (BNL_em_error_handler("PROP_UIF_ask_value", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    if (strcmp(pszValue, pszAttrValue) == 0)
    {
      // Match
      fprintf(feilog, "%s: Match! Value [%s] of attribute [%s] matches [%s].\n", BNL_module, pszAttrValue, pszName, pszValue);
      iRetCode = -1;
    }
    else
    {
      fprintf(feilog, "%s: No match! Value [%s] of attribute [%s] does not match [%s].\n", BNL_module, pszAttrValue, pszName, pszValue);
      iRetCode = 0;
    }
  }
  else
  {
    fprintf(feilog, "%s: Warning, failed to check the attribute [%s].\n", BNL_module, pszName);
    iRetCode = 0;
  }

function_exit:

  if (iRels > 0) MEM_free(ptRels);
  if (iRelsItem > 0) MEM_free(ptRelsItem);
  if (pszAttrValue) MEM_free(pszAttrValue);

  return iRetCode;
}


/**
int BOM_fei_yoda_get_bomline_info
(
  tag_t tLine,          <I>
  tag_t *ptRev,         <O>
  char **pszItemId,     <OF>
  char **pszType        <OF>
)

  Description:
    Get the information from the given bomline

  Returns:

    ITK_ok or !ITK_ok

*/
int BOM_fei_yoda_get_bomline_info
(
  tag_t tLine,
  tag_t *ptRev,
  char **pszItemId,
  char **pszType
)
{
  int iRetCode            = 0;
  int iAttrRevId          = 0;
  int iAttrItemId         = 0;
  int iAttrTypeId         = 0;


  // rev
  iRetCode = BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId);
  if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BOM_line_ask_attribute_tag(tLine, iAttrRevId, ptRev);
  if (BNL_em_error_handler("BOM_line_look_up_string", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  // item pszId
  iRetCode = BOM_line_look_up_attribute(bomAttr_itemId, &iAttrItemId);
  if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BOM_line_ask_attribute_string(tLine, iAttrItemId, pszItemId);
  if (BNL_em_error_handler("BOM_line_look_up_string", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  // type
  iRetCode = BOM_line_look_up_attribute(bomAttr_itemType, &iAttrTypeId);
  if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BOM_line_ask_attribute_string(tLine, iAttrTypeId, pszType);
  if (BNL_em_error_handler("BOM_line_look_up_string", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  return iRetCode;
}


int BNL_fei_yoda_check
(
  tag_t tParent,
  tag_t tChild,
  int iPrefCount,
  char **pszPrefValues,
  char **pszError
)
{
  int iRetCode              = 0;
  int iCheck                = 0;
  int i                     = 0;

  tag_t tParentRev          = NULL_TAG;
  tag_t tChildRev           = NULL_TAG;

  char *pszPrefCI           = NULL;
  char *pszPrefSI          	= NULL;
  char *pszParentTypeCI    	= NULL;
  char *pszChildTypeCI    	= NULL;
  char *pszParentTypeSI    	= NULL;
  char *pszChildTypeSI    	= NULL;
  char *pszAttrName    	    = NULL;
  char *pszAttrValue    	  = NULL;

  char *pszParentId         = NULL;
  char *pszChildId          = NULL;
  char *pszTypeParent       = NULL;
  char *pszTypeChild        = NULL;

  char szTemp[256];

  logical lNotMatch          = false;


  if (iPrefCount >0)
  {
    iRetCode = BOM_fei_yoda_get_bomline_info(tParent, &tParentRev, &pszParentId, &pszTypeParent);
    if (iRetCode != 0) goto function_exit;

    fprintf(feilog, "%s: Parent [%d] has ID [%s] and is of type [%s].\n",
      BNL_module, tParentRev, pszParentId, pszTypeParent);

    iRetCode = BOM_fei_yoda_get_bomline_info(tChild, &tChildRev, &pszChildId, &pszTypeChild);
    if (iRetCode != 0) goto function_exit;

    fprintf(feilog, "%s: Child [%d] has ID [%s] and is of type [%s].\n",
      BNL_module, tChildRev, pszChildId, pszTypeChild);
  }

  for (i=0; i<iPrefCount && lNotMatch == false; i++)
  {
    pszPrefCI = BNL_strings_copy_string(pszPrefValues[i]);
    // Split the peference value
    pszParentTypeCI = strtok(pszPrefCI, ".");
    pszChildTypeCI = strtok(NULL, ".");
    pszAttrName = strtok(NULL, ".");
    pszAttrValue	= strtok(NULL, ".");

    if ((pszParentTypeCI == NULL) || (pszChildTypeCI == NULL))
    {
      fprintf(feilog, "%s: Warning, value [%s] of preference [%s] has not been defined correctly (<parent type>.<child type>.<attribute>.<value> or <parent type>.<child type>).\n",
        BNL_module, pszPrefCI, BNL_BOM_VAL_PREF);
      continue;
    }

    //fprintf(feilog, "%s: Pref value %d:\n", BNL_module, (i  + 1));
    //fprintf(feilog, "\tParent Type [%s]\n", pszParentTypeCI);
    //fprintf(feilog, "\tChild Type [%s]\n", pszChildTypeCI);
    //fprintf(feilog, "\tAttribute Name [%s]\n", pszAttrName);
    //fprintf(feilog, "\tAttribute Value [%s]\n", pszAttrValue);

    if ((strcmp(pszTypeChild, pszChildTypeCI) == 0) && (strcmp(pszTypeParent, pszParentTypeCI) == 0))
    {
      if (pszAttrName == NULL && pszAttrValue == NULL)
      {
        sprintf(szTemp, "Parent of type [%s] with ID [%s] has child of type [%s] with ID [%s]. Not allowed.", pszTypeParent, pszParentId, pszTypeChild, pszChildId);

        fprintf(feilog, "%s: %s\n", BNL_module, szTemp);

        if (*pszError == NULL)
        {
          *pszError = (char *)malloc(sizeof(char) * strlen(szTemp) + 1);
          strcpy(*pszError, szTemp);
        }
        else
        {
          *pszError = (char *)realloc(*pszError, (strlen(*pszError) + strlen(szTemp) + 3));
          sprintf(*pszError, "%s\n%s", *pszError, szTemp);
        }

        //lNotMatch = true;
      }
      else
      {
        // Check the attribute of the child
        iCheck = BNL_fei_check_attr_value(tChildRev, pszAttrName, pszAttrValue);
        if (iCheck > 0) // Error
        {
          iRetCode = iCheck;
          goto function_exit;
        }

        if (iCheck == -1) // match => not allowed
        {
          sprintf(szTemp, "The attribute [%s] of child of type [%s] with ID [%s] has value [%s]. Not allowed.",
            pszAttrName,pszTypeChild, pszChildId, pszAttrValue);

          fprintf(feilog, "%s: %s\n", BNL_module, szTemp);

          if (*pszError == NULL)
          {
            *pszError = (char *)malloc(sizeof(char) * strlen(szTemp) + 1);
            strcpy(*pszError, szTemp);
          }
          else
          {
            *pszError = (char *)realloc(*pszError, (strlen(*pszError) + strlen(szTemp) + 3));
            sprintf(*pszError, "%s\n%s", *pszError, szTemp);
          }
         // lNotMatch = true;
        }
        else
        {
          // Check the attribute of the parent
          iCheck = BNL_fei_check_attr_value(tParentRev, pszAttrName, pszAttrValue);
          if (iCheck > 0) // Error
          {
            iRetCode = iCheck;
            goto function_exit;
          }

          if (iCheck == 0) // no match => not allowed
          {
            sprintf(szTemp, "The attribute [%s] of parent of type [%s] with ID [%s] does not match value [%s].",
            pszAttrName, pszTypeParent, pszParentId, pszAttrValue);

            fprintf(feilog, "%s: %s Not allowed.\n", BNL_module, szTemp);

            if (*pszError == NULL)
            {
              *pszError = (char *)malloc(sizeof(char) * strlen(szTemp) + 1);
              strcpy(*pszError, szTemp);
            }
            else
            {
              *pszError = (char *)realloc(*pszError, (strlen(*pszError) + strlen(szTemp) + 3));
              sprintf(*pszError, "%s\n%s", *pszError, szTemp);
            }
            //lNotMatch = true;
          }
        }
      }
    }
  }

function_exit:

  if (pszPrefCI) free(pszPrefCI);
  if (pszPrefSI) free(pszPrefSI);

  if (pszParentId) MEM_free(pszParentId);
  if (pszChildId) MEM_free(pszChildId);
  if (pszTypeParent) MEM_free(pszTypeParent);
  if (pszTypeChild) MEM_free(pszTypeChild);

  return iRetCode;
}

int BNL_fei_yoda_add_to_list(void * pReturnValue)
{
	int iRetCode				      = 0;

  tag_t tComp               = NULL_TAG;

  fprintf(feilog, "*** In BNL_fei_add_to_list.\n");

  iRetCode = USERARG_get_tag_argument(&tComp);
	if (BNL_em_error_handler("USERARG_get_tag_argument", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  giComps++;
  gptComps = MEM_realloc(gptComps, sizeof(tag_t) * giComps);
  gptComps[giComps - 1] = tComp;

  fprintf(feilog, "*** Component [%d] has been added.\n", tComp);


function_exit:


	fprintf(feilog,"*** End BNL_fei_yoda_add_to_list.\n\n");

	return iRetCode;
}

int BNL_fei_yoda_remove_from_list(void * pReturnValue)
{
	int iRetCode				      = 0;
  int iCompTemp             = 0;
  int i                     = 0;

  tag_t tComp               = NULL_TAG;

  tag_t *ptCompTemp        = 0;

  fprintf(feilog, "*** In BNL_fei_yoda_remove_from_list.\n");

  iRetCode = USERARG_get_tag_argument(&tComp);
	if (BNL_em_error_handler("USERARG_get_tag_argument", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  if (tComp != NULL_TAG)
  {
    fprintf(feilog, "*** Number of components in list before removal [%d].\n", giComps);

    for (i = 0; i < giComps; i++)
    {
      if (tComp != gptComps[i])
      {
        iCompTemp++;
        ptCompTemp = MEM_realloc(ptCompTemp, sizeof(tag_t) * iCompTemp);
        ptCompTemp[iCompTemp - 1] = gptComps[i];
      }
      else
      {
        fprintf(feilog, "*** Component [%d] has been removed.\n", tComp);
      }
    }

    if (giComps > 0)
    {
      MEM_free(gptComps);
      giComps = 0;
      gptComps = NULL;
    }

    giComps = iCompTemp;
    gptComps = ptCompTemp;

    fprintf(feilog, "*** Number of components in list after removal [%d].\n", giComps);
  }

  // Reset fru
  gtFruRev = NULL_TAG;

  if (gpsListPortal != NULL)
  {
    // Clear memory
    for (i = 0; i < gpsListPortal->iObjects; i++)
    {
      free(gpsListPortal->pszNames[i]);
      gpsListPortal->pszNames[i] = NULL;
    }
    MEM_free(gpsListPortal->pszNames);
    gpsListPortal->pszNames = NULL;
    MEM_free(gpsListPortal->ptImfs);
    gpsListPortal->ptImfs = NULL;
    MEM_free(gpsListPortal);
    gpsListPortal = NULL;
  }

  BNL_FEI_empty_form_list();



function_exit:


	fprintf(feilog,"*** End BNL_fei_yoda_remove_from_list.\n\n");

	return iRetCode;
}


int BNL_FEI_yoda_create_predecessor(void * pReturnValue)
{
	int iRetCode				      = 0;
  int i                     = 0;

  tag_t tNewRev             = NULL_TAG;
  tag_t tOldRev             = NULL_TAG;


  fprintf(feilog, "*** In BNL_FEI_yoda_create_predecessor.\n");

  iRetCode = USERARG_get_tag_argument(&tNewRev);
	if (BNL_em_error_handler("USERARG_get_tag_argument", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;


  fprintf(feilog, "%s: Checking if predecessor should be created.\n", BNL_module);
  // Predecessor
  iRetCode = WSOM_ask_based_on(tNewRev, &tOldRev);
  if (BNL_em_error_handler("WSOM_ask_based_on", BNL_module, EMH_severity_user_error, iRetCode, true)) return iRetCode;

  // Check it is in the list
  for (i = 0; i < giComps; i++)
  {
    fprintf(feilog, "gptComps[i] [%d]\n", gptComps[i]);
    if (tOldRev == gptComps[i])
    {
      fprintf(feilog, "%s: Creating predecessor.\n", BNL_module);

      if ((iRetCode = BNL_assign_predeccesor_relation(tNewRev, tOldRev)) != 0) goto function_exit;

      break;
    }
  }

function_exit:

	fprintf(feilog,"*** End BNL_FEI_yoda_create_predecessor.\n\n");

	return iRetCode;
}


int BNL_FEI_check_for_csp(void * pReturnValue)
{
	int iRetCode				      = 0;
  int i                     = 0;

  tag_t tItem               = NULL_TAG;
  tag_t tItemRev            = NULL_TAG;

  fprintf(feilog, "*** In BNL_FEI_check_for_csp.\n");


  if (gpsListPortal != NULL)
  {
    for (i = 0; i < gpsListPortal->iObjects ; i++)
    {
      // Get belonging Item
      iRetCode = AOM_ask_value_tag(gpsListPortal->ptImfs[i], "item_for_form", &tItem);
      if (BNL_em_error_handler("AOM_ask_value_tag", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

      if (tItem != NULL_TAG)
      {
        iRetCode = ITEM_ask_latest_rev(tItem, &tItemRev);
        if (BNL_em_error_handler("ITEM_ask_latest_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
      }

      if (tItemRev != NULL_TAG)
      {
        gpsListPortal->ptImfs[i] = NULL_TAG;

        fprintf(feilog, "%s: Csp needs to be created, prop/type [%s] (1).\n", BNL_module, gpsListPortal->pszNames[i]);

        // Do the thing
        if ((iRetCode = BNL_FEI_create_plot_request(tItemRev, gpsListPortal->pszNames[i], 0, NULL)) != 0) goto function_exit;
      } // End of if (tItemRev != NULL_TAG)
    } // End of for (i = 0; i < gpsListPortal->iObjects; i++)
  } // End of if (gpsListPortal != NULL)

  if (gtFruRev != NULL_TAG)
  {
    fprintf(feilog, "Creating csp file for FRU Item.\n");

    if ((iRetCode = BNL_FEI_create_plot_request(gtFruRev, "FRU", 0, NULL)) != 0) goto function_exit;

    gtFruRev = NULL_TAG;
  }


function_exit:

	fprintf(feilog,"*** End BNL_FEI_check_for_csp.\n\n");

	return iRetCode;
}




/**
int BNL_fei_yoda_check_uom
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:
    Checks the selected UOM value is valid for this item type

  Returns:

    ITK_ok or !ITK_ok

*/
int BNL_fei_yoda_check_uom
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;
  int iLOV          = 0;
  int iLOVvals      = 0;
  int i             = 0;

  char szObjType[256]  ={""};
  char szPrefName[256] ={""};
  char szMessg[1024]   = "";

  char *pszUOMLOV  = NULL;
  char *pszValue   = NULL;

  char **pptLOVvals = NULL;

  tag_t tItem = NULL_TAG;

  tag_t *pptLOV = NULL;

  logical lFound = false;
  logical lHasBypass = false;
  logical lAdmin = false;

  fprintf(feilog, "%s: Begin ... BNL_FEI_check_uom \n", BNL_module);

  tItem = va_arg(args, tag_t);    /* Object */
  //fprintf(feilog, "tItemForm tag [%d].\n", tItemForm);

  POM_is_user_sa(&lAdmin);
  ITK_ask_bypass(&lHasBypass);
  if (lAdmin && lHasBypass)
  {
    fprintf(feilog, "%s: Warning, user has bypass, checking of UOM is being skipped.\n", BNL_module);
    goto function_exit;
  }

  //PREF_set_search_scope(IMAN_preference_site);

  iRetCode = BNL_tb_determine_object_type(tItem, szObjType);
  if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) return false;

  sprintf(szPrefName,"FEI_%s_UOM_LOV",szObjType);

  BNL_tb_pref_ask_char_value(szPrefName, IMAN_preference_site, &pszUOMLOV);
  if (pszUOMLOV == NULL)
  {
    fprintf(feilog, "%s: Preference [%s] is not defined. No UOM validation!\n", BNL_module, szPrefName);
  }
  else
  {
    fprintf(feilog, "%s: Preference [%s] has value [%s].\n", BNL_module, szPrefName, pszUOMLOV);

    iRetCode = LOV_find(pszUOMLOV, &iLOV, &pptLOV);
    if (iLOV == 0)
    {
      fprintf(feilog, "%s: List of value with name [%s] is not found.\n", BNL_module, pszUOMLOV);
      goto function_exit;
    }

    iRetCode = LOV_ask_values_string(pptLOV[0],&iLOVvals,&pptLOVvals);
    //fprintf(feilog, "DEBUG: [%d].\n", iLOVvals);
    if (iLOVvals == 0)
    {
      fprintf(feilog, "%s: List of value with name [%s] has no values.\n", BNL_module, pszUOMLOV);
      iRetCode = 0;
      EMH_clear_errors();
      goto function_exit;
    }

    iRetCode = AOM_UIF_ask_value(tItem, BNL_FEI_UOM, &pszValue);
    if (iRetCode != 0)
    {
      fprintf(feilog, "%s: Warning, unable to check the current UOM value. Ignoring...\n", BNL_module, pszValue);
      EMH_clear_errors();
      iRetCode = 0;
    }
    fprintf(feilog, "%s: Current UOM value [%s].\n", BNL_module, pszValue);

    if (pszValue != NULL && strcmp(pszValue, "each") != 0 && strlen(pszValue) > 0)
    {
      for (i=0; i<iLOVvals && lFound == false; i++)
      {
        if (_stricmp(pszValue, pptLOVvals[i]) == 0)
        {
          fprintf(feilog, "%s: Value [%s] is allowed.\n", BNL_module, pszValue);
          lFound = true;
        }
      }

      if (lFound == false)
      {
        fprintf(feilog, "%s: The selected value [%s] for UOM is not valid for item type [%s].\n",BNL_module, pszValue,szObjType);

        sprintf(szMessg, "The selected value [%s] for UOM is not valid for item type [%s].\n",pszValue,szObjType);

        iRetCode = BNL_UOM_NOT_ALLOWED;
        BNL_em_error_handler_s1(NULL, BNL_module, EMH_severity_error, iRetCode, true, szMessg);
      }
    }
  }

function_exit:

  if (pszUOMLOV != NULL) MEM_free(pszUOMLOV);
  if (pszValue != NULL) MEM_free(pszValue);

  fprintf(feilog, "%s: End ... BNL_FEI_check_uom \n", BNL_module);
  fprintf(feilog, "\n");

  return iRetCode;
}


// Taken from BNL_register_feimeth.c

// Add flag if from nx
int BNL_assign_predeccesor_relation
(
  tag_t tSuccesor,
  tag_t tPredeccesor
)
{
  int iRetCode            = 0;
  int iFail               = 0;
  int iAttCount           = 0;
  int iCount              = 0;

  tag_t *ptPredecessors   = NULL;

  tag_t tRelType          = NULLTAG;
  tag_t tRelation         = NULLTAG;


  fprintf(feilog, "Enter BNL_assign_predeccesor_relation \n");

  iRetCode = GRM_find_relation_type(PREDECESSOR_RELATION, &tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;

  if (tRelType == NULLTAG)
  {
    fprintf(feilog, "Error, did not find the relation [%s] in the database\n", PREDECESSOR_RELATION);
    return 1;
  }

  iRetCode = GRM_list_secondary_objects_only(tSuccesor, tRelType, &iCount, &ptPredecessors);
  if (BNL_em_error_handler("GRM_list_secondary_objects_only", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  /*if there is already a predeccesor then cut out. This could happen because
    the new created succesor is a copy from its predeccsor.*/
  if (iCount > 0)
  {
    iRetCode = GRM_find_relation(tSuccesor, ptPredecessors[0], tRelType, &tRelation);
    if (BNL_em_error_handler("GRM_find_relation", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;


    iRetCode = GRM_delete_relation(tRelation);
    if (BNL_em_error_handler("GRM_delete_relation", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;


    tRelation = NULLTAG;
  }

  /*just to be secure check if the realtion not already exists*/
  iRetCode = GRM_find_relation(tSuccesor, tPredeccesor, tRelType, &tRelation);
  if (BNL_em_error_handler("GRM_find_relation", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  if (tRelation == NULLTAG)
  {
    /*create the predeccesor relation*/
    iRetCode = GRM_create_relation(tSuccesor, tPredeccesor, tRelType, NULLTAG, &tRelation);
    EMH_ask_last_error(&iFail);

    // For preventing that the error will be added once more to the stack
    if (iRetCode != 0 && iRetCode == iFail)
    {
      goto function_exit;
    }
    else
    {
      if (BNL_em_error_handler("GRM_create_relation", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;
    }

    iRetCode = GRM_save_relation(tRelation);
    if (BNL_em_error_handler("GRM_save_relation", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;
  }

function_exit:

  if (iCount > 0) MEM_free(ptPredecessors);

  fprintf(feilog, "Leave BNL_assign_predeccesor_relation \n");

  return iRetCode;
}


/**
int BNL_fei_yoda_deep_copy_post_action(METHOD_message_t *m, va_list args)

  Description:

  BNL_fei_yoda_deep_copy_post_action.

*/
/*
int BNL_fei_yoda_deep_copy_post_action(METHOD_message_t *m, va_list args)
{
  int iRetCode            = 0;
  int i                   = 0;

  // ITEM_deep_copy_msg paramseters
  // The tag of the newly created Item Revision
  tag_t tNewRev = va_arg(args, tag_t);

  // Whether the operation is Save As/Revise
  char *pszOperation = va_arg(args, char*);

  // The ItemRevision over which Save As/Revise pszOperation will be performed
  tag_t tOldRev = va_arg(args, tag_t);

  // Number of deep copy rules set
  // int *picCopyCount = va_arg(args, int*);

  // Deep Copy Rule Information
  // ITEM_deepcopy_info_t *copyInfo = va_arg(args, ITEM_deepcopy_info_t*);

  // Number of Datasets attached
  // int *piCount = va_arg(args, int*);

  // The tags of the deep copied attachments
  // tag_t **ptCopiedObjects = va_arg(args, tag_t**);

  // End of ITEM_deep_copy_msg paramseters


  fprintf(feilog, "\nStarting BNL_fei_yoda_deep_copy_post_action.\n");

  fprintf(feilog, "NewRev [%d]\n", tNewRev);
  fprintf(feilog, "OldRev [%d]\n", tOldRev);


  if (strcmp(pszOperation, "SaveAs") != 0)
  {
    goto function_exit;
  }


  // Check it is in the list
  for (i = 0; i < giComps; i++)
  {
    if (tOldRev == gptComps[i])
    {
      fprintf(feilog, "%s: Creating predecessor.\n", BNL_module);

      if ((iRetCode = BNL_assign_predeccesor_relation(tNewRev, tOldRev)) != 0) goto function_exit;

      break;
    }
  }

function_exit:

  fprintf(feilog, "End of BNL_fei_yoda_deep_copy_post_action.\n");

  return iRetCode;
}*/


// iMode:
// 0 => removal
// 1 => create
int BNL_fei_yoda_check_relation_access(tag_t tRelType, tag_t tPrimary, tag_t tSecondary, int iMode)
{
  int iRetCode        = 0;
  int iGroupRoles     = 0;
  int i               = 0;

  char szPrefAllowed[240 + 1];
  char szRelType[IMANTYPE_name_size_c + 1];
  char szRole[SA_name_size_c + 1];
  char szMessg[256];

  char *pszGroup      = NULL;
  char *pszTemp       = NULL;
  char *pszDispName   = NULL;

  char **pszGroupRoles= NULL;

  tag_t tGroup        = NULL_TAG;
  tag_t tRole         = NULL_TAG;
  tag_t tType         = NULL_TAG;
  tag_t tRelProp      = NULL_TAG;

  IMAN_preference_search_scope_t old_scope;

  logical lAllowed    = false;


  // Get the current group and role
  iRetCode = POM_ask_group(&pszTemp, &tGroup);
  if (BNL_em_error_handler("POM_ask_group", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = SA_ask_group_full_name(tGroup, &pszGroup);
  if (BNL_em_error_handler("SA_ask_group_full_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = SA_ask_current_role(&tRole);
  if (BNL_em_error_handler("SA_ask_current_role", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = SA_ask_role_name(tRole, szRole);
  if (BNL_em_error_handler("SA_ask_role_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Current user is of group and has role [%s].\n", BNL_module, pszGroup, szRole);

  iRetCode = IMANTYPE_ask_name(tRelType, szRelType);
  if (BNL_em_error_handler("IMANTYPE_ask_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Relation type [%s] to be updated.\n", BNL_module, szRelType);

  // Get the preference for allowed roles and groups
  sprintf(szPrefAllowed, "FEI_REL_%s_group_roles", szRelType);

  fprintf(feilog, "%s: Looking for preference [%s].\n", BNL_module, szPrefAllowed);

  PREF_ask_search_scope(&old_scope);
  PREF_set_search_scope(IMAN_preference_site);

  iRetCode = BNL_tb_pref_ask_char_values(szPrefAllowed, IMAN_preference_site, &iGroupRoles, &pszGroupRoles);
  if (BNL_em_error_handler("PREF_ask_char_values", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  PREF_set_search_scope(old_scope);

  for (i = 0; i < iGroupRoles; i++)
  {
    char *pszDummy   = NULL;
    char *pszStart   = NULL;

    fprintf(feilog, "%s: Preference value [%s].\n", BNL_module, pszGroupRoles[i]);

    pszStart = pszGroupRoles[i];

    if (strchr(pszStart, ':') != NULL)
    {
      pszDummy = strchr(pszStart, ':');
      *pszDummy = '\0';

      if (strcmp(pszStart, "*") != 0 && strcmp(pszStart, pszGroup) != 0)
      {
        fprintf(feilog, "%s: Current group [%s] does not match [%s].\n", BNL_module, pszGroup, pszStart);
        continue;
      }

      *pszDummy = ':';
      pszStart = pszDummy + 1;

      if (strcmp(pszStart, "*") != 0 && strcmp(pszStart, szRole) != 0)
      {
        fprintf(feilog, "%s: Current role [%s] does not match [%s].\n", BNL_module, szRole, pszStart);
      }
      else
      {
        fprintf(feilog, "%s: Current group and role match, allowed...\n", BNL_module);
        lAllowed = true;
        break;
      }
    }
  } // End of for (i = 0; i < iGroupRoles; i++)

  if (!lAllowed)
  {
    iRetCode = IMANTYPE_ask_object_type(tPrimary, &tType);
    if (BNL_em_error_handler("IMANTYPE_ask_object_type", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    iRetCode = IMANTYPE_ask_property_by_name(tType, szRelType, &tRelProp);
    if (BNL_em_error_handler("IMANTYPE_ask_property_by_name", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    iRetCode = PROPDESC_ask_display_name(tRelProp, &pszDispName);
    if (BNL_em_error_handler("PROPDESC_ask_display_name", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    if (iMode == 1) // Create
    {
      sprintf(szMessg, "Creation of relation [%s] is not allowed.", pszDispName);
    }
    else // Remove
    {
      sprintf(szMessg, "Removal of relation [%s] is not allowed.", pszDispName);
    }

    fprintf(feilog, "%s: Error, %s\n", BNL_module, szMessg);

    iRetCode = BNL_error_message + giErrorOffset;
    BNL_em_error_handler_s1(NULL, BNL_module, EMH_severity_error, iRetCode, true, szMessg);

    goto function_exit;
  }

function_exit:

  if (iGroupRoles > 0) MEM_free(pszGroupRoles);
  if (pszGroup) MEM_free(pszGroup);
  if (pszTemp) MEM_free(pszTemp);
  if (pszDispName) MEM_free(pszDispName);

  return iRetCode;
}


// iMode:
// 0 => removal
// 1 => create
int BNL_fei_yoda_update_relation(tag_t tRelType, tag_t tPrimary, tag_t tSecondary, int iMode)
{
  int iRetCode   = 0;
  int i          = 0;
  int iValues    = 0;
  int iExtraCols = 0;

  char szPrefRel[240 + 1];
  char szRelType[IMANTYPE_name_size_c + 1];
  char szPrimaryType[WSO_name_size_c + 1];
  char szSecondaryType[WSO_name_size_c + 1];
  char szId[ITEM_id_size_c + 1];

  char **pszPrefValues = NULL;
  char **pszExtraCols  = NULL;

  char *pszTempType = NULL;

  tag_t tRel     = NULL_TAG;
  tag_t tRev     = NULL_TAG;

  WSO_description_t tDescription;


  iRetCode = IMANTYPE_ask_name(tRelType, szRelType);
  if (BNL_em_error_handler("IMANTYPE_ask_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = WSOM_ask_object_type(tPrimary, szPrimaryType);
  if (BNL_em_error_handler("WSOM_ask_object_type (primary)", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  // Get the primary type
  iRetCode = WSOM_ask_object_type(tSecondary, szSecondaryType);
  if (BNL_em_error_handler("WSOM_ask_object_type (secondary)", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  // Get the preference
  sprintf(szPrefRel, "FEI_REL_%s_types", szRelType);

  fprintf(feilog, "%s: Looking for preference [%s].\n", BNL_module, szPrefRel);

  iRetCode = BNL_tb_pref_ask_char_values(szPrefRel, IMAN_preference_site, &iValues, &pszPrefValues);
  if (BNL_em_error_handler("PREF_ask_char_values", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  for (i = 0; i < iValues; i++)
  {
    char *pszDummy   = NULL;
    char *pszStart   = NULL;

    fprintf(feilog, "%s: Preference value [%s].\n", BNL_module, pszPrefValues[i]);

    pszStart = pszPrefValues[i];

    if (strchr(pszStart, ':') != NULL)
    {
      pszDummy = strchr(pszStart, ':');
      *pszDummy = '\0';

      // pszStart => primary type
      fprintf(feilog, "%s: Primary type [%s].\n", BNL_module, pszStart);

      // Do the check
      if (strcmp(pszStart, "*") != 0 && strcmp(pszStart, szPrimaryType) != 0)
      {
        fprintf(feilog, "%s: Primary type [%s] does not match [%s].\n", BNL_module, szPrimaryType, pszStart);
        continue;
      }

      *pszDummy = ':';
      pszStart = pszDummy + 1;

      if (strchr(pszStart, ':') != NULL)
      {
        pszDummy = strchr(pszStart, ':');
        *pszDummy = '\0';

        // pszStart => secondary type
        fprintf(feilog, "%s: Secondary type [%s].\n", BNL_module, pszStart);

        // Do the check
        if (strcmp(pszStart, "*") != 0 && strcmp(pszStart, szSecondaryType) != 0)
        {
          fprintf(feilog, "%s: Secondary type [%s] does not match [%s].\n", BNL_module, szSecondaryType, pszStart);
          continue;
        }
        *pszDummy = ':';
        pszStart = pszDummy + 1;

        fprintf(feilog, "%s: Types match...\n", BNL_module);

        iRetCode = GRM_find_relation_type(pszStart, &tRelType);
        if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        // Check owning site
        iRetCode = WSOM_describe(tSecondary, &tDescription);
        if (BNL_em_error_handler("WSOM_describe", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        if (iMode == 0) // Remove
        {
          if (tRelType != NULL_TAG)
          {
            if (tDescription.owning_site_name[0] != ' ')
            {
              fprintf(feilog, "%s: Owning site secondary item is [%s]. Creating csp file.\n", BNL_module, tDescription.owning_site_name);

              pszTempType = (char *)malloc(sizeof(char) * (strlen("REL_REMOVE_") + strlen(szRelType) + 1));
              sprintf(pszTempType, "REL_REMOVE_%s", szRelType);

              // Get the latest ItemRevision
              iRetCode = ITEM_ask_latest_rev(tPrimary, &tRev);
              if (BNL_em_error_handler("ITEM_ask_latest_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              iRetCode = ITEM_ask_id(tSecondary, szId);
              if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              iExtraCols = 2;
              pszExtraCols = (char **)malloc(sizeof(char*)*iExtraCols);
              BNL_tb_copy_string(szId, &pszExtraCols[0]);
              BNL_tb_copy_string(tDescription.owning_site_name, &pszExtraCols[1]);

              // Do the thing
              if ((iRetCode = BNL_FEI_create_plot_request(tRev, pszTempType, iExtraCols, pszExtraCols)) != 0) goto function_exit;
            } // End of if (tDescription.owning_site_name[0] != ' ')
            else
            {
              iRetCode = GRM_find_relation(tSecondary, tPrimary, tRelType, &tRel);
              if (BNL_em_error_handler("GRM_find_relation", BNL_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;

              //fprintf(feilog, "%s: tRel [%d].\n", BNL_module, tRel);

              if (tRel != NULL_TAG)
              {
                fprintf(feilog, "%s: Removing relation of type [%s].\n", BNL_module, pszStart);
                glDeleteFlag = true;
                iRetCode = GRM_delete_relation(tRel);
                glDeleteFlag = false;
                if (BNL_em_error_handler("GRM_delete_relation", BNL_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;
              } // End of if (tRel != NULL_TAG)
            } // End of Else if (tDescription.owning_site_name[0] != ' ')
          } // End of if (tRelType != NULL_TAG)
        }
        else // Create
        {
          if (tRelType != NULL_TAG)
          {
            if (tDescription.owning_site_name[0] != ' ')
            {
              fprintf(feilog, "%s: Owning site secondary item is [%s]. Csp file will be created.\n", BNL_module, tDescription.owning_site_name);

              pszTempType = (char *)malloc(sizeof(char) * (strlen("REL_CREATE_") + strlen(szRelType) + 1));
              sprintf(pszTempType, "REL_CREATE_%s", szRelType);

              // Get the latest ItemRevision
              iRetCode = ITEM_ask_latest_rev(tPrimary, &tRev);
              if (BNL_em_error_handler("ITEM_ask_latest_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              iRetCode = ITEM_ask_id(tSecondary, szId);
              if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              iExtraCols = 2;
              pszExtraCols = (char **)malloc(sizeof(char*)*iExtraCols);
              BNL_tb_copy_string(szId, &pszExtraCols[0]);
              BNL_tb_copy_string(tDescription.owning_site_name, &pszExtraCols[1]);

              // Do the thing
              if ((iRetCode = BNL_FEI_create_plot_request(tRev, pszTempType, iExtraCols, pszExtraCols)) != 0) goto function_exit;
            } // End of if (tDescription.owning_site_name[0] != ' ')
            else
            {
              glCreateFlag = true;
              iRetCode = GRM_create_relation(tSecondary, tPrimary, tRelType, NULL_TAG, &tRel);
              glCreateFlag = false;
              if (iRetCode == 35010)
              {
                fprintf(feilog, "%s: Relation [%s] already present, no need to create new one.\n", BNL_module, pszStart);
                EMH_clear_last_error(35010);
                iRetCode = 0;
              }
              else
              {
                if (BNL_em_error_handler("GRM_create_relation", BNL_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;
              }

              if (tRel != NULL_TAG)
              {
                fprintf(feilog, "%s: Create new relation of type [%s].\n", BNL_module, pszStart);

                iRetCode = GRM_save_relation(tRel);
                if (BNL_em_error_handler("GRM_save_relation", BNL_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;
              }
            } // End of Else if (tDescription.owning_site_name[0] != ' ')
          }
        }
        break;
      } // End of if (strchr(pszStart, ':') != NULL)
    } // End of if (strchr(pszStart, ':') != NULL)
  } // End of for (i = 0; i < iValues; i++)

function_exit:

  if (iValues > 0) MEM_free(pszPrefValues);
  for (i = 0; i < iExtraCols; i++)
    free(pszExtraCols[i]);
  if (iExtraCols > 0) free(pszExtraCols);

  if (pszTempType != NULL) free(pszTempType);

  return iRetCode;
}


int BNL_fei_yoda_grm_create_pre(METHOD_message_t *pmMessage, va_list args)
{
  int iRetCode      = 0;

  tag_t tPrimary    = NULL_TAG;
  tag_t tSecondary  = NULL_TAG;
  tag_t tRelType    = NULL_TAG;

  fprintf(feilog, "%s: Begin ... BNL_fei_yoda_grm_create_pre\n", BNL_module);

  if (glCreateFlag) goto function_exit;

  if (giSaveAs == 1)
  {
    fprintf(feilog, "%s: Relation is created during saveas, no check needed. It's assumed it's being removed during saveas.\n", BNL_module);
    goto function_exit;
  }

  tPrimary = va_arg(args, tag_t);    /* Primary object */
  tSecondary = va_arg(args, tag_t);  /* Secondary object */
  tRelType = va_arg(args, tag_t);    /* Relation type */

  if ((iRetCode = BNL_fei_yoda_check_relation_access(tRelType, tPrimary, tSecondary, 1)) != 0) goto function_exit;

function_exit:

  fprintf(feilog, "%s: End ... BNL_fei_yoda_grm_create_pre\n\n", BNL_module);

  return iRetCode;
}

int BNL_fei_yoda_grm_create_post(METHOD_message_t *pmMessage, va_list args)
{
  int iRetCode      = 0;

  tag_t tPrimary    = NULL_TAG;
  tag_t tSecondary  = NULL_TAG;
  tag_t tRelType    = NULL_TAG;

  fprintf(feilog, "%s: Begin ... BNL_fei_yoda_grm_create_post\n", BNL_module);

  if (glCreateFlag) goto function_exit;

  tPrimary = va_arg(args, tag_t);    /* Primary object */
  tSecondary = va_arg(args, tag_t);  /* Secondary object */
  tRelType = va_arg(args, tag_t);    /* Relation type */

  if ((iRetCode = BNL_fei_yoda_update_relation(tRelType, tPrimary, tSecondary, 1)) != 0) goto function_exit;


function_exit:

  fprintf(feilog, "%s: End ... BNL_fei_yoda_grm_create_post\n\n", BNL_module);

  return iRetCode;
}


int BNL_fei_yoda_grm_delete_pre(METHOD_message_t *pmMessage, va_list args)
{
  int iRetCode      = 0;

  tag_t tRelation   = NULL_TAG;
  tag_t tRelType    = NULL_TAG;
  tag_t tPrimary    = NULL_TAG;
  tag_t tSecondary  = NULL_TAG;

  fprintf(feilog, "%s: Begin ... BNL_FEI_grm_delete_pre\n", BNL_module);

  if (glDeleteFlag) goto function_exit;

  tRelation = va_arg(args, tag_t);    /* Relation object */


  // Get the relation type
  iRetCode = IMANTYPE_ask_object_type(tRelation, &tRelType);
  if (BNL_em_error_handler("IMANTYPE_ask_object_type", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  // Get the primary object
  iRetCode = GRM_ask_primary(tRelation, &tPrimary);
  if (BNL_em_error_handler("GRM_ask_primary", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  // Get the secondary objects
  iRetCode = GRM_ask_secondary(tRelation, &tSecondary);
  if (BNL_em_error_handler("GRM_ask_secondary", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  if (giSaveAs == 1)
  {
    fprintf(feilog, "%s: Relation is deleted during saveas, no need to check access.\n", BNL_module);
  }
  else
  {
    if ((iRetCode = BNL_fei_yoda_check_relation_access(tRelType, tPrimary, tSecondary, 0)) != 0) goto function_exit;
  }

  if ((iRetCode = BNL_fei_yoda_update_relation(tRelType, tPrimary, tSecondary, 0)) != 0) goto function_exit;

function_exit:

  fprintf(feilog, "%s: End ... BNL_FEI_grm_delete_pre\n\n", BNL_module);

  return iRetCode;
}


int BNL_fei_yoda_pre_saveas(METHOD_message_t *pmMessage, va_list args)
{
  int iRetCode      = 0;
  int iNum          = 0;

  tag_t tOldItem    = va_arg(args, tag_t);
  tag_t tOldRev     = va_arg(args, tag_t);

  tag_t *ptObjects  = NULL;


  fprintf(feilog, "** Begin ... BNL_fei_yoda_pre_saveas \n");

  giSaveAs = 1;
  gtIRMF = NULL_TAG;
  gtIMF = NULL_TAG;

  // Get the IMF and IRMF
  iRetCode = AOM_ask_value_tags(tOldItem, "IMAN_master_form", &iNum, &ptObjects);
  if (BNL_em_error_handler("AOM_ask_value_tags (IMF)", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  if (iNum > 0)
  {
    gtIMF = ptObjects[0];
    MEM_free(ptObjects);
    iNum = 0;
  }
  else
  {
    fprintf(feilog, "%s: Warning failed to find the IMF.\n");
  }

  iRetCode = AOM_ask_value_tags(tOldRev, "IMAN_master_form_rev", &iNum, &ptObjects);
  if (BNL_em_error_handler("AOM_ask_value_tag (IMRF)", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  if (iNum > 0)
  {
    gtIRMF = ptObjects[0];
  }
  else
  {
    fprintf(feilog, "%s: Warning failed to find the IRMF.\n");
  }


function_exit:

  if (iNum > 0) MEM_free(ptObjects);

  fprintf(feilog, "** End ... BNL_fei_yoda_pre_saveas \n");

  return iRetCode   = 0;
}

int BNL_fei_yoda_post_saveas(METHOD_message_t *pmMessage, va_list args)
{
  int iRetCode      = 0;

  fprintf(feilog, "** Begin ... BNL_fei_yoda_post_saveas \n");

  giSaveAs = 0;
  gtIRMF = NULL_TAG;
  gtIMF = NULL_TAG;

  fprintf(feilog, "** End ... BNL_fei_yoda_post_saveas \n");

  return iRetCode   = 0;
}


int BNL_FEI_imf_csp_set_attr_names()
{
  int iRetCode                = 0;
  int iNum                    = 0;
  int i                       = 0;

  char **pszValues            = NULL;


  BNL_tb_pref_ask_char_values(FEI_MDR_METADATA_ATTRS_SEND, IMAN_preference_site, &iNum, &pszValues);

  for (i = 0; i < iNum; i++)
  {
    char *pszPrefValue = BNL_strings_copy_string(pszValues[i]);
    char *pszType      = NULL;
    char *pszAttrName  = NULL;
    char *pszDummy     = NULL;

    fprintf(feilog, "%s: Processing preference value [%s] (%d of %d)\n", BNL_module, pszPrefValue, (i + 1), iNum);

    /* Process preferences like:
       IRMF.FEI_ECO_NUM:ECONumber
       IRMF.FEI_ECO_NUM[1-200]:ECONumber
     */
    pszType = pszPrefValue;
    if ((pszAttrName = strchr(pszType, '.')) != NULL)
    {
      *pszAttrName = '\0';
      pszAttrName++;

      if ((pszDummy = strchr(pszAttrName, '[')) != NULL)
      {
        *pszDummy = '\0';
      }

      if ((pszDummy = strchr(pszAttrName, ':')) != NULL)
      {
        *pszDummy = '\0';
      }
      if (strcmp(pszType, "IMF") == 0)
      {
        giMdrMetaAttributes++;
        gpszMdrMetaAttrNames = MEM_realloc(gpszMdrMetaAttrNames, sizeof(char *) * giMdrMetaAttributes);
        BNL_tb_copy_string(pszAttrName, &gpszMdrMetaAttrNames[giMdrMetaAttributes - 1]);

        fprintf(feilog, "%s: Adding value [%s]\n", BNL_module, pszAttrName);
      }
    }
    if (pszPrefValue != NULL) free(pszPrefValue);
  }
  gpszMdrMetaAttrValuesOld = MEM_alloc(sizeof(char *) * giMdrMetaAttributes);

  // JM|11112011: Added init for preventing errors on 64bit
  for (i = 0; i < giMdrMetaAttributes; i++)
  {
    gpszMdrMetaAttrValuesOld[i] = NULL;
  }

  fprintf(feilog, "%s: Number of MDR attributes found on IMF [%d] \n", BNL_module, giMdrMetaAttributes);

  if (iNum > 0) MEM_free(pszValues);

  return iRetCode;
}

/**
int BNL_FEI_imf_csp_init
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:

    Pre action on the save IMF's for csp generation

  Returns:

*/
int BNL_FEI_imf_csp_init
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;
  int iTypes        = 0;
  int i             = 0;

  char **pszTypes   = NULL;

  char *pszValue    = NULL;

  char szType[WSO_name_size_c + 1];

  logical lHasBypass    = false;
  logical lAdmin        = false;
  logical lCreated      = false;

  tag_t tObject     = NULL_TAG;
  tag_t tItem       = NULL_TAG;
  tag_t tItemRev    = NULL_TAG;


  fprintf(feilog, "** Begin ... BNL_FEI_imf_csp_init\n");

  tObject = va_arg(args, tag_t);    /* Object */

  if (tObject == gtCspIMF)
  {
    fprintf(feilog, "Already initialized.\n");
    goto function_exit;
  }

  // Reset MDR attr values
  for (i = 0; i < giMdrMetaAttributes; i++)
  {
    if (gpszMdrMetaAttrValuesOld[i] != NULL) free(gpszMdrMetaAttrValuesOld[i]);
    gpszMdrMetaAttrValuesOld[i] = NULL;
  }
  gtCspIMF = NULL_TAG;
  glCspIMF = false;

  POM_is_user_sa(&lAdmin);
  ITK_ask_bypass(&lHasBypass);
  if (lAdmin && lHasBypass)
  {
    fprintf(feilog, "%s: Warning, user has bypass, csp generation is being skipped.\n", BNL_module);
    goto function_exit;
  }

  // Check the type
  iRetCode = BNL_tb_determine_object_type(tObject, szType);
  if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Type of object: [%s].\n", BNL_module, szType);

  // Check the type
  BNL_tb_pref_ask_char_values(BNL_FEI_PREF_IMF_TYPES, IMAN_preference_site, &iTypes, &pszTypes);

  if (BNL_tb_value_in_list(szType, iTypes, pszTypes))
  {
    fprintf(feilog, "%s: Type [%s] is configured.\n", BNL_module, szType);

    gtCspIMF = tObject;

    // Check if newly created
    POM_is_newly_created(tObject, &lCreated);
    if (!lCreated)
    {
      tag_t tProperty       = NULL_TAG;
      tag_t tOldObj         = NULL_TAG;

      fprintf(feilog, "%s: Form has been modified.\n", BNL_module);

      glCspIMF = false;

      if (giMdrMetaAttributes > 0)
      {
        // Get the storage class for retrieving the old value
        iRetCode = PROP_ask_property_by_name(tObject, "data_file", &tProperty);
        if (BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = PROP_ask_value_tag(tProperty, &tOldObj);
        if (BNL_em_error_handler("PROP_ask_value_tag", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        fprintf(feilog, "%s: Old form [%d].\n", BNL_module, tOldObj);

        if (tOldObj != NULL_TAG)
        {
          for (i = 0; i < giMdrMetaAttributes; i++)
          {
            iRetCode = AOM_UIF_ask_value(tOldObj, gpszMdrMetaAttrNames[i], &pszValue);
            if (iRetCode == 38015)
            {
              EMH_clear_last_error(iRetCode);
              iRetCode = 0;

              gpszMdrMetaAttrValuesOld[i] = NULL;
            }
            else
            {
              if (BNL_em_error_handler("AOM_UIF_ask_value", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              BNL_tb_copy_string(pszValue, &gpszMdrMetaAttrValuesOld[i]);

              fprintf(feilog, "%s: Attribute [%s] had value [%s].\n", BNL_module, gpszMdrMetaAttrNames[i], gpszMdrMetaAttrValuesOld[i]);
            }

            if (pszValue != NULL) MEM_free(pszValue);
            pszValue = NULL;
          }
        }
      }
    }
    else
    {
      //fprintf(feilog, "%s: Form has been newly created.\n", BNL_module);
      //glCspIMF = true;
	    fprintf(feilog, "%s: Form has been newly created, no need to create CSP file.\n", BNL_module);
      glCspIMF = false;
    }
  } // End of if (BNL_tb_value_in_list(szType, iTypes, pszTypes))

function_exit:

  if (iTypes > 0) MEM_free(pszTypes);
  if (pszValue != NULL) MEM_free(pszValue);

  fprintf(feilog, "** End ... BNL_FEI_imf_csp_init\n");
  fprintf(feilog, "\n");

  return iRetCode;
}


/**
int BNL_FEI_imf_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:

    Post action on the save IMF's for csp generation

  Returns:

*/
int BNL_FEI_imf_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;
  int i             = 0;


  char szType[WSO_name_size_c + 1];

  logical lHasBypass    = false;
  logical lAdmin        = false;

  tag_t tObject     = NULL_TAG;
  tag_t tItem       = NULL_TAG;
  tag_t tItemRev    = NULL_TAG;


  fprintf(feilog, "** Begin ... BNL_FEI_imf_csp \n");

  tObject = va_arg(args, tag_t);    /* Object */


  POM_is_user_sa(&lAdmin);
  ITK_ask_bypass(&lHasBypass);
  if (lAdmin && lHasBypass)
  {
    fprintf(feilog, "%s: Warning, user has bypass, csp generation is being skipped.\n", BNL_module);
    goto function_exit;
  }

  if (tObject == gtCspIMF)
  {
    for (i = 0; i < giMdrMetaAttributes; i++)
    {
      if (gpszMdrMetaAttrValuesOld[i] != NULL) // Property does exist
      {
        char *pszValue        = NULL;

        iRetCode = AOM_UIF_ask_value(tObject, gpszMdrMetaAttrNames[i], &pszValue);
        if (BNL_em_error_handler("AOM_UIF_ask_value", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        if (strcmp(gpszMdrMetaAttrValuesOld[i], pszValue) != 0)
        {
          fprintf(feilog, "%s: Value of attribute [%s] has been updated from [%s] to [%s].\n", BNL_module, gpszMdrMetaAttrNames[i], gpszMdrMetaAttrValuesOld[i], pszValue);

          glCspIMF = true;
        }
        else
        {
          fprintf(feilog, "%s: Value of attribute [%s] has been not been changed.\n", BNL_module, gpszMdrMetaAttrNames[i]);
        }
        if (pszValue != NULL) MEM_free(pszValue);
      } // End of if (gpszMdrMetaAttrValuesOld[i] != NULL)
    } // End of for (i = 0; i < giMdrMetaAttributes; i++)
  } // if (tObject == gtCspIMF)

  if (glCspIMF)
  {
    // Check the type
    iRetCode = BNL_tb_determine_object_type(tObject, szType);
    if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    fprintf(feilog, "%s: Creating csp file for object of type: [%s].\n", BNL_module, szType);

    // Get the Item
    if ((iRetCode = BNL_FEI_get_item_from_imf(tObject, &tItem)) != 0) goto function_exit;

    if (tItem == NULL_TAG && !glPortalReviseSaveAs)
    {
      fprintf(feilog, "%s: Warning, failed to find the Item (probably being created IMF [%d]).\n", BNL_module, tObject);

      if (gpsList == NULL)
      {
        gpsList = (sImfsList_t *)MEM_alloc(sizeof(sImfsList_t));
        gpsList->iObjects = 1;;
        gpsList->ptImfs = (tag_t *)MEM_alloc(sizeof(tag_t) * gpsList->iObjects);
        gpsList->ptImfs[gpsList->iObjects - 1] = tObject;
        gpsList->pszNames = MEM_alloc(sizeof(char*) * gpsList->iObjects);
        BNL_tb_copy_string(szType, &gpsList->pszNames[gpsList->iObjects - 1]);
      }
      else
      {
        gpsList->iObjects++;
        gpsList->ptImfs = (tag_t *)MEM_realloc(gpsList->ptImfs, sizeof(tag_t) * gpsList->iObjects);
        gpsList->ptImfs[gpsList->iObjects - 1] = tObject;
        gpsList->pszNames = MEM_realloc(gpsList->pszNames, sizeof(char*)*gpsList->iObjects);
        BNL_tb_copy_string(szType, &gpsList->pszNames[gpsList->iObjects - 1]);
      }
    }
    else if (glPortalReviseSaveAs)
    {
      fprintf(feilog, "%s: In Attachments window, postponing csp creation.\n", BNL_module);

      if (gpsListPortal == NULL)
      {
        gpsListPortal = (sImfsList_t *)MEM_alloc(sizeof(sImfsList_t));
        gpsListPortal->iObjects = 1;;
        gpsListPortal->ptImfs = (tag_t *)MEM_alloc(sizeof(tag_t) * gpsListPortal->iObjects);
        gpsListPortal->ptImfs[gpsListPortal->iObjects - 1] = tObject;
        gpsListPortal->pszNames = MEM_alloc(sizeof(char*) * gpsListPortal->iObjects);
        BNL_tb_copy_string(szType, &gpsListPortal->pszNames[gpsListPortal->iObjects - 1]);
      }
      else
      {
        gpsListPortal->iObjects++;
        gpsListPortal->ptImfs = (tag_t *)MEM_realloc(gpsListPortal->ptImfs, sizeof(tag_t) * gpsListPortal->iObjects);
        gpsListPortal->ptImfs[gpsListPortal->iObjects - 1] = tObject;
        gpsListPortal->pszNames = MEM_realloc(gpsListPortal->pszNames, sizeof(char*)*gpsListPortal->iObjects);
        BNL_tb_copy_string(szType, &gpsListPortal->pszNames[gpsListPortal->iObjects - 1]);
      }
    }
    else
    {
      // Get the latest ItemRevision
      iRetCode = ITEM_ask_latest_rev(tItem, &tItemRev);
      if (BNL_em_error_handler("ITEM_ask_latest_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      // Do the thing
      if ((iRetCode = BNL_FEI_create_plot_request(tItemRev, szType, 0, NULL)) != 0) goto function_exit;
    }
  } // End of if (glCspIMF)

function_exit:

  // Reset MDR attr values
  for (i = 0; i < giMdrMetaAttributes; i++)
  {
    if (gpszMdrMetaAttrValuesOld[i] != NULL) free(gpszMdrMetaAttrValuesOld[i]);
    gpszMdrMetaAttrValuesOld[i] = NULL;
  }
  gtCspIMF = NULL_TAG;
  glCspIMF = false;

  fprintf(feilog, "** End ... BNL_FEI_imf_csp\n");
  fprintf(feilog, "\n");

  return iRetCode;
}


int BNL_FEI_item_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;

  logical lHasBypass    = false;
  logical lAdmin        = false;

  tag_t tObject     = NULL_TAG;


  fprintf(feilog, "** Begin ... BNL_FEI_item_csp\n");

  tObject = va_arg(args, tag_t);    /* Object */

  POM_is_user_sa(&lAdmin);
  ITK_ask_bypass(&lHasBypass);
  if (lAdmin && lHasBypass)
  {
    fprintf(feilog, "%s: Warning, user has bypass, csp generation is being skipped.\n", BNL_module);
    goto function_exit;
  }

  if (glPortalReviseSaveAs)
  {
    fprintf(feilog, "%s: In Attachments window, postponing csp creation.\n", BNL_module);
    goto function_exit;
  }

  if ((iRetCode = BNL_FEI_create_csp(tObject)) != 0) goto function_exit;

function_exit:

  fprintf(feilog, "** End ... BNL_FEI_item_csp\n");
  fprintf(feilog, "\n");

  return iRetCode;
}


int BNL_FEI_create_csp(tag_t tItem)
{
  int iRetCode        = 0;
  int i               = 0;

  tag_t tItemRev      = NULL_TAG;
  tag_t tImf          = NULL_TAG;


  fprintf(feilog, "** Begin ... BNL_FEI_create_csp\n");

  if (gpsList != NULL)
  {
    // Get the ItemMaster form
    if ((iRetCode = BNL_FEI_get_imf_from_item(tItem, &tImf)) != 0) goto function_exit;

    fprintf(feilog, "%s: Found IMF [%d].\n", BNL_module, tImf);

    for (i = 0; i < gpsList->iObjects && tImf != NULL_TAG; i++)
    {
      if (tImf == gpsList->ptImfs[i])
      {
        iRetCode = ITEM_ask_latest_rev(tItem, &tItemRev);
        if (BNL_em_error_handler("ITEM_ask_latest_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        if (tItemRev != NULL_TAG)
        {
          gpsList->ptImfs[i] = NULL_TAG;

          fprintf(feilog, "%s: Csp needs to be created, prop/type [%s] (2).\n", BNL_module, gpsList->pszNames[i]);

          // Do the thing
          //if ((iRetCode = BNL_FEI_create_plot_request(tItemRev, gpsList->pszNames[i], 0, NULL)) != 0) goto function_exit;
          iRetCode = BNL_FEI_create_plot_request(tItemRev, gpsList->pszNames[i], 0, NULL);
          //fprintf(feilog, "%s: iRetCode [%d].\n", BNL_module, iRetCode);
        } // End of if (tItemRev != NULL_TAG)
      } // if (tImf == gpsList->ptImfs[i])
    } // End of for (i = 0; i < gpsList->iObjects; i++)
  } // End  of if (gpsList != NULL)

function_exit:

  fprintf(feilog, "** End ... BNL_FEI_create_csp\n");

  return iRetCode;
}


/**
int BNL_FEI_rel_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:

    Post action on the relation creation for csp generation

  Returns:

*/
int BNL_FEI_rel_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;

  tag_t tPrimary    = NULL_TAG;
  tag_t tSecondary  = NULL_TAG;
  tag_t tRelType    = NULL_TAG;
  tag_t tItemRev    = NULL_TAG;

  logical lAdmin    =false;
  logical lHasBypass=false;

  char szClass[64];
  char szRelType[IMANTYPE_name_size_c + 1];

  fprintf(feilog, "%s: Begin ... BNL_FEI_rel_csp\n", BNL_module);

  if (glPortalReviseSaveAs)
  {
    fprintf(feilog, "%s: Attachments window, skipping creation csp file.\n", BNL_module);
    goto function_exit;
  }

  POM_is_user_sa(&lAdmin);
  ITK_ask_bypass(&lHasBypass);
  if (lAdmin && lHasBypass)
  {
    fprintf(feilog, "%s: Warning, user has bypass, csp generation is being skipped.\n", BNL_module);
    goto function_exit;
  }

  tPrimary = va_arg(args, tag_t);    /* Primary object */
  tSecondary = va_arg(args, tag_t);  /* Secondary object */
  tRelType = va_arg(args, tag_t);    /* Relation type */

  // Check the class of the primary object
  iRetCode = BNL_tb_determine_super_class(tPrimary, szClass);
  if (BNL_em_error_handler("BNL_tb_determine_super_class", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  if (strcmp(szClass, "Item") == 0)
  {
    fprintf(feilog, "%s: Class [%s].\n", BNL_module, szClass);

    // Get the name of the relation
    iRetCode = IMANTYPE_ask_name(tRelType, szRelType);
    if (BNL_em_error_handler("IMANTYPE_ask_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    // Get the latest ItemRevision
    iRetCode = ITEM_ask_latest_rev(tPrimary, &tItemRev);
    if (BNL_em_error_handler("ITEM_ask_latest_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    if (tItemRev == NULL_TAG)
    {
      fprintf(feilog, "<TODO> No ItemRevision?\n");
    }
    else
    {
      // Create the csv file
      if ((iRetCode = BNL_FEI_create_plot_request(tItemRev, szRelType, 0, NULL)) != 0) goto function_exit;
    }
  }


function_exit:

  fprintf(feilog, "%s: End ... BNL_FEI_rel_csp\n\n", BNL_module);

  return iRetCode;
}


/**
int BNL_FEI_prop_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:
    Post action on the update of an property for csp generation

  Returns:

    ITK_ok or !ITK_ok

*/
int BNL_FEI_prop_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;
  int iPropTypes    = 0;
  int i             = 0;
  int iCountRole    = 0;

  tag_t tProp       = NULL_TAG;
  tag_t tObject     = NULL_TAG;
  tag_t tItem       = NULL_TAG;
  tag_t tItemRev    = NULL_TAG;

  char szType[IMANTYPE_name_size_c + 1];
  char szKey[128 + 1];

  char *pszPropName = NULL;
  char *pszValue    = NULL;
  char *pszOldValue = NULL;

  char **pszPropTypes = NULL;
  char **pszPrefsRole = NULL;

  logical lFound    = false;
  logical lHasBypass=false;
  logical lAdmin    = false;
  logical lCreated  = false;


  fprintf(feilog, "** Begin ... BNL_FEI_prop_csp\n");

  tProp = va_arg(args, tag_t);
  pszValue = va_arg(args, char*);

  POM_is_user_sa(&lAdmin);
  ITK_ask_bypass(&lHasBypass);
  if (lAdmin && lHasBypass)
  {
    fprintf(feilog, "%s: Warning, user has bypass, csp generation is being skipped.\n", BNL_module);
    goto function_exit;
  }

  //fprintf(feilog, "** Prop value [%s]\n", pszValue);

  iRetCode = PROP_ask_name(tProp, &pszPropName);
  if (BNL_em_error_handler("PROP_ask_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Prop name: [%s] value [%s].\n", BNL_module, pszPropName, pszValue);

  iRetCode = PROP_ask_owning_object(tProp, &tObject);
  if (BNL_em_error_handler("PROP_ask_owning_object", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = POM_is_newly_created(tObject, &lCreated);
  if (BNL_em_error_handler("POM_is_newly_created", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Object [%d], newly created: [%d].\n", BNL_module, tObject, lCreated);

  if (lCreated && strcmp(pszValue, "Yes") != 0)
  {
    fprintf(feilog, "%s: Newly created and value [%s] differs from [Yes]. No need to create csp file.\n", BNL_module, pszValue);
    goto function_exit;
  }

  if (!lCreated) // Make sure value was changed
  {
    tag_t tProperty   = NULL_TAG;
    tag_t tOldObj     = NULL_TAG;

    // Get the storage class for retrieving the old value
    iRetCode = PROP_ask_property_by_name(tObject, "data_file", &tProperty);
    if (BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    iRetCode = PROP_ask_value_tag(tProperty, &tOldObj);
    if (BNL_em_error_handler("PROP_ask_value_tag", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    fprintf(feilog, "%s: Old form [%d].\n", BNL_module, tOldObj);

    if (tOldObj != NULL_TAG)
    {
      iRetCode = AOM_UIF_ask_value(tOldObj, pszPropName, &pszOldValue);
      if (BNL_em_error_handler("PROP_ask_value_tag", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      fprintf(feilog, "%s: Old value [%s].\n", BNL_module, pszOldValue);

      if (strcmp(pszOldValue, pszValue) == 0)
      {
        fprintf(feilog, "%s: Modified but value was not changed. No need to create csp file.\n", BNL_module);
        goto function_exit;
      }
    }

    if (pszOldValue != NULL) MEM_free(pszOldValue);
    pszOldValue = NULL;
  }

  iRetCode = BNL_tb_determine_object_type(tObject, szType);
  if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Type of owning object: [%s].\n", BNL_module, szType);

  // Check the type
  BNL_tb_pref_ask_char_values(BNL_FEI_PREF_PROP_NAMES, IMAN_preference_site, &iPropTypes, &pszPropTypes);

  for (i=0; i<iPropTypes; i++)
  {
    char *pszType     = NULL;
    char *pszProp     = NULL;

    fprintf(feilog, "%s: Found prop/type [%s].\n", BNL_module, pszPropTypes[i]);

    pszType = pszPropTypes[i];

    if (strchr(pszType, ';') != NULL)
    {
      pszProp = strchr(pszType, ';');
      *pszProp = '\0';
      pszProp++;

      if (strcmp(pszType, szType) == 0 && strcmp(pszProp, pszPropName) == 0)
      {
        lFound = true;
        break;
      }
    }
  }

  if (lFound)
  {
    fprintf(feilog, "%s: This is the one...\n", BNL_module);

    // Check if property is allowed to get modified
    // Attribute security (1)
    sprintf(szKey, "%s_%s", BNL_FEI_ATTR_ROLE, szType);
    fprintf(feilog, "%s: Looking for preference [%s].\n", BNL_module, szKey);
    BNL_tb_pref_ask_char_values(szKey, IMAN_preference_site, &iCountRole, &pszPrefsRole);
    fprintf(feilog, "%s: Found [%d] preference values.\n", BNL_module, iCountRole);
    if (iCountRole > 0)
    {
      iRetCode = FEI_check_attributes_role(tObject, iCountRole, pszPrefsRole, true);
      if (iRetCode == (BNL_error_message + giErrorOffset))
      {
        fprintf(feilog, "%s: Skipping creation of csp file, since attribute is not allowed to get modified.\n", BNL_module, iCountRole);

        iRetCode = 0;
        goto function_exit;
      }
    }

    // Get the Item
    if ((iRetCode = BNL_FEI_get_item_from_imf(tObject, &tItem)) != 0) goto function_exit;

    if (tItem == NULL_TAG && !glPortalReviseSaveAs)
    {
      fprintf(feilog, "%s: Warning, failed to find the Item (probably being created, IMF [%d]).\n", BNL_module, tObject);

      if (gpsList == NULL)
      {
        gpsList = (sImfsList_t *)MEM_alloc(sizeof(sImfsList_t));
        gpsList->iObjects = 1;;
        gpsList->ptImfs = (tag_t *)MEM_alloc(sizeof(tag_t) * gpsList->iObjects);
        gpsList->ptImfs[gpsList->iObjects - 1] = tObject;
        gpsList->pszNames = MEM_alloc(sizeof(char*) * gpsList->iObjects);
        BNL_tb_copy_string(pszPropName, &gpsList->pszNames[gpsList->iObjects - 1]);
      }
      else
      {
        gpsList->iObjects++;
        gpsList->ptImfs = (tag_t *)MEM_realloc(gpsList->ptImfs, sizeof(tag_t) * gpsList->iObjects);
        gpsList->ptImfs[gpsList->iObjects - 1] = tObject;
        gpsList->pszNames = MEM_realloc(gpsList->pszNames, sizeof(char*)*gpsList->iObjects);
        BNL_tb_copy_string(pszPropName, &gpsList->pszNames[gpsList->iObjects - 1]);
       }
    }
    else if (glPortalReviseSaveAs)
    {
      fprintf(feilog, "%s: In Attachments window, postponing csp creation.\n", BNL_module);

      if (gpsListPortal == NULL)
      {
        gpsListPortal = (sImfsList_t *)MEM_alloc(sizeof(sImfsList_t));
        gpsListPortal->iObjects = 1;
        gpsListPortal->ptImfs = (tag_t *)MEM_alloc(sizeof(tag_t) * gpsListPortal->iObjects);
        gpsListPortal->ptImfs[gpsListPortal->iObjects - 1] = tObject;
        gpsListPortal->pszNames = MEM_alloc(sizeof(char*) * gpsListPortal->iObjects);
        BNL_tb_copy_string(pszPropName, &gpsListPortal->pszNames[gpsListPortal->iObjects - 1]);
      }
      else
      {
        gpsListPortal->iObjects++;
        gpsListPortal->ptImfs = (tag_t *)MEM_realloc(gpsListPortal->ptImfs, sizeof(tag_t) * gpsListPortal->iObjects);
        gpsListPortal->ptImfs[gpsListPortal->iObjects - 1] = tObject;
        gpsListPortal->pszNames = MEM_realloc(gpsListPortal->pszNames, sizeof(char*)*gpsListPortal->iObjects);
        BNL_tb_copy_string(pszPropName, &gpsListPortal->pszNames[gpsListPortal->iObjects - 1]);
      }
    }
    else
    {
      if (gpsList != NULL)
      {
        for (i = 0; i < gpsList->iObjects; i++)
        {
          if (gpsList->ptImfs[i] == tObject)
          {
            fprintf(feilog, "%s: Also in list, removing...\n", BNL_module);
            gpsList->ptImfs[i] = NULL_TAG;
          }
        }
      }

      // Get the latest ItemRevision
      iRetCode = ITEM_ask_latest_rev(tItem, &tItemRev);
      if (BNL_em_error_handler("ITEM_ask_latest_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      // Do the thing
      if ((iRetCode = BNL_FEI_create_plot_request(tItemRev, pszPropName, 0, NULL)) != 0) goto function_exit;
    }

  }


function_exit:

  if (pszPropName != NULL) MEM_free(pszPropName);
  if (iPropTypes > 0) MEM_free(pszPropTypes);
  if (pszOldValue != NULL) MEM_free(pszOldValue);
  if (iCountRole > 0) MEM_free(pszPrefsRole);

  fprintf(feilog, "** End ... BNL_FEI_prop_csp\n\n");

  return iRetCode;

}

int BNL_FEI_get_item_from_imf(tag_t tIMF, tag_t *ptItem)
{
  int iRetCode      = 0;
  int iCount        = 0;
  int i             = 0;

  tag_t tRelType    = NULL_TAG;

  tag_t *ptPrimaryObjects = NULL;

  char szClass[64];


  iRetCode = GRM_find_relation_type("IMAN_master_form", &tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = GRM_list_primary_objects_only(tIMF, tRelType, &iCount, &ptPrimaryObjects);
  if (BNL_em_error_handler("GRM_list_primary_objects_only", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  for (i = 0; i < iCount; i++)
  {
    iRetCode = BNL_tb_determine_super_class(ptPrimaryObjects[i], szClass);
    if (BNL_em_error_handler("BNL_tb_determine_super_class", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    if (strcmp(szClass, "Item") == 0)
    {
      // This is the one
      *ptItem = ptPrimaryObjects[i];
      break;
    }
  }

function_exit:

  if (iCount > 0) MEM_free(ptPrimaryObjects);

  return iRetCode;
}


int BNL_FEI_get_imf_from_item(tag_t tItem, tag_t *ptIMF)
{
  int iRetCode      = 0;
  int iCount        = 0;
  int i             = 0;

  tag_t tRelType    = NULL_TAG;

  tag_t *ptObjects  = NULL;

  char szClass[64];


  iRetCode = GRM_find_relation_type("IMAN_master_form", &tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = GRM_list_secondary_objects_only(tItem, tRelType, &iCount, &ptObjects);
  if (BNL_em_error_handler("GRM_list_primary_objects_only", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  for (i = 0; i < iCount; i++)
  {
    iRetCode = BNL_tb_determine_super_class(ptObjects[i], szClass);
    if (BNL_em_error_handler("BNL_tb_determine_super_class", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    if (strcmp(szClass, "Form") == 0)
    {
      // This is the one
      *ptIMF = ptObjects[i];
      break;
    }
  }

function_exit:

  if (iCount > 0) MEM_free(ptObjects);

  return iRetCode;
}
/**************************************************************************************************
 * int BNL_FEI_create_plot_request:
 *
 * History of changes
 * Date        Author           Description
 * ----------- ---------------- -------------------------------------------------------------------
 * 06-Jun-2016 DTJ              Obsolete. From Tc10 replaced by BNL_create_dispatcher_request
 **************************************************************************************************/
int BNL_FEI_create_plot_request(tag_t tItemRev, char *pszType, int iExtraCols, char **pszExtraCols)
{
  int iRetCode            = 0;

  char szPrefName[256]    = {""};

  sprintf(szPrefName, "FEI_CSP_INI_%s", pszType);
  iRetCode = BNL_create_dispatcher_request(tItemRev, szPrefName, iExtraCols, pszExtraCols);
  if (BNL_em_error_handler( "BNL_create_dispatcher_request", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

function_exit:

  return iRetCode;
}
/*
int BNL_FEI_create_plot_request(tag_t tItemRev, char *pszType, int iExtraCols, char **pszExtraCols)
{
  int iRetCode            = 0;
  int iTypesCount         = 0;
  int iScanFolder         = 0;


  char szPrefName[256]    = {""};

  char *pszIniFile        = NULL;
  char *pszTempFile       = NULL;
  char *pszTemp           = NULL;
  char *pszDevice         = NULL;
  char *pszScanDirectory  = NULL;
  char *pszScanFolder     = NULL;
  char *pszOutputItemp    = getenv("TEMP");

  tag_t tFolder           = NULL_TAG;

  char **pszObjectTypes   = NULL;

  BNL_csp_initialise(feilog, NULL);

  sprintf(szPrefName, "FEI_CSP_INI_%s", pszType);

  BNL_tb_pref_ask_char_value(szPrefName, IMAN_preference_site, &pszTempFile);
  if (pszTempFile == NULL)
  {
    fprintf(feilog, "%s: Preference [%s] is not defined. No PlotRequest is being created!\n", BNL_module, szPrefName);
  }
  else
  {
    BNL_tb_replace_env_for_path(pszTempFile, &pszIniFile);
    if (pszIniFile == NULL)
    {
      //pszIniFile = BNL_strings_copy_string(pszTempFile);
      pszIniFile = MEM_string_copy(pszTempFile);
    }

    fprintf(feilog, "%s: Using ini file [%s]\n", BNL_module, pszIniFile);

    if (!BNL_fm_check_file_or_dir(pszIniFile))
    {
      fprintf(feilog, "%s: Error, the given csv inifile [%s] not found.\n" , BNL_module, pszIniFile);
      goto function_exit;
    }

    BNL_csp_set_ini(pszIniFile);

    // Scanfolder?
    if (BNL_ini_read_ini_value(pszIniFile, "scan", "scanfolder", &pszScanFolder) == 0)
    {
      iRetCode = BNL_tb_find_folder(pszScanFolder, &tFolder);
      if (BNL_em_error_handler( "BNL_tb_find_folder", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      // check existence
      if (tFolder == NULL_TAG)
      {
        fprintf(feilog, "%s: Error, scan folder [%s] not found.\n", BNL_module, pszScanFolder);
        goto function_exit;
      }
      iScanFolder = 1;
    }
    else
    {
      if (BNL_ini_read_ini_value(pszIniFile, "scan", "scandir", &pszTemp) == 0)
      {
        BNL_tb_replace_all_env_for_path(pszTemp, &pszScanDirectory);
        if (pszScanDirectory == NULL)
        {
          pszScanDirectory = MEM_alloc(sizeof(char)*((int)strlen(pszTemp) + 1));
		      strcpy(pszScanDirectory, pszTemp);
        }

        // check existence
        if (BNL_fm_check_file_or_dir( pszScanDirectory) == 0)
        {
          fprintf( feilog, "%s: Error, scan directory [%s] not found.\n", BNL_module, pszScanDirectory);
          goto function_exit;
        }
      }
      else
      {
        fprintf( feilog, "%s: Error, no scanfolder or scandir has been defined.\n", BNL_module);
        goto function_exit;
      } // End of if (BNL_ini_read_ini_value(pszIniFile, SCAN_CONFIG, "scandir", &pszTemp) == 0)
    } // End of if (BNL_ini_read_ini_value(pszInFile, SCAN_CONFIG, "scanfolder", &pszScanFolder) == 0)

    // Get the object types
    if (BNL_ini_get_multiple_values(pszIniFile, CREATOR_CONFIG, "object_types", ';', &iTypesCount, &pszObjectTypes) != 0)
    {
      fprintf( feilog, "%s: Error, object_types has not been defined.\n", BNL_module);
      goto function_exit;
    }
    else
    {
      BNL_csp_set_objecttypes(iTypesCount, pszObjectTypes);
    }

    // Get the device
    if (BNL_ini_read_ini_value(pszIniFile, CREATOR_CONFIG, "device", &pszDevice) != 0)
    {
      fprintf( feilog, "%s: Error, device has not been defined.\n", BNL_module);
      goto function_exit;
    }
    else
    {
      BNL_csp_set_device(pszDevice);
    }

    // Set the output directory
    if (pszOutputItemp != NULL)
    {
      BNL_csp_set_csp_out_dir(pszOutputItemp);
    }
    else
    {
      fprintf( feilog, "%s: Error, setting [TEMP] has not been defined.\n", BNL_module);
      goto function_exit;
    }

    // Extra col
    if (iExtraCols > 0)
    {
      BNL_csp_set_extracols(iExtraCols, pszExtraCols);
    }

    // Create csp file in temporary directory
    if ((iRetCode = BNL_create_csp_file(tItemRev)) != 0) goto function_exit;

    if (iScanFolder)
    {
      if ((iRetCode = store_csv_file()) != 0) goto function_exit;
      if ((iRetCode = send_plotrequest(NULL_TAG, CUT_AND_PASTE, pszScanFolder)) != 0) goto function_exit;
    }
    else
    {
      if ((iRetCode = send_csv_file(pszScanDirectory)) != 0) goto function_exit;
    }
  } // End of if (pszTemp == NULL)

function_exit:

  if (pszTempFile != NULL) MEM_free(pszTempFile);
  if (pszScanDirectory != NULL) MEM_free(pszScanDirectory);

  //if (pszIniFile != NULL) free(pszIniFile);
  if (pszIniFile != NULL) MEM_free(pszIniFile);
  if (pszScanFolder != NULL) free(pszScanFolder);
  if (pszTemp != NULL) free(pszTemp);
  if (pszDevice != NULL) free(pszDevice);
  if (pszObjectTypes != NULL) free(pszObjectTypes);

  BNL_csp_exit();

  return iRetCode;
}
*/

int BNL_FEI_itemrev_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;
  int i             = 0;

  logical lHasBypass= false;
  logical lAdmin    = false;

  tag_t tObject     = NULL_TAG;
  tag_t tItem       = NULL_TAG;
  tag_t tImf        = NULL_TAG;


  fprintf(feilog, "** Begin ... BNL_FEI_itemrev_csp \n");

  tObject = va_arg(args, tag_t);    /* Object */

  POM_is_user_sa(&lAdmin);
  ITK_ask_bypass(&lHasBypass);
  if (lAdmin && lHasBypass)
  {
    fprintf(feilog, "%s: Warning, user has bypass, csp generation is being skipped.\n", BNL_module);
    goto function_exit;
  }

  if (glPortalReviseSaveAs)
  {
    fprintf(feilog, "%s: In Attachments window, postponing csp creation.\n", BNL_module);
    goto function_exit;
  }

  if (gpsList != NULL)
  {
    iRetCode = ITEM_ask_item_of_rev(tObject, &tItem);
    if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    if (tItem != NULL_TAG)
    {
      // Get the ItemMaster form
      if ((iRetCode = BNL_FEI_get_imf_from_item(tItem, &tImf)) != 0) goto function_exit;

      fprintf(feilog, "%s: Found IMF [%d].\n", BNL_module, tImf);

      for (i = 0; i < gpsList->iObjects && tImf != NULL_TAG; i++)
      {
        if (tImf == gpsList->ptImfs[i])
        {
          gpsList->ptImfs[i] = NULL_TAG;

          fprintf(feilog, "%s: Csp needs to be created, prop/type [%s] (3).\n", BNL_module, gpsList->pszNames[i]);

          // Do the thing
          if ((iRetCode = BNL_FEI_create_plot_request(tObject, gpsList->pszNames[i], 0, NULL)) != 0) goto function_exit;
        } // End of
      }
    } // End of for (i = 0; i < gpsList->iObjects; i++)
  } // End  of if (gpsList != NULL)

function_exit:

  fprintf(feilog, "** End ... BNL_FEI_itemrev_csp\n");
  fprintf(feilog, "\n");

  return iRetCode;
}

int BNL_FEI_item_create
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;

  char *pszItemId   = NULL;
  char *pszName     = NULL;
  char *pszType     = NULL;
  char *pszRevId    = NULL;

  tag_t *ptNewItem  = NULL;
  tag_t *ptNewRev   = NULL;

  logical lResult   = false;

  fprintf(feilog, "** Begin ... BNL_FEI_item_create \n");

  /*
    @param const char* item_id
    @param const char* item_name
    @param const char* type_name
    @param const char* rev_id
    @param tag_t*      new_item
    @param tag_t*      new_rev
    @param tag_t       item_master_form
    @param tag_t       item_rev_master_form
  */

  pszItemId = va_arg(args, char *);
  pszName = va_arg(args, char *);
  pszType = va_arg(args, char *);
  pszRevId = va_arg(args, char *);
  ptNewItem = va_arg(args, tag_t *);
  ptNewRev = va_arg(args, tag_t *);


  /*fprintf(feilog, "pszItemId [%s]\n", pszItemId);
  fprintf(feilog, "pszName [%s]\n", pszName);
  fprintf(feilog, "pszType [%s]\n", pszType);
  fprintf(feilog, "pszRevId [%s]\n", pszRevId);

  fprintf(feilog, "ptNewItem [%d]\n", *ptNewItem);
  fprintf(feilog, "ptNewRev [%d]\n", *ptNewRev);*/

  //fprintf(feilog, "tIMF [%d]\n", *tIMF);
  //fprintf(feilog, "tIRMF [%d]\n", tIRMF);

  if ((iRetCode = BNL_FEI_FRU_check_attr_value(*ptNewItem, &lResult)) != 0) goto function_exit;

  if (lResult)
  {
    if ((iRetCode = BNL_FEI_FRU_update_bom(*ptNewItem, *ptNewRev)) != 0) goto function_exit;
  }

function_exit:

  fprintf(feilog, "** End ... BNL_FEI_item_create\n");
  fprintf(feilog, "\n");

  return iRetCode;
}

// JM: When a group also exists as a sub group, SA_find_group fails, so workaround...
int BNL_find_group(char *pszGroup, tag_t *ptGroup)
{
  int iRetCode    = 0;
  int iCount      = 0;
  int i;

  tag_t *ptGroups = NULL;

  char *pszName       = NULL;


  *ptGroup = NULL_TAG;

  iRetCode = SA_extent_group(&iCount, &ptGroups);
  if (BNL_em_error_handler("SA_extent_group", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  for (i = 0;i  < iCount; i++)
  {
    iRetCode = SA_ask_group_full_name(ptGroups[i], &pszName);
    if (BNL_em_error_handler("SA_ask_group_full_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    if (pszGroup != NULL && strcmp(pszName, pszGroup) == 0)
    {
        *ptGroup = ptGroups[i];
        break;
    }
    if (pszName != NULL)
    {
      pszName = NULL;
      MEM_free(pszName);
    }
  }

function_exit:

  if (iCount > 0) MEM_free(ptGroups);
  if (pszName != NULL) MEM_free(pszName);

  return iRetCode;
}

int BNL_FEI_item_saveas(METHOD_message_t * msg, va_list args)
{
  int iRetCode    = 0;

  tag_t *tNewRev  = NULL;

  tag_t tNewItem  = NULL_TAG;
  tag_t tOrgRev   = NULL_TAG;
  tag_t tOrgItem  = NULL_TAG;

  char *pszNewRevId= NULL;
  char *pszName   = NULL;
  char *pszDesc   = NULL;
  char *pszItemType = NULL;

  char szType[WSO_name_size_c + 1];

  logical lResult = false;


  fprintf(feilog, "** Begin ... BNL_FEI_item_saveas\n");

  tOrgRev = va_arg(args, tag_t);
  pszNewRevId = va_arg(args, char*);
  pszName = va_arg(args, char*);
  pszDesc = va_arg(args, char*);
  tNewItem = va_arg(args, tag_t);
  tNewRev = va_arg(args, tag_t *);


  iRetCode = BNL_tb_determine_object_type(tNewItem, szType);
  if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Type of object: [%s] [%d].\n", BNL_module, szType, *tNewRev);

  // Check the Type
  BNL_tb_pref_ask_char_value(BNL_FEI_PREF_FRU_ITEM, IMAN_preference_site, &pszItemType);

  if (pszItemType != NULL && strcmp(pszItemType, szType) == 0)
  {
    if ((iRetCode = BNL_FEI_FRU_check_attr_value(tNewItem, &lResult)) != 0) goto function_exit;

    if (lResult)
    {
      if ((iRetCode = BNL_FEI_FRU_update_bom(tNewItem, *tNewRev)) != 0) goto function_exit;
    }
  }

function_exit:

  if (pszItemType != NULL) MEM_free(pszItemType);

  fprintf(feilog, "**Leave BNL_FEI_item_saveas\n\n");

  return iRetCode;
}


int BNL_FEI_FRU_create_item(char *pszItemType, char *pszDesc, char *pszName, tag_t *ptItem, tag_t *ptRev)
{
  int iRetCode        = 0;

  char *pszUser       = NULL;
  char *pszGroup      = NULL;
  char *pszStatus     = NULL;
  char *pszReceiver   = NULL;
  char *pszMailServer = NULL;

  char szCommand[400];
  char szSubject[256];
  char szId[ITEM_id_size_c + 1];

  FILE *fpConnect     = NULL;


  iRetCode = ITEM_create_item(NULL, "name", pszItemType, NULL, ptItem, ptRev);
  if (BNL_em_error_handler("ITEM_create_item", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  iRetCode = ITEM_set_description(*ptItem, pszDesc);
  if (BNL_em_error_handler("ITEM_set_description (item)", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  iRetCode = ITEM_set_rev_description(*ptRev, pszDesc);
  if (BNL_em_error_handler("ITEM_set_description (rev)", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  iRetCode = ITEM_ask_id(*ptItem, szId);
  if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  iRetCode = ITEM_set_name(*ptItem, szId);
  if (BNL_em_error_handler("ITEM_set_name", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  //iRetCode = ITEM_set_rev_name(*ptRev, pszName);
  iRetCode = ITEM_set_rev_name(*ptRev, szId);
  if (BNL_em_error_handler("ITEM_set_name", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  BNL_tb_pref_ask_char_value(BNL_FEI_PREF_FRU_USER, IMAN_preference_site, &pszUser);
  if (pszUser == NULL) fprintf(feilog, "Warning, preference [%s] has not been defined.\n", BNL_FEI_PREF_FRU_USER);

  BNL_tb_pref_ask_char_value(BNL_FEI_PREF_FRU_GROUP, IMAN_preference_site, &pszGroup);
  if (pszUser == NULL) fprintf(feilog, "Warning, preference [%s] has not been defined.\n", BNL_FEI_PREF_FRU_GROUP);

  BNL_tb_pref_ask_char_value(BNL_FEI_PREF_FRU_STATUS, IMAN_preference_site, &pszStatus);
  if (pszStatus == NULL) fprintf(feilog, "Warning, preference [%s] has not been defined.\n", BNL_FEI_PREF_FRU_STATUS);

  if ((iRetCode = BNL_FEI_FRU_set_status_change_ownership(*ptItem, *ptRev, pszUser, pszGroup, pszStatus)) != 0) goto function_exit;

  // Send E-mail
  BNL_tb_pref_ask_char_value(FEI_FRU_FUNCTIONAL_EMAIL, IMAN_preference_site, &pszReceiver);
  if (pszReceiver == NULL)
  {
    fprintf(feilog, "Warning, preference [%s] has not been defined.\n", FEI_FRU_FUNCTIONAL_EMAIL);
  }
  else
  {
    BNL_tb_pref_ask_char_value("Mail_server_name", IMAN_preference_site, &pszMailServer);
    if (pszMailServer == NULL)
    {
      fprintf(feilog, "Warning, preference [Mail_server_name] has not been defined.\n");
    }
    else
    {
      sprintf(szSubject, "FRUList Item %s belonging to Commercial Item %s has been created.", szId, pszDesc);

      sprintf(szCommand, "tc_mail_smtp -to=\"%s\" -subject=\"%s\" -server=%s",
        pszReceiver, szSubject, pszMailServer);

      fprintf(feilog, "%s: Sending: [%s]\n", BNL_module, szCommand);

      //system(szCommand);
      fpConnect = _popen(szCommand, "rt");
      if (fpConnect == NULL)
      {
        fprintf(feilog, "%s: Error while opening pipe to [%s].\n", BNL_module, szCommand);
        iRetCode = 1;
      }
      else
      {
        /* Catch the error from pclose, it returns the exitcode from the last command */
        iRetCode = _pclose(fpConnect);
        if (iRetCode != 0)
        {
          fprintf(feilog , "%s: Sending E-mail returned with error [%d].\n", BNL_module, iRetCode);
          iRetCode = 0;
        }
      }
    }
  }

function_exit:

  if (pszUser != NULL) MEM_free(pszUser);
  if (pszGroup != NULL) MEM_free(pszGroup);
  if (pszStatus != NULL) MEM_free(pszStatus);
  if (pszReceiver != NULL) MEM_free(pszReceiver);
  if (pszMailServer != NULL) MEM_free(pszMailServer);

  return iRetCode;
}


int BNL_FEI_FRU_set_status_change_ownership(tag_t tItem, tag_t tRev, char *pszUser, char *pszGroup, char *pszStatus)
{
  int iRetCode    = 0;
  int i           = 0;
  int iRels       = 0;

  tag_t tUser     = NULL_TAG;
  tag_t tGroup    = NULL_TAG;
  tag_t tStatus   = NULL_TAG;

  tag_t *ptRels   = NULL;

  logical lSaveNeeded = false;

  if (pszUser == NULL ||  pszGroup == NULL)
  {
    fprintf(feilog, "%s: User/group not defined, ownership won't be changed.\n", BNL_module, pszUser);
  }
  else
  {
    if (pszUser != NULL)
    {
      iRetCode = SA_find_user(pszUser, &tUser);
      if (BNL_em_error_handler("SA_find_user", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;
    }

    if (tUser == NULL_TAG)
    {
      fprintf(feilog, "%s: Warning, failed to find user [%s].\n", BNL_module, pszUser);
      goto function_exit;
    }

    if (pszGroup != NULL)
    {
      if ((iRetCode = BNL_find_group(pszGroup, &tGroup)) != 0) goto function_exit;
    }

    if (tGroup == NULL_TAG)
    {
      fprintf(feilog, "%s: Warning, failed to find group [%s].\n", BNL_module, pszGroup);
      goto function_exit;
    }

    // First process the ItemRevision and its attachments
    iRetCode = GRM_list_secondary_objects_only(tRev, NULL_TAG, &iRels, &ptRels);
    if (BNL_em_error_handler("GRM_list_secondary_objects" ,BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    for (i = 0; i < iRels; i++)
    {
      iRetCode = AOM_set_ownership(ptRels[i], tUser, tGroup);
      if (BNL_em_error_handler("AOM_set_ownership (itemrev rels)" ,BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      iRetCode = AOM_save(ptRels[i]);
      if (BNL_em_error_handler("AOM_save (itemrev rels)" ,BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
    }

    if (iRels > 0)
    {
      MEM_free(ptRels);
      iRels = 0;
    }

    iRetCode = AOM_set_ownership(tRev, tUser, tGroup);
    if (BNL_em_error_handler("AOM_set_ownership (rev)" ,BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    // Process the item
    iRetCode = GRM_list_secondary_objects_only(tItem, NULL_TAG, &iRels, &ptRels);
    if (BNL_em_error_handler("GRM_list_secondary_objects" ,BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    for (i = 0; i < iRels; i++)
    {
      iRetCode = AOM_set_ownership(ptRels[i], tUser, tGroup);
      if (BNL_em_error_handler("AOM_set_ownership (item rels)" ,BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      iRetCode = AOM_save(ptRels[i]);
      if (BNL_em_error_handler("AOM_save (item rels)" ,BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
    }

    iRetCode = AOM_set_ownership(tItem, tUser, tGroup);
    if (BNL_em_error_handler("AOM_set_ownership (item)" ,BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    iRetCode = ITEM_save_item(tItem);
    if (BNL_em_error_handler("AOM_save (item)" ,BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    lSaveNeeded = true;
  }

  // Change the status of the revision
  if (pszStatus != NULL)
  {
    fprintf(feilog, "%s: Setting status to [%s].\n", BNL_module, pszStatus);

    iRetCode = BNL_tb_create_new_status(pszStatus, &tStatus);
    if (BNL_em_error_handler("BNL_tb_create_new_status", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    iRetCode = BNL_tb_add_new_status(tRev, tStatus, 999, true);
    if (BNL_em_error_handler("BNL_tb_add_new_status", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    iRetCode = BNL_tb_load_object(tStatus, POM_no_lock);
    if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    lSaveNeeded = false;
  }

  if (lSaveNeeded)
  {
    iRetCode = ITEM_save_rev(tRev);
    if (BNL_em_error_handler("AOM_save (rev)" ,BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
  }

function_exit:

  if (iRels > 0) MEM_free(ptRels);

  return iRetCode;
}


int BNL_FEI_FRU_check_attr_value(tag_t tItem, logical *plResult)
{
  int iRetCode      = 0;
  int iValues       = 0;
  int iRels         = 0;

  tag_t tIMF        = NULL_TAG;
  tag_t tRelType    = NULL_TAG;

  tag_t *ptRels     = NULL;

  char *pszName     = NULL;
  char *pszValue    = NULL;

  char **pszValues  = NULL;


  // Get the IMF
  iRetCode = GRM_find_relation_type("IMAN_master_form", &tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type",BNL_module,EMH_severity_error,iRetCode, true)) goto function_exit;

  if (tRelType == NULLTAG)
	{
    fprintf(feilog, "%s: Warning, relation type [IMAN_master_form] is not found.\n", BNL_module);
		goto function_exit;
	}

  iRetCode = GRM_list_secondary_objects_only(tItem, tRelType, &iRels, &ptRels);
  if (BNL_em_error_handler("GRM_list_secondary_objects",BNL_module,EMH_severity_error,iRetCode, true)) goto function_exit;

  if (iRels == 1)
  {
    tIMF = ptRels[0];
  }
  else
  {
    fprintf(feilog, "%s: Warning, ItemMaster Form cannot be found.\n", BNL_module);
		goto function_exit;
  }

  BNL_tb_pref_ask_char_value(BNL_FEI_PREF_FRU_SUBCLASS, IMAN_preference_site, &pszName);
  BNL_tb_pref_ask_char_values(BNL_FEI_PREF_FRU_SC_VALUES, IMAN_preference_site, &iValues, &pszValues);
  if (pszName == NULL || iValues == 0)
  {
    fprintf(feilog, "%s: Warning, preference [%s] and/or [%s] is not defined.\n", BNL_module, BNL_FEI_PREF_FRU_SUBCLASS, BNL_FEI_PREF_FRU_SC_VALUES);
    goto function_exit;
  }

  fprintf(feilog, "%s: Checking attribute [%s].\n", BNL_module, pszName);

  if ((iRetCode = BNL_tb_get_property(tIMF, pszName, &pszValue)) != 0) goto function_exit;
  fprintf(feilog, "%s: Attribute has value [%s].\n", BNL_module, pszValue);

  if (!BNL_tb_value_in_list(pszValue, iValues, pszValues))
  {
    *plResult = false;
    fprintf(feilog, "%s: Warning, attribute value is not in list, skipping...\n", BNL_module);
  }
  else
  {
    *plResult = true;
    fprintf(feilog, "%s: Attribute value is in list.\n", BNL_module);
  }


function_exit:

  if (pszName != NULL) MEM_free(pszName);
  if (pszValue != NULL) MEM_free(pszValue);
  if (iValues> 0) MEM_free(pszValues);
  if (iRels > 0) MEM_free(ptRels);

  return iRetCode;
}

int BNL_FEI_FRU_update_bom(tag_t tItem, tag_t tRev)
{
  int iRetCode      = 0;
  int b             = 0;
  int iOccsCount = 0;

  char szName[ITEM_name_size_c + 1];
  //char szDesc[ITEM_desc_size_c + 1];
  char szId[ITEM_id_size_c + 1];
  char szOccType[WSO_name_size_c + 1];

  char *pszItemType = NULL;
  char *pszSeq      = NULL;
  char *pszQuantity = NULL;

  tag_t tBv         = NULL_TAG;
  tag_t tBvr        = NULL_TAG;
  tag_t tItemCreated= NULL_TAG;
  tag_t tRevCreated = NULL_TAG;
  tag_t tUgGeoNoteType= NULL_TAG;
  tag_t tOccObject  = NULL_TAG;
  tag_t tChildBomView=NULL_TAG;

  tag_t *ptOccurences = NULL;
  tag_t *ptOccs     = NULL;

  logical lViewCreated = false;


  iRetCode = ITEM_ask_name(tItem, szName);
  if (BNL_em_error_handler("ITEM_ask_name", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  // Item has no description add this time
  //iRetCode = ITEM_ask_description(tItem, szDesc);
  //if (BNL_em_error_handler("ITEM_ask_description", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  iRetCode = ITEM_ask_id(tItem, szId);
  if (BNL_em_error_handler("ITEM_ask_description", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  BNL_tb_pref_ask_char_value(BNL_FEI_PREF_FRU_ITEMTYPE, IMAN_preference_site, &pszItemType);
  if (pszItemType == NULL)
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n", BNL_module, BNL_FEI_PREF_FRU_ITEMTYPE);
    goto function_exit;
  }

  if ((iRetCode = BNL_FEI_get_bomview(tItem, tRev, &tBv, &tBvr, &lViewCreated)) != 0) goto function_exit;

  if (!lViewCreated)
  {
    iRetCode = PS_list_occurrences_of_bvr(tBvr, &iOccsCount, &ptOccurences);
    if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    fprintf(feilog, "%s: Found [%d] occurences.\n ", BNL_module, iOccsCount);

    for (b =0; b < iOccsCount; b++)
    {
      iRetCode = PS_ask_occurrence_child(tBvr, ptOccurences[b], &tOccObject, &tChildBomView);
      if (BNL_em_error_handler("PS_ask_occurrence_child", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      iRetCode = BNL_tb_determine_object_type(tOccObject, szOccType);

      fprintf(feilog, "%s: Occurence type [%s].\n ", BNL_module, szOccType);

      if (strncmp(szOccType, pszItemType, strlen(pszItemType)) == 0)
      {
        fprintf(feilog, "%s: Removing occurence.\n ", BNL_module);

        iRetCode = PS_delete_occurrence(tBvr, ptOccurences[b]);
        if (BNL_em_error_handler("PS_delete_occurrence", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
      } // End of if (strncmp(szOccType, pszItemType, strlen(pszItemType)) == 0)
    } // End of for (b =0; b < iOccsCount; b++)
  } // End of if (!lViewCreated)

  if ((iRetCode = BNL_FEI_FRU_create_item(pszItemType, szId, szName, &tItemCreated, &tRevCreated)) != 0) goto function_exit;

  iRetCode = PS_create_occurrences(tBvr, tItemCreated, NULL_TAG, 1, &ptOccs);
  if (BNL_em_error_handler("PS_create_occurrences", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  BNL_tb_pref_ask_char_value(BNL_FEI_PREF_FRU_SEQ, IMAN_preference_site, &pszSeq);
  if (pszSeq == NULL)
  {
    iRetCode = USER_ask_new_seqno(tBvr, tItemCreated, &pszSeq);
    if (BNL_em_error_handler("USER_ask_new_seqno", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    fprintf(feilog, "Preference [%s] is not defined, using default value [%s].\n", BNL_FEI_PREF_FRU_SEQ, pszSeq);
  }
  iRetCode = PS_set_seq_no(tBvr, ptOccs[0], pszSeq);
  if (BNL_em_error_handler("PS_set_seq_no", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  // Quantity
  BNL_tb_pref_ask_char_value(BNL_FEI_PREF_FRU_QUANTITY, IMAN_preference_site, &pszQuantity);

  if (pszQuantity == NULL)
  {
    fprintf(feilog, "%s: Preference [%s] is not defined.\n", BNL_module, BNL_FEI_PREF_FRU_QUANTITY);
  }
  else
  {
    fprintf(feilog, "%s: Setting quantity to [%f].\n", BNL_module, atof(pszQuantity));

    iRetCode = PS_set_occurrence_qty(tBvr, ptOccs[0], atof(pszQuantity));
    if (BNL_em_error_handler("PS_set_occurrence_qty", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
  }

  iRetCode = PS_find_note_type("UG GEOMETRY", &tUgGeoNoteType);
  if (BNL_em_error_handler("PS_find_note_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = PS_set_occurrence_note_text(tBvr, ptOccs[0], tUgGeoNoteType, "NO");
  if (BNL_em_error_handler("PS_set_occurrence_note_text", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = AOM_save(tBvr);
  if (BNL_em_error_handler("AOM_save", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  // Unlock
  iRetCode = BNL_tb_load_object(tBvr, POM_no_lock);
  if (BNL_em_error_handler("BNL_tb_load_object (tBvr)", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BNL_tb_load_object(tBv, POM_no_lock);
  if (BNL_em_error_handler("BNL_tb_load_object (tBv)", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  if (glPortalReviseSaveAs)
  {
    fprintf(feilog, "%s: In Attachments window, postponing csp creation.\n", BNL_module);
    gtFruRev = tRevCreated;
  }
  else
  {
    if ((iRetCode = BNL_FEI_create_plot_request(tRevCreated, "FRU", 0, NULL)) != 0) goto function_exit;
  }


function_exit:

  // Unlock (2)
  if (tItemCreated != NULL_TAG)
  {
    iRetCode = BNL_tb_load_object(tItemCreated, POM_no_lock);
    if (BNL_em_error_handler("BNL_tb_load_object (tBvr)", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
  }

  if (tRevCreated != NULL_TAG)
  {
    iRetCode = BNL_tb_load_object(tRevCreated, POM_no_lock);
    if (BNL_em_error_handler("BNL_tb_load_object (tBv)", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
  }

  if (iOccsCount > 0) MEM_free(ptOccurences);
  if (pszItemType != NULL) MEM_free(pszItemType);
  if (pszSeq != NULL) MEM_free(pszSeq);
  if (pszQuantity != NULL) MEM_free(pszQuantity);
  if (ptOccs != NULL) MEM_free(ptOccs);

  return iRetCode;
}

int BNL_FEI_get_bomview(tag_t tItem, tag_t tRev, tag_t *ptBv, tag_t *ptBvr, logical *plViewCreated)
{
  int iRetCode      = 0;
  int iBomCount     = 0;
  int b             = 0;
  int iBomLineCount = 0;
  int iBvrs         = 0;

  char szType[WSO_name_size_c + 1];

  char *pszViewType = NULL;

  tag_t *ptBomViews = NULL;
  tag_t *ptBvrs     = NULL;

  tag_t tVwType     = NULL_TAG;


  *plViewCreated = false;


  BNL_tb_pref_ask_char_value("PSE_default_view_type", IMAN_preference_site, &pszViewType);
  if (pszViewType == NULL)
  {
    fprintf(feilog, "Warning, preference [PSE_default_view_type] is not defined.\n");
    goto function_exit;
  }

  iRetCode = PS_find_view_type(pszViewType, &tVwType);
  if (BNL_em_error_handler("PS_find_view_type", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  // Check for existing view
  iRetCode = ITEM_list_bom_views(tItem, &iBomCount, &ptBomViews);
  if (BNL_em_error_handler("ITEM_list_bom_views", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Found [%d] bomviews at target.\n",BNL_module, iBomCount);

  /* check for the bomview type*/
  for (b = 0; b < iBomCount; b++)
  {
    iRetCode = BNL_tb_determine_object_type(ptBomViews[b], szType);
    if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    fprintf(feilog, "%s: Found view type [%s].\n", BNL_module, szType);

    if (strcmp(pszViewType, szType) == 0)
    {
      *ptBv = ptBomViews[b];

      // Get the bvr
      iRetCode = PS_list_bvrs_of_bom_view(*ptBv, &iBvrs, &ptBvrs);
      if (BNL_em_error_handler("PS_list_bvrs_of_bom_view", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      if (iBvrs > 0)
      {
        *ptBvr = ptBvrs[0];
      }
      else
      {
        fprintf(feilog, "%s: Warning, failed to find the bvr.\n", BNL_module);
      }
      break;
    } // End of if (strcmp(pszBomType, szType) == 0)
  } // End of for (b = 0; b < iBomCount; b++)

  if (*ptBv == NULL_TAG)
  {
    // Create view
    iRetCode = PS_create_bom_view(tVwType, NULL, NULL, tItem, ptBv);
    if (BNL_em_error_handler("PS_create_bom_view", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    /* Create a BVR */
    iRetCode = PS_create_bvr(*ptBv, NULL, NULL, FALSE, tRev, ptBvr);
    if (BNL_em_error_handler("PS_create_bvr", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    iRetCode = AOM_save(*ptBv);
    if (BNL_em_error_handler("AOM_save (bv)", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    iRetCode = AOM_save(tItem);
    if (BNL_em_error_handler( "AOM_save (item)", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    iRetCode = AOM_save(*ptBvr);
    if (BNL_em_error_handler("AOM_save (bvr)", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    iRetCode = AOM_save(tRev);
    if (BNL_em_error_handler("AOM_save (rev)", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

    *plViewCreated = true;
  }

function_exit:

  if (iBomCount > 0) MEM_free(ptBomViews);
  if (iBvrs > 0) MEM_free(ptBvrs);
  if (pszViewType != NULL) MEM_free(pszViewType);

  return iRetCode;
}
