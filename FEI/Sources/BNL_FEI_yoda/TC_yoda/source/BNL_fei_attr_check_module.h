/**
 (C) copyright by Siemens Product Lifecylce Management Software (NL) B.V.


 Description  :  This module header file belongs to the yoda module .c file.
                 
 Created for  :  FEI

 Comments     :  

 Filename     :  BNL_fei_attr_check_module.h

 Version info :  release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 reason /description                        Version By                  date
 ----------------------------------------------------------------------------------------------------------
 Creation                                   0.1     J. Wijnja      26-03-2009
 Added BNL_FEI_DESC_IR_TYPES                0.2     J.M.           20-04-2009
 Added BNL_FEI_SET_ATTRIBUTES               0.3     J. Wijnja      30-06-2009
 Added BNL_FEI_empty_form_list              0.4     J.M.           14-05-2012

*/

#define BNL_FEI_PREF_TYPES          "FEI_VALIDATE_TYPES"
#define BNL_FEI_RESET               "FEI_RESET"
#define BNL_FEI_SET_ATTRIBUTES      "FEI_SET_ATTRIBUTES"
#define BNL_FEI_DESC_IR_TYPES       "FEI_IR_DESC_TYPES"
#define BNL_FEI_ATTR_LEN            "FEI_ATTR_LEN"
#define BNL_FEI_ATTR_ROLE           "FEI_ATTR_ROLE"
#define BNL_FEI_ATTR_SEC            "FEI_ATTR_SEC"
#define BNL_FEI_ATTR_CHARS          "FEI_ATTR_CHARS"

#define BNL_ITEMREVISION            "ItemRevision"
#define BNL_ITEM_MASTER             "Item Master"


int BNL_fei_attr_nx_pre_saveas(char *pszInputString);
int BNL_fei_attr_nx_post_saveas(char *pszInputString);
int BNL_fei_attr_nx_create_predecessor(char *pszInputString);

int BNL_FEI_split_multiple_values(char *pszValue, char chSeparator, int  *iCount, char ***pszValues);

int BNL_FEI_get_preference_name(char *pszIn, char *pszRefName);
int BNL_FEI_initialise(FILE *pLog);
int BNL_FEI_exit_module();
int BNL_FEI_wso_copy_post(METHOD_message_t *pmMessage,va_list args);
int BNL_FEI_validate_on_save(METHOD_message_t *pmMessage,va_list args);
int BNL_FEI_populate_item_desc(METHOD_message_t *pmMessage,va_list args);
int BNL_FEI_FRU_check(METHOD_message_t *pmMessage,va_list args);

int BNL_FEI_pre_revise(METHOD_message_t *pmMessage, va_list args);
int BNL_FEI_post_revise(METHOD_message_t *pmMessage, va_list args);
int BNL_FEI_post_copy(METHOD_message_t *pmMessage, va_list args);

int BNL_FEI_attr_update(METHOD_message_t *pmMessage,va_list args);

int BNL_FEI_get_IMF(tag_t tRIMF,tag_t *tIMF);
int BNL_FEI_get_IRMF(tag_t tIMF,tag_t *tIRMF);
int BNL_FEI_load_object(tag_t Target,int iLock,int *piOldLock);

int FEI_reset_form_attributes(tag_t tObject, int iCount, char **pszPrefs);
int FEI_set_attributes(tag_t tObject, int iCount, char **pszPrefs);
int FEI_check_attributes_length(tag_t tObject, int iCount, char **pszPrefs);
int FEI_check_attributes_role(tag_t tObject, int iCount, char **pszPrefs, logical lFromCspCheck);
int FEI_check_attributes_security(tag_t tObject, int iCount, char **pszPrefs);
int FEI_check_attributes_chars(tag_t tObject, int iCount, char **pszPrefs);

int BNL_FEI_add_form_to_list(void * pReturnValue);

int BNL_FEI_remove_form_from_list(void * pReturnValue);
logical BNL_FEI_is_object_in_list(tag_t tObject);

int BNL_FEI_get_master_form(tag_t tObject, tag_t *ptForm);

void BNL_FEI_empty_form_list();