/**
 (C) copyright by Siemens Product Lifecylce Management Software (NL) B.V.


 Description  :  This module header file belongs to the yoda module .c file.

 Created for  :  FEI

 Comments     :

 Filename     :  $Id: BNL_fei_yoda_module.h 411 2015-11-10 10:05:15Z tjonkonj $

 Version info :  release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 Reason / Description                       Version By                  Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                   0.1     J.M.                15-01-2010
 Added BNL_fei_yoda_grm_create_post         0.2     J.M.                15-02-2010
 Added new preferences                      0.3     J.M.                23-02-2010
 Changed define PREDECESSOR_RELATION        0.4     DTJ                 05-11-2015

*/

#define PREDECESSOR_RELATION        "CMSolutionToImpacted"
#define BNL_FEI_PREF_IMF_TYPES      "FEI_CSP_IMF_types"
#define BNL_FEI_PREF_REL_TYPES      "FEI_CSP_REL_types"
#define BNL_FEI_PREF_PROP_NAMES     "FEI_CSP_PROP_NAMES"
#define BNL_FEI_PREF_FRU_ITEM       "FEI_FRU_Commercial_Item"
#define BNL_FEI_PREF_FRU_SUBCLASS   "FEI_FRU_Commercial_Subclass"
#define BNL_FEI_PREF_FRU_SC_VALUES  "FEI_FRU_Commercial_Subclass_values"
#define BNL_FEI_PREF_FRU_ITEMTYPE   "FEI_FRU_FRUList_Item"
#define FEI_FRU_FUNCTIONAL_EMAIL    "FEI_FRU_Functional_email"
#define BNL_FEI_PREF_FRU_USER       "FEI_FRU_owning_user"
#define BNL_FEI_PREF_FRU_GROUP      "FEI_FRU_owning_group"
#define BNL_FEI_PREF_FRU_STATUS     "FEI_FRU_status"
#define BNL_FEI_PREF_FRU_SEQ        "FEI_FRU_seq"
#define BNL_FEI_PREF_FRU_QUANTITY   "FEI_FRU_quantity"
#define BNL_FEI_PREF_FRU_CSP        "FEI_CSP_INI_FRU"
#define BNL_FEI_PREF_RTP_BVR        "FEI_RTP_BVR_name"


int BNL_assign_predeccesor_relation(tag_t tSuccesor, tag_t tPredeccesor);

int BNL_FEI_yoda_initialise(FILE *pLog);
int BNL_FEI_yoda_exit_module();

/**
int BNL_fei_rtp_property(METHOD_message_t *message, va_list args)

  Description:

  Create the runtime property.

*/
int BNL_fei_yoda_rtp_property();
int BNL_fei_yoda_rtp_bvr();


int BNL_fei_yoda_check_bom(METHOD_message_t *pmMessage, va_list args);

int BNL_fei_yoda_add_to_list(void * pReturnValue);
int BNL_fei_yoda_remove_from_list(void * pReturnValue);
int BNL_FEI_yoda_create_predecessor(void * pReturnValue);
int BNL_FEI_check_for_csp(void * pReturnValue);

/**
int BNL_fei_yoda_check_uom
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:
    Checks the selected UOM value is valid for this item type

  Returns:

    ITK_ok or !ITK_ok

*/
int BNL_fei_yoda_check_uom(METHOD_message_t *pmMessage, va_list args);

/**
int BNL_fei_yoda_check_uom
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:
    Checks the selected UOM value is valid for this item type

  Returns:

    ITK_ok or !ITK_ok

*/
int BNL_fei_yoda_check_uom
(
  METHOD_message_t *pmMessage,
  va_list args
);

/**
int BNL_fei_yoda_deep_copy_post_action(METHOD_message_t *m, va_list args)

  Description:

  BNL_fei_yoda_deep_copy_post_action.

*/
//int BNL_fei_yoda_deep_copy_post_action(METHOD_message_t *m, va_list args);

int BNL_fei_yoda_grm_create_pre(METHOD_message_t *pmMessage, va_list args);
int BNL_fei_yoda_grm_delete_pre(METHOD_message_t *pmMessage, va_list args);
int BNL_fei_yoda_grm_create_post(METHOD_message_t *pmMessage, va_list args);
int BNL_fei_yoda_pre_saveas(METHOD_message_t *pmMessage, va_list args);
int BNL_fei_yoda_post_saveas(METHOD_message_t *pmMessage, va_list args);


int BNL_FEI_imf_csp_set_attr_names();

/**
int BNL_FEI_imf_csp_init
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:

    Pre action on the save IMF's for csp generation

  Returns:

*/
int BNL_FEI_imf_csp_init
(
  METHOD_message_t *pmMessage,
  va_list args
);


/**
int BNL_FEI_imf_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:

    Post action on the save IMF's for csp generation

  Returns:

*/
int BNL_FEI_imf_csp
(
  METHOD_message_t *pmMessage,
  va_list args
);

/**
int BNL_FEI_item_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:

    Post action on the save of Item

  Returns:

*/
int BNL_FEI_item_csp
(
  METHOD_message_t *pmMessage,
  va_list args
);


/**
int BNL_FEI_itemrev_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:

    Post action on the save of ItemRevision

  Returns:

*/
int BNL_FEI_itemrev_csp
(
  METHOD_message_t *pmMessage,
  va_list args
);


/**
int BNL_FEI_rel_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:

    Post action on the relation creation for csp generation

  Returns:

*/
int BNL_FEI_rel_csp
(
  METHOD_message_t *pmMessage,
  va_list args
);

/**
int BNL_FEI_prop_csp
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:
    Post action on the update of an property for csp generation

  Returns:

    ITK_ok or !ITK_ok

*/
int BNL_FEI_prop_csp
(
  METHOD_message_t *pmMessage,
  va_list args
);


int BNL_FEI_item_create
(
  METHOD_message_t *pmMessage,
  va_list args
);

int BNL_FEI_item_saveas
(
  METHOD_message_t * msg,
  va_list args
);
