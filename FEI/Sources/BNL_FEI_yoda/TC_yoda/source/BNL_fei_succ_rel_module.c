/**
 (C) copyright by Siemens Product Lifecylce Management Software (NL) B.V.


 Description    :   BNL successor module, module for creating the successor relation.

 Created for    :   FEI

 Comments       :   Every new procedure or function in this modules starts
                    with BNL_

 Filename       :   $Id: BNL_fei_succ_rel_module.c 449 2016-06-28 09:00:09Z tjonkonj $


 History of changes
 Reason /description                                                    Version     By          Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.1         JW          01-04-2009
 Added check for owning site                                            0.2         JM          10-04-2009
 Disable creation to itself                                             0.3         JM          24-04-2009
 Fixed memory issue                                                     0.4         JM          03-07-2009
 Skipped creation of successor relation when already present            0.5         JM          14-01-2009
 Fixed issue with mis-alignement                                        0.6         JM          02-03-2010
 Updated for new successor/predecessor behavior                         0.7         JM          30-03-2010
 Added BNL_check_for_successor_relation                                 0.8         JM          03-09-2010
 Added error handling                                                   0.9         JM          08-11-2011
 Update for TC 10.1.*                                                   1.0         DTJ         31-05-2016

*/
#include <stdlib.h>

#ifdef  TC10_1
#include <tc/iman.h>
#include <tccore/aom_prop.h>
#else
#include <iman.h>
#include <item.h>
#include <bom.h>
#include <ps.h>
#include <lov.h>
#include <aom_prop.h>
#include <user_server_exits.h>
#endif

#include <BNL_register_bnllibs.h>
#include <BNL_tb_module.h>
#include <BNL_em_module.h>
#include <BNL_fm_module.h>
#include <BNL_xml_module.h>
#include <BNL_strings_module.h>
#include <BNL_errors.h>

#include <BNL_fei_succ_rel_module.h>
#include <BNL_register_yoda.h>

static char BNL_module[] = "BNL FEI SUCC REL 1.0";

tag_t gtItemSaveAs       = NULL_TAG;
//tag_t gtPrimaryItem      = NULL_TAG;
//tag_t gtSecondaryItem    = NULL_TAG;

FILE *feilog;


int BNL_FEI_succ_rel_initialise(FILE *pLog)
{
  int iRetCode        = 0;

  feilog = pLog;

  return iRetCode;
}


int BNL_FEI_succ_rel_module_exit()
{
  int iRetCode        = 0;

  return iRetCode;
}


/**
int BNL_FEI_create_successor
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:

    Pre action on the create of the predecessor to create successor relation.

  Returns:

  0 on Ok

*/
int BNL_FEI_create_successor
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;

  char *pszAttrVal     = NULL;
  char *pszPrimItemId  = NULL;
  char *pszSecondItemId = NULL;

  char szType[WSO_name_size_c + 1];
  char szMessg[256];

  tag_t tPrimRevObject     = NULL_TAG;
  tag_t tSecondRevObject   = NULL_TAG;

  tag_t tPrimItemObject     = NULL_TAG;
  tag_t tSecondItemObject   = NULL_TAG;

  WSO_description_t tDescription;

  fprintf(feilog, "%s: Begin ... BNL_FEI_create_successor\n", BNL_module);

  // Should be skipped when "Defined Attach Data" in Portal
  //if (gtItemSaveAs != NULL_TAG)
//  if (glPortalReviseSaveAs)
//  {
//    fprintf(feilog, "%s: Attachments window, skipping creation of successor.\n", BNL_module);
//    goto function_exit;
//  }

  tPrimRevObject = va_arg(args, tag_t);    /* Primary object */
  tSecondRevObject = va_arg(args, tag_t);  /* Secondary object */

  //fprintf(feilog, "Object tag [%d].\n", tObject);

  iRetCode = BNL_tb_determine_object_type(tSecondRevObject, szType);
  if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Type of secondary object [%s].\n", BNL_module,szType);

  iRetCode = ITEM_ask_item_of_rev(tSecondRevObject, &tSecondItemObject);
  if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = ITEM_ask_item_of_rev(tPrimRevObject, &tPrimItemObject);
  if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  AOM_UIF_ask_value(tPrimRevObject, "item_id", &pszPrimItemId);
  //fprintf(feilog, "%s: item id of the secondary object [%s].\n",BNL_module,pszAttrVal);

  AOM_UIF_ask_value(tSecondRevObject, "item_id", &pszSecondItemId);
  //fprintf(feilog, "%s: item id of the secondary object [%s].\n",BNL_module,pszAttrVal);

  fprintf(feilog, "%s: Id primary object [%d] [%s], id secondary object [%d] [%s] \n", BNL_module, tPrimItemObject, pszPrimItemId, tSecondItemObject ,pszSecondItemId );

  if (glPortalReviseSaveAs)
  {
    fprintf(feilog, "%s: Attachments window, skipping creation of successor.\n", BNL_module);
    goto function_exit;
  }

  iRetCode = WSOM_describe(tSecondItemObject, &tDescription);
  if (BNL_em_error_handler("WSOM_describe", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Owning site secondary object item [%s].\n", BNL_module, tDescription.owning_site_name);

  if (tSecondItemObject != tPrimItemObject)
  {
    if (tDescription.owning_site_name[0]!=' ')
    {
      //fprintf(feilog, "%s: Error, owning site predecessor with id [%s] is [%s].\n", BNL_module, pszSecondItemId, tDescription.owning_site_name);
      //sprintf(szMessg, "Failed to create relations. Owning site predecessor with id [%s] is [%s].\n", pszSecondItemId, tDescription.owning_site_name);

      sprintf(szMessg, "The successor/predecessor relation cannot be created, Item \"%s\" is a remote object.", pszSecondItemId);
      fprintf(feilog, "%s: %s\n", BNL_module, szMessg);

      iRetCode = BNL_error_message + giErrorOffset;
      BNL_em_error_handler_s1(NULL, BNL_module, EMH_severity_error, iRetCode, true, szMessg);
      return iRetCode;
    }
    else
    {
      /*if (tSecondItemObject == gtItemSaveAs && glFromPortal)
      {
        fprintf(feilog, "%s: Saveas within portal, creation of successor relation will be skipped.\n", BNL_module);
        //gtPrimaryItem = tPrimItemObject;
        //gtSecondaryItem = tSecondItemObject;
      }
      else
      {*/
        if ((iRetCode = BNL_assign_successor_relation(tSecondItemObject, tPrimItemObject)) != 0) goto function_exit;

        fprintf(feilog, "%s: The successor relation is created between item [%s] and item [%s] \n", BNL_module, pszPrimItemId ,pszSecondItemId );
      //}
    }
  }
  else
  {
    fprintf(feilog, "%s: No need to create the successor relation between item [%s] and item [%s] \n", BNL_module, pszPrimItemId ,pszSecondItemId );
  }

function_exit:

  fprintf(feilog, "%s: End ... BNL_FEI_create_successor \n", BNL_module);

  if (pszAttrVal != NULL) MEM_free(pszAttrVal);

  fprintf(feilog, "\n");

  return iRetCode;
}

/**
int BNL_FEI_delete_successor
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:

    Pre action on the delete of the predecessor to delete successor relation.

  Returns:

  0 on Ok

*/
int BNL_FEI_delete_successor
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;

  char *pszAttrVal     = NULL;
  char *pszPrimItemId  = NULL;
  char *pszSecondItemId = NULL;

  char szType[WSO_name_size_c + 1];
  char szMessg[256];

  tag_t tPrimRevObject     = NULL_TAG;
  tag_t tSecondRevObject   = NULL_TAG;

  tag_t tRel               = NULL_TAG;

  tag_t tPrimItemObject    = NULL_TAG;
  tag_t tSecondItemObject  = NULL_TAG;

  logical lExist           = false;

  WSO_description_t tDescription;

  fprintf(feilog, "%s: Begin ... BNL_FEI_delete_successor \n", BNL_module);

  tRel = va_arg(args, tag_t);    /* Relation object */

  iRetCode = BNL_tb_determine_object_type(tRel, szType);
  if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Type of relation object [%s].\n", BNL_module,szType);

  iRetCode = GRM_ask_primary(tRel,&tPrimRevObject);
  if (BNL_em_error_handler("GRM_ask_primary", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = GRM_ask_secondary(tRel,&tSecondRevObject);
  if (BNL_em_error_handler("GRM_ask_primary", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  AOM_UIF_ask_value(tPrimRevObject, "item_id", &pszPrimItemId);
  //fprintf(feilog, "%s: itam id of the secondary object [%s].\n",BNL_module,pszAttrVal);

  AOM_UIF_ask_value(tSecondRevObject, "item_id", &pszSecondItemId);
  //fprintf(feilog, "%s: itam id of the secondary object [%s].\n",BNL_module,pszAttrVal);

  fprintf(feilog, "%s: Id primary object [%s], id secondary object [%s] \n", BNL_module, pszPrimItemId ,pszSecondItemId );

  iRetCode = ITEM_ask_item_of_rev(tSecondRevObject, &tSecondItemObject);
  if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = ITEM_ask_item_of_rev(tPrimRevObject, &tPrimItemObject);
  if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  // First check if removal is needed
  iRetCode = BNL_check_for_successor_relation(tSecondItemObject, tPrimItemObject, &lExist);
  if (BNL_em_error_handler("BNL_check_for_successor_relation", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  if (!lExist)
  {
    fprintf(feilog, "%s: No need to remove successor relation.\n", BNL_module);
    goto function_exit;
  }


  iRetCode = WSOM_describe(tSecondItemObject, &tDescription);
  if (BNL_em_error_handler("WSOM_describe", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Owning site secondary object item [%s].\n", BNL_module, tDescription.owning_site_name);

  if (tDescription.owning_site_name[0]!=' ')
  {
    fprintf(feilog, "%s: Error, owning site predecessor with id [%s] is [%s].\n", BNL_module, pszSecondItemId,  tDescription.owning_site_name);

    sprintf(szMessg, "Failed to remove relations. Owning site predecessor with id [%s] is [%s].\n", pszSecondItemId, tDescription.owning_site_name);
    iRetCode = BNL_error_message + giErrorOffset;
    BNL_em_error_handler_s1(NULL, BNL_module, EMH_severity_error, iRetCode, true, szMessg);

    goto function_exit;
  }

  fprintf(feilog, "tSecondItemObject [%d] gtItemSaveAs [%d] glPortalReviseSaveAs [%d]\n", tSecondItemObject, gtItemSaveAs, glPortalReviseSaveAs);

  //if (tSecondItemObject == gtItemSaveAs)
  if (glPortalReviseSaveAs)
  {
    fprintf(feilog, "%s: Removal of successor relation will be cancelled.\n", BNL_module);
    //gtPrimaryItem = NULL_TAG;
    //gtSecondaryItem = NULL_TAG;
  }

  iRetCode = BNL_delete_successor_relation(tSecondItemObject, tPrimItemObject);
  if (BNL_em_error_handler("BNL_delete_successor_relation", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: The successor relation is deleted between item [%s] and item [%s] \n", BNL_module, pszPrimItemId ,pszSecondItemId );


function_exit:

  fprintf(feilog, "%s: End ... BNL_FEI_delete_successor \n", BNL_module);

  if (pszAttrVal != NULL) MEM_free(pszAttrVal);
  if (pszPrimItemId != NULL) MEM_free(pszPrimItemId);
  if (pszSecondItemId != NULL) MEM_free(pszSecondItemId);


  fprintf(feilog, "\n");

  return iRetCode;
}

/**
int BNL_assign_successor_relation
(
  tag_t tPrim,
  tag_t tSecond
)

  Description:

    Creates the successor relation between two item.

  Returns:

  0 on Ok

*/
int BNL_assign_successor_relation
(
  tag_t tPrim,
  tag_t tSecond
)
{
  int iRetCode            =0;
  int iCount              =0;
  int k                   =0;

  tag_t *ptSuccessors      =NULL;
  tag_t tRelType           =NULL_TAG;
  tag_t tRelation          =NULL_TAG;

  char *pszRel           = NULL;

  char szMessg[1024];
  char szItemId[ITEM_id_size_c + 1];


  fprintf(feilog,"%s: Begin... BNL_assign_successor_relation \n", BNL_module);


  BNL_tb_pref_ask_char_value(BNL_FEI_SUCCESSOR_REL, IMAN_preference_site, &pszRel);
  fprintf(feilog, "%s: Found preference value [%s] for [%s].\n", BNL_module, pszRel, BNL_FEI_SUCCESSOR_REL);

  if (pszRel == NULL)
  {
    pszRel = MEM_alloc(sizeof(char)*33);
    strcpy(pszRel, DEFAULT_SUCCRELATION);
  }

  iRetCode=GRM_find_relation_type(pszRel,&tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type",BNL_module,EMH_severity_user_error,iRetCode ,true)) goto function_exit;

  if (tRelType==NULL_TAG)
  {
    fprintf(feilog, "%s: Error, did not find the relation [%s] in the database\n",BNL_module, pszRel);
    iRetCode = 1;
    goto function_exit;
  }

  iRetCode = GRM_list_secondary_objects_only(tPrim,tRelType,&iCount,&ptSuccessors);
  if (BNL_em_error_handler("GRM_list_secondary_objects_only",BNL_module,EMH_severity_user_error,iRetCode ,true)) goto function_exit;


  /* If there is already a successor skip creation.*/
  if (iCount > 0)
  {
    // Add error to the stack
    iRetCode = ITEM_ask_id(tPrim, szItemId);
    if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_user_error, iRetCode ,true)) goto function_exit;

    sprintf(szMessg, "The successor/predecessor relation cannot be created, successor already defined for Item \"%s\".", szItemId);
    fprintf(feilog, "%s: %s\n", BNL_module, szMessg);

    iRetCode = BNL_error_message + giErrorOffset;
    BNL_em_error_handler_s1(NULL, BNL_module, EMH_severity_error, iRetCode, true, szMessg);

    goto function_exit;

    //iRetCode=GRM_find_relation(tPrim,ptSuccessors[0],tRelType,&tRelation);
    //if (BNL_em_error_handler("GRM_find_relation",BNL_module,EMH_severity_user_error,iRetCode , true)) return iRetCode;

    //iRetCode=GRM_delete_relation(tRelation);
    //if (BNL_em_error_handler("GRM_delete_relation",BNL_module,EMH_severity_user_error,iRetCode , true)) return iRetCode;

    //tRelation=NULL_TAG;
    //fprintf(feilog,"%s: The previous successor relation is deleted\n", BNL_module);
  }
  else
  {
    /*just to be secure check if the relation not already exists*/
    //iRetCode=GRM_find_relation(tPrim,tSecond,tRelType,&tRelation);
    //if (BNL_em_error_handler("GRM_find_relation",BNL_module,EMH_severity_user_error,iRetCode , true)) return iRetCode;

  //if (tRelation==NULL_TAG)
  //{
    /*create the predeccesor relation*/
    iRetCode=GRM_create_relation(tPrim,tSecond,tRelType,NULLTAG,&tRelation);
    if (BNL_em_error_handler("GRM_create_relation",BNL_module,EMH_severity_user_error,iRetCode , true)) goto function_exit;

    iRetCode=GRM_save_relation(tRelation);
    if (BNL_em_error_handler("GRM_save_relation",BNL_module,EMH_severity_user_error,iRetCode , true)) goto function_exit;

    fprintf(feilog, "%s: Successor relation has been created.\n", BNL_module);
  }

  fprintf(feilog,"%s: End... BNL_assign_successor_relation \n",BNL_module);

function_exit:

  if (iCount > 0) MEM_free(ptSuccessors);
  if (pszRel != NULL) MEM_free(pszRel);

  return iRetCode;
}

/**
int BNL_delete_successor_relation
(
  tag_t tPrim,
  tag_t tSecond
)

  Description:

    Deletes the successor relation between two item.

  Returns:

  0 on Ok

*/
int BNL_delete_successor_relation
(
  tag_t tPrim,
  tag_t tSecond
)
{
    int iRetCode            =0;
    int iCount              =0;

    int ifails              =0;
    int k                   =0;

    int *pifails;
    int *piSeverities;

    char **ppszTexts    = NULL;

    char *pszRel        = NULL;

    char szMessg[1024] ="";

    tag_t tRelType           =NULLTAG;
    tag_t tRelation          =NULLTAG;

    fprintf(feilog,"%s: Begin... BNL_delete_successor_relation \n",BNL_module);

    PREF_set_search_scope(IMAN_preference_site);

    BNL_tb_pref_ask_char_value(BNL_FEI_SUCCESSOR_REL, IMAN_preference_site, &pszRel);
    fprintf(feilog, "%s: Found preference value [%s] for [%s].\n", BNL_module, pszRel, BNL_FEI_SUCCESSOR_REL);

    if (pszRel == NULL)
    {
      pszRel = MEM_alloc(sizeof(char)*33);
      strcpy(pszRel, DEFAULT_SUCCRELATION);
    }

    iRetCode=GRM_find_relation_type(pszRel,&tRelType);
    if (BNL_em_error_handler("GRM_find_relation_type",BNL_module,EMH_severity_user_error,iRetCode , true)) return iRetCode;

    if (tRelType==NULLTAG)
    {
      fprintf(feilog,"%s: Error, did not find the relation [%s] in the database\n",BNL_module, pszRel);
      return 1;
    }

    /*just to be secure check if the realtion not already exists*/
    iRetCode=GRM_find_relation(tPrim,tSecond,tRelType,&tRelation);
    if (BNL_em_error_handler("GRM_find_relation",BNL_module,EMH_severity_user_error,iRetCode , true)) return iRetCode;

    if (tRelation!=NULLTAG)
    {
      iRetCode=GRM_delete_relation(tRelation);
      if (iRetCode != 0)
      {
        EMH_ask_errors (&ifails, &piSeverities, &pifails, &ppszTexts);
        for (k=0; k<ifails; k++)
        {
          if (pifails[k] == iRetCode)
          {
            sprintf(szMessg, "%s.\n", ppszTexts[k]);
            BNL_em_error_handler_s1(NULL, BNL_module, EMH_severity_error, iRetCode, true, szMessg);
            return iRetCode;
          }
        }
      }
      tRelation=NULLTAG;

    }

    fprintf(feilog,"%s: End... BNL_delete_successor_relation \n", BNL_module);

    if (pszRel != NULL) MEM_free(pszRel);

    return iRetCode;
}

int BNL_check_for_successor_relation
(
  tag_t tPrim,
  tag_t tSecond,
  logical *plExist
)
{
  int iRetCode            =0;

  char *pszRel        = NULL;

  tag_t tRelType           =NULLTAG;
  tag_t tRelation          =NULLTAG;


  fprintf(feilog,"%s: Begin... BNL_check_for_successor_relation.\n", BNL_module);

  *plExist = false;

  BNL_tb_pref_ask_char_value(BNL_FEI_SUCCESSOR_REL, IMAN_preference_site, &pszRel);
  fprintf(feilog, "%s: Found preference value [%s] for [%s].\n", BNL_module, pszRel, BNL_FEI_SUCCESSOR_REL);

  if (pszRel == NULL)
  {
    pszRel = MEM_alloc(sizeof(char)*33);
    strcpy(pszRel, DEFAULT_SUCCRELATION);
  }

  iRetCode = GRM_find_relation_type(pszRel, &tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  if (tRelType==NULLTAG)
  {
    fprintf(feilog,"%s: Error, did not find the relation [%s] in the database\n",BNL_module, pszRel);
    return 1;
  }

  /*just to be secure check if the realtion not already exists*/
  iRetCode=GRM_find_relation(tPrim, tSecond, tRelType, &tRelation);
  if (BNL_em_error_handler("GRM_find_relation", BNL_module, EMH_severity_user_error ,iRetCode, true)) goto function_exit;

  if (tRelation!=NULLTAG)
  {
    *plExist = true;
  }

function_exit:

   fprintf(feilog,"%s: End... BNL_check_for_successor_relation \n", BNL_module);

   if (pszRel != NULL) MEM_free(pszRel);

   return iRetCode;
}

/*
int BNL_FEI_succ_pre_saveas(void * pReturnValue)
{
	int iRetCode				      = 0;

  tag_t tComp               = NULL_TAG;
  tag_t tItem               = NULL_TAG;

  fprintf(feilog, "*** In BNL_FEI_succ_pre_saveas (true).\n");

  glPortalReviseSaveAs = true;

  /*iRetCode = USERARG_get_tag_argument(&tComp);
	if (BNL_em_error_handler("USERARG_get_tag_argument", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "*** Component [%d].\n", tComp);

  if (tComp != NULL_TAG)
  {
    iRetCode = ITEM_ask_item_of_rev(tComp, &tItem);
    if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    fprintf(feilog, "*** Item [%d].\n", tItem);

    gtItemSaveAs = tItem;
  }
  else
  {
    gtItemSaveAs = NULL_TAG;
  }*/

/*function_exit:


	fprintf(feilog,"*** End BNL_FEI_succ_pre_saveas.\n\n");

	return iRetCode;
}


int BNL_FEI_succ_post_saveas(void * pReturnValue)
{
	int iRetCode				      = 0;

  tag_t tComp               = NULL_TAG;
  tag_t tItem               = NULL_TAG;

  fprintf(feilog, "*** In BNL_FEI_succ_post_saveas (false).\n");

  glPortalReviseSaveAs = false;

  /*iRetCode = USERARG_get_tag_argument(&tComp);
	if (BNL_em_error_handler("USERARG_get_tag_argument", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "*** Component [%d].\n", tComp);

  /*if (tComp != NULL_TAG)
  {
    iRetCode = ITEM_ask_item_of_rev(tComp, &tItem);
    if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    fprintf(feilog, "*** Item [%d].\n", tItem);

    if (tItem == gtItemSaveAs && gtPrimaryItem != NULL_TAG && gtSecondaryItem != NULL_TAG)
    {
      iRetCode = BNL_assign_successor_relation(gtSecondaryItem, gtPrimaryItem);
      if (BNL_em_error_handler("BNL_assign_successor_relation", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
    }
  }*/

/*function_exit:

  //gtItemSaveAs = NULL_TAG;
  //gtPrimaryItem = NULL_TAG;
  //gtSecondaryItem = NULL_TAG;

	fprintf(feilog,"*** End BNL_FEI_succ_post_saveas.\n\n");

	return iRetCode;
}*/

