/**
 (C) copyright by Siemens PLM Software


 Description	:   This file is to register the fei exits.

 Created for	:   FEI

 Comments		  :   Every new procedure or function in this module starts with BNL_<module>_

 Filename     :   $Id: BNL_register_yoda.c 449 2016-06-28 09:00:09Z tjonkonj $

 Version info :   release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 reason /description                     Version By                  date
 ----------------------------------------------------------------------------------------------------------
 Creation                                0.1     J. Mansvelders      30-02-2009
 Added the preference search scope       0.2     J. Mansvelders      02-04-2009
 The preference to validate the BOM
 is changed to multiple values           0.3     J.Wijnja            22-05-2009
 Added functionality UOM validation      0.4     J.Wijnja            27-05-2009
 Updated UOM validation                  0.5     J.M                 05-06-2009
 Updated for predecessor creation        0.6     J.M                 01-07-2009
 Removed BNL_yoda_invoke_pdm
  (transfered to attr check)             0.7     J.M.                19-10-2009
 Update BNL_fei_check_attr_value         0.9     J.M.                19-10-2009
 Added runtime property                  1.0     J.M                 13-11-2009
 Made bom check multi level              1.1     J.M                 01-12-2009
 Added check for bypass                  1.2     J.M                 09-12-2009
 Updated BNL_FEI_check_bomlines          1.3     J.M                 18-12-2009
 Adding post actions for csp creation    1.4     J.M                 22-01-2010
 Adding post actions for rel creation    1.5     J.M                 15-02-2010
 Added new post action for fru func.     1.6     J.M                 25-02-2010
 Removed successor customization         1.7     J.M.                02-03-2010
 Fixed issue with BNL_fei_yoda_check_bom 1.8     J.M.                03-03-2010
 Updated for new successor/predecessor
  behavior                               1.9     J.M.                30-03-2010
 Updated BNL_register_fei_web            2.0     J.M.                06-04-2010
 Updated saveas post action              2.1     J.M.                16-07-2010
 Added new runtime property
  and registered saveas actions          2.2     J.M.                16-09-2010
 Changed check for the save of bom       2.3     J.M.                09-09-2010
 Fixed memory issue (pszItemType)        2.4     J.M.                03-11-2011
 Fixed issue with csp prop               2.5     J.M.                05-12-2011
 Fix for IR 1886869                      2.6     J.M.                23-03-2012
 Updated for IR 7520879                  2.8     JWO                 21-10-2015
 changed DEFAULT_PREDRELATION value      2.9     D.Tjon              04-11-2015
 Update for TC 10.1.*                    3.0     D.Tjon              31-05-2016
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef  TC10_1
#include <tc/iman.h>
#include <user_exits/user_exits.h>
#include <server_exits/user_server_exits.h>
#include <tccore/custom.h>
#include <tccore/imantype.h>
#include <bom/bom_msg.h>
#include <tccore/iman_msg.h>
#include <tccore/grm_msg.h>
#include <tccore/item_msg.h>
#include <tccore/wso_msg.h>
#include <tccore/aom_prop.h>
#else
#include <iman.h>
#include <imantype.h>
#include <ict_userservice.h>
#include <custom.h>
#include <user_exits.h>
#include <user_server_exits.h>
#include <preferences.h>
#include <nr.h>
#include <nr_errors.h>
#include <iman_msg.h>
#include <lov.h>
#include <aom_prop.h>
#include <item_msg.h>
#include <grm_msg.h>
#include <bom_msg.h>
#include <wso_msg.h>
#include <dataset_msg.h>
#endif

#include <BNL_register_bnllibs.h>
#include <BNL_em_module.h>
#include <BNL_tb_module.h>
#include <BNL_fm_module.h>
#include <BNL_strings_module.h>
#include <BNL_errors.h>

#include <BNL_fei_attr_check_module.h>
#include <BNL_fei_yoda_module.h>
#include <BNL_fei_succ_rel_module.h>
#include <BNL_register_yoda.h>

#define BNL_FEI_RELS                  "FEI_REL_names"

static char BNL_module[] = "BNL YODA 3.0";
char DLL_module[] = "BNL_YODA";

//logical glPortalReviseSaveAs;

/*because the length of filename is truncated we can not use dllmodule as name :-( */
char DLL_logfilename[] = "BNLYODA";

DLLAPI int BNL_register_fei_rtp(int *decision, va_list args);
DLLAPI int BNL_register_invoke_pdm(int *decision, va_list args);
DLLAPI int BNL_exit_bnl_yoda(int *decision, va_list args);
DLLAPI int BNL_register_fei_se(int *decision, va_list args);
DLLAPI int BNL_register_fei_user_init(int *decision, va_list args);
DLLAPI int BNL_register_fei_web(int *decision, va_list args);

int BNL_FEI_set_in_portal_flag(void * pReturnValue);
int BNL_FEI_set_in_portal_revise_flag(void * pReturnValue);

void welcome()
{
  if (giErrorLevel < 10)
  {
	  fprintf(stdout,"*******************************\n");
	  fprintf(stdout,"Copyright Siemens PLM Software BENELUX 2009-2015\n");
	  fprintf(stdout,"Version %s registered\n",BNL_module);
	  fprintf(stdout,"*******************************\n");
  }

  IMAN_write_syslog("*******************************\n");
	IMAN_write_syslog("Copyright Siemens PLM Software BENELUX 2009-2015\n");
	IMAN_write_syslog("Version %s registered\n",BNL_module);
	IMAN_write_syslog("*******************************\n");
}


/*
DLLAPI int BNL_YODA_register_callbacks()

  Description:

  The dll entry called by tceng registering.

*/
DLLAPI int BNL_YODA_register_callbacks()
{
	int iRetCode            = 0;

  char *pszPreventUserId  = NULL;


  BNL_register_bnllibs(DLL_module, BNL_module, DLL_logfilename, &ugslog);
  BNL_FEI_initialise(ugslog);
  BNL_FEI_succ_rel_initialise(ugslog);
  BNL_FEI_yoda_initialise(ugslog);
	welcome();

  glPortalReviseSaveAs = false;
  gtIRMF = NULL_TAG;
  gtIMF = NULL_TAG;

  CUSTOM_register_exit(DLL_module, "USERSERVICE_register_methods", (CUSTOM_EXIT_ftn_t)BNL_register_fei_se);
  CUSTOM_register_exit(DLL_module, "USER_invoke_pdm_server", (CUSTOM_EXIT_ftn_t)BNL_register_invoke_pdm);
  CUSTOM_register_exit(DLL_module, "USER_register_properties", (CUSTOM_EXIT_ftn_t)BNL_register_fei_rtp);
  CUSTOM_register_exit(DLL_module, "USER_init_module", (CUSTOM_EXIT_ftn_t)BNL_register_fei_user_init);
  CUSTOM_register_exit(DLL_module, "USER_invoke_user_code_taglist", (CUSTOM_EXIT_ftn_t)BNL_register_fei_web);
  CUSTOM_register_exit(DLL_module, "USER_exit_module", (CUSTOM_EXIT_ftn_t)BNL_exit_bnl_yoda);

  return iRetCode;
}

/**
int BNL_register_fei_se(int *decision, va_list args)

  Registers the portal ui interface callbacks.

*/
int BNL_register_fei_se(int *decision, va_list args)
{
	int iRetCode         = ITK_ok;
  int *piArgTypes      = NULL;
  int iNumberOfArgs    = 0;

  char *pszTemp        =NULL;


	*decision = ALL_CUSTOMIZATIONS;

  iNumberOfArgs = 1;
  piArgTypes = (int *) MEM_alloc( iNumberOfArgs * sizeof (int ) );
  piArgTypes[0] = USERARG_STRING_TYPE; // dummy
  iRetCode = USERSERVICE_register_method("BNL_check_for_csp",
                                BNL_FEI_check_for_csp,
                                iNumberOfArgs,
                                piArgTypes,
                                USERARG_VOID_TYPE);
  MEM_free(piArgTypes);
  if (BNL_em_error_handler("USERSERVICE_register_method", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iNumberOfArgs = 2;
  piArgTypes = (int *) MEM_alloc( iNumberOfArgs * sizeof (int ) );
  piArgTypes[0] = USERARG_TAG_TYPE; // selected component
  piArgTypes[1] = USERARG_STRING_TYPE; // dummy
  iRetCode = USERSERVICE_register_method("BNL_create_predecessor",
                                BNL_FEI_yoda_create_predecessor,
                                iNumberOfArgs,
                                piArgTypes,
                                USERARG_VOID_TYPE);
  MEM_free(piArgTypes);
  if (BNL_em_error_handler("USERSERVICE_register_method", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iNumberOfArgs = 2;
  piArgTypes = (int *) MEM_alloc( iNumberOfArgs * sizeof (int ) );
  piArgTypes[0] = USERARG_LOGICAL_TYPE; // true or false
  piArgTypes[1] = USERARG_STRING_TYPE; // dummy
  iRetCode = USERSERVICE_register_method("BNL_set_in_portal_flag",
                                BNL_FEI_set_in_portal_flag,
                                iNumberOfArgs,
                                piArgTypes,
                                USERARG_VOID_TYPE);

  MEM_free(piArgTypes);
  if (BNL_em_error_handler("USERSERVICE_register_method", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iNumberOfArgs = 3;
  piArgTypes = (int *) MEM_alloc( iNumberOfArgs * sizeof (int ) );
  piArgTypes[0] = USERARG_TAG_TYPE; // selected component
  piArgTypes[1] = USERARG_LOGICAL_TYPE; // true or false
  piArgTypes[2] = USERARG_STRING_TYPE; // dummy
  iRetCode = USERSERVICE_register_method("BNL_set_in_portal_revise_flag",
                                BNL_FEI_set_in_portal_revise_flag,
                                iNumberOfArgs,
                                piArgTypes,
                                USERARG_VOID_TYPE);
  MEM_free(piArgTypes);
  if (BNL_em_error_handler("USERSERVICE_register_method", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;


  /*iNumberOfArgs = 2;
  piArgTypes = (int *) MEM_alloc( iNumberOfArgs * sizeof (int ) );
  piArgTypes[0] = USERARG_TAG_TYPE; // selected component
  piArgTypes[1] = USERARG_STRING_TYPE; // dummy
  iRetCode = USERSERVICE_register_method("BNL_pre_saveas",
                                BNL_FEI_succ_pre_saveas,
                                iNumberOfArgs,
                                piArgTypes,
                                USERARG_VOID_TYPE);
  MEM_free(piArgTypes);
  if (BNL_em_error_handler("USERSERVICE_register_method", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iNumberOfArgs = 2;
  piArgTypes = (int *) MEM_alloc( iNumberOfArgs * sizeof (int ) );
  piArgTypes[0] = USERARG_TAG_TYPE; // selected component
  piArgTypes[1] = USERARG_STRING_TYPE; // dummy
  iRetCode = USERSERVICE_register_method("BNL_post_saveas",
                                BNL_FEI_succ_post_saveas,
                                iNumberOfArgs,
                                piArgTypes,
                                USERARG_VOID_TYPE);*/
 // MEM_free(piArgTypes);
 // if (BNL_em_error_handler("USERSERVICE_register_method", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

	/*iNumberOfArgs = 2;
  piArgTypes = (int *) MEM_alloc( iNumberOfArgs * sizeof (int ) );
  piArgTypes[0] = USERARG_TAG_TYPE; // bomwindow
  piArgTypes[1] = USERARG_STRING_TYPE; // dummy
  iRetCode = USERSERVICE_register_method("BNL_fei_check_bom",
                                BNL_fei_yoda_check_bom,
                                iNumberOfArgs,
                                piArgTypes,
                                USERARG_LOGICAL_TYPE);
  MEM_free(piArgTypes);
  if (BNL_em_error_handler("USERSERVICE_register_method", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;*/

  iNumberOfArgs = 2;
  piArgTypes = (int *) MEM_alloc( iNumberOfArgs * sizeof (int ) );
  piArgTypes[0] = USERARG_TAG_TYPE; // selected component
  piArgTypes[1] = USERARG_STRING_TYPE; // dummy
  iRetCode = USERSERVICE_register_method("BNL_fei_add_to_list",
                                BNL_fei_yoda_add_to_list,
                                iNumberOfArgs,
                                piArgTypes,
                                USERARG_VOID_TYPE);
  MEM_free(piArgTypes);
  if (BNL_em_error_handler("USERSERVICE_register_method", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iNumberOfArgs = 2;
  piArgTypes = (int *) MEM_alloc( iNumberOfArgs * sizeof (int ) );
  piArgTypes[0] = USERARG_TAG_TYPE; // selected component
  piArgTypes[1] = USERARG_STRING_TYPE; // dummy
  iRetCode = USERSERVICE_register_method("BNL_fei_remove_from_list",
                                BNL_fei_yoda_remove_from_list,
                                iNumberOfArgs,
                                piArgTypes,
                                USERARG_VOID_TYPE);
  MEM_free(piArgTypes);
  if (BNL_em_error_handler("USERSERVICE_register_method", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;


  	iNumberOfArgs = 2;
  piArgTypes = (int *) MEM_alloc( iNumberOfArgs * sizeof (int ) );
  piArgTypes[0] = USERARG_TAG_TYPE; // form
  piArgTypes[1] = USERARG_STRING_TYPE; // dummy
  iRetCode = USERSERVICE_register_method("BNL_add_form_to_list",
                                BNL_FEI_add_form_to_list,
                                iNumberOfArgs,
                                piArgTypes,
                                USERARG_VOID_TYPE);
  MEM_free(piArgTypes);
  if (BNL_em_error_handler("USERSERVICE_register_method", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iNumberOfArgs = 2;
  piArgTypes = (int *) MEM_alloc( iNumberOfArgs * sizeof (int ) );
  piArgTypes[0] = USERARG_TAG_TYPE; // form
  piArgTypes[1] = USERARG_STRING_TYPE; // dummy
  iRetCode = USERSERVICE_register_method("BNL_remove_form_from_list",
                                BNL_FEI_remove_form_from_list,
                                iNumberOfArgs,
                                piArgTypes,
                                USERARG_VOID_TYPE);
  MEM_free(piArgTypes);
  if (BNL_em_error_handler("USERSERVICE_register_method", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  return iRetCode;
}


/**
DLLAPI int BNL_register_fei_web(int *decision, va_list args)

  Description:

  registers the custom modules.

*/
DLLAPI int BNL_register_fei_web(int *decision, va_list args)
{
	int iRetCode			= 0;
	int iInputCode		= 0;
	int i					    = 0;
	int *iOutputCount	= NULL;

  tag_t tInputTag   = NULL_TAG;
  tag_t tOldRev     = NULL_TAG;

  tag_t *tOutputTag	= NULL;
	tag_t **ptOutputTagList= NULL;

  char **pszOutputString= NULL;
	char *pszReturnString	= NULL;
	char *pszInputString	= NULL;

  logical lErrorAdded   = false;


	//*decision = ALL_CUSTOMIZATIONS;


	/* Get the parameters of the function */
	iInputCode = va_arg(args, int);
  pszInputString = va_arg(args, char*);
	tInputTag = va_arg(args, tag_t);
	tOutputTag = va_arg(args, tag_t*);
	pszOutputString = va_arg(args, char**);
	iOutputCount = va_arg(args, int*);
	ptOutputTagList = va_arg(args, tag_t**);

	switch (iInputCode)
  {
    case 101:


      fprintf(ugslog, "%s: Creating predecessor...\n", BNL_module);

      // Create predecessor
      iRetCode = WSOM_ask_based_on(tInputTag, &tOldRev);
      if (BNL_em_error_handler("WSOM_ask_based_on", BNL_module, EMH_severity_user_error, iRetCode, true)) return iRetCode;

      iRetCode = BNL_assign_predeccesor_relation(tInputTag, tOldRev);

      fprintf(ugslog, "%s: Creating predecessor returned [%d].\n", BNL_module, iRetCode);

      if (iRetCode != 0)
      {
        int iErrors       = 0;
        int i             = 0;

        int *piSeverities = NULL;
        int *piFails      = NULL;

        char **pszTexts   = NULL;

        EMH_ask_errors(&iErrors, &piSeverities, &piFails, &pszTexts);
        for (i = 0; i < iErrors; i++)
        {
          fprintf(ugslog, "%s: Code [%d] text [%s].\n", BNL_module, piFails[i], pszTexts[i]);
          if (iRetCode == piFails[i])
          {
            *pszOutputString = MEM_alloc(sizeof(char) * ((int)strlen(pszTexts[i]) + 1) );
		        strcpy(*pszOutputString, pszTexts[i]);

            lErrorAdded = true;

            iRetCode = 0;
            EMH_clear_errors();

            break;
          }
        }
      }
      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    default:
      *decision = ALL_CUSTOMIZATIONS;
  }

  if (!lErrorAdded)
  {
    *pszOutputString = MEM_alloc(sizeof(char) * 1);
    strcpy(*pszOutputString, "");
  }

  *tOutputTag = NULL_TAG;
  *iOutputCount = 1;
  *ptOutputTagList = MEM_alloc( *iOutputCount * sizeof( tag_t ) );
  for ( i = 0; i < *iOutputCount; ++i )
  {
    (*ptOutputTagList)[i] = NULLTAG;
  }

	return iRetCode;
}


DLLAPI int BNL_register_invoke_pdm(int *decision, va_list args)
{
	int iRetCode				      = 0;
	extern FILE *ugslog;
	int iInputCode			    	= 0;
	int *iOutputCode;
	char *pszInputString	  	= NULL;
  char **pszOutputString		= NULL;

  iInputCode = va_arg(args, int);
  pszInputString = va_arg(args, char*);
  iOutputCode = va_arg(args, int*);
  pszOutputString = va_arg(args, char**);

	switch (iInputCode) // 0 is reserved for UPM
  {
    case 10:
      fprintf(ugslog, "%s: Code [%d].\n", BNL_module, iInputCode);

      BNL_fei_attr_nx_pre_saveas(pszInputString);

			*decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 11:
      fprintf(ugslog, "%s: Code [%d].\n", BNL_module, iInputCode);

      *iOutputCode = BNL_fei_attr_nx_post_saveas(pszInputString);

			*decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 12:
      fprintf(ugslog, "%s: Code [%d].\n", BNL_module, iInputCode);

      iRetCode = BNL_fei_attr_nx_create_predecessor(pszInputString);

      fprintf(ugslog, "%s: iRetCode [%d].\n", BNL_module, iRetCode);
      if (iRetCode != 0)
      {
        int iErrors       = 0;
        int i             = 0;

        int *piSeverities = NULL;
        int *piFails      = NULL;

        char **pszTexts   = NULL;

        EMH_ask_errors(&iErrors, &piSeverities, &piFails, &pszTexts);
        for (i = 0; i < iErrors; i++)
        {
          fprintf(ugslog, "%s: Code [%d] text [%s].\n", BNL_module, piFails[i], pszTexts[i]);
          if (iRetCode == piFails[i])
          {
            *pszOutputString = malloc(sizeof(char) * (strlen(pszTexts[i]) + 1) );
            strcpy(*pszOutputString, pszTexts[i]);

            break;
          }
        }
        EMH_clear_errors();
      }

      fprintf(ugslog, "%s: Output string [%s].\n", BNL_module,  *pszOutputString);

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    default:

       *decision = ALL_CUSTOMIZATIONS;
    break;
  }

  fprintf(ugslog, "%s: Leave invoke PDM server.\n",BNL_module);

  return iRetCode;
}


/**
DLLAPI int BNL_register_fei_user_init(int *decision, va_list args)

  Description:

  Registers the custom modules.

*/
DLLAPI int BNL_register_fei_user_init(int *decision, va_list args)
{
  int iRetCode    = 0;
  int i           = 0;
  int iRels       = 0;
  int iTypes      = 0;
  int iImfTypes   = 0;
  int iRelTypes   = 0;
  int iPropTypes  = 0;
  int iIrDescTypes= 0;
  int k           = 0;

  char **pszRels     = NULL;
  char **pszIrDescTypes = NULL;
  char **pszTypes    = NULL;
  char **pszImfTypes = NULL;
  char **pszRelTypes = NULL;
  char **pszPropTypes = NULL;

  char *pszTempType       = NULL;
  char *pszItemType       = NULL;
  char *pszRel            = NULL;
  char *p                 = NULL;

  char szRevType[IMANTYPE_name_size_c + 1];

  METHOD_id_t method;


  *decision = ALL_CUSTOMIZATIONS;

  fprintf(ugslog, "\n%s: Start of BNL_register_fei_user_init.\n", BNL_module);

  iRetCode = METHOD_find_method("BOMWindow", BOMWindow_save_msg, &method);
  if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

  /* Register the pre method */
  iRetCode = METHOD_add_action(method, METHOD_pre_action_type, BNL_fei_yoda_check_bom, NULL);
  if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

  fprintf(ugslog, "%s: Pre action for bom save registered.\n", BNL_module);

  iRetCode = METHOD_find_method("Item", IMAN_save_msg, &method);
  if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

  if (method.id != 0)
  {
    /* Register the post method */
    iRetCode = METHOD_add_action(method, METHOD_pre_action_type, BNL_fei_yoda_check_uom, NULL);
    if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    fprintf(ugslog, "%s: Pre action for Item save has been registered.\n", BNL_module);
  }

  /* Added post action to deepcopy */
//  iRetCode = METHOD_find_method("ItemRevision", "ITEM_deep_copy", &method);
//  if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_user_error, iRetCode, false)) return iRetCode;

//  if (method.id != NULLTAG)
//  {
//    iRetCode = METHOD_add_action(method, METHOD_post_action_type, (METHOD_function_t) BNL_fei_yoda_deep_copy_post_action, NULL);
//    if (BNL_em_error_handler("METHOD_find_method (post)", BNL_module, EMH_severity_user_error, iRetCode, false)) return iRetCode;

//    fprintf(ugslog, "BNL_fei_yoda_deep_copy_post_action successfully added!\n");
//  }

  // Get the preference
  iRetCode = BNL_tb_pref_ask_char_values(BNL_FEI_RELS, IMAN_preference_site, &iRels, &pszRels);
  if (BNL_em_error_handler("PREF_ask_char_values", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  for (i = 0; i < iRels; i++)
  {
    iRetCode = METHOD_find_method(pszRels[i], GRM_create_msg, &method);
    if (BNL_em_error_handler("METHOD_find_method (rel)", BNL_module, EMH_severity_user_error, iRetCode, false)) return iRetCode;

    if (method.id != 0)
    {
      iRetCode = METHOD_add_action(method, METHOD_pre_action_type, BNL_fei_yoda_grm_create_pre, NULL);
      if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

      fprintf(ugslog, "%s: Pre action for creation [%s] has been registered.\n", BNL_module, pszRels[i]);

      iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_fei_yoda_grm_create_post, NULL);
      if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

      fprintf(ugslog, "%s: Post action for creation [%s] has been registered.\n", BNL_module, pszRels[i]);
    }
    else
    {
      fprintf(ugslog, "%s: Warning, failed to add pre & post action to creation of relation [%s].\n", BNL_module, pszRels[i]);
    }

    iRetCode = METHOD_find_method(pszRels[i], IMAN_delete_msg, &method);
    if (BNL_em_error_handler("METHOD_find_method (rel)", BNL_module, EMH_severity_user_error, iRetCode, false)) return iRetCode;

    if (method.id != 0)
    {
      iRetCode = METHOD_add_action(method, METHOD_pre_action_type, BNL_fei_yoda_grm_delete_pre, NULL);
      if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

      fprintf(ugslog, "%s: Pre action for removal [%s] has been registered.\n", BNL_module, pszRels[i]);
    }
    else
    {
      fprintf(ugslog, "%s: Warning, failed to add pre action to removal of relation [%s].\n", BNL_module, pszRels[i]);
    }
  }

  BNL_tb_pref_ask_char_values(BNL_FEI_DESC_IR_TYPES, IMAN_preference_site, &iIrDescTypes, &pszIrDescTypes);

  if (iIrDescTypes == 0)
  {
    fprintf(ugslog, "%s: Warning, no types have been defined using preference [%s].\n\n", BNL_module, BNL_FEI_DESC_IR_TYPES);
  }
  else
  {
    fprintf(ugslog, "%s: Found types for set of description on ItemRevision [%d].\n", BNL_module, iIrDescTypes);

    for (k=0; k<iIrDescTypes; k++)
    {
      fprintf(ugslog, "%s: Found type [%s] for set of description on ItemRevision.\n", BNL_module, pszIrDescTypes[k]);

      iRetCode = METHOD_find_prop_method(pszIrDescTypes[k], "object_desc", PROP_set_value_string_msg, &method);
      if (BNL_em_error_handler("METHOD_find_prop_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

      if (method.id != 0)
      {
        iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_FEI_populate_item_desc, NULL);
        if (BNL_em_error_handler("METHOD_find_prop_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;
      }
    }
    fprintf(ugslog, "\n");
  }

  BNL_tb_pref_ask_char_values(BNL_FEI_PREF_TYPES, IMAN_preference_site, &iTypes, &pszTypes);

  if (iTypes == 0)
  {
    fprintf(ugslog, "%s: Warning, no types have been defined using preference [%s].\n\n", BNL_module, BNL_FEI_PREF_TYPES);
  }
  else
  {
    fprintf(ugslog, "%s: Found types for validating: [%d].\n", BNL_module, iTypes);

    for (i = 0; i < iTypes; i++)
    {
      fprintf(ugslog, "%s: Found type [%s].\n", BNL_module, pszTypes[i]);

      /* Register the pre action */
      iRetCode = METHOD_find_method(pszTypes[i], IMAN_save_msg, &method);
      if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

      if (method.id != 0)
      {
        /* Register the pre method */
        iRetCode = METHOD_add_action(method, METHOD_pre_action_type, BNL_FEI_validate_on_save, NULL);
        if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        fprintf(ugslog, "%s: Pre action for [%s] has been registered.\n", BNL_module, pszTypes[i]);
      }

      // Revise
      pszTempType = BNL_strings_copy_string(pszTypes[i]);
      p = strstr(pszTempType, " Master");
      if (p != NULL)
      {
        *p = '\0';

        fprintf(ugslog, "%s: Type [%s].\n", BNL_module, pszTempType);

        iRetCode = METHOD_find_method(pszTempType, ITEM_copy_rev_msg, &method);
        if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        if (method.id > 0)
        {
          fprintf(ugslog, "%s: Adding pre/post action to revise action for type [%s].\n", BNL_module, pszTempType);

          /* Register the pre action */
          iRetCode = METHOD_add_action(method, METHOD_pre_action_type, BNL_FEI_pre_revise, NULL);
          if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

          /* Register the post method */
          iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_FEI_post_revise, NULL);
          if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;
        }

        // For preventing errors during saveas and getting the IMF and IRMF
        iRetCode = METHOD_find_method(pszTempType, ITEM_create_from_rev_msg, &method);
        if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

        if (method.id != 0)
        {
          iRetCode = METHOD_add_action(method, METHOD_pre_action_type, BNL_fei_yoda_pre_saveas, NULL);
          if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

          fprintf(ugslog, "%s: Pre action for [ITEM_create_from_rev_msg] has been registered.\n", BNL_module);

          iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_fei_yoda_post_saveas, NULL);
          if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

          fprintf(ugslog, "%s: Post action for [ITEM_create_from_rev_msg] has been registered.\n", BNL_module);
        }

        iRetCode = METHOD_find_method(pszTypes[i], WSO_copy_msg, &method);
        if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_FEI_post_copy, NULL);
        if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        fprintf(ugslog, "%s: Post action for [WSO_copy_msg] for type [%s] has been registered.\n", BNL_module, pszTypes[i]);
      }

      if (pszTempType != NULL)
      {
        free(pszTempType);
        pszTempType = NULL;
      }
    }
    fprintf(ugslog, "\n");
  }

  // CSP generation
  BNL_tb_pref_ask_char_values(BNL_FEI_PREF_IMF_TYPES, IMAN_preference_site, &iImfTypes, &pszImfTypes);

  if (iImfTypes == 0)
  {
    fprintf(ugslog, "%s: Warning, no IMF types have been defined using preference [%s].\n\n", BNL_module, BNL_FEI_PREF_IMF_TYPES);
  }
  else
  {
    fprintf(ugslog, "%s: Found IMF types: [%d].\n", BNL_module, iImfTypes);

    if ((iRetCode = BNL_FEI_imf_csp_set_attr_names()) != 0) goto function_exit;

    for (i = 0; i < iImfTypes; i++)
    {
      fprintf(ugslog, "%s: Found IMF type [%s].\n", BNL_module, pszImfTypes[i]);

      /* Register the post action */
      iRetCode = METHOD_find_method(pszImfTypes[i], IMAN_save_msg, &method);
      if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

      if (method.id != 0)
      {
        /* Register the pre method */
        iRetCode = METHOD_add_action(method, METHOD_pre_action_type, BNL_FEI_imf_csp_init, NULL);
        if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        fprintf(ugslog, "%s: Pre action (csp) for [%s] has been registered (1).\n", BNL_module, pszImfTypes[i]);

        /* Register the post method */
        iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_FEI_imf_csp, NULL);
        if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        fprintf(ugslog, "%s: Post action (csp) for [%s] has been registered (1).\n", BNL_module, pszImfTypes[i]);
      }
    }
    fprintf(ugslog, "\n");
  }

  BNL_tb_pref_ask_char_values(BNL_FEI_PREF_REL_TYPES, IMAN_preference_site, &iRelTypes, &pszRelTypes);

  if (iRelTypes == 0)
  {
    fprintf(ugslog, "%s: Warning, no relation types have been defined using preference [%s].\n\n", BNL_module, BNL_FEI_PREF_REL_TYPES);
  }
  else
  {
    fprintf(ugslog, "%s: Found relation types: [%d].\n", BNL_module, iRelTypes);

    for (i = 0; i < iRelTypes; i++)
    {
      fprintf(ugslog, "%s: Found relation type [%s].\n", BNL_module, pszRelTypes[i]);

      /* Register the pre action */
      iRetCode = METHOD_find_method(pszRelTypes[i], GRM_create_msg, &method);
      if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

      if (method.id != 0)
      {
        /* Register the pre method */
        iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_FEI_rel_csp, NULL);
        if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        fprintf(ugslog, "%s: Post action (csp) for [%s] has been registered.\n", BNL_module, pszRelTypes[i]);
      }
    }
    fprintf(ugslog, "\n");
  }

  BNL_tb_pref_ask_char_values(BNL_FEI_PREF_PROP_NAMES, IMAN_preference_site, &iPropTypes, &pszPropTypes);

  if (iPropTypes == 0)
  {
    fprintf(ugslog, "%s: Warning, no prop/types have been defined using preference [%s].\n\n", BNL_module, BNL_FEI_PREF_PROP_NAMES);
  }
  else
  {
    fprintf(ugslog, "%s: Found prop/types [%d].\n", BNL_module, iPropTypes);

    for (i=0; i<iPropTypes; i++)
    {
      char *pszType     = NULL;
      char *pszProp     = NULL;

      fprintf(ugslog, "%s: Found prop/type [%s].\n", BNL_module, pszPropTypes[i]);

      pszType = pszPropTypes[i];

      if (strchr(pszType, ';') != NULL)
      {
        pszProp = strchr(pszType, ';');
        *pszProp = '\0';
        pszProp++;

        iRetCode = METHOD_find_prop_method(pszType, pszProp, PROP_set_value_string_msg, &method);
        if (BNL_em_error_handler("METHOD_find_prop_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        if (method.id != 0)
        {
          iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_FEI_prop_csp, NULL);
          if (BNL_em_error_handler("METHOD_find_prop_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

          fprintf(ugslog, "%s: Post action (csp) for property [%s] on type [%s] has been registered.\n", BNL_module, pszProp, pszType);
        }
      }
    }
    fprintf(ugslog, "\n");
  }

  if (iImfTypes > 0 || iPropTypes > 0)
  {
    iRetCode = METHOD_find_method("Item", IMAN_save_msg, &method);
    if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    if (method.id != 0)
    {
      /* Register the post method */
      iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_FEI_item_csp, NULL);
      if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

      fprintf(ugslog, "%s: Post action (csp) for [Item] has been registered.\n", BNL_module);
    }

    iRetCode = METHOD_find_method("ItemRevision", IMAN_save_msg, &method);
    if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    if (method.id != 0)
    {
      /* Register the pre method */
      iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_FEI_itemrev_csp, NULL);
      if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

      fprintf(ugslog, "%s: Post action (csp) for [ItemRevision] has been registered.\n", BNL_module);
    }
    fprintf(ugslog, "\n");
  }

  // Commercial Item Creation
  BNL_tb_pref_ask_char_value(BNL_FEI_PREF_FRU_ITEM, IMAN_preference_site, &pszItemType);

  if (pszItemType != NULL)
  {
    iRetCode = METHOD_find_method(pszItemType, ITEM_create_msg, &method);
    if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    if (method.id != 0)
    {
      iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_FEI_item_create, NULL);
      if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

      fprintf(ugslog, "%s: Post action (ITEM_create_msg) for [%s] has been registered.\n", BNL_module, pszItemType);

      if (strcmp(pszItemType, "Item") == 0 || strcmp(pszItemType, "Document") == 0)
        sprintf(szRevType, "%sRevision", pszItemType);
      else
        sprintf(szRevType, "%s Revision", pszItemType);

      //iRetCode = METHOD_find_method("ItemRevision", ITEM_copy_rev_to_existing_msg, &method);
      iRetCode = METHOD_find_method(szRevType, ITEM_copy_rev_to_existing_msg, &method);
      if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

      if (method.id != 0)
      {
        iRetCode = METHOD_add_action(method, METHOD_post_action_type, BNL_FEI_item_saveas, NULL);
        if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        fprintf(ugslog, "%s: Post action (ITEM_copy_rev_to_existing_msg) for [%s] has been registered.\n", BNL_module, szRevType);
      }
    }
    fprintf(ugslog, "\n");
  }
  else
  {
    fprintf(ugslog, "%s: Warning, preference [%s] has not been defined.\n\n", BNL_module, BNL_FEI_PREF_FRU_ITEM);
  }

  BNL_tb_pref_ask_char_value(BNL_FEI_PREDECESSOR_REL, IMAN_preference_site, &pszRel);
  fprintf(ugslog, "%s: Found preference value [%s] for [%s].\n", BNL_module, pszRel, BNL_FEI_PREDECESSOR_REL);

  if (pszRel == NULL)
  {
    pszRel = MEM_alloc(sizeof(char)*33);
    strcpy(pszRel, DEFAULT_PREDRELATION);
  }

  iRetCode = METHOD_find_method(pszRel, GRM_create_msg, &method);
  if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

  if (method.id != 0)
  {
    /* Register the post method */
    iRetCode = METHOD_add_action(method, METHOD_pre_action_type, BNL_FEI_create_successor, NULL);
    if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    fprintf(ugslog, "%s: Pre action for creation [%s] has been registered.\n", BNL_module, pszRel);
  }

  iRetCode = METHOD_find_method(pszRel, IMAN_delete_msg, &method);
  if (BNL_em_error_handler("METHOD_find_method", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

  if (method.id != 0)
  {
    /* Register the post method */
    iRetCode = METHOD_add_action(method, METHOD_pre_action_type, BNL_FEI_delete_successor, NULL);
    if (BNL_em_error_handler("METHOD_add_action", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    fprintf(ugslog, "%s: Pre action for delete [%s] has been registered.\n", BNL_module, pszRel);
  }

function_exit:

  fprintf(ugslog, "%s: End of BNL_register_fei_user_init.\n\n", BNL_module);

  if (iRels > 0) MEM_free(pszRels);
  if (iIrDescTypes > 0) MEM_free(pszIrDescTypes);
  if (iTypes > 0) MEM_free(pszTypes);
  if (iImfTypes > 0) MEM_free(pszImfTypes);
  if (iRelTypes > 0) MEM_free(pszRelTypes);
  if (iPropTypes > 0) MEM_free(pszPropTypes);
  if (pszTempType != NULL) free(pszTempType);
  if (pszItemType != NULL) MEM_free(pszItemType);
  if (pszRel != NULL) MEM_free(pszRel);

  return iRetCode;
}



/**
DLLAPI int BNL_exit_bnl_yoda(int *decision, va_list args)

  Description:

  Exit all modules.

*/
DLLAPI int BNL_exit_bnl_yoda(int *decision, va_list args)
{
	int iRetCode		= 0;


  *decision = ALL_CUSTOMIZATIONS;

  fprintf(ugslog, "\n");

  BNL_FEI_yoda_exit_module();
  BNL_FEI_exit_module();

  BNL_exit_bnllibs();

  return iRetCode;
}


/**
DLLAPI int BNL_register_fei_rtp(int *decision, va_list args)

  Description:

  Register the runtime properties.

*/
DLLAPI int BNL_register_fei_rtp(int *decision, va_list args)
{
  int iRetCode          = 0;


  iRetCode = BNL_fei_yoda_rtp_property();
  if (BNL_em_error_handler("BNL_fei_yoda_rtp_property", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  iRetCode = BNL_fei_yoda_rtp_bvr();
  if (BNL_em_error_handler("BNL_fei_yoda_rtp_bvr", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

function_exit:


  return iRetCode;
}

int BNL_FEI_set_in_portal_flag(void * pReturnValue)
{
	int iRetCode				      = 0;

  logical lFlag             = false;

  fprintf(ugslog, "*** In BNL_FEI_set_in_portal_flag.\n");


  iRetCode = USERARG_get_logical_argument(&lFlag);
	if (BNL_em_error_handler("USERARG_get_logical_argument", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(ugslog, "*** Flag [%d].\n", lFlag);
  glPortalReviseSaveAs = lFlag;

function_exit:


	fprintf(ugslog,"*** End BNL_FEI_set_in_portal_flag.\n\n");

	return iRetCode;
}

//IR 7520879
int BNL_FEI_set_in_portal_revise_flag(void * pReturnValue)
{
	int iRetCode				      = 0;

  logical lFlag             = false;
  tag_t tObj                = NULLTAG;

  int iNum                  = 0;
  tag_t *ptObjects          = NULL;

  fprintf(ugslog, "*** In BNL_FEI_set_in_portal_revise_flag.\n");

  iRetCode = USERARG_get_tag_argument(&tObj);
	if (BNL_em_error_handler("USERARG_get_logical_argument", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = USERARG_get_logical_argument(&lFlag);
	if (BNL_em_error_handler("USERARG_get_logical_argument", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(ugslog, "*** Flag [%d].\n", lFlag);

  // Get the IRMF
  iRetCode = AOM_ask_value_tags(tObj, "IMAN_master_form_rev", &iNum, &ptObjects);
  if (BNL_em_error_handler("AOM_ask_value_tag (IMRF)", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  if (iNum > 0)
  {
    gtIRMF = ptObjects[0];
  }
  else
  {
    fprintf(ugslog, "%s: Warning \n\n");
  }

function_exit:

  if (iNum > 0) MEM_free(ptObjects);

	fprintf(ugslog,"*** End BNL_FEI_set_in_portal_revise_flag.\n\n");

	return iRetCode;
}
