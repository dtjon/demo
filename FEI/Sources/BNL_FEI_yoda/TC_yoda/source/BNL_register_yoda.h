/**
 (C) copyright by Siemens Product Lifecylce Management Software (NL) B.V.


 Description  :  This module header file belongs to the yoda module .c file.
                 
 Created for  :  FEI

 Comments     :  

 Filename     :  BNL_register_yoda_module.h

 Version info :  release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 Reason / Description                       Version By                  Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                   0.1     J.M.                01-04-2010

 
*/

logical glPortalReviseSaveAs;
tag_t gtIRMF;
tag_t gtIMF;
