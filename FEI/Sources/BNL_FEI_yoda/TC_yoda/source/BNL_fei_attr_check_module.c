/**
 (C) copyright by Siemens Product Lifecylce Management Software (NL) B.V.


 Description    :   BNL FEI yoda module, module for defining post and pre action for ItemRevision and Form.

 Created for    :   FEI

 Comments       :   Every new procedure or function in this modules starts
                    with BNL_

 Filename       :   $Id: BNL_fei_attr_check_module.c 449 2016-06-28 09:00:09Z tjonkonj $


 History of changes
 Reason /description                                                    Version     By          Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.1         JW          26-03-2009
 Added item revision desc customization                                 0.2         JW          31-03-2009
 Added FRU check customization                                          0.3         JW          31-03-2009
 Updated preference names                                               0.4         JW          01-04-2009
 Fixed issue wtih BNL_FEI_FRU_check                                     0.5         JM          10-04-2009
 Updated BNL_FEI_populate_item_desc                                     0.6         JM          21-04-2009
 Fixed issue wtih BNL_FEI_FRU_check                                     0.7         JM          24-04-2009
 Updated BNL_FEI_FRU_check                                              0.8         JW          29-06-2009
 Added set attributes customization                                     0.9         JW          30-06-2009
 Update BNL_FEI_FRU_check                                               1.0         JM          02-07-2009
 Added init of lCheck                                                   1.1         JM          02-07-2009
 Fixed issue with MEM_free                                              1.2         JM          02-07-2009
 Updated preference FEI_FRU_CHECK                                       1.4         JM          03-07-2009
 Completely updated                                                     1.5         JM          21-07-2009
 Added extra check to role and len check                                1.6         JM          22-07-2009
 Added extra check                                                      1.7         JM          22-07-2009
 Updated for web and NX                                                 1.8         JM          19-10-2009
 Updated FEI_set_attributes                                             1.9         JM          19-10-2009
 Add bypass check                                                       2.0         JM          23-10-2009
 Added check for illegal characters                                     2.1         JM          29-10-2009
 Update of BNL_FEI_populate_item_desc                                   2.2         JM          09-12-2009
 Added workaround for required description on ItemRevision              2.3         JM          18-12-2009
 Extended role check                                                    2.4         JM          14-01-2010
 Update memory mgt (BNL_FEI_split_multiple_values)                      2.5         JM          15-01-2010
 Enhanced the role check functionality                                  2.6         JM          10-03-2010
 Memory management (removed BNL_FEI_split_multiple_values)              2.7         JM          05-04-2010
 Updated reset check functionality                                      2.8         JM          22-09-2010
 Updated lov checking and updated attr role functionality               2.9         JM          29-09-2010
 Fixed issue within with attr role checking                             3.0         JM          26-10-2010
 Unlock Item if needed in case description has been updated             3.1         JM          26-10-2010
 Updated for fixing issue within NX (ref 565) and added
  FEI_check_attributes_security                                         3.2         JM          01-11-2010
 Fix loading of Item when updating the description                      3.3         JM          01-11-2010
 Fixes attribute resetting                                              3.4         JM          16-11-2010
 Fixed issue with creation of predecessor                               3.5         JM          24-11-2010
 Added error handling to FEI_check_attributes_chars                     3.6         JM          08-11-2011
 Updated for issue with compound property (IR 1872661)                  3.7         JM          09-11-2011
 Fixed issue with forms check (IR 1874581)                              3.8         JM          22-11-2011
 Fix for IR 1886869                                                     3.9         JM          23-03-2012
 Fix for IR 1886869 (2)                                                 4.0         JM          11-05-2012
 Updated for IR 1894958                                                 4.1         JM          15-05-2012
 Updated for IR 1897079                                                 4.2         JM          22-05-2012
 Fixed issued due to new revise behavior and update BNL_check_value_lov 4.3         JM          24-05-2012
 Updated FEI_check_attributes_role and FEI_check_attributes_security    4.4         JM          02-04-2013
 Updated for IR 1983274                                                 4.5         JM          13-06-2014
 Updated for IR 8251949                                                 4.6         DTJ         10-11-2014
 Updated for IR 7520879                                                 4.7         JWO         22-10-2015
 Update for TC 10.1.*                                                   4.8         DTJ         31-05-2016
*/
#include <stdlib.h>

#ifdef  TC10_1
#include <tccore/item.h>
#include <user_exits/user_exits.h>
#include <tccore/imantype.h>
#include <lov/lov.h>
#include <tccore/aom_prop.h>
#include <ict/ict_userservice.h>
#else
#include <iman.h>
#include <item.h>
#include <bom.h>
#include <ps.h>
#include <lov.h>
#include <aom_prop.h>
#include <form.h>
#include <user_server_exits.h>
#endif

#include <BNL_register_bnllibs.h>
#include <BNL_tb_module.h>
#include <BNL_em_module.h>
#include <BNL_fm_module.h>
#include <BNL_xml_module.h>
#include <BNL_strings_module.h>
#include <BNL_errors.h>

#include <BNL_fei_attr_check_module.h>
#include <BNL_fei_yoda_module.h>
#include <BNL_fei_succ_rel_module.h>
#include <BNL_register_yoda.h>

static char BNL_module[] = "BNL ATTR MOD 4.8";

// Globals
char *gpszNxOldItemId    = NULL;
char *gpszNxOldRevId     = NULL;
char *gpszNxNewItemId    = NULL;
char *gpszNxNewRevId     = NULL;
int giNX                 = 0;
int giRevise             = 0;
// IR 7520879
int giReviseSave         = 0;

// Globals
int giForms       = 0;
char **gpszTypes  = NULL;
char **gpszNames  = NULL;

tag_t gtMasterForm= NULL_TAG;
tag_t gtMasterRevForm= NULL_TAG;


FILE *feilog;


int BNL_FEI_initialise(FILE *pLog)
{
  int iRetCode        = 0;

  feilog = pLog;

  return iRetCode;
}


int BNL_FEI_exit_module()
{
  int iRetCode        = 0;

  BNL_FEI_empty_form_list();

  return iRetCode;
}


int BNL_FEI_pre_revise
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;
  int iCount        = 0;
  int iNum          = 0;

  tag_t tOrgRev     = NULL_TAG;
  tag_t tOrgItem    = NULL_TAG;
  tag_t tRelType    = NULL_TAG;

  char *pszRel      = NULL;

  char szMessg[512];

  tag_t *ptSuccessors= NULL;
  tag_t *ptObjects   = NULL;

  fprintf(feilog, "** Begin ... BNL_FEI_pre_revise \n");

  tOrgRev = va_arg(args, tag_t);


  gtIRMF = NULL_TAG;
  gtIMF = NULL_TAG;

  // Get the IRMF
  iRetCode = AOM_ask_value_tags(tOrgRev, "IMAN_master_form_rev", &iNum, &ptObjects);
  if (BNL_em_error_handler("AOM_ask_value_tag (IMRF)", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  if (iNum > 0)
  {
    gtIRMF = ptObjects[0];
  }
  else
  {
    fprintf(feilog, "%s: Warning failed to find the IRMF.\n");
  }

  // Get the Item
  iRetCode = ITEM_ask_item_of_rev(tOrgRev, &tOrgItem);
  if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  // Check if successor is available
  BNL_tb_pref_ask_char_value(BNL_FEI_SUCCESSOR_REL, IMAN_preference_site, &pszRel);

  if (pszRel == NULL)
  {
    pszRel = MEM_alloc(sizeof(char)*33);
    strcpy(pszRel, DEFAULT_SUCCRELATION);
  }
  else
  {
    fprintf(feilog, "%s: Found preference value [%s] for [%s].\n", BNL_module, pszRel, BNL_FEI_SUCCESSOR_REL);
  }

  iRetCode = GRM_find_relation_type(pszRel, &tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type",BNL_module,EMH_severity_user_error,iRetCode ,true)) goto function_exit;

  if (tRelType == NULL_TAG)
  {
    fprintf(feilog, "%s: Error, did not find the relation [%s] in the database\n",BNL_module, pszRel);
    iRetCode = 1;
    goto function_exit;
  }

  iRetCode = GRM_list_secondary_objects_only(tOrgItem, tRelType, &iCount, &ptSuccessors);
  if (BNL_em_error_handler("GRM_list_secondary_objects_only",BNL_module,EMH_severity_user_error,iRetCode ,true)) goto function_exit;

  /* If there is already a successor don't allow revise.*/
  if (iCount > 0)
  {
    sprintf(szMessg, "Revise is not allowed, Item already has successor relation defined.");
    fprintf(feilog, "%s: %s\n", BNL_module, szMessg);

    iRetCode = BNL_error_message + giErrorOffset;
    BNL_em_error_handler_s1("BNL_FEI_pre_revise", BNL_module, EMH_severity_error, iRetCode, true, szMessg);
    goto function_exit;
  }

  giRevise = 1;

function_exit:

  if (pszRel != NULL) MEM_free(pszRel);
  if (iCount > 0) MEM_free(ptSuccessors);
  if (iNum > 0) MEM_free(ptObjects);

  fprintf(feilog, "** End ... BNL_FEI_pre_revise \n");

  return iRetCode;
}

int BNL_FEI_post_revise
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;


  fprintf(feilog, "** Begin ... BNL_FEI_post_revise \n");

  //gtIRMF = NULL_TAG;
  //gtIMF = NULL_TAG;

  if (giNX != 1)
  {
    // IR 1897079
    gtIRMF = NULL_TAG;
    gtIMF = NULL_TAG;
    giRevise = 0;
  }

  fprintf(feilog, "** End ... BNL_FEI_post_revise \n");

  return iRetCode;
}


// JM|23-03-2012: Fix for IR 1886869
int BNL_FEI_post_copy
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;
  char szType[512];

  tag_t tObject = va_arg(args, tag_t);    /* Object */
  char *pszName = va_arg(args, char*);
  tag_t *tNewWso = va_arg(args, tag_t *); /* New object */


  iRetCode = BNL_tb_determine_object_type(*tNewWso, szType);
  if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  if (strstr(szType, "Revision Master") != NULL)
  {
    fprintf(feilog, "%s: Copy %s [%d].\n", BNL_module, szType, *tNewWso);

    if (gtMasterForm == NULL_TAG)
    {
      gtMasterRevForm = *tNewWso;
    }
    else
    {
      fprintf(feilog, "%s: Saveas.\n", BNL_module);

      gtMasterRevForm = NULL_TAG;
      gtMasterForm = NULL_TAG;
    }
  }
  else if (strstr(szType, "Master") != NULL)
  {
    fprintf(feilog, "%s: Copy %s [%d].\n", BNL_module, szType, *tNewWso);

    gtMasterForm = *tNewWso;
  }

function_exit:

  return iRetCode;
}

/**
int BNL_FEI_validate_on_save
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:

    Pre action on the save of an ItemRevision or Form

  Returns:

*/
int BNL_FEI_validate_on_save
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;
  int iCountSet     = 0;
  int iCountReset   = 0;
  int iCountLen     = 0;
  int iCountRole    = 0;
  int iCountSec     = 0;
  int iCountChars   = 0;

  char *pszDispName    = NULL;

  char **pszPrefsReset = NULL;
  char **pszPrefsSet   = NULL;
  char **pszPrefsLen   = NULL;
  char **pszPrefsRole  = NULL;
  char **pszPrefsSec   = NULL;
  char **pszPrefsChars = NULL;

  char szType[WSO_name_size_c + 1];
  char szKey[128 + 1];
  char szClass[64];
  char szDesc[WSO_desc_size_c + 1] = { "" };

  logical lHasBypass    = false;
  logical lAdmin        = false;

  tag_t tObject     = NULL_TAG;
  tag_t tType       = NULL_TAG;
  tag_t tDesc       = NULL_TAG;

  logical lIsRequired = false;


  fprintf(feilog, "** Begin ... BNL_FEI_validate_on_save \n");

  tObject = va_arg(args, tag_t);    /* Object */


  // JM|23-03-2012: Fix for IR 1886869
  if (tObject == gtMasterRevForm)
  {
    fprintf(feilog, "%s: Skipping IRMF [%d].\n", BNL_module, tObject);
    gtMasterRevForm = NULL_TAG;
    goto function_exit;
  }

  iRetCode = BNL_tb_determine_super_class(tObject, szClass);
  if (BNL_em_error_handler("BNL_tb_determine_super_class", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  if (strcmp(szClass, "ItemRevision") == 0)
  {
    iRetCode = WSOM_ask_description(tObject, szDesc);
    if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    if (strlen(szDesc) == 0)
    {
      // Check if description is mandatory
      iRetCode = IMANTYPE_ask_object_type(tObject, &tType);
      if (BNL_em_error_handler("IMANTYPE_ask_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      iRetCode = IMANTYPE_ask_property_by_name(tType, "object_desc", &tDesc);
      if (BNL_em_error_handler("IMANTYPE_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      iRetCode = PROPDESC_is_required(tDesc, &lIsRequired);
      if (BNL_em_error_handler("PROPDESC_is_required", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      if (lIsRequired)
      {
        fprintf(feilog, "%s: Error description is required and empty.\n", BNL_module);

        iRetCode = PROPDESC_ask_display_name(tDesc, &pszDispName);
        if (BNL_em_error_handler("PROPDESC_is_required", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = 38207;
        EMH_store_error_s1(EMH_severity_error, iRetCode, pszDispName);

        goto function_exit;
      }
    }
  }

  POM_is_user_sa(&lAdmin);
  ITK_ask_bypass(&lHasBypass);
  if (lAdmin && lHasBypass)
  {
    fprintf(feilog, "%s: Warning, user has bypass, validation is being skipped.\n", BNL_module);
    goto function_exit;
  }

  iRetCode = BNL_tb_determine_object_type(tObject, szType);
  if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Type of object [%s].\n", BNL_module, szType);

  PREF_set_search_scope(IMAN_preference_site);

// First get all preferences

  // Reset attributes
  // Check if IMF or IRMF
  if (strstr(szType, "Master") != NULL)
  {
    sprintf(szKey, "%s_%s", BNL_FEI_RESET, szType);
    fprintf(feilog, "%s: IMF/IRMF Found. Looking for preference [%s].\n", BNL_module, szKey);

    BNL_tb_pref_ask_char_values(szKey, IMAN_preference_site, &iCountReset, &pszPrefsReset);
    fprintf(feilog, "%s: Found [%d] preference values.\n", BNL_module, iCountReset);
  }

  // Set defined attribute values
  sprintf(szKey, "%s_%s", BNL_FEI_SET_ATTRIBUTES, szType);
  fprintf(feilog, "%s: Looking for preference [%s].\n", BNL_module, szKey);

  BNL_tb_pref_ask_char_values(szKey, IMAN_preference_site, &iCountSet, &pszPrefsSet);
  fprintf(feilog, "%s: Found [%d] preference values.\n", BNL_module, iCountSet);

  // Check on the length of attributes
  sprintf(szKey, "%s_%s", BNL_FEI_ATTR_LEN, szType);
  fprintf(feilog, "%s: Looking for preference [%s].\n", BNL_module, szKey);

  BNL_tb_pref_ask_char_values(szKey, IMAN_preference_site, &iCountLen, &pszPrefsLen);
  fprintf(feilog, "%s: Found [%d] preference values.\n", BNL_module, iCountLen);

  // Check on the usage of illegal characters
  sprintf(szKey, "%s_%s", BNL_FEI_ATTR_CHARS, szType);
  fprintf(feilog, "%s: Looking for preference [%s].\n", BNL_module, szKey);

  BNL_tb_pref_ask_char_values(szKey, IMAN_preference_site, &iCountChars, &pszPrefsChars);
  fprintf(feilog, "%s: Found [%d] preference values.\n", BNL_module, iCountChars);

  // Attribute security (1)
  sprintf(szKey, "%s_%s", BNL_FEI_ATTR_ROLE, szType);
  fprintf(feilog, "%s: Looking for preference [%s].\n", BNL_module, szKey);

  BNL_tb_pref_ask_char_values(szKey, IMAN_preference_site, &iCountRole, &pszPrefsRole);
  fprintf(feilog, "%s: Found [%d] preference values.\n", BNL_module, iCountRole);

  // Attribute security (2)
  sprintf(szKey, "%s_%s", BNL_FEI_ATTR_SEC, szType);
  fprintf(feilog, "%s: Looking for preference [%s].\n", BNL_module, szKey);

  BNL_tb_pref_ask_char_values(szKey, IMAN_preference_site, &iCountSec, &pszPrefsSec);
  fprintf(feilog, "%s: Found [%d] preference values.\n", BNL_module, iCountSec);

// End of getting all preferences

  // Perform resetting
  if (iCountReset > 0)
    if ((iRetCode = FEI_reset_form_attributes(tObject, iCountReset, pszPrefsReset)) != 0) goto function_exit;

  // Perform checks
  if (iCountLen > 0)
    if ((iRetCode = FEI_check_attributes_length(tObject, iCountLen, pszPrefsLen)) != 0) goto function_exit;

  if (iCountChars > 0)
    if ((iRetCode = FEI_check_attributes_chars(tObject, iCountChars, pszPrefsChars)) != 0) goto function_exit;

  if (iCountRole > 0)
    if ((iRetCode = FEI_check_attributes_role(tObject, iCountRole, pszPrefsRole, false)) != 0) goto function_exit;

  if (iCountSec > 0)
    if ((iRetCode = FEI_check_attributes_security(tObject, iCountSec, pszPrefsSec)) != 0) goto function_exit;

  // Set attributes
  if (iCountSet > 0)
    if ((iRetCode = FEI_set_attributes(tObject, iCountSet, pszPrefsSet)) != 0) goto function_exit;

function_exit:

  if (iCountReset > 0) MEM_free(pszPrefsReset);
  if (iCountSet > 0) MEM_free(pszPrefsSet);
  if (iCountLen > 0) MEM_free(pszPrefsLen);
  if (iCountRole > 0) MEM_free(pszPrefsRole);
  if (iCountSec > 0) MEM_free(pszPrefsSec);
  if (iCountChars > 0) MEM_free(pszPrefsChars);
  if (pszDispName != NULL) MEM_free(pszDispName);

  fprintf(feilog, "** End ... BNL_FEI_validate_on_save\n");
  fprintf(feilog, "\n");

  return iRetCode;
}


/**
int BNL_FEI_populate_item_desc
(
  METHOD_message_t *pmMessage,
  va_list args
)

  Description:
    Populates the Item description with ItemRevision description

  Returns:

    ITK_ok or !ITK_ok

*/
int BNL_FEI_populate_item_desc
(
  METHOD_message_t *pmMessage,
  va_list args
)
{
  int iRetCode      = 0;
  int k             = 0;
  int iIrDescTypes  = 0;

  tag_t tProp       = NULL_TAG;
  tag_t tItem       = NULL_TAG;
  tag_t tItemRev    = NULL_TAG;

  char *pszValue   = NULL;

  char **pszIrDescTypes = NULL;

  char szType[IMANTYPE_name_size_c + 1];

  logical lFound    = false;
  logical lHasBypass= false;
  logical lAdmin    = false;
  int iLock = 0;


  fprintf(feilog, "** Begin ... BNL_FEI_populate_item_desc\n");

  tProp = va_arg(args, tag_t);
  pszValue = va_arg(args, char*);

  POM_is_user_sa(&lAdmin);
  ITK_ask_bypass(&lHasBypass);
  if (lAdmin && lHasBypass)
  {
    fprintf(feilog, "%s: Warning, user has bypass, population of Item description is being skipped.\n", BNL_module);
    goto function_exit;
  }

  iRetCode = PROP_ask_owning_object(tProp, &tItemRev);
  if (BNL_em_error_handler("PROP_ask_owning_object", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = BNL_tb_determine_object_type(tItemRev, szType);
  if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(feilog, "%s: Type of ItemRevision: [%s].\n", BNL_module, szType);

  // Check the type
  BNL_tb_pref_ask_char_values(BNL_FEI_DESC_IR_TYPES, IMAN_preference_site, &iIrDescTypes, &pszIrDescTypes);

  for (k=0; k<iIrDescTypes; k++)
  {
    if (strcmp(pszIrDescTypes[k], szType) == 0)
    {
      lFound = true;
      fprintf(feilog, "%s: Type is configured.\n", BNL_module);
      break;
    }
  }

  if (lFound)
  {
    if (pszValue != NULL)
    {
      if (strlen(pszValue) > 0)
      {
        /*get the item of the item revision*/
        iRetCode = ITEM_ask_item_of_rev(tItemRev, &tItem);
        if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        //if ((iRetCode = BNL_FEI_load_object(tItem, POM_modify_lock, &iLock)) != 0) goto function_exit;
        iRetCode = BNL_FEI_load_object(tItem, POM_modify_lock, &iLock);
        if (iLock == POM_no_lock)
        {
          fprintf(feilog, "%s: Item has been locked for modification.\n", BNL_module);
        }
        else
        {
          fprintf(feilog, "%s: Item already locked for modification.\n", BNL_module);
        }

        AOM_UIF_set_value(tItem, "object_desc", pszValue);

        if (iLock == POM_no_lock)
        {
          BNL_tb_save_and_unlock_object(tItem);
        }
        else
        {
          AOM_save(tItem);
        }

        fprintf(feilog, "%s: Description of item is updated with value [%s]\n", BNL_module, pszValue);
      }
    }
  }
  else
  {
    fprintf(feilog, "%s: Type is not configured, skipping.\n", BNL_module);
  }

function_exit:

  if (iIrDescTypes > 0) MEM_free(pszIrDescTypes);

  fprintf(feilog, "** End ... BNL_FEI_populate_item_desc\n\n");

  return iRetCode;

}


/**
int BNL_FEI_load_object
(
  tag_t tTarget,       <I>
  int iLock,           <I>
  int *piOldLock       <O>
)

  Description:
    Loads an object

  Returns:

    ITK_ok or !ITK_ok

*/
int BNL_FEI_load_object
(
  tag_t Target,
  int iLock,
  int *piOldLock
)
{
  int iRetCode            = 0;
  logical lPOMLoaded      = false;
  logical lNewlycreated   = false;


  iRetCode = POM_is_loaded(Target, &lPOMLoaded);
  if (BNL_em_error_handler("POM_is_loaded", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

  /* New objects can not be refreshed so we check that first */
  iRetCode = POM_is_newly_created(Target, &lNewlycreated);
  if (BNL_em_error_handler("POM_is_newly_created", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

  /* Split check to skip loaded new object */
  if (lPOMLoaded == true)
  {
    iRetCode = POM_ask_instance_lock(Target, piOldLock);
    if (BNL_em_error_handler("POM_ask_instance_lock", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (lNewlycreated == false && *piOldLock != POM_modify_lock)
    {
      iRetCode = POM_refresh_instances(1, &Target, NULLTAG, iLock);
      if (BNL_em_error_handler("POM_refresh_instances", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

      fprintf(feilog, "%s: The target is loaded and refreshed.\n", BNL_module);
    }
    else
      fprintf(feilog, "%s: The target was loaded.\n",BNL_module);
  }
  else
  {
    fprintf(feilog, "%s: The target is not loaded start to load.\n", BNL_module);

    iRetCode = POM_load_instances(1, &Target, NULLTAG, iLock);
    if (BNL_em_error_handler("POM_load_instances", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    fprintf(feilog, "%s: The target is loaded.\n", BNL_module);

  }

  return iRetCode;
}

int BNL_check_value_lov(tag_t tLov, char *pszOrgValue, logical *plAllowed)
{
  int iRetCode        = 0;
  int i               = 0;
  int j               = 0;
  int iValues         = 0;
  int iFilters        = 0;

  logical *plIsnull   = NULL;
  logical *plIsempty  = NULL;

  char **pszValues    = NULL;

  int *piFiltersIndexes = NULL;

  tag_t *ptFilters    = NULL;

  tag_t tFilterLov    = NULL_TAG;

  LOV_usage_t usage;


  *plAllowed = false;


  iRetCode = LOV_ask_disp_values(tLov, &usage, &iValues, &plIsnull, &plIsempty, &pszValues);
  if (BNL_em_error_handler("LOV_ask_disp_values", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

  iRetCode = LOV_ask_value_filters(tLov, &iFilters, &piFiltersIndexes, &ptFilters);
  if (BNL_em_error_handler("LOV_ask_value_filters", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

  for (i = 0; i < iValues; i++)
  {
    // Check for filter
    for (j = 0; j < iFilters; j++)
    {
      if (piFiltersIndexes[j] == i)
      {
        fprintf(feilog, "%s: Filter has been attached.\n", BNL_module);
        tFilterLov = ptFilters[j];
        break;
      }
    }

    if (tFilterLov != NULL_TAG)
    {
      if ((iRetCode = BNL_check_value_lov(tFilterLov, pszOrgValue, plAllowed)) != 0) goto function_exit;

      if (*plAllowed == true)
      {
        break;
      }
    }
    else
    {
      fprintf(feilog, "%s: Value [%s].\n", BNL_module, pszValues[i]);

      if (strcmp(pszOrgValue, pszValues[i]) == 0)
      {
        fprintf(feilog, "%s: Value is allowed.\n", BNL_module);
        *plAllowed = true;
        break;
      }
    }
  }

function_exit:

  if (iValues > 0)
  {
    MEM_free(plIsnull);
    MEM_free(plIsempty);
    MEM_free(pszValues);
  }

  if (iFilters > 0)
  {
    MEM_free(piFiltersIndexes);
    MEM_free(ptFilters);
  }

  return iRetCode;
}

int FEI_reset_form_attributes(tag_t tObject, int iCount, char **pszPrefs)
{
  int iRetCode        = 0;
  int i               = 0;
  int k               = 0;
  int l               = 0;
  int iValues         = 0;

  char *pszPrefValue  = NULL;
  char *pszOrgValue   = NULL;

  char **pszValues    = NULL;

  tag_t tProp         = NULL_TAG;

  logical lCreated    = false;
  logical lAllowed    = false;


  fprintf(feilog, "** Begin ... FEI_reset_form_attributes\n");

  // Check if new and if not in list
  POM_is_newly_created(tObject, &lCreated);
  if (lCreated || giNX == 1 || giRevise == 1)
  {
    if (giNX == 1)
    {
      fprintf(feilog, "%s: SaveAs/Revise from NX...\n", BNL_module, tObject);
    }
    else if (lCreated)
    {
      fprintf(feilog, "%s: Newly created object...\n", BNL_module, tObject);
    }

    if (BNL_FEI_is_object_in_list(tObject))
    {
      // In list, attributes already resetted
      fprintf(feilog, "%s: Attributes on object have already been resetted. No need to reset attributes.\n", BNL_module);
      goto function_exit;
    }
    fprintf(feilog, "%s: Attributes will be resetted.\n", BNL_module);
  }
  else
  {
    fprintf(feilog, "%s: Object already exists in database. No need to reset attributes.\n", BNL_module);
    goto function_exit;
  }

  for (k = 0; k < iCount; k++)
  {
    char *pszAttrName     = NULL;
    char *pszAttrValue    = NULL;
    char *pszCopy         = NULL;

    tag_t tLov            = NULL_TAG;

    if (pszPrefs[k] == NULL || strlen(pszPrefs[k]) == 0)
      continue;

    pszPrefValue = BNL_strings_copy_string(pszPrefs[k]);

    fprintf(feilog, "%s: Processing preference value [%s].\n", BNL_module, pszPrefValue);

    pszAttrName = pszPrefValue;
    if ((pszAttrValue = strchr(pszAttrName, '.')) != NULL)
    {
      *pszAttrValue = '\0';
      pszAttrValue++;

      if ((pszCopy = strchr(pszAttrValue, '.')) != NULL)
      {
        *pszCopy = '\0';
        pszCopy++;
      }
    } // End of if ((pszAttrValue = strchr(pszAttrValue, '.')) != NULL)

    fprintf(feilog, "%s: Attribute name [%s].\n", BNL_module, pszAttrName);
    if (pszAttrValue != NULL) fprintf(feilog, "%s: Attribute value [%s].\n", BNL_module, pszAttrValue);
    if (pszCopy != NULL) fprintf(feilog, "%s: Copy [%s].\n", BNL_module, pszCopy);

    iRetCode = PROP_ask_property_by_name(tObject, pszAttrName, &tProp);
    if (BNL_em_error_handler("PROP_UIF_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    iRetCode = PROP_UIF_ask_value(tProp, &pszOrgValue);
    if (BNL_em_error_handler("PROP_UIF_ask_value", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    iRetCode = AOM_UIF_ask_values(tObject, pszAttrName, &iValues, &pszValues);
    if (BNL_em_error_handler("PROP_UIF_ask_value", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    fprintf(feilog, "%s: orginal value [%s].\n", BNL_module, pszOrgValue);

    //if (pszCopy != NULL && strcmp(pszCopy, "%copy%") == 0 && giRevise == 1)
    //if ((pszCopy != NULL && strcmp(pszCopy, "%copy%") == 0)
      //|| (pszAttrValue != NULL && strcmp(pszAttrValue, "%copy%") == 0))
    if ((giRevise == 1 && pszCopy != NULL && strcmp(pszCopy, "%copy%") == 0)
      || (pszAttrValue != NULL && strcmp(pszAttrValue, "%copy%") == 0))
    {
      if (giRevise == 1)
      {
        fprintf(feilog, "%s: Revise and %%copy%%, checking value [%s].\n", BNL_module, pszOrgValue);
      }
      else
      {
        fprintf(feilog, "%s: %%copy%%, checking value [%s].\n", BNL_module, pszOrgValue);
      }

      if (strlen(pszOrgValue) > 0)
      {
        iRetCode = AOM_ask_lov(tObject, pszAttrName, &tLov);
        if (BNL_em_error_handler("AOM_ask_lov", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        if (tLov != NULL_TAG)
        {
          for (l = 0; l < iValues; l++)
          {
            if ((iRetCode = BNL_check_value_lov(tLov, pszValues[l], &lAllowed)) != 0) goto function_exit;

            if (!lAllowed)
            {
              fprintf(feilog, "%s: Value {%s] is not allowed.\n", BNL_module, pszValues[l]);
              break;
            }
          }

          if (!lAllowed)
          {
            fprintf(feilog, "%s: Current value is not allowed, attribute will be cleared.\n", BNL_module);
            iRetCode = PROP_UIF_set_value(tProp, "");
            if (BNL_em_error_handler("PROP_UIF_set_value", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;
          }
          else
          {
            fprintf(feilog, "%s: Current value is allowed.\n", BNL_module);
          }
        }
        else
        {
          fprintf(feilog, "%s: No Lov has been attached, nothing to check.\n", BNL_module);
        }
      }
      else
      {
        fprintf(feilog, "%s: Empty value, nothing to check.\n", BNL_module);
      }
    }
    else if (pszAttrValue != NULL)
    {
      fprintf(feilog, "%s: The initial value [%s] of attribute [%s] will be set.\n", BNL_module, pszAttrValue, pszAttrName);

      iRetCode = PROP_UIF_set_value(tProp, pszAttrValue);
      if (BNL_em_error_handler("PROP_UIF_set_value", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;
    }
    else
    {
      fprintf(feilog, "%s: Attribute [%s] will be cleared.\n", BNL_module, pszAttrName);

      iRetCode = PROP_UIF_set_value(tProp, "");
      if (BNL_em_error_handler("PROP_UIF_set_value", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;
    }

    if (pszPrefValue != NULL)
    {
      free(pszPrefValue);
      pszPrefValue = NULL;
    }
    if (pszOrgValue != NULL)
    {
      MEM_free(pszOrgValue);
      pszOrgValue = NULL;
    }
    if (pszValues != NULL)
    {
      MEM_free(pszValues);
      pszValues = NULL;
    }
  } // End of for (k = 0; k < iCount; k++)

function_exit:

  if (pszPrefValue != NULL) free(pszPrefValue);
  if (pszOrgValue != NULL) MEM_free(pszOrgValue);
  if (pszValues != NULL) MEM_free(pszValues);

  fprintf(feilog, "** End ... FEI_reset_form_attributes\n");

  return iRetCode;
}


int FEI_set_attributes(tag_t tObject, int iCount, char **pszPrefs)
{
  int iRetCode        = 0;
  int k               = 0;
  int i               = 0;

  char *pszTargetAttrVal  = NULL;
  char *pszDesAttrVal     = NULL;
  char *pszPrefValue      = NULL;

  //int iLock = 0;
  logical lSkip = false;


  fprintf(feilog, "** Begin ... FEI_set_attributes \n");

  for (k = 0; k < iCount; k++)
  {
    char *pszTarget          = NULL;
    char *pszTargetName      = NULL;
    char *pszTargetValue     = NULL;
    char *pszDestination     = NULL;
    char *pszDestinationName = NULL;
    char *pszDestinationValue= NULL;

    // Init
    lSkip = false;

    if (pszPrefs[k] == NULL || strlen(pszPrefs[k]) == 0)
      continue;

    pszPrefValue = BNL_strings_copy_string(pszPrefs[k]);

    fprintf(feilog, "%s: Processing preference value [%s].\n", BNL_module, pszPrefValue);

    pszTarget = pszPrefValue;
    if ((pszDestination = strchr(pszTarget, '~')) != NULL)
    {
      *pszDestination = '\0';
      pszDestination++;
    }

    fprintf(feilog, "%s: Target [%s].\n", BNL_module, pszTarget);
    if (pszDestination != NULL) fprintf(feilog, "%s: Destination [%s].\n", BNL_module, pszDestination);

    // Get the target property
    pszTargetName = pszTarget;
    if ((pszTargetValue = strchr(pszTargetName, '.')) != NULL)
    {
      *pszTargetValue = '\0';
      pszTargetValue++;
    }

    fprintf(feilog, "%s: Target name [%s].\n", BNL_module, pszTargetName);
    if (pszTargetValue != NULL) fprintf(feilog, "%s: Target value [%s].\n", BNL_module, pszTargetValue);

    if (pszTargetValue == NULL)
    {
      fprintf(feilog, "%s: Preference value [%s] has not been defined correctly [<target field name>.<target value>].\n", BNL_module, pszPrefValue);
      continue;
    }

    // Get the destination property
    pszDestinationName = pszDestination;
    if ((pszDestinationValue = strchr(pszDestinationName, '.')) != NULL)
    {
      *pszDestinationValue = '\0';
      pszDestinationValue++;
    }

    fprintf(feilog, "%s: Destination name [%s].\n", BNL_module, pszDestinationName);
    if (pszTargetValue != NULL) fprintf(feilog, "%s: Destination value [%s].\n", BNL_module, pszDestinationValue);

    if (pszDestinationValue == NULL)
    {
      fprintf(feilog, "%s: Preference value [%s] has not been defined correctly [<destination name>.<destination value>].\n", BNL_module, pszPrefValue);
      continue;
    }

    // Target value
    iRetCode = AOM_UIF_ask_value(tObject, pszTargetName, &pszTargetAttrVal);
    if (iRetCode != 0)
    {
      fprintf(feilog, "%s: [%s] is not found, skipping.\n", BNL_module, pszTargetName);
      EMH_clear_errors();
      iRetCode = 0;
      lSkip = true;
    }
    else
    {
      fprintf(feilog, "%s: Target attribute [%s] has value [%s].\n", BNL_module, pszTargetName, pszTargetAttrVal);

      // Check if destination needs to be updated
      iRetCode = AOM_UIF_ask_value(tObject, pszDestinationName, &pszDesAttrVal);
      if (iRetCode != 0)
      {
        fprintf(feilog, "%s: [%s] is not found, skipping.\n", BNL_module, pszDestinationName);
        EMH_clear_errors();
        iRetCode = 0;
      }
      else
      {
        fprintf(feilog, "%s: Destination attribute [%s] has value [%s].\n", BNL_module, pszDestinationName, pszDesAttrVal);
      }

      if (pszDesAttrVal == NULL
        || (strlen(pszDesAttrVal) > 0 && strlen(pszDestinationValue) > 0)
        || strcmp(pszDesAttrVal, pszDestinationValue) == 0)
      {
        fprintf(feilog, "%s: No need to update, skipping.\n", BNL_module);
        lSkip = true;
      }
    }

    if (lSkip == false)
    {
      if (strcmp(pszTargetAttrVal, pszTargetValue) == 0)
      {
        //target is found now update the defined attribute
        // JM: needed??
        /*if ((iRetCode = BNL_FEI_load_object(tDestObj, POM_modify_lock, &iLock)) != 0) goto function_exit;
        if (iLock == POM_no_lock)
        {
          fprintf(feilog, "%s: form has been locked for modification.\n", BNL_module);

          lDestLocked = true;
        }
        else
        {
          fprintf(feilog, "%s: Form already locked for modification.\n", BNL_module);
        }
        */

        iRetCode = AOM_UIF_set_value(tObject, pszDestinationName, pszDestinationValue);
        if (BNL_em_error_handler("AOM_UIF_set_value", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        fprintf(feilog, "%s: Property [%s] has been updated with value [%s].\n", BNL_module, pszDestinationName, pszDestinationValue);

      } // End of if (strcmp(pszTargetAttrVal, ppszTargetAttrs[iTargets-1]) == 0)
    }  // End of if (lSkip == false)
    if (pszTargetAttrVal != NULL)
    {
      MEM_free(pszTargetAttrVal);
      pszTargetAttrVal = NULL;
    }
    if (pszDesAttrVal != NULL)
    {
      MEM_free(pszDesAttrVal);
      pszDesAttrVal = NULL;
    }
    if (pszPrefValue != NULL)
    {
      free(pszPrefValue);
      pszPrefValue = NULL;
    }
  } // End of for (k = 0; k < iCount; k++)

function_exit:

  if (pszTargetAttrVal != NULL) MEM_free(pszTargetAttrVal);
  if (pszDesAttrVal != NULL) MEM_free(pszDesAttrVal);
  if (pszPrefValue != NULL) free(pszPrefValue);

  fprintf(feilog, "** End ... FEI_set_attributes\n");

  return iRetCode;
}

int FEI_check_attributes_length(tag_t tObject, int iCount, char **pszPrefs)
{
  int iRetCode        = 0;
  int i               = 0;
  int iLen            = 0;
  int iLenDef         = 0;

  char *pszAttrVal    = NULL;
  char *pszPrefValue  = NULL;
  char *pszDisplayName= NULL;


  logical lCreated = false;

  char szMessg[1024]    = "";


  fprintf(feilog, "** Begin ... FEI_check_attributes_length \n");

  POM_is_newly_created(tObject, &lCreated);

  //fprintf(feilog, "DEBUG: Object [%d]\n", tObject);

  for (i = 0; i < iCount; i++)
  {
    char *pszAttrName        = NULL;
    char *pszAttrLen         = NULL;

    if (pszPrefs[i] == NULL || strlen(pszPrefs[i]) == 0)
      continue;

    pszPrefValue = BNL_strings_copy_string(pszPrefs[i]);

    fprintf(feilog, "%s: Processing preference value [%s].\n", BNL_module, pszPrefValue);

    pszAttrName = pszPrefValue;
    if ((pszAttrLen = strchr(pszAttrName, ':')) != NULL)
    {
      *pszAttrLen = '\0';
      pszAttrLen++;
    } // End of if ((pszAttrLen = strchr(pszAttrValue, '.')) != NULL)

    fprintf(feilog, "%s: Attribute name [%s].\n", BNL_module, pszAttrName);
    if (pszAttrLen != NULL) fprintf(feilog, "%s: Attribute len [%s].\n", BNL_module, pszAttrLen);

    // JM:04-02-2010
    iRetCode = AOM_UIF_ask_value(tObject, pszAttrName, &pszAttrVal);
    if (BNL_em_error_handler("AOM_UIF_ask_value", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    // JM|09112011: Updated for issue with compound property (IR 1872661)
    if (pszAttrVal == NULL)
    {
      pszAttrVal = MEM_string_copy("");
    }

    fprintf(feilog, "%s: [%s] has value [%s] [%d].\n",BNL_module, pszAttrName, pszAttrVal, strlen(pszAttrVal));

    if (pszAttrVal != NULL)
    {
      int iLen = (int)strlen(pszAttrVal);
      int iLenDef = atoi(pszAttrLen);

      if (iLen > iLenDef)
      {
        // Get the display name
        iRetCode = PROPDESC_ask_display_name_by_name(pszAttrName, &pszDisplayName);
        if (BNL_em_error_handler("PROPDESC_ask_display_name_by_name", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        if (lCreated || BNL_FEI_is_object_in_list(tObject))
        {
          pszAttrVal[iLenDef] = '\0';
          fprintf(feilog, "%s: Warning, Attribute will be set to [%s] since the length [%d] is not allowed for attribute [%s]. The allowed length is [%d].\n"
            , BNL_module, pszAttrVal, iLen, pszDisplayName, iLenDef);

          iRetCode = AOM_UIF_set_value(tObject, pszAttrName, pszAttrVal);
          if (BNL_em_error_handler("AOM_UIF_set_value", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
        }
        else
        {
          sprintf(szMessg, "The length [%d] is not allowed for attribute [%s]. The allowed length is [%d].",
                        iLen, pszDisplayName, iLenDef);

          fprintf(feilog, "%s: %s\n", BNL_module, szMessg);

          iRetCode = BNL_error_message + giErrorOffset;
          BNL_em_error_handler_s1(NULL, BNL_module, EMH_severity_error, iRetCode, true, szMessg);
          goto function_exit;
        }
      }
      MEM_free(pszAttrVal);
      pszAttrVal = NULL;
    }
    if (pszPrefValue != NULL)
    {
      free(pszPrefValue);
      pszPrefValue = NULL;
    }
    if (pszDisplayName != NULL)
    {
      MEM_free(pszDisplayName);
      pszDisplayName = NULL;
    }
  }

function_exit:

  if (pszAttrVal != NULL) MEM_free(pszAttrVal);
  if (pszPrefValue != NULL) free(pszPrefValue);
  if (pszDisplayName != NULL) MEM_free(pszDisplayName);

  fprintf(feilog, "** End ... FEI_check_attributes_length\n");

  return iRetCode;
}

int FEI_check_attributes_chars(tag_t tObject, int iCount, char **pszPrefs)
{
  int iRetCode        = 0;
  int i               = 0;

  char *pszAttrVal    = NULL;
  char *pszPrefValue  = NULL;
  char *pszDisplayName= NULL;
  char *pszIlegalChars= NULL;

  logical lCreated = false;

  char szMessg[1024]    = "";


  fprintf(feilog, "** Begin ... FEI_check_attributes_chars\n");

  POM_is_newly_created(tObject, &lCreated);
  //fprintf(feilog, "CREATED [%d]\n", lCreated);

  for (i = 0; i < iCount; i++)
  {
    logical lFail       = false;
    char *pszAttrName   = NULL;
    char *pszInvalidChars = NULL;

    if (pszPrefs[i] == NULL || strlen(pszPrefs[i]) == 0)
      continue;

    pszPrefValue = BNL_strings_copy_string(pszPrefs[i]);

    fprintf(feilog, "%s: Processing preference value [%s].\n", BNL_module, pszPrefValue);

    pszAttrName = pszPrefValue;
    if ((pszInvalidChars = strchr(pszAttrName, ':')) != NULL)
    {
      *pszInvalidChars = '\0';
      pszInvalidChars++;
    } // End of if ((pszInvalidChars = strchr(pszAttrValue, '.')) != NULL)

    fprintf(feilog, "%s: Attribute name [%s].\n", BNL_module, pszAttrName);
    if (pszInvalidChars != NULL) fprintf(feilog, "%s: Invalid characters [%s].\n", BNL_module, pszInvalidChars);


    fprintf(feilog, "%s: Retrieving [%s].\n", BNL_module, pszAttrName);
    iRetCode = AOM_UIF_ask_value(tObject, pszAttrName, &pszAttrVal);
    if (BNL_em_error_handler("AOM_UIF_ask_value", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

    fprintf(feilog, "%s: After the retrieval of [%s].\n", BNL_module, pszAttrName);

    // JM|09112011: Updated for issue with compound property (IR 1872661)
    if (pszAttrVal == NULL)
    {
      pszAttrVal = MEM_string_copy("");
    }

    fprintf(feilog, "%s: [%s] has value [%s] [%d].\n", BNL_module, pszAttrName, pszAttrVal, strlen(pszAttrVal));

    if (pszInvalidChars != NULL)
    {
      char *pszCheckValue     = NULL;
      char *pszDummy          = NULL;

      pszIlegalChars = BNL_strings_copy_string(pszInvalidChars);

      pszCheckValue = pszIlegalChars;
      do
      {
        if ((pszDummy = strchr(pszCheckValue, '|')) != NULL)
        {
          *pszDummy = '\0';
          pszDummy++;
        }

        //fprintf(feilog, "DEBUG: pszCheckValue [%s]\n", pszCheckValue);

        if (strcmp(pszCheckValue, "\\B") == 0)
        {
          if (pszAttrVal[0] == ' ')
          {
            //fprintf(feilog, "DEBUG: First character is space\n");
            lFail = true;
          }
        }
        else if (strcmp(pszCheckValue, "\\n") == 0)
        {
          if (strchr(pszAttrVal, '\n') != NULL)
          {
            //fprintf(feilog, "DEBUG: Enter used\n");
            lFail = true;
          }
        }
        else if (strcmp(pszCheckValue, "\\t") == 0)
        {
          if (strchr(pszAttrVal, '\t') != NULL)
          {
            //fprintf(feilog, "DEBUG: Tab used\n");
            lFail = true;
          }
        }
        else if (strcmp(pszCheckValue, "\\b") == 0)
        {
          if (strchr(pszAttrVal, '\b') != NULL)
          {
            //fprintf(feilog, "DEBUG: Blank used\n");
            lFail = true;
          }
        }
        //JM|11-10-2009: This is not needed, since " can be used (see documentation)
        else if (strcmp(pszCheckValue, "&quot;") == 0)
        {
          if (strchr(pszAttrVal, '\"') != NULL)
          {
            //fprintf(feilog, "DEBUG: Quote used\n");
            lFail = true;
          }
        }
        else if (strstr(pszAttrVal, pszCheckValue) != NULL)
        {
          //fprintf(feilog, "DEBUG: Illegal char used [%s]\n", pszCheckValue);
          lFail = true;
        }
      } while ((pszCheckValue = pszDummy) != NULL);
    } // End of if (pszInvalidChars != NULL)

    if (lFail)
    {
      // Get the display name
      iRetCode = PROPDESC_ask_display_name_by_name(pszAttrName, &pszDisplayName);
      if (BNL_em_error_handler("PROPDESC_ask_display_name_by_name", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

      if (lCreated || BNL_FEI_is_object_in_list(tObject))
      {
        fprintf(feilog, "%s: Warning, Attribute [%s] will be cleared since illegal chars [%s] are being used.\n"
          ,BNL_module, pszDisplayName, pszInvalidChars);

        iRetCode = AOM_UIF_set_value(tObject, pszAttrName, "");
        if (BNL_em_error_handler("AOM_UIF_set_value", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        fprintf(feilog, "%s: Attribute was cleared.\n", BNL_module);
      }
      else
      {
        sprintf(szMessg, "Illegal characters [%s] are used in the value of attribute [%s]. This not allowed.",
          pszInvalidChars, pszDisplayName, pszAttrVal);

        fprintf(feilog, "%s: %s\n", BNL_module, szMessg);

        iRetCode = BNL_error_message + giErrorOffset;
        BNL_em_error_handler_s1(NULL, BNL_module, EMH_severity_error, iRetCode, true, szMessg);
        goto function_exit;
      }
    }
    if (pszAttrVal != NULL)
    {
      MEM_free(pszAttrVal);
      pszAttrVal = NULL;
    }

    if (pszPrefValue != NULL)
    {
      free(pszPrefValue);
      pszPrefValue = NULL;
    }
    if (pszDisplayName != NULL)
    {
      MEM_free(pszDisplayName);
      pszDisplayName = NULL;
    }
    if (pszIlegalChars != NULL)
    {
      free(pszIlegalChars);
      pszIlegalChars = NULL;
    }
  } // End of for (i = 0; i < iCount; i++)

function_exit:

  if (pszAttrVal != NULL) MEM_free(pszAttrVal);
  if (pszPrefValue != NULL) free(pszPrefValue);
  if (pszDisplayName != NULL) MEM_free(pszDisplayName);
  if (pszIlegalChars != NULL) free(pszIlegalChars);

  fprintf(feilog, "** End ... FEI_check_attributes_chars\n");

  return iRetCode;
}

logical FEI_check_group_role(char *pszGroup, char *pszRole, char *pszGroupRole)
{
  char *pszDummy    = NULL;

  if (strchr(pszGroupRole, '~') != NULL)
  {
    pszDummy = strchr(pszGroupRole, '~');
    *pszDummy = '\0';

    if (strcmp(pszGroupRole, "*") != 0 && strcmp(pszGroupRole, pszGroup) != 0)
    {
      fprintf(feilog, "%s: Current group [%s] does not match [%s].\n", BNL_module, pszGroup, pszGroupRole);
      return false;
    }

    *pszDummy = '~';
    pszGroupRole = pszDummy + 1;

    if (strcmp(pszGroupRole, "*") != 0 && strcmp(pszGroupRole, pszRole) != 0)
    {
      fprintf(feilog, "%s: Current role [%s] does not match [%s].\n", BNL_module, pszRole, pszGroupRole);
      return false;
    }
    else
    {
      fprintf(feilog, "%s: Current group and role match, allowed...\n", BNL_module);
      return true;
    }
  }
  return false;
}


int FEI_check_attributes_role(tag_t tObject, int iCount, char **pszPrefs, logical lFromCspCheck)
{
  int iRetCode        = 0;
  int k               = 0;

  tag_t tProperty     = NULL_TAG;
  tag_t tOldObj       = NULL_TAG;
  tag_t tRole         = NULL_TAG;
  tag_t tGroup        = NULL_TAG;
  tag_t tMasterForm   = NULL_TAG;

  char szRolename[SA_name_size_c+1];
  char szMessg[1024]  = "";

  char *pszNewAttrVal = NULL;
  char *pszOldAttrVal = NULL;
  char *pszPrefValue  = NULL;
  char *pszDisplayName= NULL;
  char *pszInitial    = NULL;
  char *pszGroupTemp  = NULL;
  char *pszGroup      = NULL;
  char *pszGroupRoleCopy = NULL;

  //int iLock = 0;
  logical lSkip           = false;
  logical lCehck          = false;
  //tag_t tDestProp = NULL_TAG;

  logical lCheck          = false;
  logical lCreated        = false;
  logical lSaveRequired   = false;

  int iState              = 0;

  fprintf(feilog, "** Begin ... FEI_check_attributes_role\n");


  POM_is_newly_created(tObject, &lCreated);
  //fprintf(feilog, "DEBUG: Created [%d] giRevise [%d]\n", lCreated, giRevise);

  //IR 7520879
  if (giRevise == 1)
  {
    giReviseSave = 1;
  }

  if ((!lCreated && !BNL_FEI_is_object_in_list(tObject)) && giRevise != 1)
  {
    // Get the storage class for retrieving the old value
    iRetCode = PROP_ask_property_by_name(tObject, "data_file", &tProperty);
    // IR 8251949: skip check if attr data_file not exist
    if (iRetCode == 38015)
    {
      EMH_clear_last_error(iRetCode);
      iRetCode = 0;
    }
    else
    {
      if (BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      iRetCode = PROP_ask_value_tag(tProperty, &tOldObj);
      if (BNL_em_error_handler("PROP_ask_value_tag", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      //fprintf(feilog, "DEBUG: tOldObj [%d]\n", tOldObj);
    }
  }

  // JM|06112013: Removed, see IR 1938741
  /*if (BNL_FEI_is_object_in_list(tObject))
  {
    // In list, attributes already checked
    fprintf(feilog, "%s: Attributes on object have already been checked. No need to check attributes.\n", BNL_module);
    //goto function_exit;
  }*/


  if (tOldObj == NULL_TAG && (lCreated || giRevise == 1 || giReviseSave == 1))//IR 7520879
  {
    char szTypeObject[WSO_name_size_c + 1];
    char szType[WSO_name_size_c + 1];
    // Check the type of the incoming object

    iRetCode = BNL_tb_determine_object_type(tObject, szTypeObject);
    if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    //fprintf(feilog, "DEBUG: Object type [%s]\n", szTypeObject);

    if (gtIRMF != NULL_TAG)
    {
      iRetCode = BNL_tb_determine_object_type(gtIRMF, szType);
      if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      //fprintf(feilog, "DEBUG: IRMF type [%s]\n", szType);

      if (strcmp(szTypeObject, szType) == 0)
      {
        tMasterForm = gtIRMF;
      }
      else
      {
        if (gtIMF != NULL_TAG)
        {
          iRetCode = BNL_tb_determine_object_type(gtIMF, szType);
          if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

          //fprintf(feilog, "DEBUG: IMF type [%s]\n", szType);

          if (strcmp(szTypeObject, szType) == 0)
          {
            tMasterForm = gtIMF;
          }
        }
      }
    }
  }

  for (k = 0; k < iCount; k++)
  {
    char *pszAttrName       = NULL;
    char *pszGroupRole      = NULL;
    char *pszDefaultValue   = NULL;
    char *pszGivenValue     = NULL;
    char *pszMode           = NULL;

    tag_t tObj              = NULL_TAG;


    if (pszPrefs[k] == NULL || strlen(pszPrefs[k]) == 0)
      continue;

    pszPrefValue = BNL_strings_copy_string(pszPrefs[k]);

    // Init
    lCheck  = false;

    fprintf(feilog, "%s: Processing preference value [%s].\n", BNL_module, pszPrefValue);

    pszAttrName = pszPrefValue;
    if ((pszGroupRole = strchr(pszAttrName, ':')) != NULL)
    {
      *pszGroupRole = '\0';
      pszGroupRole++;

      if ((pszDefaultValue = strchr(pszGroupRole, ':')) != NULL)
      {
        *pszDefaultValue = '\0';
        pszDefaultValue++;

        if (pszDefaultValue != NULL && (pszGivenValue = strchr(pszDefaultValue, ':')) != NULL)
        {
          *pszGivenValue = '\0';
          pszGivenValue++;
        }

        if (pszGivenValue != NULL && (pszMode = strchr(pszGivenValue, ':')) != NULL)
        {
          *pszMode = '\0';
          pszMode++;
        }
      }
    } // End of if ((pszGroupRole = strchr(pszAttrName, ':')) != NULL)

    fprintf(feilog, "%s: Attribute name [%s].\n", BNL_module, pszAttrName);
    if (pszGroupRole != NULL) fprintf(feilog, "%s: Group/role [%s].\n", BNL_module, pszGroupRole);
    if (pszDefaultValue != NULL) fprintf(feilog, "%s: Default value [%s].\n", BNL_module, pszDefaultValue);
    if (pszGivenValue != NULL) fprintf(feilog, "%s: Given value [%s].\n", BNL_module, pszGivenValue);

    tObj = tOldObj;
    if (tObj == NULL_TAG)
    {
      if (pszMode != NULL)
      {
        fprintf(feilog, "%s: Mode [%s].\n", BNL_module, pszMode);
        if (strcmp(pszMode, "1") == 0) // Revise
        {
          fprintf(feilog, "%s: Performing check during revise and skipping check during saveas.\n", BNL_module);
          if (gtIMF == NULL_TAG && gtIRMF != NULL_TAG) // Revise
            tObj = tMasterForm;
        }
        else if (strcmp(pszMode, "2") == 0) // SaveAs
        {
          fprintf(feilog, "%s: Performing check during saveas and skipping check during revise.\n", BNL_module);

          if (gtIMF != NULL_TAG && gtIRMF != NULL_TAG) // SaveAs
            tObj = tMasterForm;
        }
        else if (strcmp(pszMode, "3") == 0) // Revise and SaveAs
        {
          fprintf(feilog, "%s: Performing check during saveas and during revise.\n", BNL_module);
          if (gtIRMF != NULL_TAG) // SaveAs and Revise
            tObj = tMasterForm;
        }
        else
        {
          fprintf(feilog, "%s: Unknown mode [%s].\n", BNL_module);

          if (gtIRMF != NULL_TAG) // SaveAs and Revise
          {
            tObj = NULL_TAG;
          }
        }
      }
      else
      {
        if (gtIRMF != NULL_TAG) // SaveAs and Revise
        {
          tObj = NULL_TAG;
        }
      }
    }

    iRetCode = AOM_UIF_ask_value(tObject, pszAttrName, &pszNewAttrVal);
    if (BNL_em_error_handler("AOM_UIF_ask_value 1", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    // JM|09112011: Updated for issue with compound property (IR 1872661)
    if (pszNewAttrVal == NULL)
    {
      pszNewAttrVal = MEM_string_copy("");
    }

    fprintf(feilog, "%s: Attribute [%s] has user entered value [%s].\n", BNL_module, pszAttrName, pszNewAttrVal);

    if (tObj != NULL_TAG)
    {
      iRetCode = AOM_UIF_ask_value(tObj, pszAttrName, &pszOldAttrVal);
      if (BNL_em_error_handler("AOM_UIF_ask_value 2", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      // JM|09112011: Updated for issue with compound property (IR 1872661)
      if (pszOldAttrVal == NULL)
      {
        pszOldAttrVal = MEM_string_copy("");
      }

      fprintf(feilog, "%s: Attribute [%s] has original value [%s].\n", BNL_module, pszAttrName, pszOldAttrVal);
    }

    if (pszOldAttrVal != NULL)
    {
      // Existing object
      if (strcmp(pszOldAttrVal, pszNewAttrVal) != 0)
      {
        // Was modified
        if (pszGivenValue != NULL && strlen(pszGivenValue) > 0)
        {
          fprintf(feilog, "%s: When attribute has value [%s] it can't be modified anymore.\n", BNL_module, pszGivenValue);

          // If old old value, then check
          if (strcmp(pszOldAttrVal, pszGivenValue) == 0)
          {
            fprintf(feilog, "%s: Old value [%s] matches. Check needed.\n", BNL_module, pszOldAttrVal);
            lCheck = true;
          }
          else if (strcmp(pszGivenValue, "*") == 0 && strlen(pszOldAttrVal) > 0)
          {
            fprintf(feilog, "%s: Old value was set [%s]. Check needed.\n", BNL_module, pszOldAttrVal);
            lCheck = true;
          }
        }
        else
        {
          fprintf(feilog, "%s: New value differs from old value. Check needed.\n", BNL_module);
          lCheck = true;
        }
      }
    }
    else
    {
      fprintf(feilog, "%s: Newly created object.\n", BNL_module);

      // Get the initial value
      if (pszGivenValue != NULL && strlen(pszGivenValue) > 0)
      {
        fprintf(feilog, "%s: A check is only needed when original value was [%s]. No check needed.\n", BNL_module, pszGivenValue);
      }
      else if (pszDefaultValue != NULL && pszGivenValue == NULL)
      {
        fprintf(feilog, "%s: Initial value has been configured [%s].\n", BNL_module, pszDefaultValue);

        if (strcmp(pszDefaultValue, pszNewAttrVal) != 0)
        {
          fprintf(feilog, "%s: New value differs from initial value. Check needed.\n", BNL_module);
          lCheck = true;
        }
      }
      else
      {
        fprintf(feilog, "%s: No initial value has been configured.\n", BNL_module);

        //JM|01112010: No needed anymore, this was needed for logical returning "No" when value wasn't set
        //if ((strlen(pszNewAttrVal)>0) && (stricmp(pszNewAttrVal,"NO")!=0))
        if (strlen(pszNewAttrVal)>0)
        {
          lCheck = true;
        }
      }
    }

    if (lCheck == true)
    {
      char *pszDummy         = NULL;
      logical lRoleOk        = false;

      if (pszGroupRole != NULL && strlen(pszGroupRole) > 0)
      {
        iRetCode = POM_ask_group(&pszGroupTemp, &tGroup);
        if (BNL_em_error_handler("POM_ask_group", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = SA_ask_group_full_name(tGroup, &pszGroup);
        if (BNL_em_error_handler("SA_ask_group_full_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = SA_ask_current_role(&tRole);
        if (BNL_em_error_handler("SA_ask_current_role", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = SA_ask_role_name(tRole, szRolename);
        if (BNL_em_error_handler("SA_ask_role_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
        fprintf(feilog, "%s: Role of the current user [%s] \n", BNL_module,szRolename);

        // Check if group/role is configured
        fprintf(feilog, "Checking groups/roles [%s]\n", pszGroupRole);

        pszGroupRoleCopy = BNL_strings_copy_string(pszGroupRole);

        pszDummy = strtok(pszGroupRoleCopy, ";");
        while (pszDummy != NULL)
        {
          fprintf(feilog, "Checking group/role [%s]\n", pszDummy);

          if ((lRoleOk = FEI_check_group_role(pszGroup, szRolename, pszDummy))) break;

          pszDummy = strtok(NULL, ";");
        }

      } // End of if (strlen(ppszVals[1] > 0))

      if (lRoleOk)
      {
        fprintf(feilog, "%s: The attribute [%s] is editable for the user.\n", BNL_module, pszAttrName);
      }
      else
      {
        // Get the display name
        iRetCode = PROPDESC_ask_display_name_by_name(pszAttrName, &pszDisplayName);
        if (BNL_em_error_handler("PROPDESC_ask_display_name_by_name", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        if (pszGroupRole != NULL && strlen(pszGroupRole) > 0)
        {
          sprintf(szMessg, "The attribute [%s] is not editable. Only the user with group(s)/role(s) [%s] can edit this attribute.", pszDisplayName, pszGroupRole);
        }
        else
        {
          sprintf(szMessg, "The attribute [%s] is not editable.", pszDisplayName);
        }

        fprintf(feilog, "%s: %s \n", BNL_module, szMessg);

        if (!lFromCspCheck)
        {
          if (giNX == 1 || lCreated || BNL_FEI_is_object_in_list(tObject) || giRevise == 1)
          {
            if (giNX == 1)
            {
              fprintf(feilog, "%s: SaveAs/Revise from NX...\n", BNL_module, tObject);
            }

            if (pszDefaultValue != NULL && pszGivenValue == NULL)
            {
              fprintf(feilog, "%s: Warning, attribute [%s] will resetted to [%s].\n", BNL_module, pszDisplayName, pszDefaultValue);
              iRetCode = AOM_UIF_set_value(tObject, pszAttrName, pszDefaultValue);
            }
            else if (pszOldAttrVal != NULL)
            {
              fprintf(feilog, "%s: Warning, attribute [%s] will resetted to [%s].\n", BNL_module, pszDisplayName, pszOldAttrVal);
              iRetCode = AOM_UIF_set_value(tObject, pszAttrName, pszOldAttrVal);
            }
            else
            {
              fprintf(feilog, "%s: Warning, attribute [%s] will be cleared.\n", BNL_module, pszDisplayName);
              iRetCode = AOM_UIF_set_value(tObject, pszAttrName, "");
            }
            if (BNL_em_error_handler("AOM_UIF_set_value", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

            fprintf(feilog, "%s: Attribute was resetted.\n", BNL_module);
          }
          else
          {
            iRetCode = BNL_error_message + giErrorOffset;
            BNL_em_error_handler_s1(NULL, BNL_module, EMH_severity_error, iRetCode, true, szMessg);

            goto function_exit;
          }
        } // End of if (!lFromCspCheck)
        else
        {
          iRetCode = BNL_error_message + giErrorOffset;
          goto function_exit;
        }
      }
    } //  End of if (lCheck == true)

    if (pszGroupRoleCopy != NULL)
    {
      free(pszGroupRoleCopy);
      pszGroupRoleCopy = NULL;
    }

    if (pszNewAttrVal != NULL)
    {
      MEM_free(pszNewAttrVal);
      pszNewAttrVal = NULL;
    }

    if (pszOldAttrVal != NULL)
    {
      MEM_free(pszOldAttrVal);
      pszOldAttrVal = NULL;
    }
    if (pszPrefValue)
    {
      free(pszPrefValue);
      pszPrefValue = NULL;
    }
    if (pszDisplayName)
    {
      MEM_free(pszDisplayName);
      pszDisplayName = NULL;
    }
    if (pszGroup != NULL)
    {
      MEM_free(pszGroup);
      pszGroup = NULL;
    }

    if (pszGroupTemp != NULL)
    {
      MEM_free(pszGroupTemp);
      pszGroupTemp = NULL;
    }
  } // End of for (k = 0; k < iCount; k++)


function_exit:

  if (pszNewAttrVal != NULL) MEM_free(pszNewAttrVal);
  if (pszOldAttrVal != NULL) MEM_free(pszOldAttrVal);
  if (pszPrefValue) free(pszPrefValue);
  if (pszDisplayName) MEM_free(pszDisplayName);
  if (pszGroup != NULL) MEM_free(pszGroup);
  if (pszGroupTemp != NULL) MEM_free(pszGroupTemp);
  if (pszGroupRoleCopy != NULL) free(pszGroupRoleCopy);

  fprintf(feilog, "** End ... FEI_check_attributes_role\n");

  return iRetCode;
}


int FEI_check_attributes_security(tag_t tObject, int iCount, char **pszPrefs)
{
  int iRetCode        = 0;
  int k               = 0;

  tag_t tProperty     = NULL_TAG;
  tag_t tOldObj       = NULL_TAG;
  tag_t tRole         = NULL_TAG;
  tag_t tGroup        = NULL_TAG;
  tag_t tMasterForm   = NULL_TAG;

  char szRolename[SA_name_size_c+1];
  char szMessg[1024]  = "";

  char *pszAttr1Val    = NULL;
  char *pszAttr2Val    = NULL;
  char *pszOldAttr1Val = NULL;

  char *pszNewAttrVal = NULL;
  char *pszOldAttrVal = NULL;
  char *pszPrefValue  = NULL;
  char *pszDisplayName= NULL;
  char *pszInitial    = NULL;
  char *pszGroupTemp  = NULL;
  char *pszGroup      = NULL;
  char *pszGroupRoleCopy = NULL;

  //int iLock = 0;
  logical lSkip           = false;
  logical lCehck          = false;
  //tag_t tDestProp = NULL_TAG;

  logical lCheck          = false;
  logical lCreated        = false;
  logical lSaveRequired   = false;

  int iState              = 0;

  fprintf(feilog, "** Begin ... FEI_check_attributes_security\n");


  POM_is_newly_created(tObject, &lCreated);
  //fprintf(feilog, "DEBUG: Created [%d]\n", lCreated);

  if (!lCreated && !BNL_FEI_is_object_in_list(tObject))
  {
    // Get the storage class for retrieving the old value
    iRetCode = PROP_ask_property_by_name(tObject, "data_file", &tProperty);
    // IR 8251949: skip check if attr data_file not exist
    if (iRetCode == 38015)
    {
      EMH_clear_last_error(iRetCode);
      iRetCode = 0;
    }
    else
    {
      if (BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      iRetCode = PROP_ask_value_tag(tProperty, &tOldObj);
      if (BNL_em_error_handler("PROP_ask_value_tag", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      //fprintf(feilog, "DEBUG: tOldObj [%d]\n", tOldObj);
    }
  }

  // JM|06112013: Removed, see IR 1938741
  /*if (BNL_FEI_is_object_in_list(tObject))
  {
    // In list, attributes already checked
    fprintf(feilog, "%s: Attributes on object have already been checked. No need to check attributes.\n", BNL_module);
    goto function_exit;
  }*/

  if (tOldObj == NULL_TAG && lCreated)
  {
    char szTypeObject[WSO_name_size_c + 1];
    char szType[WSO_name_size_c + 1];
    // Check the type of the incoming object

    iRetCode = BNL_tb_determine_object_type(tObject, szTypeObject);
    if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    //fprintf(feilog, "DEBUG: Object type [%s]\n", szTypeObject);

    if (gtIRMF != NULL_TAG)
    {
      iRetCode = BNL_tb_determine_object_type(gtIRMF, szType);
      if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      //fprintf(feilog, "DEBUG: IRMF type [%s]\n", szType);

      if (strcmp(szTypeObject, szType) == 0)
      {
        tMasterForm = gtIRMF;
      }
      else
      {
        if (gtIMF != NULL_TAG)
        {
          iRetCode = BNL_tb_determine_object_type(gtIMF, szType);
          if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

          //fprintf(feilog, "DEBUG: IMF type [%s]\n", szType);

          if (strcmp(szTypeObject, szType) == 0)
          {
            tMasterForm = gtIMF;
          }
        }
      }
    }
  }

  for (k = 0; k < iCount; k++)
  {
    char *pszAttr_1       = NULL;
    char *pszGroupRole    = NULL;
    char *pszAttr_2       = NULL;
    char *pszAttr_2_value = NULL;

    tag_t tObj            = NULL_TAG;

    if (pszPrefs[k] == NULL || strlen(pszPrefs[k]) == 0)
      continue;

    pszPrefValue = BNL_strings_copy_string(pszPrefs[k]);

    // Init
    lCheck  = false;

    fprintf(feilog, "%s: Processing preference value [%s].\n", BNL_module, pszPrefValue);

    pszAttr_1 = pszPrefValue;
    if ((pszGroupRole = strchr(pszAttr_1, ':')) != NULL)
    {
      *pszGroupRole = '\0';
      pszGroupRole++;

      if ((pszAttr_2 = strchr(pszGroupRole, ':')) != NULL)
      {
        *pszAttr_2 = '\0';
        pszAttr_2++;

        if ((pszAttr_2_value = strchr(pszAttr_2, ':')) != NULL)
        {
          *pszAttr_2_value = '\0';
          pszAttr_2_value++;
        }
      } // End of if ((pszGroupRole = strchr(pszAttr_1, ':')) != NULL)
    } // End of if ((pszAttr_2 = strchr(pszAttr_1, ':')) != NULL)

    fprintf(feilog, "%s: Name of attribute (1) [%s].\n", BNL_module, pszAttr_1);
    if (pszGroupRole != NULL) fprintf(feilog, "%s: Group/role [%s].\n", BNL_module, pszGroupRole);
    if (pszAttr_2 != NULL) fprintf(feilog, "%s: Name of attribute (2) [%s].\n", BNL_module, pszAttr_2);
    if (pszAttr_2_value != NULL) fprintf(feilog, "%s: Value of attribute (2) [%s].\n", BNL_module, pszAttr_2_value);

    iRetCode = AOM_UIF_ask_value(tObject, pszAttr_1, &pszAttr1Val);
    if (BNL_em_error_handler("AOM_UIF_ask_value 1", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    // JM|09112011: Updated for issue with compound property (IR 1872661)
    if (pszAttr1Val == NULL)
    {
      pszAttr1Val = MEM_string_copy("");
    }

    fprintf(feilog, "%s: Attribute 1 [%s] has user entered value [%s].\n", BNL_module, pszAttr_1, pszAttr1Val);

    iRetCode = AOM_UIF_ask_value(tObject, pszAttr_2, &pszAttr2Val);
    if (BNL_em_error_handler("AOM_UIF_ask_value 2", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    // JM|09112011: Updated for issue with compound property (IR 1872661)
    if (pszAttr2Val == NULL)
    {
      pszAttr2Val = MEM_string_copy("");
    }

    fprintf(feilog, "%s: Attribute 2 [%s] has value [%s].\n", BNL_module, pszAttr_2, pszAttr2Val);

    tObj = tOldObj;
    if (tObj == NULL_TAG && gtIMF == NULL_TAG && gtIRMF != NULL_TAG)
    {
      // Revise
      fprintf(feilog, "%s: Revise...\n", BNL_module);
      tObj = tMasterForm;
    }

    if (tObj != NULL_TAG)
    {
      iRetCode = AOM_UIF_ask_value(tObj, pszAttr_1, &pszOldAttr1Val);
      if (BNL_em_error_handler("AOM_UIF_ask_value 1", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      // JM|09112011: Updated for issue with compound property (IR 1872661)
      if (pszOldAttr1Val == NULL)
      {
        pszOldAttr1Val = MEM_string_copy("");
      }

      fprintf(feilog, "%s: Attribute 1 [%s] has original value [%s].\n", BNL_module, pszAttr_1, pszOldAttr1Val);
    }

    if (pszAttr2Val == NULL || strcmp(pszAttr2Val, pszAttr_2_value) != 0)
    {
      fprintf(feilog, "%s: Attribute [%s] has not been set to [%s]. No check needed.\n", BNL_module, pszAttr_2, pszAttr_2_value);
    }
    else if (pszOldAttr1Val != NULL)
    {
      // Existing object
      if (strcmp(pszOldAttr1Val, pszAttr1Val) != 0)
      {
        // Was modified
        fprintf(feilog, "%s: Attribute [%s] has been updated. Check needed.\n", BNL_module, pszAttr_1);
        lCheck = true;
      }
    }
    else
    {
      fprintf(feilog, "%s: Newly created object. Nothing to check.\n", BNL_module);
    }

    if (lCheck == true)
    {
      char *pszDummy         = NULL;
      logical lRoleOk        = false;

      if (pszGroupRole != NULL && strlen(pszGroupRole) > 0)
      {
        iRetCode = POM_ask_group(&pszGroupTemp, &tGroup);
        if (BNL_em_error_handler("POM_ask_group", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = SA_ask_group_full_name(tGroup, &pszGroup);
        if (BNL_em_error_handler("SA_ask_group_full_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = SA_ask_current_role(&tRole);
        if (BNL_em_error_handler("SA_ask_current_role", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = SA_ask_role_name(tRole, szRolename);
        if (BNL_em_error_handler("SA_ask_role_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
        fprintf(feilog, "%s: Role of the current user [%s] \n", BNL_module,szRolename);

        // Check if group/role is configured
        fprintf(feilog, "Checking groups/roles [%s]\n", pszGroupRole);

        pszGroupRoleCopy = BNL_strings_copy_string(pszGroupRole);

        pszDummy = strtok(pszGroupRoleCopy, ";");
        while (pszDummy != NULL)
        {
          fprintf(feilog, "Checking group/role [%s]\n", pszDummy);

          if ((lRoleOk = FEI_check_group_role(pszGroup, szRolename, pszDummy))) break;

          pszDummy = strtok(NULL, ";");
        }
      } // End of if (strlen(ppszVals[1] > 0))

      if (lRoleOk)
      {
        fprintf(feilog, "%s: The attribute [%s] is editable for the user.\n", BNL_module, pszAttr_1);
      }
      else
      {
        // Get the display name
        iRetCode = PROPDESC_ask_display_name_by_name(pszAttr_1, &pszDisplayName);
        if (BNL_em_error_handler("PROPDESC_ask_display_name_by_name", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

        if (pszGroupRole != NULL && strlen(pszGroupRole) > 0)
        {
          sprintf(szMessg, "The attribute [%s] is not editable. Only the user with group(s)/role(s) [%s] can edit this attribute.", pszDisplayName, pszGroupRole);
        }
        else
        {
          sprintf(szMessg, "The attribute [%s] is not editable.", pszDisplayName);
        }

        fprintf(feilog, "%s: %s \n", BNL_module, szMessg);

        if (giNX == 1 || lCreated || BNL_FEI_is_object_in_list(tObject))
        {
          if (giNX == 1)
          {
            fprintf(feilog, "%s: SaveAs/Revise from NX...\n", BNL_module, tObject);
          }

          if (pszOldAttr1Val != NULL)
          {
            fprintf(feilog, "%s: Warning, attribute [%s] will resetted to [%s].\n", BNL_module, pszDisplayName, pszOldAttr1Val);
            iRetCode = AOM_UIF_set_value(tObject, pszAttr_1, pszOldAttr1Val);
          }
          else
          {
            fprintf(feilog, "%s: Warning, attribute [%s] will be cleared.\n", BNL_module, pszDisplayName);
            iRetCode = AOM_UIF_set_value(tObject, pszAttr_1, "");
          }
          if (BNL_em_error_handler("AOM_UIF_set_value", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

          fprintf(feilog, "%s: Attribute was resetted.\n", BNL_module);
        }
        else
        {
          iRetCode = BNL_error_message + giErrorOffset;
          BNL_em_error_handler_s1(NULL, BNL_module, EMH_severity_error, iRetCode, true, szMessg);

          goto function_exit;
        }
      }
    } //  End of if (lCheck == true)


    if (pszGroupRoleCopy != NULL)
    {
      free(pszGroupRoleCopy);
      pszGroupRoleCopy = NULL;
    }

    if (pszAttr1Val != NULL)
    {
      MEM_free(pszAttr1Val);
      pszAttr1Val = NULL;
    }

    if (pszAttr2Val != NULL)
    {
      MEM_free(pszAttr2Val);
      pszAttr2Val = NULL;
    }

    if (pszOldAttr1Val != NULL)
    {
      MEM_free(pszOldAttr1Val);
      pszOldAttr1Val = NULL;
    }
    if (pszPrefValue)
    {
      free(pszPrefValue);
      pszPrefValue = NULL;
    }
    if (pszDisplayName)
    {
      MEM_free(pszDisplayName);
      pszDisplayName = NULL;
    }
    if (pszGroup != NULL)
    {
      MEM_free(pszGroup);
      pszGroup = NULL;
    }

    if (pszGroupTemp != NULL)
    {
      MEM_free(pszGroupTemp);
      pszGroupTemp = NULL;
    }
  } // End of for (k = 0; k < iCount; k++)


function_exit:

  if (pszAttr1Val != NULL) MEM_free(pszAttr1Val);
  if (pszAttr2Val != NULL) MEM_free(pszAttr2Val);
  if (pszOldAttr1Val != NULL) MEM_free(pszOldAttr1Val);
  if (pszPrefValue) free(pszPrefValue);
  if (pszDisplayName) MEM_free(pszDisplayName);
  if (pszGroup != NULL) MEM_free(pszGroup);
  if (pszGroupTemp != NULL) MEM_free(pszGroupTemp);
  if (pszGroupRoleCopy != NULL) free(pszGroupRoleCopy);

  fprintf(feilog, "** End ... FEI_check_attributes_security\n");

  return iRetCode;
}


int BNL_FEI_add_form_to_list(void * pReturnValue)
{
	int iRetCode				      = 0;

  tag_t tForm               = NULL_TAG;

  char szObjectType[WSO_name_size_c + 1] = {""};
  char szObjectName[WSO_name_size_c + 1] = {""};


  fprintf(feilog, "*** In BNL_FEI_add_form_to_list.\n");

  iRetCode = USERARG_get_tag_argument(&tForm);
	if (BNL_em_error_handler("USERARG_get_tag_argument", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = WSOM_ask_object_type(tForm, szObjectType);
  if (BNL_em_error_handler("WSOM_ask_object_type (object)", BNL_module, EMH_severity_error, iRetCode, true)) return false;

  iRetCode = WSOM_ask_name(tForm, szObjectName);
  if (BNL_em_error_handler("WSOM_ask_object_type (object)", BNL_module, EMH_severity_error, iRetCode, true)) return false;

  giForms++;
  gpszTypes = MEM_realloc(gpszTypes, sizeof(tag_t) * giForms);
  gpszNames = MEM_realloc(gpszNames, sizeof(tag_t) * giForms);

  gpszTypes[giForms - 1] = MEM_string_copy(szObjectType);
  gpszNames[giForms - 1] = MEM_string_copy(szObjectName);

  fprintf(feilog, "%s: Form with name [%s] of type [%s] will be added.\n", BNL_module, szObjectName, szObjectType);

function_exit:


	fprintf(feilog,"*** End BNL_FEI_add_form_to_list.\n\n");

	return iRetCode;
}

int BNL_FEI_remove_form_from_list(void * pReturnValue)
{
	int iRetCode				      = 0;
  int i                     = 0;
  int iCount                = 0;

  tag_t tForm               = NULL_TAG;

  char **pszTempTypes       = NULL;
  char **pszTempNames       = NULL;

  char szObjectType[WSO_name_size_c + 1] = {""};
  char szObjectName[WSO_name_size_c + 1] = {""};


  fprintf(feilog, "*** In BNL_FEI_remove_form_from_list.\n");

  iRetCode = USERARG_get_tag_argument(&tForm);
	if (BNL_em_error_handler("USERARG_get_tag_argument", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = WSOM_ask_object_type(tForm, szObjectType);
  if (BNL_em_error_handler("WSOM_ask_object_type (object)", BNL_module, EMH_severity_error, iRetCode, true)) return false;

  iRetCode = WSOM_ask_name(tForm, szObjectName);
  if (BNL_em_error_handler("WSOM_ask_object_type (object)", BNL_module, EMH_severity_error, iRetCode, true)) return false;

  //fprintf(feilog, "DEBUG: Form name [%s] of type [%s] .\n", szObjectName, szObjectType);

  for (i = 0; i < giForms; i++)
  {
    //fprintf(feilog, "DEBUG: Form [%d] name [%s] of type [%s].\n", (i + 1), gpszNames[i], gpszTypes[i]);
    // Recreate list
    if (strcmp(szObjectType, gpszTypes[i]) != 0 || strcmp(szObjectName, gpszNames[i]) != 0)
    {
      iCount++;
      pszTempTypes = MEM_realloc(pszTempTypes, sizeof(tag_t) * iCount);
      pszTempNames = MEM_realloc(pszTempNames, sizeof(tag_t) * iCount);

      pszTempTypes[iCount - 1] = MEM_string_copy(gpszTypes[i]);
      pszTempNames[iCount - 1] = MEM_string_copy(gpszNames[i]);
    }
    else
    {
      fprintf(feilog, "%s: Form with name [%s] of type [%s] was removed from list.\n", BNL_module, gpszNames[i], gpszTypes[i]);
    }
  }

  if (giForms > 0)
  {
    for (i = 0; i < giForms; i ++)
    {
      if (gpszTypes[i] != NULL) MEM_free(gpszTypes[i]);
      gpszTypes[i] = NULL;
      if (gpszNames[i] != NULL) MEM_free(gpszNames[i]);
      gpszNames[i] = NULL;
    }
    if (gpszTypes != NULL) MEM_free(gpszTypes);
    gpszTypes = NULL;
    if (gpszNames != NULL) MEM_free(gpszNames);
    gpszNames = NULL;
  }
  giForms = iCount;
  gpszTypes = pszTempTypes;
  gpszNames = pszTempNames;


function_exit:

	fprintf(feilog,"*** End BNL_FEI_remove_form_from_list.\n\n");

	return iRetCode;
}

void BNL_FEI_empty_form_list()
{
	int i				      = 0;

  if (giForms > 0)
  {
    for (i = 0; i < giForms; i ++)
    {
      if (gpszTypes[i] != NULL) MEM_free(gpszTypes[i]);
      gpszTypes[i] = NULL;
      if (gpszNames[i] != NULL) MEM_free(gpszNames[i]);
      gpszNames[i] = NULL;
    }
    if (gpszTypes != NULL) MEM_free(gpszTypes);
    gpszTypes = NULL;
    if (gpszNames != NULL) MEM_free(gpszNames);
    gpszNames = NULL;
  }
  giForms = 0;
}


logical BNL_FEI_is_object_in_list(tag_t tObject)
{
  int iRetCode              = 0;
  int i                     = 0;

  char szObjectType[WSO_name_size_c + 1] = {""};
  char szObjectName[WSO_name_size_c + 1] = {""};


  iRetCode = WSOM_ask_object_type(tObject, szObjectType);
  if (BNL_em_error_handler("WSOM_ask_object_type (object)", BNL_module, EMH_severity_error, iRetCode, true)) return false;

  iRetCode = WSOM_ask_name(tObject, szObjectName);
  if (BNL_em_error_handler("WSOM_ask_object_type (object)", BNL_module, EMH_severity_error, iRetCode, true)) return false;

  for (i = 0; i < giForms; i++)
  {
    if (strcmp(szObjectType, gpszTypes[i]) == 0 && strcmp(szObjectName, gpszNames[i]) == 0)
    {
      fprintf(feilog, "%s: Object with name [%s] of type [%s] is in list.\n", BNL_module, gpszNames[i], gpszTypes[i]);
      return true;
    }
  }

  fprintf(feilog, "%s: Object with name [%s] of type [%s] is NOT in list.\n", BNL_module, szObjectName, szObjectType);

	return false;
}


int BNL_fei_attr_nx_pre_saveas(char *pszInputString)
{
  int iRetCode        = 0;

  char *pszItemId           = NULL;
  char *pszRevId            = NULL;

  tag_t tRevOld             = NULL_TAG;
  tag_t tRevNew             = NULL_TAG;

  char *pszTemp             = NULL;

  fprintf(feilog, "%s: Input string [%s].\n", BNL_module, pszInputString);

  giNX = 1;
  giRevise = 0;

  //fprintf(feilog, "DEBUG: giRevise 1 [%d]\n", giRevise);

  pszTemp = BNL_strings_copy_string(pszInputString);

  pszItemId = strtok(pszTemp, ":");
  pszRevId = strtok(NULL, ":");

  if (gpszNxOldItemId != NULL)
  {
    free(gpszNxOldItemId);
    gpszNxOldItemId = NULL;
  }

  if (gpszNxOldRevId != NULL)
  {
    free(gpszNxOldItemId);
    gpszNxOldRevId = NULL;
  }

  gpszNxOldItemId = BNL_strings_copy_string(pszItemId);
  gpszNxOldRevId = BNL_strings_copy_string(pszRevId);

  if (pszTemp != NULL)
  {
    free(pszTemp);
  }

  return iRetCode;
}


int BNL_fei_attr_nx_post_saveas(char *pszInputString)
{
  int iRetCode        = 0;

  char *pszItemId           = NULL;
  char *pszRevId            = NULL;

  char *pszTemp             = NULL;


  fprintf(feilog, "%s: Input string [%s].\n", BNL_module, pszInputString);

  giNX = 0;
  giRevise = 0;
  gtIRMF = NULL_TAG;
  gtIMF = NULL_TAG;
  //fprintf(feilog, "DEBUG: giRevise 1 [%d]\n", giRevise);

  pszTemp = BNL_strings_copy_string(pszInputString);

  pszItemId = strtok(pszTemp, ":");
  pszRevId = strtok(NULL, ":");

  if (gpszNxNewItemId != NULL)
  {
    free(gpszNxNewItemId);
    gpszNxNewItemId = NULL;
  }

  if (gpszNxNewRevId != NULL)
  {
    free(gpszNxNewRevId);
    gpszNxNewRevId = NULL;
  }

  gpszNxNewItemId = BNL_strings_copy_string(pszItemId);
  gpszNxNewRevId = BNL_strings_copy_string(pszRevId);

  if (gpszNxOldItemId == NULL || gpszNxOldRevId == NULL)
  {
    fprintf(feilog, "%s: No old ItemRevision. Probably an error, nothing to do.\n", BNL_module);
    iRetCode = -1;
  }
  else if (strcmp(gpszNxOldItemId, gpszNxNewItemId) != 0)
  {
    fprintf(feilog, "%s: Save as.\n", BNL_module);

    iRetCode = 1;
  }
  else
  {
    if (strcmp(gpszNxOldRevId, gpszNxNewRevId) != 0)
    {
      iRetCode = 0;
      fprintf(feilog, "%s: Old item is is the same as new item id (revise). Nothing to do.\n", BNL_module);
    }
    else
    {
      fprintf(feilog, "%s: Old rev is is the same as new rev. Probably an error, nothing to do.\n", BNL_module);
      iRetCode = -1;
    }
  }

  if (pszTemp != NULL)
  {
    free(pszTemp);
  }

  return iRetCode;
}

int BNL_fei_attr_nx_create_predecessor(char *pszInputString)
{
  int iRetCode        = 0;

  tag_t tRevOld             = NULL_TAG;
  tag_t tRevNew             = NULL_TAG;


  ITEM_find_rev(gpszNxOldItemId, gpszNxOldRevId, &tRevOld);
  ITEM_find_rev(gpszNxNewItemId, gpszNxNewRevId, &tRevNew);

  if (tRevOld != NULL_TAG && tRevNew != NULL_TAG)
  {
    fprintf(feilog, "%s: Creating predecessor.\n", BNL_module);
    iRetCode = BNL_assign_predeccesor_relation(tRevNew, tRevOld);
  }

  // Reset
  if (gpszNxOldItemId != NULL)
  {
    free(gpszNxOldItemId);
    gpszNxOldItemId = NULL;
  }

  if (gpszNxOldRevId != NULL)
  {
    free(gpszNxOldItemId);
    gpszNxOldRevId = NULL;
  }

  return iRetCode;
}
