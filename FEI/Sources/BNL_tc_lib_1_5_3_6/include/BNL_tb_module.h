/**
 (C) copyright by UGS Benelux 2001

 Developed for IMAN
 Versions     OS                          platform
 -----------------------------------------------------------------------------------------------------------
 5.x									  Intel
 7.x                                      Intel

 Description    :   This module header file belongs to the handler tools module .c file.
                    The header file should be added to the modules.h file if this module should be
                    added to the customization.

 Created for    :   Unigraphics Solutions BENELUX


 Comments       :   Every new procedure or function in this mdoules starts
                    with BNL_<module>_

 Filename       :   $Id: BNL_tb_module.h 1590 2015-10-27 09:39:40Z tjonkonj $

 Version info   :   release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 reason /description                                            By      Version     Date
 ----------------------------------------------------------------------------------------------------------
 Creation of this file in new structure                         EN        0.0       18-12-2000
 Release of old mdoule file was 2.7 this will be relased to     EN        3.0       18-12-2000
 Comment improved of BNL_load_object                            HH        3.1       16-02-2001
 added:     BNL_tb_get_dataset_namedrefnames
            BNL_tb_get_dataset_namedrefs

 added:     BNL_tb_set_instance_field_stringvalue               EN        3.2       02-03-2001
            BNL_tb_get_instance_field_stringvalue
            BNL_tb_rename_status
 Print statements added in BNL_tb_get_arguments_optional:
 all arguments are printed in the log file.                     HH        3.3       30-03-2001
 added:     BNL_tb_bom_apply_config_rule
            BNL_tb_bom_get_item_revs
            BNL_tb_bom_check_item_rev_stats
            BNL_tb_value_in_list                                HH        3.4       24-04-2001
 added:     BNL_tb_ask_object_statuses
            BNL_tb_remove_status                                HH        3.5       02-05-2001

 Changed to V7 UGSBENELUX label to BNL                          EN        3.6       07-05-2001
 Updated for new structure of the BNL layers and naming
 convention                                                     LB        3.7       14-08-2001
 Removed register and initialize functions and moved them
 to BNL_ue_module
 Promote UGSBENELUX_handler_tools.h into BNL_tb_module.h        EN        0.0       17-08-2001
 Added global BNL version definition file                       LB        0.1       06-09-2001
 Added function BNL_tb_get_property                             LB        0.2       15-10-2001
 added function BNL_tb_replace_env_for_path add
 also in fm module becuase of free and SM_free conflict         EN        0.3
 Added function BNL_tb_create_dataset                           LB        0.4       24-10-2001
 Added function BNL_tb_get_named_references                     EN        0.5       14-01-2002
 change of int BNL_tb_bom_get_item_revs                         EN        0.6       06-03-2002
 added BNL_tb_get_form_value                                    EN        0.7       02-07-2002
 added BNL_tb_get_occurences
       BNL_tb_get_bom_occurences                                EN        0.8       10-07-2002

 added BNL_tb_add_status_to_task                                EN        0.9       26-07-2002
 changed function arguments of BNL_tb_check_item_rev_stats      EN        1.0       14-12-2002
 added BNL_tb_move_to_folder BNL_tb_find_folder                 EN        1.1       08-12-2002
 added int BNL_tb_ask_object_status_tag                         EN        1.2       06-08-2003
 Added function BNL_values_in_list                              EN        1.3       08-09-2005
 Make it BNL_lib_dll ready                                      EN        1.4       11-05-2006
 added new function BNL_tb_get_wso_projects                     EN        1.5       19-08-2006
 renamed and add functions to get alias list email addresses    EN        1.6       26-08-2006
 Added function BNL_tb_check_access_on_object                   JM        1.7       06-10-2006
 Export function BNL_tb_add_objects_to_task                     JM        1.8       02-10-2008
 Updated includes for Unified Architecture                      JM        1.9       18-02-2008
 Added function BNL_tb_replace_all_env_for_path                 JM        2.0       21-04-2008
 Added function BNL_tb_replace_symbols                          JM        2.1       02-06-2008
 Added function BNL_tb_get_extension_rule_param                 JM        2.2       09-23-2008
 Added function BNL_tb_retrieve_all_subtasks                    JM        2.3       10-07-2009
 Added function BNL_tb_determine_super_class                    JM        2.4       04-09-2009
 Added functions BNL_tb_pref_ask_char_value(s)                  JM        2.5       18-01-2010
 Updated for usage in C++ env                                   JM        2.6       17-05-2010
 Added BNL_find_group and BNL_tb_get_propvalue                  JM        2.7       10-10-2011
 Added BNL_tb_set_nonlocalized                                  JM        2.8       21-11-2011
 Added BNL_tb_item_find_rev and BNL_tb_item_find_item           JM        2.9       24-12-2012
 Updated for TC10.1                                             JM        3.0       29-08-2013
 Added BNL_tb_get_non_note_excluded_revs                        JM        3.1       30-09-2013
 Added BNL_tb_get_date_time_stamp                               DTJ       3.2       27-10-2015
*/

/*
  BNL Handler Tools Module (tb)

*/

#ifndef BNL_TB_H
#define BNL_TB_H

/*OS and Ansi includes*/
#include <ctype.h>
#include <stdlib.h>
#include <basetyps.h>


/*iMAN includes*/
#ifdef  TC10_1
#include <tc/iman_preferences.h>
#include <itk/bmf.h>
#include <tc/mail_errors.h>
#include <tc/preferences.h>
#include <pom/pom/pom.h>
//#include <pom_errors.h>
//#include <ps.h>
#include <tccore/item.h>
#include <tc/iman.h>
#include <tccore/tctype.h>
#include <tc/iman_util.h>
#include <user_exits/user_exits.h>
#include <tc/tc_arguments.h>
#include <sa/sa.h>
#include <ae/ae.h>
#include <tccore/aom.h>
#include <sa/am.h>
#include <tc/iman_arguments.h>
#include <bom/bom.h>
#include <cfm/cfm.h>
//#include <cr.h>
//#include <cr_errors.h>
#include <tc/emh.h>
#include <tc/envelope.h>
#include <epm/epm.h>
#include <epm/epm_errors.h>
#include <epm/epm_toolkit_tc_utils.h>

#include <tc/folder.h>

#include <tccore/grm.h>
#else
#include <ae.h>
#include <aom.h>
#include <am.h>

#include <bom.h>

#include <cfm.h>
#include <cr.h>
#include <cr_errors.h>

#include <emh.h>
#include <envelope.h>
#include <epm.h>
#include <epm_errors.h>
#include <epm_toolkit_utils.h>

#include <folder.h>

#include <grm.h>
#include <mail_errors.h>

#include <preferences.h>
#include <pom.h>
#include <pom_errors.h>
#include <ps.h>

#include <item.h>
#include <iman.h>
#include <imantype.h>
#include <iman_util.h>

#include <user_exits.h>

#include <iman_arguments.h>

#include <sa.h>
#endif


#if defined TCENG10000 && !defined TC10_1
#include <bmf.h>
#endif

#include <BNL_version.h>
#include <BNL_tceng_lib.h>


#define ARGVALLENGTH 62
#define BNL_sz_length 100

#define BNLNAMEDREFTEXT "Text"
#define BNLDSTYPETEXT   "Text"


typedef struct sNamedReference
{
    tag_t                   tDataset;
    char                    szNamedRefName[64];
    tag_t                   tReference;
} sNamedReference_t;

/**
char * BNL_tb_ask_version()

  Description:

  Returns the version of he module.

  parameters: none
*/
EXTERN_C char DllExport *BNL_tb_ask_version
();

/**
int BNL_tb_set_log
(
    FILE *pLog      <I>
    char *pszIni    <I>
)

  Description:

  This function sets the log pointer to the given
  logpointer. If not set then the internal logpointer points
  stderr.

  If the toolbased is used with handlers or other functions
  which uses ugslog then with this function the ugslog can set.


  the pszIni option is not used, but is availble because
  of module consistence reasons


  Returns:

  0 on Ok

*/
EXTERN_C int DllExport BNL_tb_set_log
(
    FILE *pLog
);

/**
int BNL_tb_initialise
(
    FILE *plog      <I>
)

  Description:


  Returns:

    ITK_ok

*/
EXTERN_C int DllExport BNL_tb_initialise
(
    FILE *plog
);

/**
int BNL_tb_exit
(
)

  Description:

  Exit the module in save way after calling this function
  and reuse this module it needs to be initialised again

  Returns:

  0 on Ok
*/
EXTERN_C int DllExport BNL_tb_exit
(
);

/**
int int BNL_tb_parse_multiple_values
(
    char *pszUnparsedval,  <I>
    char cSep,             <I>
    int *iCount,           <O>
    char ***pszValuelist   <OF> * n
)


  Description:

  Parses a value containing values seperated by a given separator into an array with values.
  The array should be freed by SM_free n times.
  An example of unparsed values is:
  item;document;eco

  Return value:

  ITK_ok

*/
EXTERN_C int DllExport BNL_tb_parse_multiple_values
(
 char *pszUnparsedval,
 char cSep,
 int *iCount,
 char ***pszValuelist
);

/**
int BNL_tb_get_arguments_optional
(
    IMAN_argument_list_t *ArgList,        <I>
    tag_t Task,                           <I>
    char *selectargument,                 <I>
    logical lOptional,                    <I>
    int *n_Vals                           <O>
    char ***pszArgValue,                  <OF>

)


Description :   This function loops through the arguments given in a handler definition
                list and searches for the given switch. It gives back a list with values
                of the switch.
                Depending on the lOptional the argument must be present in the definition
                of the handler or not.

                example -type=item;document

                where -type is the switch
                item and document are the two value which appear in the array.

Returns     :   ITK_ok or BNL_message_argument_missing

*/
EXTERN_C int DllExport BNL_tb_get_arguments_optional
(
 IMAN_argument_list_t *ArgList,
 tag_t Task, char *selectargument,
 logical lOptional,
 int *n_Vals,
 char ***pszArgValue
);

/**
int BNL_tb_translate_action_string
(
    char szActionString[],      <I>
    EPM_action_t *tAction       <O>
)

  Description:

    This function translates the task action string in an EPM_action_t.
    The possible strings are mentioned at the documentation of the routine
    EPM_ask_action_string.


  Returns:

    ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_translate_action_string
(
    char szActionString[],
    EPM_action_t *tAction
);

/**
int BNL_tb_execute_procedure
(
    tag_t *ptTargets,               <I>
    int iCount,                     <I>
    char *pszProcedureName,         <I>
    char *pszJobname,               <I>
    char *pszDescription,           <I>
    tag_t *tCreatedJob              <O>
)


  Description:

    This funtion executes a workflow procedure.


Returns     :   ITK_ok or !ITK_ok
*/
EXTERN_C int	DllExport BNL_tb_execute_procedure
(
    tag_t *ptTargets,
    int iCount,
    char *pszProcedureName,
    char *pszJobname,
    char *pszDescription,
    tag_t *tCreatedJob
);

/**
int BNL_tb_delete_job
(
    tag_t tJob,             <I>
    tag_t *ptTargets,       <I>
    int iCountOfTargets     <I>
)



  Description:

    This function is used to delete the job in proper way.
    At this moment we have big problems with it.!!!
    Job will not be deleted in VT100 mode.


  Returns:

    ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_delete_job
(
    tag_t tJob,
    tag_t *ptTargets,
    int iCountOfTargets
);

/**
int BNL_tb_determine_object_class
(
    tag_t tObject,              <I>
    char *pszClassName          <O>
)

  Description:

    Determines the class where he instance object is inherited from.

  Return:

    ITK_ok !ITK_ok


*/
EXTERN_C int DllExport BNL_tb_determine_object_class
(
    tag_t tObject,
    char *pszClassName
);


/**
int BNL_tb_determine_super_class
(
  tag_t tObject,              <I>
  char *pszClassName          <O>
)

  Description:

    Gets the name of the highest superclass before WorkspaceObject.

  Return:

    ITK_ok !ITK_ok


*/

EXTERN_C int DllExport BNL_tb_determine_super_class
(
  tag_t tObject,
  char *pszClassName
);

/**
int BNL_tb_determine_object_type
(
    tag_t tObject,              <I>
    char *pszTypeName           <O>
)

  Description:

     Function to determine the type of the given iMAN object

  Returns:

  ITK_ok !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_determine_object_type
(
    tag_t tObject,
    char *pszTypeName
);

/**
int BNL_tb_ask_object_status
(
    tag_t tObjectTag,   <I>
    char *pszStatus     <O>
)

  Description:

  This function asks the status of an object. If there are more then one status the
  last status is given back.
  If the object has no status the pszStatus argument will contain '-'

*/
EXTERN_C int DllExport BNL_tb_ask_object_status
(
    tag_t tObjectTag,
    char *pszStatus
);

/**
int BNL_tb_load_object
(
    tag_t Target,   <I>
    int iLock       <I>
)

  Description:

    This function loads the object with the given lock,
    before locking it will be checked if the object is allready locked
    if so then the lock will be changed. it checks also if the object is newly
    created then it is not needed to load the object and only the lock
    will be changed.

  Returns:

  ITK_ok or !ITK_ok


*/
EXTERN_C int DllExport BNL_tb_load_object
(
    tag_t Target,
    int iLock
);

/**
int BNL_tb_get_itemrev_id
(
    tag_t tItemRev,     <I>
    char *pszRevId      <O>
)

  Description:
    Gets the itemrevision id as xxxx/x with hte appropiate seperator
    as defined in iman env file.

*/
EXTERN_C int DllExport BNL_tb_get_itemrev_id
(
    tag_t tItemRev,
    char *pszRevId
);

/**
int BNL_tb_report_instance_propertys
(
   int iCountOfObjects,         <I>
   tag_t *ptObjectInstances     <I>
)

  Description:

      This function reports the properties of the given objects into
      the log file.

  Returns:


    ITK_ok

*/
EXTERN_C int DllExport BNL_tb_report_instance_propertys
(
    int iCountOfObjects,
    tag_t *ptObjectInstances
);

/**
int BNL_tb_get_dataset_revisions
(
    tag_t ptDataSet,        <I>
    tag_t **ptRevisions,    <OF>
    int *iCount             <O>
)

  Description:

    Funtion to get get all dataset versions of a particular dataset.

  Returns:

    Returns ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_get_dataset_revisions
(
    tag_t ptDataSet,
    tag_t **ptRevisions,
    int *iCount
);

/**
int BNL_tb_cut_out_statusses
(
    tag_t tTarget,              <I>
    char *pszReplaceStatus,     <I>
    int *iStatusPos,            <O>
    int *iRevStatusLen,         <O>
    tag_t **tStatusListTags     <OF>
)

    Description :

        Cuts out statusses of the target, and gives them back in a list.
        The status tags are explecitiet unlocked.

        The position is the position of the status where it was cut out.

    Returns     :

        RetCode ITK_ok or !ITK_ok

    Parameters  :

        None
*/
EXTERN_C int DllExport BNL_tb_cut_out_statusses
( tag_t tTarget,
  char *pszReplaceStatus,
  int *iStatusPos,
  int *iRevStatusLen,
  tag_t **tStatusListTags
);

/**
int BNL_tb_create_new_status
(
    char *pszStatus,            <I>
    tag_t *tNewStatus           <O>
)

    Description :

        Creates new status and gives it back.

    Returns     :

        RetCode ITK_ok or !ITK_ok

    Parameters  :

        None
*/
EXTERN_C int DllExport BNL_tb_create_new_status
(
    char *pszStatus,
    tag_t *tNewStatus
);

/**
int BNL_tb_add_new_status
(
    tag_t tTarget,      <I>
    tag_t tNewStatus,   <I>
    int iPosition,      <I>
    logical lSetRelDate <I>
)

    Description :

        Adds a status to a given target at a given position and sets the release date if lSetRelDate is true.

    Returns     :

        RetCode ITK_ok or !ITK_ok

    Parameters  :

        None
*/
EXTERN_C int DllExport BNL_tb_add_new_status
(
    tag_t tTarget,
    tag_t tNewStatus,
    int iPosition,
    logical lSetRelDate
);

/**
int BNL_tb_delete_status_instances
(
    tag_t *tStatusListTags, <I>
    int iRevStatusLen       <I>
)

    Description :

        Deletes statusses and frees them, be sure the status is not referenced
        anymore. Call BNL_remove_statusses_from_task before calling this procedure.
        This procedure will handle the audit file attachment.

    Returns     :

        RetCode ITK_ok or !ITK_ok

    Parameters  :

        None
*/
EXTERN_C int DllExport BNL_tb_delete_status_instances
(
    tag_t *tStatusListTags,
    int iRevStatusLen
);

/**
int BNL_tb_set_release_date
(
    tag_t tObject,      <I>
    tag_t tRelStat,     <I>
    logical lEmpty      <I>
)

    Description :

        The idea was to set the release date field with the creation date of the
        release status. But the release status has not a creaton date. Now we put in
        the creation date  of the object if handler is in update mode, and else the modification
        date of the object be aware before use this function you must do a save or set the
        modification date first.

    Returns     :

        RetCode ITK_ok or !ITK_ok

    Parameters  :

        None
*/
EXTERN_C int DllExport BNL_tb_set_release_date
(
    tag_t tObject,
    tag_t tRelStat,
    logical lEmpty
);

/**
int BNL_tb_update_release_field
(
    tag_t tObject      <I>
)

    Description :

        Updates release date of object.

    Returns     :

        RetCode ITK_ok or !ITK_ok

    Parameters  :

        None
*/
EXTERN_C int DllExport BNL_tb_update_release_field
(
    tag_t tObject
);

/**
int BNL_tb_get_alias_list_content
(
    char *pszAliasListName, <I>
    int *piCount,           <O>
    char ***pszMaillist     <OF * n>

  Description:

    This function gets contents of the given alias list. The alias list
    is used to store mail adresses outside tceng. The array contains content of
    the alias list these can be users groups and email addresses.

  Returns:

  ITK_ok or !ITK_ok where !ITK_ok is the error code.
)

*/
EXTERN_C int BNL_tb_get_alias_list_content
(
    char *pszAliasListName,
    int *piCount,
    char ***pszMaillist
);



/*
int DllExport BNL_tb_get_alias_list_mailadresses
(
char *pszAliasListName,
int *piCount,
char ***pszMaillist
);

Description:

Get from the alias list users,groups and collect the email addresses.
Also emailaddresses direct in the alias list are supported.
*/

EXTERN_C int DllExport BNL_tb_get_alias_list_mailadresses
(
char *pszAliasListName,
int *piCount,
char ***pszMaillist
);

/**
int BNL_tb_determine_id_structure
(
  char *CmprStruc,      <I>
  char *IDStruc,        <I>
  logical *lIdEqual     <I>
)


Description :   This fuction compares the ID structure with a given structure
                it checks on NC lenght and character or numbers.
                If a joker (?) is used on a certain position in the given structure then any character
                will do on that spot in the ID.

Returns     :   ITK_ok if stucture is equal or !ITK_ok if not. If not ITK_ok the
                error messages is present on the error stack can be called with
                UIF_display_error_stack(NULL).

Parameters  :   char *CmprStruc <I> Structure of ID to compare with
                char *IDstruc   <I> ID to be compared


  /*idea if the joker is a * then it should be possible to have itemid with different
  lengths so only the start of he id should be look like the given struc
*/
EXTERN_C int DllExport BNL_tb_determine_id_structure
(
    char *CmprStruc,
    char *IDStruc,
    logical *lIdEqual
);

/**
int BNL_tb_get_dataset_namedrefs
(
    tag_t tDataset,             <I>
    int *iCountofobjects,       <O>
    tag_t **ptRefs              <OF>
)


Description :   Gets from the dataset the namedreferecences gives back the number of named
                reference tags and their tags.

                note: use BNL_tb_get_dataset_namedreferences in place of this function.

Returns     :   ITK_ok or !ITK_ok

Parameters  :   tag_t           tDataset                <I>
                int             *iCountofobjects        <O>
                tag_t           **ptRefs                <OF>
*/
EXTERN_C int DllExport BNL_tb_get_dataset_namedrefs
(
    tag_t tDataset,
    int *iCountofobjects,
    tag_t **ptRefs
);

/**

int BNL_tb_get_dataset_namedrefnames
(
    tag_t tDataset,             <I>
    int *iCountofobjects,       <O>
    char ***pszNames            <OF> * n
)

Description :   Gets from the dataset the number of named references and their names.

                Note: use BNL_tb_get_dataset_namedreferences in place of this function

Returns     :   ITK_ok or !ITK_ok

Parameters  :   tag_t           tDataset                <I>
                int             *iCountofobjects        <O>
                char            ***pszNames             <OF>
*/
EXTERN_C int DllExport BNL_tb_get_dataset_namedrefnames
(
    tag_t tDataset,
    int *iCountofobjects,
    char ***pszNames
);

/**
int BNL_tb_set_instance_field_stringvalue
(
  tag_t tInstance,              <I>
  char *pszAttributename,       <I>
  char *pszValue                <I>
)


  Description:

  Sets the value of an instance field
  The function also loads and unloads the instance.

  Returns:

  ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_set_instance_field_stringvalue
(
  tag_t tInstance,
  char *pszAttributename,
  char *pszValue
);

/**
int BNL_tb_get_instance_field_stringvalue
(
    tag_t tInstance,            <I>
    char *pszAttributename,     <I>
    char **pszValue             <OF>
)

  Description:

  Get the string value of an instance field.

  Returns:

  ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_get_instance_field_stringvalue
(
    tag_t tInstance,
    char *pszAttributename,
    char **pszValue
);

/**
int BNL_tb_rename_status
(
  tag_t tObject,        <I>
  char pszNewStatus     <I>
)

  description:

  Gets the status list od the given object and replaces the name of
  the latest status with the given name.

  Returns:

  ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_rename_status
(
   tag_t tObject,
   char *pszNewStatus
);

/**
int BNL_tb_bom_apply_config_rule
(
    tag_t tBomwindow,         <I>
    char *pszRule             <I>

)

  Description:

    This function applies a config rule to the BOM window.
    In V7 replaced by a new function.

  Returns:

    ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_bom_apply_config_rule
(
   tag_t tBomwindow,
   char *pszRule
);

/**
int BNL_tb_bom_get_item_revs
(
  tag_t tBomLine,           <I>
  int   iUnconfig           <I>
  int   iLevel,             <I>
  int   iMaxLevel,          <I>
  int   *iRevCount,         <O>
  tag_t **tItemRevs         <OF>

)

  Description:

    This function gets the relevant item rev tags from the
    bomview the ingoing bomline tag is the topline of the bom.
    It wil return itemrevision in array of tags which are found
    in the bom. If it is  multilevel bom then the given back revs can
    be stopped at a particular level deep with iMaxlevel.
    iLevel should be 0 if the tBonline is the topline.

    if iMaxlevel = -1 then it will return the revs of all levels.

    This function is called recursive.


  Returns:

    0 on Ok if returned -1 then there where unconfigured occurences in the
    bom.

*/
EXTERN_C int DllExport BNL_tb_bom_get_item_revs
(
  tag_t tBomLine,
  int   iUnconfig,
  int   iLevel,
  int   iMaxLevel,
  int   *iRevCount,
  tag_t **tItemRevs
);

/**
logical BNL_tb_check_item_rev_stats
(
    tag_t *tItemRevs,         <I>
    int iItemRevCount,        <I>
    char **ptStatusList,      <I>
    int iStatusCount,         <I>
    int iStatList             <I>
    int iUsingMessage,        <I>
    int *ErrorCount,          <O>
    char ***pszErrorMessages  <OF *n>

)

  Description:

    This function checks the statusses of the item revs to the given list.
    It will create a messages list of itemrevs which do not apply the status.
    The iUsingMessage message is the number of the error which should be generated.
    This number is coupled to the BNL error messages list so the messages can be
    customised by user.

    if the iCheckIfTarget is set the procedure will first check if the the object with
    the wrong status exists in the target list if so then do not generate a error.

    Message should have always 2 variables namely the itemrevid and the status

    if iStatList =1 then it will check any status if there are more statusses at the object.


  Returns:

    ITK_ok or !ITK_ok

*/
EXTERN_C logical  DllExport BNL_tb_check_item_rev_stats
(
    int  iTargets,
    tag_t *ptTargets,
    int  iCheckIfTarget,
   tag_t *tItemRevs,
   int iItemRevCount,
   char **pszStatusList,
   int iStatList,
   int iStatusCount,
   int iUsingMessage,
   int *ErrorCount,
   char ***ErrorMessages
);

/**
logical BNL_tb_value_in_list
(
    char *pszValue,     <I>
    int iListCount,     <I>
    char **pszList      <I>
)

  Description:

    Looks if the value appears in the given list if so a true will be send back
    otherwhise a false.


  Returns:

  true or false
*/
EXTERN_C logical  DllExport BNL_tb_value_in_list
(
   char *pszValue,
   int iListCount,
   char **pszList
);

/**
int BNL_tb_ask_object_statuses
(
    tag_t tObjectTag,      <I>
    int *iCountStatuses   <O>
    char **pszStatuses     <OF>
)

  Description:

  This function asks the statuses of an object and gives them back in
  a array of strings.

*/
EXTERN_C int DllExport BNL_tb_ask_object_statuses
(
    tag_t tObjectTag,
    int *iCountStatuses,
    char ***pszStatuses
);

/**
int BNL_tb_remove_status
(
    tag_t tTarget,              <I>
    char *pszStatusToRemove     <I>
    int *iPosition              <O>
)

    Description :

        Cuts out status from the target, returns the position. Only the last status with the required
        name will be removed.

    Returns     :

        RetCode ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_remove_status
(
    tag_t tTarget,
    char *pszStatusToRemove,
    int *iPosition
);

/**
int BNL_tb_remove_status_of_relation
(
    tag_t tTarget,              <I>
    char *pszStatusToRemove     <I>
    int *iPosition              <O>
)

    Description :

        Cuts out status from the objects on the given relation,
        returns the position. Only the last status with the required
        name will be removed.

    Returns     :

        RetCode ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_remove_status_of_relation
(
    tag_t tTarget,
    char *pszRelation,
    char *pszStatusToRemove,
    int  *iPosition
);

/**
int BNL_tb_add_new_status_at_relation
(
    tag_t tTarget,      <I>
    char *pszStatus     <I>
    tag_t tNewStatus,   <I>
    int iPosition,      <I>
    logical lSetRelDate <I>
)

    Description :

        Adds a status to the relations of a given target at a given position and sets
        the release date if lSetRelDate is true.

    Returns     :

        RetCode ITK_ok or !ITK_ok

    Parameters  :

        None
*/
EXTERN_C int DllExport BNL_tb_add_new_status_at_relations
(
    tag_t tTarget,
    char *pszRelation,
    char *pszNewStatus,
    int iPosition,
    logical lSetRelDate
);

/**
int BNL_tb_set_instance_field_datevalue
(
  tag_t tInstance,              <I>
  char *pszAttributename,       <I>
  date_t *dDate                 <I>
)


  Description:

  Sets the date of an instance field
  The function loads and unloads the instance.

  Returns:

  ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_set_instance_field_datevalue
(
  tag_t tInstance,
  char *pszAttributename,
  date_t dDate
);

/**
int BNL_tb_get_instance_field_datevalue
(
    tag_t tInstance,            <I>
    char *pszAttributename,     <I>
    date_t **dDate              <F>
)

  Description:

  Get the date value of an instance field.

  Returns:

  ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_get_instance_field_datevalue
(
    tag_t tInstance,
    char *pszAttributename,
    date_t *dDate
);

/**
int BNL_tb_get_ugpart_drawings
(
   tag_t tUgDataset,        <I>
   int *iDrawingcount,      <O>
   char **pszDrawings       <OF * n>
)

  Description:

  Changed this function, give the namedreference and be sure that this
  named ref is a rerence to a ug part file. If it is a ug part file then
  it will get the drawings in it.


  The advantageof this function is that it not uses Unigraphics or UGMGR to get the
  drawings. In this way we prefent communications between applications and
  performance problems.

  Unfortunatly it not full proof we test it on diffrent part files but there
  could be cases that some drawings do not appear and that it will show views
  too.

  Returns:

  0 on Ok.
*/
EXTERN_C int DllExport BNL_tb_get_ugpart_drawings
(
   tag_t tUgDataset,
   int *iDrawingcount,
   char ***pszDrawings
);

/*
int BNL_tb_replace_env_for_path
(
    char *pszPath       <I>
    char **pszReplace   <OF>
)

  Description:

  This function will search for a environment setting
  in path like %iman_root% or $iman_root this part will be replaced by
  the real environment getted with getenv.

  If no path found then the returned value will be NULL.
  This same function is also available in the fm module difference
  is the way how the returned path is allocated.

  the returned valeu shold be freed with BNL_mm_free();

  Returns

  0 on Ok
*/
EXTERN_C int DllExport BNL_tb_replace_env_for_path
(
    char *pszPath,
    char **pszReplace
);

/**
int BNL_tb_get_property
(
    tag_t   tObject,        <I> object to get the property from
    char    *szPropName,    <I> name of the property
    char    **pszVal        <OF> value of the property
)

  Description:

    Get the value for a given property of a given object

  Returns:

    ITK_ok if ok

*/
EXTERN_C int DllExport BNL_tb_get_property
(
    tag_t   tObject,
    char    *szPropName,
    char    **pszVal
);

/**
int BNL_tb_create_dataset
(
    char *pszDSname,
    char *pszDSdesc,
    tag_t tDStype,
    tag_t *ptDataset
)

Description :   create a new dataset

Returns     :   Returns ITK_ok or RetCode

Parameters  :   char *pszDStype,    <I>
                char *pszDSname,    <I>
                char *pszDSdesc,    <I>
                tag_t *ptDataset    <O>
*/
EXTERN_C int DllExport BNL_tb_create_dataset
(
    char *pszDSname,
    char *pszDSdesc,
    tag_t tDStype,
    tag_t *ptDataset
);

/**
int BNL_tb_save_and_unlock_object
(
    tag_t     tObject
)

  Description:

    Save and unlock the given object.

  Returns:

    ITK_ok on success else RetCode

*/
EXTERN_C int DllExport BNL_tb_save_and_unlock_object
(
    tag_t     tObject
);

/**
int BNL_tb_import_file
(
    tag_t       tDatasettype,
    char        *pszFilename,
    char        *pszRefName,
    tag_t       *ptFile
)

  Description:

    Import the datafile into INFOMANAGER database.
    If file-name has an extension, strip it off from file name.
    Use the input file-name to name the dataset and for the
    associated Iman file, use the USER_new_file_name function
    to generate a unique name for it.

  Returns:

    ITK_ok upon success
    ITK library error code upon failure.
*/
EXTERN_C int DllExport BNL_tb_import_file
(
    tag_t       tDatasettype,
    char        *pszFilename,
    char        *pszDSname,
    char        *pszRefName,
    tag_t       *ptFile
);

/**
int BNL_tb_get_dataset_namedreferences
(
    tag_t tDataset,             <I>
    int *iCountofrefs,          <O>
    sNamedReference_t **ptRefs  <OF>
)


Description :   Gets from the dataset the namedreferecences gives back the number of named
                reference in a structure. In this structure is stored the datasettag,namedrefrences name
                and the namedreference tag.


Returns     :   ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_get_dataset_namedreferences
(
    tag_t tDataset,
    int *iCountOfRefs,
    sNamedReference_t **ptRefs
);

/**
int BNL_tb_add_targetref_ds
(
   int iCount,      <I>
   tag_t ptList,    <I>
   char *dsName,    <I>
   char *dsDesc,    <I>
   tag_t *ptDs      <O>
)

  Description:

  This function create a Text dataset and creates a list in it with the puid and info of the tags.
  The list can be read out with the function BNL_tb_get_tags_from_targetref_ds
*/
EXTERN_C int DllExport BNL_tb_add_targetref_ds
(
   int iCount,
   tag_t *ptList,
   char *dsName,
   char *dsDesc,
   tag_t *ptDs
);

/**
int BNL_tb_get_tags_from_targetref_ds
(
 char  *pszDsName,   <I>
 tag_t tJob,         <I>
 tag_t tDs,          <I>
 int *iCount,        <O>
 tag_t **ptList      <OF>
)

  Description:

  This function reads the given targetrefs dataset and gives back the a list
  of tags which was found in the dataset. If pszDsName is NULL and tJob is NULLTAG and tDs is filled in
  then it will not search for the dataset at the job

  Returns:
  0 on ok.
*/
EXTERN_C int DllExport BNL_tb_get_tags_from_targetref_ds
(
 char  *pszDsName,
 tag_t tJob,
 tag_t tDs,
 int *iCount,
 tag_t **ptList
);

/**
int BNL_tb_get_form_value
(
    tag_t tForm,            <I>
    char *pszField,         <I>
    char **char pszValue    <OF>
)

  Description:

  Gets the form value from the given field.

  Returns:

  ITK_ok on no errors.

*/
EXTERN_C int DllExport BNL_tb_get_form_value
(
    tag_t tForm,
    char *pszField,
    char **pszVal
);

/**
int BNL_tb_get_bom_occurences
(
  tag_t tItemRev,
  char *pszRevRule,
  char *pszBomType,
  int  iLevel,
  int *iOccurences,
  tag_t **ptOccurences,
  tag_t *tBomWin
)

  Description:

  Get all BOM occurences of a given Item Revision

  Returns:

  ITK_ok on no errors.

*/
EXTERN_C int DllExport BNL_tb_get_bom_occurences
(
  tag_t tItemRev,
  char *pszRevRule,
  char *pszBomType,
  int  iLevel,
  int *iOccurences,
  tag_t **ptOccurences,
  tag_t *tBomWin
);

/**
int BNL_tb_get_occurences
(
  tag_t tOccurence,         <I>
  int   iLevel,             <I>
  int   iMaxLevel,          <I>
  int   *iOccCount,         <O>
  tag_t **ptOccurences         <OF>

)

  Description:

    This function gets the boline tags from the bomview.
    The ingoing bomline tag is the topline of the bom.
    It wil return bomlines in array of tags which are found
    in the bom. If it is  multilevel bom then the given back bomlines can
    be stopped at a particular level deep with iMaxlevel.
    iLevel should be 0 if the tBomline is the topline.

    if iMaxlevel = -1 then it will return the revs of all levels.

    This function is called recursive.


  Returns:

    0 on Ok if returned -1 then there where unconfigured occurences in the
    bom.

*/
EXTERN_C int DllExport BNL_tb_get_occurences
(
 tag_t tOccurence,
 int iLevel,
 int iMaxLevel,
 int *iOccCount,
 tag_t **ptOccurences
);

/**
int BNL_tb_add_status_to_task
(
  tag_t tTask,
  tag_t tStatus
)

  Description:

  Adds the status object to the given task.

  Returns:

  0 on Ok

*/
EXTERN_C int DllExport BNL_tb_add_status_to_task
(
  tag_t tTask,
  tag_t tStatus
);

/**
int BNL_tb_find_folder
(
   char *pszFolderName,
   tag_t *tFolder
)

  Description:

  This function gets the asked folder. If there are more then two
  folders with the same name the function will return an error.

  Returns:

  ITK_ok or !ITK_ok
*/
EXTERN_C int DllExport BNL_tb_find_folder
(
   char *pszFolderName,
   tag_t *tFolder
);

/**
int BNL_tb_move_to_folder
(
  tag_t tObject,
  tag_t tSourceFolder,
  tag_t tTargetFolder
)

  Description:

  Move the the given object from the source folder to the
  target folder.
  If source folder is nulltag then it will place the object into
  the target folder.

*/
EXTERN_C int DllExport BNL_tb_move_to_folder
(
  tag_t tObject,
  tag_t tSourceFolder,
  tag_t tTargetFolder
);

/**
int BNL_tb_ask_object_status_tag
(
    tag_t tObjectTag,   <I>
    tag_t *tStatus     <O>
)

  Description:

  This function asks the tag of the status of an object. If there are more then one status the
  last status is given back.
  If the object has no status a NULLTAG is given back.

*/
EXTERN_C int DllExport BNL_tb_ask_object_status_tag
(
    tag_t tObjectTag,
    tag_t *tStatus
);

/**
int BNL_tb_copy_string
(
  char *pszString, <I>
  char **pszCopy   <OF>
)

  Description:

  The function will copy the given string into a new allocated string.
  The new string need to be free with standard free. If allocation fails
  or if input is NULL the returned string is also NULL.

*/
EXTERN_C int DllExport BNL_tb_copy_string
(
  char *pszString,
  char **pszCopy
);


/**
int BNL_tb_is_super_class
(
  char *szSuperClass,  <I>
  char *szClass,       <I>
  logical *lSuper      <O>
)

  Description:

  Function to determine if the class is a super class.

  i.e. to determine if the class MEOperation given is a child of superclass "Item"
  fill in for super class "Item" and for the class "MEOperation"

  returns false ot true if true superclass is i.e. Item.

*/
EXTERN_C int DllExport BNL_tb_is_super_class
(
  char *szSuperClass,
  char *szClass,
  logical *lSuper
);

/**
int BNL_tb_get_argument_optional
(
    IMAN_argument_list_t *ArgList,        <I>
    tag_t Task,                           <I>
    char *selectargument,                 <I>
    logical lOptional,                    <I>
    int *n_Vals                           <O>
    char ***pszArgValue,                  <OF>

)


Description :   This function loops through the arguments given in a handler definition
                list and searches for the given switch. It gives back the value of the switch.
                Depending on the lOptional the argument must be present in the definition
                of the handler or not.

                example -type=item

                where -type is the switch
                item is the value which is returned.

Returns     :   ITK_ok or BNL_message_argument_missing

*/
EXTERN_C int DllExport BNL_tb_get_argument_optional
(
    IMAN_argument_list_t *ArgList,
    tag_t Task,
    char *selectargument,
    logical lOptional,
    char **pszArgValue

);

/*
int BNL_tb_values_in_list
(
  iFirst,
  pszFirst,
  iSecond,
  pszSecond
)

  Description:

  Function that will check if in the list of values one of the values appears in the
  other list. If so then it will return a true (=1).

  It will check with the second list, so the second list is always looped. For performance
  reasons put in the second list always the shortest
*/
EXTERN_C int DllExport BNL_tb_values_in_list
(
  int iFirst,
  char **pszFirst,
  int iSecond,
  char **pszSecond
);

/**
int BNL_tb_form_set_value
(
  tag_t tForm,            <I>
  char *pszFormField,     <I>
  char *pszFormValue      <I>
)

  Descirption:

  Sets the field in the form.
  call AOM_save to save the set values in the db.
*/
EXTERN_C int DllExport BNL_tb_form_set_value
(
  tag_t tForm,
  char *pszFormField,
  char *pszFormValue
);



/*
int BNL_tb_form_ask_value
(
  tag_t tForm,            <I>
  char *pszFormField,     <I>
  char **pszFormValue     <OF>
)

  Description:

  Gets the value of a form field.
*/

EXTERN_C int DllExport BNL_tb_form_ask_value
(
  tag_t tForm,
  char *pszFormField,
  char **pszFormValue
);

EXTERN_C int DllExport BNL_tb_ask_object_status_tags
(
    tag_t tObjectTag,
	int   *iStatusCount,
    tag_t **tStatusses
);

/*
int BNL_tb_get_wso_projects
(
   tag_t tObjectTag,                <I>
   int *iProj,tag_t **ptProjects    <OF>
)

Description:

Function to get assigned project at the given object.
The object can be any workspaceobject.

*/
EXTERN_C int DllExport BNL_tb_get_wso_projects
(
   tag_t tObjectTag,
   int *iProj,tag_t **ptProjects
);

/*
int DllExport BNL_tb_get_prj_members
(
   tag_t tProject,
   int *iMembs,
   tag_t **ptMembers
)

Description:

Get the project team members. (=group members)

*/
EXTERN_C int DllExport BNL_tb_get_prj_members
(
   tag_t tProject,
   int *iMembs,
   tag_t **ptMembers
);

/*
int DllExport BNL_tb_get_proj_name_id
(
  tag_t tProject,
  char **pszProjId,    <OF>
  char **pszProjName,  <OF>
  char **pszProjDesc   <OF>
)
Description:

Get the project name, id and description.

*/
EXTERN_C int DllExport  BNL_tb_get_proj_name_id
(
  tag_t tProject,
  char **pszProjId,
  char **pszProjName,
  char **pszProjDesc
);

/**
logical DllExport BNL_tb_check_access_on_object
(
  tag_t tObject,         (I) object to check
  char *pszAccesToCheck  (I) access to check
)

  Description:

    Check access on a given object for the given

  Returns:

    True when current user has the requested access

  Remarks:

*/
EXTERN_C logical DllExport BNL_tb_check_access_on_object
(
  tag_t tObject,
  char *pszAccesToCheck
);

/**
EXTERN_C int DllExport BNL_tb_add_objects_to_task
(
  tag_t tRootTask,      <I>
  int iObjects,         <I>
  tag_t *ptObjects,     <I>
  int iAttType          <I>
);

Description:
Will add objects to the given task, attachment type is
determined by the given type

Possible attachment types:

EPM_target_attachment
EPM_reference_attachment
EPM_signoff_attachment
EPM_release_status_attachment
EPM_comment_attachment
EPM_instruction_attachment

*/
EXTERN_C int DllExport BNL_tb_add_objects_to_task
(
  tag_t tRootTask,
  int iObjects,
  tag_t *ptObjects,
  int iAttType
);

/**
int BNL_tb_replace_all_env_for_path
(
    char *pszPath       <I>
    char **pszReplace   <OF>
)

  Description:

  This function will search for environment settings
  in the path like %iman_root% or $iman_root.
  All these settings will be replaced by the real environment getted with getenv.

  If no path found then the returned value will be NULL.
  The returned valeu shold be freed with MEM_free();

  Returns

  0 on Ok
*/
EXTERN_C int DllExport BNL_tb_replace_all_env_for_path
(
  char *pszPath,
  char **pszReplace
);

/**
int BNL_tb_replace_symbols
(
  const char *pszIn,    <I>   Input string
  tag_t tTask,          <I>   Current task
  char **pszOut         <OF>  Output string
)

  Description:


  Returns

  0 on Ok

*/
EXTERN_C int DllExport BNL_tb_replace_symbols
(
  const char *pszIn,    /* <I>   Input string */
  tag_t tTask,          /* <I>   Current task */
  char **pszOut         /* <OF>  Output string */
);

#ifdef TCENG10000
/**
int BNL_tb_get_extension_rule_param
(
  BMF_extension_arguments_t* p,     <I>
  int iArgCount,                    <I>
  char *pszParamName                <I>
)

  Description:

    Get the index of the Extension Rule parameter.

  Returns

    The index of the parameter or -1 when not found.

*/
EXTERN_C int DllExport BNL_tb_get_extension_rule_param
(
  BMF_extension_arguments_t* p,
  int iArgCount,
  char *pszParamName
);
#endif

/**
int BNL_tb_retrieve_all_subtasks
(
  tag_t tTask,                     <I>
  char *pszType,                   <I>
  char *pszName,                   <I>
  int *piTasks,                    <O>
  tag_t **ptAllTasks               <OF>
)

  Description:

    Retrieves all sub tasks of the given type and name.

  Returns

    ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_retrieve_all_subtasks(tag_t tTask, char *pszType, char *pszName, int *piTasks, tag_t **ptAllTasks);


/**
int BNL_tb_pref_ask_char_values
(
  char *pszPrefName,                       <I>
  IMAN_preference_search_scope_t scope,    <I>
  int *piCount,                            <O>
  char ***pszValues                        <OF>
)

  Description:

    Gets the preference values.

    Scope can be:
      IMAN_preference_all
      IMAN_preference_user
      IMAN_preference_role
      IMAN_preference_group
      IMAN_preference_site

  Returns

    ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_pref_ask_char_values
(
  char *pszPrefName,
  IMAN_preference_search_scope_t scope,
  int *piCount,
  char ***pszValues
);


/**
int BNL_tb_pref_ask_char_value
(
  char *pszPrefName,                       <I>
  IMAN_preference_search_scope_t scope,    <I>
  char ***pszValue                         <OF>
)

  Description:

    Gets the preference value.

    Scope can be:
      IMAN_preference_all
      IMAN_preference_user
      IMAN_preference_role
      IMAN_preference_group
      IMAN_preference_site

  Returns

    ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_pref_ask_char_value
(
  char *pszPrefName,
  IMAN_preference_search_scope_t scope,
  char **pszValue
);


/**
int BNL_tb_find_group
(
  char *pszGroup,            <I> group name
  tag_t *ptGroup             <O> group tag
)

  Description:

    Gets the group with a given name.

  Returns

    ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_find_group
(
  char *pszGroup,
  tag_t *ptGroup
);


/**
int BNL_tb_get_propvalue
(
  tag_t tObject,        <I>  object to get the property from
  char *pszAttr,        <I>  name of the property (e.g. IMAN_master_form_rev.user_data_1)
  char **pszVal         <OF> value of the property
)

  Description:

    Get the value for the given property of a given object

  Returns

    ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_tb_get_propvalue
(
  tag_t tObject,
  char *pszAttr,
  char **pszVal
);

/**
void BNL_tb_set_localized
(
  logical lSwitch       <I>
)

  Description:

    When set to true, BNL_tb_get_property retrieves the non-localized value (in case of string).

*/
EXTERN_C void DllExport BNL_tb_set_nonlocalized(logical lSwitch);

/**
int BNL_tb_item_find_rev
(
  char *pszItemId,       <I>  item id
  char *pszRevid,        <I>  revision id
  tag_t *tRev            <O>  itemrevision
}

  Description:

    Wrapper for ITEM_find_rev

  Returns

    ITK_ok or !ITK_ok

*/
EXTERN_C int BNL_tb_item_find_rev
(
  char *pszItemId,
  char *pszRevid,
  tag_t *tRev
);


/**
int BNL_tb_item_find_item
(
  char *pszItemId,       <I>  item id
  tag_t *tItem           <O>  item
}

  Description:

    Wrapper for ITEM_find_item

  Returns

    ITK_ok or !ITK_ok

*/
EXTERN_C int BNL_tb_item_find_item
(
  char *pszItemId,
  tag_t *tItem
);

/**
int BNL_get_non_note_excluded_revs
(
  char       *pszNote,         <I>
  char       *pszValue,        <I>
  tag_t      tBomLine,         <I>
  int        iIgnoreUnconfig,  <I>
  int        iLevel,           <I>
  int        iMaxLevel,        <I>
  int        *iRevCount,       <O>
  tag_t      **tItemRevs       <OF>
)

  Description:

    This function gets the relevant item rev tags from the
    bomview. The ingoing bomline tag is the topline of the bom.
    It wil return itemrevisions in array,  which are filtered using
    a given note type and value, of tags which are found
    in the bom. If it is  multilevel bom then the given back revs can
    be stopped at a particular level deep with iMaxlevel.
    iLevel should be 0 if the tBomline is the topline.

    if iMaxlevel = -1 then it will return the revs of all levels.

    This function is called recursive.

  Returns:

    0 on Ok if returned -1 then there where unconfigured occurences in the
    bom.

*/
EXTERN_C int DllExport BNL_tb_get_non_note_excluded_revs
(
  char       *pszNote,
  char       *pszValue,
  tag_t      tBomLine,
  int        iIgnoreUnconfig,
  int        iLevel,
  int        iMaxLevel,
  int        *iRevCount,
  tag_t      **tItemRevs
);
EXTERN_C void DllExport BNL_tb_get_date_time_stamp(char **pszDate, char *format);
#endif
