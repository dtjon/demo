/***********************************************************************************************************************
 (C) copyright by UGS Benelux

 
 OS                          platform
 -----------------------------------------------------------------------------------------------------------
 Microsoft Windows NT		  Intel	

 Created by		:	E.Navis	

 Description	:   This header file belongs to the ini module.
                   
 				    
 Created for	:   UGS Benelux

 Comments		:   Every new procedure or function starts with BNL_
				    This Actionhandler uses BNL_tools.

 Filename		:	BNL_ini_module.h
	
 Using modules  :   BNL_file

 Version info   :   The number consist of the iman release and then the release of the handler
				    <release>.<pointrelease>

					
 History of changes											
 reason /description				                                           Version By          Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                               0.0     EH          15-06-2001
 Added global BNL version definition file                               0.1     LB          06-09-2001
 Added function BNL_ini_free_ini_array                                  0.2     LB          05-10-2001
 Added BNL_ini_set_comment_char to define different comment char        1.2     LB          02-12-2002
 Added BNL_ini_set_case_sensitive to set case sensitivity               1.2     LB          02-12-2002
 Make it BNL_lib_dll ready                                              1.3     EN          11-05-2006
 Updated for usage in C++ env                                           1.4     JM          17-05-2010
 Make BNL_ini_free_ini_array extern for usage in C++                    1.5     DTJ         21-01-2015                              
 ************************************************************************************************************************/

#ifndef BNL_INI_H
#define BNL_INI_H

#include <stdio.h>
#include <basetyps.h>
#include <BNL_tceng_lib.h>

typedef struct inivaluepair_s
{
    char *pszMainkey;
    char *pszKey;
    char *pszValue;

}inivaluepair_t;

/**
int BNL_ini_initialise
(  
  FILE *pLog,
  char *pszIni
)

  Description:

  Initialise the module

  Returns:

  0 on Ok
*/
EXTERN_C int DllExport BNL_ini_initialise
(
  FILE *pLog,
  char *pszIni
);

/**
int BNL_ini_exit
(
)

  Description:

  Exit the module in save way after calling this function
  and reuse this module it needs to be initialised again

  Returns:

  0 on Ok
*/
int DllExport BNL_ini_exit
(
);

/**
int BNL_ini_set_log
(
    FILE *fp
)

  Description:

  Sets the loging file pointer, if not set then the internal
  log pointer points to stderr.

  Returns:

  0 on Ok.


*/
EXTERN_C int DllExport BNL_ini_set_log
(
    FILE *fp
);

/**
int BNL_ini_set_comment_char
(
    char    cNewCommentChar
)

  Description:

    With this function the comment character can be changed

  Returns:

    0

*/
EXTERN_C int DllExport BNL_ini_set_comment_char
(
    char    cNewCommentChar
);

/**
int BNL_ini_set_case_sensitive
(
    int     bOnOff
)

  Description:

    With this function the case sensitive switch can be set

  Returns:

    0

*/
EXTERN_C int  DllExport BNL_ini_set_case_sensitive
(
    int     bOnOff
);

/**
int BNL_ini_set_case_sensitive
(
    int iNewBufSize
)

  Description:

    With this function the string size can be set, to enable longer lines

  Returns:

    0

*/
EXTERN_C int DllExport BNL_ini_set_max_string_size
(
    int iNewBufSize
);

/**
int BNL_ini_read_ini_value
(
    char *pszinifile,       <I>
    char *pszMainKey,       <I>
    char *pszKey,           <I>
    char **pszVal           <OF>
)



  Description:

  This function reads values in an ini file build up with keys and values. The function recognize comment
  by text that start with a #. The main key will be recognizede by text starting by [ and closing by ]

  example inifile entry:

  [mainkey]
  key=value #this comment

  a value with spaces can be given as follow
  key="Hello a key"

  The pszVal parameter must be freed by the calling function.

  The function returns 0 by getting value succesfull otherwhise a 1 will be returned.
  Max possible key amd value lengths are iMaxBufSize  characters


 

*/
EXTERN_C int DllExport BNL_ini_read_ini_value
(
    char *pszinifile,
    char *pszMainKey, 
    char *pszKey, 
    char **pszVal
);

/**
int BNL_ini_read_ini_array
(
    char *pszinifile,               <I>
    char *pszMainKey,               <I>
    int *iCount,                    <O>
    inivaluepair_t ***pValuepairs   <O n times>
)


  Description:

  This function reads the contents beneath the Mainkey of the inifile into a array of valuepair structures.

  example inifile entry:

  [mainkey]
  key=value #this will be read
  key=value #this will be read into struc

  The pszVal parameter must be freed by the calling function.

  The function returns 0 by getting value succesfull otherwhise a 1 will be returned.
  Max possible key amd value lengths are iMaxBufSize characters

  Mainkey attribute will not be used and remains empty

  Todo: spaces are not trimmed may be do this in the future

*/
EXTERN_C int DllExport BNL_ini_read_ini_array
(
    char *pszinifile,
    char *pszMainKey, 
    int *iCount, 
    inivaluepair_t ***pValuepairs  
);

/**
int BNL_ini_free_ini_array
(
    int iCount,                    <I>
    inivaluepair_t **pValuepairs   <I n times>
)


  Description:

  This function frees the memory, previousle allocated with BNL_ini_read_ini_array.

*/
EXTERN_C void DllExport BNL_ini_free_ini_array
(
    int iCount, 
    inivaluepair_t **pValuepairs  
);

/**
int BNL_ini_get_multiple_values
(
 char *pszInFile,   <I>
 char *pszMainKey,  <I>
 char *pszKey,      <I>
 char chSeparator,  <I>
 int  *iCount,      <O>
 char ***pszValues   <OF>


)

  Description:

  Funtion to get multiple values in the ini file an example entry is

  [multiple]
  key=value1;value2;value3

  Where the seperator in this example is ";"

  Returns:

  0 on Ok

*/
EXTERN_C int DllExport BNL_ini_get_multiple_values
(
 char *pszInFile,
 char *pszMainKey,
 char *pszKey,
 char chSeparator,
 int  *iCount,
 char ***pszValues
);

/**
char *BNL_ini_ask_version
(
)

  Description:

  Gives back the version of the module

  Returns:

  pointer to array of char
*/
EXTERN_C char DllExport *BNL_ini_ask_version
(
);

#endif
