/**
 (C) copyright UGS Benelux

 
 Description	:  Header file belonging to BNL_register_bnllibs
                   module is responsible for registering the bnl libs in custom dlls.
				   
 Created for	:   UGS


 Comments		:   Every new procedure or function in this mdoules starts 
                    with BNL_register_
				    

 Filename		:	BNL_register_bnllibs.h

 Version info   :   release.pointrelease (versions of the modules are stored in the module itself.
					
 History of changes											
 reason /description									Version By       date
 ----------------------------------------------------------------------------------------------------------
 Creation												      0.0     EN        29-04-2005
 added handler offset variable        0.2     EN        04-07-2005
 Make it dll ready                    0.3     EN        11-05-2006
*/

#include <BNL_tceng_lib.h>

/*The general used file pointer in handlers*/
FILE *ugslog;


/*the general used module error offset value*/
int giErrorOffset;

/*the errorlevel
0 = all logging into cmd screen
1 = all generic log into file.
10= all logging (including welcomes) into log file
20= all logging (including welcomes) into temporary log file
*/
int giErrorLevel;


/**
int DllExport BNL_register_bnllibs
(
  char *DLL_module,      <I>
  char *DLL_version,     <I>
  char *DLL_logfilename, <I>
  FILE **fpLog           <O>
);

  Description:

  Function called first to register all modules proper.

  The BNL will create an logfile, to write in the logfile
  use the returned fpLog.


*/
int DllExport BNL_register_bnllibs
(
  char *DLL_module,
  char *DLL_version,
  char *DLL_logfilename,
  FILE **fpLog
);


/**
int BNL_exit_bnllibs
(
  char *DLL_module
)

  Description:

  Function called to exit all modules proper.


*/
int DllExport BNL_exit_bnllibs
(
);

/**
int BNL_register_bnllibs_user
(
  char *DLL_module,               <I>
  FILE *fpLog                     <I>
)

Description:

This function will register the BNL libs but will not create its own 
logfile. It will write in the given logfile pointer.

DLL_module, The name of the dll module, used to determine the error_offset.
            if NULL no error offset used else in the preference file the error
            offset can be used <modulename>_error_offset=<a number>

fpLog, logfile pointer so the bnl modules will write there loging 
       if the fplog is NULL then it will write to the stderr.

*/
int DllExport BNL_register_bnllibs_user
(
  char *DLL_module,
  FILE *fpLog
);

void DllExport BNL_bnllibs_ask_log_file_location(char **pszLogFile);

