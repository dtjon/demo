/**
 (C) copyright by UGS Benelux

 
 Description    :   Header file. Belongs to the tcpip module.

 Created for    :   UGS BENELUX

 Comments       :   Every new procedure or function starts with BNL_tcpip_

 Filename       :   BNL_tcpip_module.c

 Version info   :   release.pointrelease

 History of changes
 Reason / Description                                       Version By          date
 -------------------------------------------------------------------------------------------
 Creation                                                   0.0     EN          26-04-2002
 WSOCCK.h was local defined moved it into the BNL include   0.1     EN          20-03-2006
 Make it BNL_lib_dll ready                                  0.2     EN          11-05-2006
 Added BNL_stop_tcpip_listner                               0.3     JM          02-01-2007
 Updated for usage in C++ env                               0.4     JM          17-05-2010
 
*/


#ifndef BNL_TCPIP
#define BNL_TCPIP

#include <stdio.h> 
#include <basetyps.h>

#ifdef WIN32
#include <windows.h>
#include <Wsock.h>
#include <BNL_tceng_lib.h>
#endif

/*module errors*/
#define BNL_WSA_INI_FAIL      1000
#define BNL_SOCKET_FAIL       1001
#define BNL_CLOSE_SOCKET_FAIL 1002
#define BNL_BIND_FAIL         1003
#define BNL_LISTEN_FAIL       1004
#define BNL_ACCEPT_FAIL       1005
#define BNL_CONNECTION_BROKEN 1006

#define BNL_PROTOCOL_RES_FAIL 1007
#define BNL_THREAD_ERROR      1008
#define BNL_WRITE_FAIL        1009
#define BNL_READ_FAIL         1010


EXTERN_C int DllExport BNL_tcpip_init
(
  FILE *pLog
);

EXTERN_C int DllExport BNL_tcpip_exit();

EXTERN_C int DllExport BNL_tcpip_set_log
(
  FILE *pLog
);

EXTERN_C char DllExport *BNL_ask_tcpip_version();


EXTERN_C int DllExport BNL_start_tcpip_listner
(
  int iPort, int iMaxCon
);

EXTERN_C int DllExport BNL_stop_tcpip_listner();

EXTERN_C int DllExport BNL_tcpip_client_listner
(
  int iMaxConnections, int iPort
);

EXTERN_C void DllExport BNL_tcpip_session
(
  PTHREADPACK ptp
);

EXTERN_C int DllExport BNL_tcpip_recv
( PTHREADPACK ptp,
  int iLen,
  char **pszRecieveMsg
);


EXTERN_C int DllExport BNL_handle_tcpip_message
(
  char *pszBuffer,
  int *iKeepAlive,
  int *iMessLen,
  char *pszMessage
);


/*need always to be set otherwhise a error will occur*/
EXTERN_C int DllExport BNL_tcpip_set_parse_function(void pf(PTHREADPACK,char*,int*,int*,char**));

EXTERN_C int DllExport BNL_tcpip_send
(
  PTHREADPACK ptp,
  int iMessLen,
  char *pMessage
);


/*************************************client****************************************************/
int DllExport BNL_tcpip_client_connect
(
   char *pszServer, 
   int iPort,
   SOCKET  *sConnectionSocket
);

int DllExport BNL_tcpip_client_send
(
   SOCKET sSocket,
   int iMessSize,
   char *pszMessage
);

int DllExport BNL_tcpip_client_recv
(
   SOCKET sSocket, 
   int iMessSize,
   char *pszMessage
);

int DllExport BNL_close_client_connnection
(
   SOCKET sSocket
);

#endif
