/**
 (C) copyright by UGS Benelux

 Developed for IMAN
 Versions   OS              platform
 -----------------------------------------------------------------------------------------------------------
 5.x          Intel
 7.x          Intel

 Description  :   This module header file belongs to the memory module .c file.
                  The header file should be added to the modules.h file if this module should be
                  added to the customization.


 Created for  :   Unigraphics Solutions BENELUX


 Comments     :   Every new procedure or function in this mdoules starts
                  with BNL_<module>_


 Filename     :   BNL_mm_module.h

 Version info   :   release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 reason /description                       Version By    Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                  0.0     EH    02-01-2001
 Added global BNL version definition file  0.1     LB    06-09-2001
 Added object types                        0.2     JM    21-01-2010
 Added send_csv_file,store_csv_file and
  send_plotrequest                         0.3     JM    21-01-2010
 Added BNL_csp_set_extracol                0.4     JM    15-02-2010
 Removed BNL_csp_set_extracol, 
  added BNL_csp_set_extracols              0.5     JM    17-02-2010
 Updated for usage in C++ env              0.6     JM    17-05-2010
 Added preferences                         0.7     JM    19-01-2010
 Updated for TC10.1                        0.8     JM    29-08-2013

*/


/*
  Memory module (MM)
*/

#ifndef BNL_CSP_H
#define BNL_CSP_H

#include <basetyps.h>
#ifdef  TC10_1
#include <tc/iman.h>
#else
#include <iman.h>
#endif
#include <BNL_version.h>
#include <BNL_ini_module.h>

#define BNL_DS_PLOTREQ_PREF     "BNL_DS_PLOTREQUEST"
#define BNL_DS_PLOTREQ_NR_PREF  "BNL_DS_PLOTREQUEST_NR"

#define PLOT_REQ_DS_TYPE    "PlotRequests"      /* Dataset type for plot requests */
#define NAMED_REF_NAME      "PlotRequest"       /* Named reference name */
#define CUT_AND_PASTE       2

#define CREATOR_CONFIG "config"

// From bnl_ups.h
enum
{
    FIXED_COL_TASKPUID,
    FIXED_COL_USER,
    FIXED_COL_GROUP,
    FIXED_COL_PARTNO,
    FIXED_COL_PARTREV,
    FIXED_COL_DATASET,
    FIXED_COL_DATASETTYPE,
    FIXED_COL_NAMEDREF,
    FIXED_COL_SHEETNAME,
    FIXED_COL_FORMAT,
    FIXED_COL_DEVICE
};

#define CSV_EXT                     "csp"

#define FIXED_COL_NAMES "\"Task_puid\",\"User\",\"Group\",\"Part_no\",\"Part_rev\",\"Dataset\",\"Dataset_Type\",\"NamedRef\",\"Sheet_name\",\"Format\",\"Device\""
#define FIXEDCOL_REVRULE "revrule"
#define FIXEDCOL_LEVEL   "level"
#define FIXEDCOL_EXTRACOL "extra_col"

#define CSV_QUOTE_STR	"\""

#define UPM_INI_SEC_GENERAL         "General"
#define UPM_INI_SEC_DEVICES         "Devices"
#define UPM_INI_SEC_EXTRA_ATTRS     "Extra Attributes"
#define UPM_INI_SEC_STORAGECLASS    "Storageclass"
#define UPM_INI_SEC_REQUEST         "PlotRequest"

#define UPM_INI_ITM_DSNAME          "dsname"
#define UPM_INI_ITM_QUOTED          "quoted"
#define UPM_INI_ITM_DEF_DEVICE      "default_device"
#define UPM_INI_ITM_QUOTED          "quoted"
#define UPM_INI_ITM_DRAWINGNAMES    "drawingnames"
#define UPM_INI_ITM_DRAWINGSIZES    "drawingsizes"
#define UPM_INI_ITM_FOLDER          "targetfolder"
#define UPM_INI_ITM_DELIMITER       "delimiter"

#define BNL_ATTR_DRWS               "BNL_drw_names"
#define BNL_ATTR_SIZES              "BNL_drw_sizes"

typedef struct s_UpmCsv
{
  char      *pszINIfile;
  char      *pszUser;
  char      *pszGroup;
  char      *pszDevice;
  char      **pszNamedRef;
  char      **pszDStypes;
  char      **pszObjectsTypes;
  char      **pszSheetNames;
  char      *pszNoteType;
  char      *pszNote;
  char      *pszCspOutPutDir;
  inivaluepair_t  **ppsExtraAttrs;
  int       iObjectTypes;
  int       iNamedRefCount;
  int       iDStypesCount;
  int       iExtraAttrsCount;
  int       iSheetNamesCount;
  char      szDrawingNamesAttr[64];
  char      szSizesAttr[64];
  char      szListName[64];
  char      szBomview[32];
  char      szRevisionRule[32];
  char      szLoadRevRule[32];
  int       iBomLevel;
  int       iExcludeNote;
  tag_t     tItem;
  tag_t     tItemRev;
  tag_t     tDataset;
  tag_t     tNamedRef;
  char      *pszRevRule;
  tag_t     tUser;
} t_UpmCsv;


EXTERN_C char * BNL_csp_ask_version
(
);

EXTERN_C int BNL_csp_initialise
(
  FILE *pLog,
  char *pszIni
);

EXTERN_C int BNL_csp_exit
(
);

int BNL_csp_set_log
(
  FILE *pLog
);


EXTERN_C  int replace_var
(

  char    *pszVar,
  char    **pszValue
);


EXTERN_C void replace_all_substrings
(
  char    **pszString,
  char    *pszItemID,
  char    *pszItemRevID,
  char    *pszDSname,
  char    *pszDStype
);

EXTERN_C  int  print_csv_header
(

);

EXTERN_C  int replace_var_list
(

  char    *pszVarList,
  char    **pszValue
);

EXTERN_C int get_form_field_value
(
  tag_t   tForm,
  char  *pszFormFieldName,
  char  **pszValue
);

EXTERN_C int process_target
(
  tag_t     tObject
);

static int process_object
(
  tag_t tObject
);

int  BNL_create_csp_file
(
  tag_t   tTarget
);

void BNL_csp_set_dstypes(int iDsCount,char **pszDsTypes);

void BNL_csp_set_objecttypes(int iTypesCount,char **pszObjectTypes);

void BNL_csp_set_nrs(int iNrCount,char **pszNrs);

void BNL_csp_set_device(char *pszDevice);

void BNL_csp_set_recursive(int iRecursive);

void BNL_csp_set_sheetNames(int iNrSht,char **pszSheetNames);

void BNL_csp_set_bomviewtype(char *pszBomType);

void BNL_csp_set_revrule(char *pszRevRule);

void BNL_csp_set_leveldepth(int iLevel);

void BNL_csp_set_excludenote(char *pszNoteType, char *pszNoteValue);

void BNL_csp_set_loadrevrule(char *pszLoadRevRule);

void BNL_csp_set_csp_out_dir(char *pszDir);

char *BNL_csp_get_csp_file_name();

int send_csv_file(char *pszScanDir);

int store_csv_file();

int send_plotrequest
(
  tag_t tJob,
  int iMode,
  char *pszScanFolder
);

/**
int BNL_csp_set_ini
(
  char *pszIni
)

  Description:

  Set the ini file.

  
  Returns:
  
  0 on Ok.


*/
int BNL_csp_set_ini
(
  char *pszIni
);

void BNL_csp_set_extracols(int iValue, char **pszValues);

#endif
