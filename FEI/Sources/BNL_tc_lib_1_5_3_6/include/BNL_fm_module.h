/**
 (C) copyright by UGS Benelux

 OS                          platform
 -----------------------------------------------------------------------------------------------------------
 WINNT						 Intel 	
 Solaris                     Unix

 Description	:   This module header file belongs to the file module .c file.
                    The header file should be added to the modules.h file if this module should be 
                    added to the customization.

 Created for	:   UGS BENELUX


 Comments		:   Every new procedure or function in this mdoules starts 
                    with BNL_<module>_
				    

 Filename		:	BNL_fm_module.h

 Version info   :   release.pointrelease (versions of the modules are stored in the module itself.
					
 History of changes											
 reason /description										                    Version release   date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                   0.0     EH        02-01-2001
 Added global BNL version definition file                   0.1     LB        06-09-2001
 Added function BNL_fm_replace_illegal_chars                0.2     LB        18-03-2002
 Added function BNL_fm_find_file                            0.3     EH        02-08-2002
 Make it BNL_lib_dll ready                                  0.4     EN        11-05-2006
 Updated for usage in C++ env                               0.5     JM        17-05-2010

*/


/*
  File Module (FM)
  BNL_file.c 
*/

#ifndef BNL_FM_H
#define BNL_FM_H

#include <stdlib.h>
#include <stdio.h>
#include <BNL_tceng_lib.h>
#include <basetyps.h>

#ifdef WIN32
#include <windows.h>
#include <time.h>
#include <direct.h>

#define DELETEFILE "del"
#define COPYFILE "copy"
#define DIR      "dir"

#define FILESEP "\\"
#else
#include <dirent.h>
#define FILESEP "/"
#define MAX_PATH 1024
#define DELETEFILE "rm"
#define COPYFILE "cp"

#endif

/**
char * BNL_fm_ask_version()

  Description: 

  Returns the version of he module.

  parameters: none
*/
EXTERN_C char DllExport *BNL_fm_ask_version( void);

/**
int BNL_fm_set_log
(
    FILE *fp
)

  Description:

  Sets the loging file pointer, if not set then 
  it will point to stderr.

  Returns:

  0 on Ok.


*/
EXTERN_C int DllExport BNL_fm_set_log
(
    FILE *fp
);

/**
int BNL_fm_open_file
(
    FILE **fp,               <O> 
    char *pszDirectory,      <I>
    char *pszFileName,       <I>
    char *pszAcces,          <I> 
    int  lLogfile            <I>
)

  Description:

  Function opens a file that can be a text file or binary file depending on the given parameters.
  If the parameter pszFileName is NULL then the function assume that the full path and filename is
  given in pszDirecotry. If pszDirecotry and pszFileName are filled in then the function composes the
  file name and directory together.
  
  The pszAcces determines the file type and access like r+t, w+t etc.
  If the lLogfile parameter is set to 1 then a unbufferd file will be created for log puposes. 

*/
EXTERN_C int DllExport BNL_fm_open_file
(
 FILE **fp, 
 char *pszDirectory, 
 char *pszFileName, 
 char *pszAcces, 
 int lLogfile
);

/**
int BNL_fm_close_file
(
    FILE *fp
)

  Description:

  This function closes the file which is connected with the file pointer.
*/
EXTERN_C int DllExport BNL_fm_close_file
(
 FILE *fp
);

/**
int BNL_fm_delete_file
(
)

Description:

  Deletes the given file from the filesystem. The function
  is created operatingsystem independable.

*/
EXTERN_C int DllExport BNL_fm_delete_file
(
    char *pszDir, 
    char *pszFile
);

/**
int BNL_fm_move_file
(
    char *pszSourceDir, 
    char *pszSourceFile,
    char *pszTargetDir,
    char *pszTargetFile

)

  Description:

  This function moves the file from one to another if filepaths
  give then the filepath should exist. Also direcotries can be moved
  fill in for the file pointer a NULL.

  If only renaming the file then fill in for the directories a NULL.
  
  Created platform indepenendaple

*/
EXTERN_C int DllExport BNL_fm_move_file
(
    char *pszSourceDir, 
    char *pszSourceFile,
    char *pszTargetDir,
    char *pszTargetFile
);

/**
int BNL_fm_compose_file_name
(
    char *pszDirectory,        <I>
    char *pszFileName,         <I> 
    char *pszExt,              <I> 
    char *pszNewFileName       <OF>    
) 

  Description:

  Composes a filename with a timestamp. If all entrys are NULL then it will 
  give a timestamp back. It is also possible to let one of the entries NULL.

  Returns:

  ITK_ok.

*/
EXTERN_C int DllExport BNL_fm_compose_file_name
(
    char *pszDirectory, 
    char *pszFileName, 
    char *pszExt, 
    char **pszNewFileName
);

/**
int BNL_fm_remove_directory
(
    char *pszDirectory <I>
)

  Description:

  Removes the direcotry system independaple.

  Returns:

  0 or 1 on error.
*/

EXTERN_C int DllExport BNL_fm_remove_directory
(
    char *pszDirectory
);

/**
int BNL_fm_create_directory
(
    char *pszDirectory <I>
)

  Description:

  Creates the given direcotry system independable

  Returns:

  0 or 1 on error.
*/
EXTERN_C int DllExport BNL_fm_create_directory
(
    char *pszDirectory
);

/**
int BNL_fm_check_file_or_dir
(
    char *pszFileOrDir <I>
)


  Description:

  Checks if a directory or file exists

  Returns:

  0 on non existing and 1 on exists.

*/
EXTERN_C int DllExport BNL_fm_check_file_or_dir
(
    char *pszFileOrDir
);

/**
int BNL_fm_initialise
(
  FILE *pLog,
  char *pszIni
)

  Description:

  Initialises the module and sets the file pointer.
  If file pointer NULL it will be set to stderr.

*/
EXTERN_C int DllExport BNL_fm_initialise
(
  FILE *pLog
);

/**
int BNL_fm_exit
(
)

  Description:

  Exit the module in save way after calling this function
  and reuse this module it needs to be initialised again

  Returns:

  0 on Ok
*/
EXTERN_C int DllExport BNL_fm_exit( void);

/**
int BNL_fm_parse_path
(
    char *pszPath,      <I>
    char **pszPPath,    <OF>
    char **pszName,     <O>    
    char **pszExt       <O>
)

  Description:

  Parse a fullpathname into three components:
  the path, the name and the extensions.
  Based on the seperator . and / or \

  if one of the components is not found then the component will be NULL.

  Returns

  0 On ok.

  
*/
EXTERN_C int DllExport BNL_fm_parse_path
(
    char *pszPath, 
    char **pszPPath,
    char **pszName, 
    char **pszExt
);

/**
int BNL_fm_replace_env_for_path
(
    char *pszPath       <I>
    char **pszReplace   <OF>
)

  Description:

  This function will search for a environment setting
  in path like %iman_root% or $iman_root this part will be replaced by
  the real environment getted with getenv.

  If no path found then the returned value will be NULL.

  The returned path should be freed with free(). For use with
  ITK use the BNL_tb_replace_env_for_path.

  Returns

  0 on Ok
*/
EXTERN_C int DllExport BNL_fm_replace_env_for_path
(
    char *pszPath,
    char **pszReplace
);

/**
int BNL_fm_replace_illegal_chars
(
    char *pszName
)

  Description:

    Replace all illegal filename characters in pszName by '_'.
    Illegal chars are: \ , / : * ? " < > |
    This function can only be used for file names without path and extension because of : and \ or / and .
    

  Returns:

    0

*/
EXTERN_C int DllExport BNL_fm_replace_illegal_chars
(
    char *pszName
);

/**
int BNL_fm_delete_directory
(
    char    *pszDirName,
    int     iDelDir
)

  Description:

    Deletes all the files from pszDirName.
    If iDelDir == true, the directory itself will be deleted too.

  Returns:

    0 if successfull, else Retcode

*/
EXTERN_C int DllExport BNL_fm_delete_directory
(
    char    *pszDirName,
    int     iDelDir
);

/**
int BNL_fm_find_file
(
    char    *pszFileName, <I>
    int     *iFound,      <O>
    char    *pszFullname  <OF>
)

  Description:

    
      Find file in given directory with the given file name
      The given file name could be:
       
       Ex

      then it will find a file with

       Executable.exe

      this file name will be returned.

  Returns:

    0 if successfull, else Retcode

*/
EXTERN_C int DllExport BNL_fm_find_file
(
    char    *pszDir,
    char    *pszFileName,
    char    **pszFullname
   
);

/**
int BNL_fm_readfileline
(
  FILE *fp, 
  char **pszFileLine
)

  Description:

  Read exactly one file line in a text file.
  The line should be ended by a new line char or end of file char
  to recognize thats the end of the line.

  Please note:
  In case of an UNIX file, make sure the file was opened in binary mode.
  Else there's a problem with the filepointer and the function will not work properly.

  Returns:

  0 on ok.
  
*/
EXTERN_C int DllExport BNL_fm_readfileline
(
  FILE *fp, 
  char **pszFileLine
);

/**
FILE *BNL_fm_temp_file
(
  char** pszFileLoc   <OF>
)

Opens a tempfile, after opening file close file and delete

*/
FILE DllExport *BNL_fm_temp_file
(
  char** pszFileLoc
);

/**
int BNL_fm_read_dir_content
(
   char *pszDirectory,     <I>
   int *iContent,          <O>
   char ***pszDirContent   <OF>
)

Description:

Read the directory content into an string array
*/
int DllExport BNL_fm_read_dir_content
(
   char *pszDirectory,
   int *iContent,
   char ***pszDirContent
);

/**
int BNL_fm_copy_file
(
   char *pszPath,       <I>
   char *pszFile,       <I>
   char *pszTargetPath, <I>
   char *pszTargetFile  <I>
)

Description:

Copies a file from source to target. Supports wildchars
Example:
BNL_fm_copy_file(pszResourcesDir,"*.css",pszExportDir,NULL);

*/
int DllExport BNL_fm_copy_file
(
   char *pszPath,
   char *pszFile,
   char *pszTargetPath,
   char *pszTargetFile
);


#endif
