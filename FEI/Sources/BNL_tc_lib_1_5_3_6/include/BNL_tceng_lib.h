/**
 (C) copyright by UGS Benelux

 
 Description    :   Header file. Belongs to the bnl_library.

 Created for    :   UGS BENELUX

 Comments       :   

 Filename       :   BNL_tceng_lib.c

 Version info   :   release.pointrelease

 History of changes
 Reason / Description                                       Version By          date
 -------------------------------------------------------------------------------------------
 Creation                                                   0.0     EN          10-05-2006

 */

#if defined(_WIN32)
#define DllExport __declspec(dllexport)
#else
#define DllExport
#endif

/*temp define we should read out the version from the resource file, since we this is used in a dll
looks like its not possible to read the version of the dll itself unfortunatly*/
#define BNL_LIB_VERSION "BNL_LIB 1.5.3.6"
