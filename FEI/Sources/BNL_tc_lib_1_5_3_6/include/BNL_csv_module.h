/**
 (C) copyright by UGS Benelux 2001

 Description    :   This header file belongs to the BNL_csv_module.c

 Created for    :   UGS BENELUX

 Comments       :   Every new procedure or function starts with BNL_csv_
				    
 Filename       :   BNL_csv_module.h

 Version info   :   release.pointrelease
                    
					
 History of changes											
 reason /description										By      Version     Date
 ----------------------------------------------------------------------------------------------------------
 Updated from UPM module to a general module                EN      0.0         23-03-2005
 Add BNL_parse_csv_line_into_array function                 JM      0.1         23-03-2006
 Make it BNL_lib_dll ready                                  EN      0.2         11-05-2006
 Updated for usage in C++ env                               JM      0.3         17-05-2010


 !! if change version then change version in BNL_module variable
*/
#ifndef BNL_CSV_H
#define BNL_CSV_H

#include<stdio.h>
#include <basetyps.h>
#include <BNL_tceng_lib.h>

typedef struct bnl_csv_ini_s
{
    char *pszINIfile;
    char *pszDevicesDir;
    char cDelimiter;
} bnl_csv_ini_t;


typedef struct bnl_csv_content_s
{
  char *pszFile;
  char cDelimiter;
  int  iColumns;
  int  iRows;
  char **pszHeader;
  char ***pszRows;
} bnl_csv_content_t;


EXTERN_C int DllExport BNL_csv_initialise
(
  FILE *pLog,
  char *pszIni
);

EXTERN_C int DllExport BNL_csv_set_devices_dir
(
    char *pszDevicesDir
);

EXTERN_C int DllExport BNL_csv_exit
(
);

EXTERN_C int DllExport BNL_csv_set_log
(
   FILE *fp
);

/*
EXTERN_C int BNL_csv_convert
(
    char            *pszCSVfile,
    t_XMLElement    **pXMLroot
);
*/

EXTERN_C int DllExport  BNL_csv_free_ini
(
);

EXTERN_C int DllExport  BNL_csv_report_ini
(
);

EXTERN_C char DllExport  *BNL_csv_ask_version
();

/*
int BNL_read_csv_file
(
  char *pszFile,            <I>
  char cSep,char			<I>
  cQuote,					<I>
  bnl_csv_content_t *pCsv	<OF using special csv free function>
)

  Description:

  Returns a structure with arrays containing the header and values of the file. 
  if lines do match the reuirements then they will be read in, if there are some
  which do not met the requirement then they will be skipped.

*/
int DllExport  BNL_read_csv_file
(
  char *pszFile, 
  char cSep,
  char cQuote, 
  bnl_csv_content_t **pCsv
);


/*
int BNL_csv_free_csvblock
(
   bnl_csv_content_t *pCsv
)

  description:

  free the allocated memory of the read in csv file.
*/
int DllExport  BNL_csv_free_csvblock
(
   bnl_csv_content_t *pCsv
);


/**
int BNL_csv_get_cell_value
(
   int iLine,           <I>
   int iColumn,         <I>
   char *pszColumnName  <I>
   char **pszValue      <OF>
)

  Description:

  Get the cell value in the csv file.
  if *pszColumnName is not NULL then it will use the columname to deterimine
  the column. If the name not found the NULL is returned.

*/
int DllExport  BNL_csv_get_cell_value
(
   bnl_csv_content_t *pCsv,
   int iRow, 
   int iColumn,
   char *pszColumnName,
   char **pszValue
);

/*
int BNL_parse_csv_line_into_array
(
  pszLine,
  cSep,
  cQuote,
  &iCols,
  &pszArray
)

  Description:

  Will parse the csv line into an array.
  
  if cQoute is '\0' then it assumed as non qouted line.
*/
int DllExport  BNL_parse_csv_line_into_array
( char *pszLine,
  char cSep,
  char cQuote,
  int  *iCols,
  char ***pszArray
);


#endif






