/**
 (C) copyright by UGS Benelux

 Developed for IMAN 
 Versions     OS                          platform
 -----------------------------------------------------------------------------------------------------------
 5.x									  Intel
 7.x									  Intel 	
 
 Description	:   This module header file belongs to the error module .c file.
                  The header file should be added to the modules.h file if this module should be 
                  added to the customization.

 Created for	:   Unigraphics Solutions BENELUX

 Comments		  :   Every new procedure or function in this mdoules starts with BNL_<module>_
				    
 Filename		  :   BNL_em_module.h

 Version info :   release.pointrelease (versions of the modules are stored in the module itself.
					
 History of changes											
 Reason /description					  					                  Version By        Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                   0.0     EN        02-01-2001
 Added global BNL version definition file                   0.1     LB        06-09-2001
 changed function parameters added function                 0.2     EN  
 Added function BNL_em_get_mode + enum defs			            0.3     LB        25-07-2002
 because of new xml file handling the errorlist
 function are removed                                       0.4     EN        04-08-2005
 Make it BNL_lib_dll ready                                  0.5     EN        11-05-2006
 Updated for usage in C++ env                               0.6     JM        16-05-2010
 Updated for TC10.1                                         0.7     JM        29-08-2013
 Updated error in macro ITK_CALL2                           0.8     DTJ       22-01-2015
*/


/*
  Error Module (EM)
  BNL_error.c 
*/

#ifndef BNL_ERROR_H
#define BNL_ERROR_H

#define BNL_DEBUG_ENV_VAR "BNL_DEBUG_FLAG"

#define ITK_CALL(Function)\
  iRetCode=Function;\
  if (iRetCode != ITK_ok)\
  {\
    BNL_em_error_handler_2(#Function, __FILE__, EMH_severity_user_error, iRetCode, __LINE__);\
    goto CLEANUP;\
  }\

#define ITK_CALL2(Function)\
{\
  int i_retcode = Function;                                             \
  if (i_retcode != ITK_ok)\
  {\
    BNL_em_error_handler_2(#Function, __FILE__, EMH_severity_user_error, i_retcode, __LINE__);\
  }\
}\

#define ITK_ERROR\
  iRetCode=-1;\
  goto CLEANUP;\


#include <BNL_tceng_lib.h>
#include <BNL_mm_module.h>
#ifdef  TC10_1
#include <tc/emh.h>
#else
#include <emh.h>
#endif
#include <basetyps.h>

typedef enum
{
    UNKNOWN = 0, 
    VT100, 
    MOTIF, 
    WEB
} t_DispMode;



/*most definitions are done in the BNL_errors.h file */
EXTERN_C char DllExport *BNL_em_ask_version
();

/**
int BNL_em_set_log
(
    FILE *pLog  <I>
)

  Description:

  This function sets the log pointer to the given 
  logpointer. If not set then the internal logpointer points
  stderr.

  Returns:

  0 on Ok

*/
EXTERN_C int DllExport BNL_em_set_log
(
    FILE *pLog
);

/**
int BNL_em_error_handler
(
    char *pszErrorFunction, <I>
    char *pszModule,        <I>
    int iEMH_severity,      <I>
    int iErrorNumber,       <I>
    logical lDisplay        <I>
)



  Description:  
  
    This function displays a error message to the user or 
    displaying the reason of nogos for rule handlers. If in motif
    mode the window will displayed otherwise a messages in the display 
    will appear.

  Returns:  
  
    ITK_ok or !ITK_ok
*/
EXTERN_C int DllExport BNL_em_error_handler
(
 char *pszErrorFunction,
 char *pszModule,
 int iEMH_severity, 
 int iErrorNumber, 
 logical lDisplay
);

/**
int BNL_em_error_handler_s1
(   
    char *pszErrorFunction,         <I>
    char *pszModule,                <I>
    int iEMH_severity,              <I>
    int iErrorNumber,               <I>
    logical lDisplay,               <I>
    const char *pszAddString1       <I>
)



  Description:  
  
    This function displays a error message to the user or 
    displaying the reason of nogos for rule handlers. If in motif
    mode the window will displayed otherwise a messages in the display 
    will appear.
    The given string will be also added into the displayed message.

  Returns:  
  
    ITK_ok or !ITK_ok
*/
EXTERN_C int DllExport BNL_em_error_handler_s1
(
    char *pszErrorFunction, 
    char *pszModule,
    int iEMH_severity, 
    int iErrorNumber, 
    logical lDisplay, 
    const char *pszAddString1
);

/**
int BNL_em_error_handler_s2
(
    char *pszErrorFunction,         <I>
    char *pszModule,                <I>
    int iEMH_severity,              <I>
    int iErrorNumber,               <I>
    logical lDisplay,               <I>
    const char *pszAddString1,      <I>
    const char *pszAddString2       <I>
)



  Description:  
  
    This function displays a error message to the user or 
    displaying the reason of nogos for rule handlers. If in motif
    mode the window will displayed otherwise a messages in the display 
    will appear.
    The given string will be also added into the displayed message.

  Returns:  
  
    ITK_ok or !ITK_ok
*/
EXTERN_C int DllExport BNL_em_error_handler_s2
(
    char *pszErrorFunction, 
    char *pszModule,
    int iEMH_severity, 
    int iErrorNumber, 
    logical lDisplay, 
    const char *pszAddString1, 
    const char *pszAddString2
);

/**
int BNL_em_error_handler_s3
(
    char *pszErrorFunction,         <I>
    char *pszModule,                <I>
    int iEMH_severity,              <I>
    int iErrorNumber,               <I>
    logical lDisplay,               <I>
    const char *pszAddString1,      <I>
    const char *pszAddString2,      <I>
    const char *pszAddString3       <I>
)



  Description:  
  
    This function displays a error message to the user or 
    displaying the reason of nogos for rule handlers. If in motif
    mode the window will displayed otherwise a messages in the display 
    will appear.
    The given string will be also added into the displayed message.

  Returns:  
  
    ITK_ok or !ITK_ok
*/
EXTERN_C int DllExport BNL_em_error_handler_s3
(
    char *pszErrorFunction, 
    char *pszModule,
    int iEMH_severity, 
    int iErrorNumber, 
    logical lDisplay, 
    const char *pszAddString1, 
    const char *pszAddString2, 
    const char *pszAddString3
);

/**
int BNL_em_error_handler_s4
(
    char *pszErrorFunction,         <I>
    char *pszModule,                <I>
    int iEMH_severity,              <I>
    int iErrorNumber,               <I>
    logical lDisplay,               <I>
    const char *pszAddString1,      <I>
    const char *pszAddString2,      <I>
    const char *pszAddString3,      <I>
    const char *pszAddString4       <I>
)



  Description:  
  
    This function displays a error message to the user or 
    displaying the reason of nogos for rule handlers. If in motif
    mode the window will displayed otherwise a messages in the display 
    will appear.
    The given string will be also added into the displayed message.

  Returns:  
  
    ITK_ok or !ITK_ok
*/
EXTERN_C int DllExport BNL_em_error_handler_s4
(
    char *pszErrorFunction, 
    char *pszModule,
    int iEMH_severity, 
    int iErrorNumber, 
    logical lDisplay, 
    const char *pszAddString1, 
    const char *pszAddString2, 
    const char *pszAddString3, 
    const char *pszAddString4
);

/**
int BNL_em_error_handler_s5
(
    char *pszErrorFunction,         <I>
    char *pszModule,                <I>
    int iEMH_severity,              <I>
    int iErrorNumber,               <I>
    logical lDisplay,               <I>
    const char *pszAddString1,      <I>
    const char *pszAddString2,      <I>
    const char *pszAddString3,      <I> 
    const char *pszAddString4,      <I> 
    const char *pszAddString5       <I>
)


  Description:  

  Errors numbers can comming from modules and TcEng.
  warning numbers can come from modules and TcEng.
  information can be messages needed to display.

  Error handling, 

  to have customization possibilitys on errors we to write 
  a function thats simple to use and can be used in all cases.

  BNL convention is that error messages should give always
  the following info:

  modulename_version: message                    
  modulename_version: Error in functionname, message -> used on unexpected errors
  modulename_version: message variable -> used on warnings like if rule handler sends nogo

    
  The called function has a parameter severity,which can be
    
      1. information
      2. warning
      3. error

    !!!Philosophy
    Warning and information messages are just messages displayed to the user or into a logfile after 
    that the program goes on.
    Error message are message displayed to the user or displayed into a logfile, but after this
    message handling the program will be stopped or exited.
    
    This function clears in all cases the internal TcEng error stack.

    Returns:    
  
    ITK_ok or !ITK_ok if the severity is an error then !ITK_ok will be generated.

*/
EXTERN_C int DllExport BNL_em_error_handler_s5
(
    char *pszErrorFunction, 
    char *pszModule,
    int iEMH_severity, 
    int iErrorNumber, 
    logical lDisplay, 
    const char *pszAddString1, 
    const char *pszAddString2, 
    const char *pszAddString3, 
    const char *pszAddString4, 
    const char *pszAddString5
);


/**
int BNL_em_initialize
(
    FILE *fpLog <I>
    
)

  Description:

  Function to initialize the error module and 
  sets the logfile pointer.

  Returns:

  ITK_ok

*/
int DllExport BNL_em_initialise
(
    FILE *pLog
);

/**
int BNL_em_exit
(
)

  Description:

  Exit the module in save way after calling this function
  and reuse this module it needs to be initialised again

  Returns:

  0 on Ok
*/
int DllExport BNL_em_exit
(
);

/**
int BNL_em_complete_errormessage
(
 int iSeverity,        <I>
 char *pszNumber,      <I>
 char *pszMessage,     <I>
 const char *pszAddString1,  <I>
 const char *pszAddString2,  <I>
 const char *pszAddString3,  <I>
 const char *pszAddString4,  <I>
 const char *pszAddString5,  <I>     
 char **pszErrorString <OF>
)

  Description:

  Function to get an standard BNL error message
  like:

  "module: severity_number message "
  
  Typical example of a errormessageline will be coverted to:
  
    "919304 The Item or revision ID %2$ is not the right NC number"
    "Warning_919304: The Item or revision ID X123 is not the right NC number"

  Where the message can contain variables which are filled 
  in by the pszaddstring variables, in the example pszAddstring1 was X123 and the severity
  was warning.

  Suported severity's:

  Error, Warning and Information.

  Returns:
  
    ITK_ok
*/
EXTERN_C int DllExport BNL_em_complete_errormessage
(
 int  iSeverity,
 char *pszNumber,
 char *pszMessage,
 const char *pszAddString1,
 const char *pszAddString2,
 const char *pszAddString3, 
 const char *pszAddString4,
 const char *pszAddString5,
 char **pszErrorString
);


/**
int BNL_em_get_mode( void)

  Description:
    
    Determines the mode of the process

  Returns:

    one of VT100, MOTIF or WEB if mode could be determined
    UNKNOWN if mode could NOT be determined

*/
EXTERN_C int DllExport BNL_em_get_mode( void);

/**
int BNL_em_get_error_string
(
    int iErrorNum, 
    char **pszErrorString
)


  Description:

  Returns the error string of which belongs to the given number
  function should be used bu modules outside this module to get acces
  to the message.

  returns

  0 on Ok.

*/

int DllExport BNL_em_get_error_string
(
    int iErrorNum, 
    char **pszErrorString
);

/*
int BNL_em_error_handler_list_messages
( 
   char        *pszErrorFunction,   <I>
   char        *pszModule,          <I>
   int         iEMH_severity,       <I>
   int         iErrorNumber,        <I>
   logical     lDisplay,            <I>
   int         iCount,              <I>
   char        **pszMessages        <I>
)

  Description:

  A function to put a list on the errorstack. The list should
  be given as array.

*/
int DllExport BNL_em_error_handler_list_messages
( 
   char        *pszErrorFunction,   
   char        *pszModule,          
   int         iEMH_severity, 
   logical     lDisplay, 
   int         iCount,
   char        **pszMessages
);

/*******************************************************************************
 * int DllExport BNL_debug_set_debug:
 * Return the debug flag based on preference setting variable BNL_DEBUG_ENV_VAR
 *
 * @param[I]  pszModName:       The name of the method, or All
 *
 * @retval 1 if debug for the handler is set, else 0.
 *
 * History of changes
 * Date        Author           Description
 * ----------- ---------------- ------------------------------------------------
 * 07-jun-2012 D. Tjon Kon Joe  Created
 ******************************************************************************/
int DllExport BNL_em_debug_set_debug(
    char        *pszModName     // <I>
);

/*******************************************************************************
 * int DllExport BNL_em_error_handler_2
 *
 * This function displays a error message to the user or 
 * displaying the reason of nogos for rule handlers. If in motify
 * mode the window will displayed otherwise a messages in the display 
 * will appear.

 * The called function has a parameter severity,which can be
 *   
 *     1. information
 *     2. warning
 *     3. error

 *   !!!Philosophy
 *   Warning and information messages are just messages displayed to the user or into a logfile after 
 *   that the program goes on.
 *   Error message are message displayed to the user or displayed into a logfile, but after this
 *   message handling the program will be stopped or exited.
 *
 * Returns:
 *
 *  ITK_ok or !ITK_ok if the severity is an error then !ITK_ok will be generated.
 *
 *
 * @param[I]  pszFunction:      Full method e.g PREF_ask_char_value(pszVals[1], 0, &pszTempValue)
 * @param[I]  pszModName:       Current filename of the method
 * @param[I]  iEMH_severity:    EMH_severity_error, EMH_severity_warning, or EMH_severity_information
 * @param[I]  iErrorNumber:     Code of the error
 * @param[I]  iLineNumber:      linenumber in the filename
 *
 * @retval ITK_ok on successful execution of function, Else ITK Error code.
 *
 * History of changes
 * Date        Author           Description
 * ----------- ---------------- ------------------------------------------------
 * 07-jun-2012 D. Tjon Kon Joe  Created
 ******************************************************************************/
int DllExport BNL_em_error_handler_2
(
    char        *pszFunction,   //<I>: Full method e.g PREF_ask_char_value(pszVals[1], 0, &pszTempValue)
    char        *pszModule,     //<I>: Current filename
    int         iEMH_severity,
    int         iErrorNumber,
    int         iLineNumber  //<I>: Current linenumber
);

#endif
