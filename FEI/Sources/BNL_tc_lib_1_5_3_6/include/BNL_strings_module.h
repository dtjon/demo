/**
 (C) copyright by UGS Benelux 2005

 Description    :   This header file belongs to the BNL_strings_module.c

 Created for    :   UGS BENELUX

 Comments       :   Every new procedure or function starts with BNL_strings_
				    
 Filename       :   BNL_strings_module.h

 Version info   :   release.pointrelease
                    
					
 History of changes											
 reason /description										                    Version     By          Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                   0.0         EN          16-03-2005
 Make it BNL_lib_dll ready                                  0.1         EN          11-05-2006
 Added function BNL_strings_trim_decimal                    0.2         JM          12-02-2008
 Added fucntion BNL_strings_replace_string                  0.3         JM          27-10-2008
 Updated for usage in C++ env                               0.4         JM          17-05-2010
 
 !! if change version then change version in BNL_module variable
*/

#ifndef BNL_STRINGS_H
#define BNL_STRINGS_H

#include <stdio.h>
#include <basetyps.h>
#include <stdlib.h>
#include <BNL_tceng_lib.h>


/**
int BNL_string_initialise
(
  FILE *pLog,       <I>
  char *pszIni      <I>
)

  Description:

  Initializes the module, ini is for future use and can be NULL.

  Returns:

  0 on Ok.

*/
EXTERN_C int DllExport BNL_strings_initialise
(
  FILE *pLog,
  char *pszIni
);


/**
int BNL_strings_exit
(
)

  Description:

  Exits the module after calling this function
  the initialize function of this module need to
  be called.

  Returns:

  0 on Ok.

*/
EXTERN_C int DllExport BNL_strings_exit
(
);


/**
int BNL_strings_set_log
(
    FILE *fp
)

  Description:

  This functions sets the internal file pointer to 
  the given file pointer. Filepointer is standard initialized to
  stderr.

  Returns:

  0 on Ok.

*/
EXTERN_C int DllExport BNL_strings_set_log
(
  FILE *fp
);


/**
char * BNL_strings_ask_version()

  Description: 

  Returns the version of he module.

  parameters: none
*/
EXTERN_C char DllExport *BNL_strings_ask_version
(
);


/*
char *BNL_strings_copy_string
(
  char *pszChars
)

  Description:

  Copies the given string into new memory block.
  The returned memory block should freed.
  If input string is NULL, NULL will be returned.

*/
EXTERN_C char DllExport *BNL_strings_copy_string
(
  char *pszChars
);


/*
int BNL_strings_trim_decimal
(
  char *pszValue
)

  Description:

  Remove trailing zeros from decimal number
  For example: 2.2200 would come back as 2.22

*/
int DllExport BNL_strings_trim_decimal
(
  char *pszValue
);


/**
char *BNL_strings_replace_string
(
  char *pszString    <I>
  char *pszOld       <I>
  char *pszNew       <I>
)

  Description:

  The functions replaces all occurences of the string pszOld with the string pszNew
  in the string pszString.
  
  Returns:

  The new string.

*/
char DllExport *BNL_strings_replace_string(char *pszString, char *pszOld, const char *pszNew);

/*******************************************************************************
 * int DllExport BNL_split_multiple_values:
 * Function to split string using the given separator
 *
 * @param[I]  pszValue:       the input string to split
 * @param[I]  chSeparator:    the separator character
 * @param[O]  iCount:         the number of tokens built/splitted
 * @param[OF] pszValues:      the output strings (must MEM_free)
 *
 * @retval ITK_ok on successful execution of function, Else ITK Error code.
 *
 * History of changes
 * Date        Author           Description
 * ----------- ---------------- ------------------------------------------------
 * 07-jun-2012 D. Tjon Kon Joe  Created
 ******************************************************************************/
int DllExport BNL_strings_split_multiple_values(char *pszValueOrg,char chSeparator,int  *iCount,char ***pszValues);

/*******************************************************************************
 * char DllExport* low_strssa:
 * Appends a substring to the end of a string.
 * String to append substring to, freed before return;
 *
 * @param[IOF]pszBuf:       String to append substring to, freed before return
 * @param[I]  chSeparator:    Substring to append
 * @param[O]  iCount:         the number of tokens built/splitted
 * @param[OF] pszValues:      the output strings (must SM_free)
 *
 * @retval the concatenated string
 *
 * USAGE:
 * char *pszItemId = NULL;
 * pszItemId = low_strssa(NULL, "init"); -> equal to strcpy
 * pszItemId = low_strssa(pszItemId, " string to append");
 * Result: "init string to append"
 *
 * History of changes
 * Date        Author           Description
 * ----------- ---------------- ------------------------------------------------
 * 07-jun-2012 D. Tjon Kon Joe  Created
 ******************************************************************************/
char DllExport* BNL_strings_low_strssa(
    char        *buf,           // IOF
    const char  *str            // I
);



#endif
