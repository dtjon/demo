/**
 (C) copyright by UGS Benelux

 Developed for IMAN 
 Versions     OS                          platform
 -----------------------------------------------------------------------------------------------------------
 5.x									  Intel
 7.x									  Intel
 
 Description	:   This header file contains definitions of error numbers and 
                    other static info used by the Error Module.

 Created for	:   Unigraphics Solutions BENELUX


 Comments		:   Every new procedure or function in this mdoules starts 
                    with BNL_<module>_
				    

 Filename		:	BNL_errors.h

 Version info   :   release.pointrelease (versions of the modules are stored in the module itself.
					
 History of changes											
 reason /description                                        Version By          Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                   0.0     EN          18-12-2000
 Added global BNL version definition file                   0.1     LB          06-09-2001
 added wrong content error                                  0.2     EN          14-03-2002
 Adapted // comment for SUN Solaris                         0.3     LB          30-05-2002
 Added messages BNL_did_not_found_ds and
                BNL_wrong_report_template                   0.4     EH          02-08-2002
 Added message BNL_AH_no_items_for_upm                      0.5     LB          05-09-2002
 removed daf stuff and promote this as general error files  0.6     EN          21-04-2004
 Make it BNL_lib_dll ready                                  3.2     EN          11-05-2006
*/

#ifndef _BNL_ERRORS_H
#define _BNL_ERRORS_H

#include <BNL_tceng_lib.h>

/*

  Error numbering is starting at
  
  EMH_USER_error_base
  
  BNL_error_base is the starting point of the error numbering 
  We can use the offset to avoid problems with collisions if some other 
  customisation will be integrated.

  The error offset is reintroduced, but is configurable available in the 
  .iman_env file. by setting BNL_error_handler_offset parameter.
  
  


*/

#define BNL_error_offset    0 //offset is made configurable in .iman_env
#define BNL_error_base      (EMH_USER_error_base + BNL_error_offset) //=919000 
#define BNL_AH_error_base   ( BNL_error_base + 100)
#define BNL_RH_error_base   ( BNL_AH_error_base + 100)
#define BNL_error_end       ( BNL_RH_error_base +100 )


/*
 messages errors or warnings which can be generated in every customization.

*/

/*
   General errors/warnings/info 919000-919099
*/

#define BNL_error_message                 ( BNL_error_base + 2)
#define BNL_message_argument_missing      ( BNL_error_base + 3)
#define BNL_argument_has_wrong_content    ( BNL_error_base + 4)
#define BNL_display_confirmation          ( BNL_error_base + 5)  


/*
    Action handler errors/warnings/info 919100-919199
*/
#define BNL_AH_mail_is_sent               (BNL_AH_error_base + 1) 
#define BNL_AH_more_folders_with_name     (BNL_AH_error_base + 2) 
//#define BNL_AH_item_revision_already_exists (BNL_AH_error_base + 3)
#define BNL_AH_confirmation_mess          (BNL_AH_error_base + 3) 
//#define BNL_did_not_found_ds              (BNL_AH_error_base + 5) 
//#define BNL_wrong_report_template         (BNL_AH_error_base + 6)   
//#define BNL_AH_no_items_for_upm           (BNL_AH_error_base + 7)

/*
     Rule handler errors/warnings/info 919200-919300
*/
#define BNL_RH_not_approver              ( BNL_RH_error_base + 1 )
#define BNL_RH_not_owner                 ( BNL_RH_error_base + 2 )
#define BNL_RH_not_right_nc              ( BNL_RH_error_base + 3 )
#define BNL_RH_is_not_right_initiator    ( BNL_RH_error_base + 4 )
#define BNL_RH_relations_have_not_right_status  ( BNL_RH_error_base + 5 )
#define BNL_RH_not_right_status_in_bom   ( BNL_RH_error_base + 6 )
#define BNL_RH_wrong_target_type         ( BNL_RH_error_base + 7 )
//#define BNL_RH_send_nogo_based_on        ( BNL_RH_error_base + 8 )
#define BNL_RH_status_of_item_nok          ( BNL_RH_error_base + 8 )
//#define BNL_RH_send_nogo_not_found       ( BNL_RH_error_base + 9 )
//#define BNL_RH_not_right_status          ( BNL_RH_error_base + 10 )
#define BNL_RH_wrong_target_class        ( BNL_RH_error_base + 9 )
#define BNL_RH_no_write_access           ( BNL_RH_error_base + 10 )
/*
#define BNL_RH_status_found              ( BNL_RH_error_base + 13 )
*/
#define BNL_RH_no_configured_occurences  ( BNL_RH_error_base + 11 )
#define BNL_RH_wso_has_not_right_status  ( BNL_RH_error_base + 12 )
#define BNL_RH_more_parents_found        ( BNL_RH_error_base + 13 )
#define BNL_RH_parent_status_list        ( BNL_RH_error_base + 14 )
#define BNL_RH_wrong_target_count        ( BNL_RH_error_base + 15 )
#define BNL_RH_relations_wrong_property_value  ( BNL_RH_error_base + 16 )
#define BNL_RH_drawing_missing           ( BNL_RH_error_base + 17 )
#define BNL_RH_drawing_outofdate         ( BNL_RH_error_base + 18 )
#define BNL_RH_targets_in_workflow       ( BNL_RH_error_base + 19 )

#endif /* _BNL_ERRORS_H */
