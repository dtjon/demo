/**
 (C) copyright by Unigraphics Solutions Benelux

 Developed for IMAN 
 Versions     OS                          platform
 -----------------------------------------------------------------------------------------------------------
 7.x          WINNT                       Intel 
    
 

 Description    :   This include file contains definitions and macros to trace the sequence of programs

 Created for    :   Unigraphics Solutions BENELUX


 Comments       :   
                    

 Filename       :   BNL_debug.h

 Version info   :   The number consist of the iman release and then the release of the handler
                    <handlerrelease>.<pointrelease>

                    
 History of changes                                         
 reason /description                                        Version By          date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                   0.0     LB          15-10-2001
 Make use of windows debug messaging system                 0.1     LB          20-02-2002
 Make switch for windoes and other OS                       0.2     LB          25-02-2002
 Added BNL_DEBUG_MSG                                        0.3     LB          16-04-2002
 Added heap check to ENTER and LEAVE macros with switch
    BNL_HEAP_CHECK                                          0.4     LB          12-09-2002
 Updated for usage in C++ env                               0.5     JM          17-05-2010

*/

#define BNL_DEBUG_XML

#ifndef BNL_DEBUG_H
#define BNL_DEBUG_H

#ifdef BNL_DEBUG

#include <stdio.h>
#include <basetyps.h>

#ifdef WIN32
#include <windows.h>

#ifdef BNL_HEAP_CHECK
EXTERN_C void BNL_debug_heap_check(char *pszcomment);
#else /* no heap check */
#define BNL_debug_heap_check( _szVar)
#endif

#ifdef BNL_DEBUG_XML

#define BNL_DEBUG_MSG( _szVar) \
{ \
    char  _szDbg[1024]; \
    sprintf( _szDbg, "<DBG msg=\"%s\"/>\n", _szVar); \
    OutputDebugString( (LPCTSTR) _szDbg); \
}

#define BNL_DEBUG_LOG( _szVar) \
{ \
    char  _szDbg[1024]; \
    sprintf( _szDbg, "<DBG var=\"%s\" value=\"%s\"/>\n", #_szVar, _szVar); \
    OutputDebugString( (LPCTSTR) _szDbg); \
}

#define BNL_ENTER( _module, _func) \
{ \
    char  _szDbg[256]; \
    sprintf( _szDbg, "<%s module=\"%s\">\n", _func, _module); \
    OutputDebugString( (LPCTSTR) _szDbg); \
    BNL_debug_heap_check( _func); \
}

#define BNL_LEAVE( _module, _func) \
{ \
    char  _szDbg[256]; \
    sprintf( _szDbg, "</%s>\n", _func); \
    OutputDebugString( (LPCTSTR) _szDbg); \
    BNL_debug_heap_check( _func); \
}


#else /* not BNL_DEBUG_XML */

#define BNL_DEBUG_MSG( _szVar) \
{ \
    char  _szDbg[1024]; \
    sprintf( _szDbg, ">DBG msg=\"%s\"\n", _szVar); \
    OutputDebugString( (LPCTSTR) _szDbg); \
}

#define BNL_DEBUG_LOG( _szVar) \
{ \
    char  _szDbg[1024]; \
    sprintf( _szDbg, "> %d [%s]=[%s]\n", _dbg_level++, #_szVar, _szVar); \
    OutputDebugString( (LPCTSTR) _szDbg); \
}

#define BNL_ENTER( _module, _func) \
{ \
    char  _szDbg[256]; \
    sprintf( _szDbg, "> %d %s %s\n", _dbg_level++, _module, _func); \
    OutputDebugString( (LPCTSTR) _szDbg); \
    BNL_debug_heap_check( _func); \
}

#define BNL_LEAVE( _module, _func) \
{ \
    char  _szDbg[256]; \
    sprintf( _szDbg, "< %d %s %s\n", --_dbg_level, _module, _func); \
    OutputDebugString( (LPCTSTR) _szDbg); \
    BNL_debug_heap_check( _func); \
}

#endif /* BNL_DEBUG_XML */

#else /* Not WIN32 */

#ifdef BNL_DEBUG_DECLARE
FILE    *_pfDBG = NULL;
int     _dbg_level = 0;
#else
EXTERN_C  FILE    *_pfDBG;
EXTERN_C  int     _dbg_level;
#endif
#ifdef BNL_DEBUG_XML

#define BNL_ENTER( _module, _func) \
{ \
    if (_pfDBG == NULL) \
    { \
        char _szOutFile[133]; \
        sprintf( _szOutFile, "%s/bnl_trace.log", getenv("TEMP")); \
        if ((_pfDBG = fopen( _szOutFile, "w")) == NULL) _pfDBG = stderr; \
    } \
    fprintf( _pfDBG, "<%s module=\"%s\">\n", _func, _module); \
    fflush( _pfDBG); \
}

#define BNL_LEAVE( _module, _func) \
{ \
    fprintf( _pfDBG, "</%s>\n", _func); \
    fflush( _pfDBG); \
}


#else /* not BNL_DEBUG_XML */


#define BNL_ENTER( _module, _func) \
{ \
    if (_pfDBG == NULL) \
    { \
        char _szOutFile[133]; \
        sprintf( _szOutFile, "%s/bnl_trace.log", getenv("TEMP")); \
        if ((_pfDBG = fopen( _szOutFile, "w")) == NULL) _pfDBG = stderr; \
    } \
    fprintf( _pfDBG, "> %d %s %s\n", _dbg_level++, _module, _func); \
    fflush( _pfDBG); \
}

#define BNL_LEAVE( _module, _func) \
{ \
    fprintf( _pfDBG, "< %d %s %s\n", --_dbg_level, _module, _func); \
    fflush( _pfDBG); \
}

#endif /* BNL_DEBUG_XML */

#endif  /* WIN32 */

#else /* BNL_DEBUG not defined */

#define BNL_DEBUG_MSG( _szVar)
#define BNL_DEBUG_LOG( _szVar)
#define BNL_ENTER( _module, _func)
#define BNL_LEAVE( _module, _func)

#endif /* BNL_DEBUG */


#endif /* BNL_DEBUG_H */
