/**
 (C) copyright by EDS PLM Solutions Benelux

 Developed for IMAN
 

 Description    :   Mapping Query module for handling the mapping querys taht 
                    can be given in files for reporting

 Created for    :   

 Comments       :   Every new procedure or function starts with BNL_mpq_

 Filename       :   BNL_mpq_module.c

 Version info   :   release.pointrelease

 History of changes
 Reason / Description                                       Version By            date
 -------------------------------------------------------------------------------------------
 Creation                                                   0.0     EN            08-02-2002
 Updated for usage in C++ env                               0.1     JM            17-05-2010
 Updated for TC10.1                                         0.2     JM            29-08-2013

*/

#include <basetyps.h>

#ifdef  TC10_1
#include <tc/iman.h>
#else
#include <iman.h>
#endif

#ifndef BNL_MPQ
#define BNL_MPQ

#define BNL_MPQ_SYNTAX_ERROR   1;
#define BNL_MPQ_VAL_ERROR      2; 


/*query type identifiers*/
#define BNL_MPQ_TABLE           "TABLE"
#define BNL_MPQ_VALUE           "VALUE"
#define BNL_MPQ_FILE            "FILE"
#define BNL_MPQ_PICTURE         "PICTURE"
#define BNL_MPQ_INCLUDE         "INCLUDE"
#define BNL_MPQ_LINK            "LINK"
#define BNL_MPQ_FIXEDTEXT       "FIXEDTEXT"

/*relation pointers*/
#define BNL_MPQ_RP              "RP"
#define BNL_MPQ_TARGET          "TARGET"
#define BNL_MPQ_BOM             "BOM"
#define BNL_MPQ_REVISIONS       "REVISIONS"
#define BNL_MPQ_ITEMREVISION    "ITEMREVISION"
#define BNL_MPQ_ITEM            "ITEM"
#define BNL_MPQ_NAMEDREF        "NAMEDREF"
#define BNL_MPQ_PRIMARY         "PRIMARY"


/*object pointer*/
#define BNL_MPQ_STORAGE         "STORAGE"

/*attribute pointer*/
#define BNL_MPQ_EXPORTFILE      "EXPORTFILE"

typedef struct sQueryData
{
    tag_t                   tSourceObject;
    char                    *pszValue;

} queryData_t;

/*enumeration of types*/
enum{
      BNL_ATTR_POINT_TYPE,
      BNL_NR_POINT_TYPE,
      BNL_STORAGE_POINT_TYPE,
      BNL_EXPFILE_POINT_TYPE,
      BNL_BOM_POINT_TYPE,
      BNL_REVISIONS_POINT_TYPE,
      BNL_ITEMREVISION_POINT_TYPE,
      BNL_TARGET_POINT_TYPE,
      BNL_RP_POINT_TYPE,
      BNL_ITEM_POINT_TYPE,
      BNL_PRIMARY_POINT_TYPE
} BNL_POINT_TYPES;



EXTERN_C int BNL_mpq_initialise
(
  FILE *pLog
);

EXTERN_C int BNL_mpq_exit
(
);


EXTERN_C char * BNL_mpq_ask_version
(
);

EXTERN_C int BNL_mpq_set_log
(
  FILE *fp
);


EXTERN_C int BNL_mpq_get_next_query_part
(
  char *pszQueryPart,
  char **pszQPart, 
  char **pszNxtQueryPart
);


EXTERN_C int BNL_mpq_execute_mapping_query
(
  int   iQueryObjects,
  tag_t *ptQueryObjects,
  char *pszMappingQuery,
  char **pszQueryTypeId,
  char **pszFormat,
  int  *iColumns,
  int  *iRows,
  queryData_t **pszData

);




EXTERN_C int BNL_mpq_parse_relpointer
(
  char *pszQueryPart,
  char **pszRelation,
  int *iTypes,
  char ***pszTypes
);

EXTERN_C int BNL_mpq_parse_table_field_mpq
(
   char *pszQueryPart,
   int *iCount,
   char ***pszFieldMpq
);

EXTERN_C int BNL_mpq_parse_nr_pointer
(
    char *pszValuecarier,
    char **pszNamedRef,
    char **pszNrType
);

EXTERN_C int BNL_mpq_determine_pointertype
(
    char *pszValueCarier
);

EXTERN_C int BNL_mpq_get_object_value
(
 tag_t ptObject,
 char *pszQueryPart,
 char **Value
);

EXTERN_C int BNL_mpq_get_relation_objects
( 
  tag_t tQueryObject,
  char *pszRelPoint,
  int  *iRelations,
  tag_t **ptRelObjects
);

EXTERN_C int BNL_mpq_determine_rp_type
(
  char *pszRelationPointer
);

int BNL_mpq_get_relations
(
  int   iQueryObjects,
  tag_t *ptQueryObjects,
  char *pszMappingQuery, 
  char **pszQueryPart, 
  int *iRelations, 
  tag_t **ptRelations
);


EXTERN_C int BNL_mpq_set_exportdir
(
   char *pszDir
);

EXTERN_C int BNL_mpq_get_qti
(
  char *pszMappingQuery,
  char **pszIdentifier,
  char **pszFormatInfo,
  char **pszQueryPart
);


#endif BNL_MPQ