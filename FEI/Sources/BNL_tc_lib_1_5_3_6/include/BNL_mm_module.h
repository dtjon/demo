/**
 (C) copyright by UGS Benelux

 Developed for IMAN 
 Versions     OS                          platform
 -----------------------------------------------------------------------------------------------------------
 5.x									  Intel
 7.x									  Intel
 
 Description	:   This module header file belongs to the memory module .c file.
                    The header file should be added to the modules.h file if this module should be 
                    added to the customization.


 Created for	:   Unigraphics Solutions BENELUX


 Comments		:   Every new procedure or function in this mdoules starts 
                    with BNL_<module>_
				    

 Filename		:	BNL_mm_module.h

 Version info   :   release.pointrelease (versions of the modules are stored in the module itself.
					
 History of changes											
 reason /description										                    Version By        date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                   0.0     EH        02-01-2001
 Added global BNL version definition file                   0.1     LB        06-09-2001
 Make it BNL_lib_dll ready                                  0.2     EN        11-05-2006
 Updated for usage in C++ env                               0.3     JM        17-05-2010
 Updated for TC10.1                                         0.4     JM        29-08-2013

*/


/*
    Memory module (MM)
*/

#ifndef BNL_EM_H
#define BNL_EM_H

#ifdef  TC10_1
#include <tc/iman.h>
#else
#include <iman.h>
#endif
#include <basetyps.h>
#include <BNL_tceng_lib.h>

typedef struct s_Listhead
{
    char szListName[32+1];
    int  iCount;
    void *list;

} t_Listhead;


typedef struct s_Taglist
{
    tag_t  tObject;
    struct s_Taglist *next;

} t_Taglist;

/**
char * BNL_ask_MM_version()

  Description: 

  Returns the version of he module.

  parameters: none
*/
EXTERN_C char DllExport *BNL_mm_ask_version
();

/**
void BNL_mm_free
(
    void * pointer      <I>
)

  Description:

  
  A pointer to an allocated memory block by SM_alloc will be freed with SM_free, 
  only if it's NO NULL pointer.

*/
EXTERN_C void DllExport BNL_mm_free
(
    void *             pointer
);

/**
void BNL_mm_free_list
(
    void **     ptPointerList,          <I>
    int         iNumOfElements          <I>    
)

  Description:

  A list of pointers with allocated memory blocks by SM_alloc memory will be freed by 
  every list entry only if they are NO NULL pointers.

*/
EXTERN_C void DllExport BNL_mm_free_list
(
    void **     ptPointerList,
    int         iNumOfElements
);

/**
EXTERN_C int BNL_mm_free_list_head
(
    t_Listhead *pListhead       <I>
)

  Description:
    
      Function to free the listhead which contains a linked list
      of a taglist.

  Returns:

  ITK_ok or !ITK_ok

*/
EXTERN_C int DllExport BNL_mm_free_list_head
(
    t_Listhead *pListhead
);

/**
int BNL_mm_set_log
(
    FILE *pLog  <I>
)

  Description:

  This function sets the log pointer to the given 
  logpointer. If not set then the internal logpointer points
  stderr.

  Returns:

  0 on Ok

*/
EXTERN_C int DllExport BNL_mm_set_log
(
    FILE *pLog
);

/**
char * BNL_ask_version()

  Description: 

  Returns the version of he module.

  parameters: none
*/
EXTERN_C char DllExport *BNL_mm_ask_version
(
);

/**
int BNL_mm_initialise
(
  FILE *pLog
  
)

  Description:

  Initialises the memory module.

  
  Returns:
    
  0 on Ok.


*/
EXTERN_C int DllExport BNL_mm_initialise
(
  FILE *pLog
);

/**
int BNL_mm_exit
(
)

  Description:

  Exit the module in save way after calling this function
  and reuse this module it needs to be initialised again

  Returns:

  0 on Ok
*/
EXTERN_C int DllExport BNL_mm_exit
(
);

#endif
