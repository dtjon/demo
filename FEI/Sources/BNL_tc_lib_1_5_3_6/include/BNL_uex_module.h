/**
 (C) copyright by UGS PLM Solutions

 Description	:  Universal export module, exports iman objects in the 
                   existence of XML objects


 Created for	:   UGS PLM Benelux


 Comments		:   

 Filename		:	BNL_uex_module.h

 Version info   :   release.pointrelease (versions of the modules are stored in the module itself.
					
 History of changes											
 reason /description							              Version   by       date
 ----------------------------------------------------------------------------------------------------------
 Creation                                       0.0       EN       14-10-2003
 made it module								                  0.1       EN       24-02-2004
 set bufsize to 10000							              0.2       EN       26-04-2004
 added MEProcess class                          0.3       EN       11-02-2005
 Make it BNL_lib_dll ready                      3.2       EN       11-05-2006
 Updated for TC10.1                             3.3       JM       29-08-2013
*/

#ifndef BNL_UEX_H
#define BNL_UEX_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef TC10_1
#include <tc/iman.h>
#include <sa/tcfile.h>
#else
#include <iman.h>
#include <imanfile.h>
#endif

#include <BNL_fm_module.h>
#include <BNL_xml_module.h>
#include <BNL_ini_module.h>
#include <BNL_em_module.h>
#include <BNL_tb_module.h>
#include <BNL_tceng_lib.h>



#define BNL_CHECK_ERROR(Function,RetCode)  {   \
   char *err_string = NULL;   \
   if (RetCode != 0)   \
   {   \
      fprintf(stderr,"%s: Error_%d in %s line %d.\n",BNL_module,RetCode,#Function,__LINE__ ); \
      fprintf(fpLog,"%s: Error_%d in %s line %d.\n",BNL_module,RetCode,#Function,__LINE__ ); \
   }   \
  }

#define BUFSIZE 10000


#define BNL_DBOBJECT_TAG    "dbobject"
#define BNL_RELATIONS_TAG   "relations"
#define BNL_RELATION_TAG    "relation"
#define BNL_PROPS_TAG       "props"
#define BNL_PROP_TAG        "prop"
#define BNL_FORMVALS_TAG    "formvals"
#define BNL_FORMVAL_TAG     "formval"
#define BNL_NAMEDREFS_TAG   "namedreferences"
#define BNL_NAMEDREF_TAG    "namedreference"
#define BNL_BOMLINES_TAG    "bomlines"
#define BNL_BOMLINE_TAG     "bomline"
#define BNL_CLASS_ATTR      "class"
#define BNL_NAME_ATTR       "name"
#define BNL_TYPE_ATTR       "type"
#define BNL_PUID_ATTR       "puid"
#define BNL_ITEMPUID_ATTR   "itempuid"
#define BNL_BVPUID_ATTR     "bvpuid"
#define BNL_LOCATION_ATTR   "location"
#define BNL_PRECISE_ATTR	"precise"
#define BNL_REVRULE_ATTR    "revrule"
#define BNL_PARENTPUID_ATTR "parentpuid"


#define BNL_VARIANTCONDITION_TAG "variantcondition"
#define BNL_MVLBLOCK_TAG "mvlblock"
#define BNL_OPTIONS_TAG     "options"
#define BNL_OPTION_TAG      "option"
#define BNL_XML_TAG         "bnlxml"


#define BNL_INI_RELATIONS   "relations"
#define BNL_INI_PROPS       "props"
#define BNL_INI_BL_PROPS    "bl_props"
#define BNL_INI_FORMVALS    "formvals"
#define BNL_INI_NAMEDREFS   "namedrefs"
#define BNL_INI_REVRULE     "revrule"
#define BNL_INI_LEVEL       "level"
#define BNL_INI_BL_CORRES   "bl_corresponding"
#define BNL_INI_BL_CORRES_REL "bl_corres_relations"
#define BNL_INI_BL_VARIANTDATA "export_variantdata"


/*module runtime props*/ 
#define BNL_ITEM_ID              "item_id"
#define BNL_ITEM_NAME			 "item_name"
#define BNL_REVISION_LIST        "revision_list"
#define BNL_RELEASE_STATUS_LIST  "release_status_list"
#define BNL_OBJECT_NAME          "object_name"
#define BNL_ITEMREVISION         "ItemRevision"
#define BNL_ITEM                 "Item"
#define BNL_FORM                 "Form"
#define BNL_DATASET              "Dataset"
#define BNL_IMANFILE             "ImanFile"
#define BNL_STRUCTURE_REVISIONS  "structure_revisions"
#define BNL_BOM_VIEW_TAGS        "bom_view_tags"
#define BNL_PSBOMVIEW            "PSBOMView"
#define BNL_PSBOMVIEWREVISION    "PSBOMViewRevision"
#define BNL_DATASETATTR          "datasetattr"
#define BNL_MEPROCESS            "MEProcess"
#define BNL_MASTER_FORM_REV      "IMAN_master_form_rev"
#define BNL_MASTER_FORM          "IMAN_master_form"
#define BNL_MEOPREVISION		 "MEOPRevision"
#define BNL_ACTIVITY			 "activity"
#define BNL_FOLDER				 "Folder"
#define BNL_CONTENTS			 "contents"


/*hardcoded vars used to compare*/
#define BNL_BOMLINE "BOMLine"


int DllExport BNL_uex_set_log
(
  FILE *pLog
);

int DllExport BNL_uex_initialise
(
   FILE *pLog, 
   char *pszIni
);

int DllExport BNL_uex_set_ini_loc
(
  char *pszIniFile
);

int DllExport BNL_uex_export_imanfile
(
   tag_t tImanFile, 
   char *pszPath, 
   char *pszFileName,
   char **pszFullExportPath
);

int DllExport BNL_uex_ask_original_file_location
(
   tag_t tImanFile, 
   char **pszOrgPath
);


int DllExport BNL_uex_get_attribuut_value
( t_XMLElement *pXMLElement, 
  char *pszAttrName, 
  char **pszVal
);


int DllExport BNL_uex_handle_bomlines
(
  tag_t tBomWindow,
  char *szType,
  int iCorres,
  int iCorresRel,
  int iVariantData,
  tag_t tParentLine,
  tag_t tBomLine,
  int iLevel,
  int iMaxLevel,
  t_XMLElement **pBomObject
);


/**
int BNL_create_xml_object
(
  tag_t tObject,				<I>
  t_XMLElement **pXmlObject		<O>
);

  This function creates the XML object, depending on the initialisation
  file it will walk into the object model. The previsios relation can be set
  to NULL. if given and relation in inifile contains levelstop (relx:1) then
  it will only go one level deep, in example

  revision
     relx           <-it will stop here and go not further on next relations of relx
	    revision
		      relx 
			     revision
  The returning xml object can be direclty put into file using the xml 
  module.

  In case iNoRel is 1, relations will not reported. 
*/
int DllExport BNL_uex_create_xml_object
(
  char *pszPrevRelation,
  tag_t tObject,
  int iNoRel,
  t_XMLElement **pXmlObject
);

int DllExport BNL_uex_get_property
(
  tag_t tObject,
  char *pszProp,char **pszVal
);

int DllExport BNL_uex_exit();

#endif



