/**
 (C) copyright by UGS Benelux

 Developed for IMAN 
 Versions     OS                          platform
 -----------------------------------------------------------------------------------------------------------
 			  winnt 					  Intel 	
 
 Description	:   This module header file contains the version setting of the BNL tools. 
                    It should be included in all BNL_modules.


 Created for	:   UGS BENELUX


 Comments		:   
				    

 Filename		:	BNL_version.h

 Version info   :   release.pointrelease (versions of the modules are stored in the module itself.
					
 History of changes											
 reason /description                                by      version     date
 ----------------------------------------------------------------------------------------------------------
 Creation                                           EN      1.7         06-09-2001
 put the registered company string in here the 
 company.h file is obsolete                         EN      1.9         12-11-2002          
 make it version 2.0                                EN      2.0         03-02-2003
 change name from eds to UGS		                    EN	    2.1         05-02-2004
 make it standard in scm and changed text for 
 registerd version                                  EN      2.2         21-04-2004 
 Make it BNL_lib_dll ready                          EN      2.3         11-05-2006
*/
#ifndef BNL_VERSION_H
#define BNL_VERSION_H

static char BNL_registered_company[]="Licensed Program(s) provided by Siemens PLM Software Benelux,\nare furnished to Customer solely for Customer's own use.\nUnauthorized use is prohibited.\n";
//because we get the version of the lib is determined by scm we do not use this anymore.
static char BNL_registered_version[]="See releasenotes";

#endif
