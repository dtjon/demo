/**
 (C) copyright by UGS Benelux 2001

 Description    :   This header file belongs to the BNL_xml_module.c

 Created for    :   UGS BENELUX

 Comments       :   Every new procedure or function starts with BNL_xml_
				    
 Filename       :   BNL_xml module.h

 Version info   :   release.pointrelease
                    
					
 History of changes											
 reason /description										        Version     By          Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                           0.0         EN          10-07-2001
 Added Three routine definitions                                    0.1         LB          6-11-2001
 Added BNL_xml_element_delete                                       0.2         LB          20-08-2002
 made sections in file and added some functions			          			0.3         EN          17-03-2005
 Added functions BNL_xml_set_preserve_whitespace and          			0.4         JM          30-09-2005
 BNL_xml_ask_preserve_whitespace
 Make it BNL_lib_dll ready                                          0.5         EN          11-05-2006
 Updated definition of functions
  BNL_xml_attribute_create and BNL_xml_element_add_attribute        0.6         JM          17-07-2008
 Added BNL_xml_replace_chars_to_enitity_reference                   0.7         JM          01-12-2009
 Updated for usage in C++ env                                       0.8         JM          19-04-2010


 !! if change version then change version in BNL_module variable
*/

#ifndef BNL_XML_H
#define BNL_XML_H

#include <stdio.h>
#include <stdlib.h>
#include <basetyps.h>
#include <BNL_tceng_lib.h>

typedef struct s_Attribute
{
    char *pszName;
    char *pszValue;
    struct s_Attribute *pNext;
}t_Attribute;

typedef struct s_XMLelement
{
    char *pszElementname;
    char *pszElementValue;
    int iAttributes;
    t_Attribute *pAttributes;
    struct s_XMLelement *pRoot;
    struct s_XMLelement *pParent;
    struct s_XMLelement *pNextSibling;
    struct s_XMLelement *pPrevSibling;
    struct s_XMLelement *pChilds;
    int iChilds;
}t_XMLElement;


/*#######module functions##############*/
EXTERN_C int DllExport BNL_xml_initialise
(
  FILE *pLog,
  char *pszIni
);

EXTERN_C char DllExport *BNL_xml_ask_version
(
);

EXTERN_C  int DllExport BNL_xml_exit
(
);

EXTERN_C  int DllExport BNL_xml_set_log
(
  FILE *fp
);


/*#######element functions##################*/
EXTERN_C  t_XMLElement DllExport *BNL_xml_element_create
(
    char *pszTag, 
    char *pszValue
);


EXTERN_C int DllExport BNL_xml_element_add_element
(
    t_XMLElement *pParentElement, 
    t_XMLElement *pChildElement
);

EXTERN_C int DllExport BNL_xml_element_set_name
(
    t_XMLElement *pElement, 
    char *pszName
);

EXTERN_C int DllExport BNL_xml_element_set_value
(
    t_XMLElement *pElement, 
    char *pszValue
);

EXTERN_C int DllExport BNL_xml_element_report
(
    FILE * fp,t_XMLElement *pElement
);

EXTERN_C int BNL_xml_element_free
(
    t_XMLElement *pElementToFree
);

EXTERN_C int DllExport BNL_xml_element_add_attribute
(
    t_XMLElement *pElement,
    t_Attribute *pAttribute
);

EXTERN_C int DllExport BNL_xml_element_create_attribute
(
  t_XMLElement *pElement,
  char *pszAttrName,
  char *pszAttrvalue
   
);

EXTERN_C int BNL_xml_element_delete_attribute
(
   t_XMLElement *pElm,
   char *pszAttrName
);

EXTERN_C int DllExport BNL_xml_element_delete
(
    t_XMLElement *pWorkingElement
);

EXTERN_C int DllExport BNL_xml_element_get_attribute_value
(
   t_XMLElement *pElm,
   char *pszAttrName,
   char **pszAttrVal
);

/**
int BNL_xml_element_set_attribute_value
(
   t_XMLElement *pElm, <I>
   char *pszAttrName,  <I>
   char **pszAttrVal   <I>
)


  Description:

  Sets from the element the attribuut value of the given attribuut name.


*/
int DllExport BNL_xml_element_set_attribute_value
(
   t_XMLElement *pElm,
   char *pszAttrName,
   char *pszAttrVal
);




/*###################attribute functions###################*/
EXTERN_C t_Attribute DllExport *BNL_xml_attribute_create
(
   char *pszName ,
   char *pszValue
);

EXTERN_C int BNL_xml_attribute_set_name
(
    t_Attribute *pAttribute ,
    char *pszName
);

EXTERN_C int BNL_xml_attribute_set_value
(
    t_Attribute *pAttribute,
    char *pszValue
);

EXTERN_C int BNL_xml_attribute_free
(
  t_Attribute *pAttribute
);



/*############General XML functions############*/
EXTERN_C int DllExport BNL_xml_load_document
(
    char *pszFilePath, 
    t_XMLElement **pDocument
);

EXTERN_C int DllExport BNL_xml_close_document
(
    t_XMLElement *pDocument 
);

EXTERN_C int BNL_xml_read_file
(
    FILE * pfStream,
    t_XMLElement **pDocumentEntry
);

EXTERN_C int DllExport BNL_xml_write_document
(
   FILE *fp, 
   t_XMLElement *pDocumentroot
);

/* query functions */
EXTERN_C int DllExport BNL_xml_get_siblings_of_level
(
    t_XMLElement *pRootElement,
    int iLevel,
    int *iElements, 
    t_XMLElement ***pElements
);

EXTERN_C int DllExport BNL_xml_get_element_child_with_name
(
    t_XMLElement *pElement,   
    char *pszElementName, 
    t_XMLElement **pElementReturn
);



EXTERN_C int DllExport BNL_xml_element_copy
(
    t_XMLElement *pElement,
    t_XMLElement **pCopiedElement 
);

EXTERN_C t_XMLElement DllExport *BNL_xml_find_first
(
    t_XMLElement    *pParent,
    char            *pszTag
);

EXTERN_C t_XMLElement DllExport *BNL_xml_find_next
(
    t_XMLElement    *pLast,
    char            *pszTag
);

EXTERN_C t_XMLElement DllExport *BNL_xml_find_prev
(
    t_XMLElement    *pLast,
    char            *pszTag
);




int DllExport BNL_xml_convert_xml_to_buffer
(
  t_XMLElement *pXmlStruc,
  int *iBufSize,
  char **pszBuffer
);


int DllExport BNL_xml_convert_buffer_to_xml
(
   char *pszBuffer,
   t_XMLElement **pXmlStruc
);


/**
int BNL_xml_set_preserve_whitespace
(
)

  Description:

  Options to preserve the white space in the values.

  Returns:

  0 on Ok.

*/

int DllExport BNL_xml_set_preserve_whitespace
(
  int iPreserveWhiteSpace
);

/**
int BNL_xml_ask_preserve_whitespace
(
)

  Description:

  Returns the setting of the option to preserve the white space in the values.

  Returns:

  The setting.

*/
int DllExport BNL_xml_ask_preserve_whitespace();


/**
int BNL_xml_replace_chars_to_enitity_reference
(
  char *pszString,
  char **pszReturn
)

  Description:

  This function repalces chars like '&' and '<' to entity references like
  '&amp;' and '&lt;'


*/
int DllExport BNL_xml_replace_chars_to_enitity_reference
(
  char *pszString,
  char **pszReturn
);
#endif
