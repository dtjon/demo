/**
 (C) copyright by Unigraphics Solutions BV Netherlands


 Description	:   This file is to register the fei report web user exits.

 Created for	:   UGS BNL


 Comments		:   Every new procedure or function in this module starts
                    with BNL_<module>_

 Filename       :   $Id: BNL_register_feirep.c 481 2016-07-07 14:53:04Z tjonkonj $

 Version info   :   release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 reason /description										Version By                  date
 ----------------------------------------------------------------------------------------------------------
 Creation of template                                       0.0     Erik Navis          28-04-2005
 Added docdownload menu option to start procedure           0.1     Erik Navis          10-01-2006
 check length of processname in initiate process action     0.2     Erik Navis          03-03-2007
 Removed SM_ function to be able to support tceng2005       0.3     Erik Navis          12-03-2007
 Fixed problem with process name length                     0.4     J. Mansvelders      14-12-2007
 Added ebom reports                                         0.5     J. Mansvelders      13-08-2008
 Updated BNL_rp_web_bomdiff and BNL_rp_web_eco_diffs        0.6     J. Mansvelders      20-05-2009
 Added attrdiff report                                      0.7     J.Wijnja            11-06-2009
 Updated BNL_rp_web_where_used                              0.8     J.Mansvelders       12-06-2009
 Added product attributes report                            0.9     J.Wijnja            15-06-2009
 Added handler                                              1.0     J. Mansvelders      15-06-2009
 Added successor/predecessor report                         1.1     J.Wijnja            16-06-2009
 Updated where_used                                         1.2     J. Mansvelders      10-09-2009
 Added handler                                              1.3     J. Mansvelders      05-10-2009
 Added bomrep mfg/pro                                       1.4     J. Mansvelders      05-01-2010
 Removed AH-FEI-osmail-report                               1.5     J. Mansvelders      18-01-2010
 Added where used top level                                 1.6     J. Mansvelders      22-02-2010
 Added FRUList report                                       1.7     J. Wijnja           25-03-2010
 Update BNL_register_web_feirep                             1.8     J. Mansvelders      06-04-2010
 Updatef for TC 8.3.*                                       1.9     J. Mansvelders      16-09-2011
 Updated mem management for 64 bit                          2.0     J. Mansvelders      11-11-2011
 Update version number only for IR-7542280                  2.1     DTJ                 10-11-2015
 Update for TC 10.1.*                                       2.2     D.Tjon              31-05-2016
 Remove BNL_Register_RH_complicance_check                   2.3     D.Tjon              06-07-2016

*/


/*

  The tmpl should be replaced by an meaningfull abbrivation. of the aplication to register.

  !!!NOTE that the real functionality is not placed in this file, only registration should take place here.
  This makes it easier to maintain the source.

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef  TC10_1
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#else
#include <iman.h>
#include <ict_userservice.h>
#include <custom.h>
#include <user_exits.h>
#include <user_server_exits.h>
#include <preferences.h>
#include <folder.h>
#include <aom.h>
#include <epm.h>
#endif

#include <BNL_register_bnllibs.h>
#include <BNL_rp_module.h>
#include <BNL_em_module.h>
#include <BNL_tb_module.h>

#include <BNL_RH_compliance_check.h>

char BNL_module[] ="BNL FEIREP 2.3";
char DLL_module[] ="BNL_FEIREP";

/*because the length of filename is truncated we can not use dllmodule as name :-( */
char DLL_logfilename[] = "BNLFEIREP";


DLLAPI int BNL_register_bnl_feirep(int *decision, va_list args);
DLLAPI int BNL_exit_bnl_feirep(int *decision, va_list args);
DLLAPI int BNL_feirep(int *decision, va_list args);
DLLAPI int BNL_register_web_feirep(int *decision, va_list args);

int BNL_register_se_feirep(int *decision, va_list args);
int BNL_feirep_execute_process(char *pszProcedure,char *pszProcessName, int iTargetCount, tag_t *ptTargets);
int BNL_us_start_process(void *pReturnValue);
int EPM_find_template(char *proc,int iX,tag_t *tProcTemplate);



void welcome()
{
  if (giErrorLevel < 10)
  {
	  fprintf(stdout,"*******************************\n");
	  fprintf(stdout,"Copyright Siemens PLM Software BENELUX 2005-2010\n");
	  fprintf(stdout,"Version %s registered\n",BNL_module);
	  fprintf(stdout,"*******************************\n");
  }

  IMAN_write_syslog("*******************************\n");
	IMAN_write_syslog("Copyright Siemens PLM Software BENELUX 2005-2010\n");
	IMAN_write_syslog("Version %s registered\n",BNL_module);
	IMAN_write_syslog("*******************************\n");
}


/*
DLLAPI int BNL_FEIREP_register_callbacks()

  Description:

  The dll entry called by.

*/
DLLAPI int BNL_FEIREP_register_callbacks()
{
	int iRetCode=0;


  /*register the bnl libs*/
  iRetCode=BNL_register_bnllibs(DLL_module,BNL_module,DLL_logfilename,&ugslog);

	welcome();

	/*
	  at this point we register the user exit or server exit functions
	  in this example the user_init and user_exit.

	*/

	CUSTOM_register_exit(DLL_module, "USER_init_module", (CUSTOM_EXIT_ftn_t)BNL_register_bnl_feirep);

	CUSTOM_register_exit(DLL_module, "USER_invoke_user_code_taglist", (CUSTOM_EXIT_ftn_t)BNL_register_web_feirep);
  CUSTOM_register_exit(DLL_module, "USERSERVICE_register_methods", (CUSTOM_EXIT_ftn_t)BNL_register_se_feirep);

	CUSTOM_register_exit(DLL_module, "USER_exit_module", (CUSTOM_EXIT_ftn_t)BNL_exit_bnl_feirep);

  return iRetCode;
}


/**
DLLAPI int BNL_register_bnl_feirep(int *decision, va_list args)

  Description:

  registers the bnl lib and open the logfile.

*/
DLLAPI int BNL_register_bnl_feirep(int *decision, va_list args)
{
	int iRetCode			=0;

	*decision = ALL_CUSTOMIZATIONS;

	/*register the report module*/
	BNL_rp_initialise(ugslog);

  // Register the handlers
  //BNL_Register_AH_FEI_osmail_report();

  // from tc10, replaced by BNL_CEP
  //BNL_Register_RH_complicance_check();

	return iRetCode;
}

/**
DLLAPI int BNL_feirep(int *decision, va_list args)

  Description:

  registers the custom modules.

*/
DLLAPI int BNL_register_web_feirep(int *decision, va_list args)
{
	int iRetCode			= 0;
	int iInputCode		= 0;
	int i					    = 0;
	int *iOutputCount	= NULL;
  int iLevel        = 0;
  int iComplianceResult = 0;

  tag_t tInputTag			=NULLTAG;
	tag_t tPred				=NULLTAG;
	tag_t tProd				=NULLTAG;
	tag_t tRev				=NULLTAG;
	tag_t tECO				=NULLTAG;
	tag_t *tOutputTag		=NULL;
	tag_t **ptOutputTagList	=NULL;


	char **pszOutputString	=NULL;
	char *pszReturnString	=NULL;
	char *pszInputString	=NULL;
  char *pszPredecessor    =NULL;
	char *pszTemp			=NULL;
	char *pszRevRule		=NULL;
  char *pszRuleName		=NULL;
	char *pszLevel			=NULL;
	char *pszEffDate		=NULL;
  char *pszCheckName  =NULL;
  char *pszFilterType    = NULL;

  t_XMLElement *pDocument =NULL;



	//*decision = ALL_CUSTOMIZATIONS;


	/*get the parameters of the function*/
	iInputCode=va_arg(args,int);
  pszInputString=va_arg(args,char*);
	tInputTag=va_arg(args,tag_t);
	tOutputTag=va_arg(args,tag_t*);
	pszOutputString=va_arg(args,char**);
	iOutputCount=va_arg(args,int*);
	ptOutputTagList=va_arg(args,tag_t**);


	switch (iInputCode)
  {
    *decision = ONLY_CURRENT_CUSTOMIZATION;
    case 1:

      pszPredecessor=strchr(pszInputString,';');
      if (pszPredecessor!=NULL)
      {
        *pszPredecessor='\0';
        pszPredecessor++;
        iRetCode=POM_string_to_tag(pszPredecessor,&tPred);
      }
      else
      {
        tPred=NULLTAG;
      }

      iRetCode=POM_string_to_tag(pszInputString,&tProd);

      iRetCode=BNL_rp_web_docdiff(tProd,tPred,&pszReturnString,&pDocument);

      *pszOutputString=MEM_alloc(sizeof(char) * ((int)strlen(pszReturnString)+1) );
		  strcpy(*pszOutputString,pszReturnString);

      BNL_xml_close_document(pDocument);

      //MEM_free(pszReturnString);
      free(pszReturnString);

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 2:

      pszPredecessor=strchr(pszInputString,';');
      if (pszPredecessor!=NULL)
      {
        *pszPredecessor='\0';
        pszPredecessor++;
        iRetCode=POM_string_to_tag(pszPredecessor,&tPred);
      }
      else
      {
        tPred=NULLTAG;
      }

      iRetCode=POM_string_to_tag(pszInputString,&tProd);


      //fprintf(ugslog, "DEBUG incoming tag :[%d]\n", tInputTag);
      iRetCode = CFM_ask_rule_text(tInputTag, &pszRuleName);
      //fprintf(ugslog, "DEBUG name :[%s]\n", pszRuleName);


      //tInputTag


      iRetCode=BNL_rp_web_bomdiff(tProd,tPred,pszRuleName,&pszReturnString,&pDocument);

      fprintf(ugslog,"returns:[%s]",pszReturnString);

      *pszOutputString=MEM_alloc(sizeof(char) * ((int)strlen(pszReturnString)+1) );
		  strcpy(*pszOutputString,pszReturnString);

      BNL_xml_close_document(pDocument);

      //MEM_free(pszReturnString);
      free(pszReturnString);

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 3:

  	  fprintf(ugslog,"DEBUG:inputstring[%s]\n",pszInputString);
      iRetCode=POM_string_to_tag(pszInputString,&tRev);

		  fprintf(ugslog,"DEBUG:tag[%d]\n",tRev);

		  fprintf(ugslog,"run rp_webdocs\n");

		  BNL_rp_web_docs(tRev, &pszReturnString,&pDocument);
		  fprintf(ugslog,"come out rp_webdocs\n [%s]\n",pszReturnString);

      *pszOutputString=MEM_alloc(sizeof(char) * ((int)strlen(pszReturnString)+1) );
		  strcpy(*pszOutputString,pszReturnString);

      fprintf(ugslog,"returns:[%s]",*pszOutputString);

      BNL_xml_close_document(pDocument);

      //MEM_free(pszReturnString);
      free(pszReturnString);

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 4:

      iRetCode=POM_string_to_tag(pszInputString,&tECO);

      //tInputTag
      //fprintf(ugslog, "DEBUG incoming tag :[%d]\n", tInputTag);
      iRetCode = CFM_ask_rule_text(tInputTag, &pszRuleName);
      //fprintf(ugslog, "DEBUG name :[%s]\n", pszRuleName);

      iRetCode=BNL_rp_web_eco_diffs(tECO,pszRuleName,&pszReturnString,&pDocument);

      *pszOutputString=MEM_alloc(sizeof(char) * ((int)strlen(pszReturnString)+1) );
		  strcpy(*pszOutputString,pszReturnString);

      fprintf(ugslog,"returns:[%s]",pszReturnString);

      fprintf(ugslog,"Closing document\n");
      BNL_xml_close_document(pDocument);
      fprintf(ugslog,"document closed\n");

      //fprintf(ugslog,"free return\n");
      //MEM_free(pszReturnString);
      free(pszReturnString);
      //fprintf(ugslog,"return freed\n");

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 5: // bomrep
    case 8: // ebompackedrep
    case 9: // ebomrep
    case 11: // bomrep extended
    case 15: // bomrep mfg/pro

      /*input consist of
        tag:revrule:level:effdate
      */

      fprintf(ugslog,"input [%s]\n",pszInputString);

      if ((pszRevRule=strchr(pszInputString,':')) != NULL)
      {
        *pszRevRule='\0';
        pszRevRule++;
      }

      if ((pszLevel=strchr(pszRevRule,':')) != NULL)
      {
        *pszLevel='\0';
        pszLevel++;
      }

      if ((pszEffDate=strchr(pszLevel,':')) != NULL)
      {
        *pszEffDate='\0';
        pszEffDate++;
      }

      fprintf(ugslog,"parsed [%s %s %s]\n",pszInputString,pszRevRule,pszLevel,pszEffDate);

      iRetCode=POM_string_to_tag(pszInputString,&tRev);

      if (iInputCode == 8) // ebompackedrep
      {
        iRetCode=BNL_rp_web_bom(EBOMPACKEDREP,tRev,pszRevRule,pszEffDate,atoi(pszLevel),0,&pszReturnString,&pDocument,1);
      }
      else if (iInputCode == 9) // ebomrep
      {
        iRetCode=BNL_rp_web_bom(EBOMREP,tRev,pszRevRule,pszEffDate,atoi(pszLevel),0,&pszReturnString,&pDocument,1);
      }
      else if (iInputCode == 11) // bomrep_extended
      {
        iRetCode=BNL_rp_web_bom(BOMREPEXT,tRev,pszRevRule,pszEffDate,atoi(pszLevel),0,&pszReturnString,&pDocument,1);
      }
      else if (iInputCode == 15) // bomrep mfg/rpo
      {
        iRetCode=BNL_rp_web_bom(BOMREPMFG,tRev,pszRevRule,pszEffDate,atoi(pszLevel),0,&pszReturnString,&pDocument,1);
      }
      else // bomrep
      {
        iRetCode=BNL_rp_web_bom(BOMREP,tRev,pszRevRule,pszEffDate,atoi(pszLevel),0,&pszReturnString,&pDocument,1);
      }

      *pszOutputString=MEM_alloc(sizeof(char) * ((int)strlen(pszReturnString)+1) );
		  strcpy(*pszOutputString,pszReturnString);

      fprintf(ugslog,"returns:[%s]",pszReturnString);

      BNL_xml_close_document(pDocument);

      //MEM_free(pszReturnString);
      free(pszReturnString);

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 6:

      {
        char *pszProcedure    =NULL;
        char *pszNode         =NULL;
        char *pszProcessName  =NULL;
        char *pszTag          =NULL;
        int iTags             =0;
        tag_t *ptTags         =NULL;
        tag_t tTarget         =NULLTAG;
        char szClass[33];

        fprintf(ugslog,"input [%s]\n",pszInputString);
        //procedure:processname:tag:tag:tag:tag...

        pszProcedure=pszInputString;
        if ((pszNode=strchr(pszInputString,':')) != NULL)
        {
          *pszNode='\0';
          pszNode++;
          pszProcessName=pszNode;
        }

        if ((pszNode=strchr(pszNode,':')) != NULL)
        {
          *pszNode='\0';
          pszNode++;
        }

        //the rest are tags
        while (pszNode!=NULL)
        {
          pszTag=pszNode;

          pszNode=strchr(pszNode,':');
          if (pszNode!=NULL)
          {
            *pszNode='\0';
            pszNode++;
          }

          fprintf(ugslog,"DEBUG:tag=%s\n",pszTag);
          //translate here pszTag and add to array
          iRetCode=POM_string_to_tag(pszTag,&tTarget);

          fprintf(ugslog,"DEBUG:ttag=%d\n",tTarget);

          BNL_tb_determine_super_class(tTarget,szClass);

          fprintf(ugslog,"DEBUG:class=%s\n",szClass);

          if (iTags==0)
          {
            ptTags=MEM_alloc(sizeof(tag_t)*1);
            ptTags[0]=tTarget;
            iTags++;
          }
          else
          {
            iTags++;
            ptTags=MEM_realloc(ptTags,iTags);
            ptTags[iTags-1]=tTarget;
          }
        }


        fprintf(ugslog,"Found [%d] tags to process\n",iTags);

        //check length of processname
        if (strlen(pszProcessName)>32)
        {
          //pszProcessName[32]='~';
          //pszProcessName[33]='\0';
          pszProcessName[31]='~';
          pszProcessName[32]='\0';
        }

        iRetCode=BNL_feirep_execute_process(pszProcedure,pszProcessName, iTags, ptTags);

        fprintf(ugslog,"Process [%s] with name [%s] initiated\n",pszProcedure,pszProcessName);

        MEM_free(ptTags);

        *pszOutputString=MEM_alloc(sizeof(char) * 32 );

        sprintf(*pszOutputString,"%d",iRetCode);

        fprintf(ugslog,"return [%s]\n",*pszOutputString);
      }

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 7:

		  fprintf(ugslog,"DEBUG: inputstring [%s]\n",pszInputString);

      if ((pszLevel=strchr(pszInputString,':')) != NULL)
      {
        *pszLevel='\0';
        pszLevel++;
      }

      if ((pszFilterType=strchr(pszLevel,':')) != NULL)
      {
        *pszFilterType='\0';
        pszFilterType++;
      }

      fprintf(ugslog,"DEBUG: level [%s]\n", pszLevel);
      fprintf(ugslog,"DEBUG: filter type [%s]\n", pszFilterType);

      if (pszLevel == NULL)
      {
        iLevel = 1;
      }
      else
      {
        iLevel = atoi(pszLevel);
      }

      iRetCode=POM_string_to_tag(pszInputString, &tRev);

		  fprintf(ugslog,"DEBUG:tag[%d]\n",tRev);

		  fprintf(ugslog,"run FEI whereused\n");

      if (strcmp(pszFilterType, "-") == 0)
      {
        BNL_rp_web_where_used(1, iLevel, NULL, NULL, tRev, &pszReturnString, &pDocument);
      }
      else
      {
        BNL_rp_web_where_used(1, iLevel, pszFilterType, NULL, tRev, &pszReturnString, &pDocument);
      }

      fprintf(ugslog,"come out BNL_rp_web_where_used\n [%s]\n",pszReturnString);

      *pszOutputString=MEM_alloc(sizeof(char) * ((int)strlen(pszReturnString)+1) );
		  strcpy(*pszOutputString,pszReturnString);

      fprintf(ugslog,"returns:[%s]",*pszOutputString);

      BNL_xml_close_document(pDocument);
      // JM|Gives error?
      //MEM_free(pszReturnString);
      free(pszReturnString);

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    case 10:

      pszPredecessor=strchr(pszInputString,';');
      if (pszPredecessor!=NULL)
      {
        *pszPredecessor='\0';
        pszPredecessor++;
        iRetCode=POM_string_to_tag(pszPredecessor,&tPred);
      }
      else
      {
        tPred=NULLTAG;
      }

      iRetCode=POM_string_to_tag(pszInputString,&tProd);

      iRetCode=BNL_rp_web_attrdiff(tProd,tPred,&pszReturnString,&pDocument);

      *pszOutputString=MEM_alloc(sizeof(char) * ((int)strlen(pszReturnString)+1) );
		  strcpy(*pszOutputString,pszReturnString);

      BNL_xml_close_document(pDocument);

      //MEM_free(pszReturnString);
      free(pszReturnString);

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 12:

      iRetCode=POM_string_to_tag(pszInputString,&tProd);

      iRetCode=BNL_rp_web_prod_attr(tProd,&pszReturnString,&pDocument);

      *pszOutputString=MEM_alloc(sizeof(char) * ((int)strlen(pszReturnString)+1) );
		  strcpy(*pszOutputString,pszReturnString);

      BNL_xml_close_document(pDocument);

      //MEM_free(pszReturnString);
      free(pszReturnString);

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 13:

      iRetCode=POM_string_to_tag(pszInputString,&tProd);

      iRetCode=BNL_rp_web_succ_pred(tProd,&pszReturnString,&pDocument);

      *pszOutputString=MEM_alloc(sizeof(char) * ((int)strlen(pszReturnString)+1) );
		  strcpy(*pszOutputString,pszReturnString);

      BNL_xml_close_document(pDocument);

      //MEM_free(pszReturnString);
      free(pszReturnString);

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 14: // Compliance report

      fprintf(ugslog, "input [%s]\n", pszInputString);

      if ((pszCheckName = strchr(pszInputString,':')) != NULL)
      {
        *pszCheckName = '\0';
        pszCheckName++;
      }

      fprintf(ugslog, "parsed [%s %s]\n", pszInputString, pszCheckName);

      iRetCode = POM_string_to_tag(pszInputString, &tProd);

      iRetCode = BNL_rp_web_comprep(1, &tProd, 0, pszCheckName, &pszReturnString, &pDocument, &iComplianceResult);

      fprintf(ugslog, "result [%d]\n", iComplianceResult);

      *pszOutputString=MEM_alloc(sizeof(char) * ((int)strlen(pszReturnString)+1) );
		  strcpy(*pszOutputString,pszReturnString);

      BNL_xml_close_document(pDocument);

      //MEM_free(pszReturnString);
      free(pszReturnString);

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 16: // Where used (Top Level)

      fprintf(ugslog,"DEBUG: inputstring [%s]\n",pszInputString);

      if ((pszLevel = strchr(pszInputString,':')) != NULL)
      {
        *pszLevel = '\0';
        pszLevel++;
      }

      if ((pszFilterType = strchr(pszLevel,':')) != NULL)
      {
        *pszFilterType = '\0';
        pszFilterType++;
      }

      if ((pszRevRule = strchr(pszFilterType,':')) != NULL)
      {
        *pszRevRule = '\0';
        pszRevRule++;
      }

      fprintf(ugslog,"DEBUG: level [%s]\n", pszLevel);
      fprintf(ugslog,"DEBUG: filter type [%s]\n", pszFilterType);
      fprintf(ugslog,"DEBUG: revision rule [%s]\n", pszRevRule);

      iRetCode = POM_string_to_tag(pszInputString, &tRev);

		  fprintf(ugslog, "DEBUG:tag [%d]\n",tRev);

		  fprintf(ugslog, "run FEI whereused\n");

      if (strcmp(pszFilterType, "-") == 0)
      {
        BNL_rp_web_where_used(1, -2, NULL, pszRevRule, tRev, &pszReturnString, &pDocument);
      }
      else
      {
        BNL_rp_web_where_used(1, -2, pszFilterType, pszRevRule, tRev, &pszReturnString, &pDocument);
      }

      fprintf(ugslog,"come out BNL_rp_web_where_used\n [%s]\n", pszReturnString);

      *pszOutputString=MEM_alloc(sizeof(char) * ((int)strlen(pszReturnString)+1) );
		  strcpy(*pszOutputString, pszReturnString);

      fprintf(ugslog,"returns:[%s]", *pszOutputString);

      BNL_xml_close_document(pDocument);
      // JM|Gives error?
      //MEM_free(pszReturnString);
      free(pszReturnString);

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    case 18:

      fprintf(ugslog,"input [%s]\n",pszInputString);

      if ((pszRevRule=strchr(pszInputString,':')) != NULL)
      {
        *pszRevRule='\0';
        pszRevRule++;
      }

      fprintf(ugslog,"parsed [%s %s ]\n",pszInputString,pszRevRule);

      iRetCode=POM_string_to_tag(pszInputString,&tRev);

      iRetCode=BNL_rp_web_frulist_item(tRev,pszRevRule,&pszReturnString,&pDocument);

      *pszOutputString=MEM_alloc(sizeof(char) * ((int)strlen(pszReturnString)+1) );
		  strcpy(*pszOutputString,pszReturnString);

      BNL_xml_close_document(pDocument);

      //MEM_free(pszReturnString);
      free(pszReturnString);

      *decision = ONLY_CURRENT_CUSTOMIZATION;
    break;

    default:
      *decision = ALL_CUSTOMIZATIONS;

    break;
  }

  *tOutputTag = NULLTAG;
  *iOutputCount = 1;
  *ptOutputTagList = MEM_alloc( *iOutputCount * sizeof( tag_t ) );

  for ( i = 0; i < *iOutputCount; ++i )
  {
    (*ptOutputTagList)[i] = NULLTAG;
  }

  if (pszRuleName != NULL) MEM_free(pszRuleName);

	return iRetCode;
}

int BNL_register_se_feirep(int *decision, va_list args)
{
	int iRetCode         =ITK_ok;
    int *piArgTypes     =NULL;
    int iNumberOfArgs   =0;


	*decision = ALL_CUSTOMIZATIONS;

	/*register the server exit functions for start process*/
	iNumberOfArgs=4;
    //piArgTypes = (int *) SM_alloc_persistent( iNumberOfArgs * sizeof (int ) );
    piArgTypes = (int *) MEM_alloc( iNumberOfArgs * sizeof (int ) );
    piArgTypes[0] = USERARG_TAG_TYPE + USERARG_ARRAY_TYPE; //objects
    piArgTypes[1] = USERARG_STRING_TYPE; //process name
    piArgTypes[2] = USERARG_STRING_TYPE; //procedure
    piArgTypes[3] = USERARG_STRING_TYPE; //not used dummy


    USERSERVICE_register_method("BNL_us_start_process",
                                 BNL_us_start_process,
                                 iNumberOfArgs,
                                 piArgTypes,
                                 USERARG_INT_TYPE);


    return iRetCode;
}


int BNL_us_start_process(void *pReturnValue)
{
  int iRetCode    =0;
  int iTargets    =0;
  tag_t *ptTargets      =NULL;
  char *pszProcessname  =NULL;
  char *pszProcedure    =NULL;


  fprintf(ugslog,"In BNL_us_start_process\n");

  iRetCode=USERARG_get_tag_array_argument(&iTargets,&ptTargets);
  iRetCode=USERARG_get_string_argument(&pszProcessname);
  iRetCode=USERARG_get_string_argument(&pszProcedure); //procedure


  iRetCode=BNL_feirep_execute_process(pszProcedure,pszProcessname,iTargets,ptTargets);

  *( (int *) pReturnValue ) = iRetCode;

  MEM_free(ptTargets);
  MEM_free(pszProcessname);
  MEM_free(pszProcedure);

  return iRetCode;
}


int BNL_feirep_execute_process(char *pszProcedure,char *pszProcessName, int iTargetCount, tag_t *ptTargets)
{
    int iRetCode        =0;
    int iAttType        =1; //is target att type

    tag_t tFolder       =NULLTAG;
    tag_t tProcTemplate =NULLTAG;
    tag_t tProcess      =NULLTAG;

    int i   =0;

    char szClass[33];

    //create folder
    iRetCode=FL_create(pszProcessName,"Created for docdownload process",&tFolder);
    if (BNL_em_error_handler("FL_create",BNL_module,EMH_severity_error,iRetCode, true)) return iRetCode;


//debuging
    for (i=0;i<iTargetCount;i++)
    {
        BNL_tb_determine_super_class(ptTargets[i],szClass);
        fprintf(ugslog,"%s\n",szClass);
    }

    //put targets in it
    iRetCode=FL_insert_instances(tFolder,iTargetCount,ptTargets,0);
    if (BNL_em_error_handler("FL_insert_instances",BNL_module,EMH_severity_error,iRetCode, true)) return iRetCode;

    iRetCode=AOM_save(tFolder);
    if (BNL_em_error_handler("AOM_save",BNL_module,EMH_severity_error,iRetCode, true)) return iRetCode;

    iRetCode=EPM_find_template(pszProcedure,0,&tProcTemplate);
    if (BNL_em_error_handler("EPM_find_template",BNL_module,EMH_severity_error,iRetCode, true)) return iRetCode;

    //start process with name on folder
    iRetCode=EPM_create_process(pszProcessName,"autocreated by docdownload",tProcTemplate ,1,&tFolder,&iAttType,&tProcess);
    if (BNL_em_error_handler("EPM_create_process",BNL_module,EMH_severity_error,iRetCode, true))
    {
        int i       =0;
        int iFail   =0;
        //remove the folder
        for (i=0;i<iTargetCount;i++)
        {

          iFail=FL_remove(tFolder,ptTargets[i]);
          if (iFail!=0) fprintf(ugslog,"Error while remove the instance from the created process folder\n");

        }

        iFail=AOM_delete(tFolder);
        if (iFail!=0) fprintf(ugslog,"Error while deleting the process folder\n");


        return iRetCode;
    }


    return iRetCode;
}



/**
DLLAPI int BNL_exit_bnl_feirep(int *decision, va_list args)

  Description:

  Exit all modules.


*/
DLLAPI int BNL_exit_bnl_feirep(int *decision, va_list args)
{
	int iRetCode		=0;


	*decision = ALL_CUSTOMIZATIONS;

	BNL_exit_bnllibs();



	return iRetCode;

}

