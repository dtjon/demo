/**
 (C) copyright by Siemens PLM Software Benelux

 Developed for IMAN
 Versions     OS                          platform
 -----------------------------------------------------------------------------------------------------------
 5.x          Microsoft Windows NT        Intel


 Description    :   This handler performs the compliance check.


 Created for    :   FEI


 Comments       :   Every new procedure or function starts with BNL_


 Filename       :   $Id: BNL_RH_compliance_check.c 452 2016-06-28 09:03:36Z tjonkonj $


 Version info   :   The number consist of the iman release and then the release of the handler
                    <handlerrelease>.<pointrelease>


 History of changes
 Reason / description                            By                          Version   Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                        JM                          0.1       02-10-2009
 Added name argument                             JM                          0.2       30-10-2009
 Replaced deprecated CR functions                JM                          0.3       15-09-2011
 Fixed mem issue for 64 bit                      JM                          0.4       11-11-2011
 Update for TC 10.1.*                            D.Tjon                      0.5       31-05-2016

*/

/*BNL included files*/
#include <BNL_em_module.h>
#include <BNL_tb_module.h>
#include <BNL_fm_module.h>
#include <BNL_xml_module.h>
#include <BNL_errors.h>

#include <BNL_rp_module.h>
#include <BNL_fei_rep_parser.h>

/*Global declarations*/
extern FILE *ugslog;
extern int giErrorOffset;

EPM_decision_t BNL_RH_compliance_check(EPM_rule_message_t);
static char RH_Version[]="RH-compliance-check 0.5";

/**
int BNL_Register_RH_complicance_check(void)

    Description :

        Registers the callback rulehandler

    Returns     :

        ITK_ok or !ITK_ok

    Parameters  :

        None

*/
int BNL_Register_RH_complicance_check(void)
{
  int iRetCode     = ITK_ok;

  iRetCode = EPM_register_rule_handler("RH-FEI-compliance-check", "", BNL_RH_compliance_check);
  if (BNL_em_error_handler("EPM_register_rule_handler", RH_Version ,EMH_severity_user_error, iRetCode, false)) return iRetCode;

  fprintf(ugslog, "%s\n", RH_Version);

  return iRetCode;
}
/**
int BNL_RH_compliance_check()

    Description :

        This is rule handler performs the compiance check

    Returns     :

        Decision

    Parameters  :

        None
*/
EPM_decision_t BNL_RH_compliance_check(EPM_rule_message_t  msg)
{
  EPM_decision_t decision         = EPM_nogo;

  int iRetCode                    = ITK_ok;
  int iArgCount                   = 0;
  int i                           = 0;
  int iErrorCode                  = 0;
  int iTargetCount                = 0;
  int iRefCount                   = 0;
  int iComplianceResult           = 0;
  int iLen                        = 0;

  char *pszNogoArgument           = NULL;
  char *pszConfig                 = NULL;
  char *pszName                   = NULL;
  char *pszSetTaskResult          = NULL;
  char *pszDisplayError           = NULL;
  char *pszXMLOut                 = NULL;
  char *pszLogDir                 = getenv("TEMP");
  char *pszFileName               = NULL;
  char *pszHtmlFile               = NULL;

  logical lSetTaskResult          = false;

  tag_t tTriggerTask              = NULL_TAG;
  tag_t tRootTask                 = NULL_TAG;
  tag_t tJob                      = NULL_TAG;
  tag_t tDStype                   = NULL_TAG;
  tag_t tDataset                  = NULL_TAG;
  tag_t tImanFile                 = NULL_TAG;

  tag_t *ptTargets                = NULL;
  tag_t *ptRefs                   = NULL;

  EPM_action_t tAction            = NULL_TAG;

  char szOnNogoValue[WSO_name_size_c + 1];
  char szSelectArgument[SA_name_size_c + 1];
  char szMsg[257];
  char szName[WSO_name_size_c + 1];
  char szType[WSO_name_size_c + 1];

  t_XMLElement *pDocument         = NULL;

  FILE *fpXml			                = NULL;

  /*-----------------------------------------------------------------------*/

  fprintf(ugslog, "\n");
  fprintf(ugslog, "%s Start...\n",RH_Version);

  iRetCode = EPM_ask_job(msg.task, &tJob);
  if (BNL_em_error_handler("EPM_ask_job", RH_Version, EMH_severity_error, iRetCode, true))
  {
    decision = EPM_nogo;
    goto handler_exit;
  }

  iRetCode=  EPM_ask_root_task(tJob, &tRootTask);
  if (BNL_em_error_handler("EPM_ask_root_task", RH_Version, EMH_severity_error, iRetCode, true))
  {
    decision = EPM_nogo;
    goto handler_exit;
  }

  /* Get the arguments */
  strcpy(szSelectArgument, "on_nogo");
  iRetCode = BNL_tb_get_argument_optional(msg.arguments, msg.task, szSelectArgument, true, &pszNogoArgument);
  if (BNL_em_error_handler("BNL_tb_get_argument_optional",RH_Version,EMH_severity_warning, iRetCode, true))
  {
    decision = EPM_nogo;
    goto handler_exit;
  }

  if (pszNogoArgument != NULL)
  {
    /*
      -on_nogo=SKIP  as usual the trigger on roottask
      -on_nogo=roottask:SKIP  trigger at roottask
      -on_nogo=owntask:SKIP trigger on the task itself
    */

    char *pszNode = NULL;

    if ((pszNode=strchr(pszNogoArgument,':')) != NULL)
    {
      *pszNode='\0';
      pszNode++;

      strcpy(szOnNogoValue, pszNode);

      if (strcmp(pszNogoArgument, "roottask")==0)
      {
        tTriggerTask = tRootTask;
      }
      else if (strcmp(pszNogoArgument, "owntask")==0)
      {
        tTriggerTask = msg.task;
      }
      else
      {
        iRetCode = EPM_ask_sub_task(tRootTask, pszNogoArgument, &tTriggerTask);
        if (BNL_em_error_handler("BNL_translate_action_string", RH_Version,EMH_severity_error, iRetCode, true))
        {
          decision = EPM_nogo;
          goto handler_exit;
        }
      }

      iRetCode = BNL_tb_translate_action_string(pszNode, &tAction);
      if (BNL_em_error_handler("BNL_translate_action_string", RH_Version, EMH_severity_error, iRetCode, true))
      {
        decision=EPM_nogo;
        goto handler_exit;
      }
    }
    else
    {
      /* No preset task then used the root task, as it worked always */
      strcpy(szOnNogoValue, pszNogoArgument);

      iRetCode = BNL_tb_translate_action_string(pszNogoArgument, &tAction);
      if (BNL_em_error_handler("BNL_translate_action_string", RH_Version, EMH_severity_error, iRetCode, true))
      {
        decision = EPM_nogo;
        goto handler_exit;
      }
      tTriggerTask = tRootTask;
    }
  }

  strcpy(szSelectArgument, "config");
  iRetCode = BNL_tb_get_argument_optional(msg.arguments, msg.task, szSelectArgument, false, &pszConfig);
  if (BNL_em_error_handler("BNL_tb_get_argument_optional", RH_Version, EMH_severity_error, iRetCode, true))
  {
    decision = EPM_nogo;
    goto handler_exit;
  }

  strcpy(szSelectArgument, "name");
  iRetCode = BNL_tb_get_argument_optional(msg.arguments, msg.task, szSelectArgument, false, &pszName);
  if (BNL_em_error_handler("BNL_tb_get_argument_optional", RH_Version, EMH_severity_error, iRetCode, true))
  {
    decision = EPM_nogo;
    goto handler_exit;
  }

  strcpy(szSelectArgument,"settaskresult");
  iRetCode = BNL_tb_get_argument_optional(msg.arguments, msg.task, szSelectArgument, true, &pszSetTaskResult);
  if (BNL_em_error_handler("BNL_tb_get_argument_optional", RH_Version, EMH_severity_error, iRetCode, true))
  {
    decision = EPM_nogo;
    goto handler_exit;
  }
  if (pszSetTaskResult != NULL)
  {
    if (tolower(pszSetTaskResult[0]) == 't')
      lSetTaskResult = true;
    BNL_mm_free((void*)pszSetTaskResult);
  }

  // Get the targets
  iRetCode = EPM_ask_attachments(tRootTask, EPM_target_attachment, &iTargetCount, &ptTargets);
  if (BNL_em_error_handler("EPM_ask_attachments", RH_Version, EMH_severity_error, iRetCode, true))
  {
    decision = EPM_nogo;
    goto handler_exit;
  }

  fprintf(ugslog, "Found [%d] target objects.\n", iTargetCount);

  // Perform the report
  if ((iRetCode = BNL_rp_web_comprep(iTargetCount, ptTargets, 1, pszConfig, &pszXMLOut, &pDocument, &iComplianceResult)) != 0)
  {
    decision = EPM_nogo;
    goto handler_exit;
  }

  if (lSetTaskResult)
  {
    // Add the report as reference
    pszFileName = USER_new_file_name("fei", "report", "html", 1);
    BNL_fm_open_file(&fpXml, pszLogDir, pszFileName, "w+t", 1);
    BNL_fei_rep_parse_html(fpXml, pDocument, COMPREP, 0, pszConfig, NULL, NULL);
    BNL_fm_close_file(fpXml);

    iLen = (int)strlen(pszLogDir) + (int)strlen(pszFileName) + 2;
    pszHtmlFile =(char *)malloc(sizeof(char) * iLen);
    sprintf(pszHtmlFile, "%s%s%s", pszLogDir, FILESEP, pszFileName);

    fprintf(ugslog, "DEBUG: File [%s\\%s] has been created.\n", pszLogDir, pszFileName);

    iRetCode = AE_find_datasettype("HTML", &tDStype);
    if (BNL_em_error_handler("AE_find_datasettype", RH_Version, EMH_severity_error, iRetCode, true))
    {
      decision = EPM_nogo;
      goto handler_exit;
    }

    // Import the file
    if ((iRetCode = BNL_tb_import_file(tDStype, pszHtmlFile, pszName, "HTML", &tImanFile) != 0))
    {
      decision = EPM_nogo;
      goto handler_exit;
    }

    // Get current reference objects
    //iRetCode = CR_ask_reference_objects(tJob, &iRefCount, &ptRefs);
    iRetCode = EPM_ask_attachments(tRootTask, EPM_reference_attachment, &iRefCount, &ptRefs);
    if (BNL_em_error_handler("EPM_ask_attachments", RH_Version, EMH_severity_error, iRetCode, true))
    {
      decision = EPM_nogo;
      goto handler_exit;
    }

    fprintf(ugslog, "Found [%d] reference objects.\n", iRefCount);

    // Check if dataset with name is there
    for (i = 0; i < iRefCount; i++)
    {
      // Check type and name
      iRetCode = WSOM_ask_object_type(ptRefs[i], szType);
      if (BNL_em_error_handler("WSOM_ask_object_type", RH_Version, EMH_severity_error, iRetCode, true))
      {
        decision = EPM_nogo;
        goto handler_exit;
      }

      fprintf(ugslog, "Type [%s].\n", szType);

      if (_stricmp(szType, "HTML") == 0)
      {
        iRetCode = WSOM_ask_name(ptRefs[i], szName);
        if (BNL_em_error_handler("WSOM_ask_name", RH_Version, EMH_severity_error, iRetCode, true))
        {
          decision = EPM_nogo;
          goto handler_exit;
        }

        fprintf(ugslog, "Name [%s].\n", szName);

        if (_stricmp(szName, pszName) == 0)
        {
          fprintf(ugslog, "Dataset of type [%s] and with name [%s] has been found.\n", szType, szName);
          tDataset = ptRefs[i];
          break;
        } // End of if (_stricmp(szName, pszName) == 0)
      } // End of if (_stricmp(szType, "HTML") == 0)
    } // End of for (i = 0; i < iRefCount; i++)

    if (tDataset != NULL_TAG)
    {
      tag_t tRevAnch        = NULL_TAG;
      tag_t tRevisedDs        = NULL_TAG;


      // Revise the existing dataset
      iRetCode = AOM_refresh(tDataset, true);
      if (BNL_em_error_handler("AOM_refresh", RH_Version, EMH_severity_error, iRetCode, true))
      {
        decision = EPM_nogo;
        goto handler_exit;
      }

      iRetCode = AE_ask_dataset_anchor(tDataset, &tRevAnch);
      if (BNL_em_error_handler("AE_ask_dataset_anchor", RH_Version, EMH_severity_error, iRetCode, true))
      {
        decision = EPM_nogo;
        goto handler_exit;
      }

      iRetCode = AE_ask_anchor_revision(tRevAnch, 0, &tRevisedDs);
      if (BNL_em_error_handler("AE_ask_anchor_revision", RH_Version, EMH_severity_error, iRetCode, true))
      {
        decision = EPM_nogo;
        goto handler_exit;
      }

      // Remove existing named reference
      iRetCode = AE_remove_dataset_named_ref(tRevisedDs, "HTML");
      if (BNL_em_error_handler("AE_remove_dataset_named_ref", RH_Version, EMH_severity_error, iRetCode, true))
      {
        decision = EPM_nogo;
        goto handler_exit;
      }

      // Add new named reference
      iRetCode = AE_add_dataset_named_ref(tRevisedDs, "HTML", AE_PART_OF, tImanFile);
      if (BNL_em_error_handler("AE_add_dataset_named_ref", RH_Version, EMH_severity_error, iRetCode, true))
      {
        decision = EPM_nogo;
        goto handler_exit;
      }

      iRetCode = AOM_save(tRevisedDs);
      if (BNL_em_error_handler("AOM_save", RH_Version, EMH_severity_error, iRetCode, true))
      {
        decision = EPM_nogo;
        goto handler_exit;
      }

      iRetCode = AOM_unlock(tRevisedDs);
      if (BNL_em_error_handler("AOM_unlock", RH_Version, EMH_severity_error, iRetCode, true))
      {
        decision = EPM_nogo;
        goto handler_exit;
      }
    }
    else
    {
      if ((iRetCode = BNL_tb_create_dataset(pszName, pszName, tDStype, &tDataset)) != 0)
      {
        decision = EPM_nogo;
        goto handler_exit;
      }

      iRetCode = AE_add_dataset_named_ref(tDataset, "HTML", AE_PART_OF, tImanFile);
      if (BNL_em_error_handler("AE_add_dataset_named_ref", RH_Version, EMH_severity_error, iRetCode, true))
      {
        decision = EPM_nogo;
        goto handler_exit;
      }

      iRetCode = AOM_save(tDataset);
      if (BNL_em_error_handler("AOM_save", RH_Version, EMH_severity_error, iRetCode, true))
      {
        decision = EPM_nogo;
        goto handler_exit;
      }

      //iRetCode = CR_add_reference_objects(tJob, 1, &tDataset);
      //if (BNL_em_error_handler("CR_add_reference_objects", RH_Version, EMH_severity_error, iRetCode, true))
      if ((iRetCode = BNL_tb_add_objects_to_task(tRootTask, 1, &tDataset, EPM_reference_attachment)) != 0)
      {
        decision = EPM_nogo;
        goto handler_exit;
      }
    } // End of Else if (tDataset != NULL_TAG)
  } // End of if (lSetTaskResult)

  if (iComplianceResult == 1)
  {
    decision = EPM_nogo;

    sprintf(szMsg, "Compliance check failed.");
    fprintf(ugslog, "%s: %s\n", RH_Version, szMsg);

    if (!lSetTaskResult)
    {
      iErrorCode = BNL_error_message;
      EMH_store_error_s1(EMH_severity_error, iErrorCode, szMsg);
    }
  }
  else
  {
    decision = EPM_go;
    fprintf(ugslog, "%s: Compliance checks passed.\n", RH_Version);
  }

  /**
   This label will be called on errors or if decided to exit the handler in anyway.
   beneath this label the memory will be freed in a save way and logfile messages will
   be logical closed.
  */
handler_exit:

  /*
    The trigger action put the job in a given state depeninding on arguments
    this following the BNL_handler conventions
  */
  if (decision == EPM_nogo)
  {
    if (lSetTaskResult)
    {
      decision = EPM_go;

      fprintf(ugslog, "%s: Setting the task result to false. \n", RH_Version);
      iRetCode = EPM_set_condition_task_result(msg.task, 0);
      BNL_em_error_handler("EPM_set_condition_task_result", RH_Version, EMH_severity_error, iRetCode, true);
    }
    else
    {
      if (tAction != NULL_TAG)
      {
        fprintf(ugslog, "%s: Send action %s to task.\n", RH_Version, szOnNogoValue);
        iRetCode = EPM_trigger_action(tTriggerTask, tAction, " ");
        BNL_em_error_handler("EPM_trigger_action",RH_Version,EMH_severity_error, iRetCode, true);
      }
      fprintf(ugslog, "%s: Send a nogo.\n", RH_Version);
    }
  }
  else if (decision == EPM_go)
  {
    if(lSetTaskResult)
    {
      fprintf(ugslog, "%s: Setting the task result to true.\n", RH_Version);
      iRetCode = EPM_set_condition_task_result(msg.task, 1);
      BNL_em_error_handler("EPM_set_condition_task_result",RH_Version,EMH_severity_error, iRetCode, true);
    }
  }

  /* Free memory */
  if (ptTargets != NULL) MEM_free(ptTargets);
  if (ptRefs != NULL) MEM_free(ptRefs);

  if (pszNogoArgument != NULL) MEM_free(pszNogoArgument);
  if (pszConfig != NULL) MEM_free(pszConfig);
  if (pszName != NULL) MEM_free(pszName);

  if (pszHtmlFile != NULL) free(pszHtmlFile);

  // Free xml struc
  BNL_xml_close_document(pDocument);

  if (pszXMLOut != NULL) free(pszXMLOut);
  //if (pszXMLOut != NULL) MEM_free(pszXMLOut);

  fprintf(ugslog,"%s: End.\n", RH_Version);
  fprintf(ugslog,"\n");

  return decision;
} /* End of BNL_RH_compliance_check */
