/**
 (C) copyright by Siemens PLM Software Benelux

 Developed for IMAN 
 Versions     OS                          platform
 -----------------------------------------------------------------------------------------------------------
 5.x          Microsoft Windows NT        Intel 
 

 

 Description    :   Header file belonging to the rule handler template 

 Created for    :   Siemens PLM Software BENELUX

 Comments       :   Every new procedure or function starts with BNL_
                    
 Filename       :   BNL_RH_compliance_check.h

 Version info   :   The number consist of the iman release and then the release of the handler
                    <handlerrelease>.<pointrelease>

                    
 History of changes                                         
 Reason /description                By                      Version release   date
 ----------------------------------------------------------------------------------------------------------
 Creation                           JM                      0.1               02-12-2009

 */

/**
int BNL_Register_RH_complicance_check(void)

    Description :

        Registers the callback rulehandler

    Returns     :

        ITK_ok or !ITK_ok

    Parameters  :

        None

*/
extern int BNL_Register_RH_complicance_check(void);

