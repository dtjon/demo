/*
#======================================
# Copyright Siemens PLM Software Benelux
#
# 18-09-2003   Erik Navis     0.0 Creation
# 07-03-2005   Erik Navis     0.1 Make it compatible with tceng 9.x
# 14-09-2006   J. Mansvelders 0.2 Fix the problem when 2 ItemRevisions have been selected (see IR 1545399)
# 07-02-2007   Erik Navis     0.3 Added WU option.
# 02-10-2007   J. Mansvelders 0.4 Make it compatible with TcEng 2005/2007
# 13-aug-2008  J. Mansvelders 0.5 Added ebom reports
# 10-Jun-2009  J. Wijnja      0.6 Added Bom report extended
# 11-Jun-2009  J. Wijnja      0.7 Added attrdiff report
# 12-Jun-2009  J. Mansvelders 0.8 Updated for WhereUsed report
# 15-Jun-2009  J. Wijnja      0.9 Added product attributes report
# 17-Jun-2009  J. Wijnja      0.9 Added successor/predecessor report
# 03-Aug-2009  J. Mansvelders 1.0 Accept also Items for successor/predecessor report
# 04-Aug-2009  J. Mansvelders 1.1 Added CR_extended
# 05-Oct-2009  J. Mansvelders 1.2 Added handle_comprep_dialog
# 12-Dec-2009  J. Mansvelders 1.3 Updated for handle_wu_dialog
# 01-Jan-2010  J. Mansvelders 1.4 Fixed issue with Bom report extended
# 22-Feb-2010  J. Mansvelders 1.5  Added handle_wu_top_dialog
# 25-Mar-2010  J. Wijnja      1.6 Added handle_frulist_item_dialog
# 16-Sep-2011  J. Mansvelders 1.7 Updated for TC 8.3.*
#====================================
*/

//get the slections from workspace and start the appropiate report page

function FEI_getselections(value)
{
  var selections = selection();

  if ( selections.length == 0 || selections.length > 2  )
  {
    alert( "You may only select two or one ItemRevisions" );
    return;
  }

  for (i=0;i<selections.length;i++)
  {
    if (selections[i].iman_class!='ItemRevision' && selections[i].iman_class!='F3I_ChangeNoticeRevision')
    {
      alert( "The selected lines should be an ItemRevision or FEI ChangeNotice Revision" );
      return;
    }
  }

  switch (value)
  {
    case 'CDL':

      if (selections.length==1)
      {
        //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_cdl.html";
        this.url = "/tc/webclient?TC_file=common/fei/fei_report_cdl.html";
        this.url += "&tprod=" + selections[0].iman_tag + "&tpred=" + NULLTAG;
      }
      else
      {
        //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_cdl.html";
        this.url = "/tc/webclient?TC_file=common/fei/fei_report_cdl.html";
        // JM|15-09-2006 selections[0] is always the predecessor (this is independent of the selection sequence)
        this.url += "&tprod=" + selections[1].iman_tag + "&tpred=" + selections[0].iman_tag;
       //this.url += "&tprod=" + selections[0].iman_tag + "&tpred=" + selections[1].iman_tag;
      }

      window.open(this.url)

    break;
    
    case 'CATTR':
    
      if (selections.length==1)
      {
        //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_cattr.html";
        this.url = "/tc/webclient?TC_file=common/fei/fei_report_cattr.html";
        this.url += "&tprod=" + selections[0].iman_tag + "&tpred=" + NULLTAG;
      }
      else
      {
        //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_cattr.html";
        this.url = "/tc/webclient?TC_file=common/fei/fei_report_cattr.html";
        // JM|15-09-2006 selections[0] is always the predecessor (this is independent of the selection sequence)
        this.url += "&tprod=" + selections[1].iman_tag + "&tpred=" + selections[0].iman_tag;
       //this.url += "&tprod=" + selections[0].iman_tag + "&tpred=" + selections[1].iman_tag;
      }
    
      window.open(this.url)
    
    break;

    case 'CBL':

      if (selections.length==1)
      {
        //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_cbl.html";
        this.url = "/tc/webclient?TC_file=common/fei/fei_report_cbl.html";
        this.url += "&tprod=" + selections[0].iman_tag + "&tpred=" + NULLTAG;
      }
      else
      {
        //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_cbl.html";
        this.url = "/tc/webclient?TC_file=common/fei/fei_report_cbl.html";
        // JM|15-09-2006 selections[0] is always the predecessor (this is independent of the selection sequence)
        this.url += "&tprod=" + selections[1].iman_tag + "&tpred=" + selections[0].iman_tag;
        //this.url += "&tprod=" + selections[0].iman_tag + "&tpred=" + selections[1].iman_tag;
      }
      window.open(this.url);

    break;

    case 'PDL':

      //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_pdl.html";\
      this.url = "/tc/webclient?TC_file=common/fei/fei_report_pdl.html";
      this.url += "&tprod=" + selections[0].iman_tag;
      window.open(this.url);

    break;

    case 'CR':

      //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_cr.html";
      this.url = "/tc/webclient?TC_file=common/fei/fei_report_cr.html";
      this.url += "&tprod=" + selections[0].iman_tag;
      window.open(this.url);

    break;
    
    case 'CR_extended':
    
          this.url = "/tc/webclient?TC_file=common/fei/fei_report_cr_extended.html";
          this.url += "&tprod=" + selections[0].iman_tag;
          window.open(this.url);
    
    break;

    case 'BOMREP':

       //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_bomrep.html";
       this.url = "/tc/webclient?TC_file=common/fei/fei_report_bomrep.html";
       this.url += "&tprod=" + selections[0].iman_tag;

       window.open(this.url);

    break;

    //case 'WU':

      //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_wu.html";
      //this.url = "/tc/webclient?IMAN_file=fei/fei_report_wu.html";
      //this.url += "&tprod=" + selections[0].iman_tag;

      //window.open(this.url);

    //break;

    default:

      alert('Not implemented yet');

    break;
  }
}

function FEI_getselection(value)
{
  var selections = selection();

  if ( selections.length == 0 || selections.length > 1  )
  {
    if (value == 'SUCCPRED')
    {
      alert( "You may only select one Item/ItemRevision" );
    }
    else
    {
      alert( "You may only select one ItemRevision" );
    }
    return;
  }

  for (i=0;i<selections.length;i++)
  {
    if (value == 'SUCCPRED')
    {
      if (selections[i].iman_class!='ItemRevision' && selections[i].iman_class!='Item')
      {
        alert( "The selected line should be an Item or an ItemRevision" );
        return;
      }
    }
    else
    {
      if (selections[i].iman_class!='ItemRevision')
      {
        alert( "The selected line should be an ItemRevision" );
        return;
      }
    }
  }

  switch (value)
  {
    case 'ATTR':
    
      this.url = "/tc/webclient?TC_file=common/fei/fei_report_prod_attr.html";
      this.url += "&tprod=" + selections[0].iman_tag;
      
      window.open(this.url)
    
    break;
    
    case 'SUCCPRED':
        
          this.url = "/tc/webclient?TC_file=common/fei/fei_report_succ_pred.html";
          this.url += "&tprod=" + selections[0].iman_tag;
          
          window.open(this.url)
        
    break;

    default:

      alert('Not implemented yet');

    break;
  }
}

function handle_bomrep_dialog(dialog)
{
   popdown(dialog);

   var selections = selection();

   /*get the value from the dialog fields*/

   //                                         th            tb               tr            td label   td     input
   var cellRevRule=dialog.table.firstChild.firstChild.firstChild.nextSibling.firstChild
   var cellEffDate=dialog.table.firstChild.firstChild.nextSibling.firstChild.nextSibling.firstChild
   var cellLevel=  dialog.table.firstChild.firstChild.nextSibling.nextSibling.firstChild.nextSibling.firstChild

   //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_bomrep.html";
   this.url = "/tc/webclient?TC_file=common/fei/fei_report_bomrep.html";
   this.url += "&tprod=" + selections[0].iman_tag;
   this.url += "&revrule=" + cellRevRule.value;
   this.url += "&effdate=" + cellEffDate.value;
   this.url += "&level=" + cellLevel.value;

   window.open(this.url);
}

function handle_wu_dialog(dialog)
{
   popdown(dialog);

   var selections = selection();

   /* Get the value from the dialog fields */

   //                                         th            tb               tr            td label   td     input
   var cellLevel = dialog.table.firstChild.firstChild.firstChild.nextSibling.firstChild
   var cellItemType = dialog.table.firstChild.firstChild.nextSibling.firstChild.nextSibling.firstChild

   this.url = "/tc/webclient?TC_file=common/fei/fei_report_wu.html";
   this.url += "&tprod=" + selections[0].iman_tag;
   
   if (cellLevel.value == "")
   {
   this.url += "&level=1";
   }
   else
   {
     this.url += "&level=" + cellLevel.value;
   }
   this.url += "&itemtype=" + cellItemType.value;

   window.open(this.url);
}

function handle_wu_top_dialog(dialog)
{
   popdown(dialog);

   var selections = selection();

   /* Get the value from the dialog fields */

   //                                         th            tb               tr            td label   td     input
   var cellItemType = dialog.table.firstChild.firstChild.firstChild.nextSibling.firstChild

   this.url = "/tc/webclient?TC_file=common/fei/fei_report_wu_top.html";
   this.url += "&tprod=" + selections[0].iman_tag;
   
   this.url += "&level=-2&itemtype=" + cellItemType.value;

   window.open(this.url);
}

function handle_bomrep_ext_dialog(dialog)
{
   popdown(dialog);

   var selections = selection();

   /*get the value from the dialog fields*/

   //                                         th            tb               tr            td label   td     input
   var cellRevRule=dialog.table.firstChild.firstChild.firstChild.nextSibling.firstChild
   var cellEffDate=dialog.table.firstChild.firstChild.nextSibling.firstChild.nextSibling.firstChild
   var cellLevel=  dialog.table.firstChild.firstChild.nextSibling.nextSibling.firstChild.nextSibling.firstChild

   //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_bomrep_ext.html";
   this.url = "/tc/webclient?TC_file=common/fei/fei_report_bomrep_ext.html";
   this.url += "&tprod=" + selections[0].iman_tag;
   this.url += "&revrule=" + cellRevRule.value;
   this.url += "&effdate=" + cellEffDate.value;
   this.url += "&level=" + cellLevel.value;

   window.open(this.url);
}

function handle_bomrep_mfgpro_dialog(dialog)
{
   popdown(dialog);

   var selections = selection();

   /*get the value from the dialog fields*/

   //                                         th            tb               tr            td label   td     input
   var cellRevRule=dialog.table.firstChild.firstChild.firstChild.nextSibling.firstChild
   var cellEffDate=dialog.table.firstChild.firstChild.nextSibling.firstChild.nextSibling.firstChild
   var cellLevel=  dialog.table.firstChild.firstChild.nextSibling.nextSibling.firstChild.nextSibling.firstChild

   this.url = "/tc/webclient?TC_file=common/fei/fei_report_bomrep_mfgpro.html";
   this.url += "&tprod=" + selections[0].iman_tag;
   this.url += "&revrule=" + cellRevRule.value;
   this.url += "&effdate=" + cellEffDate.value;
   this.url += "&level=" + cellLevel.value;

   window.open(this.url);
}

function handle_comprep_dialog(dialog)
{
   popdown(dialog);

   var selections = selection();

   /*get the value from the dialog fields*/

   //                                         th            tb               tr            td label   td     input
   var cellValCheck=dialog.table.firstChild.firstChild.firstChild.nextSibling.firstChild

   this.url = "/tc/webclient?TC_file=common/fei/fei_report_comprep.html";
   this.url += "&tprod=" + selections[0].iman_tag;
   this.url += "&checkvalue=" + cellValCheck.value;

   window.open(this.url);
}

function handle_frulist_item_dialog(dialog)
{
   popdown(dialog);
   
   var selections = selection();

   /*get the value from the dialog fields*/

   //                                         th            tb               tr            td label   td     input
   var cellRevRule=dialog.table.firstChild.firstChild.firstChild.nextSibling.firstChild

   this.url = "/tc/webclient?TC_file=common/fei/fei_report_frulist_item.html";
   this.url += "&tprod=" + selections[0].iman_tag;
   this.url += "&revrule=" + cellRevRule.value;

   window.open(this.url);
}

function handle_ebompackedrep_dialog(dialog)
{
   popdown(dialog);

   var selections = selection();

   /*get the value from the dialog fields*/

   //                                         th            tb               tr            td label   td     input
   var cellRevRule=dialog.table.firstChild.firstChild.firstChild.nextSibling.firstChild
   var cellEffDate=dialog.table.firstChild.firstChild.nextSibling.firstChild.nextSibling.firstChild
   var cellLevel=  dialog.table.firstChild.firstChild.nextSibling.nextSibling.firstChild.nextSibling.firstChild

   //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_bomrep.html";
   this.url = "/tc/webclient?TC_file=common/fei/fei_report_ebompackedrep.html";
   this.url += "&tprod=" + selections[0].iman_tag;
   this.url += "&revrule=" + cellRevRule.value;
   this.url += "&effdate=" + cellEffDate.value;
   this.url += "&level=" + cellLevel.value;

   window.open(this.url);
}

function handle_ebomrep_dialog(dialog)
{
   popdown(dialog);

   var selections = selection();

   /*get the value from the dialog fields*/

   //                                         th            tb               tr            td label   td     input
   var cellRevRule=dialog.table.firstChild.firstChild.firstChild.nextSibling.firstChild
   var cellEffDate=dialog.table.firstChild.firstChild.nextSibling.firstChild.nextSibling.firstChild
   var cellLevel=  dialog.table.firstChild.firstChild.nextSibling.nextSibling.firstChild.nextSibling.firstChild

   //this.url = "/cgi-bin/iman?IMAN_file=fei/fei_report_bomrep.html";
   this.url = "/tc/webclient?TC_file=common/fei/fei_report_ebomrep.html";
   this.url += "&tprod=" + selections[0].iman_tag;
   this.url += "&revrule=" + cellRevRule.value;
   this.url += "&effdate=" + cellEffDate.value;
   this.url += "&level=" + cellLevel.value;

   window.open(this.url);
}