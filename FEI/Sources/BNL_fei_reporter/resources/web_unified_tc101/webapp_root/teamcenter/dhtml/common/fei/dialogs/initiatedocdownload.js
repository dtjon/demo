/*
#======================================
# Copyright UGS Benlux
# 
# 07-feb-2007  Erik Navis     0.0  Creation
# 02-Oct-2007  J. Mansvelders 0.1  Make it compatible with TcEng 2005/2007
# 16-Sep-2011  J. Mansvelders 0.2  Updated for TC 8.3.*
# 
# 
#====================================
*/
function isAllowedType(pAllowedTypes,pObject)
{
   var iAllowed=0;
   
   pszType = new String(pObject.iman_type);
   pszType=pszType.replace("%20"," ");
   
  
	   for (a=0;a<pAllowedTypes.length;a++)
	   {
	   
	      if (pszType==pAllowedTypes[a]) 
          {
		    iAllowed=1;
			break;
          }		  
		  
	   }
	   
	   return iAllowed;
}

function initiateDocDownload(pszProcessName,pAllowedTypes)
{	
    //check selection on type
	var selects=selection();
	var iChecked=1;
	
	for (s=0;s<selects.length;s++)
	{
	   
	   if (!isAllowedType(pAllowedTypes,selects[s]))
       {	   
	       iChecked=0;
		   alert("One of the selected types is not an allowed type");
		   break;
	   }

	}
	
	if (iChecked)
	{
      doRequest( new initiateDocDownloadRequest( selection(), pszProcessName ) );
	}
}

function initiateDocDownloadRequest( rows, pszProcessName )
{
	
    //this.url = "/cgi-bin/iman?IMAN_file=fei/dialogs/initiatedocdownloadform.xml";
    this.url = "/tc/webclient?TC_file=common/fei/dialogs/initiatedocdownloadform.xml";
	
    for( var n = 0; n < rows.length; n++  )
    {
        this.url += "&targets[0]=" + rows[n].iman_tag;
    }
	
	this.url += "&pszProcessName=" + pszProcessName;
    this.callback = initiateDocDownloadCallback;
}

function initiateDocDownloadCallback( documentNode )
{

    var dialog = createActionDialog( documentNode,
                                     "common/actions/initiatedocdownload.xml",
                                     true,
                                     false );
    
    dialog.popup();
}
