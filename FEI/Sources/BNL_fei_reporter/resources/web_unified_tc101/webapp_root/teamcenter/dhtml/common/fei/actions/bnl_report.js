/* 
#======================================
# Copyright Siemens PLM Software Benelux
# 
# 18-sept-2003  Erik Navis     0.0  creation
# 13-oct-2003   Erik Navis     0.1  When no document undefined was displayed
# 28-oct-2003   Erik Navis     0.2  If UOM and Quantity null then quantity is 1
# 31-oct-2003   Erik Navis     0.3  Quantity was set always to one, even when quantity was filled
# 06-may-2004   Erik Navis     0.4  Quantity of predecessor is set to one if empty.
# 16-feb-2007   Erik Navis     0.5  Added where used parsing.
# 19-mar-2007   Erik Navis     0.6  Removed table split in where used report.
# 11-jun-2009   Jale Wijnja    0.7  Added attrdiff report
# 12-jun-2009   J. Mansvelders 0.8  Updated create_whereused_rep function and added function export_to_excel
# 03-Aug-2009   J. Mansvelders 0.9  Add heading information to table
# 04-Aug-2009   J. Mansvelders 1.0  Add attribute changes to the change report
# 04-Aug-2009   J. Mansvelders 1.1  Update create_eco_report and report_comps
# 06-Oct-2009   J. Mansvelders 1.2  Added create_comp_report
# 30-Oct-2009   J. Mansvelders 1.3  Updated create_comp_report
# 01-Dec-2009   J. Mansvelders 1.4  Use xml instead nodeValue
# 12-Oct-2009   J. Mansvelders 1.5  Updated create_whereused_rep
# 22-Feb-2010   J. Mansvelders 1.6  Added create_whereused_top_rep
# 01-Mar-2010   J. Mansvelders 1.7  Updated for compliance report
# 16-Sep-2011   J. Mansvelders 1.8  Updated for TC 8.3.*
# 02-Feb-2012   J. Mansvelders 1.9  Updated for upm equals each
#====================================

*/


function get_product_props(document)
{

   product=document.getElementsByTagName("product");

   theNode=product.item(0);   

   for(var i=0;i<theNode.childNodes.length;i++)
   {
   
     if (theNode.childNodes(i).nodeName=='props')
     {
       theProps=theNode.childNodes(i);
          
     }
   }
   
   return theProps

}

function get_predecessor_props(document)
{

   predecessor=document.getElementsByTagName("predecessor");

   theNode=predecessor.item(0);   

   for(var i=0;i<theNode.childNodes.length;i++)
   {
     if (theNode.childNodes(i).nodeName=='props')
     {
       theProps=theNode.childNodes(i);
       
          
     }
   }
   
   return theProps

}

function get_collection_element(aNode,szElement)
{
 var collection=null;
    for (i=0;i<aNode.childNodes.length;i++)
  {
    if (aNode.childNodes(i).nodeName==szElement)
    {
       collection=aNode.childNodes(i);
     break;
    }
  }
  
  return collection;
}

function get_collection(aNode,szElement)
{
 var collection=null;
    for (i=0;i<aNode.childNodes.length;i++)
  {
    if (aNode.childNodes(i).nodeName==szElement)
    {
       collection=aNode.childNodes(i).childNodes;
     break;
    }
  }
  
  return collection;
}

function get_wu_eco_props(whereusedelement)
{
   var eco=null;
   var theProps=null;
   
   for (var wu=0;wu<whereusedelement.childNodes.length;wu++)
   {
        
        if (whereusedelement.childNodes(wu).nodeName=='ecos')
    {
       ecos=whereusedelement.childNodes(wu);
       
       if (ecos.hasChildNodes)
       {
          
          eco=ecos.childNodes(0);
        break;
       }
    }
   
   }
   
   if (eco!=null)
   {
      
       for(var i=0;i<eco.childNodes.length;i++)
     {
       
       if (eco.childNodes(i).nodeName=='props')
       {
         theProps=eco.childNodes(i);
       
       break;
            
       }
     }
   }
   
  
   return theProps;

}

function get_diff_props(theDiff)
{
  


   for (var i=0;i<theDiff.childNodes.length;i++)
   {
     
     if (theDiff.childNodes(i).nodeName=='props')
     {
      
       var theProps=theDiff.childNodes(i);
         
     }
   }

  return theProps
}

function check_error(document)
{

   error=document.getElementsByTagName("error");
   
   if (error.length > 0)
   {
     alert(error.item(0).firstChild.nodeValue);
     return 1;
   }
   else
   {
     return 0;
   }
  
}

function compose_table_head(theProps)
{
   var props=theProps.childNodes;
   
   /*header*/
   for (var i=0;i<props.length;i++)
   { 
     
     if (props(i).nodeName=="prop")
     {
       pszAttr=props(i).attributes.getNamedItem("colname");
       document.write('<td><B>');
         if (pszAttr!=null) document.writeln(pszAttr.value); 
       document.writeln('</B></td>');
     }
   }
  
}

function get_table_format(theProps)
{
   var props=theProps.childNodes;
   var szFormat=' '; //default empty


   /*header*/
   for (var i=0;i<props.length;i++)
   {   
     var node=props(i).firstChild;
     if (props(i).nodeName=='table')
     {
       
       szFormat=node.nodeValue;
       
     }
   }
  
  return szFormat;
}

function get_uom(theProps)
{
   var props=theProps.childNodes;
   var uom=null;
   /*header*/
   for (var i=0;i<props.length;i++)
   {   
       var node=props(i).firstChild;
       
       if (props(i).nodeName=='prop')
       {
         pszAttr=props(i).attributes.getNamedItem("attr");
         
         if (pszAttr.value=="bl_item_uom_tag" )
         {
          if (node!=null) uom=node.nodeValue;
         }
       }
    
   }
   
   return uom;
  
}

var nopredec=false;

function compose_propval_line(theProps)
{
   var props=theProps.childNodes;
   var isPred=false;
  
   
   
   //check if its a predecessor
   var pParent=theProps.parentNode;
   if (pParent.nodeName=='predecessor')
   {
       isPred=true;
   }


   /*header*/
   for (var i=0;i<props.length;i++)
   {   
       var node=props(i).firstChild;
       
       if (props(i).nodeName=='prop')
       {

         var uom=get_uom(theProps);
         pszAttr=props(i).attributes.getNamedItem("attr");

         if (pszAttr.value=='id' && isPred)
         {
           if (node==null) nopredec=true;
           
         } 

         if (pszAttr.value=='bl_quantity' && uom!=null && uom=='each' && node==null)
         {
          document.writeln('<td>');
            document.writeln('1'); 
          document.writeln('</td>');
         } else if (pszAttr.value=='predquant' && uom!=null && uom=='each' && node==null && nopredec==false)
         {
          document.writeln('<td>');
            document.writeln('1'); 
          document.writeln('</td>');
         }else if ((pszAttr.value == "object_desc") || (pszAttr.value == "bl_rev_object_desc"))
         {
           document.writeln('<td>');
           //if (node!=null) document.writeln(node.nodeValue.substring(0,24)); 
           if (node!=null) document.writeln(node.xml.substring(0,24)); 
     document.writeln('</td>');
   }
         else
         {
          document.writeln('<td>');
           //if (node!=null) document.writeln(node.nodeValue); 
           //if (node!=null) document.writeln(node.xml); 
           if (node!=null) document.writeln(node.xml.replace("&lt;BR&gt;", "<BR>")); 
          document.writeln('</td>');
         }
       }
    
   }
  
}

function create_prod_pred_header(theProdProps,thePredProps)
{
  /*nested tables*/
  var szTable='<table '
  szTable += get_table_format(theProdProps) + '>';
   
  /*table product*/ 
  document.writeln(szTable);
  document.write('<tr>');
  compose_table_head(theProdProps);
      
  if (thePredProps != null)
  {
    compose_table_head(thePredProps);
  }
  
  document.writeln('</tr>');
  document.write('<tr>');
  compose_propval_line(theProdProps);
  
  if (thePredProps != null)
  {
    compose_propval_line(thePredProps);
  }
  
  document.writeln('</tr>');
  document.writeln('</table>');   
}

function create_document_table(theDocs)
{
  /*not so nice the table format is given with every prop, should be
  changed in future*/
  
   if (theDocs.length>0)
   {
   
    doc=theDocs.item(0);
    theProps=get_diff_props(doc);
  
     var szTable='<table '
     szTable += get_table_format(theProps) + '>';
     
     document.writeln(szTable);
 

    for(var i=0;i<theDocs.length;i++)
    {
      doc=theDocs.item(i);

      theProps=get_diff_props(doc);
  
      if (i==0)
      {
        document.write('<tr>');
        compose_table_head(theProps);
        document.write('</tr>');
      }
    
      document.write('<tr>'); 
      compose_propval_line(theProps);
      document.writeln('</tr>');
  
    }

    document.writeln('</table>');
  }   
}

function create_diff_table(theDiffs, iMode)
{
  if (theDiffs.length>0)
  {
    var diff = theDiffs.item(0);
    theProps = get_diff_props(diff);
    
    if (theProps != null)
    {
    
    
    var szTable='<table '
    szTable += get_table_format(theProps) + '>';
     
    document.writeln(szTable);
    

    for(var i=0;i<theDiffs.length;i++)
    {
      var diff=theDiffs.item(i);
      //var theChange=get_diff_change(diff);
      var theChange=get_collection_element(diff,"change");
      var theProps=get_diff_props(diff);

      if (i==0)
      {
        document.write('<tr>');
        if (iMode == 1)
        {
          document.write('<td>');
          document.writeln('<B>+\\-<\B>');
          document.writeln('</td>');
        }
        compose_table_head(theProps);
        document.write('</tr>');
      }
    
      document.write('<tr>');
      
      if (iMode == 1)
      {
        document.write('<td>');
        //document.write(theChange.firstChild.nodeValue);
        document.write(theChange.firstChild.xml);
        document.writeln('</td>');
      }
      compose_propval_line(theProps, iMode);
      document.writeln('</tr>');
    }

    document.writeln('</table>');
    }
  }
}

function create_diff_attr_table(theDiffs)
{
  if (theDiffs.length>0)
  {
    var diff =theDiffs.item(0);
    theProps=get_diff_props(diff);
    
    
    var szTable='<table '
    szTable += get_table_format(theProps) + '>';
     
    document.writeln(szTable);
     
    document.write('<tr>');
    compose_table_head(theProps);
    document.write('</tr>');
    
    for(var i=0;i<theDiffs.length;i++)
    {
      var diff=theDiffs.item(i);
  
      var theProps=get_diff_props(diff);
     
      if (theProps != null)
      {
        document.write('<tr>'); 
        compose_propval_line(theProps);
        document.writeln('</tr>');
      }
    }

    document.writeln('</table>');
  }
}


function report_comps(theDocComp,theBomComp,theAttrComp)
{
   /*get the product, predessor*/
   
   var predProps=get_predecessor_props(theDocComp);
   var prodProps=get_product_props(theDocComp);

   create_prod_pred_header(prodProps,predProps);

   var docDiffs=theDocComp.getElementsByTagName("diff");
   
   document.writeln('<BR>');

   document.writeln('<table><tr><td>');
   document.writeln('<H3>Change of Document Lines</H3>');
   document.writeln('</td></tr></table>');
   
   create_diff_table(docDiffs, 1);
   
   document.writeln('<BR>');
   
   var bomDiffs=theBomComp.getElementsByTagName("diff");
   
   document.writeln('<table><tr><td>');
   document.writeln('<H3>Changes of BOM Lines</H3>');
   document.writeln('</td></tr></table>');

   create_diff_table(bomDiffs, 1);
   
   document.writeln('<BR>');
   
   if (theAttrComp != null)
   {
     var attrDiffs=theAttrComp.getElementsByTagName("diff");
      
     document.writeln('<table><tr><td>');
     document.writeln('<H3>Change of Product Attributes</H3>');
     document.writeln('</td></tr></table>');
   
     create_diff_attr_table(attrDiffs);
     document.writeln('<BR>');
   }
}


function report_comp_comps(theStatusComp, theAttrComp, theBomComp)
{
  /* Get the product, predessor*/
  var prodProps = get_product_props(theStatusComp);

  create_prod_pred_header(prodProps, null);
  
  var statusDiffs = theStatusComp.getElementsByTagName("diff");
  
  document.writeln('<BR>');

  document.writeln('<table><tr><td>');
  document.writeln('<H3>Overview failing status checks</H3>');
  document.writeln('</td></tr></table>');
   
  create_diff_table(statusDiffs, 0);
   
  document.writeln('<BR>');
   
  var attrDiffs = theAttrComp.getElementsByTagName("diff");
   
  document.writeln('<table><tr><td>');
  document.writeln('<H3>Overview failing attribute checks</H3>');
  document.writeln('</td></tr></table>');

  create_diff_table(attrDiffs, 0);
  
  document.writeln('<BR>');
  
  var bomDiffs = theBomComp.getElementsByTagName("diff");
     
  document.writeln('<table><tr><td>');
  document.writeln('<H3>Overview failing bom checks</H3>');
  document.writeln('</td></tr></table>');
  
  create_diff_table(bomDiffs, 0);

  document.writeln('<BR>');
}


function create_eco_report(theDoc, mode)
{

 
   var docNode=theDoc.documentElement;     
   var ecoNode=docNode.firstChild;
   var docDiffsNode=ecoNode.nextSibling;
   var bomDiffsNode=docDiffsNode.nextSibling;
   var attrDiffsNode=bomDiffsNode.nextSibling;
   
   var ecoProps=get_collection_element(ecoNode,"props");
   
   var szTable='<table '
   szTable += get_table_format(ecoProps) + '>';
      
   document.writeln(szTable);
   document.write('<tr>');
   compose_table_head(ecoProps);
   document.writeln('</tr>');
   document.write('<tr>');
   compose_propval_line(ecoProps)
   document.writeln('</tr>');
   document.writeln('</table>');
     
   theDocComps=docDiffsNode.getElementsByTagName('comp');
   theBomComps=bomDiffsNode.getElementsByTagName('comp');
   theAttrComps=attrDiffsNode.getElementsByTagName('comp');
   
   for (var i=0;i<theDocComps.length;i++)
   {
       /*assumes the comps are in sync*/
       document.writeln('<table><tr><td>');
       document.writeln('<H3>Product change</H3>');
       document.writeln('</td></tr></table>');
       
       document.writeln('<BR>');
       
       if (mode == 1)
       {
         report_comps(theDocComps.item(i),theBomComps.item(i),theAttrComps.item(i));
       }
       else
       {
         report_comps(theDocComps.item(i),theBomComps.item(i),null);
       }
       
       document.writeln('<HR>');

   }     
}

function create_comp_report(theDoc)
{
  var docNode = theDoc.documentElement;     
  var ecoNode = docNode.firstChild;
  var ecoProps = get_collection_element(ecoNode,"props");
  
  if (ecoProps != null)
  {
    var ecoProps = get_collection_element(ecoNode,"props");
    
    var szTable='<table '
    szTable += get_table_format(ecoProps) + '>';
        
    document.writeln(szTable);
    document.write('<tr>');
    compose_table_head(ecoProps);
    document.writeln('</tr>');
    document.write('<tr>');
    compose_propval_line(ecoProps)
    document.writeln('</tr>');
    document.writeln('</table><BR>');

    var statusDiffsNode = ecoNode.nextSibling;
  }
  else
  {
    var statusDiffsNode = docNode.firstChild;
  }
  var attrDiffsNode = statusDiffsNode.nextSibling;
  var bomDiffsNode = attrDiffsNode.nextSibling;
     
  theStatusComps = statusDiffsNode.getElementsByTagName('comp');
  theAttrComps = attrDiffsNode.getElementsByTagName('comp');
  theBomComps = bomDiffsNode.getElementsByTagName('comp');
  for (var i = 0; i < theStatusComps.length; i++)
  {
    /* Assume the comps are in sync */
    document.writeln('<table><tr><td>');
    document.writeln('<H3>Product information</H3>');
    document.writeln('</td></tr></table>');

    report_comp_comps(theStatusComps.item(i), theAttrComps.item(i), theBomComps.item(i));

    document.writeln('<HR>');
  }     
}


function compose_wu_table_head(theProps,theEcoProps)
{
   var props=theProps.childNodes;
   
   
   //document.writeln("ecoprops:" + ecoprops.length);
   
   /*header*/
   document.writeln('<tr>');
   for (var i=0;i<props.length;i++)
   { 
     
   
     if (props(i).nodeName=="prop")
     {
  
       pszAttr=props(i).attributes.getNamedItem("colname");
       document.write('<td><B>');
         if (pszAttr!=null) document.writeln(pszAttr.value); 
       document.writeln('</B></td>');
     }
   }
   
   
   
   if (theEcoProps!=null)
   {
      var ecoprops=theEcoProps.childNodes;
   
   
     for (var i=0;i<ecoprops.length;i++)
     { 
       
       if (ecoprops(i).nodeName=="prop")
       {
    
         pszAttr=ecoprops(i).attributes.getNamedItem("colname");
         document.write('<td><B>');
           if (pszAttr!=null) document.writeln(pszAttr.value); 
         document.writeln('</B></td>');
       }
     }
     
     document.writeln('</tr>');
   }
   else
   {
     document.writeln('</tr>');
   }
   
  
}


function write_wu_prop_line(aWuElement)
{
   
   var wuProps=get_collection(aWuElement,'props');
   
   document.writeln('<tr>')
   //find the eco and props
   for (var p=0;p<wuProps.length;p++)
   {
        if (wuProps(p).nodeName=='prop')
    {
           document.writeln('<td>');
  
       if ( wuProps(p).firstChild!=null)
       {
             //document.writeln(wuProps(p).firstChild.nodeValue);
             document.writeln(wuProps(p).firstChild.xml);
        
       }
       
       document.writeln('</td>');
    }
   
   }
  
   //find the ecoprops
   theEcos=get_collection(aWuElement,'ecos');
   if (theEcos!=null)
   {
     
    
   for (var e=0;e<theEcos.length;e++)
   {
  
            var ecoProps=get_collection(theEcos(e),'props');
          if (e==0)
          {
            for (var i=0;i<ecoProps.length;i++)
              {
              if (ecoProps(i).nodeName=='prop')
            {
                document.writeln('<td>');
            
              if (ecoProps(i).firstChild!=null)
              {
                   //document.writeln(ecoProps(i).firstChild.nodeValue);
                   document.writeln(ecoProps(i).firstChild.xml);
               
              }
             
               document.writeln('</td>');
             }
              }
          }
          else
          {
             document.writeln("<tr>")
             //-1 because of table format prop
             for (i=0;i<wuProps.length-1;i++)
             {
               document.writeln("<td></td>")
             }
             
             for (var i=0;i<ecoProps.length;i++)
               {
              if (ecoProps(i).nodeName=='prop')
            {
                document.writeln('<td>');
            
              if (ecoProps(i).firstChild!=null)
              {
                   //document.writeln(ecoProps(i).firstChild.nodeValue);
                   document.writeln(ecoProps(i).firstChild.xml);
               
              }
             
               document.writeln('</td>');
             }
              }
             
             if (e<theEcos.length-1) document.writeln("</tr>");
          }
    
   
      }
  
        document.writeln('</tr>');
   }
   else
   {
   
      document.writeln('</tr>');
   }
   

   
}


function create_whereused_table(theWhereuseds)
{   
   var theWuProducts=null;
   var theEcoProps=null;
  
   for (var wu=0;wu<theWhereuseds.length;wu++)
   {
     
     theWuProps=get_collection_element(theWhereuseds(wu),"props");
   
     theEcoProps=get_wu_eco_props(theWhereuseds(wu));
   
     theWuProducts=null;
     theWuProducts=get_collection_element(theWhereuseds(wu),"products");

    var szTable='<table ';
    szTable += get_table_format(theWuProps) + '>';
    document.writeln(szTable);
    //write out here the table with source target
        
    compose_wu_table_head(theWuProps,theEcoProps);

    write_wu_prop_line(theWhereuseds(wu));

    document.writeln('</table>');
      
    document.writeln('<br>');

    if (theWuProducts.childNodes!=null )
    {
      if (theWuProducts.childNodes.length>0)
      {
        var szTable='<table ';

        theProdProps=get_collection_element(theWuProducts.childNodes(0),"props");

        szTable += get_table_format(theProdProps) + '>';
        document.writeln(szTable);

        compose_wu_table_head(theProdProps,theEcoProps);

        for (var p=0;p<theWuProducts.childNodes.length;p++)
        {
          write_wu_prop_line(theWuProducts.childNodes[p]);
        }

        document.writeln('</table>');
      }
    }
  }
}

function create_whereused_rep(xmldoc, level, itemtype)
{
   if (check_error(xmldoc)) return;

   var theWhereuseds=xmldoc.getElementsByTagName('whereused');

   document.writeln('<html><head></head>');
   
   document.writeln("<script type='text/javascript' src='teamcenter/dhtml/common/fei/actions/bnl_report.js'><" + "/script>");

   document.writeln('<table><tr><td>');
   document.writeln('<H2>Where Used</H2>');
   document.writeln('</td></tr></table>');
   
   document.writeln('<table><tr><td>');
   document.writeln('<B>Number of levels (-1 is all): </B>' + level);
   document.writeln('</td></tr>');
   document.writeln('<tr><td>');
   document.writeln('<B>Filter by Item Type: </B>' + itemtype);
   document.writeln('</td></tr></table>');
   document.writeln('<BR>');
   
   document.writeln('<p><a href="javascript:export_to_excel();">Export all tables to Excel</a></p>');
   document.writeln('<BR>');

   create_whereused_table(theWhereuseds);
   document.writeln('<BR>');

   document.writeln('</html>');
   
   window.location.reload();
}

function create_whereused_top_rep(xmldoc, revrule, itemtype)
{
   if (check_error(xmldoc)) return;

   var theWhereuseds=xmldoc.getElementsByTagName('whereused');

   document.writeln('<html><head></head>');
   
   document.writeln("<script type='text/javascript' src='teamcenter/dhtml/common/fei/actions/bnl_report.js'><" + "/script>");

   document.writeln('<table><tr><td>');
   document.writeln('<H2>Where Used (Top Level)</H2>');
   document.writeln('</td></tr></table>');
   
   document.writeln('<table><tr><td>');
   document.writeln('<B>Revision rule: </B>' + revrule);
   document.writeln('</td></tr>');
   document.writeln('<tr><td>');
   document.writeln('<B>Filter by Item Type: </B>' + itemtype);
   document.writeln('</td></tr></table>');
   document.writeln('<BR>');
   
   document.writeln('<p><a href="javascript:export_to_excel();">Export all tables to Excel</a></p>');
   document.writeln('<BR>');

   create_whereused_table(theWhereuseds);
   document.writeln('<BR>');

   document.writeln('</html>');
   
   window.location.reload();
}


function export_to_excel()
{
  var row = 0;
  var allTables = document.getElementsByTagName('table');
  
  if (allTables.length > 0)
  {
    var objXL = new ActiveXObject("Excel.Application");
    var objWB = objXL.Workbooks.Add();
    var objWS = objWB.ActiveSheet;

    for(var t = 0; t < allTables.length; t++)
    {
      for (var i = 0; i < allTables[t].rows.length; i++)
      {
        for (var j = 0; j < allTables[t].rows(i).cells.length; j++)
        {
          var mycell = allTables[t].rows(i).cells(j);

          objWS.Cells(row + 1,j + 1).NumberFormat = "@";
          objWS.Cells(row + 1,j + 1).Value = mycell.innerText;
        }
        row++;;
      }
      row++;
    }

    objWS.Range("A1", "Z1").EntireColumn.AutoFit();
    objXL.Visible = true;
  }
  else
  {
    alert('There are no tables to export to Excel');
  }
}
