/* 
#======================================
# Copyright Siemens PLM Software Benelux
# 
# 18-sep-2003  Erik Navis     0.0  Creation
# 07-Mar-2005  Erik Navis     0.1  Make it compatible with tceng 9.x
# 13-aug-2008  J. Mansvelders 0.2  Added ebom reports
# 10-jun-2009  Jale wijnja    0.3  Added Bom report extended
# 12-Jun-2009  J. Mansvelders 0.4  Updated for WhereUsed dialog
# 10-Oct-2009  J. Mansvelders 0.5  Updated for Compliance Report dialog
# 12-Dec-2009  J. Mansvelders 0.6  Updated createBomRepWuDialog
# 05-Jan-2010  J. Mansvelders 0.7  Added Bom report MFG/PRO
# 22-Feb-2010  J. Mansvelders 0.8  Added createBomRepWuTopDialog
# 21-Feb-2012  J. Mansvelders 0.9  Fixed issue with bom report extended and compliance report
#====================================
*/
var BomRepExtDialog = null;
var BomRepMfgDialog = null;
var BomRepDialog = null;
var EBomPackedRepDialog = null;
var EBomRepDialog = null;
var WhereUsedRepDialog = null;
var WhereUsedTopRepDialog = null;
var CompRepExtDialog = null;

var rules = null;


function createBomRepDialog(report)
{
  var revruledoc=document.all("revrules").XMLDocument;
  
  var title;


  if (report == 'BOM')
  {
    buttons="<input type=button onmousedown=\"handle_bomrep_dialog(getDialog( this ))\" value=OK>" + 
    "<input type=button onclick=\"buttonCancel(getDialog(this))\" value=\"" + getString( "web_cancel" ) + "\">";
    
    title = 'Bill of Material report';
  }
  else if (report == 'BOM_extended')
  {
    buttons="<input type=button onmousedown=\"handle_bomrep_ext_dialog(getDialog( this ))\" value=OK>" + 
    "<input type=button onclick=\"buttonCancel(getDialog(this))\" value=\"" + getString( "web_cancel" ) + "\">";
    
    title = 'Bill of Material extended report';
  }
  else if (report == 'BOM_mfgpro')
    {
      buttons="<input type=button onmousedown=\"handle_bomrep_mfgpro_dialog(getDialog( this ))\" value=OK>" + 
      "<input type=button onclick=\"buttonCancel(getDialog(this))\" value=\"" + getString( "web_cancel" ) + "\">";
      
      title = 'Bill of Material (MFG/Pro)';
  }
  else if (report == 'EBOMPACKED')
  {
    buttons="<input type=button onmousedown=\"handle_ebompackedrep_dialog(getDialog( this ))\" value=OK>" + 
    "<input type=button onclick=\"buttonCancel(getDialog(this))\" value=\"" + getString( "web_cancel" ) + "\">";
    
    title = 'Packed Electrical BOM report';
  }
  else if (report == 'EBOM')
  {
    buttons="<input type=button onmousedown=\"handle_ebomrep_dialog(getDialog( this ))\" value=OK>" + 
    "<input type=button onclick=\"buttonCancel(getDialog(this))\" value=\"" + getString( "web_cancel" ) + "\">";
    
    title = 'Unpacked Electrical BOM report';
  }

  var dialog = createDialog(2,buttons);

  dialog.titleCell.innerHTML = title;
  
  var revRow = document.createElement( 'tr' );
  var revLabel = document.createElement( 'td' );
  var revField = document.createElement( 'td' );

  // table.firstChild.nextSibling.firstChild
  //       thead    tbody 
  dialog.table.firstChild.insertBefore( revRow );
  dialog.table.firstChild.insertBefore( revField );
  revRow.insertBefore( revLabel );
  revRow.insertBefore( revField );

  var DateRow = document.createElement( 'tr' );
  var DateLabel = document.createElement( 'td' );
  var DateField = document.createElement( 'td' );
  //       thead    tbody
  dialog.table.firstChild.insertBefore( DateRow );
  DateRow.insertBefore( DateLabel );
  DateRow.insertBefore( DateField );

  var levelRow = document.createElement( 'tr' );
  var levelLabel = document.createElement( 'td' );
  var levelField = document.createElement( 'td' );
  //       thead    tbody
  dialog.table.firstChild.insertBefore( levelRow );
  levelRow.insertBefore( levelLabel );
  levelRow.insertBefore( levelField );

  levelLabel.innerHTML = "Level:"
  
  DateLabel.innerHTML = "Effectivity date:";
  revLabel.innerHTML = "Revision Rule:"; 

  /* This is also possible be we use the function create by iman development */
  //revField.innerHTML = "<input type=\"text\" size=\"20\" name=\"revrule\" value=\"\">"

  if (rules == null)
  {
    rules=revruledoc.getElementsByTagName('rule');
  }
  var revrulselect ="<select name=\"revrules\">"; 
  
  for (var i=0; i<rules.length ;i++)
  {  
    revrulselect += "<option value=\"" + rules.item(i).firstChild.nodeValue + "\">" + rules.item(i).firstChild.nodeValue;
  }

  revrulselect += "</select>";
  
  revField.innerHTML=revrulselect;
  
  insertValueField( DateField,"", "text",50,"effdate");

  levelField.innerHTML="<select name=\"level\"> <option value=\"1\">First level <option value=\"-1\">All levels</select>"

  return dialog;
}

function createBomRepWuDialog(report)
{
  var title;
  var itemTypePrefValues = askImanPreferenceArray('BNL_rep_wu_item_types');

  buttons="<input type=button onmousedown=\"handle_wu_dialog(getDialog( this ))\" value=OK>" + 
  "<input type=button onclick=\"buttonCancel(getDialog(this))\" value=\"" + getString( "web_cancel" ) + "\">";

  title = 'Where Used report';

  var dialog = createDialog(2,buttons);

  dialog.titleCell.innerHTML = title;
  

  var levelRow = document.createElement( 'tr' );
  var levelLabel = document.createElement( 'td' );
  var levelField = document.createElement( 'td' );
  //       thead    tbody
  dialog.table.firstChild.insertBefore( levelRow );
  levelRow.insertBefore( levelLabel );
  levelRow.insertBefore( levelField );

  levelLabel.innerHTML = "Number of levels (-1 is all):"
  
  insertValueField(levelField,"", "text",10,"level");


  var itemTypeRow = document.createElement( 'tr' );
  var itemTypeLabel = document.createElement( 'td' );
  var itemTypeField = document.createElement( 'td' );
  //       thead    tbody
  dialog.table.firstChild.insertBefore( itemTypeRow );
  itemTypeRow.insertBefore( itemTypeLabel );
  itemTypeRow.insertBefore( itemTypeField );
  
  itemTypeLabel.innerHTML = "Filter by Item Type:";
  
  var typesselect ="<select name=\"itemtypes\">"; 
  for (var i=0; i<itemTypePrefValues.length ;i++)
  {  
    typesselect += "<option value=\"" + itemTypePrefValues[i] + "\">" + itemTypePrefValues[i];
  }
  typesselect += "</select>";
  
  itemTypeField.innerHTML=typesselect;

  return dialog;
}

function createBomRepWuTopDialog(report)
{
  var title;
  var itemTypePrefValues = askImanPreferenceArray('BNL_rep_wu_item_types');

  buttons="<input type=button onmousedown=\"handle_wu_top_dialog(getDialog( this ))\" value=OK>" + 
  "<input type=button onclick=\"buttonCancel(getDialog(this))\" value=\"" + getString( "web_cancel" ) + "\">";

  title = 'Where Used (Top Level) report';

  var dialog = createDialog(2,buttons);

  dialog.titleCell.innerHTML = title;
  
  var itemTypeRow = document.createElement( 'tr' );
  var itemTypeLabel = document.createElement( 'td' );
  var itemTypeField = document.createElement( 'td' );
  //       thead    tbody
  dialog.table.firstChild.insertBefore( itemTypeRow );
  itemTypeRow.insertBefore( itemTypeLabel );
  itemTypeRow.insertBefore( itemTypeField );
  
  itemTypeLabel.innerHTML = "Filter by Item Type:";
  
  var typesselect ="<select name=\"itemtypes\">"; 
  for (var i=0; i<itemTypePrefValues.length ;i++)
  {  
    typesselect += "<option value=\"" + itemTypePrefValues[i] + "\">" + itemTypePrefValues[i];
  }
  typesselect += "</select>";
  
  itemTypeField.innerHTML=typesselect;

  return dialog;
}

function createCompRepDialog()
{
  var validationPrefValues = askImanPreferenceArray('FEI_compliance_checks');
  
  var title = 'Compliance report';

  buttons="<input type=button onmousedown=\"handle_comprep_dialog(getDialog( this ))\" value=OK>" + 
   "<input type=button onclick=\"buttonCancel(getDialog(this))\" value=\"" + getString( "web_cancel" ) + "\">";

  var dialog = createDialog(2, buttons);

  dialog.titleCell.innerHTML = title;

  // Validation
  var valRow = document.createElement( 'tr' );
  var valLabel = document.createElement( 'td' );
  var valField = document.createElement( 'td' );

  // table.firstChild.nextSibling.firstChild
  //       thead    tbody 
  dialog.table.firstChild.insertBefore( valRow );
  dialog.table.firstChild.insertBefore( valField );
  valRow.insertBefore( valLabel );
  valRow.insertBefore( valField );

  valLabel.innerHTML = "Validation configuration:";

  var valselect ="<select name=\"valchecks\">"; 
  for (var i=0; i<validationPrefValues.length ;i++)
  {  
    valselect += "<option value=\"" + validationPrefValues[i] + "\">" + validationPrefValues[i];
  }
  valselect += "</select>";
  
  valField.innerHTML=valselect;

  return dialog;
}


function popupBomRepDialog(report)
{
  var selections = selection();

  if ( selections.length == 0 || selections[0].iman_class != 'ItemRevision' )
  {
    alert( "Please select an ItemRevision" );
    return;
  }

  if (report == 'BOM')
  {
    if ( BomRepDialog == null )
    {
      BomRepDialog = createBomRepDialog(report);
    }
    BomRepDialog.popup();
  }
  else if (report == 'BOM_extended')
  {
    if ( BomRepExtDialog == null )
    {
      BomRepExtDialog = createBomRepDialog(report);
    }
    BomRepExtDialog.popup();
  }
  else if (report == 'BOM_mfgpro')
    {
      if ( BomRepMfgDialog == null )
      {
        BomRepMfgDialog = createBomRepDialog(report);
      }
      BomRepMfgDialog.popup();
  }
  else if (report == 'EBOMPACKED')
  {
    if ( EBomPackedRepDialog == null )
    {
      EBomPackedRepDialog = createBomRepDialog(report);
    }
    EBomPackedRepDialog.popup();
  }
  else if (report == 'WU')
  {
    if (WhereUsedRepDialog == null )
    {
      WhereUsedRepDialog = createBomRepWuDialog(report);
    }
    WhereUsedRepDialog.popup();
  }
  else if (report == 'WU_TOP')
    {
      if (WhereUsedTopRepDialog == null )
      {
        WhereUsedTopRepDialog = createBomRepWuTopDialog(report);
      }
      WhereUsedTopRepDialog.popup();
  }
  else if (report == 'EBOM')
  {
    if ( EBomRepDialog == null )
    {
      EBomRepDialog = createBomRepDialog(report);
    }
    EBomRepDialog.popup();
  }
}

function popupCompRepDialog()
{
  var selections = selection();

  if ( selections.length == 0 || selections[0].iman_class != 'ItemRevision' && selections[0].iman_class!='F3I_ChangeNoticeRevision')
  {
    alert( "Please select an ItemRevision" );
    return;
  }

  if ( CompRepExtDialog == null )
  {
    CompRepExtDialog = createCompRepDialog();
  }
  CompRepExtDialog.popup();
}
