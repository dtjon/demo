/***********************************************************************************************************************
 (C) copyright by UGS Benelux

 
 OS                          platform
 -----------------------------------------------------------------------------------------------------------
 Microsoft Windows NT		  Intel	

 Created by		:	Erik Navis

 Description	:   This is the header belonging to docdownload
 				    
 Created for	:   UGS Benelux

 Comments		:   Every new procedure or function starts with BNL_template

 Filename		:	BNL_docdownload_module.h
	
 Using modules  :   

 Version info   :   The number consist of the iman release and then the release of the handler
				    <release>.<pointrelease>

					
 History of changes											
 reason /description                                By          Version         date
 ----------------------------------------------------------------------------------------------------------
 Creation                                           EN		    0.0			    11-12-2006

************************************************************************************************************************/

#ifndef BNL_DOCDOWNLOAD_H
#define BNL_DOCDOWNLOAD_H

#include <BNL_xml_module.h>
typedef struct bnl_docdownload_ini_s
{
    char    *pszWorkDir;
    char    *pszMasterIni;
/* <ADD> here the fields required for this module */
}bnl_docdownload_ini_t;


extern BNL_docdownload_initialize
( 
  char *pszWorkDir,
  char *pszMasterIni,
  char *pszLogFile,
  char *pszUserLog
);

extern int BNL_docdownload_set_work_dir
(
    char *pszValue
);

extern int BNL_docdownload_set_master_ini_location
(
    char *pszValue
);

extern int BNL_docdownload_free_ini( void);

extern int BNL_docdownload_exit( void);

extern char * BNL_docdownload_ask_version( void);


extern int BNL_module_action(
  module_ini_t *modini,
  char *pszTimeStamp,
  t_XMLElement *pParentAction,
  t_XMLElement *pAction,
  t_XMLElement *pOrder,
  t_XMLElement *pActionData
);

/**
int BNL_docdownload_process
(
    char            *pszTimeStamp,
	t_XMLElement    *pParentAction,
    t_XMLElement    *pAction,
    t_XMLElement    *pOrder,
    t_XMLElement    *pInActionData
)
  Description:

    Start the actual module action.

  Returns:

    0 if ok

*/

extern int BNL_docdownload_process
(
    char            *pszTimeStamp,
	t_XMLElement    *pParentAction,
    t_XMLElement    *pAction,
    t_XMLElement    *pOrder,
    t_XMLElement    *pInActionData
);
/* <ADD> your module specific function definitions here */

#endif
