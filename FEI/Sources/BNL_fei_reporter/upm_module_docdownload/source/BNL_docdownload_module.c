/***********************************************************************************************************************
 (C) copyright by UGS Benelux


 OS                          platform
 -----------------------------------------------------------------------------------------------------------
 Microsoft Windows NT        Intel

 Created by     :   E. Navis

 Description    :   The docdownload module is custom module for FEI which is based
                    on their report modules.

 Created for    :

 Comments       :   Every new procedure or function starts with BNL_docdownload_

 Filename       :   $Id: BNL_docdownload_module.c 429 2015-11-27 10:02:11Z tjonkonj $

 Using modules  :

 Version info   :   The number consist of the iman release and then the release of the handler
                    <release>.<pointrelease>


 History of changes
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 creation                                                               0.0         EN          12-04-2005
 added header include bnl_em_module                                     0.1         EN          10-01-2006
 added one level bomrep handling                                        0.2         EN          02-02-2007
 Do not make a report if no document is given                           0.3         EN          06-02-2007
 added extension filter                                                 0.4         EN          12-03-2007
 If the eco not released then use Latest Working rev rule               0.5         EN          22-03-2007
 ....                                                                   0.6         EN          ##-##-####
 Added check if output file has been created                            0.7         JM          03-01-2008
 Updated ECODIFF and BOMDIFF                                            0.8         JM          20-09-2008
 Added ECODIFF_EXT                                                      0.9         JM          04-08-2009
 Added name filter                                                      1.0         JM          01-09-2010
 Used given revision rule for change report                             1.1         JM          22-11-2010
 Update version number only for IR-7542280                              1.2         DTJ         10-11-2015

************************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <windows.h>
#include <BNL_fm_module.h>
#include <BNL_xml_module.h>
#include <BNL_ini_module.h>
#include <BNL_ups.h>
#include <BNL_debug.h>

#include <BNL_dll_upm_interface.h>
#include <BNL_docdownload_module.h>

#include <io.h>
#include <iman.h>
#include <imantype.h>
#include <grm.h>
#include <item.h>

#include <ecm.h>


#include <BNL_tb_module.h>
#include <BNL_var_module.h>
#include <BNL_strings_module.h>
#include <BNL_ux_module.h>

#include <BNL_rp_module.h>
#include <BNL_fei_rep_parser.h>
#include <BNL_mm_module.h>
#include <BNL_tb_module.h>
#include <BNL_strings_module.h>
#include <BNL_em_module.h>




static  char BNL_module[]        ="DOCDOWNLOAD 1.2";

static  bnl_docdownload_ini_t *docdownload_ini   =NULL;

static  FILE    *modulelog;
static  FILE    *userlog;


/*
Module paramters
*/
#define BNL_DOCDOWNLOAD_DOWNLOADTYPE  "downloadtype"
#define BNL_DOCDOWNLOAD_DSTYPES       "dstypes"
#define BNL_DOCDOWNLOAD_REVISIONRULE  "revisionrule"
#define BNL_DOCDOWNLOAD_LEVEL         "level"
#define BNL_DOCDOWNLOAD_DOWNLOADNAME  "downloadname"
#define BNL_DOCDOWNLOAD_REPORTNAME    "reportname"
#define BNL_DOCDOWNLOAD_RELATIONS     "relations"
#define BNL_DOCDOWNLOAD_NAMEDREFS     "namedreferences"
#define BNL_DOCDOWNLOAD_ONELEVELREP   "onelevel_bomrep"
#define BNL_DOCDOWNLOAD_EXTFILTER     "extension_filter"
#define BNL_DOCDOWNLOAD_DSNAMEFILTER  "dsname_filter"



/*function that is always called by the dll interface*/
int BNL_module_action
(
  module_ini_t *modini,
  char *pszTimeStamp,
  t_XMLElement *pParentAction,
  t_XMLElement *pAction,
  t_XMLElement *pOrder,
  t_XMLElement *pActionData
)
{
  int iRetCode=0;

  iRetCode=BNL_docdownload_initialize(modini->pszWorkDir,modini->pszMasterIni,modini->pszLogFile,modini->pszUserLog);
  if (iRetCode!=0) return iRetCode;

  iRetCode=BNL_docdownload_process(pszTimeStamp,pParentAction,pAction,pOrder,pActionData);

  return iRetCode;
}
/*function is always called by dll interface*/
int BNL_module_exit
(
)
{
  int iRetCode=0;


  BNL_docdownload_exit();


  return iRetCode;
}


int BNL_docdownload_initialize
(
  char *pszWorkDir,
  char *pszMasterIni,
  char *pszLogFile,
  char *pszUserLog
)
{

  int iRetCode    =0;


  BNL_fm_initialise( NULL);

  /*because dll can not share filepointers we need to open the logfiles here*/
  iRetCode = BNL_fm_open_file( &modulelog, pszWorkDir, pszLogFile, "a+t", true);
  iRetCode = BNL_fm_open_file( &userlog, pszWorkDir, pszUserLog, "a+t", true);

  if (iRetCode == 0)
  {
    /*initialise modules here*/
    BNL_fm_set_log( modulelog);
    BNL_ini_initialise( modulelog, NULL);
    BNL_xml_initialise( modulelog, NULL);
    BNL_var_initialise( modulelog, NULL);
    BNL_csv_initialise( modulelog, NULL);
    BNL_ux_initialise ( modulelog );

    BNL_rp_initialise ( modulelog );
    BNL_mm_initialise(modulelog);
    BNL_tb_initialise(modulelog);
    BNL_em_initialise(modulelog);
    BNL_strings_initialise(modulelog,NULL);


    /*get the port from the ini file*/


    docdownload_ini=malloc(sizeof(bnl_docdownload_ini_t));

    if (docdownload_ini!=NULL)
    {
      docdownload_ini->pszMasterIni   = NULL;
      docdownload_ini->pszWorkDir     = NULL;

      BNL_docdownload_set_work_dir(pszWorkDir);
      BNL_docdownload_set_master_ini_location(pszMasterIni);

      /*read here ini file settings for the module if needed*/
    }
    else
      iRetCode=1;
  }

  return iRetCode;
}

/**
int BNL_docdownload_exit(void)

  Description:

  Exit function called to exit the module properly.
  The functional modules should be closed here.
*/

int BNL_docdownload_exit(void)
{

  int iRetCode =0;

  BNL_ENTER( "docdownload", "BNL_docdownload_exit");


  iRetCode=BNL_docdownload_free_ini();

  BNL_fm_exit();
  BNL_ini_exit();
  BNL_xml_exit();
  BNL_var_exit();


  fprintf( modulelog,"%s :Module exited. \n\n",BNL_module);
  fprintf( userlog,"Module exited. <BR>");

  fclose (modulelog);
  fclose (userlog);

  BNL_LEAVE( "docdownload", "BNL_docdownload_exit");

  return iRetCode;
}



/**
int BNL_docdownload_set_work_dir
(
    char *pszValue      <I>
)

  Description:

    Sets the work dir.

  Returns:

    0 on Ok.

*/
int BNL_docdownload_set_work_dir
(
    char *pszValue
)
{

    int iLen        =0;
    int RetCode     =0;

    BNL_ENTER( "docdownload", "BNL_docdownload_set_work_dir");

    iLen=strlen(pszValue)+1;

    if (docdownload_ini->pszWorkDir!=NULL)
    {
        free(docdownload_ini->pszWorkDir);
    }

    docdownload_ini->pszWorkDir=malloc(sizeof(char)*iLen);

    strcpy(docdownload_ini->pszWorkDir,pszValue);

    BNL_LEAVE( "docdownload", "BNL_docdownload_set_work_dir");

    return RetCode;
}/*end of int BNL_ugplot_set_work_dir*/


/**
int BNL_docdownload_set_master_ini_location
(
    char *pszValue      <I>
)

  Description:

    Sets the ini file location in ini struc

  Returns:

    0 on Ok.

*/
int BNL_docdownload_set_master_ini_location
(
    char *pszValue
)
{

    int iLen        =0;
    int RetCode     =0;

    BNL_ENTER( "docdownload", "BNL_docdownload_set_master_ini_location");

    iLen=strlen(pszValue)+1;

    if (docdownload_ini->pszMasterIni!=NULL)
    {
        free(docdownload_ini->pszMasterIni);
    }

    docdownload_ini->pszMasterIni=malloc(sizeof(char)*iLen);

    strcpy(docdownload_ini->pszMasterIni,pszValue);

    BNL_LEAVE( "docdownload", "BNL_docdownload_set_master_ini_location");

    return RetCode;

}/*end of int BNL_docdownload_set_master_ini_location*/


/**
int BNL_docdownload_free_ini
()

  Description:

    Frees the ini struc in save way.

  Returns:

    0 on ok.

*/
int BNL_docdownload_free_ini()
{
    int RetCode     =0;

    BNL_ENTER( "docdownload", "BNL_docdownload_free_ini");

    if (docdownload_ini->pszWorkDir!=NULL) free (docdownload_ini->pszWorkDir);
    if (docdownload_ini->pszMasterIni!=NULL) free (docdownload_ini->pszMasterIni);

    /* <ADD> be sure to free all allocated memory */

    free (docdownload_ini);

    BNL_LEAVE( "docdownload", "BNL_docdownload_free_ini");

    return RetCode;
}



/**
char * BNL_docdownload_ask_version( void)

  Description:

    Returns the version of he module.

  parameters: none
*/
char * BNL_docdownload_ask_version( void)
{
    return BNL_module;
}/*end of char * BNL_ugplot_ask_version*/



/**
int BNL_docdownload_process
(
  char            *pszTimeStamp,
  t_XMLElement    *pParentAction,
  t_XMLElement    *pAction,
  t_XMLElement    *pOrder,
  t_XMLElement    *pInActionData
)
  Description:

  Start the actual module action.
  pParentaction contains the id and device file used
  pAction is the loaded device file
  pOrder is the complete order structure
  pInActionData is the incomming action data, these can be filled in sequential actions

  Returns:

    0 if ok

*/

int BNL_docdownload_process
(
  char            *pszTimeStamp,
  t_XMLElement    *pParentAction,
  t_XMLElement    *pAction,
  t_XMLElement    *pOrder,
  t_XMLElement    *pInActionData
)
{
  int             iRetCode        = 0;
  int             iMode           = 0;
  int             iLevel          = 0;
  int             iDsTypes        = 0;
  int             iNrs            = 0;
  int             iRels           = 0;
  int             iExtTypes       = 0;
  int             iDsNames        = 0;

  t_XMLElement    *pOutActionData =NULL;

  t_XMLElement    *pParams    =NULL;


  t_XMLElement    *pLoadAction  =NULL;

  t_XMLElement    *pItem      =NULL;
  t_XMLElement    *pDataset   =NULL;
  t_XMLElement    *pRev     =NULL;
  t_XMLElement    *pRel     =NULL;
  t_XMLElement    *pContent   =NULL;
  t_XMLElement    *pNamedRef    =NULL;

  t_XMLElement    *pDownLoadType  =NULL;
  t_XMLElement    *pDownLoadName  =NULL;
  t_XMLElement    *pReportName    =NULL;
  t_XMLElement    *pLevel         =NULL;
  t_XMLElement    *pRevisionRule  =NULL;
  t_XMLElement    *pNamedRefs     =NULL;
  t_XMLElement    *pRelations     =NULL;
  t_XMLElement    *pDsTypes       =NULL;
  t_XMLElement    *pOneLevelRep   =NULL;
  t_XMLElement    *pExtTypes      =NULL;
  t_XMLElement    *pDsNames       =NULL;

  t_XMLElement    *pDocument      =NULL;

  tag_t tEco      =NULLTAG;
  tag_t tRev      =NULLTAG;

  char *pszPuid     =NULL;
  char *pszId       =NULL;
  char *pszDsName     =NULL;
  char *pszDsType     =NULL;
  char *pszItem     =NULL;
  char *pszRev      =NULL;
  char *pszDrwName    =NULL;
  char *pszRelation   =NULL;
  char *pszNamedRef   =NULL;
  char *pszXMLOut         =NULL;
  char *pszDownLoadName   =NULL;
  char *pszExportDir      =NULL;
  char *pszReportName     =NULL;
  char *pszEffDate        =NULL;
  char **pszDsTypes       =NULL;
  char **pszRelations     =NULL;
  char **pszNrs           =NULL;
  char **pszExtTypes      =NULL;
  char **pszDsNames       =NULL;

  FILE *fpReport          =NULL;

  var_data_t tCurrentData;
  int   iPrefValues       = 0;
  char  **pszPrefValues   = NULL;

  BNL_ENTER( "docdownload", "BNL_docdownload_process");

  if (pInActionData!=NULL)
  {
    fprintf(modulelog,"\n*******incoming data**********\n\n");
    BNL_xml_write_document(modulelog,pInActionData);
    fprintf(modulelog,"\n*******************************\n\n");
  }

  /*get the action puid and id, use id for variables and puid for objects*/
  iRetCode=BNL_xml_element_get_attribute_value(pParentAction,UPM_ATTR_PUID,&pszPuid);
  iRetCode=BNL_xml_element_get_attribute_value(pParentAction,UPM_ATTR_ID,&pszId);


  /*
  Get the params of the action
  */
  pParams=BNL_xml_find_first(pAction,UPM_TAG_PARAMS);

  /*determine first wether to load or unload the part*/

  if (pParams!=NULL)
  {
    pDownLoadType=BNL_xml_find_first(pParams,BNL_DOCDOWNLOAD_DOWNLOADTYPE);

    if (pDownLoadType==NULL)
    {
      fprintf(modulelog,"%s :Error the action parameter [%s] is not found\n",BNL_module,BNL_DOCDOWNLOAD_DOWNLOADTYPE);
      fprintf(userlog,"Error the action parameter [%s] is not found <BR>\n",BNL_DOCDOWNLOAD_DOWNLOADTYPE);
      iRetCode=-1;
      goto module_exit;
    }
    else
    {
      if(_stricmp(pDownLoadType->pszElementValue,"BOMREP")==0)
      {
        iMode=BOMREP;
      }
      else if(_stricmp(pDownLoadType->pszElementValue,"DOCSREP")==0)
      {
        iMode=DOCSREP;
      }
      else if(_stricmp(pDownLoadType->pszElementValue,"ECODIFF")==0)
      {
        iMode=ECODIFF;
      }
      else if(_stricmp(pDownLoadType->pszElementValue,"ECODIFF_EXT")==0)
      {
        iMode=ECODIFF_EXT;
      }
      else
      {
        fprintf(modulelog,"%s :Error the action parameter [%s] does not contain the right value\n",BNL_module,pDownLoadType->pszElementValue);
        fprintf(userlog,"Error the action parameter [%s] does not contain the right value <BR>\n",pDownLoadType->pszElementValue);
        iRetCode=-1;
        goto module_exit;
      }
    }

    pDownLoadName=BNL_xml_find_first(pParams,BNL_DOCDOWNLOAD_DOWNLOADNAME);
    if (pDownLoadName==NULL)
    {
      fprintf(modulelog,"%s :Error the action parameter [%s] is not found\n",BNL_module,BNL_DOCDOWNLOAD_DOWNLOADNAME);
      fprintf(userlog,"Error the action parameter [%s] is not found <BR>\n",BNL_DOCDOWNLOAD_DOWNLOADNAME);
      iRetCode=-1;
      goto module_exit;
    }

    pReportName=BNL_xml_find_first(pParams,BNL_DOCDOWNLOAD_REPORTNAME);
    if (pReportName==NULL)
    {
      fprintf(modulelog,"%s :Error the action parameter [%s] is not found\n",BNL_module,BNL_DOCDOWNLOAD_REPORTNAME);
      fprintf(userlog,"Error the action parameter [%s] is not found <BR>\n",BNL_DOCDOWNLOAD_REPORTNAME);
      iRetCode=-1;
      goto module_exit;
    }


    pDsTypes=BNL_xml_find_first(pParams,BNL_DOCDOWNLOAD_DSTYPES);
    if (pDsTypes==NULL)
    {
      fprintf(modulelog,"%s :Error the action parameter [%s] is not found\n",BNL_module,BNL_DOCDOWNLOAD_DSTYPES);
      fprintf(userlog,"Error the action parameter [%s] is not found <BR>\n",BNL_DOCDOWNLOAD_DSTYPES);
      iRetCode=-1;
      goto module_exit;
    }

    pRelations=BNL_xml_find_first(pParams,BNL_DOCDOWNLOAD_RELATIONS);
    if (pRelations==NULL)
    {
      fprintf(modulelog,"%s :Error the action parameter [%s] is not found\n",BNL_module,BNL_DOCDOWNLOAD_RELATIONS);
      fprintf(userlog,"Error the action parameter [%s] is not found <BR>\n",BNL_DOCDOWNLOAD_DSTYPES);
      iRetCode=-1;
      goto module_exit;
    }

    pExtTypes=BNL_xml_find_first(pParams,BNL_DOCDOWNLOAD_EXTFILTER);

    pDsNames = BNL_xml_find_first(pParams, BNL_DOCDOWNLOAD_DSNAMEFILTER);

    pNamedRefs=BNL_xml_find_first(pParams,BNL_DOCDOWNLOAD_NAMEDREFS);
    if (pNamedRefs==NULL)
    {
      fprintf(modulelog,"%s :Error the action parameter [%s] is not found\n",BNL_module,BNL_DOCDOWNLOAD_NAMEDREFS);
      fprintf(userlog,"Error the action parameter [%s] is not found <BR>\n",BNL_DOCDOWNLOAD_NAMEDREFS);
      iRetCode=-1;
      goto module_exit;
    }

    pOneLevelRep=BNL_xml_find_first(pParams,BNL_DOCDOWNLOAD_ONELEVELREP);

    /*
     example

        #define BNL_DOCDOWNLOAD_DOWNLOADTYPE "downloadtype"
        #define BNL_DOCDOWNLOAD_DSTYPES      "dstypes"
        #define BNL_DOCDOWNLOAD_REVISIONRULE "revisionrule"
        #define BNL_DOCDOWNLOAD_LEVELS       "levels"


    pLoadAction=BNL_xml_find_first(pParams,BNL_DOCDOWNLOAD_TASK);

    if (pLoadAction!=NULL)
    {

    }
    else
    {
       fprintf(modulelog,"%s :Error the action parameter is not found\n",BNL_module);
       fprintf(userlog,"Error the action parameter is not found <BR>\n");
       iRetCode=-1;
       goto module_exit;
    }
    */


  }
  else
  {
     fprintf(modulelog,"%s :Error the device action does not contain parameters\n",BNL_module);
     fprintf(userlog,"Error the device action does not contain parameters <BR>\n");
     iRetCode=-1;
     goto module_exit;
  }


  /*find the object data item,revision,dataset etc*/
  pItem=BNL_ux_find_dbobject_in_upper_relation(pOrder,pszId,pszPuid,UPM_VAL_IO_CLASS_ITEM);
  pDataset=BNL_ux_find_dbobject_in_upper_relation(pOrder,pszId,pszPuid,UPM_VAL_IO_CLASS_DS);
  pRev=BNL_ux_find_dbobject_in_upper_relation(pOrder,pszId,pszPuid,UPM_VAL_IO_CLASS_REV);
  pContent=BNL_ux_find_dbobject_in_upper_relation(pOrder,pszId,pszPuid,UPM_VAL_IO_CLASS_DRW);



  if (pItem==NULL || pRev==NULL)
  {
    fprintf(modulelog,"Error in structure tried to find upper relations, returned NULL\n");
    fprintf(userlog,"Error in order structure, action stopped\n");
    iRetCode=-1;
    goto module_exit;
  }

  BNL_ux_get_prop_value(pOrder,pItem,UPM_CSV_COL_PARTNO,&pszItem);
  BNL_ux_get_prop_value(pOrder,pRev,UPM_CSV_COL_PARTREV,&pszRev);
  if (pDataset!=NULL)
  {
    pRel=BNL_ux_find_relation(pOrder,pRev,pDataset);
    BNL_ux_get_prop_value(pOrder,pDataset,UPM_CSV_COL_DATASET,&pszDsName);
    BNL_xml_element_get_attribute_value(pDataset,UPM_ATTR_TYPE,&pszDsType);
    pNamedRef=BNL_ux_find_relation(pOrder,pDataset,pContent);
    BNL_xml_element_get_attribute_value(pNamedRef,UPM_ATTR_NAME,&pszNamedRef);
    BNL_ux_get_prop_value(pOrder,pContent,UPM_CSV_COL_SHEETNAME,&pszDrwName);
    BNL_xml_element_get_attribute_value(pRel,UPM_ATTR_NAME,&pszRelation);
  }


  /*for using the var module the current data structure should be filles
  */
  tCurrentData.pOrder=pOrder;
  tCurrentData.pszDatasetName=pszDsName;
  tCurrentData.pszDatasetType=pszDsType;
  tCurrentData.pszDrawingName=pszDrwName; //can be get only in case of files with sheets like UG part or excel
  tCurrentData.pszItemId=pszItem;
  tCurrentData.pszNamedRef=pszNamedRef; // only in case of object dataset
  tCurrentData.pszOrderId=pszId;
  tCurrentData.pszRelation=pszRelation;
  tCurrentData.pszRevId=pszRev;
  tCurrentData.pszWorkDir=docdownload_ini->pszWorkDir;


  BNL_rp_parse_parameter(pDsTypes->pszElementValue,&iDsTypes,&pszDsTypes);

  BNL_rp_parse_parameter(pRelations->pszElementValue,&iRels,&pszRelations);

  BNL_rp_parse_parameter(pNamedRefs->pszElementValue,&iNrs,&pszNrs);

  if (pExtTypes!=NULL) BNL_rp_parse_parameter(pExtTypes->pszElementValue,&iExtTypes,&pszExtTypes);

  if (pDsNames != NULL) BNL_rp_parse_parameter(pDsNames->pszElementValue, &iDsNames, &pszDsNames);

  //create in workdir a dir with this name.
  pszDownLoadName=BNL_var_replace_vars(pDownLoadName->pszElementValue,tCurrentData);

  //remove non allowed dir chars.
  BNL_fm_replace_illegal_chars(pszDownLoadName);

  //create export dir
  pszExportDir=malloc(sizeof(char)*strlen(docdownload_ini->pszWorkDir)+strlen(pszDownLoadName)+4);
  sprintf(pszExportDir,"%s\\%s",docdownload_ini->pszWorkDir,pszDownLoadName);

  if (!BNL_fm_check_file_or_dir(pszExportDir))
  {
      BNL_fm_create_directory(pszExportDir);
  }

  pszReportName=BNL_var_replace_vars(pReportName->pszElementValue,tCurrentData);
  BNL_fm_replace_illegal_chars(pszReportName);
  pszReportName=strcat(pszReportName,".html");


  //initialise rp module
  BNL_rp_set_docdownload_mode(1);

  //do not free given arrays and strings.
  BNL_rp_set_docdownload_extfilter(iExtTypes, pszExtTypes);
  BNL_rp_set_docdownload_dsnamefilter(iDsNames, pszDsNames);
  BNL_rp_set_docdownload_types(iDsTypes,pszDsTypes);
  BNL_rp_set_docdownload_nrs(iNrs,pszNrs);
  BNL_rp_set_docdownload_rels(iRels,pszRelations);
  BNL_rp_set_downloaddir(pszExportDir);


  BNL_rp_open_user_report();

  if (pOneLevelRep!=NULL)
  {
    if (pOneLevelRep->pszElementValue[0]=='y' || pOneLevelRep->pszElementValue[0]=='Y')
    {
      BNL_rp_set_onelevel_bomreps(1);
    }
    else
    {
          BNL_rp_set_onelevel_bomreps(0);
    }
  }


  /*do here a check on found data and report a message if not ok*/

  /*do here the action*/

  switch (iMode)
  {
    case ECODIFF:
    case ECODIFF_EXT:

      pRevisionRule=BNL_xml_find_first(pParams,BNL_DOCDOWNLOAD_REVISIONRULE);
      if (pRevisionRule!=NULL)
      {
        BNL_rp_set_downloadrevrule(pRevisionRule->pszElementValue);
      }
      else
      {
        BNL_rp_set_downloadrevrule(DEFAULT_REVRULE);
      }

      /*find the eco in db*/
      iRetCode = ITEM_find_rev(pszItem, pszRev, &tEco);
      if (tEco!=NULLTAG)
      {
        char szType[WSO_name_size_c + 1];

        // validate processing type
        iRetCode = BNL_tb_pref_ask_char_values(BNL_REP_CHANGE_REV_TYPES_PREF, TC_preference_site, &iPrefValues, &pszPrefValues);
        if( iPrefValues == 0)
        {
          fprintf(modulelog,"%s : Error There are no values specified in preference [%s].\n", BNL_module, BNL_REP_CHANGE_REV_TYPES_PREF);
          fprintf(userlog, "Error: There are no values specified in preference [%s].\n", BNL_REP_CHANGE_REV_TYPES_PREF);
          goto module_exit;
        }
        iRetCode = BNL_tb_determine_object_type(tEco, szType);
        if (!BNL_tb_value_in_list(szType, iPrefValues, pszPrefValues))
        {
          fprintf(modulelog,"%s : Error Type [%s] is not defined in preference [%s].\n", BNL_module, szType, BNL_REP_CHANGE_REV_TYPES_PREF);
          fprintf(userlog, "Error: Type [%s] is not defined in preference [%s].\n", szType, BNL_REP_CHANGE_REV_TYPES_PREF);
          goto module_exit;
        }

        if (!BNL_rp_is_released(tEco))
        {
          fprintf(modulelog, "ECO is not released, using [%s] for download.\n", DEFAULT_REVRULE);
          fprintf(userlog, "ECO is not released, using [%s] for download. <BR>\n", DEFAULT_REVRULE);

          BNL_rp_set_downloadrevrule("Latest Working");
        }

        //iRetCode=BNL_rp_web_eco_diffs(tEco,DEFAULT_REVRULE,&pszXMLOut,&pDocument);
        if (pRevisionRule != NULL)
        {
          iRetCode = BNL_rp_web_eco_diffs(tEco, pRevisionRule->pszElementValue, &pszXMLOut, &pDocument);
        }
        else
        {
          iRetCode = BNL_rp_web_eco_diffs(tEco, DEFAULT_REVRULE, &pszXMLOut, &pDocument);
        }
      }
      else
      {
        fprintf(modulelog,"%s :Error the ECO [%s][%s] is not found in the database\n",BNL_module,pszItem,pszRev);
        fprintf(userlog,"Error the ECO [%s][%s] is not found in the database <BR>\n",pszItem,pszRev);
        goto module_exit;
      }
    break;

    case BOMREP:

      pLevel=BNL_xml_find_first(pParams,BNL_DOCDOWNLOAD_LEVEL);
      if (pLevel==NULL)
      {
        fprintf(modulelog,"%s :Error the action parameter [%s] is not found\n",BNL_module,BNL_DOCDOWNLOAD_LEVEL);
        fprintf(userlog,"Error the action parameter [%s] is not found <BR>\n",BNL_DOCDOWNLOAD_LEVEL);
        iRetCode=-1;
        goto module_exit;
      }

      iLevel=atoi(pLevel->pszElementValue);

      pRevisionRule=BNL_xml_find_first(pParams,BNL_DOCDOWNLOAD_REVISIONRULE);
      if (pRevisionRule==NULL)
      {
        fprintf(modulelog,"%s :Error the action parameter [%s] does not contain the right value\n",BNL_module,BNL_DOCDOWNLOAD_REVISIONRULE);
        fprintf(userlog,"Error the action parameter [%s] does not contain the right value <BR>\n",BNL_DOCDOWNLOAD_REVISIONRULE);
        iRetCode=-1;
        goto module_exit;
      }

      pszEffDate=malloc(sizeof(char)*4);
      *pszEffDate='\0';


      /*find the rev in db*/
      iRetCode=ITEM_find_rev(pszItem,pszRev,&tRev);

      if (tRev!=NULLTAG)
      {
        iRetCode=BNL_rp_web_bom(BOMREP,tRev,pRevisionRule->pszElementValue,pszEffDate,iLevel,1,&pszXMLOut,&pDocument,1);
      }
      else
      {
        fprintf(modulelog,"%s :Error the product [%s][%s] is not found in the database\n",BNL_module,pszItem,pszRev);
        fprintf(userlog,"Error the product [%s][%s] is not found in the database <BR>\n",pszItem,pszRev);
        goto module_exit;
      }
    break;

     case DOCSREP:

      /*find the rev in db*/
      iRetCode=ITEM_find_rev(pszItem,pszRev,&tRev);

      if (tRev!=NULLTAG)
      {
        iRetCode=BNL_rp_web_docs(tRev,&pszXMLOut,&pDocument);
      }
      else
      {
        fprintf(modulelog,"%s :Error the product [%s][%s] is not found in the database\n",BNL_module,pszItem,pszRev);
        fprintf(userlog,"Error the product [%s][%s] is not found in the database <BR>\n",pszItem,pszRev);
        goto module_exit;
      }
    break;

    default:
      fprintf(modulelog,"%s :The downloadtype is not right translated.\n",BNL_module);
      fprintf(userlog,"The downloadtype is not right translated. <BR>\n");

    break;
  }

  if (pDocument!=NULL)
  {
    //create the report file
    iRetCode = BNL_fm_open_file(&fpReport,pszExportDir,pszReportName,"w+t",1);
    // JM|03-01-2008: In some cases the opening of the file fails and this causes the UPM to crash.
    if (iRetCode == 0)
    {
      BNL_fei_rep_parse_html(fpReport,pDocument,iMode,0,NULL,NULL,NULL);

      BNL_fm_close_file(fpReport);
      //free xml struc
      BNL_xml_close_document(pDocument);

      if (pszXMLOut!=NULL) free(pszXMLOut);
    }
  }

  if (pszDownLoadName!=NULL) free(pszDownLoadName);

  if (pszExportDir!=NULL) free(pszExportDir);


  /*return of data, in xml format
    example pOutActionData

    <actiondata>
     <data id="file">a value</data>
    </actiondata>

    if not used pOutActionData may be NULL.

    de can contain anything the next called action determines if it can use the action data


    pOutActionData=BNL_xml_element_create(UPM_TAG_MOD_ACTIONDATA,NULL);
    pData=BNL_xml_element_create(UPM_TAG_MOD_DATA,pszPlotFile);
    BNL_xml_element_create_attribute(pData,UPM_ATTR_MOD_ID,"file");
    BNL_xml_element_add_element(pOutActionData,pData);
   */


module_exit:

  BNL_rp_close_user_report(pszReportName);

  if (pszReportName!=NULL) free(pszReportName);
  if (pszDsName!=NULL)   free(pszDsName);
  if (pszDsType!=NULL)   free(pszDsType);
  if (pszRev!=NULL)      free(pszRev);
  if (pszItem!=NULL)     free(pszItem);
  if (pszPuid!=NULL)     free(pszPuid);
  if (pszId!=NULL)       free(pszId);
  if (pszRelation!=NULL) free(pszRelation);
  if (pszDrwName!=NULL)  free(pszDrwName);
  if (pszNamedRef!=NULL) free(pszNamedRef);

  if (iPrefValues > 0) MEM_free(pszPrefValues);

  /*make sure the actiondata is set in the UPM via callback.*/
  BNL_dll_callback(pOutActionData);

  BNL_LEAVE( "docdownload", "BNL_docdownload_process");

  return iRetCode;
}
