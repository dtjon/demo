/***********************************************************************************************************************
 (C) copyright by UGS Benelux

 Created by     :   E. Navis 

 Description	:   Docdownload dialog, responsible for asking process name and initiate the process
                    at server side using custom itk code.
                     
 Created for    :   FEI

 Comments       :   

 Filename       :   BnlProcessDialog.java
    
 Using modules  :   

 Version info   :   The number consist of the TC release and then the release
                    <release>.<pointrelease>
                    
 History of changes                                            
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 creation                                                               0.0         EN          10-01-2006
 Updated for Teamcenter 8.3.*                                           0.1         EN          16-09-2011
 Updated for Teamcenter 10.1.*                                          0.2         JWO         04-07-2016
************************************************************************************************************************/

package com.fei.rac.commands.docdownload;

import com.teamcenter.rac.util.*;
import com.teamcenter.rac.util.Painter;
import com.teamcenter.rac.aif.kernel.*;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.aif.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class BnlProcessDialog extends AbstractAIFDialog
{
  // A reference to the session
  protected TCSession session = null;
  // A reference to the desktop
  protected AIFDesktop desktop = null;
  // A reference to the parent frame
  protected Frame parent = null;
  // A refrecne to the service
  protected TCUserService service = null;
  // A reference to the registry
  private Registry appReg;

  //the panels
  private JPanel parentPanel;
  private JPanel contentsPanel;
  private JPanel buttonPanel;

  //the buttons
  JButton cancelButton;
  JButton startButton;
  

  //labels
  JLabel processNameLabel;
  JLabel selectedProcessNameLabel;
  
  //textfields
  JTextField processNameText;

  TCComponent[] pComponents;
  
  AbstractAIFUIApplication abstapp;
  

  /*constructors*/

    public BnlProcessDialog(Frame parent, InterfaceAIFComponent[] comps )
    {
        super ( parent, true );
	    session = (TCSession)comps[0].getSession();
	    this.parent = parent;

	    if ( parent instanceof AIFDesktop )
        {
	      desktop = (AIFDesktop)parent;
	    }

        createDialog("");
   }

    public BnlProcessDialog(Frame parent, TCSession theSession )
    {
	     super ( parent, true );
         session = theSession;
	     this.parent = parent;
	     if ( parent instanceof AIFDesktop )
         {
	       desktop = (AIFDesktop)parent;
	     }

         createDialog("");
	}

   public BnlProcessDialog(Frame parent , TCComponent[] TCComponents,String pszProcess )
   {
      super ( parent, true );
      this.parent = parent;
      if ( parent instanceof AIFDesktop )
      {
	    desktop = (AIFDesktop)parent;
     }

      //try
      //{
        session = (TCSession)TCComponents[0].getSession();
        service = (TCUserService)session.getUserService();
        appReg = Registry.getRegistry(this);
        //appReg.printRegistryHierarchy();

      //}catch(TCException ex)
      //{
        //   MessageBox msgBox=new MessageBox( ex );
        //   msgBox.setModal( true );
        //   msgBox.setVisible( true );
      //}

      pComponents=TCComponents;
      createDialog(pszProcess);
      //dataPopulater(TCComponent);
   }


  void createDialog(String pszProcess)
  {
	  
	  System.out.println("DEBUG in create dialog");
    //the panels
    parentPanel =new JPanel(new VerticalLayout(5,2,2,2,2));
    contentsPanel = new JPanel(new VerticalLayout(5,2,2,2,2));



    buttonPanel =new JPanel(new ButtonLayout());

    //***the labels
    processNameLabel=new JLabel(appReg.getString("processname.LABEL"));
    selectedProcessNameLabel = new JLabel(pszProcess);

//  ***buttons
    cancelButton =new JButton(appReg.getString("cancelButton.LABEL"));
    cancelButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });

    startButton =new JButton(appReg.getString("startButton.LABEL"));
    startButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        startButton_actionPerformed(e);
      }
    });

    //****fields    
    
    if(pComponents != null)
    {
        String s1 = pComponents[0].toString();
        if(s1.getBytes().length > 32)
            s1 = new String(s1.getBytes(), 0, 32);
        processNameText = createTextField(s1,32,32, true);    
    }
    else
    {
    	processNameText = createTextField("",32,32, true);
    	startButton.setEnabled(false);
    }

    

    
    

    

    //*****placement of all the components

    //the contentspanel

    contentsPanel.add("top.bind",processNameLabel);
    contentsPanel.add("top.bind",processNameText);
    

    //the buttonpanel
    buttonPanel.add(startButton);
    buttonPanel.add(cancelButton);

    //the parentpanel
    this.getContentPane().add(parentPanel);
    parentPanel.add("top.bind",selectedProcessNameLabel);
    parentPanel.add("top.bind",new Separator ());
    parentPanel.add("top.bind", contentsPanel );

    parentPanel.add("top.bind",new JLabel(" "));
    parentPanel.add("top.bind",new JLabel(" "));
   
    parentPanel.add("bottom.bind.center.top", buttonPanel);
    parentPanel.add("bottom.bind", new Separator());



    this.setTitle(appReg.getString("BnlProcessDialog.TITLE"));
    this.pack();
    this.centerToScreen(1.0, 1.0);

  }

 
  /*
    when startbutton is pressed this method will be carried out
  */
 public  void startButton_actionPerformed(ActionEvent e)
 {

	Integer iRetCode;
		 
	setCursor( Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR) );
    
	try
    {

      

    	  //create_process	
	      Object[] Objtosend=new Object[4];
	      Objtosend[0]=pComponents;
	      Objtosend[1]=processNameText.getText();
	      Objtosend[2]=selectedProcessNameLabel.getText();
	      Objtosend[3]=new String("");//dummy
	
	      iRetCode=(Integer)service.call("BNL_us_start_process",Objtosend);
	      if (iRetCode.intValue()!=0)
	      {
	           JOptionPane messageBox=new JOptionPane();
	           JOptionPane.showMessageDialog(null, appReg.getString("BnlProcessDialog.processCreationNoSucces"), "Error", JOptionPane.ERROR_MESSAGE);
	      }
      
      }
      catch (TCException ex)
      {
            MessageBox msgBox=new MessageBox( ex );
            msgBox.setModal( true );
            msgBox.setVisible( true );
      }

      setCursor( Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR) );
      dispose();
    

 }

 /*when cancel button is pressed this method will be carried out*/
 public  void cancelButton_actionPerformed(ActionEvent e)
 {
    dispose();
 }
 
 
 public JTextField createTextField(String defaultValue, int fieldLength, int imputLimit, boolean bRequired  )
 {
     int defaultLength = 10;
     int length = fieldLength;
     if (length <= 0 ) length = defaultLength;

     String value = defaultValue;
     if(value == null) value = "";

     JTextField field;

     if(bRequired)
     {
         field = new JTextField (value, length )
         {
             public void paint(Graphics g)
             {
                 super.paint(g);
                 Painter.paintIsRequired(this, g);
             }
         };
     }
     else
         field = new JTextField (value, length );

     if (imputLimit > 0 )
     {

         final int limit = imputLimit;

         field.addKeyListener ( new KeyAdapter()
         {
             public void keyTyped ( KeyEvent e )
             {
                 JTextField text = (JTextField)e.getSource();
                 String txt = text.getText();

                 if (txt.length() > 0 )
                 {
                    startButton.setEnabled(true);
                    
                 }
                 else
                 {
                   startButton.setEnabled(false);
                   
                 }

                 int nameLength = limit;
                 if ( txt.length() >= nameLength )
                 {
                     text.setText ( txt.substring(0, nameLength-1) );
                     Toolkit.getDefaultToolkit().beep();
                 }
             }
         });
     }



     return field;
 }



}