/***********************************************************************************************************************
 (C) copyright by UGS Benelux

 Created by     :   E. Navis 

 Description	:   DocdownloadMenuCommand used to push the dialog
                     
 Created for    :   FEI

 Comments       :   

 Filename       :   DocDowloadMenuCommand.java
    
 Using modules  :   

 Version info   :   The number consist of the TC release and then the release
                    <release>.<pointrelease>

                    
 History of changes                                            
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 creation                                                               0.0         EN          10-01-2006
 Updated for Teamcenter 8.3.*                                           0.1         EN          16-09-2011

************************************************************************************************************************/



package com.fei.rac.commands.docdownload;

import java.awt.*;
import com.fei.rac.commands.docdownload.BnlProcessDialog;
import com.teamcenter.rac.aif.*;
import com.teamcenter.rac.aif.kernel.*;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.util.*;

public class DocDownloadMenuCommand extends AbstractAIFCommand
{
	public DocDownloadMenuCommand(Frame frParent, TCSession session)
	  {
	    //if we come here then there are no objects selected
	    noObjectsMessage();

	  }


  public DocDownloadMenuCommand(Frame frParent, InterfaceAIFComponent[] targets,String szProcess)
  {
     //Registry registry = Registry.getRegistry(this);
     //TCSession session = (TCSession)targets[0].getSession();
     //AIFDesktop desktop = (AIFDesktop)frParent;

     int count = targets.length;
     
     
     
     System.out.println("DEBUG command started");

/*typecast array ???? stupid isn't it*/

        TCComponent comps[] = new TCComponent[count];

        for (int index = 0; index < count; index++)
        {
            comps[index] = (TCComponent)targets[index];
        }
        int iTypes[]=new int[count];
        for (int index = 0; index < count; index++)
        {
           iTypes[index]=TCAttachmentType.TARGET;
        }
        
        //check type for allowed type
        
        for (int index = 0; index < count; index++)
        {
        	
        	if (isAllowedType( comps[index].getType(),szProcess )==0 )
        	{
        		wrongTypeMessage();
        		return;
        	
        	}
        	
        }
        
        

        try
        {
        	   System.out.println("DEBUG launch dialog");
    	       launchProcessDialog(frParent,comps,szProcess);
        }
        catch (Exception ex )
        {
                //Debug.printStackTrace("NEWPROCESS",ex);
                MessageBox mb = new MessageBox ( frParent, ex );
                mb.setModal ( true );
                mb.setVisible ( true );
        }

  }

//This method will get the info for the dialog.
  public void launchProcessDialog(Frame parent,TCComponent[] targets,String szProcess )
  {

    //Registry.getRegistry(this).printRegistryHierarchy();
     // Create the dialog.
     BnlProcessDialog dialog = new BnlProcessDialog(parent,targets,szProcess);
     dialog.toFront();
     dialog.setModal(true);
     dialog.setVisible(true);
  }

  private int isAllowedType(String pszType, String pszProcess)
  {
	  Registry registry = Registry.getRegistry(this);
	  
	  String[] AllowedTypes = registry.getStringArray("AllowedRevisionTypes_"+pszProcess);
	  
	  System.out.println("Found " + AllowedTypes.length );
	  
	  for (int i=0;i<AllowedTypes.length;i++)
	  {
		  System.out.println("Type: " + AllowedTypes[i] );
		  
	    if (pszType.compareTo(AllowedTypes[i])==0)
	    {
	       return 1;
	    }
	  }
	  
	  return 0;
  
  }
  
  
  private void wrongTypeMessage()
  {
	  System.out.println("DEBUG wrongtype msg dialog");
	  Registry registry = Registry.getRegistry(this);
	  String message=new String(registry.getString("wrongTypeMessage"));
	  MessageBox.post(message,"Warning",1);
  
  }
  
  private void noObjectsMessage()
  {
	  
     Registry registry = Registry.getRegistry(this);
     String message=new String(registry.getString("noObjectsMessage"));
     MessageBox.post(message,"Warning",1);
  }
}