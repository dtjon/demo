/**
 (C) copyright by Siemens PLM Software


 Description    :   Custom menu action

 Created for    :   Siemens PLM Software


 Comments       :

 Filename       :   DocDownloadMenuAction.java

 Version info   :   release.pointrelease

 History of changes
 reason /description                                        By      Version     Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                   JM      0.0         18-04-2002

 */

package com.fei.rac.common.actions;

import java.awt.*;
import com.teamcenter.rac.aif.common.actions.*;
import com.teamcenter.rac.aif.*;
import com.teamcenter.rac.aif.kernel.*;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.util.*;

public class DocDownloadMenuAction extends AbstractAIFAction {
	public DocDownloadMenuAction(AbstractAIFUIApplication theApp, Frame frParent,
			String szActionName) {
		super(theApp, frParent, szActionName);

	}

	public DocDownloadMenuAction(AbstractAIFUIApplication theApp, String szActionName) {
		super(theApp, szActionName);

	}

	public void run() {
		try {
			InterfaceAIFComponent comps[] = application.getTargetComponents();
			String szCommandKey = getCommandKey();
			AbstractAIFCommand cmd;

			String szProcess = registry.getString(this.actionName + "."
					+ "PROCEDURE");

			if (comps == null) {
				cmd = (AbstractAIFCommand) registry.newInstanceFor(
						szCommandKey, new Object[] { parent,
								(TCSession) application.getSession() });
			} else {
				cmd = (AbstractAIFCommand) registry
						.newInstanceFor(szCommandKey, new Object[] { parent,
								comps, szProcess });
			}

			cmd.executeModal();
		} catch (Exception ex) {
			MessageBox mb = new MessageBox(parent, ex);
			mb.setModal(true);
			mb.setVisible(true);
		}
	}

}