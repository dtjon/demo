/**
 (C) copyright by UGS Solutions

 Description	:  Tool to create report xml files of FEI reports.


 Created for	:   EDS PLM Benelux


 Comments		:

 Filename       :   $Id: reporter.c 454 2016-06-28 09:04:50Z tjonkonj $

 Version info   :   release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 reason /description                            Version By                  date
 ----------------------------------------------------------------------------------------------------------
 Creation                                       0.0                         04-07-2003
 Updateted to test and support all reports      0.1                         25-01-2005
 make it comaptible with templates              0.2                         11-03-2005
 chaned year in welcome and build against new
 rp module                                      0.3                         26-01-2007
 Since rp_web_bom can return NULL docs check
 for it                                         0.5                         06-02-2007
 added fixes for deleting files if no content
 is added                                       0.6                         03-03-2007
 added revrule for ecodiff docdownload          0.7                         19-03-2007
 added option onelevel_bomrep                   0.8                         02-04-2007
 make it final release 1.0 included last report
 module.                                        1.0                         25-04-2007
 Added fix for IR 1645275                       1.1                         18-02-2008
 Create download directory if it not exits      1.2                         21-02-2008
 Added ebom reports                             1.3                         13-08-2008
 Updated ECODIFF and BOMDIFF                    1.4                         20-09-2008
 Updated BNL_rp_web_where_used                  1.5                         12-06-2009
 Removed reporter.h                             1.6                         16-06-2009
 Added new reports                              1.7                         08-03-2009
 Updated where_used                             1.8                         10-09-2009
 Added compliance report                        1.9                         21-09-2009
 Updated where used report                      2.0                         15-12-2009
 Added bomrep mfg/pro                           2.1                         05-01-2010
 Added whereused_top                            2.2                         19-02-2010
 Added FRUList item report                      2.3                         26-03-2010
 Added parameter pcb_suppress_export and
  pcb_skip_target                               2.4                         27-09-2010
 Prevent the removal of index.html by
  BNL_rp_close_user_report                      2.5                         07-10-2010
  Used given revision rule for change report    2.6                         22-11-2010
 Update version number only for IR-7542280      2.7                         10-11-2015
 Use preference BNL_ENGCHANGE_REVISION          2.8                         26-11-2015
 Update for TC 10.1.*                           2.9     D.Tjon              31-05-2016

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#ifdef  TC10_1
#include <tc/iman.h>
#else
#include <iman.h>
#include <form.h>
#include <workspaceobject.h>
#include <imantype.h>
#include <item.h>
#include <ae.h>
#include <aom.h>
#include <ps.h>
#include <bom.h>
#include <IMAN_date.h>
#include <ecm.h>
#endif

#include <BNL_fm_module.h>
#include <BNL_tb_module.h>
#include <BNL_em_module.h>
#include <BNL_mm_module.h>
#include <BNL_ini_module.h>
#include <BNL_xml_module.h>
#include <BNL_csv_module.h>
#include <BNL_rp_module.h>
#include <BNL_fei_rep_parser.h>
#include <BNL_strings_module.h>


#define BNL_CHECK_ERROR(Function,RetCode)  {   \
    char *err_string = NULL;   \
    if (RetCode != 0)   \
    {   \
      fprintf(fpLog,"%s: Error_%d in %s line %d.\n",BNL_module,RetCode,#Function,__LINE__ ); \
      fprintf(stderr,"%s: Error_%d in %s line %d.\n",BNL_module,RetCode,#Function,__LINE__ ); \
    }   \
  }

static char *BNL_module="FEI Report 2.9";

static FILE *fpLog    =NULL;
static FILE *fpReport =NULL;


void check_heap(int iPos)
{
  int heapstatus=0;
  heapstatus = _heapchk();
  switch( heapstatus )
  {
    case _HEAPOK:
      printf(" %d OK - heap is fine\n",iPos );
    break;
    case _HEAPEMPTY:
      printf("%d OK - heap is empty\n",iPos );
    break;
    case _HEAPBADBEGIN:
      printf( "%d ERROR - bad start of heap\n",iPos );
    break;
    case _HEAPBADNODE:
      printf( "%dERROR - bad node in heap\n",iPos );
    break;
  }
}

void welcome()
{
  printf("Siemens PLM Software Benelux (c) 2005-2009\n");
  printf("\n");
  printf("%s\n",BNL_module);
  printf("\n");
} /* End of welcome */


void usage()
{
  printf("Usage:\n");
  printf("fei_report\n");
  printf("\nrequired parameters:\n");
  printf("-u=<user>\n");
  printf("-p=<password>\n");
  printf("-g=<group>\n");
  printf("-om=<xml|xmlhtml|html>\n");
  printf("-js=<path and name to java_script template if -om is xmlhtml>\n");
	printf("-of=<name and path for the output html file>\n");
	printf("-m=<bomrep|bomrep_ext|bomrep_mfg|ecodiff|ecodiff_ext|docsrep|attrrep|docdiff|attrdiff|bomdiff|whereused|whereused_top|ebompackedrep|ebomrep|succpred|comprep|frulist>");

	printf("\n\nbomdiff parameters\n");
	printf("-prodid=<product id>\n");
	printf("-prodrev=<product revision>\n");
	printf("[-predid=<predecessor id>]\n");
	printf("[-predrev=<predecessor revision>]\n");

	printf("\necodiff parameters\n");
	printf("-ecoid=<enigineering change order id>\n");
	printf("-ecorev=<enigineering change order revision>\n");
  printf("-revrule=<revision rule, used for report and export>\n");

  printf("\necodiff_ext parameters\n");
	printf("-ecoid=<enigineering change order id>\n");
	printf("-ecorev=<enigineering change order revision>\n");
  printf("-revrule=<revision rule, used for report and export>\n");

	printf("\ndocsrep parameters\n");
	printf("-prodid=<product id>\n");
	printf("-prodrev=<product revision>\n");

  printf("\nattrrep parameters\n");
	printf("-prodid=<product id>\n");
	printf("-prodrev=<product revision>\n");

	printf("\ndocdiff parameters\n");
	printf("-prodid=<product id>\n");
	printf("-prodrev=<product revision>\n");
	printf("[-predid=<predecessor id>]\n");
	printf("[-predrev=<predecessor revision>]\n");

  printf("\nattrdiff parameters\n");
	printf("-prodid=<product id>\n");
	printf("-prodrev=<product revision>\n");
	printf("[-predid=<predecessor id>]\n");
	printf("[-predrev=<predecessor revision>]\n");

	printf("\nbomrep, bomrep_ext, bomrep_mfg, ebompackedrep and ebomrep parameters\n");
	printf("-prodid=<product id>\n");
	printf("-prodrev=<product revision>\n");
	printf("-revrule=<revision rule>\n");
	printf("[-effdate=<effectivity date]>\n");
	printf("-level=<levels to go in -1 is all>\n");

  printf("\nwhereused parameters\n");
  printf("-prodid=<product id>\n");
	printf("-prodrev=<product revision>\n");
  printf("-level=<levels to go in -1 is all>\n");
  printf("[-filtertype=<item type to filter on>]\n");

  printf("\nwhereused_top parameters\n");
  printf("-prodid=<product id>\n");
	printf("-prodrev=<product revision>\n");
  printf("-revrule=<revision rule>\n");
  printf("[-filtertype=<item type to filter on>]\n");

  printf("\nsuccpred parameters\n");
  printf("-prodid=<product id>\n");

  printf("\ncomprep parameters\n");
  printf("-prodid=<product id>\n");
  printf("-prodrev=<product revision>\n");
  printf("-config=<validation check name>\n");

  printf("\nfrulist parameters\n");
	printf("-prodid=<product id>\n");
	printf("-prodrev=<product revision>\n");
	printf("-revrule=<revision rule>\n\n");

  printf("Optional parameters:\n");
  printf("-export_dstypes=<type>;<type>;<type>...\n");
  printf("-export_relations=<relation>;<relation>;<relation>...\n");
  printf("-namedrefs=<nr>;<nr>;<nr>...\n");
  printf("-extension_filter=<ext>;<ext>;<ext>...\n");
  printf("-dsname_filter=<naming rule>;<naming rule>;<naming rule>...\n");
  printf("-downloaddir=<directory>\n\n");
  printf("-onelevel_bomrep=<Y|N>\n");
  printf("-pcb_suppress_export=<Y|N>\n");
  printf("-pcb_skip_target=<Y|N>\n");
}/* End of usage */


int ITK_user_main(int argc, char *argv[])
{
  int iRetCode        = 0;
	int iMode			      = 0;
	int iLevel					= 0;
  int iPrintmode      = 0;
  int iComplianceResult = 0;

  char szFile[MAX_PATH];

  char *pszUserId       = NULL;
  char *pszPassId       = NULL;
  char *pszGroupId      = NULL;
  char *pszLogDir       = NULL;
  char *pszMode				  = NULL;
	char *pszOutFile			= NULL;
	char *pszProdId				= NULL;
	char *pszProdRev			= NULL;
	char *pszPredId				= NULL;
	char *pszPredRev			= NULL;
	char *pszRevRule			= NULL;
	char *pszEffDate			= NULL;
	char *pszLevel				= NULL;
  char *pszFilterType  		= NULL;
	char *pszEcoId				= NULL;
	char *pszEcoRev				= NULL;
	char *pszXMLOut				= NULL;
	char *pszScript				= NULL;
  char *pszOutMode      = NULL;
  char *pszConfig       = NULL;
  char *pszDownloadDir        = NULL;
  char *pszExportDsTypes      = NULL;
  char *pszExportRelations    = NULL;
  char *pszNamedRef           = NULL;
  char *pszExtensionFilter    = NULL;
  char *pszDsNameFilter       = NULL;
  char *pszOneLevelBomRep     = NULL;
  char *pszSkipExport   = NULL;
  char *pszPcbSkipTarget= NULL;
  char *pszFileName     = NULL;

  tag_t tProduct				= NULL_TAG;
	tag_t tPredecessor		= NULL_TAG;
	tag_t tEco				    = NULL_TAG;
	tag_t tRev					  = NULL_TAG;

  t_XMLElement *pDocument     = NULL;

  FILE  *fpXml            = NULL;
  int   iPrefValues       = 0;
  char  **pszPrefValues   = NULL;

  welcome();

  pszUserId=ITK_ask_cli_argument ("-u=");
  pszPassId=ITK_ask_cli_argument ("-p=");
  pszGroupId=ITK_ask_cli_argument ("-g=");
  pszMode=ITK_ask_cli_argument ("-m=");
  pszScript=ITK_ask_cli_argument ("-js=");
	pszOutFile=ITK_ask_cli_argument ("-of=");
  pszOutMode=ITK_ask_cli_argument ("-om=");

  pszExportDsTypes=ITK_ask_cli_argument ("-export_dstypes=");
  pszExportRelations=ITK_ask_cli_argument ("-export_relations=");
  pszNamedRef=ITK_ask_cli_argument ("-namedrefs=");
  pszExtensionFilter=ITK_ask_cli_argument ("-extension_filter=");
  pszDsNameFilter=ITK_ask_cli_argument("-dsname_filter=");
  pszDownloadDir=ITK_ask_cli_argument ("-downloaddir=");
  pszOneLevelBomRep=ITK_ask_cli_argument ("-onelevel_bomrep=");
  pszSkipExport = ITK_ask_cli_argument("-pcb_suppress_export=");
  pszPcbSkipTarget = ITK_ask_cli_argument("-pcb_skip_target=");

  BNL_rp_initialise(stderr);

  if (pszMode==NULL || pszOutFile==NULL || pszOutMode==NULL)
  {
    usage();
    printf("Missing one of the parameters:\n -m -of -om\n");
    return 1;
  }

  //determine out mode.
  if (strcmp(pszOutMode,STR_XMLHTML)==0 && pszScript==NULL )
  {
    usage();
    printf("Missing one of the parameter:\n -js\n");
    return 1;
  }

  if (strcmp(pszOutMode,STR_XMLHTML)==0)
  {
    /*check paths*/
  	if (!BNL_fm_check_file_or_dir(pszScript))
	  {
		  fprintf(stderr,"The given javascript [%s] template does not exist\n",pszScript);
		  return 1;
	  }
    iPrintmode=BNL_XMLHTML;
  }

  if (strcmp(pszOutMode,STR_HTML)==0) iPrintmode=BNL_HTML;
  if (strcmp(pszOutMode,STR_XML)==0) iPrintmode=BNL_XML;

  if (strcmp(pszMode,"bomrep")==0 || strcmp(pszMode,"bomrep_ext")==0 || strcmp(pszMode,"bomrep_mfg")==0
    || strcmp(pszMode,"ebompackedrep")==0 || strcmp(pszMode,"ebomrep")==0)
  {
    if (strcmp(pszMode,"bomrep")==0)
      iMode=BOMREP;
    else if (strcmp(pszMode,"bomrep_ext")==0)
      iMode=BOMREPEXT;
    else if (strcmp(pszMode,"bomrep_mfg")==0)
      iMode=BOMREPMFG;
    else if (strcmp(pszMode,"ebompackedrep")==0)
      iMode=EBOMPACKEDREP;
    else
      iMode=EBOMREP;

    pszProdId=ITK_ask_cli_argument ("-prodid=");
	  pszProdRev=ITK_ask_cli_argument ("-prodrev=");
	  pszRevRule=ITK_ask_cli_argument ("-revrule=");
    pszEffDate=ITK_ask_cli_argument ("-effdate=");
	  pszLevel=ITK_ask_cli_argument ("-level=");


	  if (pszEffDate==NULL)
	  {
		  pszEffDate=malloc(sizeof(char)*2);
	    *pszEffDate='\0';
	  }

	  if (pszProdId==NULL ||pszProdRev==NULL || pszRevRule==NULL || pszLevel==NULL )
	  {
		  usage();
      printf("Missing one of the parameters:\n -prodid -prodrev -revrule -level\n");
		  return 1;
	  }
  }
  else if (strcmp(pszMode,"ecodiff")==0 || strcmp(pszMode,"ecodiff_ext")==0)
  {
    if (strcmp(pszMode,"ecodiff")==0)
    {
		  iMode=ECODIFF;
    }
    else
    {
      iMode=ECODIFF_EXT;
    }

		pszEcoId=ITK_ask_cli_argument ("-ecoid=");
		pszEcoRev=ITK_ask_cli_argument ("-ecorev=");
    pszRevRule=ITK_ask_cli_argument ("-revrule=");


	  if (pszEcoId==NULL || pszEcoRev==NULL)
		{
		  usage();
      printf("Missing one of the parameters:\n -ecoid -ecorev\n");
		  return 1;
		}
  }
  else if (strcmp(pszMode,"docsrep")==0)
  {
		iMode=DOCSREP;

		pszProdId=ITK_ask_cli_argument ("-prodid=");
		pszProdRev=ITK_ask_cli_argument ("-prodrev=");


	  if (pszProdId==NULL || pszProdRev==NULL )
		{
		  usage();
      printf("Missing one of the parameters:\n -prodid -prodrev\n");
		  return 1;
		}
  }
  else if (strcmp(pszMode,"attrrep")==0)
  {
		iMode = ATTRREP;

		pszProdId = ITK_ask_cli_argument("-prodid=");
		pszProdRev = ITK_ask_cli_argument("-prodrev=");


	  if (pszProdId == NULL || pszProdRev == NULL )
		{
		  usage();
      printf("Missing one of the parameters:\n -prodid -prodrev\n");
		  return 1;
		}
  }
  else if (strcmp(pszMode,"docdiff")==0)
  {
		iMode=DOCDIFF;

		pszProdId=ITK_ask_cli_argument ("-prodid=");
		pszProdRev=ITK_ask_cli_argument ("-prodrev=");
	  pszPredId=ITK_ask_cli_argument ("-predid=");
		pszPredRev=ITK_ask_cli_argument ("-predrev=");


	  //if (pszProdId==NULL || pszProdRev==NULL || pszPredId==NULL || pszPredRev==NULL)
    if (pszProdId==NULL || pszProdRev==NULL)
		{
		  usage();
      printf("Missing one of the parameters: -prodid -prodrev\n");
		  return 1;
		}
  }
  else if (strcmp(pszMode, "attrdiff")==0)
  {
		iMode = ATTRDIFF;

		pszProdId = ITK_ask_cli_argument("-prodid=");
		pszProdRev = ITK_ask_cli_argument("-prodrev=");
	  pszPredId = ITK_ask_cli_argument("-predid=");
		pszPredRev = ITK_ask_cli_argument("-predrev=");


	  //if (pszProdId == NULL || pszProdRev == NULL || pszPredId == NULL || pszPredRev == NULL)
    if (pszProdId == NULL || pszProdRev == NULL)
		{
		  usage();
      printf("Missing one of the parameters: -prodid -prodrev\n");
		  return 1;
		}
  }
  else if (strcmp(pszMode,"bomdiff")==0)
  {
		iMode=BOMDIFF;

		pszProdId=ITK_ask_cli_argument ("-prodid=");
		pszProdRev=ITK_ask_cli_argument ("-prodrev=");
	  pszPredId=ITK_ask_cli_argument ("-predid=");
		pszPredRev=ITK_ask_cli_argument ("-predrev=");


	  //if (pszProdId==NULL || pszProdRev==NULL || pszPredId==NULL || pszPredRev==NULL)
    if (pszProdId==NULL || pszProdRev==NULL)
		{
		  usage();
      printf("Missing one of the parameters: -prodid -prodrev\n");
		  return 1;
		}
  }
  else if (strcmp(pszMode,"whereused")==0)
  {
    iMode=WHEREUSED;

		pszProdId=ITK_ask_cli_argument ("-prodid=");
		pszProdRev=ITK_ask_cli_argument ("-prodrev=");
    pszLevel=ITK_ask_cli_argument ("-level=");
    pszFilterType=ITK_ask_cli_argument ("-filtertype=");

    if (pszLevel == NULL)
    {
      iLevel = 1;
    }
    else
    {
      iLevel = atoi(pszLevel);
      if (iLevel < -1) iLevel = -1;
    }

	  if (pszProdId==NULL || pszProdRev==NULL)
		{
		  usage();
      printf("Missing one of the parameters: -prodid -prodrev\n");
		  return 1;
		}
  }
  else if (strcmp(pszMode, "whereused_top")==0)
  {
    iMode = WHEREUSEDTOP;

		pszProdId = ITK_ask_cli_argument ("-prodid=");
		pszProdRev = ITK_ask_cli_argument ("-prodrev=");
    pszRevRule = ITK_ask_cli_argument ("-revrule=");
    pszFilterType = ITK_ask_cli_argument ("-filtertype=");

    iLevel = -2;

	  if (pszProdId == NULL || pszProdRev == NULL || pszRevRule == NULL)
		{
		  usage();
      printf("Missing one of the parameters: -prodid -prodrev -revrule\n");
		  return 1;
		}
  }
  else if (strcmp(pszMode, "succpred")==0)
  {
    iMode = SUCCPRED;

		pszProdId = ITK_ask_cli_argument("-prodid=");

	  if (pszProdId==NULL)
		{
		  usage();
      printf("Missing the parameter: -prodid\n");
		  return 1;
		}
  }
  else if (strcmp(pszMode, "comprep")==0)
  {
    iMode = COMPREP;

		pszProdId = ITK_ask_cli_argument("-prodid=");
    pszProdRev = ITK_ask_cli_argument("-prodrev=");
    pszConfig = ITK_ask_cli_argument("-config=");

	  if (pszProdId == NULL || pszProdRev == NULL || pszConfig == NULL)
		{
		  usage();
      printf("Missing one of the parameters: -prodid -prodrev -config\n");
		  return 1;
		}
  }
  else if (strcmp(pszMode, "frulist")==0)
  {
     iMode=FRULISTITEM;

     pszProdId=ITK_ask_cli_argument ("-prodid=");
	   pszProdRev=ITK_ask_cli_argument ("-prodrev=");
	   pszRevRule=ITK_ask_cli_argument ("-revrule=");

     if (pszProdId==NULL ||pszProdRev==NULL || pszRevRule==NULL)
	  {
		  usage();
      printf("Missing one of the parameters:\n -prodid -prodrev -revrule -level\n");
		  return 1;
	  }
  }
  else
	{
		usage();
    printf("Unknown mode\n");
		return 1;
	}

  if (pszExportDsTypes!=NULL)
  {
    int iDsTypes            =0;
    char **pszDsTypes       =NULL;
    int iRels               =0;
    char **pszRelations     =NULL;
    int iNrs                =0;
    char **pszNrs           =NULL;
    int iExtFilter          =0;
    int iDsNameFilter       =0;
    char **pszExtFilter     =NULL;
    char **pszNameFilter    =NULL;

    if (pszNamedRef==NULL || pszExportRelations==NULL || pszDownloadDir==NULL )
    {
      usage();
      printf("Missing one of the parameters: -export_relations -namedrefs= -downloaddir=\n");
      return 1;
    }

    BNL_rp_parse_parameter(pszExportDsTypes,&iDsTypes,&pszDsTypes);

    BNL_rp_parse_parameter(pszExportRelations,&iRels,&pszRelations);

    BNL_rp_parse_parameter(pszNamedRef,&iNrs,&pszNrs);

    if (pszExtensionFilter!=NULL)
    {
      BNL_rp_parse_parameter(pszExtensionFilter,&iExtFilter,&pszExtFilter);
    }

    if (pszDsNameFilter != NULL)
    {
      BNL_rp_parse_parameter(pszDsNameFilter, &iDsNameFilter, &pszNameFilter);
    }

    BNL_rp_set_docdownload_mode(1);
    BNL_rp_set_onelevel_bomreps(1);

    //do not free given arrays and strings.
    BNL_rp_set_docdownload_types(iDsTypes,pszDsTypes);
    BNL_rp_set_docdownload_nrs(iNrs,pszNrs);
    BNL_rp_set_docdownload_rels(iRels,pszRelations);

    if (!BNL_fm_check_file_or_dir(pszDownloadDir))
    {
      // Directory does not exist, create it
      printf("Creating directory: [%s]\n", pszDownloadDir);
      if (BNL_fm_create_directory(pszDownloadDir) != 0)
      {
        printf("Error, failed to create directory [%s].\n", pszDownloadDir);
        return 1;
      }
    }
    BNL_rp_set_downloaddir(pszDownloadDir);
    BNL_rp_set_docdownload_extfilter(iExtFilter,pszExtFilter);
    BNL_rp_set_docdownload_dsnamefilter(iDsNameFilter, pszNameFilter);

    BNL_rp_open_user_report(pszOutFile);
  }

  pszLogDir=getenv("TEMP");

  pszFileName = USER_new_file_name("feireport", "log", "log", 1);
  sprintf(szFile, "%s%s%s", pszLogDir, FILESEP, pszFileName);

  fpLog=fopen(szFile,"w+t");

  if (fpLog==NULL)
  {
    printf("Error while opening log file [%s] \n",szFile);
    return 1;
  }
  /*unbuff it*/
  setvbuf(fpLog , NULL, _IONBF, 0 );

  printf("Log file [%s] opened\n", szFile);

  //shouldnt this comment out ?
  //fpLog=stdout;

  BNL_fm_initialise(fpLog);
  BNL_em_initialise(fpLog);
  BNL_mm_initialise(fpLog);
  BNL_tb_initialise(fpLog);
  BNL_csv_initialise(fpLog, NULL);

  BNL_rp_set_log(fpLog);

  iRetCode = ITK_initialize_text_services(0);
  BNL_CHECK_ERROR(ITK_initialize_text_services,iRetCode);


  if (pszUserId==NULL || pszPassId==NULL || pszGroupId==NULL)
  {
#if defined(TC8000_3_0) || defined(TC10_1)
    iRetCode = TC_auto_login();
#else
    iRetCode = ITK_auto_login();
#endif
    if ( iRetCode )
    {
      printf ("TcEng Login fail\n"  );
      return 1;
    }
    else
    {
      printf ("Logged into TcEng...\n" );
    }
  }
  else
  {
    /*log in iman database*/
#ifdef TC8000_3_0
    iRetCode=TC_init_module(pszUserId,pszPassId,pszGroupId);
#else
    iRetCode=ITK_init_module(pszUserId,pszPassId,pszGroupId);
#endif
    BNL_CHECK_ERROR(ITK_init_module,iRetCode);
  }

  /*set bypass on*/
  iRetCode=ITK_set_bypass(true);
  BNL_CHECK_ERROR(ITK_set_bypass,iRetCode);

  printf("Bypass is setup\n");


  /*open the output file little bit overdone*/
  sprintf(szFile,"%s%sfeireport.html",pszLogDir,FILESEP);

 	BNL_fm_open_file(&fpXml,pszOutFile,NULL,"w+t",1);

	if (fpXml==NULL)
	{
    printf("Error creating file [%s], create file [%s]\n",pszOutFile,szFile);
    BNL_fm_open_file(&fpXml,szFile,NULL,"w+t",1);
    pszOutFile=BNL_strings_copy_string(szFile);
    printf("Output file [%s] opened\n",pszOutFile);
  }
	else
	{
		printf("Output file [%s] opened\n",pszOutFile);
  }

  if (pszOneLevelBomRep!=NULL && (pszOneLevelBomRep[0]=='y' || pszOneLevelBomRep[0]=='Y') )
  {
    BNL_rp_set_onelevel_bomreps(1);
  }
  else
  {
    BNL_rp_set_onelevel_bomreps(0);
  }

  if (pszSkipExport != NULL && (pszSkipExport[0] == 'y' || pszSkipExport[0] == 'Y') )
  {
    fprintf(fpLog, "Skipping export when pcb export was started.\n",pszOutFile);
    BNL_rp_set_docdownload_skip_export(1);
  }
  else
  {
    BNL_rp_set_docdownload_skip_export(0);
  }

  if (pszPcbSkipTarget != NULL && (pszPcbSkipTarget[0] == 'y' || pszPcbSkipTarget[0] == 'Y') )
  {
    fprintf(fpLog, "Skipping target in case of an pcb export.\n",pszOutFile);
    BNL_rp_set_pcb_skip_target(1);
  }
  else
  {
    BNL_rp_set_pcb_skip_target(0);
  }

  switch (iMode)
	{
    case ECODIFF:
    case ECODIFF_EXT:

      if (pszRevRule!=NULL)
      {
        BNL_rp_set_downloadrevrule(pszRevRule);
      }
      else
      {
        BNL_rp_set_downloadrevrule(DEFAULT_REVRULE);
      }
      /* find the eco in db */
      iRetCode = ITEM_find_rev(pszEcoId, pszEcoRev, &tEco);
      if (tEco!=NULLTAG)
      {
        char szType[WSO_name_size_c + 1];

        // validate processing type
        iRetCode = BNL_tb_pref_ask_char_values(BNL_REP_CHANGE_REV_TYPES_PREF, TC_preference_site, &iPrefValues, &pszPrefValues);
        if( iPrefValues == 0)
        {
          fprintf(fpLog, "Error: There are no values specified in preference [%s].\n", BNL_REP_CHANGE_REV_TYPES_PREF);
          goto program_exit;
        }
        iRetCode = BNL_tb_determine_object_type(tEco, szType);
        if (!BNL_tb_value_in_list(szType, iPrefValues, pszPrefValues))
        {
          fprintf(fpLog, "Error: Type [%s] is not defined in preference [%s].\n", szType, BNL_REP_CHANGE_REV_TYPES_PREF);
          goto program_exit;
        }

        if (!BNL_rp_is_released(tEco))
        {
          printf("ECO is not released, using [%s] for download.\n", DEFAULT_REVRULE);
          BNL_rp_set_downloadrevrule("Latest Working");
        }
        //iRetCode=BNL_rp_web_eco_diffs(tEco,DEFAULT_REVRULE,&pszXMLOut,&pDocument);
        if (pszRevRule != NULL)
        {
          iRetCode = BNL_rp_web_eco_diffs(tEco, pszRevRule, &pszXMLOut, &pDocument);
        }
        else
        {
          iRetCode = BNL_rp_web_eco_diffs(tEco, DEFAULT_REVRULE, &pszXMLOut, &pDocument);
        }
      }
      else
      {
        printf("ECO [%s] [%s] not found the db\n", pszEcoId, pszEcoRev);
        goto program_exit;
      }
    break;

    case BOMREP:
    case BOMREPEXT:
    case EBOMPACKEDREP:
    case EBOMREP:
    case BOMREPMFG:
      iLevel=atoi(pszLevel);

		  /*find the rev in db*/
		  iRetCode=ITEM_find_rev(pszProdId,pszProdRev,&tRev);

      if (tRev!=NULLTAG)
      {
        iRetCode=BNL_rp_web_bom(iMode,tRev,pszRevRule,pszEffDate,iLevel,1,&pszXMLOut,&pDocument,1);
      }
      else
      {
        printf("Product [%s] [%s] not found the db\n",pszProdId,pszProdRev);
        goto program_exit;
      }
    break;

    case DOCSREP:

		  /*find the rev in db*/
		  iRetCode=ITEM_find_rev(pszProdId,pszProdRev,&tRev);

      if (tRev!=NULLTAG)
      {
        iRetCode=BNL_rp_web_docs(tRev,&pszXMLOut,&pDocument);
      }
      else
      {
        printf("Product [%s] [%s] not found the db\n",pszProdId,pszProdRev);
        goto program_exit;
      }
    break;

    case ATTRREP:

		  /*find the rev in db*/
		  iRetCode = ITEM_find_rev(pszProdId, pszProdRev, &tRev);

      if (tRev != NULLTAG)
      {
        iRetCode = BNL_rp_web_prod_attr(tRev, &pszXMLOut, &pDocument);
      }
      else
      {
        printf("Product [%s] [%s] not found the db\n", pszProdId, pszProdRev);
        goto program_exit;
      }
    break;

    case DOCDIFF:

  		iRetCode=ITEM_find_rev(pszProdId,pszProdRev,&tProduct);

      if (pszPredId != NULL && pszPredRev != NULL)
      {
        iRetCode=ITEM_find_rev(pszPredId,pszPredRev,&tPredecessor);

        if (tPredecessor==NULLTAG)
        {
          printf("Warning, predecessor [%s] [%s] not found the db\n",pszPredId,pszPredRev);
        }
      }

      //if (tProduct!=NULLTAG && tPredecessor!=NULLTAG )
      if (tProduct!=NULLTAG)
      {
		    iRetCode=BNL_rp_web_docdiff(tProduct,tPredecessor,&pszXMLOut,&pDocument);
      }
      else
      {
        printf("Product [%s] [%s] not found the db\n",pszProdId,pszProdRev);
        goto program_exit;
      }
    break;

    case ATTRDIFF:

  		iRetCode = ITEM_find_rev(pszProdId, pszProdRev, &tProduct);

      if (pszPredId != NULL && pszPredRev != NULL)
      {
	  	  iRetCode = ITEM_find_rev(pszPredId, pszPredRev, &tPredecessor);

        if (tPredecessor==NULLTAG)
        {
          printf("Warning, predecessor [%s] [%s] not found the db\n",pszPredId,pszPredRev);
        }
      }

      //if (tProduct != NULLTAG && tPredecessor != NULLTAG )
      if (tProduct != NULLTAG)
      {
        iRetCode = BNL_rp_web_attrdiff(tProduct, tPredecessor, &pszXMLOut, &pDocument);
      }
      else
      {
        //printf("Product [%s] [%s] or predecessor [%s] [%s] not found the db\n",pszProdId,pszProdRev,pszPredId,pszPredRev);
        printf("Product [%s] [%s] not found the db\n",pszProdId,pszProdRev,pszPredId,pszPredRev);
        goto program_exit;
      }
    break;

	  case BOMDIFF:

	 	  iRetCode=ITEM_find_rev(pszProdId,pszProdRev,&tProduct);

      if (pszPredId != NULL && pszPredRev != NULL)
      {
		    iRetCode=ITEM_find_rev(pszPredId,pszPredRev,&tPredecessor);

        if (tPredecessor==NULLTAG)
        {
          printf("Warning, predecessor [%s] [%s] not found the db\n",pszPredId,pszPredRev);
        }
      }

      //if (tProduct!=NULLTAG && tPredecessor!=NULLTAG )
      if (tProduct!=NULLTAG)
      {
        iRetCode=BNL_rp_web_bomdiff(tProduct,tPredecessor, DEFAULT_REVRULE,&pszXMLOut,&pDocument);
        fprintf(fpLog,"returns:[%s]",pszXMLOut);
      }
      else
      {
        printf("Product [%s] [%s] not found the db\n",pszProdId,pszProdRev);
        goto program_exit;
      }
    break;

    case WHEREUSED:
    case WHEREUSEDTOP:

      iRetCode=ITEM_find_rev(pszProdId,pszProdRev,&tProduct);

      if (tProduct!=NULLTAG)
      {
		    iRetCode = BNL_rp_web_where_used(1, iLevel, pszFilterType, pszRevRule, tProduct, &pszXMLOut, &pDocument);
      }
      else
      {
        printf("Product [%s] [%s] not found the db\n",pszProdId,pszProdRev);
        goto program_exit;
      }
    break;

    case SUCCPRED:

      // Get the first created ItemRevision

      iRetCode = ITEM_find_item(pszProdId, &tProduct);

      if (tProduct != NULL_TAG)
      {
        iRetCode = BNL_rp_web_succ_pred(tProduct, &pszXMLOut, &pDocument);
      }
      else
      {
        printf("Product [%s] not found the db\n" ,pszProdId);
        goto program_exit;
      }
    break;

    case COMPREP:

      // Get the first created ItemRevision
      iRetCode = ITEM_find_rev(pszProdId, pszProdRev, &tProduct);

      if (tProduct != NULL_TAG)
      {
        iRetCode = BNL_rp_web_comprep(1, &tProduct, 0, pszConfig, &pszXMLOut, &pDocument, &iComplianceResult);
      }
      else
      {
        printf("Product [%s] not found the db\n" ,pszProdId);
        goto program_exit;
      }
    break;

    case FRULISTITEM:
      iLevel = -1;
      /*find the rev in db*/
		  iRetCode=ITEM_find_rev(pszProdId,pszProdRev,&tRev);

      if (tRev!=NULLTAG)
      {
        iRetCode=BNL_rp_web_frulist_item(tRev,pszRevRule,&pszXMLOut,&pDocument);
      }
      else
      {
        printf("Product [%s] [%s] not found the db\n",pszProdId,pszProdRev);
        goto program_exit;
      }
    break;

	  default:
		break;
  }

  // DEBUG
  //if (iMode == COMPREP)
  //{
    //BNL_xml_load_document("D:\\development\\projects\\fei\\BNL_fei_reporter\\build\\tceng10038\\comprep.xml", &pDocument);
    //BNL_xml_convert_xml_to_buffer(pDocument, &iComplianceResult,&pszXMLOut);
  //}

  if (pDocument!=NULL)
  {
    switch(iPrintmode)
    {
      case BNL_XML:
        BNL_fei_xml_write_document(fpXml,pszXMLOut,iMode,pszMode,pszRevRule,pszEffDate);
      break;

      case BNL_HTML:
        BNL_fei_rep_parse_html(fpXml,pDocument,iMode,iLevel,pszConfig,pszFilterType,pszRevRule);
      break;

      case BNL_XMLHTML:
        BNL_fei_write_xmlhtml(fpXml,pszMode,pszXMLOut,iMode,pszScript,pszRevRule,pszEffDate);
      break;

      default:
        printf("Undefined -om mode found\n");
      break;
    }

    //free xml struc
    BNL_xml_close_document(pDocument);

    //free string
    if (pszXMLOut!=NULL) free(pszXMLOut);
  }
  else
  {
    fclose(fpXml);
    //delete the opened fpXml file
    BNL_fm_delete_file(NULL,pszOutFile);
    fpXml=NULL;

    //not very clean, but two kinds of malloc used so we don't free
    // JM|07-10-2010: Prevent the removal of index.html by BNL_rp_close_user_report
    //pszOutFile=NULL;
  }

  if (pszExportDsTypes!=NULL)
  {
    BNL_rp_close_user_report(pszOutFile);
  }

program_exit:
  if (iPrefValues > 0) MEM_free(pszPrefValues);

  fprintf(fpLog,"Closed program.\n");

  if (ITK_exit_module(true) != ITK_ok)
  {
    printf("Error logging out...\n");
  }
  printf("Closed program.\n");


  //return iRetCode;

	if (fpXml!=NULL)fclose(fpXml);
  fclose(fpLog);

  return iRetCode;
} /* End of main */



/*

  parameters for generate a out file as tested


    -u=infodba -p=infodba -g=dba -om=html -of=C:\temp\report.html -m=docsrep -prodid=12345678910 -prodrev=A

    -u=infodba -p=infodba -g=dba -om=html -of=D:\temp\docdownload\report.html -m=docsrep -prodid="1234 5567 8910" -prodrev=A -export_dstypes="PDF;MSWord;MSExcel" -namedrefs="word;excel;PDF" -export_relations="IMAN_specification" -downloaddir=D:\temp\docdownload

    -u=infodba -p=infodba -g=dba -om=html -of=D:\temp\docdownload\report.html -m=bomrep -prodid="1234 5567 8910" -prodrev=A -revrule="Latest Working" -level=-1 -export_dstypes="PDF;MSWord;MSExcel" -namedrefs="word;excel;PDF" -export_relations="IMAN_specification" -downloaddir=D:\temp\docdownload
    -u=infodba -p=infodba -g=dba -om=html -of=D:\temp\docdownload\report.html -m=bomrep -prodid="1234 5567 8910" -prodrev=B -revrule="Latest Working" -level=-1 -export_dstypes="PDF;MSWord;MSExcel" -namedrefs="word;excel;PDF" -export_relations="IMAN_specification" -downloaddir=D:\temp\docdownload
    -u=infodba -p=infodba -g=dba -om=html -of=D:\temp\docdownload\report.html -m=bomrep -prodid="1234 5567 8910" -prodrev=B -revrule="Latest Working" -level=-1 -export_dstypes="PDF;MSWord;MSExcel" -namedrefs="word;excel;PDF" -export_relations="IMAN_specification" -extension_filter=doc;par -downloaddir=D:\temp\docdownload

    -u=infodba -p=infodba -g=dba -om=html -of=D:\temp\docdownload\report.html -m=ecodiff -ecoid=CR0001 -ecorev=A -export_dstypes="PDF;MSWord;MSExcel" -namedrefs="word;excel;PDF" -export_relations="IMAN_specification" -downloaddir=D:\temp\docdownload
    -u=infodba -p=infodba -g=dba -om=html -of=D:\temp\docdownload\report.html -m=ecodiff -ecoid=CR0001 -ecorev=A -export_dstypes="PDF;MSWord;MSExcel" -namedrefs="word;excel;PDF" -export_relations="IMAN_specification" -downloaddir=D:\temp\docdownload
    -u=infodba -p=infodba -g=dba -om=html -of=D:\temp\docdownload\report.html -m=ecodiff -ecoid=CR0001 -ecorev=A -export_dstypes="PDF;MSWord;MSExcel" -namedrefs="word;excel;PDF" -export_relations="IMAN_specification" -downloaddir=D:\temp\docdownload

    -u=infodba -p=infodba -g=dba -om=html -of=C:\temp\report.html -m=bomdiff -prodid=123455678910 -prodrev=A -predid=123455678910 -predrev=B -revrule="Latest Working" -level=-1

    -u=infodba -p=infodba -g=dba -om=html -of=C:\temp\report.html -m=whereused -prodid=1234567894 -prodrev=A

*/
