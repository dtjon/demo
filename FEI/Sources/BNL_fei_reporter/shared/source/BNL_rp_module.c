/**
 (C) copyright by EDS PLM Solutions

 Description  :  Replaces the given new ItemRevision with the given
                   old revision in all bomviews.


 Created for  :   FEI Eindhoven


 Comments     :

 Filename     :   $Id: BNL_rp_module.c 456 2016-06-28 09:06:20Z tjonkonj $

 Version info :   release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 reason /description                            by         version     date
 ----------------------------------------------------------------------------------------------------------
 Creation                                       EN          0.0        04-07-2003
 functions implenented in module                EN          0.1        12-09-2003
 updated working for ec report                  EN          0.2        18-09-2003
 Update it so another xml struc is created
 from templates so the showed props are
 configurable                                   EN          0.3        23-09-2003
 hanged pred and prod for eco report to get
 the right + and -
 added level field for boms                     EN          0.4        01-10-2003
 Bomlevel for report bom changes configureable
 If NULL_TAGs for predecessor come in then see
 all docs as added.                             EN          0.5        13-10-2003
 Forgot to give in bomrep diff the level by get
 items

 used in BNL_rp_web_eco_diffs a -> so and not xx[].
 in this way the predecessor and succesor where
 always the same

 for got to return full count solution items and
 revision items                                 EN          0.6        22-10-2003

  forgot to reset iFound switch in loop
  and change the position of quatitty change
  check
  comment out freeing of mem temponarly         EN          0.7        27-10-2003
 comparing bomlines, compare also sequence      EN          0.8        31-10-2003
 bomlines quantity 1 and not filled is the same
 adding oldquantity in case if change is 0      EN          0.9        21-11-2003
 counter mismatch                               EN          1.0        19-12-2003
 found another counter mismatch                 EN          1.1        06-01-2004
 delete unreferenced variables                  EN          1.2        23-03-2004
 If there is no predecesssor then display
 all thing as + added
 BNL_rp_web_bom changed so it will accept a
 bomview tag and also revisieions               EN          1.3        03-05-2004
 Updated report function so they give back xml
 structure                                      EN          1.4        23-11-2006
 Add docdownload mode                           EN          1.5        12-12-2006
 change handling of getting properties          EN          1.6        05-01-2007
 added pcb command                              EN          1.7        10-01-2007
 added in pcb command workdir                   EN          1.8        12-01-2007
 Found mistake in pcb recognisation             EN          1.9        12-01-2007
 replacement of illegal chars placed
 at other position.                             EN          2.0        17-01-2007
 changed function BNL_rp_export_docs so it
 will export also the product if is pcb         EN          2.1        25-01-2007
 changed export cmd to exportdir, itemrev, item EN          2.2        26-01-2007
 Added change so also bomreps can export
 onelevel bomviews                              EN          2.3        02-02-2007
 Take revision rule over in one level bomreps   EN          2.4        06-02-2007
 Ignore unconfigured revs report them as id/??? EN          2.5        06-02-2007
 No one level bomrep in case there is no bom    EN          2.7        06-02-2007
 If level is 0 then only top level bomreport
 should be made                                 EN          2.8        13-02-2007
 reporting onelevels structure changed          EN          2.9        13-02-2007
 added fixed fields 2d 3d and latest_id         EN          3.0        15-02-2007
 added fixes if no report content then
 reports are deleted. Onlevel bomreps for
 ecodiff,accept multiple predecessors           EN          3.1        03-03-2007
 found allocation bug                           EN          3.2        12-03-2007
 Solved issue in productline ending with
 obsolete status.                               EN          3.3        12-03-2007
 The relation of the eco, followed wrong tag    EN          3.4        12-03-2007
 if predecessor is NULL_TAG do extra filter
 check                                          EN          3.5        12-03-2007
 Added the extension export filter              EN          3.6        12-03-2007
 If the it is a JT dataset DirectModel then
 do only export at some status                  EN          3.7        12-03-2007
 If the end object of productline has status
 obsolete NULL_TAG is returned                  EN          3.8        14-03-2007
 logic of test to check export only added
 documents                                      EN          3.9        16-03-2007
 Issue release_date correction solved           EN          4.0        16-03-2007
 added release_state in properties              EN          4.1        16-03-2007
 Updated ecodiff also do pcb export             EN          4.2        19-03-2007
 Changed getting state of task, Look for
 Started state in place of Pending              EN          4.3        21-03-2007
 UG2D data from latest rev.                     EN          4.4        21-03-2007
 Do not print latest if product is same         EN          4.5        21-03-2007
 Prevent double creation of oneleve bom rep in
 BNL_rp_create_onelevel_bomreport               EN          4.6        21-03-2007
 Add latest props                               EN          4.7        22-03-2007
 Get ecos from latest revision                  EN          4.8        22-03-2007
 replaced depricated CFM_set_date function      EN          4.9        30-03-2007
 Updated EWU following flowchart
 systemconcept EWU 0.6                          EN          5.0        24-04-2007
 Updated after  development tests               EN          5.1        01-05-2007
 make BNL_id_is_higher alphanummeric sensetive  EN          5.2        04-05-2007
 removed some debug prints for whereused        EN          5.3        14-05-2007
 Skip the export of a named reference with no
 extension                                      JM          5.4        13-05-2008
 Added refresh of dataset before export         JM          5.5        15-05-2008
 Updated for generation of ebom reports         JM          5.6        13-08-2008
 Close temportay file before deleting           JM          5.7        28-11-2008
 Added refresh                                  JM          5.8        04-12-2008
 Added refresh (2)                              JM          5.9        05-01-2009
 Changed eco retrieval                          JM          6.0        10-02-2009
 Updated BNL_rp_web_bomdiff and
  BNL_rp_web_eco_diffs                          JM          6.1        20-05-2009
 Updated BNL_rp_create_xml_props                JWO         6.2        10-06-2009
 Added attrdiff report                          JWO         6.3        11-06-2009
 Added level to BNL_rp_web_where_used           JM          6.4        12-06-2009
 Added product attributes report                JWO         6.5        15-06-2009
 Added successor/predecessor report             JWO         6.6        16-06-2009
 Added special property has_bom                 JM          6.7        17-06-2009
 Fixed issue with IMAN_ROOT and with display
  value                                         JM          6.8        07-06-2009
 Updated where_used                             JM          6.9        10-09-2009
 Update bom reports                             JM          7.0        17-09-2009
 Not unpack for ebompackedrep                   JM          7.1        18-09-2009
 Update ecodiff report                          JM          7.2        21-09-2009
 Updated BNL_rp_attr_2_check                    JM          7.3        26-10-2009
 Added bom check                                JM          7.4        30-10-2009
 Added one_child_check                          JM          7.5        10-11-2009
 Fixed issue with [Empty];[Empty]               JM          7.6        12-11-2009
 Added error description to compliance report   JM          7.7        02-12-2009
 Enhanced bom check to check also bomline props JM          7.8        09-12-2009
 Fixed issue with pszRefDes                     JM          7.9        12-10-2009
 Updated BNL_rp_get_prop_type                   JM          8.0        22-12-2009
 Added bomrep mfg/pro                           JM          8.1        05-01-2010
 Use bl_bomview_rev instead of bl_bomview       JM          8.2        12-01-2010
 Extended status check when directmodel         JM          8.3        14-01-2010
 Updated display of errors in comp report       JM          8.4        01-03-2010
 Added FRUList item report                      JWO         8.5        26-03-2010
 Update of the movement of latest revisions     JM          8.6        13-05-2010
 Skip given Item types when retrieving bom      JM          8.7        07-08-2010
 Added bomrep_mfg                               JM          8.8        31-08-2010
 Updated the pcb export                         JM          8.9        01-09-2010
 Changed back ITEMREVISION to ItemRevision      JM          9.0        06-09-2010
 Updated BNL_pcb_export (1)                     JM          9.1        06-09-2010
 Updated BNL_pcb_export (2)                     JM          9.2        09-09-2010
 Updated BNL_pcb_export (3) and
  BNL_revision_download                         JM          9.3        27-09-2010
 Added extra logging                            JM          9.4        26-08-2011
 Updated for TC 8.3.*                           JM          9.5        16-09-2011
 Updated BNL_rp_xmlstuc_to_string               JM          9.6        11-11-2011
 Fixed MEM issue                                JM          9.7        28-11-2011
 Fixed issue with fru report                    JM          9.8        09-01-2011
 Implemented workaround for IR 1894974          JM          9.9        15-05-2012
 changed EC_<relations>                         DTJ        10.0        04-11-2015
 Use define BNL_ENGCHANGE_REVISION              DTJ        10.1        26-11-2015
 Bugs fixing:
   -report old ECO details
   -removal of Impacted items(CMHasImpactedItem)
    information, only solution item information
    for new change in the reports               JWO        10.2        31-03-2016
 Update for TC 10.1.*                           DTJ        10.3        31-05-2016
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/stat.h>


#ifdef  TC10_1
#include <tc/iman.h>
#include <ecm/ecm.h>
#include <fclasses/tc_date.h>
#include <property/nr.h>
#include <tccore/aom_prop.h>
#else
#include <iman.h>
#include <form.h>
#include <workspaceobject.h>
#include <imantype.h>
#include <item.h>
#include <ae.h>
#include <aom.h>
#include <ps.h>
#include <bom.h>
#include <IMAN_date.h>
#include <ecm.h>
#include <cfm.h>
#include <imanfile.h>
#include <aom_prop.h>
#include <nr.h>
#endif

#include <BNL_fm_module.h>
#include <BNL_tb_module.h>
#include <BNL_em_module.h>
#include <BNL_mm_module.h>
#include <BNL_ini_module.h>
#include <BNL_rp_module.h>
#include <BNL_xml_module.h>
#include <BNL_csv_module.h>
#include <BNL_errors.h>

#include <BNL_strings_module.h>
#include <BNL_fei_rep_parser.h>

static char BNL_module[]     ="FEI RP 10.3";

static FILE *rplog         = NULL;
static FILE *fpUserRep     = NULL;


static giDocDownload = 0;
static giDocDownloadSkipExport = 0;
static giPcbSkipTarget = 0;
static giReported = 0;

typedef struct docdownload_s
{
    char *pszExportDir;
    int iDsTypes;
    int iDsNames;
    char **pszDsTypes;
    char **pszDsNames;
    int iNrs;
    char **pszNrs;
    int iRels;
    char **pszRels;
    int onelevelbomreps;
    int iExtTypes;
    char **pszExtTypes;
    char *pszRevRule;
} docdownload_t;

typedef struct wu_list_s
{
  int iLevel;
  int iTopLevel;
  tag_t tRevision;
  tag_t tLatestRevision;
  char szItemId[33];
  struct wu_list_s *pPrev;
  struct wu_list_s *pNext;
}wu_list_t;

static docdownload_t gDocdownload_info;

int BNL_rp_create_onelevel_bomreport(tag_t tRevision, char *pszRevRule, char *pszEffDate, int iTopLine);
// JM|01-09-2010: Not used anymore
//int is_released_pcb_or_non_pcb(tag_t tRevision);
int BNL_report_a_line(char *pszItemid,char *pszDocId,char *pszFileName,char *pszState);
int BNL_rp_get_latest_productline_revision(tag_t tProduct, tag_t *tLatestRev);
int BNL_rp_is_obsolete(tag_t tObject);
int BNL_EBOM_COMPARE(tag_t tLine1, tag_t tLine2, void *client_data);
int BNL_split_multiple_values(char *pszValue,char chSeparator, int  *iCount,char ***pszValues);
int BNL_rp_create_xml_attr_props(tag_t tObject,char *pszTemplate,char *pszAttrname,char *pszAttrval,char *pszOldAttrval,t_XMLElement **pDiff);
int BNL_rp_get_succ_rel_objs(tag_t tProduct, int *iObj,tag_t **ptObjs);
int BNL_rp_get_pred_rel_objs(tag_t tProduct, int *iObj,tag_t **ptObjs);

int BNL_rp_wso_status_check(tag_t tRev, t_XMLElement *pXml, int *piStatusCount, diffinfo_t **pStatusDiffs);
int BNL_rp_rel_status_check(tag_t tRev, t_XMLElement *pXml, int *piStatusCount, diffinfo_t **pStatusDiffs);
int BNL_rp_bvr_status_check(tag_t tRev, t_XMLElement *pXml, int iTargets, tag_t *ptTargets, int *piStatusCount, diffinfo_t **pStatusDiffs);
int BNL_rp_attr_1_check(tag_t tRev, t_XMLElement *pXml, int *piAttrCount, diffinfo_t **pAttrDiffs);
int BNL_rp_attr_2_check(tag_t tRev, t_XMLElement *pXml, int *piAttrCount, diffinfo_t **pAttrDiffs);
int BNL_rp_attr_3_check(tag_t tRev, t_XMLElement *pXml, int *piAttrCount, diffinfo_t **pAttrDiffs);
int BNL_rp_bom_check(tag_t tRev, t_XMLElement *pXml, int *piBomCount, diffinfo_t **pBomDiffs);

int BNL_rp_initialise(FILE *pLog)
{
    int iRetCode        = 0;

    if (pLog !=NULL) BNL_rp_set_log(pLog);

    gDocdownload_info.iDsTypes= 0;
    gDocdownload_info.iDsNames= 0;
    gDocdownload_info.iExtTypes= 0;
    gDocdownload_info.iNrs= 0;
    gDocdownload_info.iRels= 0;
    gDocdownload_info.onelevelbomreps= 0;
    gDocdownload_info.pszDsTypes= NULL;
    gDocdownload_info.pszExportDir= NULL;
    gDocdownload_info.pszExtTypes= NULL;
    gDocdownload_info.pszDsNames = NULL;
    gDocdownload_info.pszNrs= NULL;
    gDocdownload_info.pszRels= NULL;
    gDocdownload_info.pszRevRule= NULL;

    return iRetCode;
}

int BNL_rp_set_docdownload_mode(int iLogical)
{
    int iRetCode        = 0;

    if (iLogical) giDocDownload=1;

    return iRetCode;
}

int BNL_rp_set_docdownload_skip_export(int iLogical)
{
    int iRetCode        = 0;

    if (iLogical) giDocDownloadSkipExport=1;

    return iRetCode;
}

BNL_rp_set_pcb_skip_target(int iLogical)
{
  int iRetCode        = 0;

  if (iLogical) giPcbSkipTarget = 1;

  return iRetCode;
}

int BNL_rp_set_downloaddir(char *pszDowloadDir)
{
    int iRetCode        = 0;

    gDocdownload_info.pszExportDir=pszDowloadDir;

    return iRetCode;

}

int BNL_rp_set_downloadrevrule(char *pszRevRule)
{
    int iRetCode        = 0;

      gDocdownload_info.pszRevRule=pszRevRule;

    return iRetCode;
}

int BNL_rp_set_log(FILE *fpLog)
{
    int iRetCode        = 0;

    rplog=fpLog;

    return iRetCode     = 0;

}

char *BNL_rp_ask_version()
{
    return BNL_module;
}


int BNL_rp_set_onelevel_bomreps(int iLogical)
{
    int iRetCode        = 0;


    gDocdownload_info.onelevelbomreps=iLogical;


    return iRetCode;
}

/*do not free the given array
  after calling this function
*/
int BNL_rp_set_docdownload_types(int iDsTypes,char **pszDsTypes)
{
    int iRetCode        = 0;

    gDocdownload_info.iDsTypes=iDsTypes;
    gDocdownload_info.pszDsTypes=pszDsTypes;

    return iRetCode;
}

/*do not free the given array
  after calling this function
*/
int BNL_rp_set_docdownload_nrs(int iNrs,char **pszNrs)
{
    int iRetCode    = 0;

    gDocdownload_info.iNrs=iNrs;
    gDocdownload_info.pszNrs=pszNrs;


    return iRetCode;
}

/*do not free the given array
  after calling this function
*/
int BNL_rp_set_docdownload_rels(int iRels, char **pszRelations)
{
    int iRetCode        = 0;

    gDocdownload_info.iRels=iRels;
    gDocdownload_info.pszRels=pszRelations;
    return iRetCode;
}

int BNL_rp_set_docdownload_dsnamefilter(int iDsNames,char **pszDsNames)
{
  int iRetCode        = 0;

  gDocdownload_info.iDsNames = iDsNames;
  gDocdownload_info.pszDsNames = pszDsNames;

  return iRetCode;
}

int BNL_rp_set_docdownload_extfilter(int iExtTypes,char **pszExtTypes)
{
  int iRetCode        = 0;

  gDocdownload_info.iExtTypes=iExtTypes;
  gDocdownload_info.pszExtTypes=pszExtTypes;

  return iRetCode;
}


/*if pszParamter is NULL then a * is returned as parameter.*/

int BNL_rp_parse_parameter(char *pszParameter, int *iParameters, char ***pszParameters)
{
    int iRetCode        = 0;
    int iParams         = 0;
    char *pszNode       = NULL;
    char *pszParam      = NULL;
    char **pszParams    = NULL;
    char *pszStart      = NULL;

    if (pszParameter==NULL)
    {
        pszParams=malloc(sizeof(char*));
        pszParams[0]=BNL_strings_copy_string("*");
        *pszParameters=pszParams;
        *iParameters=1;
        return iRetCode;
    }

    pszParam=BNL_strings_copy_string(pszParameter);
    pszNode=pszParam;
    pszStart=pszParam;

    while (*pszNode!='\0')
    {
        if (*pszNode==';' || *pszNode=='\0')
        {
            *pszNode='\0';
            iParams++;

            if (pszParams==NULL)
            {
                pszParams=malloc(sizeof(char*));
                pszParams[0]=BNL_strings_copy_string(pszStart);
                pszStart=pszNode+1;
            }
            else
            {
                pszParams=realloc(pszParams,sizeof(char*)*iParams);
                pszParams[iParams-1]=BNL_strings_copy_string(pszStart);
                pszStart=pszNode+1;
            }
        }


      pszNode++;

      if (*pszNode=='\0')
      {
            iParams++;

            if (pszParams==NULL)
            {
                pszParams=malloc(sizeof(char*));
                pszParams[0]=BNL_strings_copy_string(pszStart);
                pszStart=pszNode+1;
            }
            else
            {
                pszParams=realloc(pszParams,sizeof(char*)*iParams);
                pszParams[iParams-1]=BNL_strings_copy_string(pszStart);
                pszStart=pszNode+1;
            }
        }
    }

    free(pszParam);

    *pszParameters=pszParams;
    *iParameters=iParams;

    return iRetCode;
}


int BNL_rp_get_sort_prop_value(t_XMLElement *pProps,char *pszProp,char **pszPropValue)
{
    int iRetCode        = 0;

    t_XMLElement *pProp = NULL;

    char *pszAttrVal    = NULL;


    /*if nothing found we can be sure we end back a NULL*/
    *pszPropValue= NULL;

    pProp=BNL_xml_find_first(pProps,XML_EL_PROP);

    while (pProp!=NULL)
    {
        iRetCode = BNL_xml_element_get_attribute_value(pProp,XML_ATTR_ATTR, &pszAttrVal);
        if (pszAttrVal!=NULL)
        {
            if (strcmp(pszAttrVal, pszProp)== 0)
            {


               *pszPropValue=malloc(sizeof(char)*(strlen(pProp->pszElementValue)+1));
               strcpy(*pszPropValue, pProp->pszElementValue);
               free(pszAttrVal);
               break;
            }

            free(pszAttrVal);
            pszAttrVal= NULL;

        }
        pProp=pProp->pNextSibling;

    }

    return iRetCode;
}

int BNL_rp_place_in_list(char *pszProp,diffinfo_t *pDocument,diffinfo_t *pList)
{
    int iRetCode        = 0;
    int iNotPlaced      =1;
    int iPrevComp       = 0;
    int iComp           = 0;

    diffinfo_t *pPrev       = NULL;
    diffinfo_t *pListNext   = NULL;


    char *pszListPropValue  = NULL;
    char *pszListDocProp    = NULL;


    /*get propInfo*/
    BNL_rp_get_sort_prop_value(pList->pXMLprops, pszProp, &pszListPropValue);
    BNL_rp_get_sort_prop_value(pDocument->pXMLprops, pszProp, &pszListDocProp);

    if (pszListPropValue!=NULL || pszListDocProp!=NULL)
    {
       iPrevComp=strcmp(pszListPropValue, pszListDocProp);
    }

    if (pszListPropValue!=NULL) free(pszListPropValue);


    /*first time*/
    if (pList->pNext==NULL && pList->pPrev==NULL)
    {
        if(iPrevComp==-1 || iPrevComp== 0)
        {
            if (pList!=pDocument)
            {
                pList->pNext=pDocument;
                pDocument->pPrev=pList;
            }
        }

        if (iPrevComp==1)
        {
            if (pList!=pDocument)
            {
                pList->pPrev=pDocument;
                pDocument->pNext=pList;
            }
        }

        if (pszListDocProp!=NULL) free(pszListDocProp);

        return iRetCode;
    }

    while (iNotPlaced)
    {
        /*get propinfo from pList*/

        BNL_rp_get_sort_prop_value(pList->pXMLprops, pszProp, &pszListPropValue);

        if (pszListPropValue!=NULL || pszListDocProp!=NULL)
        {
           iComp=strcmp(pszListPropValue, pszListDocProp);
        }

        if (pszListPropValue!=NULL) free(pszListPropValue);

        if (iComp==-1)
        {
            pPrev=pList;
            pList=pList->pNext;
        }

        if (iComp==1)
        {
            pPrev=pList;
            pList=pList->pPrev;
        }


        if(iComp== 0 || pList==NULL || iComp!=iPrevComp)
        {

          /*fill in*/
          if (iComp== 0 || (iComp==1 && iComp!=iPrevComp) )
          {

               /*add behind, watch out it is reversed in case we come here becuase
                 iComp==1 and iComp!=iPrevComp because icomp=1 we should expect that it
                 is a add before but this is reversed, becuase the previos loop cycle has
                 changed the direction*/


            if (pList->pNext==NULL)
            {
                 pList->pNext=pDocument;
                 pDocument->pPrev=pList;
            }
            else
            {

                pListNext=pList->pNext;
                pList->pNext=pDocument;
                pDocument->pPrev=pList;
                pDocument->pNext=pListNext;
                pListNext->pPrev=pDocument;
            }

          }/*if (iComp== 0 || (iComp==1 && iComp!=iPrevComp) )*/

          if (iComp==-1 && iComp!=iPrevComp)
          {
                /*add before, watch out it is reversed because the previos loop cycle has
                  changed the direction*/

              if (pList->pPrev==NULL)
              {
                  pList->pPrev=pDocument;
                  pDocument->pNext=pList;
              }
              else
              {
                  pListNext=pList->pPrev;
                  pList->pPrev=pDocument;
                  pDocument->pNext=pList;
                  pDocument->pPrev=pListNext;
                  pListNext->pNext=pDocument;
              }

          }/*end of if (iComp==-1 && iComp!=iPrevComp)*/

          if (pList==NULL)
          {
              if (iComp==1)
              {
                  pPrev->pPrev=pDocument;
                  pDocument->pNext=pPrev;

              }

              if (iComp==-1)
              {
                  pPrev->pNext=pDocument;
                  pDocument->pPrev=pPrev;
              }
          }

           iNotPlaced= 0;
        }/*end of if(iComp== 0 || pList==NULL || iComp!=iPrevComp)*/

        iPrevComp=iComp;
    }/*end of while (iNotPlaced)*/

    if (pszListDocProp!=NULL)free(pszListDocProp);

    return iRetCode;
}

int BNL_rp_sort_on_prop(char *pszProp, int iDiffCount, diffinfo_t *ptDocuments)
{
    int iRetCode            = 0;
    int i                   = 0;


    /*the diffinfo-t has a pnext node so make a sorted linked list of the array


    */

    for (i= 0;i<iDiffCount;i++)
    {
        iRetCode = BNL_rp_place_in_list(pszProp, &ptDocuments[i], ptDocuments);
    }/*end of for (i= 0;i<iDiffCount;i++)*/

    return iRetCode;
}

/*
int BNL_rp_get_documents
(
  tag_t tRev,               <I>
  int   iDocCount           <O>
  tag_t **ptDocuments        <OF>
)

  Description:

  Input: Product (=ItemRevision) Get all attached objects of the ITEMREVISION.
  Get all objects of class Dataset and return them. Returns a array of dataset tags.
*/
int BNL_rp_get_documents
(
  tag_t tRev,
  char  *pszMode,
  int   *iDocCount,
  diffinfo_t **ptDocuments
)
{
    int iRetCode        = 0;
    int iAttCount       = 0;
    int i               = 0;
    int iCount          = 0;

    t_XMLElement *pProps     = NULL;

    diffinfo_t *ptDocs       = NULL;

    ITEM_attached_object_t *ptAttObjs;

    char szClass[WSO_name_size_c+1];

    if (tRev==NULL_TAG)
    {
        *ptDocuments= NULL;
        iDocCount= 0;
        return iRetCode;

    }

    iRetCode = ITEM_list_all_rev_attachments(tRev, &iAttCount, &ptAttObjs);
    if (BNL_em_error_handler("ITEM_list_all_rev_attachments", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    for (i= 0;i<iAttCount;i++)
    {

        iRetCode = BNL_tb_determine_super_class(ptAttObjs[i].attachment,szClass);
        if (BNL_em_error_handler("BNL_tb_determine_super_class", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

        if (strcmp(szClass,DATASET)== 0)
        {
            iCount++;
            ptDocs=(diffinfo_t *) MEM_realloc(ptDocs, iCount * sizeof(diffinfo_t));


            BNL_rp_create_xml_props(ptAttObjs[i].attachment, pszMode,0, &pProps,NULL,NULL,NULL,NULL,NULL);

            ptDocs[iCount-1].tObject=ptAttObjs[i].attachment;
            ptDocs[iCount-1].pszQuant=BNL_strings_copy_string("");

            ptDocs[iCount-1].pszSeq=BNL_strings_copy_string("");
            ptDocs[iCount-1].pszRefDes=BNL_strings_copy_string("");
            ptDocs[iCount-1].pXMLprops=pProps;
            ptDocs[iCount-1].iChange= 0;

            ptDocs[iCount-1].pNext= NULL;
            ptDocs[iCount-1].pPrev= NULL;
        }

    }



    if (iAttCount!=0) MEM_free(ptAttObjs);


    *iDocCount=iCount;
    *ptDocuments=ptDocs;
    return iRetCode;
}


/*
int BNL_rp_get_prod_attrs
(
  tag_t tRev,               <I>
  char *pszMode             <I>
  tag_t **ptAttrs           <OF>
)

  Description:

  Input: Product (=ItemRevision)
*/
int BNL_rp_get_prod_attrs
(
  tag_t tRev,
  tag_t tPred,
  char  *pszMode,
  int *iDiff,
  diffinfo_t **ptAttrs
)
{
    int iRetCode        = 0;
    int iAttCount       = 0;
    int i               = 0;
    int iCount          = 0;
    int iVals           = 0;
    int iRels           = 0;

    t_XMLElement *pPredProps    = NULL;
    t_XMLElement *pProdProps    = NULL;
    diffinfo_t *ptDiffs     = NULL;

    t_XMLElement *pTemplate = NULL;
    t_XMLElement *pProp     = NULL;
    t_Attribute  *pAttr     = NULL;

    tag_t tProdObj = NULL_TAG;
    tag_t tPredObj = NULL_TAG;
    tag_t tItem    = NULL_TAG;

    char szKey[128];
    char szAttrName[128];
    char szType[128];
    char *pszTemplateFile   = NULL;
    char *pszTemp           = NULL;
    char *pszProdVal        = NULL;
    char *pszPredVal        = NULL;
    char *pszDisVal         = NULL;

    char **pszValues = NULL;

    logical lFound          =false;

    fprintf(rplog, "BNL_rp_get_prod_attrs\n");

    iRetCode = ITEM_ask_item_of_rev(tRev, &tItem);

    BNL_tb_determine_object_type(tItem, szType);

    sprintf(szKey, "BNL_prop_%s_%s",szType, pszMode);

    iRetCode = PREF_ask_char_value(szKey,0, &pszTemp);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (pszTemp==NULL)
    {
      fprintf(rplog, "The preference [%s] was not defined, so there is no template to create properties structure\n",szKey);
      return iRetCode;
    }

    BNL_tb_replace_env_for_path(pszTemp, &pszTemplateFile);
    if (pszTemplateFile==NULL)
    {
      pszTemplateFile=pszTemp;
    }
    else
    {
      MEM_free(pszTemp);
    }

    BNL_xml_load_document(pszTemplateFile, &pTemplate);

    /*find the change prop*/
    pProp=BNL_xml_find_first(pTemplate,XML_EL_PROP);

    while (pProp)
    {
      char *pszTemp  = NULL;
      pAttr=pProp->pAttributes;

      while (pAttr!=NULL)
      {
        if (strcmp(pAttr->pszName,XML_ATTR_ATTR)== 0)
        {
          BNL_xml_element_get_attribute_value(pProp, XML_ATTR_COLNAME, &pszDisVal);
          break;
        }
        pAttr=pAttr->pNext;
      }

      pszTemp = BNL_strings_copy_string(pAttr->pszValue);
      iRetCode = BNL_split_multiple_values(pszTemp,'.', &iVals, &pszValues);
      if (BNL_em_error_handler("BNL_split_multiple_values", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
      if (iVals == 1)
      {
        // For getting an empty line
        if (_stricmp(pAttr->pszValue, "-") == 0)
        {
          pszProdVal = (char *)MEM_alloc(2);
          strcpy(pszProdVal, "-");
        }
        else
        {
          iRetCode = BNL_tb_get_property(tRev, pAttr->pszValue, &pszProdVal);
        }
        if (tPred != NULL_TAG)
        {
          // For getting an empty line
          if (_stricmp(pAttr->pszValue, "-") == 0)
          {
            pszPredVal = NULL;
          }
          else
          {
            iRetCode = BNL_tb_get_property(tPred, pAttr->pszValue, &pszPredVal);
          }
        }
        tProdObj = tRev;
        tPredObj = tPred;

        if (pszDisVal != NULL)
        {
          sprintf(szAttrName, "%s", pszDisVal);
        }
        else
        {
		      sprintf(szAttrName, "%s", pAttr->pszValue);
        }
        //if (pszDisVal != NULL) MEM_free(pszDisVal);
      }
      else
      {
        int iValues       = 0;
        int k             = 0;
        tag_t tProp       = NULL_TAG;
        tag_t *ptValues   = NULL;

        //char *pszDisVal   = NULL;

        tProdObj = tRev;
        tPredObj = tPred;

        for (k= 0; k<iVals-1; k++)
        {
          iValues = 0;

          iRetCode = PROP_ask_property_by_name(tProdObj, pszValues[k], &tProp);
          //if (BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
          BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true);

          if (tProp != NULL_TAG)
          {
            iRetCode = PROP_ask_value_tags(tProp, &iValues, &ptValues);
            BNL_em_error_handler("PROP_ask_value_tags", BNL_module, EMH_severity_error, iRetCode, true);
          }

          if (iValues > 0)
          {
            tProdObj = ptValues[0];
            MEM_free(ptValues);
            iValues = 0;
          }
          else
          {
            tProdObj = NULL_TAG;
          }

          if (tPredObj != NULL_TAG)
          {
            iRetCode = PROP_ask_property_by_name(tPredObj, pszValues[k], &tProp);
            //if (BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
            BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true);

            if (tProp != NULL_TAG)
            {
              iRetCode = PROP_ask_value_tags(tProp, &iValues, &ptValues);
              BNL_em_error_handler("PROP_ask_value_tags", BNL_module, EMH_severity_error, iRetCode, true);
            }

            if (iValues > 0)
            {
              tPredObj = ptValues[0];
              MEM_free(ptValues);
            }
            else
            {
              tPredObj = NULL_TAG;
            }
          }
        }

        /*if (tProdObj != NULL_TAG)
        {
          AOM_UIF_ask_name(tProdObj, pszValues[iVals-1], &pszDisVal);
        }*/
        if (pszDisVal != NULL)
        {
          sprintf(szAttrName, "%s", pszDisVal);
        }
        else
        {
		      sprintf(szAttrName, "%s", pszValues[iVals-1]);
        }

        //if (pszDisVal != NULL) MEM_free(pszDisVal);

        if (tPredObj != NULL_TAG)
        {
          iRetCode = BNL_tb_get_property(tPredObj, pszValues[iVals-1], &pszPredVal);
        }

        if (tProdObj != NULL_TAG)
        {
          iRetCode = BNL_tb_get_property(tProdObj, pszValues[iVals-1], &pszProdVal);
        }
      }

      if (pszProdVal!=NULL)
      {
        if (pszPredVal == NULL || (strcmp(pszPredVal, pszProdVal) !=0 && (strcmp(pAttr->pszValue, "object_name") !=0)))
        {
          if (pszPredVal == NULL)
          {
            if (_stricmp(pAttr->pszValue, "-") == 0)
              BNL_rp_create_xml_attr_props(tProdObj, pszMode,szAttrName, pszProdVal, "-", &pProdProps);
            else
              BNL_rp_create_xml_attr_props(tProdObj, pszMode,szAttrName, pszProdVal, "", &pProdProps);
          }
          else
          {
            //BNL_rp_create_xml_attr_props(tPredObj, pszMode,szAttrName, pszPredVal,0, &pPredProps);
            BNL_rp_create_xml_attr_props(tProdObj, pszMode,szAttrName, pszProdVal, pszPredVal, &pProdProps);
          }


          iCount++;
          ptDiffs=(diffinfo_t *) MEM_realloc(ptDiffs, iCount * sizeof(diffinfo_t));

          ptDiffs[iCount-1].iChange= 0;
          ptDiffs[iCount-1].pXMLprops=pProdProps;
          ptDiffs[iCount-1].pszQuant=BNL_strings_copy_string("");

          ptDiffs[iCount-1].pszSeq=BNL_strings_copy_string("");
          ptDiffs[iCount-1].pszRefDes=BNL_strings_copy_string("");
          ptDiffs[iCount-1].tObject=tRev;
          ptDiffs[iCount-1].pNext= NULL;
          ptDiffs[iCount-1].pPrev= NULL;

          iCount++;
          ptDiffs=(diffinfo_t *) MEM_realloc(ptDiffs, iCount * sizeof(diffinfo_t));

          ptDiffs[iCount-1].iChange= 0;
          ptDiffs[iCount-1].pXMLprops=pPredProps;
          ptDiffs[iCount-1].pszQuant=BNL_strings_copy_string("");

          ptDiffs[iCount-1].pszSeq=BNL_strings_copy_string("");
          ptDiffs[iCount-1].pszRefDes=BNL_strings_copy_string("");
          ptDiffs[iCount-1].tObject=tPred;
          ptDiffs[iCount-1].pNext= NULL;
          ptDiffs[iCount-1].pPrev= NULL;

        }
      }
      else
      {
        fprintf(rplog, "Found property [%s] given in property file [%s] does not exist on given object.\n", pAttr->pszValue, pszTemplateFile);
      }
      if (pszTemp != NULL)
      {
        free(pszTemp);
        pszTemp = NULL;
      }
      if (pszProdVal != NULL)
      {
        MEM_free(pszProdVal);
        pszProdVal = NULL;
      }
      if (pszPredVal != NULL)
      {
        MEM_free(pszPredVal);
        pszPredVal = NULL;
      }

      pProp=BNL_xml_find_next(pProp,XML_EL_PROP);
    }

    *iDiff = iCount;
    *ptAttrs=ptDiffs;
    MEM_free(pszTemplateFile);

    return iRetCode;
}

/*should be may be placed in tb module*/
int BNL_rp_get_bomview
(
  tag_t tRev,
  char *pszViewType,
  tag_t *tBomView
)
{
    int iRetCode            = 0;
    int i                   = 0;

    int iBomRevCount        = 0;

    char *pszType           = NULL;

    tag_t tBom              =NULL_TAG;
    tag_t tViewType         =NULL_TAG;
    tag_t *ptBomRevs        = NULL;


    iRetCode = ITEM_rev_list_bom_view_revs(tRev, &iBomRevCount, &ptBomRevs);
    if (BNL_em_error_handler("ITEM_rev_list_bom_view_revs", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;


    for (i= 0;i<iBomRevCount;i++)
    {


      iRetCode = PS_ask_bom_view_of_bvr(ptBomRevs[i], &tBom);
      if (BNL_em_error_handler("PS_ask_bom_view_of_bvr", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;


      iRetCode = PS_ask_bom_view_type(tBom, &tViewType);
      if (BNL_em_error_handler("PS_ask_bom_view_type", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;


      iRetCode = PS_ask_view_type_name(tViewType, &pszType);
      if (BNL_em_error_handler("PS_ask_view_type_name", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      if (strcmp(pszType, pszViewType)== 0)
      {
         *tBomView=tBom;
      }

      MEM_free(pszType);
    }



    return iRetCode;
}

//works only if there is only one object is possible at that relation at that object.
//Always the first found object of type is returned
int BNL_rp_get_related_object(tag_t tObject,char *pszRelation,char *pszObjectType,tag_t *tRelatedObject)
{
    int iRetCode    = 0;
    int i           = 0;
    int iObjs       = 0;

    tag_t tRelType      =NULL_TAG;
    tag_t *ptObjects    = NULL;

    GRM_relation_t *ptGrmEcos = NULL;

    char szType[33];


     iRetCode = GRM_find_relation_type(pszRelation, &tRelType);
     if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

     iRetCode = GRM_list_secondary_objects_only(tObject,tRelType, &iObjs, &ptObjects);
     if (BNL_em_error_handler("GRM_list_primary_objects", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

     for (i= 0;i<iObjs;i++)
     {
         iRetCode = BNL_tb_determine_object_type(ptObjects[i],szType);
         if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

         if (strcmp(szType, pszObjectType)== 0)
         {
             *tRelatedObject=ptObjects[i];
             break;
         }
     }

     if (ptObjects!=NULL) MEM_free(ptObjects);



    return iRetCode;

}



/*same as tb function but changed so it can handle the structure*/
int BNL_rp_bom_get_item_revs
(
  char       *pszMode,
  tag_t      tBomLine,
  int        iLevel,
  int        iMaxLevel,
  int        *iRevCount,
  diffinfo_t **tItemRevs
)
{
    tag_t *tBomLines            = NULL;
    tag_t tItemRev              =NULL_TAG;
    tag_t tItem                 =NULL_TAG;

    int iRetCode                =ITK_ok;
    int iAttrRevId              = 0;
    int iAttrItemId             = 0;

    int iBomLineCount           = 0;
    int l                       = 0;
    int i                       = 0;
    int iFound                  = 0;
    int iCount                  = 0;

    int iAttrQuantId            = 0;
    int iAttrTypeId             = 0;

    char *pszQuantity           = NULL;
    char *pszSequence           = NULL;
    char *pszRefDes             = NULL;

    logical lUnpack             = false;

    t_XMLElement *pProps     = NULL;


    diffinfo_t *ptRevs=*tItemRevs;

    iCount=*iRevCount;

    if (iLevel>0)
    {
       iRetCode = BOM_line_look_up_attribute(bomAttr_occQty, &iAttrQuantId);
       if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return false;

       iRetCode = BOM_line_ask_attribute_string(tBomLine, iAttrQuantId, &pszQuantity);
       if (BNL_em_error_handler("BOM_line_ask_attribute_string", BNL_module, EMH_severity_error, iRetCode, true)) return false;

       iRetCode = BOM_line_look_up_attribute(bomAttr_occSeqNo, &iAttrQuantId);
       if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return false;

       iRetCode = BOM_line_ask_attribute_string(tBomLine, iAttrQuantId, &pszSequence);
       if (BNL_em_error_handler("BOM_line_ask_attribute_string", BNL_module, EMH_severity_error, iRetCode, true)) return false;

       // REF DES
       BNL_tb_get_property(tBomLine, "REF DES", &pszRefDes);
       if (pszRefDes == NULL)
       {
         fprintf(rplog, "%s: Warning, property [REF DES] does not exist.\n", BNL_module);
         EMH_clear_errors();
         pszRefDes = MEM_string_copy("");
       }

       iRetCode = BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId);
       if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return false;

       iRetCode = BOM_line_look_up_attribute(bomAttr_lineItemTag, &iAttrItemId);
       if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return false;

       iRetCode = BOM_line_ask_attribute_tag(tBomLine, iAttrRevId, &tItemRev);
       if (BNL_em_error_handler("BOM_line_ask_attribute_tag", BNL_module, EMH_severity_error, iRetCode, true)) return false;

       iRetCode = BOM_line_ask_attribute_tag(tBomLine, iAttrItemId, &tItem);
       if (BNL_em_error_handler("BOM_line_ask_attribute_tag", BNL_module, EMH_severity_error, iRetCode, true)) return false;

       if(tItemRev!=NULL_TAG)
       {
          // JM|04-12-2008
          AOM_refresh(tItemRev, false);
          AOM_refresh(tItem, false);

          iCount++;
          ptRevs=(diffinfo_t *) MEM_realloc(ptRevs, iCount * sizeof(diffinfo_t));

          ptRevs[iCount-1].tObject=tItemRev;
          //ptRevs[iCount-1].pszQuant=pszQuantity;
          ptRevs[iCount-1].pszQuant=BNL_strings_copy_string(pszQuantity);


          //ptRevs[iCount-1].pszSeq=pszSequence;
          ptRevs[iCount-1].pszSeq=BNL_strings_copy_string(pszSequence);
          //ptRevs[iCount-1].pszRefDes=pszRefDes;
          ptRevs[iCount-1].pszRefDes=BNL_strings_copy_string(pszRefDes);

          BNL_rp_create_xml_props(tBomLine, pszMode, iLevel, &pProps,NULL,NULL,NULL,NULL,NULL);
          ptRevs[iCount-1].pXMLprops=pProps;
          ptRevs[iCount-1].iChange= 0;
          ptRevs[iCount-1].pNext= NULL;
          ptRevs[iCount-1].pPrev= NULL;
        }
        else
        {
          /*this happens when the revision rule is set and there are occurence wich not
            which is not a allowed occurence the (???) occurence.
            EN quik and dirty implementation 21-05-2001 for Philips EMT
          */

          fprintf(rplog, "%s:Unconfigured occurences found\n", BNL_module);

          iCount++;
          ptRevs=(diffinfo_t *) MEM_realloc(ptRevs, iCount * sizeof(diffinfo_t));
          ptRevs[iCount-1].tObject=tItem;
          //ptRevs[iCount-1].pszQuant=pszQuantity;
          ptRevs[iCount-1].pszQuant=BNL_strings_copy_string(pszQuantity);


          //ptRevs[iCount-1].pszSeq=pszSequence;
          ptRevs[iCount-1].pszSeq=BNL_strings_copy_string(pszSequence);
          //ptRevs[iCount-1].pszRefDes=pszRefDes;
          ptRevs[iCount-1].pszRefDes=BNL_strings_copy_string(pszRefDes);

          BNL_rp_create_xml_props(tBomLine, pszMode, iLevel, &pProps,NULL,NULL,NULL,NULL,NULL);
          ptRevs[iCount-1].pXMLprops=pProps;
          ptRevs[iCount-1].iChange= 0;
          ptRevs[iCount-1].pNext= NULL;
          ptRevs[iCount-1].pPrev= NULL;
        }
       if (pszRefDes != NULL)
       {
         MEM_free(pszRefDes);
         pszRefDes = NULL;
       }
       if (pszQuantity != NULL)
       {
         MEM_free(pszQuantity);
         pszQuantity = NULL;
       }
       if (pszSequence != NULL)
       {
         MEM_free(pszSequence);
         pszSequence = NULL;
       }
    }
    else
    {
      *tItemRevs= NULL;
      *iRevCount= 0;
    }


    if ((iLevel < iMaxLevel) || iMaxLevel==-1)
    {
      // For checking the type
      iRetCode = BOM_line_look_up_attribute(bomAttr_itemType, &iAttrTypeId);
      if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return false;

      iRetCode = BOM_line_ask_child_lines(tBomLine, &iBomLineCount, &tBomLines);
      if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return false;

      fprintf(rplog, "DEBUG: Mode [%s].\n", pszMode);

      //JM|31-08-2010: Added bomrep_mfg
      if (strcmp(pszMode, "ebompackedrep") != 0 && strcmp(pszMode, "bomrep_mfg") != 0)
      {
        for (l= 0;l<iBomLineCount;l++)
        {
          logical lPacked         = false;
          char *pszType           = NULL;

          // Check the type, if FEI_Electronics Item, then unpack
          iRetCode = BOM_line_ask_attribute_string(tBomLines[l], iAttrTypeId, &pszType);
          if (BNL_em_error_handler("BOM_line_ask_attribute_string", BNL_module, EMH_severity_error, iRetCode, true)) return false;

          fprintf(rplog, "%s: BOMline is of type [%s].\n", BNL_module, pszType);

          if (strcmp(pszType, "FEI_Electronics") == 0)
          {
            iRetCode = BOM_line_is_packed(tBomLines[l], &lPacked);
            if (BNL_em_error_handler("BOM_line_is_packed", BNL_module, EMH_severity_error, iRetCode, true)) return false;

            fprintf(rplog, "DEBUG: Lined packed [%d].\n", lPacked);

            if (lPacked)
            {
              iRetCode = BOM_line_unpack(tBomLines[l]);
              if (BNL_em_error_handler("BOM_line_unpack", BNL_module, EMH_severity_error, iRetCode, true)) return false;

              lUnpack = true;

              fprintf(rplog, "%s: Unpacked a line.\n", BNL_module);
            }
          }
          if (pszType != NULL) MEM_free(pszType);
        } // End of for (l= 0;l<iBomLineCount;l++)
        if (lUnpack)
        {
          if (iBomLineCount>0) BNL_mm_free((void*)tBomLines);

          iRetCode = BOM_line_ask_child_lines(tBomLine, &iBomLineCount, &tBomLines);
          if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return false;
        }
      }

      fprintf(rplog, "%s: Found at bomline at level [%d] [%d] bomlines lines\n  ", BNL_module,(iLevel+1), iBomLineCount);

      for (l= 0;l<iBomLineCount;l++)
      {
        int iPrefValues         = 0;
        char *pszType           = NULL;
        char **pszPrefValues    = NULL;

        char szKey[128];

        logical lSkip           = false;


        sprintf(szKey, "BNL_hide_type_%s", pszMode);

        fprintf(rplog, "%s: Looking for preference [%s].\n", BNL_module, szKey);

        PREF_ask_char_values(szKey, &iPrefValues, &pszPrefValues);
        EMH_clear_errors();

        if (iPrefValues > 0)
        {
          fprintf(rplog, "%s: Preference [%s] has been defined.\n", BNL_module, szKey);

          // Check the type, if FEI_Electronics Item, then unpack
          iRetCode = BOM_line_ask_attribute_string(tBomLines[l], iAttrTypeId, &pszType);
          if (BNL_em_error_handler("BOM_line_ask_attribute_string", BNL_module, EMH_severity_error, iRetCode, true)) return false;

          fprintf(rplog, "%s: Item on bomline is of type [%s]\n", BNL_module, pszType);

          if (BNL_tb_value_in_list(pszType, iPrefValues, pszPrefValues))
          {
            fprintf(rplog, "%s: Type has defined, skipping...\n", BNL_module);
            lSkip = true;
          }
        }
        else
        {
          fprintf(rplog, "%s: Preference [%s] has not been defined.\n", BNL_module, szKey);
        }

        if (!lSkip)
        {
          iRetCode = BNL_rp_bom_get_item_revs(pszMode,tBomLines[l],(iLevel+1), iMaxLevel, &iCount, &ptRevs);
          if (BNL_em_error_handler("BNL_tb_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) return false;

          fprintf(rplog, "%s: Found at bomline [%d] of level [%d] [%d] child lines\n", BNL_module,l,(iLevel+1),*iRevCount);
        }

        if (pszType != NULL) MEM_free(pszType);
        if (iPrefValues > 0) MEM_free(pszPrefValues);
      }

      if (iBomLineCount>0) BNL_mm_free((void*)tBomLines);
    }


    *tItemRevs=ptRevs;
    *iRevCount=iCount;

    return iRetCode;

} /* end of BNL_rp_bom_get_item_revs */


int BNL_rp_compare_arrays
(
   int iCountOne,diffinfo_t *pArrayOne,
   int iCountTwo,diffinfo_t *pArrayTwo,
   int *iDiffCount,
   diffinfo_t  **pDiffs
)
{
    int iRetCode        = 0;
    int i               = 0;
    int t               = 0;
    int s               = 0;
    int o               = 0;
    int iTimesOne       = 0;
    int iFound          = 0;
    int iTimesTwo       = 0;
    int iCalDiff        = 0;
    int iCount          = 0;
    int iQuantDiff      = 0;

    char *pszCompOneQuant        = NULL;
    char *pszCompTwoQuant        = NULL;


    diffinfo_t *ptDiffs         = NULL;
    t_XMLElement *pXMLProps     = NULL;

    if (iCountOne== 0)
    {
        /*everything is added*/

        for (i= 0;i<iCountTwo;i++)
        {
           iCount++;
           ptDiffs=(diffinfo_t *) MEM_realloc(ptDiffs, iCount * sizeof(diffinfo_t));

           BNL_xml_element_copy(pArrayTwo[i].pXMLprops, &pXMLProps);
           ptDiffs[iCount-1].iChange=1;
           ptDiffs[iCount-1].pXMLprops=pXMLProps;
           ptDiffs[iCount-1].pszQuant=BNL_strings_copy_string(pArrayTwo[i].pszQuant);

           ptDiffs[iCount-1].pszSeq=BNL_strings_copy_string(pArrayTwo[i].pszSeq);
           ptDiffs[iCount-1].pszRefDes=BNL_strings_copy_string(pArrayTwo[i].pszRefDes);

           ptDiffs[iCount-1].tObject=pArrayTwo[i].tObject;
           ptDiffs[iCount-1].pNext= NULL;
           ptDiffs[iCount-1].pPrev= NULL;

        }

    } else if (iCountTwo== 0)
    {
        /*everything is removed*/
        for (i= 0;i<iCountOne;i++)
        {
           iCount++;
           ptDiffs=(diffinfo_t *) MEM_realloc(ptDiffs, iCount * sizeof(diffinfo_t));

           BNL_xml_element_copy(pArrayOne[i].pXMLprops, &pXMLProps);
           ptDiffs[iCount-1].iChange=-1;
           ptDiffs[iCount-1].pXMLprops=pXMLProps;
           ptDiffs[iCount-1].pszQuant=BNL_strings_copy_string(pArrayOne[i].pszQuant);

           ptDiffs[iCount-1].pszSeq=BNL_strings_copy_string(pArrayOne[i].pszSeq);
           ptDiffs[iCount-1].pszRefDes=BNL_strings_copy_string(pArrayOne[i].pszRefDes);
           ptDiffs[iCount-1].tObject=pArrayOne[i].tObject;
           ptDiffs[iCount-1].pNext= NULL;
           ptDiffs[iCount-1].pPrev= NULL;

        }

    } else
    {
      //for debuging
      char szName[65];
      tag_t tObjClass = NULL_TAG;
      char *pszNameOfClass  = NULL;

        fprintf(rplog, "\n++++++++++++++++++debug+++++++++++++++++++++++++++\n\n");

        fprintf(rplog, "ArrayOne: [%d]\n", iCountOne);

        for (i= 0;i<iCountOne;i++)
        {
            WSOM_ask_name(pArrayOne[i].tObject,szName);
            POM_class_of_instance (pArrayOne[i].tObject, &tObjClass);
            POM_name_of_class(tObjClass, &pszNameOfClass);

            if (strcmp(pszNameOfClass, "ItemRevision")== 0)
            {

                fprintf(rplog, "%d : name=[%s] tag=[%d] seq=[%s] refdes=[%s] change=[%d] quant=[%s]\n", i, BNL_rp_get_full_id(pArrayOne[i].tObject), pArrayOne[i].tObject, pArrayOne[i].pszSeq, pArrayOne[i].pszRefDes, pArrayOne[i].iChange, pArrayOne[i].pszQuant);
            }
            else
            {

              fprintf(rplog, "%d : name=[%s] tag=[%d] seq=[%s] refdes=[%s] change=[%d] quant=[%s]\n", i,szName, pArrayOne[i].tObject, pArrayOne[i].pszSeq, pArrayOne[i].pszRefDes, pArrayOne[i].iChange, pArrayOne[i].pszQuant);
            }

            MEM_free(pszNameOfClass);



        }

        fprintf(rplog, "\nArrayTwo: [%d]\n", iCountTwo);

        for (i= 0;i<iCountTwo;i++)
        {
            WSOM_ask_name(pArrayTwo[i].tObject,szName);
            POM_class_of_instance (pArrayTwo[i].tObject, &tObjClass);
            POM_name_of_class(tObjClass, &pszNameOfClass);

            if (strcmp(pszNameOfClass, "ItemRevision")== 0)
            {

               fprintf(rplog, "%d : name=[%s] tag=[%d] seq=[%s] refdes=[%s] change=[%d] quant=[%s]\n", i, BNL_rp_get_full_id(pArrayTwo[i].tObject), pArrayTwo[i].tObject, pArrayTwo[i].pszSeq, pArrayTwo[i].pszRefDes, pArrayTwo[i].iChange, pArrayTwo[i].pszQuant);
            }
            else
            {
              fprintf(rplog, "%d : name=[%s] tag=[%d] seq=[%s] refdes=[%s] change=[%d] quant=[%s]\n", i,szName, pArrayTwo[i].tObject, pArrayTwo[i].pszSeq, pArrayTwo[i].pszRefDes, pArrayTwo[i].iChange, pArrayTwo[i].pszQuant);

            }

             MEM_free(pszNameOfClass);

        }

        fprintf(rplog, "\n++++++++++++++++++++++++++++++++++++++++++++++++++\n\n");

         /*check for added components in array two*/
         for (i= 0;i<iCountTwo;i++)
         {
            for (o= 0;o<iCountOne;o++)
            {
              //if (pArrayOne[o].tObject==pArrayTwo[i].tObject && strcmp(pArrayOne[o].pszSeq, pArrayTwo[i].pszSeq)== 0)
              if (pArrayOne[o].tObject==pArrayTwo[i].tObject && strcmp(pArrayOne[o].pszSeq, pArrayTwo[i].pszSeq)== 0
                && strcmp(pArrayOne[o].pszRefDes, pArrayTwo[i].pszRefDes)== 0)
              {
                 iFound=1;
                 break;
              }
            }

            if (!iFound)
            {
              iCount++;
              ptDiffs=(diffinfo_t *) MEM_realloc(ptDiffs, iCount * sizeof(diffinfo_t));

              BNL_xml_element_copy(pArrayTwo[i].pXMLprops, &pXMLProps);
              ptDiffs[iCount-1].iChange=1;
              ptDiffs[iCount-1].pXMLprops=pXMLProps;
              ptDiffs[iCount-1].pszQuant=BNL_strings_copy_string(pArrayTwo[i].pszQuant);

              ptDiffs[iCount-1].pszSeq=BNL_strings_copy_string(pArrayTwo[i].pszSeq);
              ptDiffs[iCount-1].pszRefDes=BNL_strings_copy_string(pArrayTwo[i].pszRefDes);
              ptDiffs[iCount-1].tObject=pArrayTwo[i].tObject;
              ptDiffs[iCount-1].pNext= NULL;
              ptDiffs[iCount-1].pPrev= NULL;

            }

            iFound= 0;
         }



        for (i= 0;i<iCountOne;i++)
        {

            /*check if array object appears initself and count*/
            for(s= 0;s<iCountOne;s++)
            {
              //if (pArrayOne[i].tObject==pArrayOne[s].tObject && strcmp(pArrayOne[i].pszSeq, pArrayOne[s].pszSeq)== 0)
              if (pArrayOne[i].tObject==pArrayOne[s].tObject && strcmp(pArrayOne[i].pszSeq, pArrayOne[s].pszSeq)== 0
                  && strcmp(pArrayOne[i].pszRefDes, pArrayOne[s].pszRefDes)== 0)
                {
                    iTimesOne++;
                }
            }


            /*check if array object appears in array two and check how often*/
            for (t= 0;t<iCountTwo;t++)
            {
              //if (pArrayOne[i].tObject==pArrayTwo[t].tObject && strcmp(pArrayOne[i].pszSeq, pArrayTwo[t].pszSeq)== 0)
              if (pArrayOne[i].tObject==pArrayTwo[t].tObject && strcmp(pArrayOne[i].pszSeq, pArrayTwo[t].pszSeq)== 0
                && strcmp(pArrayOne[i].pszRefDes, pArrayTwo[t].pszRefDes)== 0)
              {

                /*we should check if quantity is changed but if quant is empty then
                  its also 1*/

                  /*fprintf(rplog, "debug: %d. tag_one=[%d] quant=[%s] tag_two=[%d] quant=[%s] \n", i, pArrayOne[i].tObject, pArrayOne[i].pszQuant, pArrayTwo[t].tObject, pArrayTwo[t].pszQuant);*/
                  if (pArrayOne[i].pszQuant[0]=='\0')
                  {
                      pszCompOneQuant=BNL_strings_copy_string("1");
                  }
                  else
                  {
                      pszCompOneQuant=BNL_strings_copy_string(pArrayOne[i].pszQuant);

                  }

                  if (pArrayTwo[t].pszQuant[0]=='\0')
                  {
                      pszCompTwoQuant=BNL_strings_copy_string("1");
                  }
                  else
                  {
                      pszCompTwoQuant=BNL_strings_copy_string(pArrayTwo[t].pszQuant);
                  }


               /*fprintf(rplog, "debug :pszCompOneQuant=[%s], pszCompTwoQuant=[%s]\n ", pszCompOneQuant, pszCompTwoQuant);*/
                if (strcmp(pszCompOneQuant, pszCompTwoQuant)!=0)
                {

                     /*add to array as 0*/
                     iCount++;
                     ptDiffs=(diffinfo_t *) MEM_realloc(ptDiffs, iCount * sizeof(diffinfo_t));
                     BNL_xml_element_copy(pArrayTwo[t].pXMLprops, &pXMLProps);
                     ptDiffs[iCount-1].iChange= 0;
                     ptDiffs[iCount-1].pszQuant=BNL_strings_copy_string(pArrayOne[i].pszQuant);

                     ptDiffs[iCount-1].pszSeq=BNL_strings_copy_string(pArrayOne[i].pszSeq);
                     ptDiffs[iCount-1].pszRefDes=BNL_strings_copy_string(pArrayOne[i].pszRefDes);

                     /*add to the props the old quant*/
                     BNL_set_pred_quant(pXMLProps, pArrayOne[i].pszQuant);

                     ptDiffs[iCount-1].pXMLprops=pXMLProps;
                     ptDiffs[iCount-1].tObject=pArrayTwo[t].tObject;
                     ptDiffs[iCount-1].pNext= NULL;
                     ptDiffs[iCount-1].pPrev= NULL;
                }

                free(pszCompTwoQuant);
                free(pszCompOneQuant);

                iFound=1;
                iTimesTwo++;
              }

            }

            if (iFound && iTimesTwo!=iTimesOne)
            {
               iCalDiff=iTimesOne-iTimesTwo;

               if (iCalDiff<0)
               {
                /*create new one and set to 1*/
                 iCount++;
                 ptDiffs=(diffinfo_t *) MEM_realloc(ptDiffs, iCount * sizeof(diffinfo_t));

                 BNL_xml_element_copy(pArrayOne[i].pXMLprops, &pXMLProps);
                 ptDiffs[iCount-1].iChange=1;
                 ptDiffs[iCount-1].pXMLprops=pXMLProps;
                 ptDiffs[iCount-1].pszQuant=BNL_strings_copy_string(pArrayOne[i].pszQuant);
                 ptDiffs[iCount-1].pszSeq=BNL_strings_copy_string(pArrayOne[i].pszSeq);
                 ptDiffs[iCount-1].pszRefDes=BNL_strings_copy_string(pArrayOne[i].pszRefDes);
                 ptDiffs[iCount-1].tObject=pArrayOne[i].tObject;
                 ptDiffs[iCount-1].pNext= NULL;
                 ptDiffs[iCount-1].pPrev= NULL;



               }
               else
               {
                /*create new one and set to -1*/
                 iCount++;
                 ptDiffs=(diffinfo_t *) MEM_realloc(ptDiffs, iCount * sizeof(diffinfo_t));


                 BNL_xml_element_copy(pArrayOne[i].pXMLprops, &pXMLProps);

                 ptDiffs[iCount-1].iChange=-1;
                 ptDiffs[iCount-1].pszQuant=BNL_strings_copy_string(pArrayOne[i].pszQuant);

                 ptDiffs[iCount-1].pszSeq=BNL_strings_copy_string(pArrayOne[i].pszSeq);
                 ptDiffs[iCount-1].pszRefDes=BNL_strings_copy_string(pArrayOne[i].pszRefDes);
                 ptDiffs[iCount-1].pXMLprops=pXMLProps;
                 ptDiffs[iCount-1].tObject=pArrayOne[i].tObject;
                 ptDiffs[iCount-1].pNext= NULL;
                 ptDiffs[iCount-1].pPrev= NULL;


               }
            } /*end of if (iFound && iTimesTwo!=iTimesOne)*/

            if (!iFound)
            {
            /*create new add to array  as -1*/
             iCount++;
             ptDiffs=(diffinfo_t *) MEM_realloc(ptDiffs, iCount * sizeof(diffinfo_t));
             BNL_xml_element_copy(pArrayOne[i].pXMLprops, &pXMLProps);

             ptDiffs[iCount-1].iChange=-1;
             ptDiffs[iCount-1].pszQuant=BNL_strings_copy_string(pArrayOne[i].pszQuant);

             ptDiffs[iCount-1].pszSeq=BNL_strings_copy_string(pArrayOne[i].pszSeq);
             ptDiffs[iCount-1].pszRefDes=BNL_strings_copy_string(pArrayOne[i].pszRefDes);
             ptDiffs[iCount-1].pXMLprops=pXMLProps;
             ptDiffs[iCount-1].tObject=pArrayOne[i].tObject;
             ptDiffs[iCount-1].pNext= NULL;
             ptDiffs[iCount-1].pPrev= NULL;


            }


            iFound= 0;
            iTimesOne= 0;
            iTimesTwo= 0;
            iQuantDiff= 0;
        }/*end of for (i= 0;i<iCountOne;i++)*/
    }/*end of if (iCountTwo== 0)*/


    *iDiffCount=iCount;
    *pDiffs=ptDiffs;


    return iRetCode;
}

/*

int BNL_rp_bom_diff
(
  tag_t tProduct,       <I>
  tag_t Predecessor,    <I>
  char pszViewType,     <I>
  char *pszRevRule,     <I>
  int  *iDiffCount      <O>
  diffinfo_t **pDiffs   <OF>
)


Input Product (=ItemRevision), Predecessor (=ItemRevision), bomviewtype,
Revision rule  Find on the both revisions a bomview of given type.
Open the bomviews with the given revision rule.
Loop through both bomviews and collect differences.
Possible differences are add line (+) remove line (-) change in quantity (0).
Returns an array of structures, structure contains the tag of corresponding
revision and the change value.
*/
int BNL_rp_bom_diff
(
  char *pszMode,
  tag_t tPred,
  tag_t tProd,
  char *pszViewType,
  char *pszRevRule,
  int  iLevel,
  int  *iDiffCount,
  diffinfo_t **pDiffs
)
{
    int iRetCode        = 0;
    int iBomRevCount    = 0;
    int iProdCount      = 0;
    int iPredCount      = 0;
    int iDiffs          = 0;

    tag_t ptBomRevs     =NULL_TAG;

    tag_t tProdBom      =NULL_TAG;
    tag_t tPredBom      =NULL_TAG;

    tag_t tBomWindow    =NULL_TAG;
    tag_t tParent       =NULL_TAG;

    tag_t tTopLine      =NULL_TAG;

    diffinfo_t *pProdRevs   = NULL;
    diffinfo_t *pPredRevs   = NULL;
    diffinfo_t *ptDiffs     = NULL;

    diffinfo_t *pBomdiff    = NULL;

    t_XMLElement *pXMLProps    = NULL;

    /*get the bomviews*/

    if (tPred!=NULL_TAG)
    {
      iRetCode = BNL_rp_get_bomview(tPred, pszViewType, &tPredBom);
      if (BNL_em_error_handler("BNL_rep_get_bomview", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }

      iRetCode = BNL_rp_get_bomview(tProd, pszViewType, &tProdBom);
      if (BNL_em_error_handler("BNL_rep_get_bomview", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;



    if (tProdBom==NULL_TAG && tPredBom==NULL_TAG)
    {
         *iDiffCount= 0;
         *pDiffs= NULL;
         return iRetCode;

    }

/*for product*/

    if (tPredBom !=NULL_TAG)
    {
        iRetCode = BOM_create_window(&tBomWindow);
        if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

        iRetCode = BNL_tb_bom_apply_config_rule(tBomWindow, pszRevRule);
        if (BNL_em_error_handler("BNL_tb_bom_apply_config_rule", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

        iRetCode = BOM_set_window_top_line(tBomWindow,NULL_TAG,tPred,tPredBom, &tTopLine);
        if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

        iRetCode = BNL_rp_bom_get_item_revs(pszMode,tTopLine,0, iLevel, &iPredCount, &pPredRevs);
        if (BNL_em_error_handler("BNL_rp_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

        BOM_close_window(tBomWindow);
        if (BNL_em_error_handler("BOM_close_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    }

/*fro predecessor*/
    if (tProdBom!=NULL_TAG)
    {
        iRetCode = BOM_create_window(&tBomWindow);
        if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

        iRetCode = BNL_tb_bom_apply_config_rule(tBomWindow, pszRevRule);
        if (BNL_em_error_handler("BNL_tb_bom_apply_config_rule", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

        iRetCode = BOM_set_window_top_line(tBomWindow,NULL_TAG,tProd,tProdBom, &tTopLine);
        if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

        iRetCode = BNL_rp_bom_get_item_revs(pszMode,tTopLine,0, iLevel, &iProdCount, &pProdRevs);
        if (BNL_em_error_handler("BNL_rp_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

        iRetCode = BOM_close_window(tBomWindow);
        if (BNL_em_error_handler("BOM_close_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    }

    /*now we have two arrays to compare*/
    iRetCode = BNL_rp_compare_arrays(iPredCount, pPredRevs, iProdCount, pProdRevs, &iDiffs, &ptDiffs);
    if (BNL_em_error_handler("BNL_rp_compare_arrays", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;


    *iDiffCount=iDiffs;
    *pDiffs=ptDiffs;


    /*free the diffs*/

     fprintf(rplog, "Freeing memory using BNL_rp_free_diff_struc in BNL_rp_bom_diff products\n");
     BNL_rp_free_diff_struc(iPredCount, pPredRevs);
     fprintf(rplog, "Freeing memory using BNL_rp_free_diff_struc in BNL_rp_bom_diff predecessor\n");
     BNL_rp_free_diff_struc(iProdCount, pProdRevs);


    return iRetCode;
}



/*
int BNL_rp_document_diff
(
  tag_t tProduct,      <I>
  tag_t tPredecessor,  <I>
  int  *iDiffCount,    <O>
  diffinfo_t **pDiffs  <OF>
)

Input Product (=ItemRevision),
Predecessor (=ItemRevision).
Get all attachments of both in a array using BNL_rp_documents.
Compare arrays if tag is found in product array and predecessor array it is assumed
the document is the same and not changed. (Tags are unique)If there new
tags found in the product array there is product added (+) if there is a
tag missing the document is removed (-). A added document that is a replacement of
another should be recognized and reported as (0). There is no current solution to
recognize this document replacement.

*/

int BNL_rp_document_diff
(
  char *pszMode,
  tag_t tProduct,
  tag_t tPredecessor,
  int   *iDiffCount,
  diffinfo_t **pDiffs
)
{
    int iRetCode        = 0;
    int iProdDocCount   = 0;
    int iPredDocCount   = 0;
    int iDiffs          = 0;

    diffinfo_t *ptProdDocs      = NULL;
    diffinfo_t *ptPredDocs      = NULL;
    diffinfo_t *ptDiffs         = NULL;

    if (tProduct!=NULL_TAG)
    {

      iRetCode = BNL_rp_get_documents(tProduct, pszMode, &iProdDocCount, &ptProdDocs);
      if (BNL_em_error_handler("BNL_rp_get_documents", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }

    if (tPredecessor!=NULL_TAG)
    {
      iRetCode = BNL_rp_get_documents(tPredecessor, pszMode, &iPredDocCount, &ptPredDocs);
      if (BNL_em_error_handler("BNL_rp_get_documents", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }

    iRetCode = BNL_rp_compare_arrays(iProdDocCount, ptProdDocs, iPredDocCount, ptPredDocs, &iDiffs, &ptDiffs);
    if (BNL_em_error_handler("BNL_rp_compare_arrays", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    *iDiffCount=iDiffs;
    *pDiffs=ptDiffs;


    /*free here the diff strucs*/
     fprintf(rplog, "Freeing memory using BNL_rp_free_diff_struc in BNL_rp_document_diff products\n");
     BNL_rp_free_diff_struc(iProdDocCount, ptProdDocs);

     fprintf(rplog, "Freeing memory using BNL_rp_free_diff_struc in BNL_rp_document_diff predecessor\n");
     BNL_rp_free_diff_struc(iPredDocCount, ptPredDocs);


    return iRetCode;
}

/*
int BNL_rp_prod_attr
(
  tag_t tProduct,      <I>
  int  *iDiffCount,    <O>
  diffinfo_t **pDiffs  <OF>
)

Input Product (=ItemRevision),
*/

int BNL_rp_prod_attr
(
  char *pszMode,
  tag_t tRev,
  int   *iDiffCount,
  diffinfo_t **pDiffs
)
{
    int iRetCode        = 0;
    int iVals           = 0;
    int iDiffs          = 0;

    tag_t tItem         =NULL_TAG;
    tag_t tProdObj      =NULL_TAG;
    diffinfo_t *ptDiffs = NULL;

    char szKey[128];
    char szAttrName[128];
    char szType[128];
    char *pszTemplateFile   = NULL;
    char *pszTemp           = NULL;
    char *pszProdVal        = NULL;
    char *pszDisVal         = NULL;

    char **pszValues = NULL;

    t_XMLElement *pTemplate  = NULL;
    t_XMLElement *pProdProps = NULL;
    t_XMLElement *pProp      = NULL;
    t_Attribute  *pAttr      = NULL;

    iRetCode = ITEM_ask_item_of_rev(tRev, &tItem);

    BNL_tb_determine_object_type(tItem, szType);

    sprintf(szKey, "BNL_prop_%s_%s",szType, pszMode);

    iRetCode = PREF_ask_char_value(szKey,0, &pszTemp);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (pszTemp==NULL)
    {
      fprintf(rplog, "The preference [%s] was not found, so there is no template to create properties structure\n",szKey);
      return iRetCode;
    }

    BNL_tb_replace_env_for_path(pszTemp, &pszTemplateFile);
    if (pszTemplateFile==NULL)
    {
      pszTemplateFile=pszTemp;
    }
    else
    {
      MEM_free(pszTemp);
    }

    fprintf(rplog, "Loading file [%s].\n", pszTemplateFile);
    BNL_xml_load_document(pszTemplateFile, &pTemplate);

    /*find the prop*/
    pProp=BNL_xml_find_first(pTemplate,XML_EL_PROP);

    while (pProp)
    {
      char *pszTemp   = NULL;
      pAttr=pProp->pAttributes;

      while (pAttr!=NULL)
      {
        if (strcmp(pAttr->pszName,XML_ATTR_ATTR)== 0)
        {
          BNL_xml_element_get_attribute_value(pProp, XML_ATTR_COLNAME, &pszDisVal);
          break;
        }
        pAttr=pAttr->pNext;
      }

      pszTemp = BNL_strings_copy_string(pAttr->pszValue);
      iRetCode = BNL_split_multiple_values(pszTemp,'.', &iVals, &pszValues);
      if (BNL_em_error_handler("BNL_split_multiple_values", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
      if (iVals == 1)
      {
        // For getting an empty line
        if (_stricmp(pAttr->pszValue, "-") == 0)
        {
          pszProdVal = (char *)MEM_alloc(2);
          strcpy(pszProdVal, "-");
        }
        else
        {
          iRetCode = BNL_tb_get_property(tRev, pAttr->pszValue, &pszProdVal);
        }

        tProdObj = tRev;

        //AOM_UIF_ask_name(tRev, pAttr->pszValue, &pszDisVal);
        if (pszDisVal != NULL)
        {
          sprintf(szAttrName, "%s", pszDisVal);
        }
        else
        {
		      sprintf(szAttrName, "%s", pAttr->pszValue);
        }
        //if (pszDisVal != NULL) MEM_free(pszDisVal);
      }
      else
      {
        int iValues       = 0;
        int k             = 0;
        tag_t tProp       = NULL_TAG;
        tag_t *ptValues   = NULL;

        //char *pszDisVal   = NULL;

        tProdObj = tRev;

        for (k= 0; k<iVals-1; k++)
        {
          iValues = 0;

          iRetCode = PROP_ask_property_by_name(tProdObj, pszValues[k], &tProp);
          //if (BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
          BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true);

          if (tProp != NULL_TAG)
          {
            iRetCode = PROP_ask_value_tags(tProp, &iValues, &ptValues);
            BNL_em_error_handler("PROP_ask_value_tags", BNL_module, EMH_severity_error, iRetCode, true);
          }

          if (iValues > 0)
          {
            tProdObj = ptValues[0];
            MEM_free(ptValues);
          }
          else
          {
            tProdObj = NULL_TAG;
          }
        }

        /*if (tProdObj != NULL_TAG)
        {
          AOM_UIF_ask_name(tProdObj, pszValues[iVals-1], &pszDisVal);
        }*/
        if (pszDisVal != NULL)
        {
          sprintf(szAttrName, "%s", pszDisVal);
        }
        else
        {
		      sprintf(szAttrName, "%s", pszValues[iVals-1]);
        }

        //if (pszDisVal != NULL) MEM_free(pszDisVal);

        if (tProdObj != NULL_TAG)
        {
          iRetCode = BNL_tb_get_property(tProdObj, pszValues[iVals-1], &pszProdVal);
        }
      }

      if (pszProdVal!=NULL)
      {
        BNL_rp_create_xml_attr_props(tProdObj, pszMode,szAttrName, pszProdVal,NULL, &pProdProps);

        iDiffs++;
        ptDiffs=(diffinfo_t *) MEM_realloc(ptDiffs, iDiffs * sizeof(diffinfo_t));

        ptDiffs[iDiffs-1].iChange= 0;
        ptDiffs[iDiffs-1].pXMLprops=pProdProps;
        ptDiffs[iDiffs-1].pszQuant=BNL_strings_copy_string("");

        ptDiffs[iDiffs-1].pszSeq=BNL_strings_copy_string("");
        ptDiffs[iDiffs-1].pszRefDes=BNL_strings_copy_string("");
        ptDiffs[iDiffs-1].tObject=tRev;
        ptDiffs[iDiffs-1].pNext= NULL;
        ptDiffs[iDiffs-1].pPrev= NULL;
      }
      else
      {
        fprintf(rplog, "Found property [%s] given in property file [%s] does not exist on given object.\n", pAttr->pszValue, pszTemplateFile);
      }
      if (pszTemp != NULL)
      {
        free(pszTemp);
        pszTemp = NULL;
      }

      if (pszProdVal != NULL)
      {
        MEM_free(pszProdVal);
        pszProdVal = NULL;
      }
      pProp=BNL_xml_find_next(pProp,XML_EL_PROP);
    }

    *iDiffCount=iDiffs;
    *pDiffs=ptDiffs;
    MEM_free(pszTemplateFile);

    return iRetCode;
}



/*
int BNL_rp_attr_diff
(
  tag_t tProduct,      <I>
  tag_t tPredecessor,  <I>
  int  *iDiffCount,    <O>
  diffinfo_t **pDiffs  <OF>
)

Input Product (=ItemRevision),
Predecessor (=ItemRevision).

Generates the xml structure for the change of Item/ItemRevision attributes report
*/

int BNL_rp_attr_diff
(
  char *pszMode,
  tag_t tProduct,
  tag_t tPredecessor,
  int   *iDiffCount,
  diffinfo_t **pDiffs
)
{
    int iRetCode        = 0;

    int iDiffs          = 0;


    diffinfo_t *ptDiffs          = NULL;

    if (tProduct!=NULL_TAG)
    {
      iRetCode = BNL_rp_get_prod_attrs(tProduct,tPredecessor, pszMode, &iDiffs, &ptDiffs);
      if (BNL_em_error_handler("BNL_rp_get_documents", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }


    *iDiffCount=iDiffs;
    *pDiffs=ptDiffs;

    return iRetCode;
}


int BNL_compare_dates(date_t dateOne, date_t dateTwo)
{
    int iRetCode = 0;

    /*
    dateOne == dateTwo   = 0
    dateOne > dateTwo    = 1
    dateOne < dateTwo    =-1
    */



    /*start compare years*/

    if (dateOne.year>dateTwo.year) return 1;
    if (dateOne.year<dateTwo.year) return -1;


    if (dateOne.year==dateTwo.year)
    {
        if (dateOne.month > dateTwo.month) return 1;
        if (dateOne.month < dateTwo.month) return -1;

        if (dateOne.month == dateTwo.month)
        {
            if (dateOne.day > dateTwo.day) return 1;
            if (dateOne.day < dateTwo.day) return -1;


            if (dateOne.day == dateTwo.day)
            {
                if (dateOne.hour > dateTwo.hour) return 1;
                if (dateOne.hour < dateTwo.hour) return -1;

                if (dateOne.hour == dateTwo.hour)
                {
                    if (dateOne.minute > dateTwo.minute) return 1;
                    if (dateOne.minute < dateTwo.minute) return -1;

                    if (dateOne.minute == dateTwo.minute)
                    {
                        if (dateOne.second > dateTwo.second) return 1;
                        if (dateOne.second < dateTwo.second) return -1;

                        if (dateOne.second == dateTwo.second) return 0;


                    }
                }
            }
        }
    }




    return iRetCode;
}


/*
int BNL_rp_eco_diffs
(
   tag_t tEco,                  <I>
   int *iAffDocCount,           <O>
   diffinfo_t **pAffDocDiffs,   <OF>
   int *iAffBomCount,           <O>
   diffinfo_t **pAffBomDiffs,   <OF>
   int *iSolDocCount,           <O>
   diffinfo_t **pSolDocDiffs,   <OF>
   int *iSolBomCount,           <O>
   diffinfo_t **pSolBomDiffs,   <OF>
   int *iAffAttrCount,          <O>
   array_t **pAffAttrDiffs      <OF>
)
Input Change (=ECO object)
Loop through the "EC_affected_item_rel" or "CMHasImpactedItem" relations
and carry out function BNL_rp_document_diff and  BNL_rp_bom_diff.
The predecessor is found as relation "EC_affected _to_problem relation" or "CMSolutionToImpacted" of the found objects
*/
int BNL_rp_eco_diffs
(
   tag_t tEco,
   int iLevel,
   char *pszRevRule,
   char *pszViewType,
   int *iAffDocCount,
   array_t **pAffDocDiffs,
   int *iAffBomCount,
   array_t **pAffBomDiffs,
   int *iAffAttrCount,
   array_t **pAffAttrDiffs
)
{
    int iRetCode        = 0;
    int iRevs           = 0;
    int i               = 0;
    int iDiffCount      = 0;

    char *pszDateFormat = NULL;
    char *pszEffDate    = NULL;

    tag_t *ptAffRevs        = NULL;
    tag_t tPredecessor      =NULL_TAG;

    diffinfo_t *pDiffs      = NULL;

    array_t  *ptAffDocArray     = NULL;
    array_t  *ptAffBomArray     = NULL;
    array_t  *ptAffAttrArray    = NULL;

    int iSolRevs                = 0;
    tag_t *ptSolRevs            = NULL;

    date_t dReleased;
    date_t dHighestDate;
    char szType[WSO_name_size_c + 1];


    dHighestDate.day= 0;
    dHighestDate.hour= 0;
    dHighestDate.minute= 0;
    dHighestDate.month= 0;
    dHighestDate.second= 0;
    dHighestDate.year= 0;

    iRetCode = BNL_tb_determine_object_type(tEco, szType);
    if (BNL_em_error_handler("BNL_tb_determine_object_class",BNL_module,EMH_severity_error,iRetCode, true)) return iRetCode;
    fprintf(rplog, "Processing change [%s].\n", szType);

    if (strcmp(szType, BNL_ENGCHANGE_REVISION) != 0)
    {
       //tag_t tRelType = NULL_TAG;
       tag_t tRelType2 = NULL_TAG;

       /*iRetCode=GRM_find_relation_type("CMHasImpactedItem", &tRelType);
       if (BNL_em_error_handler("GRM_find_relation_type",BNL_module,EMH_severity_error,iRetCode, true)) return iRetCode;

       iRetCode=GRM_list_secondary_objects_only(tEco, tRelType, &iRevs, &ptAffRevs);
       if (BNL_em_error_handler("GRM_list_secondary_objects_only1",BNL_module,EMH_severity_error,iRetCode, true)) return iRetCode;
       fprintf(rplog, "%s: Number of affected Items [%d].\n", BNL_module, iRevs);*/

       iRetCode=GRM_find_relation_type("CMHasSolutionItem", &tRelType2);
       if (BNL_em_error_handler("GRM_find_relation_type",BNL_module,EMH_severity_error,iRetCode, true)) return iRetCode;

       iRetCode=GRM_list_secondary_objects_only(tEco,tRelType2,&iSolRevs, &ptSolRevs);
       if (BNL_em_error_handler("GRM_list_secondary_objects_only2",BNL_module,EMH_severity_error,iRetCode, true)) return iRetCode;
       fprintf(rplog, "%s: Number of solution Items [%d].\n", BNL_module, iSolRevs);
    }
    else
    {
       iRetCode = ECM_get_contents(tEco, "affected_items", &iRevs, &ptAffRevs);
       if (BNL_em_error_handler("ECM_get_contents", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

       iRetCode = ECM_get_contents(tEco, "solution_items", &iSolRevs, &ptSolRevs);
       if (BNL_em_error_handler("ECM_get_contents", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }

    ptAffDocArray=MEM_alloc(sizeof(array_t)*(iRevs+iSolRevs));
    ptAffBomArray=MEM_alloc(sizeof(array_t)*(iRevs+iSolRevs));
    ptAffAttrArray=MEM_alloc(sizeof(array_t)*(iRevs+iSolRevs));

   //get effectivity date, is release date of eco.
   //The related objects of the eco are released later, so in case of configuring boms using this date it will go wrong.
   //for that reason we will searched the latest released object of the affected and solutions itemrevs.

    iRetCode = BNL_tb_get_instance_field_datevalue(tEco, "date_released", &dReleased);
    if (BNL_em_error_handler("BNL_tb_get_instance_field_datevalue", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    if (dReleased.day== 0 && dReleased.year== 0)
    {
        pszEffDate=MEM_alloc(sizeof(char)*2);
        strcpy(pszEffDate, "");
    }
    else
    {
        iRetCode = PREF_ask_char_value("BNL_date_format",0, &pszDateFormat);
        if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

        if (pszDateFormat==NULL)
        {
            fprintf(rplog, "The preference BNL_date_format is not found in the preferences file.\n");
            return -1;
        }

        //get the latest released object
        for (i= 0;i<iRevs;i++)
        {
            // JM|04-12-2008
            AOM_refresh(ptAffRevs[i], false);

            iRetCode = BNL_tb_get_instance_field_datevalue(ptAffRevs[i], "date_released", &dReleased);
            if (BNL_em_error_handler("BNL_tb_get_instance_field_datevalue", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

            /*
              dateOne == dateTwo   = 0
              dateOne > dateTwo    = 1
              dateOne < dateTwo    =-1
              */

            if (BNL_compare_dates(dHighestDate, dReleased)==-1)
            {

                 dHighestDate=dReleased;
            }



        }

        for (i= 0;i<iSolRevs;i++)
        {
            // JM|04-12-2008
            AOM_refresh(ptSolRevs[i], false);

            iRetCode = BNL_tb_get_instance_field_datevalue(ptSolRevs[i], "date_released", &dReleased);
            if (BNL_em_error_handler("BNL_tb_get_instance_field_datevalue", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

            /*
              dateOne == dateTwo   = 0
              dateOne > dateTwo    = 1
              dateOne < dateTwo    =-1
              */

            if (BNL_compare_dates(dHighestDate, dReleased)==-1)
            {

                 dHighestDate=dReleased;
            }

        }

        if (dHighestDate.day== 0 && dHighestDate.year== 0)
        {
          pszEffDate=MEM_alloc(sizeof(char)*2);
          strcpy(pszEffDate, "");
        }
        else
        {

          iRetCode = DATE_date_to_string(dHighestDate, pszDateFormat, &pszEffDate);
          if (BNL_em_error_handler("DATE_date_to_string", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
        }

        if (pszDateFormat!=NULL) MEM_free(pszDateFormat);

    }


   for (i= 0;i<iRevs;i++)
   {
       if (giDocDownload && gDocdownload_info.onelevelbomreps)
       {
          iRetCode = BNL_rp_create_onelevel_bomreport(ptAffRevs[i], pszRevRule, pszEffDate,1);
          if (BNL_em_error_handler("BNL_rp_create_onelevel_bomreport", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
       }

      /*
      does not work as expected
        iRetCode = ECM_get_problem_of_affected(tEco, ptAffRevs[i], &tPredecessor);
      if (BNL_em_error_handler("ECM_get_problem_of_affected", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
      */

      iRetCode = BNL_rp_get_predecessor(ptAffRevs[i], &tPredecessor);
      if (BNL_em_error_handler("BNL_rp_get_predecessor", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      ptAffDocArray[i].tProduct=ptAffRevs[i];
      ptAffDocArray[i].tPredecessor=tPredecessor;

      iRetCode = BNL_rp_document_diff("ecodoc",tPredecessor, ptAffRevs[i], &iDiffCount, &pDiffs);
      if (BNL_em_error_handler("BNL_rp_document_diff", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      ptAffDocArray[i].ptDiffs=pDiffs;
      ptAffDocArray[i].iDiffsCount=iDiffCount;

      pDiffs= NULL;
      iDiffCount= 0;

      ptAffBomArray[i].tProduct=ptAffRevs[i];
      ptAffBomArray[i].tPredecessor=tPredecessor;

      iRetCode = BNL_rp_bom_diff("ecobom",tPredecessor, ptAffRevs[i], pszViewType, pszRevRule, iLevel, &iDiffCount, &pDiffs);
      if (BNL_em_error_handler("BNL_rp_bom_diff", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      ptAffBomArray[i].ptDiffs=pDiffs;
      ptAffBomArray[i].iDiffsCount=iDiffCount;

      pDiffs= NULL;
      iDiffCount= 0;

    ptAffAttrArray[i].tProduct=ptAffRevs[i];
    ptAffAttrArray[i].tPredecessor=tPredecessor;

    iRetCode = BNL_rp_attr_diff("ecoattr", ptAffRevs[i],tPredecessor, &iDiffCount, &pDiffs);
    if (BNL_em_error_handler("BNL_rp_bom_diff", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    ptAffAttrArray[i].ptDiffs=pDiffs;
    ptAffAttrArray[i].iDiffsCount=iDiffCount;

    pDiffs= NULL;
    iDiffCount= 0;

   }

   for (i= 0;i<iSolRevs;i++)
   {
      /*
      does not work as aspected
        iRetCode = ECM_get_problem_of_affected(tEco, ptAffRevs[i], &tPredecessor);
      if (BNL_em_error_handler("ECM_get_problem_of_affected", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
      */

      if (giDocDownload && gDocdownload_info.onelevelbomreps)
      {

        iRetCode = BNL_rp_create_onelevel_bomreport(ptSolRevs[i], pszRevRule, pszEffDate,1);
        if (BNL_em_error_handler("BNL_rp_create_onelevel_bomreport", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

     }

      iRetCode = BNL_rp_get_predecessor(ptSolRevs[i], &tPredecessor);
      if (BNL_em_error_handler("BNL_rp_get_predecessor", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      ptAffDocArray[i+iRevs].tProduct=ptSolRevs[i];
      ptAffDocArray[i+iRevs].tPredecessor=tPredecessor;

      /*EN|03052004 Also when there is no predecessor just let see the info

    if (tPredecessor!=NULL_TAG)
      {

       iRetCode = BNL_rp_document_diff("ecodoc",tPredecessor, ptSolRevs[i], &iDiffCount, &pDiffs);
       if (BNL_em_error_handler("BNL_rp_document_diff", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
      }*/

    iRetCode = BNL_rp_document_diff("ecodoc",tPredecessor, ptSolRevs[i], &iDiffCount, &pDiffs);
       if (BNL_em_error_handler("BNL_rp_document_diff", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      ptAffDocArray[i+iRevs].ptDiffs=pDiffs;
      ptAffDocArray[i+iRevs].iDiffsCount=iDiffCount;

      pDiffs= NULL;
      iDiffCount= 0;

      ptAffBomArray[i+iRevs].tProduct=ptSolRevs[i];
      ptAffBomArray[i+iRevs].tPredecessor=tPredecessor;

    /*EN|03052004 Also when there is no predecessor just let see the info

      if (tPredecessor!=NULL_TAG)
      {
       iRetCode = BNL_rp_bom_diff("ecobom",tPredecessor, ptSolRevs[i], pszViewType, pszRevRule, iLevel, &iDiffCount, &pDiffs);
       if (BNL_em_error_handler("BNL_rp_bom_diff", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
      }
    */
    iRetCode = BNL_rp_bom_diff("ecobom",tPredecessor, ptSolRevs[i], pszViewType, pszRevRule, iLevel, &iDiffCount, &pDiffs);
    if (BNL_em_error_handler("BNL_rp_bom_diff", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    ptAffBomArray[i+iRevs].ptDiffs=pDiffs;
    ptAffBomArray[i+iRevs].iDiffsCount=iDiffCount;

    pDiffs= NULL;
    iDiffCount= 0;

    ptAffAttrArray[i+iRevs].tProduct=ptSolRevs[i];
    ptAffAttrArray[i+iRevs].tPredecessor=tPredecessor;

    iRetCode = BNL_rp_attr_diff("ecoattr", ptSolRevs[i],tPredecessor, &iDiffCount, &pDiffs);
    if (BNL_em_error_handler("BNL_rp_bom_diff", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    ptAffAttrArray[i+iRevs].ptDiffs=pDiffs;
    ptAffAttrArray[i+iRevs].iDiffsCount=iDiffCount;

    pDiffs= NULL;
    iDiffCount= 0;

  }


    *iAffDocCount=iRevs+iSolRevs;
    *pAffDocDiffs=ptAffDocArray;
    *iAffBomCount=iRevs+iSolRevs;
    *pAffBomDiffs=ptAffBomArray;
    *iAffAttrCount=iRevs+iSolRevs;
    *pAffAttrDiffs=ptAffAttrArray;

    if (iRevs!=0) MEM_free(ptAffRevs);
    if (pszEffDate!=NULL) MEM_free(pszEffDate);


    return iRetCode;
}

char* BNL_rp_get_full_id(tag_t tRevision)
{
    int iRetCode        = 0;
    char *pszId         = NULL;
    char *pszSep        = NULL;
    tag_t tItem         =NULL_TAG;
    char szRevId[33];
    char szItemId[33];
    char szClass[33];


    pszId=MEM_alloc(sizeof(char)*128);

    iRetCode = PREF_ask_char_value("FLColumnCatIVFSeparatorPref",0, &pszSep);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return NULL;

    if (tRevision!=NULL_TAG)
    {
        //since there can be items in structures by unconfigured revs we will first determine class
        iRetCode = BNL_tb_determine_super_class(tRevision,szClass);

        if (strcmp(szClass, ITEM_CLASS)== 0)
        {
            iRetCode = ITEM_ask_id(tRevision,szItemId);
            strcpy(szRevId, "???");

        }
        else
        {


          iRetCode = ITEM_ask_item_of_rev(tRevision, &tItem);
         //if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

         iRetCode = ITEM_ask_rev_id(tRevision,szRevId);
      //if (BNL_em_error_handler("ITEM_ask_rev_id", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

         iRetCode = ITEM_ask_id(tItem,szItemId);
      //if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
        }

      sprintf(pszId, "%s%s%s",szItemId, pszSep,szRevId);
    }
    else
    {
        strcpy(pszId, " ");
    }

    if (pszSep != NULL) MEM_free(pszSep);

    return pszId;


}


int BNL_set_pred_quant(t_XMLElement *pProps,char *pszOldQuant)
{
    int iRetCode            = 0;
    t_XMLElement *pProp     = NULL;
    t_Attribute  *pAttr     = NULL;

    pProp=BNL_xml_find_first(pProps,XML_EL_PROP);

    while (pProp)
    {
        pAttr=pProp->pAttributes;

        while (pAttr!=NULL)
        {
            if (strcmp(pAttr->pszName,XML_ATTR_ATTR)== 0)
            {
               break;
            }
           pAttr=pAttr->pNext;
        }

        /*now we found the attribuut name which contains the property
        name we need to get the value from the property from given object*/

        if (strcmp(pAttr->pszValue, "predquant")== 0)
        {
            BNL_xml_element_set_value(pProp, pszOldQuant);
        }

        pProp=BNL_xml_find_next(pProp,XML_EL_PROP);
    }

    return iRetCode;
}


int BNL_rp_get_process_state(tag_t tObject,char **pszState)
{
    int iRetCode            = 0;
    int iProcesses          = 0;
    int p                   = 0;
    int t                   = 0;

    tag_t tProcessesId      =NULL_TAG;

    char szTaskName[WSO_name_size_c+1];

    tag_t *ptProcesses          = NULL;
    logical *pNulls             = NULL;
    logical *pEmtpies           = NULL;

    int iSubTasks       = 0;
    tag_t *ptTasks   = NULL;

    EPM_state_t  pState;

    char szTaskState[33];


     iRetCode = POM_attr_id_of_attr("process_stage_list", "workspaceobject", &tProcessesId);

     iRetCode = BNL_tb_load_object(tObject, POM_read_lock);

     iRetCode = POM_length_of_attr(tObject, tProcessesId, &iProcesses);

     if (iProcesses== 0)
     {
         iRetCode = BNL_tb_load_object(tObject, POM_no_lock);
         *pszState= NULL;
         return iRetCode;
     }

     iRetCode = POM_ask_attr_tags  ( tObject,tProcessesId,0, iProcesses, &ptProcesses, &pNulls, &pEmtpies);

     iRetCode = BNL_tb_load_object(tObject, POM_no_lock);

     //loop through processes assumes there is always one

     fprintf(rplog, "%s :Found [%d] attached processes. \n", BNL_module, iProcesses);

     for (p= 0;p<iProcesses;p++)
     {
         iRetCode = EPM_ask_sub_tasks(ptProcesses[p], &iSubTasks, &ptTasks);

         fprintf(rplog, "%s :Found [%d] subtasks processes. \n", BNL_module, iProcesses);


             for (t= 0;t<iSubTasks;t++)
             {
                 iRetCode = EPM_ask_state(ptTasks[t], &pState);

                 iRetCode = EPM_ask_state_string  (pState,szTaskState);

                 if (strcmp(szTaskState, "Started")== 0)
                 {
                      iRetCode = EPM_ask_name  ( ptTasks[t],szTaskName);

                      *pszState=BNL_strings_copy_string(szTaskName);

                      p=iProcesses; //just to break out the upperloop;

                      break;
                 }

             }

             if (t==iSubTasks)
             {
                 //there was no break
                 //so get the name of the root task
                 iRetCode = EPM_ask_name  ( ptProcesses[p],szTaskName);

                 *pszState=BNL_strings_copy_string(szTaskName);

             }
     }

     if (ptTasks!=NULL) MEM_free(ptTasks);
     if (ptProcesses!=NULL) MEM_free(ptProcesses);
     if (pNulls!=NULL) MEM_free(pNulls);
     if (pEmtpies!=NULL) MEM_free(pEmtpies);

    return iRetCode;
}

int BNL_rp_create_xml_props
(
  tag_t tObject,
  char *pszTemplate,
  int iLevel,
  t_XMLElement **pDiff,
  char *pszObjType,
  char *pszAttrName,
  char *pszAttrValues,
  char *pszAllowedVal,
  char *pszErrorDescription
)
{
  int iRetCode        = 0;

  char szKey[128];
  char *pszTemplateFile   = NULL;
  char *pszTemp           = NULL;
  char *pszVal            = NULL;
  char *pszDummy          = NULL;
  char *pszPacked         = NULL;
  char szLevel[5];
  char szType[33];

  t_XMLElement *pTemplate = NULL;
  t_XMLElement *pProp     = NULL;
  t_Attribute  *pAttr     = NULL;

  tag_t tDummy            =NULL_TAG;


  /*create xml structure based on template
    template looks like

  <props>
    <prop attr=""></prop>
     ..
    <props>

      where props are variable

    */

    /*get the template file from the iman env

       template key = BNL_prop_<variable>

  */

  sprintf(szKey, "BNL_prop_%s", pszTemplate);

  iRetCode = PREF_ask_char_value(szKey,0, &pszTemp);
  if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

  if (pszTemp==NULL)
  {
    fprintf(rplog, "The preference [%s] was not found, so there is no template to create properties structure\n",szKey);
    //return iRetCode;
  }
  else
  {
    BNL_tb_replace_env_for_path(pszTemp, &pszTemplateFile);

    if (pszTemplateFile==NULL)
    {
      pszTemplateFile=pszTemp;
    }
    else
    {
      MEM_free(pszTemp);
    }

    BNL_xml_load_document(pszTemplateFile, &pTemplate);

    if (tObject!=NULL_TAG)
    {
      //remember latest for performance reasons
      tag_t tLatest   =NULL_TAG;

      /*find the change prop*/
      pProp=BNL_xml_find_first(pTemplate,XML_EL_PROP);

      while (pProp)
      {
        pAttr=pProp->pAttributes;

        while (pAttr!=NULL)
        {
          if (strcmp(pAttr->pszName,XML_ATTR_ATTR)== 0)
          {
            break;
          }
          pAttr=pAttr->pNext;
        }

        /*now we found the attribute name which contains the property name
          we need to get the value from the property from given object*/

        if (strcmp(pAttr->pszValue, "has_bom")== 0)
        {
          tag_t tPropTemp   = NULL_TAG;
          tag_t tViewTemp   = NULL_TAG;

          fprintf(rplog, "DEBUG: has_bom.\n");

          iRetCode = PROP_ask_property_by_name(tObject, "bl_bomview_rev", &tPropTemp);

          fprintf(rplog, "DEBUG: checking property [bl_bomview_rev].\n");

          if (tPropTemp != NULL_TAG)
          {
            //iRetCode = PROP_ask_value_tag(tPropTemp, &tItemTemp);
            iRetCode = PROP_ask_value_tag(tPropTemp, &tViewTemp);
            fprintf(rplog, "DEBUG: [bl_bomview_rev] = [%d].\n", tViewTemp);
          }

          if (tViewTemp != NULL_TAG)
          {
            BNL_xml_element_set_value(pProp, "Y");
          }
          else
          {
            BNL_xml_element_set_value(pProp, "N");
          }
        }
        else if (strcmp(pAttr->pszValue, "level")== 0)
        {
          sprintf(szLevel, "%d", iLevel);
          BNL_xml_element_set_value(pProp,szLevel);
        }
        else if (strcmp(pAttr->pszValue, "id")== 0)
        {
          /*object should be revision*/
          pszDummy=BNL_rp_get_full_id(tObject);
          BNL_xml_element_set_value(pProp, pszDummy);
          MEM_free(pszDummy);
        }
        else if (strcmp(pAttr->pszValue, "predquant")== 0)
        {
          BNL_xml_element_set_value(pProp, "");
        }
        else if (strcmp(pAttr->pszValue, "latest_revision")== 0)
        {
          //tag_t tItem     =NULL_TAG;
          //char szItemId[33];
          if (tLatest==NULL_TAG)
          {
            iRetCode = BNL_rp_get_latest_productline_revision(tObject, &tLatest);
            if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision (xml props)", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
          }

          if (tLatest!=NULL_TAG && tLatest!=tObject)
          {
            //get the itemid
            //iRetCode = ITEM_ask_item_of_rev(tLatest, &tItem);
            //iRetCode = ITEM_ask_id(tItem,szItemId);
            //BNL_xml_element_set_value(pProp,szItemId);

            pszDummy=BNL_rp_get_full_id(tLatest);
            BNL_xml_element_set_value(pProp, pszDummy);
            MEM_free(pszDummy);
          }
        }
        else if (strcmp(pAttr->pszValue, "ug_2D")== 0)
        {
          iRetCode = BNL_rp_get_related_object(tObject,RELATION_2D3D,TYPE_2D, &tDummy);
          if (BNL_em_error_handler("BNL_rp_get_related_object", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

          if (tDummy==NULL_TAG)
          {
            BNL_xml_element_set_value(pProp, "NO");
          }
          else
          {
            BNL_xml_element_set_value(pProp, "YES");
          }
        }
        else if (strcmp(pAttr->pszValue, "ug_2D_latest")== 0)
        {
          if (tLatest==NULL_TAG)
          {
            iRetCode = BNL_rp_get_latest_productline_revision(tObject, &tLatest);
            if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision (xml props)", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
          }

          if (tLatest!=NULL_TAG)
          {
            iRetCode = BNL_rp_get_related_object(tLatest,RELATION_2D3D,TYPE_2D, &tDummy);
            if (BNL_em_error_handler("BNL_rp_get_related_object", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
          }

          if (tDummy==NULL_TAG)
          {
            BNL_xml_element_set_value(pProp, "NO");
          }
          else
          {
            BNL_xml_element_set_value(pProp, "YES");
          }
        }
        else if (strcmp(pAttr->pszValue, "ug_3D")== 0)
        {
          iRetCode = BNL_rp_get_related_object(tObject,RELATION_2D3D,TYPE_3D, &tDummy);
          if (BNL_em_error_handler("BNL_rp_get_related_object", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

          if (tDummy==NULL_TAG)
          {
            BNL_xml_element_set_value(pProp, "NO");
          }
          else
          {
            BNL_xml_element_set_value(pProp, "YES");
          }
        }
        else if (strcmp(pAttr->pszValue, "ug_3D_latest")== 0)
        {
          if (tLatest==NULL_TAG)
          {
            iRetCode = BNL_rp_get_latest_productline_revision(tObject, &tLatest);
            if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision (xml props)", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
          }

          if (tLatest!=NULL_TAG)
          {
            iRetCode = BNL_rp_get_related_object(tLatest,RELATION_2D3D,TYPE_3D, &tDummy);
            if (BNL_em_error_handler("BNL_rp_get_related_object", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
          }

          if (tDummy==NULL_TAG)
          {
            BNL_xml_element_set_value(pProp, "NO");
          }
          else
          {
            BNL_xml_element_set_value(pProp, "YES");
          }
        }
        else if (strcmp(pAttr->pszValue, "empty")== 0)
        {
          BNL_xml_element_set_value(pProp, "");
        }
        else if (strcmp(pAttr->pszValue, "release_state")== 0)
        {
          char szStatus[33];

          //check if object has status
          //if has status get the status and use the status name
          iRetCode = BNL_tb_ask_object_status(tObject,szStatus);
          if (BNL_em_error_handler("BNL_tb_ask_object_status", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

          if (strcmp(szStatus, "-")!=0)
          {
            BNL_xml_element_set_value(pProp,szStatus);
          }
          else
          {
            char *pszState  = NULL;
            //if not released check if it is in process if not then field is empty
            //if in process the get the current running taskname of the process and return that name as field.
            iRetCode = BNL_rp_get_process_state(tObject, &pszState);
            if (BNL_em_error_handler("BNL_tb_ask_object_status", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

            if (pszState!=NULL)
            {
              BNL_xml_element_set_value(pProp, pszState);
              free(pszState);
            }
            else
            {
              BNL_xml_element_set_value(pProp, "");
            }
          }
        }
        else if (strcmp(pAttr->pszValue, "owning_site_latest")== 0)
        {
          if (tLatest==NULL_TAG)
          {
            iRetCode = BNL_rp_get_latest_productline_revision(tObject, &tLatest);
            if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision (xml props)", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
          }

          if (tLatest!=NULL_TAG)
          {
            //JM: 05-02-2009
            AOM_refresh(tLatest, false);
            iRetCode = BNL_tb_get_property(tLatest, "owning_site", &pszVal);

            if (pszVal!=NULL)
            {
              BNL_xml_element_set_value(pProp, pszVal);
              MEM_free(pszVal);
              pszVal = NULL;
            }
            else
            {
              fprintf(rplog, "Found property [%s] given in property file [%s] does not exist on given object.\n", pAttr->pszValue, pszTemplateFile);
            }
          }
        }
        else if (strcmp(pAttr->pszValue, "owning_user_latest")== 0)
        {
          if (tLatest==NULL_TAG)
          {
            iRetCode = BNL_rp_get_latest_productline_revision(tObject, &tLatest);
            if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision (xml props)", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
          }

          if (tLatest!=NULL_TAG)
          {
            //JM: 05-02-2009
            AOM_refresh(tLatest, false);
            iRetCode = BNL_tb_get_property(tLatest, "owning_user", &pszVal);

            if (pszVal!=NULL)
            {
              BNL_xml_element_set_value(pProp, pszVal);
              MEM_free(pszVal);
              pszVal = NULL;
            }
            else
            {
              fprintf(rplog, "Found property [%s] given in property file [%s] does not exist on given object.\n", pAttr->pszValue, pszTemplateFile);
            }
          }
        }
        else if (strcmp(pAttr->pszValue, "fei_error_description")== 0)
        {
          BNL_xml_element_set_value(pProp, pszErrorDescription);
        }
        else if (strcmp(pAttr->pszValue, "allowed_stat")== 0 || strcmp(pAttr->pszValue, "allowed_attr_val")== 0)
        {
          BNL_xml_element_set_value(pProp, pszAllowedVal);
        }
        else if (strcmp(pAttr->pszValue, "attr_name")== 0)
        {
          BNL_xml_element_set_value(pProp, pszAttrName);
        }
        else if (strcmp(pAttr->pszValue, "attr_val")== 0)
        {
          BNL_xml_element_set_value(pProp, pszAttrValues);
        }
        else if (strcmp(pAttr->pszValue, "obj_type")== 0)
        {
          BNL_xml_element_set_value(pProp, pszObjType);
        }
        // JM: Replaced by "has_bom" property
        /*else if (strcmp(pAttr->pszValue, "bl_num_children")== 0)
        {
          BNL_tb_determine_object_type(tObject, szType);

          if (strcmp(szType, "BOMLine") == 0)
          {
            int iNumChild = 0;
            int iAttrChildId = 0;

            iRetCode = BOM_line_look_up_attribute(bomAttr_numChildren, &iAttrChildId);
            if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return false;

            iRetCode = BOM_line_ask_attribute_int(tObject, iAttrChildId, &iNumChild);
            if (BNL_em_error_handler("BOM_line_ask_attribute_tag", BNL_module, EMH_severity_error, iRetCode, true)) return false;

            if (iNumChild > 0)
            {
              BNL_xml_element_set_value(pProp, "Y");
            }
            else
            {
              BNL_xml_element_set_value(pProp, "N");
            }
          }
        }*/
        else
        {
          if (tObject!=NULL_TAG)
          {
            int iCount = 0;
            char *pszTemp   = NULL;

            char **pszValues = NULL;

            tag_t tObj = NULL_TAG;

            //JM: 10-02-2009: Don't refresh bomlines
            BNL_tb_determine_object_type(tObject, szType);
            //fprintf(rplog, "szType [%s]\n", szType);

            if (strcmp(szType, "BOMLine") != 0)
            {
              //JM: 05-02-2009
              AOM_refresh(tObject, false);
            }

            pszTemp = BNL_strings_copy_string(pAttr->pszValue);
            iRetCode = BNL_split_multiple_values(pszTemp,'.', &iCount, &pszValues);
            if (BNL_em_error_handler("BNL_split_multiple_values", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
            if (iCount == 1)
            {
              iRetCode = BNL_tb_get_property(tObject, pAttr->pszValue, &pszVal);
            }
            else
            {
              int iValues       = 0;
              int k             = 0;
              tag_t tProp       = NULL_TAG;
              tag_t *ptValues   = NULL;

              tObj = tObject;

              for (k= 0; k<iCount-1; k++)
              {
                iValues = 0;
                //fprintf(rplog, "DEBUG: tObj [%d] pszValues [%s]\n", tObj, pszValues[k]);
                iRetCode = PROP_ask_property_by_name(tObj, pszValues[k], &tProp);
                //if (BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
                BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true);


                //fprintf(rplog, "DEBUG: tProp [%d] \n", tProp);

                if (tProp != NULL_TAG)
                {
                  iRetCode = PROP_ask_value_tags(tProp, &iValues, &ptValues);
                  BNL_em_error_handler("PROP_ask_value_tags", BNL_module, EMH_severity_error, iRetCode, true);

                  //fprintf(rplog, "DEBUG: iValues [%d] \n", iValues);
                }

                if (iValues > 0)
                {
                  tObj = ptValues[0];
                  //fprintf(rplog, "DEBUG: tObj [%d] \n", tObj);
                  MEM_free(ptValues);
                }
                else
                {
                  tObj = NULL_TAG;
                }
              }

              if (tObj != NULL_TAG)
              {
                BNL_tb_determine_object_type(tObj, szType);

                //fprintf(rplog, "DEBUG: szType [%s] pszValues[iCount-1] [%s]\n", szType, pszValues[iCount-1]);

                iRetCode = BNL_tb_get_property(tObj, pszValues[iCount-1], &pszVal);

                //fprintf(rplog, "DEBUG: pszVal [%s]\n", pszVal);
              }
            }
            if (pszVal!=NULL)
            {
              // Packed
              BNL_xml_element_get_attribute_value(pProp, XML_EL_PACKED , &pszPacked);

              if (pszPacked != NULL && strcmp(pszPacked, "true") == 0)
              {
                int iCount      = 0;
                int p           = 0;
                tag_t *ptLines  = NULL;

                fprintf(rplog, "INFO: packed=true\n");

                iRetCode = BOM_line_ask_packed_lines(tObject, &iCount, &ptLines);

                if (iRetCode != 0)
                {
                  fprintf(rplog, "Warning, error [%s] when asking the packed lines.\n", iRetCode);
                }
                else if (iCount > 0)
                {
                  char *pszTempVal      = NULL;

                  if (pszVal != NULL)
                  {
                    MEM_free(pszVal);
                    pszVal = NULL;
                  }

                  pszVal = MEM_alloc(sizeof(char)*1);
                  strcpy(pszVal, "");

                  BOM_line_unpack(tObject);

                  //JM: 05-02-2009
                  AOM_refresh(tObject, false);
                  iRetCode = BNL_tb_get_property(tObject, pAttr->pszValue, &pszTempVal);

                  if (pszTempVal != NULL && strlen(pszTempVal) > 0)
                  {
                    pszVal = (char *)MEM_realloc(pszVal, ((int)strlen(pszTempVal) + 1));
                    strcpy(pszVal, pszTempVal);
                  }

                  if (pszTempVal != NULL)
                  {
                    MEM_free(pszTempVal);
                    pszTempVal = NULL;
                  }

                  for (p = 0; p < iCount; p++)
                  {
                    //JM: 05-02-2009
                    AOM_refresh(ptLines[p], false);
                    iRetCode = BNL_tb_get_property(ptLines[p], pAttr->pszValue, &pszTempVal);

                    if (pszTempVal != NULL && strlen(pszTempVal) > 0)
                    {
                      if (strlen(pszVal) == 0)
                      {
                        pszVal = (char *)MEM_realloc(pszVal, ((int)strlen(pszTempVal) + 1));
                        strcpy(pszVal, pszTempVal);
                      }
                      else
                      {
                        pszVal = (char *)MEM_realloc(pszVal, ((int)strlen(pszVal) + (int)strlen(pszTempVal) + 2));
                        sprintf(pszVal, "%s,%s", pszVal, pszTempVal);
                      }
                    }
                    if (pszTempVal != NULL)
                    {
                      MEM_free(pszTempVal);
                      pszTempVal = NULL;
                    }
                  }
                  MEM_free(ptLines);

                  BOM_line_pack(tObject);
                }
                BNL_xml_element_set_value(pProp, pszVal);
                MEM_free(pszVal);
                pszVal = NULL;
              }
              else
              {

                BNL_xml_element_set_value(pProp, pszVal);
                MEM_free(pszVal);
                pszVal = NULL;
              }
            }
            else
            {
              fprintf(rplog, "Found property [%s] given in property file [%s] does not exist on given object.\n", pAttr->pszValue, pszTemplateFile);
            }
            if (pszTemp) free(pszTemp);
          }
        }
        pProp=BNL_xml_find_next(pProp,XML_EL_PROP);
      }
    }
    MEM_free(pszTemplateFile);
  } // End of if (pszTemp==NULL)

  *pDiff=pTemplate;

  return iRetCode;
}

/*
int BNL_rp_create_xml_attr_props
(
  tag_t tObject,               <I>
  char *pszTemplate,           <I>
  char *pszAttrname,           <I>
  char *pszAttrval,            <I>
  char *pszOldAttrval,          <I>
  tt_XMLElement **pDiff        <OF>
)

 Description:
 This function is used for item attributes reports to generate the result xml
 */
int BNL_rp_create_xml_attr_props
(
  tag_t tObject,
  char *pszTemplate,
  char *pszAttrname,
  char *pszAttrval,
  char *pszOldAttrval,
  t_XMLElement **pDiff
)
{
  int iRetCode        = 0;

  char szKey[128];
  char *pszTemplateFile   = NULL;
  char *pszTemp           = NULL;
  char *pszVal            = NULL;

  char szType[33];

  t_XMLElement *pTemplate = NULL;
  t_XMLElement *pProp     = NULL;
  t_Attribute  *pAttr     = NULL;

  tag_t tItemRev          =NULL_TAG;
  tag_t tItem             =NULL_TAG;
  tag_t tRel              =NULL_TAG;

  tag_t *ptRels           = NULL;

  int iAttrRevId              = 0;
  int iAttrItemId             = 0;
  int iRels                   = 0;


  /*create xml structure based on template
    template looks like

  <props>
    <prop attr=""></prop>
     ..
    <props>

      where props are variable

    */

    /*get the template file from the iman env

       template key = BNL_prop_<variable>

  */

  sprintf(szKey, "BNL_prop_%s", pszTemplate);

  fprintf(rplog, "BNL_rp_create_xml_attr_props\n");

  iRetCode = PREF_ask_char_value(szKey, 0, &pszTemp);
  if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

  if (pszTemp == NULL)
  {
    fprintf(rplog, "The preference [%s] was not defined, so there is no template to create properties structure.\n", szKey);
    return iRetCode;
  }

  BNL_tb_replace_env_for_path(pszTemp, &pszTemplateFile);

  if (pszTemplateFile == NULL)
  {
    pszTemplateFile = pszTemp;
  }
  else
  {
    MEM_free(pszTemp);
  }

  BNL_xml_load_document(pszTemplateFile, &pTemplate);

  if (tObject!=NULL_TAG)
  {
    //remember latest for performance reasons
    tag_t tLatest   =NULL_TAG;

    /*find the change prop*/
    pProp=BNL_xml_find_first(pTemplate,XML_EL_PROP);

    while (pProp)
    {
      pAttr=pProp->pAttributes;

      while (pAttr!=NULL)
      {
        if (strcmp(pAttr->pszName,XML_ATTR_ATTR)== 0)
        {
          break;
        }
        pAttr=pAttr->pNext;
      }

      /*now we found the attribute name which contains the property name
        we need to get the value from the property from given object*/


      if (strcmp(pAttr->pszValue, "attr_name")== 0)
      {
        BNL_xml_element_set_value(pProp, pszAttrname);
      }
      else if (strcmp(pAttr->pszValue, "attr_val")== 0)
      {
        BNL_xml_element_set_value(pProp, pszAttrval);
      }
      else if (strcmp(pAttr->pszValue, "old_attr_val")== 0)
      {
        BNL_xml_element_set_value(pProp, pszOldAttrval);
      }
      //pszOldAttrval
      else
      {
        if (tObject!=NULL_TAG)
        {
          int iCount = 0;
          char *pszTemp = NULL;

          char **pszValues = NULL;

          tag_t tObj = NULL_TAG;

          pszTemp = BNL_strings_copy_string(pAttr->pszValue);
          iRetCode =BNL_split_multiple_values(pszTemp,'.', &iCount, &pszValues);
          if (BNL_em_error_handler("BNL_split_multiple_values", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
          if (iCount == 1)
          {
            // For getting an empty line
            if (strcmp(pszAttrname, "-") == 0 && strcmp(pAttr->pszValue, "object_type") == 0)
            {
              pszVal = (char *)MEM_alloc(2);
              strcpy(pszVal, "-");
            }
            else
            {
              iRetCode = BNL_tb_get_property(tObject, pAttr->pszValue, &pszVal);
            }
          }
          else
          {
            int iValues       = 0;
            int k             = 0;
            tag_t tProp       = NULL_TAG;
            tag_t *ptValues   = NULL;

            tObj = tObject;
            for (k= 0; k<iCount-1; k++)
            {
              iValues = 0;

              iRetCode = PROP_ask_property_by_name(tObj, pszValues[k], &tProp);
              //if (BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
              BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true);

              if (tProp != NULL_TAG)
              {
                iRetCode = PROP_ask_value_tags(tProp, &iValues, &ptValues);
                BNL_em_error_handler("PROP_ask_value_tags", BNL_module, EMH_severity_error, iRetCode, true);
              }

              if (iValues > 0)
              {
                tObj = ptValues[0];
                MEM_free(ptValues);
              }
              else
              {
                tObj = NULL_TAG;
              }
            }

            if (tObj != NULL_TAG)
            {
              BNL_tb_determine_object_type(tObj, szType);

              iRetCode = BNL_tb_get_property(tObj, pszValues[iCount-1], &pszVal);
            }
          }
          if (pszVal!=NULL)
          {
            BNL_xml_element_set_value(pProp, pszVal);
            MEM_free(pszVal);
            pszVal = NULL;
          }
          else
          {
            fprintf(rplog, "Found property [%s] given in property file [%s] does not exist on given object.\n", pAttr->pszValue, pszTemplateFile);
          }

          if (pszTemp) free(pszTemp);
        }
      }
      pProp=BNL_xml_find_next(pProp,XML_EL_PROP);
    }
  }

  *pDiff=pTemplate;

  MEM_free(pszTemplateFile);

  return iRetCode;
}

/*
int BNL_rp_create_xml_struc
(
  diffinfo_t pDiffInfo,        <I>
  char *pszKey,                <I>
  tt_XMLElement **pDiff        <OF>
)

 Description:
 This function is used for successor/predecessor report to generate the result xml
 */
int BNL_rp_create_xml_struc
(
  diffinfo_t pDiffInfo,
  char *pszKey,
  t_XMLElement **pXMLDiff
)
{

    int iRetCode     = 0;

    t_XMLElement *pDiff     = NULL;
    t_XMLElement *pProps    = NULL;
    t_XMLElement *pChange   = NULL;


    pDiff=BNL_xml_element_create(pszKey,NULL);
    BNL_xml_element_copy(pDiffInfo.pXMLprops, &pProps);
    BNL_xml_element_add_element(pDiff, pProps);

    *pXMLDiff=pDiff;

    return iRetCode;
}


int BNL_rp_create_xml_diff
(
  diffinfo_t pDiffInfo,
  t_XMLElement **pXMLDiff
)
{

    int iRetCode     = 0;
    char ch;
    char szString[5];

    t_XMLElement *pDiff     = NULL;
    t_XMLElement *pProps    = NULL;
    t_XMLElement *pChange   = NULL;



    if (pDiffInfo.iChange==-1) ch='-';
    if (pDiffInfo.iChange== 0)  ch='o';
    if (pDiffInfo.iChange==1)  ch='+';

    sprintf(szString, "%c",ch);

    pDiff=BNL_xml_element_create("diff",NULL);

    pChange=BNL_xml_element_create("change",szString);

    BNL_xml_element_add_element(pDiff, pChange);
    BNL_xml_element_copy(pDiffInfo.pXMLprops, &pProps);
    BNL_xml_element_add_element(pDiff, pProps);

    *pXMLDiff=pDiff;

    return iRetCode;
}

int BNL_rp_xmlstuc_to_string(t_XMLElement *pXMLDoc, char **pszXMLDoc)
{

    int iRetCode            = 0;
    int iFileNo             = 0;


    struct _stat buf;

    char *pszDir             = NULL;
    char *pszBuf             = NULL;
    char *pszLine            = NULL;
    char szFullPath[MAX_PATH];

    FILE *fpTemp                = NULL;


    /*now we have a xmldocument*/

    pszDir=getenv("TEMP");
    sprintf(szFullPath, "%s%swebrep%d", pszDir,FILESEP, iFileNo);


    fpTemp=fopen(szFullPath, "w+t");

    if (fpTemp==NULL)
    {
        while(fpTemp==NULL && iFileNo<10)
        {
            iFileNo++;
            sprintf(szFullPath, "%s%swebrep%d", pszDir,FILESEP, iFileNo);
            fpTemp=fopen(szFullPath, "w+t");
        }

        if (fpTemp==NULL)
        {
            fprintf(rplog, "Error while open temponary xml record file\n");
            return -1;
        }

    }

    iRetCode = BNL_xml_write_document(fpTemp, pXMLDoc);
    fflush(fpTemp);
    fclose(fpTemp);

    /*determine size of file*/
    _stat( szFullPath, &buf );

    //pszBuf=MEM_alloc(sizeof(char)*buf.st_size);
    pszBuf=malloc(sizeof(char)*buf.st_size);

    *pszBuf='\0';


    fpTemp=fopen(szFullPath, "r");

    while (BNL_fm_readfileline(fpTemp, &pszLine)!=-1)
    {
        strcat(pszBuf, pszLine);
        strcat(pszBuf, "\n");
        free(pszLine);
    }

    // JM|28-11-2008: Close temportay file before deleting
    BNL_fm_close_file(fpTemp);

    BNL_fm_delete_file(szFullPath,NULL);


    *pszXMLDoc=pszBuf;

    return iRetCode;

}

/*
int BNL_rp_get_xml_struc
(
  char *pszReportType         <I>
  array_t pArray,             <I>
  char **pszXMLDoc,
  t_XMLElement **pDocument

)
*/
int BNL_rp_get_xml_struc
(
  char *pszReportType,
  array_t pArray,
  char **pszXMLDoc,
  t_XMLElement **pDocument

)
{
    int iRetCode        = 0;
    int i               = 0;

    char szPref[128];

    t_XMLElement *pParent;

    t_XMLElement *pProd;
    t_XMLElement *pPred;
    diffinfo_t   *pDiffs        = NULL;
    diffinfo_t   *pNode         = NULL;

    t_XMLElement *pDiff         = NULL;

    t_XMLElement *pProps       = NULL;



    /*need to create structure

  <document>
    <product>
      <prop atrr="xxx"></prop>
    </product>
    <predecessor>
       <prop attr="xxx"></prop>
    </predecessor>
    <diffs>
  <diff>
      <change></change>
      <prop attr="xxx"></prop>
  </diff>

  */
    pParent=BNL_xml_element_create("document",NULL);


    pProd=BNL_xml_element_create("product",NULL);

    sprintf(szPref, "prod_%s", pszReportType);
    BNL_rp_create_xml_props(pArray.tProduct,szPref,0, &pProps,NULL,NULL,NULL,NULL,NULL);

    BNL_xml_element_add_element(pProd, pProps);

    pPred=BNL_xml_element_create("predecessor",NULL);

    sprintf(szPref, "pred_%s", pszReportType);
    BNL_rp_create_xml_props(pArray.tPredecessor,szPref,0, &pProps,NULL,NULL,NULL,NULL,NULL);

    BNL_xml_element_add_element(pPred, pProps);

    BNL_xml_element_add_element(pParent, pProd);
    BNL_xml_element_add_element(pParent, pPred);

    pDiffs=pArray.ptDiffs;

    if (pDiffs!=NULL)
    {
      /*check if it is sorted if yes then use it*/
      if (pDiffs->pNext!=NULL || pDiffs->pPrev!=NULL)
      {
        /*first loop through to the first node*/
        pNode=pDiffs;
        while (pNode->pPrev!=NULL)
        {
            pNode=pNode->pPrev;
        }


        while(pNode!=NULL)
        {
            BNL_rp_create_xml_diff(*pNode, &pDiff);
            BNL_xml_element_add_element(pParent, pDiff);
            pNode=pNode->pNext;
        }



      }
      else
      {
       for (i= 0;i<pArray.iDiffsCount;i++)
       {

        BNL_rp_create_xml_diff(pDiffs[i], &pDiff);
        BNL_xml_element_add_element(pParent, pDiff);
       }
      }

    }


    iRetCode = BNL_rp_xmlstuc_to_string(pParent, pszXMLDoc);

    *pDocument=pParent;


    return iRetCode;

}

int BNL_rp_get_succ_pred_xml_struc
(
  array_t pArray,
  diffinfo_t *pSuccs,
  int iSucc,
  diffinfo_t *pPreds,
  int iPred,
  char **pszXMLDoc,
  t_XMLElement **pDocument

)
{
    int iRetCode        = 0;
    int i               = 0;

    t_XMLElement *pParent;

    t_XMLElement *pProd;
    t_XMLElement *pPred;

    diffinfo_t   *pTemp        = NULL;
    diffinfo_t   *pDiffs       = NULL;
    diffinfo_t   *pNode        = NULL;

    t_XMLElement *pDiff         = NULL;

    t_XMLElement *pProps       = NULL;



    /*need to create structure

  <document>
    <product>
      <prop atrr="xxx"></prop>
    </product>
    <predecessor>
       <prop attr="xxx"></prop>
    </predecessor>
    <succ>
        <change></change>
        <prop attr="xxx"></prop>
    </succ>
    <pred>
      <change></change>
      <prop attr="xxx"></prop>
    </pred>
  */
    pParent=BNL_xml_element_create("document",NULL);

    pProd=BNL_xml_element_create("product",NULL);

    BNL_rp_create_xml_props(pArray.tProduct, "prod_succpred",0, &pProps,NULL,NULL,NULL,NULL,NULL);

    BNL_xml_element_add_element(pProd, pProps);

    pPred=BNL_xml_element_create("predecessor",NULL);

    BNL_rp_create_xml_props(pArray.tPredecessor, "pred_succpred",0, &pProps,NULL,NULL,NULL,NULL,NULL);

    BNL_xml_element_add_element(pPred, pProps);

    BNL_xml_element_add_element(pParent, pProd);
    BNL_xml_element_add_element(pParent, pPred);

    pTemp=pSuccs;

    if (pTemp!=NULL)
    {
      /*check if it is sorted if yes then use it*/
      if (pTemp->pNext!=NULL || pTemp->pPrev!=NULL)
      {
        /*first loop through to the first node*/
        pNode=pTemp;
        while (pNode->pPrev!=NULL)
        {
            pNode=pNode->pPrev;
        }
        while(pNode!=NULL)
        {
            BNL_rp_create_xml_struc(*pNode, "succ", &pDiff);
            BNL_xml_element_add_element(pParent, pDiff);
            pNode=pNode->pNext;
        }
      }
      else
      {
        for (i= 0;i<iSucc;i++)
        {
          BNL_rp_create_xml_struc(pTemp[i], "succ", &pDiff);
          BNL_xml_element_add_element(pParent, pDiff);
        }
      }

    }

    pTemp=pPreds;

    if (pTemp!=NULL)
    {
      /*check if it is sorted if yes then use it*/
      if (pTemp->pNext!=NULL || pTemp->pPrev!=NULL)
      {
        /*first loop through to the first node*/
        pNode=pTemp;
        while (pNode->pPrev!=NULL)
        {
            pNode=pNode->pPrev;
        }
        while(pNode!=NULL)
        {
            BNL_rp_create_xml_struc(*pNode, "pred", &pDiff);
            BNL_xml_element_add_element(pParent, pDiff);
            pNode=pNode->pNext;
        }
      }
      else
      {
        for (i= 0;i<iPred;i++)
        {
          BNL_rp_create_xml_struc(pTemp[i], "pred", &pDiff);
          BNL_xml_element_add_element(pParent, pDiff);
        }
      }

    }


    iRetCode = BNL_rp_xmlstuc_to_string(pParent, pszXMLDoc);

    *pDocument=pParent;


    return iRetCode;

}

int BNL_rp_xml_error(char *pszError,char **pszXMLError,t_XMLElement **pDocument )
{
    int iRetCode        = 0;

    t_XMLElement *pParent;
    t_XMLElement *pError;


    if (!giDocDownload)
    {
     pParent=BNL_xml_element_create(XML_EL_DOCUMENT,NULL);

     pError=BNL_xml_element_create(XML_EL_ERROR, pszError);

     BNL_xml_element_add_element(pParent, pError);

     BNL_rp_xmlstuc_to_string(pParent, pszXMLError);


     *pDocument=pParent;
    }
    else
    {
        fprintf(rplog, "%s\n", pszError);
        *pDocument= NULL;
        *pszXMLError= NULL;
    }

    return iRetCode;
}


int BNL_rp_get_predecessor
(
  tag_t tProduct,
  tag_t *tPredecessor
)
{
    int iRetCode            = 0;
    int iAttCount           = 0;
    int iCount              = 0;


    tag_t *ptPredecessors    = NULL;
    tag_t tRelType          =NULL_TAG;


    char *pszPredRelation   = NULL;


    /*get relation from iman_env file*/
    iRetCode = PREF_ask_char_value("BNL_pred_relation",0, &pszPredRelation);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (pszPredRelation==NULL)
    {
        pszPredRelation=MEM_alloc(sizeof(char)*33);
        strcpy(pszPredRelation,DEFAULT_PREDRELATION);

    }

    iRetCode = GRM_find_relation_type(pszPredRelation, &tRelType);
    if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    MEM_free(pszPredRelation);

    iRetCode = GRM_list_secondary_objects_only(tProduct,tRelType, &iCount, &ptPredecessors);
    if (BNL_em_error_handler("GRM_list_secondary_objects_only", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    /*at the moment we support only one predecessor*/
    if (iCount>0)
        *tPredecessor=ptPredecessors[0];
    else
        *tPredecessor=NULL_TAG;


    if (iCount > 0) MEM_free(ptPredecessors);

    return iRetCode;
}

int BNL_rp_export_imanfiles(tag_t tRevision, tag_t tDataset)
{
  int iRetCode        = 0;
  int i               = 0;
  int r               = 0;
  int n               = 0;
  int iNrs            = 0;

  tag_t *ptNrs        = NULL;

  char szClass[33];
  char szOrgName[128];
  char szDsName[33];

  char *pszFullId         = NULL;
  char *pszExportLoc      = NULL;
  char *pszPath           = NULL;
  char *pszName           = NULL;
  char *pszExt            = NULL;
  char *pszExportFileName = NULL;


  pszFullId = BNL_rp_get_full_id(tRevision);


  WSOM_ask_name(tDataset, szDsName);
  fprintf(rplog, "Name of dataset [%s].\n", szDsName);

  // Check if the dataset should be exported
  for (i = 0; i < gDocdownload_info.iDsNames; i++)
  {
    logical lMatched        = false;

    fprintf(rplog, "%s: Checking name of dataset [%s] with pattern [%s].\n", BNL_module, szDsName, gDocdownload_info.pszDsNames[i]);

    NR_is_name_matching_pattern(gDocdownload_info.pszDsNames[i], szDsName, &lMatched);

    if (lMatched)
    {
      fprintf(rplog, "%s: Name matches, dataset will be skipped...\n", BNL_module);

      return 0;
    }
  }


  /* Refresh the dataset to be sure we have the latest stuff in it */
  AOM_refresh(tDataset, false);

  pszExportFileName = malloc(sizeof(char)* (strlen(pszFullId) + strlen(szDsName) + 4) );
  sprintf(pszExportFileName, "%s-%s", pszFullId, szDsName);

  BNL_fm_replace_illegal_chars(pszExportFileName);

  for ( n= 0; n < gDocdownload_info.iNrs; n++)
  {
    iRetCode = AE_ask_all_dataset_named_refs(tDataset, gDocdownload_info.pszNrs[n], &iNrs, &ptNrs);

    for (r = 0; r < iNrs; r++)
    {
      // check if nr is imanfile
      BNL_tb_determine_object_class(ptNrs[r], szClass);

      if (strcmp(szClass, IMANFILE_CLASS)== 0)
      {
        char *pszFileName       = NULL;

        iRetCode = AOM_refresh(ptNrs[r],false);

        iRetCode = IMF_ask_original_file_name(ptNrs[r], szOrgName);
        BNL_fm_parse_path(szOrgName, &pszPath, &pszName, &pszExt);

        if (pszExt == NULL)
        {
          fprintf(rplog, "%s: Unable to get the extension of the named reference with name [%s]. Skipping...\n", BNL_module, szOrgName);
          continue;
        }

        if (!BNL_tb_values_in_list(gDocdownload_info.iExtTypes, gDocdownload_info.pszExtTypes, 1, &pszExt))
        {
          pszExportLoc = malloc(sizeof(char) * (strlen(gDocdownload_info.pszExportDir) + strlen(pszExportFileName) + strlen(pszExt) + 8 ));
          sprintf(pszExportLoc, "%s\\%s.%s", gDocdownload_info.pszExportDir, pszExportFileName, pszExt);

          pszFileName = malloc(sizeof(char) * strlen(pszExportFileName) + strlen(pszExt) + 8);
          sprintf(pszFileName, "%s.%s", pszExportFileName, pszExt);

          if (!BNL_fm_check_file_or_dir(pszExportLoc))
          {
            iRetCode = IMF_export_file(ptNrs[r], pszExportLoc);
            if (iRetCode!=0)
            {
              BNL_report_a_line(pszFullId,szDsName, pszFileName, "Y");
            }
            else
            {
              BNL_report_a_line(pszFullId,szDsName, pszFileName, "");
            }
          }
          free(pszFileName);
          free(pszPath);
          pszPath= NULL;
        } // End of if (!BNL_tb_values_in_list(gDocdownload_info.iExtTypes, gDocdownload_info.pszExtTypes, 1, &pszExt))
      } // End of if (strcmp(szClass, IMANFILE_CLASS)== 0)
    } // End of for (r = 0; r < iNrs; r++)
    if (ptNrs != NULL) MEM_free(ptNrs);
    ptNrs = NULL;
  }

  if (pszFullId != NULL) MEM_free(pszFullId);

  return iRetCode;
}

/*if it is pcb then it will chech ikf released.
  if a non pcb 1 returned.
  if pcb and released 1 returned.
  if pcb and non released 0 returned.
*/
// JM|01-09-2010: Not used anymore
/*int is_released_pcb_or_non_pcb(tag_t tRevision)
{
    int f                 = 0;
    int isPcb             = 0;
    int iRetCode          =1;
    int iIdFilters        = 0;
    int iDescFilters      = 0;
    int iFail             = 0;


    char **pszIdFilters   = NULL;
    char **pszDescFilters = NULL;
    char *pszFullId       = NULL;
    char szStatus[64];
    char *pszNode         = NULL;

    char szDescription[WSO_desc_size_c + 1];

    iFail=PREF_ask_char_values("FEI_pcb_id_filter", &iIdFilters, &pszIdFilters);
    if (iFail==1700)
    {
        iFail= 0;
        EMH_clear_errors();
    }
    else
    {
      if (BNL_em_error_handler("PREF_ask_char_values (FEI_pcb_id_filter)", BNL_module, EMH_severity_error, iFail, false)) return 0;
    }

    iFail=PREF_ask_char_values("FEI_pcb_desc_filter", &iDescFilters, &pszDescFilters);
    if (iFail==1700)
    {
        iFail=1;
        EMH_clear_errors();
    }
    else
    {
      if (BNL_em_error_handler("PREF_ask_char_values (FEI_pcb_id_filter)", BNL_module, EMH_severity_error, iFail, false)) return 0;
    }


    if (pszIdFilters==NULL && pszDescFilters==NULL)
    {
        goto function_exit;
    }

    pszFullId=BNL_rp_get_full_id(tRevision);

    //determine if it is pcb
    if (pszIdFilters!=NULL && pszDescFilters!=NULL)
    {
        //check first id if not pcb follow id then check description
        for (f= 0;f<iIdFilters;f++)
        {
                pszNode=strchr(pszIdFilters[f],'*');
                if (pszNode!=NULL)
                {
                  *pszNode='\0';
                }

                if (strstr(pszFullId, pszIdFilters[f])!=NULL)
                {
                    isPcb=1;
                    break;
                }
        }

        if (isPcb== 0)
        {
          iFail=WSOM_ask_description(tRevision,szDescription);
          if (BNL_em_error_handler("WSOM_ask_description", BNL_module, EMH_severity_error, iFail, false)) return 0;

          for (f= 0;f<iDescFilters;f++)
          {
            if (strstr(szDescription, pszDescFilters[f])!=NULL)
            {
                isPcb=1;
                break;
            }
            else
            {
                isPcb= 0;
            }
          }//for (f= 0;f<iDescFilters;f++)
        }//if (isPcb== 0)

    }// if (pszIdFilters!=NULL && pszDescFilters!=NULL)

    if (pszIdFilters!=NULL && pszDescFilters==NULL)
    {
        for (f= 0;f<iIdFilters;f++)
        {
                pszNode=strchr(pszIdFilters[f],'*');
                if (pszNode!=NULL)
                {
                  *pszNode='\0';
                }

                if (strstr(pszFullId, pszIdFilters[f])!=NULL)
                {
                    fprintf(rplog, "%s:[%s] is defined as pcb\n", BNL_module, pszFullId);
                    isPcb=1;
                    break;
                }
        }


    }//if (pszIdFilters!=NULL && pszDescFilters==NULL)

    if (pszIdFilters==NULL && pszDescFilters!=NULL)
    {
        iFail=WSOM_ask_description(tRevision,szDescription);
        if (BNL_em_error_handler("WSOM_ask_description", BNL_module, EMH_severity_error, iFail, false)) return 0;

        for (f= 0;f<iDescFilters;f++)
        {
            if (strstr(szDescription, pszDescFilters[f])!=NULL)
            {
                fprintf(rplog, "%s:[%s] is defined as pcb\n", BNL_module, pszFullId);
                isPcb=1;
                break;
            }
        }
    }//if (pszIdFilters==NULL && pszDescFilters!=NULL)


    if (isPcb)
    {
                    fprintf(rplog, "%s:[%s] is defined as pcb\n", BNL_module, pszFullId);

                    iFail=BNL_tb_ask_object_status(tRevision,szStatus);
                    if (BNL_em_error_handler("BNL_tb_ask_object_status", BNL_module, EMH_severity_error, iFail, false)) return 0;

                    if (strcmp(szStatus, "-")== 0)
                    {
                      iRetCode = 0;
                      goto function_exit;
                    }
                    else
                    {
                      iRetCode = 1;
                      goto function_exit;
                    }
      }//if (isPcb)
    else
    {
        iRetCode = 0;
    }

function_exit:


    if (pszFullId!=NULL) MEM_free(pszFullId);
    if (pszIdFilters!=NULL) MEM_free(pszIdFilters);
    if (pszDescFilters!=NULL) MEM_free(pszDescFilters);

    return iRetCode;

}
*/

int BNL_get_subclass(tag_t tRev, char **pszSubClass)
{
  int iRetCode           = 0;
  int iObjs              = 0;

  tag_t tItem            = NULL_TAG;
  tag_t tRelType         = NULL_TAG;
  tag_t tIMFObj          = NULL_TAG;

  tag_t *ptObjects       = NULL;

  char *pszAttrVal       = NULL;
  char *pszSubClassAttr  = NULL;


  // Get the Item
  ITEM_ask_item_of_rev(tRev, &tItem);

  if (tItem != NULL_TAG)
  {
    GRM_find_relation_type("IMAN_master_form", &tRelType);

    if (tRelType != NULL_TAG)
    {
      GRM_list_secondary_objects_only(tItem, tRelType, &iObjs, &ptObjects);

      if (iObjs > 0)
      {
        tIMFObj = ptObjects[0];
      }
    } // End of if (tRelType != NULL_TAG)
  }

  if (tIMFObj != NULL_TAG)
  {
    // Get FEI_Physical_Subclass
    BNL_tb_pref_ask_char_value("FEI_pcb_subclass_attr", IMAN_preference_all, &pszSubClassAttr);

    if (pszSubClassAttr != NULL)
    {
      AOM_UIF_ask_value(tIMFObj, pszSubClassAttr, &pszAttrVal);
    }
    else
    {
      AOM_UIF_ask_value(tIMFObj, "FEI_Physical_Subclass", &pszAttrVal);
    }
  }


  *pszSubClass = pszAttrVal;

  if (iObjs != 0) MEM_free(ptObjects);
  if (pszSubClassAttr != NULL) MEM_free(pszSubClassAttr);

  return iRetCode;
}


int BNL_pcb_export(tag_t tRev, logical lTop, logical *plExported)
{
  int iRetCode        = 0;
  int iSubClasses     = 0;

  char *pszPcbExpCmd  = NULL;
  char *pszCommand    = NULL;
  char *pszSubClass   = NULL;

  char **pszSubClasses  = NULL;

  char szItemid[33];
  char szRevId[33];
  char szPrefCommand[256];

  tag_t tItem         = NULL_TAG;


  *plExported = false;

  // Get the pcb subclasses preferences
  //BNL_tb_pref_ask_char_values("FEI_pcb_subclasses", IMAN_preference_site, &iSubClasses, &pszSubClasses);
  BNL_tb_pref_ask_char_values("FEI_pcb_subclasses", IMAN_preference_all, &iSubClasses, &pszSubClasses);

  if (iSubClasses == 0)
  {
    fprintf(rplog, "%s: Preference [FEI_pcb_subclasses] has not been defined.\n", BNL_module);
    goto function_exit;
  }

  // Get subclass
  BNL_get_subclass(tRev, &pszSubClass);

  if (pszSubClass == NULL)
  {
    fprintf(rplog, "%s: Unable to get the subclass.\n", BNL_module);
    goto function_exit;
  }

  fprintf(rplog, "%s: Subclass [%s].\n", BNL_module, pszSubClass);

  // Check if subclass type is defined
  if (!BNL_tb_value_in_list(pszSubClass, iSubClasses, pszSubClasses))
  {
    fprintf(rplog, "%s: Subclass type [%s] has not been configured.\n", BNL_module, pszSubClass);
    goto function_exit;
  }

  // Check if cmd was defined
  sprintf(szPrefCommand, "FEI_pcb_%s_cmd", pszSubClass);
  //BNL_tb_pref_ask_char_value(szPrefCommand, IMAN_preference_site, &pszPcbExpCmd);
  BNL_tb_pref_ask_char_value(szPrefCommand, IMAN_preference_all, &pszPcbExpCmd);

  if (pszPcbExpCmd == NULL)
  {
    fprintf(rplog, "%s: Preference [%s] has not been defined.\n", BNL_module, szPrefCommand);
    goto function_exit;
  }

  if (lTop && giPcbSkipTarget)
  {
    fprintf(rplog, "%s: Top level, skipping pcb action.\n", BNL_module);
    goto function_exit;
  }

  if (!lTop)
  {
    fprintf(rplog, "%s: No top level.\n", BNL_module);
  }

  // No do the export
  iRetCode = ITEM_ask_item_of_rev(tRev, &tItem);
  if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

  iRetCode = ITEM_ask_id(tItem, szItemid);
  if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

  iRetCode = ITEM_ask_rev_id(tRev, szRevId);
  if (BNL_em_error_handler("ITEM_ask_rev_id", BNL_module, EMH_severity_error, iRetCode, false)) goto function_exit;

  pszCommand = malloc(sizeof(char) * (strlen(pszPcbExpCmd) + strlen(szItemid) + strlen(gDocdownload_info.pszExportDir) + 8));

  //command exportdir itemid revid
  sprintf(pszCommand, "%s \"%s\" %s %s", pszPcbExpCmd, gDocdownload_info.pszExportDir, szRevId, szItemid);

  fprintf(rplog, "Running command [%s]\n", pszCommand);

  system(pszCommand);

  *plExported = true;

function_exit:

  if (pszSubClass) MEM_free(pszSubClass);
  if (iSubClasses > 0) MEM_free(pszSubClasses);
  if (pszPcbExpCmd != NULL) MEM_free(pszPcbExpCmd);

  if (pszCommand != NULL) free(pszCommand);

  return iRetCode;
}


int BNL_revision_download(tag_t tRev, logical lTop, logical *plPcbExported)
{
  int iRetCode        = 0;
  int t               = 0;
  int iCount          = 0;
  int r               = 0;

  tag_t tRelType      = NULL_TAG;
  tag_t *ptRelations  = NULL;

  char *pszType       = NULL;

  char szType[33];
  char szStatus[33];

  logical lExported   = false;


  iRetCode = BNL_pcb_export(tRev, lTop, &lExported);
  BNL_em_error_handler("BNL_pcb_export", BNL_module, EMH_severity_error, iRetCode, false);

  *plPcbExported = lExported;

  if (giDocDownloadSkipExport && lExported)
  {
    fprintf(rplog, "PCB, skipping export action.\n");
  }
  else
  {
    // moved block
    for (t = 0; t < gDocdownload_info.iRels; t++)
    {
      // get relations
      iRetCode = GRM_find_relation_type(gDocdownload_info.pszRels[t], &tRelType);
      if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

      iRetCode = GRM_list_secondary_objects_only(tRev, tRelType, &iCount, &ptRelations);
      if (BNL_em_error_handler("GRM_list_secondary_objects_only", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

      // if one of the found types export
      for (r = 0; r < iCount; r++)
      {
        // check if not pom stub ?
        BNL_tb_determine_object_type(ptRelations[r], szType);
        pszType = szType;

        if (BNL_tb_values_in_list(gDocdownload_info.iDsTypes, gDocdownload_info.pszDsTypes, 1, &pszType))
        {
          //if type is jt dataset then check its release status ofr revision
          if (strcmp(szType, BNL_DIRECTMODEL_TYPE) == 0)
          {
            iRetCode = BNL_tb_ask_object_status(tRev, szStatus);
            if (BNL_em_error_handler("GRM_list_secondary_objects_only", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

            if (strcmp(szStatus, BNL_RELEASED_STATUS) == 0
              || strcmp(szStatus, BNL_OBSOLETE_STATUS) == 0
              || strcmp(szStatus, BNL_SERVICE_STATUS) == 0
              || strcmp(szStatus, BNL_PRODUCTION_STATUS) == 0
              || strcmp(szStatus, BNL_TECHOBS_STATUS) == 0)
            {
              iRetCode = BNL_rp_export_imanfiles(tRev, ptRelations[r]);
              BNL_em_error_handler("BNL_rp_export_imanfiles", BNL_module, EMH_severity_error, iRetCode, false);
            }
          }
          else
          {
            // if something goes wrong just give error message but dont break the loop
            iRetCode = BNL_rp_export_imanfiles(tRev, ptRelations[r]);
            BNL_em_error_handler("BNL_rp_export_imanfiles", BNL_module, EMH_severity_error, iRetCode, false);
          }
        }
      } // End of for (r = 0;r < iCount; r++)
    } // End of for (t = 0; t < gDocdownload_info.iRels; t++)
  }

  return iRetCode;
}

int BNL_rp_export_docs(array_t pArray, int iOnlyDiffs)
{
    int iRetCode        = 0;
    int a               = 0;
    int iLevel          = 0;

    char *pszType       = NULL;
    char szClass[33];
    char szType[33];

    logical lExported   = false;


    if (pArray.tPredecessor!=NULL_TAG)
    {
    }

    if (pArray.tProduct!=NULL_TAG && iOnlyDiffs== 0 )
    {
        //if it is not a pcb it will not start a download
        iRetCode = BNL_revision_download(pArray.tProduct, true, &lExported); // This is top level
        if (BNL_em_error_handler("BNL_revision_download", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    }//end of if (pArray.tProduct!=NULL_TAG)

    if (giDocDownloadSkipExport && lExported)
    {
      fprintf(rplog, "PCB, skipping actions.\n");
    }
    else
    {
      //export the diffs, loop
      for (a= 0;a<pArray.iDiffsCount;a++)
      {
        int iLevelTemp     = 0;
        //if (giDocDownloadSkipExport && lExported)
        //{
          //fprintf(rplog, "PCB, skipping further actions.\n");
          //break;
        //}
        {
          t_XMLElement *pXMLProps     = NULL;
          t_XMLElement *pProp         = NULL;
          t_Attribute  *pAttr         = NULL;

          pXMLProps = pArray.ptDiffs[a].pXMLprops;

          //BNL_xml_write_document(rplog, pAttr);

          pProp = BNL_xml_find_first(pXMLProps, XML_EL_PROP);

          while (pProp)
          {
            pAttr = pProp->pAttributes;

            while (pAttr!=NULL)
            {
              if (strcmp(pAttr->pszName, XML_ATTR_ATTR)== 0)
              {
                break;
              }
              pAttr = pAttr->pNext;
            }

            if (strcmp(pAttr->pszValue, "level")== 0)
            {
              fprintf(rplog, "DEBUG: LEVEL [%s]\n", pProp->pszElementValue);

              iLevelTemp = atoi(pProp->pszElementValue);
              fprintf(rplog, "DEBUG: iLevelTemp [%d] iLevel [%d]\n", iLevelTemp, iLevel);
              if (iLevel == 0 || iLevelTemp > iLevel)
              {
                iLevel = iLevelTemp;
              }
              else if (iLevelTemp <= iLevel)
              {
                // Reset
                fprintf(rplog, "DEBUG: Resetting...\n");
                lExported = false;
                iLevel = iLevelTemp;
              }
              break;
            }
            pProp = BNL_xml_find_next(pProp, XML_EL_PROP);
          }
        }

        if (giDocDownloadSkipExport && lExported)
        {
          fprintf(rplog, "PCB, continue...\n");
          continue;
        }


          //Double code here, could be optimised to do the logic in a higher level but can't overview the impact now.

          //just export
          if (iOnlyDiffs== 0)
          {


              //object can be ItemRevision of dataset
              iRetCode = BNL_tb_determine_super_class(pArray.ptDiffs[a].tObject,szClass);
              if (BNL_em_error_handler("BNL_tb_determine_super_class", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

              //only on itemrevs start the pcb download if its a pcb
              if (strcmp(szClass, ITEMREVISION_CLASS)== 0)
              {
                //if it is not a pcb it will not start a download
                //if something goes wrong just give error message but dont break the loop
                iRetCode = BNL_revision_download(pArray.ptDiffs[a].tObject, false, &lExported);
                BNL_em_error_handler("BNL_revision_download", BNL_module, EMH_severity_error, iRetCode, false);

              }//if (strcmp(szClass, ITEMREVISION_CLASS)== 0)


              if (strcmp(szClass,DATASET_CLASS)== 0)
              {

                  //check type, if type export
                  iRetCode = BNL_tb_determine_object_type(pArray.ptDiffs[a].tObject,szType);
                  if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

                  pszType=szType;
                  if (BNL_tb_values_in_list(gDocdownload_info.iDsTypes,gDocdownload_info.pszDsTypes,1, &pszType))
                  {
                      //if something goes wrong just give error message but dont break the loop
                      iRetCode = BNL_rp_export_imanfiles(pArray.tProduct, pArray.ptDiffs[a].tObject);
                      BNL_em_error_handler("BNL_rp_export_imanfiles", BNL_module, EMH_severity_error, iRetCode, false);
                  }

              }//if (strcmp(szClass,DATASET_CLASS)== 0)
          }
          else
          {
              //only export the added diffs
              if (pArray.ptDiffs[a].iChange==1)
              {
                  //object can be ItemRevision of dataset
                  iRetCode = BNL_tb_determine_super_class(pArray.ptDiffs[a].tObject,szClass);
                  if (BNL_em_error_handler("BNL_tb_determine_super_class", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

                  //only on itemrevs start the pcb download if its a pcb
                  if (strcmp(szClass, ITEMREVISION_CLASS)== 0)
                  {
                  //if it is not a pcb it will not start a download
                  //if something goes wrong just give error message but dont break the loop
                  iRetCode = BNL_revision_download(pArray.ptDiffs[a].tObject, false, &lExported);
                  BNL_em_error_handler("BNL_revision_download", BNL_module, EMH_severity_error, iRetCode, false);

                  }//if (strcmp(szClass, ITEMREVISION_CLASS)== 0)


                  if (strcmp(szClass,DATASET_CLASS)== 0)
                  {

                      //check type, if type export
                      iRetCode = BNL_tb_determine_object_type(pArray.ptDiffs[a].tObject,szType);
                      if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

                      pszType=szType;
                      if (BNL_tb_values_in_list(gDocdownload_info.iDsTypes,gDocdownload_info.pszDsTypes,1, &pszType))
                      {
                          //if something goes wrong just give error message but dont break the loop
                          iRetCode = BNL_rp_export_imanfiles(pArray.tProduct, pArray.ptDiffs[a].tObject);
                          BNL_em_error_handler("BNL_rp_export_imanfiles", BNL_module, EMH_severity_error, iRetCode, false);
                      }

                  }//if (strcmp(szClass,DATASET_CLASS)== 0)
              }//end of if (pArray.ptDiffs[a].iChange==1)

          }


      }//for (a= 0;a<pArray.iDiffsCount;a++)
    }

    return iRetCode;
}

int BNL_rp_web_bomdiff
(
  tag_t tProduct,
  tag_t tPredecessor,
  char *pszRevRule,
  char **pszXMLdiff,
  t_XMLElement **pDocument
)
{
    int iRetCode        = 0;
    int iDiffCount      = 0;

    char *pszViewType   = NULL;
    //char *pszRevRule    = NULL;
    char *pszLevel      = NULL;
    char *pszSort       = NULL;

    diffinfo_t *pDiffs  = NULL;
    array_t pArray;

     clock_t start, finish;
    double  duration;

    fprintf(rplog, "BNL_rp_web_bomdiff executed.\n");

    start=clock();

    if (tPredecessor==NULL_TAG)
    {
        iRetCode = BNL_rp_get_predecessor(tProduct, &tPredecessor);

    }


    iRetCode = PREF_ask_char_value("BNL_rep_viewtype",0, &pszViewType);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    //iRetCode = PREF_ask_char_value("BNL_rep_revrule",0, &pszRevRule);
    //if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    fprintf(rplog, "Revision rule: [%s]\n", pszRevRule);

    iRetCode = PREF_ask_char_value("BNL_rep_bomlevel",0, &pszLevel);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;


    if (pszViewType==NULL)
    {
        pszViewType=MEM_alloc(sizeof(char)*33);
        strcpy(pszViewType,DEFAULT_VIEWTYPE);

    }

    //if (pszRevRule==NULL)
    //{
      //  pszRevRule=MEM_alloc(sizeof(char)*33);
        //strcpy(pszRevRule,DEFAULT_REVRULE);
    //}

    if (pszLevel==NULL)
    {
        pszLevel=MEM_alloc(sizeof(char)*5);
        strcpy(pszLevel,DEFAULT_LEVEL);
    }



    iRetCode = BNL_rp_bom_diff("bomdiff",tPredecessor,tProduct, pszViewType, pszRevRule,atoi(pszLevel), &iDiffCount, &pDiffs);


    MEM_free(pszViewType);
    //MEM_free(pszRevRule);
    MEM_free(pszLevel);


    pArray.iDiffsCount=iDiffCount;
    pArray.ptDiffs=pDiffs;
    pArray.tPredecessor=tPredecessor;
    pArray.tProduct=tProduct;


    iRetCode = PREF_ask_char_value("BNL_sort_bomlinesdiff",0, &pszSort);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (pszSort!=NULL)
    {
     fprintf(rplog, "Sorting document differences on property [%s]\n", pszSort);
     BNL_rp_sort_on_prop(pszSort, iDiffCount, pDiffs);
     MEM_free(pszSort);
    }


    BNL_rp_get_xml_struc("bomdiff", pArray, pszXMLdiff, pDocument);

    BNL_rp_free_diff_struc(iDiffCount, pDiffs);



    finish=clock();
    duration = (double)(finish - start) / CLOCKS_PER_SEC;
    fprintf(rplog, "BOM difference report creation took:[%2.1f]s\n",duration);


    return iRetCode;

}

int BNL_rp_web_docdiff
(
  tag_t tProduct,
  tag_t tPredecessor,
  char **pszXMLdiff,
  t_XMLElement **pDocument
)
{
    int iRetCode = 0;
    int iDiffCount;

    char *pszSort   = NULL;

    diffinfo_t *pDiffs  = NULL;
    array_t pArray;

    clock_t start, finish;
    double  duration;

    fprintf(rplog, "BNL_rp_web_docdiff executed.\n");

    start=clock();


    if (tPredecessor==NULL_TAG)
    {
        iRetCode = BNL_rp_get_predecessor(tProduct, &tPredecessor);
    }

    /*read the change between predecessor and product */

    BNL_rp_document_diff("docdiff",tPredecessor,tProduct, &iDiffCount, &pDiffs);

    pArray.iDiffsCount=iDiffCount;
    pArray.ptDiffs=pDiffs;
    pArray.tPredecessor=tPredecessor;
    pArray.tProduct=tProduct;

    iRetCode = PREF_ask_char_value("BNL_sort_docsdiff",0, &pszSort);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (pszSort!=NULL)
    {
     fprintf(rplog, "Sorting document differences on property [%s]\n", pszSort);
     BNL_rp_sort_on_prop(pszSort, iDiffCount, pDiffs);
     MEM_free(pszSort);
    }

    BNL_rp_get_xml_struc("docdiff", pArray, pszXMLdiff, pDocument);

    fprintf(rplog, "Freeing memory using BNL_rp_free_diff_struc in BNL_rp_web_docdiff\n");
    BNL_rp_free_diff_struc(iDiffCount, pDiffs);

    finish=clock();
    duration = (double)(finish - start) / CLOCKS_PER_SEC;
    fprintf(rplog, "Document difference report creation took:[%2.1f]s\n",duration);

    return iRetCode;
}

/*
int BNL_rp_web_attrdiff
(
  tag_t tProduct,               <I>
  tag_t tPredecessor,           <I>
  char **pszXMLdiff,            <OF>
  t_XMLElement **pDocument      <OF>
)

 Description:
 Generates Change of Item/ITEMREVISION Attributes
 */
int BNL_rp_web_attrdiff
(
  tag_t tProduct,
  tag_t tPredecessor,
  char **pszXMLdiff,
  t_XMLElement **pDocument
)
{
    int iRetCode = 0;
    int iDiffCount;

    char *pszSort   = NULL;

    diffinfo_t *pDiffs  = NULL;
    array_t pArray;

    clock_t start, finish;
    double  duration;

    fprintf(rplog, "BNL_rp_web_attrdiff executed.\n");

    start=clock();


    if (tPredecessor==NULL_TAG)
    {
        iRetCode = BNL_rp_get_predecessor(tProduct, &tPredecessor);
    }

    /*read the change between predecessor and product */

    if (tPredecessor==NULL_TAG)
    {
      fprintf(rplog, "tPredecessor is null.\n");
    }
    BNL_rp_attr_diff("attrdiff",tProduct,tPredecessor, &iDiffCount, &pDiffs);

    pArray.iDiffsCount=iDiffCount;
    pArray.ptDiffs=pDiffs;
    pArray.tPredecessor=tPredecessor;
    pArray.tProduct=tProduct;

    /*iRetCode = PREF_ask_char_value("BNL_sort_docsdiff",0, &pszSort);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (pszSort!=NULL)
    {
     fprintf(rplog, "Sorting document differences on property [%s]\n", pszSort);
     BNL_rp_sort_on_prop(pszSort, iDiffCount, pDiffs);
     MEM_free(pszSort);
    }
     */
    BNL_rp_get_xml_struc("attrdiff", pArray, pszXMLdiff, pDocument);

    fprintf(rplog, "Freeing memory using BNL_rp_free_diff_struc in BNL_rp_web_proddiff\n");
    BNL_rp_free_diff_struc(iDiffCount, pDiffs);

    finish=clock();
    duration = (double)(finish - start) / CLOCKS_PER_SEC;
    fprintf(rplog, "Attribute difference report creation took:[%2.1f]s\n",duration);

    return iRetCode;
}

/*
int BNL_rp_web_prod_attr
(
  tag_t tProduct,               <I>
  char **pszXMLdiff,            <OF>
  t_XMLElement **pDocument      <OF>
)

 Description:
 generates Product Attributes report
 */
int BNL_rp_web_prod_attr
(
  tag_t tProduct,
  char **pszXMLdiff,
  t_XMLElement **pDocument
)
{
    int iRetCode = 0;
    int iDiffCount;

    char *pszSort   = NULL;

    diffinfo_t *pDiffs  = NULL;
    array_t pArray;

    clock_t start, finish;
    double  duration;

    fprintf(rplog, "BNL_rp_web_prod_attr executed.\n");

    start=clock();

    BNL_rp_prod_attr("attrrep",tProduct, &iDiffCount, &pDiffs);

    pArray.iDiffsCount=iDiffCount;
    pArray.ptDiffs=pDiffs;
    pArray.tPredecessor=NULL_TAG;
    pArray.tProduct=tProduct;

    /*iRetCode = PREF_ask_char_value("BNL_sort_docsdiff",0, &pszSort);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (pszSort!=NULL)
    {
     fprintf(rplog, "Sorting document differences on property [%s]\n", pszSort);
     BNL_rp_sort_on_prop(pszSort, iDiffCount, pDiffs);
     MEM_free(pszSort);
    }
     */
    BNL_rp_get_xml_struc("attrrep", pArray, pszXMLdiff, pDocument);

    fprintf(rplog, "Freeing memory using BNL_rp_free_diff_struc in BNL_rp_web_prod_attr\n");
    BNL_rp_free_diff_struc(iDiffCount, pDiffs);

    finish=clock();
    duration = (double)(finish - start) / CLOCKS_PER_SEC;
    fprintf(rplog, "Product Attribute report creation took:[%2.1f]s\n",duration);

    return iRetCode;
}


int BNL_rp_web_docs
(
  tag_t tRev,
  char **pszXMLDiff,
  t_XMLElement **pDocument
)
{
    int iRetCode            = 0;
    int iDocCount           = 0;

    char *pszSort           = NULL;

    diffinfo_t *ptDocuments = NULL;
    array_t pArray;

    clock_t start, finish;
    double  duration;

    fprintf(rplog, " BNL_rp_web_docs executed.\n");

    start=clock();

    iRetCode = BNL_rp_get_documents(tRev, "doclist", &iDocCount, &ptDocuments);

    pArray.iDiffsCount=iDocCount;
    pArray.ptDiffs=ptDocuments;
    pArray.tPredecessor=NULL_TAG;
    pArray.tProduct=tRev;

    /*sort the docs*/

    iRetCode = PREF_ask_char_value("BNL_sort_docs",0, &pszSort);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (pszSort!=NULL)
    {

     fprintf(rplog, "Sorting documents on property [%s]\n", pszSort);
     BNL_rp_sort_on_prop(pszSort, iDocCount, ptDocuments);
     MEM_free(pszSort);
    }


    BNL_rp_get_xml_struc("docsrep", pArray, pszXMLDiff, pDocument);

    if (giDocDownload)
    {
        iRetCode = BNL_rp_export_docs(pArray,0);
    }

    fprintf(rplog, "Freeing memory using BNL_rp_free_diff_struc in BNL_rp_web_docs\n");


    BNL_rp_free_diff_struc(iDocCount, ptDocuments);

    finish=clock();
    duration = (double)(finish - start) / CLOCKS_PER_SEC;
    fprintf(rplog, "Web docs report creation took:[%2.1f]s\n",duration);

    return iRetCode;

}

int BNL_rp_create_onelevel_bomreport(tag_t tRevision, char *pszRevRule, char *pszEffDate, int iTopLine)
{
    int iRetCode            = 0;
    char *pszXml            = NULL;
    //char *pszRevRule        = NULL;
    char *pszBomFile        = NULL;
    char *pszPostFix        = NULL;
    t_XMLElement *pDocument = NULL;

    char *pszFullId         = NULL;
    char *pszsl             = NULL;
    char *pszFileName       = NULL;


    FILE *fpHtml            = NULL;

    char *pszXmlRep         = NULL;
    array_t *pArray         = NULL;


    /*create one level bomreport form product
        iRetCode = PREF_ask_char_value("BNL_rep_revrule",0, &pszRevRule);
        if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

        if (pszRevRule==NULL)
        {
          pszRevRule=MEM_alloc(sizeof(char)*33);
          strcpy(pszRevRule,DEFAULT_REVRULE);
        }
        */

        iRetCode = PREF_ask_char_value("FEI_docdownload_onelevelbom_postfix",0, &pszPostFix);
        if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

        //revrule effdate
        if (iTopLine)
        {
            iRetCode = BNL_rp_get_bom_structure(BOMREP,tRevision, pszRevRule, pszEffDate,1, &pszXmlRep, &pArray, &pDocument);
            if (BNL_em_error_handler("BNL_rp_get_bom_structure", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

            BNL_rp_free_diff_struc(pArray->iDiffsCount, pArray->ptDiffs);
            free(pArray);
            free(pszXmlRep);
        }
        else
        {

         //recursive
         iRetCode = BNL_rp_web_bom(BOMREP,tRevision, pszRevRule, pszEffDate,1,0, &pszXml, &pDocument, iTopLine);
        }



         //if the document is NULL, happens on error then do not create a file

         if (pDocument!=NULL)
         {
                pszFullId=BNL_rp_get_full_id(tRevision);

                // Could cause error
                //pszsl=strchr(pszFullId,'/');
                if (pszsl != NULL) *pszsl='-';

                BNL_fm_replace_illegal_chars(pszFullId);

                if (pszPostFix!=NULL) BNL_fm_replace_illegal_chars(pszPostFix);


                if (pszPostFix==NULL)
                {
                pszBomFile=malloc(strlen(pszFullId) + strlen(gDocdownload_info.pszExportDir) + 8);
                //open file
                sprintf(pszBomFile, "%s\\%s.html",gDocdownload_info.pszExportDir, pszFullId);

                pszFileName=malloc(sizeof(char)*strlen(pszFullId)+8);
                sprintf(pszFileName, "%s.html", pszFullId);

                }
                else
                {
                    pszBomFile=malloc(strlen(pszFullId) +strlen(pszPostFix) + strlen(gDocdownload_info.pszExportDir) + 8);
                    //open file
                    sprintf(pszBomFile, "%s\\%s%s.html",gDocdownload_info.pszExportDir, pszFullId, pszPostFix);

                    pszFileName=malloc(sizeof(char)*strlen(pszFullId)+strlen(pszPostFix)+8);
                    sprintf(pszFileName, "%s%s.html", pszFullId, pszPostFix);

                }

                if (!BNL_fm_check_file_or_dir(pszBomFile))
                {

                  iRetCode = BNL_fm_open_file(&fpHtml, pszBomFile,NULL, "w+t",1);
                  if (iRetCode == 0)
                  {
                    BNL_fei_rep_parse_html(fpHtml, pDocument, BOMREP,0,NULL,NULL,NULL);
                    BNL_fm_close_file(fpHtml);
                    BNL_report_a_line(pszFullId,NULL, pszFileName, "");
                    free(pszFileName);

                   }
                   else
                   {
                      //should we give any message here to the user
                      BNL_report_a_line(pszFullId,NULL, pszFileName, "Y");
                      iRetCode = 0;
                   }
                }

                if (pszFullId!=NULL) MEM_free(pszFullId);
                if (pszXml!=NULL) free(pszXml);
                if (pszBomFile!=NULL) free(pszBomFile);
                //if (pszRevRule!=NULL) MEM_free(pszRevRule);

                BNL_xml_close_document(pDocument);
         }

         if (pszPostFix!=NULL) MEM_free(pszPostFix);

         return iRetCode;
}

int BNL_rp_web_eco_diffs
(
  tag_t tEco,
  char *pszRevRule,
  char **pszXMLdiff,
  t_XMLElement **pDocument
)
{
    int iRetCode    = 0;
    int iDocCount   = 0;
    int iBomCount   = 0;
    int iAttrCount  = 0;
    int i           = 0;
    int d           = 0;
    int p           = 0;

    char *pszViewType       = NULL;
    //char *pszRevRule        = NULL;
    char *pszLevel          = NULL;
    char *pszSort           = NULL;

    array_t *pDocDiffs;
    array_t *pBomDiffs;
    array_t *pAttrDiffs;


    t_XMLElement *pParent           = NULL;
    t_XMLElement *pDocdiffs         = NULL;
    t_XMLElement *pBomdiffs         = NULL;
    t_XMLElement *pAttrdiffs        = NULL;
    t_XMLElement *pComp             = NULL;
    t_XMLElement *pProd             = NULL;
    t_XMLElement *pPred             = NULL;
    t_XMLElement *pDiff             = NULL;
    t_XMLElement *pProps            = NULL;
    t_XMLElement *pEcoProps         = NULL;
    t_XMLElement *pEcoInfo          = NULL;

    diffinfo_t *pDiffs;
    diffinfo_t *pNode;

    clock_t start, finish;
    double  duration;

    logical lExported       = false;


    fprintf(rplog, " BNL_rp_web_eco_diffs executed.\n");

    start=clock();

    // JM|04-12-2008
    AOM_refresh(tEco, false);


    iRetCode = PREF_ask_char_value("BNL_rep_viewtype",0, &pszViewType);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    //iRetCode = PREF_ask_char_value("BNL_rep_revrule",0, &pszRevRule);
    //if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (!giDocDownload)
    {
      fprintf(rplog, "Revision rule: [%s]\n", pszRevRule);
    }

     iRetCode = PREF_ask_char_value("BNL_rep_bomlevel",0, &pszLevel);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;



    if (pszViewType==NULL)
    {
        pszViewType=MEM_alloc(sizeof(char)*33);
        strcpy(pszViewType,DEFAULT_VIEWTYPE);

    }

    ///if (pszRevRule==NULL)
    //{
//        pszRevRule=MEM_alloc(sizeof(char)*33);
  //      strcpy(pszRevRule,DEFAULT_REVRULE);
//    }

    if (pszLevel==NULL)
    {
        pszLevel=MEM_alloc(sizeof(char)*5);
        strcpy(pszLevel, "-1");
    }



    if (giDocDownload)
    {
        if (gDocdownload_info.pszRevRule!=NULL)
        {
          fprintf(rplog, "Using configured revision rule [%s]\n",gDocdownload_info.pszRevRule);
          iRetCode = BNL_rp_eco_diffs(tEco,atoi(pszLevel),gDocdownload_info.pszRevRule, pszViewType, &iDocCount, &pDocDiffs, &iBomCount, &pBomDiffs, &iAttrCount, &pAttrDiffs);
        }
        else
        {
            fprintf(rplog, "Using default revision rule [%s]\n", pszRevRule);
            iRetCode = BNL_rp_eco_diffs(tEco,atoi(pszLevel), pszRevRule, pszViewType, &iDocCount, &pDocDiffs, &iBomCount, &pBomDiffs, &iAttrCount, &pAttrDiffs);
        }
    }
    else
    {
        iRetCode = BNL_rp_eco_diffs(tEco,atoi(pszLevel), pszRevRule, pszViewType, &iDocCount, &pDocDiffs, &iBomCount, &pBomDiffs, &iAttrCount, &pAttrDiffs);
    }

    MEM_free(pszViewType);
    //MEM_free(pszRevRule);
    MEM_free(pszLevel);

    if (iRetCode!=0) return iRetCode;





    if (giDocDownload)
    {
        for (p= 0;p<iDocCount;p++)
        {
          BNL_pcb_export(pDocDiffs[p].tProduct, false, &lExported); // JM: These are the affected and solution Items

          if (giDocDownloadSkipExport && lExported)
          {
            fprintf(rplog, "PCB, skipping BNL_rp_export_docs.\n");
          }
          else
          {
            iRetCode = BNL_rp_export_docs(pDocDiffs[p],1);
          }
        }
    }




    /*create xml structure

    <document>
     <docdiffs>
       <comp>
        <product>Ah38BBal3IK9wB</product>
        <predecessor>Ah38BBal3IK9wB</predecessor>
        <diffs>
          <diff>
            <object>1</object>
            <change>1+</change>
            <quant>0</quant>
          </diff>
        </diffs>
      </comp>
     <docdiffs>

    </document>


  */
     pParent=BNL_xml_element_create(XML_EL_DOCUMENT,NULL);

     pEcoInfo=BNL_xml_element_create(XML_EL_ECO,NULL);

     BNL_rp_create_xml_props(tEco,XML_EL_ECO,0, &pEcoProps,NULL,NULL,NULL,NULL,NULL);

     BNL_xml_element_add_element(pEcoInfo, pEcoProps);

     BNL_xml_element_add_element(pParent, pEcoInfo);

     pDocdiffs=BNL_xml_element_create(XML_EL_DOCDIFFS,NULL);

     pBomdiffs=BNL_xml_element_create(XML_EL_BOMDIFFS,NULL);

     pAttrdiffs=BNL_xml_element_create(XML_EL_ATTRDIFFS,NULL);

     for (i= 0;i<iDocCount;i++)
     {

        pComp=BNL_xml_element_create(XML_EL_COMP,NULL);


        pProd=BNL_xml_element_create(XML_EL_PRODUCT,NULL);

        //BNL_rp_create_xml_props(pDocDiffs[i].tProduct, "prod_docsdiff",0, &pProps,NULL);
        BNL_rp_create_xml_props(pDocDiffs[i].tProduct, "prod_ecodiff",0, &pProps,NULL,NULL,NULL,NULL,NULL);

        BNL_xml_element_add_element(pProd, pProps);

        pPred=BNL_xml_element_create(XML_EL_PREDECESSOR,NULL);

        //BNL_rp_create_xml_props(pDocDiffs[i].tPredecessor, "pred_docsdiff",0, &pProps,NULL);
        BNL_rp_create_xml_props(pDocDiffs[i].tPredecessor, "pred_ecodiff",0, &pProps,NULL,NULL,NULL,NULL,NULL);

        BNL_xml_element_add_element(pPred, pProps);

        BNL_xml_element_add_element(pComp, pProd);
        BNL_xml_element_add_element(pComp, pPred);

        pDiffs= pDocDiffs[i].ptDiffs;

        /*SORT*/


         iRetCode = PREF_ask_char_value("BNL_sort_ecodocsdiff",0, &pszSort);
         if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

         if (pszSort!=NULL && pDocDiffs[i].iDiffsCount!=0)
         {
           fprintf(rplog, "Sorting eco document differences on property [%s]\n", pszSort);
           BNL_rp_sort_on_prop(pszSort, pDocDiffs[i].iDiffsCount, pDiffs);
           MEM_free(pszSort);

           pNode=pDiffs;

           while (pNode->pPrev!=NULL)
           {
               pNode=pNode->pPrev;

           }

           while(pNode!=NULL)
           {
                BNL_rp_create_xml_diff(*pNode, &pDiff);
                BNL_xml_element_add_element(pComp, pDiff);
                pNode=pNode->pNext;
           }


         }
         else
         {

           for (d= 0;d<pDocDiffs[i].iDiffsCount;d++)
           {
              BNL_rp_create_xml_diff(pDiffs[d], &pDiff);
              BNL_xml_element_add_element(pComp, pDiff);
           }
         }

         BNL_xml_element_add_element(pDocdiffs, pComp);
     }

     BNL_xml_element_add_element(pParent, pDocdiffs);


     for (i= 0;i<iBomCount;i++)
     {

        pComp=BNL_xml_element_create(XML_EL_COMP,NULL);

        pProd=BNL_xml_element_create(XML_EL_PRODUCT,NULL);

        BNL_rp_create_xml_props(pBomDiffs->tProduct, "prod_bomdiff",0, &pProps,NULL,NULL,NULL,NULL,NULL);

        BNL_xml_element_add_element(pProd, pProps);

        pPred=BNL_xml_element_create(XML_EL_PREDECESSOR,NULL);

        BNL_rp_create_xml_props(pBomDiffs->tPredecessor, "pred_bomdiff",0, &pProps,NULL,NULL,NULL,NULL,NULL);

        BNL_xml_element_add_element(pPred, pProps);



        BNL_xml_element_add_element(pComp, pProd);
        BNL_xml_element_add_element(pComp, pPred);

        pDiffs= pBomDiffs[i].ptDiffs;

        /*sort*/
         iRetCode = PREF_ask_char_value("BNL_sort_ecobomlines",0, &pszSort);
         if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

         if (pszSort!=NULL && pBomDiffs[i].iDiffsCount!=0)
         {
           fprintf(rplog, "Sorting eco bomlines differences on property [%s]\n", pszSort);
           BNL_rp_sort_on_prop(pszSort, pBomDiffs[i].iDiffsCount, pDiffs);
           MEM_free(pszSort);

           pNode=pDiffs;

           while (pNode->pPrev!=NULL)
           {
               pNode=pNode->pPrev;

           }

           while(pNode!=NULL)
           {
                BNL_rp_create_xml_diff(*pNode, &pDiff);
                BNL_xml_element_add_element(pComp, pDiff);
                pNode=pNode->pNext;
           }


         }
         else
         {


           for (d= 0;d<pBomDiffs[i].iDiffsCount;d++)
           {
             BNL_rp_create_xml_diff(pDiffs[d], &pDiff);
             BNL_xml_element_add_element(pComp, pDiff);
           }
         }

         BNL_xml_element_add_element(pBomdiffs, pComp);
     }

     BNL_xml_element_add_element(pParent, pBomdiffs);

     for (i= 0;i<iAttrCount;i++)
     {
        pComp = BNL_xml_element_create(XML_EL_COMP, NULL);

        pProd = BNL_xml_element_create(XML_EL_PRODUCT, NULL);

        BNL_rp_create_xml_props(pAttrDiffs->tProduct, "prod_attrdiff", 0, &pProps,NULL,NULL,NULL,NULL,NULL);

        BNL_xml_element_add_element(pProd, pProps);

        pPred = BNL_xml_element_create(XML_EL_PREDECESSOR, NULL);

        BNL_rp_create_xml_props(pAttrDiffs->tPredecessor, "pred_attrdiff", 0, &pProps,NULL,NULL,NULL,NULL,NULL);

        BNL_xml_element_add_element(pPred, pProps);

        BNL_xml_element_add_element(pComp, pProd);
        BNL_xml_element_add_element(pComp, pPred);

        pDiffs= pAttrDiffs[i].ptDiffs;

        for (d= 0;d<pAttrDiffs[i].iDiffsCount;d++)
        {
          BNL_rp_create_xml_diff(pDiffs[d], &pDiff);
          BNL_xml_element_add_element(pComp, pDiff);
        }
        BNL_xml_element_add_element(pAttrdiffs, pComp);
     }
     BNL_xml_element_add_element(pParent, pAttrdiffs);

     BNL_rp_xmlstuc_to_string(pParent, pszXMLdiff);

     fprintf(rplog, "Freeing memory using BNL_rp_free_array_struc in BNL_rp_web_eco_diffs\n");

     BNL_rp_free_array_struc(iDocCount, pDocDiffs);

     fprintf(rplog, "Freeing memory using BNL_rp_free_array_struc in BNL_rp_web_eco_diffs\n");
     BNL_rp_free_array_struc(iBomCount, pBomDiffs);

     fprintf(rplog, "Freeing memory using BNL_rp_free_array_struc in BNL_rp_web_eco_diffs\n");
     BNL_rp_free_array_struc(iAttrCount, pAttrDiffs);

    finish=clock();
    duration = (double)(finish - start) / CLOCKS_PER_SEC;
    fprintf(rplog, "ECO report creation took:[%2.1f]s\n",duration);


     *pDocument=pParent;

     return iRetCode;
}



int BNL_rp_free_diff_struc(int iCount, diffinfo_t *pDiff)
{
    int iRetCode = 0;
    int i= 0;



    for(i= 0;i<iCount;i++)
    {
       if (pDiff[i].pszQuant!=NULL) free(pDiff[i].pszQuant);
       if (pDiff[i].pszSeq!=NULL) free(pDiff[i].pszSeq);
       if (pDiff[i].pszRefDes!=NULL) free(pDiff[i].pszRefDes);
       if (pDiff[i].pXMLprops!=NULL) BNL_xml_element_free(pDiff[i].pXMLprops);
    }

    if (pDiff!=NULL) MEM_free(pDiff);

    return iRetCode;
}


int BNL_rp_free_array_struc(int iArrays, array_t *ptArray)
{
    int iRetCode    = 0;
    int i           = 0;


    for (i= 0;i<iArrays;i++)
    {

       BNL_rp_free_diff_struc(ptArray[i].iDiffsCount, ptArray[i].ptDiffs);

    }

    if (ptArray!=NULL) MEM_free(ptArray);



    return iRetCode;
}

void BNL_rp_store_configinfo(t_XMLElement *pDocument,char *pszRevRule,char *pszEffDate, int iLevel)
{
    t_XMLElement *pBomConfig        = NULL;
    t_XMLElement *pRevRule          = NULL;
    t_XMLElement *pEffdate          = NULL;
    t_XMLElement *pLevel            = NULL;
    char szLevel[33];

    szLevel[0]='\0';
    sprintf(szLevel, "%d", iLevel);

    pBomConfig=BNL_xml_element_create("bomconfig",NULL);

    pRevRule=BNL_xml_element_create(XML_EL_REVRULE, pszRevRule);
    pEffdate=BNL_xml_element_create(XML_EL_EFFDATE, pszEffDate);
    pLevel=BNL_xml_element_create(XML_EL_LEVEL,szLevel);

    BNL_xml_element_add_element(pBomConfig, pRevRule);
    BNL_xml_element_add_element(pBomConfig, pEffdate);
    BNL_xml_element_add_element(pBomConfig, pLevel);

    BNL_xml_element_add_element(pDocument, pBomConfig);

}

int BNL_EBOM_COMPARE(tag_t tLine1, tag_t tLine2, void *client_data)
{
  /* returns strcmp style -1/0/+1 according to whether tLine1 and tLine2 sort <, = or > */
  int iRetCode      = 0;
  int iResult       = 0;
  int iSeqId        = 0;
  int iNameId       = 0;

  char *pszSeq1     = NULL;
  char *pszSeq2     = NULL;
  char *pszName1    = NULL;
  char *pszName2    = NULL;
  char *pszVal1    = NULL;
  char *pszVal2    = NULL;

  iRetCode = BOM_line_look_up_attribute(bomAttr_occSeqNo, &iSeqId);
  if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BOM_line_ask_attribute_string (tLine1, iSeqId, &pszSeq1);
  if (BNL_em_error_handler("PS_list_owning_revs_of_bvr", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

  //fprintf(rplog, "DEBUG: Seq 1 [%s]\n", pszSeq1);

  iRetCode = BOM_line_ask_attribute_string (tLine2, iSeqId, &pszSeq2);
  if (BNL_em_error_handler("PS_list_owning_revs_of_bvr", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

  //fprintf(rplog, "DEBUG: Seq 2 [%s]\n", pszSeq2);

  if (pszSeq1 == NULL || pszSeq2 == NULL)
  {
    iResult = 0;
  }
  else
  {
    iResult = strcmp(pszSeq1, pszSeq2);
  }

  if (iResult == 0)
  {
    /* Sort on names */
    iRetCode = BOM_line_look_up_attribute(bomAttr_formattedIndentedTitle, &iNameId);
    if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    iRetCode = BOM_line_ask_attribute_string (tLine1, iNameId, &pszName1);
    if (BNL_em_error_handler("PS_list_owning_revs_of_bvr", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    //fprintf(rplog, "DEBUG: Name 1 [%s]\n", pszName1);

    iRetCode = BOM_line_ask_attribute_string (tLine2, iNameId, &pszName2);
    if (BNL_em_error_handler("PS_list_owning_revs_of_bvr", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    //fprintf(rplog, "DEBUG: Name 2 [%s]\n", pszName1);

    iResult = strcmp(pszName1, pszName2);
  }

  if (iResult == 0)
  {
    // When we return 1 at this time, the structure will be the same when running from the Web an command-line
    return 1;
  }

  if (pszSeq1 != NULL) MEM_free(pszSeq1);
  if (pszSeq2 != NULL) MEM_free(pszSeq2);
  if (pszName1 != NULL) MEM_free(pszName1);
  if (pszName2 != NULL) MEM_free(pszName2);

  return iResult;
}

int BNL_rp_get_bom_structure
(
  int iMode,
  tag_t tRev,
  char *pszRevRule,
  char *pszEffDate,
  int  iLevel,
  char **pszXMLrep,
  array_t **ppArray,
  t_XMLElement **pDocument
 )
{
    int iRetCode        = 0;
    int iCount          = 0;
    int iLines          = 0;


    int imonth          = 0;
    int iday            = 0;
    int iyear           = 0;
    int ihour           = 0;
    int iminute         = 0;
    int isecond         = 0;
    int iRevs     = 0;


    char szClass[33];
    char *pszViewType   = NULL;
    char *pszDateFormat = NULL;
    char *pszSort       = NULL;



    tag_t tBomWindow     =NULL_TAG;
    tag_t tTopLine       =NULL_TAG;
    tag_t tBom           =NULL_TAG;
    tag_t tPresetRule    =NULL_TAG;
    tag_t tCopyRule      =NULL_TAG;
    tag_t tDateEntry     =NULL_TAG;
    tag_t *ptRevs    = NULL;


    diffinfo_t *pLines   = NULL;
    array_t *pArray;

    date_t dStartEff;

    /*EN|03052004 the incomming tag could be also the bomview itself
    check this and get the revision and pickup the right bomview.
  */

    pArray=malloc(sizeof(array_t));
    pArray->iDiffsCount= 0;
    pArray->ptDiffs= NULL;
    pArray->tPredecessor=NULL_TAG;
    pArray->tProduct=NULL_TAG;

    *ppArray=pArray;

    pArray->tProduct=tRev;


  BNL_tb_determine_super_class(tRev,szClass);

  if (strcmp(szClass, BOMVIEWREVISION)== 0)
  {
    tBom=tRev;

    /*gives back an array but since tceng 3.4 it is allways one itemrev*/
    iRetCode = PS_list_owning_revs_of_bvr(tBom, &iRevs, &ptRevs);
    if (BNL_em_error_handler("PS_list_owning_revs_of_bvr", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    tRev=ptRevs[0];

    MEM_free(ptRevs);
  }
  else if (strcmp(szClass, ITEMREVISION)== 0)
  {

       iRetCode = PREF_ask_char_value("BNL_rep_viewtype",0, &pszViewType);
       if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

       if (pszViewType==NULL)
     {
          pszViewType=MEM_alloc(sizeof(char)*5);
          strcpy(pszViewType, "view");
     }

       iRetCode = BNL_rp_get_bomview(tRev, pszViewType, &tBom);
       if (BNL_em_error_handler("BNL_rep_get_bomview", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

       MEM_free(pszViewType);
  }
  else
  {
    BNL_rp_xml_error("The chosen object is not an ItemRevision, choose an ItemRevision\n", pszXMLrep, pDocument);
    return iRetCode;

  }

    if (tBom==NULL_TAG)
    {
        BNL_rp_xml_error("The chosen revision does not have a bomview revision", pszXMLrep, pDocument);
        return iRetCode;
    }

    iRetCode = CFM_find(pszRevRule, &tPresetRule);
    if (iRetCode == 710041)
    {
         BNL_rp_xml_error("The given revision rule does not exist", pszXMLrep, pDocument);
         return iRetCode;
    }

    if (BNL_em_error_handler("CFM_find", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;


    if (*pszEffDate!='\0')
    {

      fprintf(rplog, "Copy revision rule [%s] and set effective date to [%s]\n", pszRevRule, pszEffDate);

      iRetCode = PREF_ask_char_value("BNL_date_format",0, &pszDateFormat);
      if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

      if (pszDateFormat==NULL)
      {
          fprintf(rplog, "Missing preference BNL_date_format in preferences file.\n");
          return -1;
      }

      iRetCode = DATE_string_to_date(pszEffDate, pszDateFormat, &imonth, &iday, &iyear, &ihour, &iminute, &isecond);
      if (BNL_em_error_handler("DATE_string_to_date", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;


      dStartEff.month=imonth;
      dStartEff.day=iday;
      dStartEff.year=iyear;
      dStartEff.hour=ihour;
      dStartEff.minute=iminute;
      dStartEff.second=isecond;

      if (pszDateFormat!=NULL) MEM_free(pszDateFormat);

      /*copy the rule and add the date*/
      iRetCode = CFM_rule_copy (tPresetRule, "copied rule", &tCopyRule);
      if (BNL_em_error_handler("CFM_rule_copy", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      /*TODO:before add entry we should check if the entry already exists otherwhise we need to replace*/

      iRetCode = CFM_rule_add_entry (tCopyRule,CFM_entry_date, &tDateEntry);
      if (BNL_em_error_handler("CFM_rule_add_entry", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      iRetCode = CFM_rule_set_date(tCopyRule,dStartEff);
      if (BNL_em_error_handler("CFM_rule_set_date", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      tPresetRule=tCopyRule;


    }

    iRetCode = BOM_create_window(&tBomWindow);
    if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    if (iMode==EBOMREP)
    {
      iRetCode = BOM_set_window_pack_all(tBomWindow,false);
      if (BNL_em_error_handler("BOM_set_window_pack_all", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }
    else // JM| 03-09-2009: Default
    //if (iMode==EBOMPACKEDREP)
    {
      iRetCode = BOM_set_window_pack_all(tBomWindow,true);
      if (BNL_em_error_handler("BOM_set_window_pack_all", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }

    iRetCode = BOM_set_window_config_rule( tBomWindow, tPresetRule);
    if (BNL_em_error_handler("BNL_tb_bom_apply_config_rule", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

/*
    if (iMode==EBOMPACKEDREP || iMode==EBOMREP)
    {
      // Workaround for getting the same structure from Web as from command-line
      iRetCode = BOM_set_window_sort_compare_fn(tBomWindow, (void *)BNL_EBOM_COMPARE, NULL);
      if (BNL_em_error_handler("BOM_set_window_sort_compare_fn", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }
*/

    iRetCode = BOM_set_window_top_line(tBomWindow,NULL_TAG,tRev,tBom, &tTopLine);
    if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    /*set revision rule*/

    if (iMode==EBOMPACKEDREP)
    {
      iRetCode = BNL_rp_bom_get_item_revs("ebompackedrep",tTopLine,0, iLevel, &iLines, &pLines);
      if (BNL_em_error_handler("BNL_rp_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }
    else if (iMode==EBOMREP)
    {
      iRetCode = BNL_rp_bom_get_item_revs("ebomrep",tTopLine,0, iLevel, &iLines, &pLines);
      if (BNL_em_error_handler("BNL_rp_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }
    else if (iMode==BOMREPEXT)
    {
      iRetCode = BNL_rp_bom_get_item_revs("bomrep_ext",tTopLine,0, iLevel, &iLines, &pLines);
      if (BNL_em_error_handler("BNL_rp_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }
    else if (iMode==BOMREPMFG)
    {
      iRetCode = BNL_rp_bom_get_item_revs("bomrep_mfg",tTopLine,0, iLevel, &iLines, &pLines);
      if (BNL_em_error_handler("BNL_rp_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }
    else
    {
      iRetCode = BNL_rp_bom_get_item_revs("bomrep",tTopLine,0, iLevel, &iLines, &pLines);
      if (BNL_em_error_handler("BNL_rp_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }

    iRetCode = BOM_close_window(tBomWindow);
    if (BNL_em_error_handler("BOM_close_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    pArray->iDiffsCount=iLines;
    pArray->ptDiffs=pLines;
    pArray->tPredecessor=NULL_TAG;

    iRetCode = PREF_ask_char_value("BNL_sort_bomlines",0, &pszSort);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (pszSort!=NULL)
    {
     fprintf(rplog, "Sorting bomlines on property [%s]\n", pszSort);
     BNL_rp_sort_on_prop(pszSort, iLines, pLines);
     MEM_free(pszSort);
    }

    if (iMode==EBOMPACKEDREP)
    {
      BNL_rp_get_xml_struc("ebompackedrep", *pArray, pszXMLrep, pDocument);
    }
    else if (iMode==EBOMREP)
    {
      BNL_rp_get_xml_struc("ebomrep", *pArray, pszXMLrep, pDocument);
    }
    else if (iMode==BOMREPEXT)
    {
      BNL_rp_get_xml_struc("bomrep_ext", *pArray, pszXMLrep, pDocument);
    }
    else if (iMode==BOMREPMFG)
    {
      BNL_rp_get_xml_struc("bomrep_mfg", *pArray, pszXMLrep, pDocument);
    }
    else
    {
      BNL_rp_get_xml_struc("bomrep", *pArray, pszXMLrep, pDocument);
    }

    //for new report parser to html we add the info, it will not appear in the web structure.
    BNL_rp_store_configinfo(*pDocument, pszRevRule, pszEffDate, iLevel);

    return iRetCode;
}

int BNL_rp_web_bom
(
  int iMode,
  tag_t tRev,
  char *pszRevRule,
  char *pszEffDate,
  int  iLevel,
  int  iExportDocs,
  char **pszXMLrep,
  t_XMLElement **pDocument,
  int iTop
)
{
    int iRetCode        = 0;
    array_t *pArray;


    clock_t start, finish;
    double  duration;

    fprintf(rplog, "BNL_rp_web_bom executed.\n");

    start=clock();

    // JM|04-12-2008
    AOM_refresh(tRev, false);


    iRetCode = BNL_rp_get_bom_structure(iMode,tRev, pszRevRule, pszEffDate, iLevel, pszXMLrep, &pArray, pDocument);

    if (giDocDownload)
    {

        if (iExportDocs)
        {
          iRetCode = BNL_rp_export_docs(*pArray,0);
        }

        if (gDocdownload_info.onelevelbomreps)
        {
            int l= 0;

            //create onelevel bomrep of top level, iTop prevents endless loop.
            if (iTop)
            {
              //the zero is actually iTop because this function is recursive called by the create onelevel report.
              BNL_rp_create_onelevel_bomreport(tRev, pszRevRule, pszEffDate, iTop);

            }

            if (iLevel!=0)
            {
                  //now of a line bomrep of top level
                for (l= 0;l<pArray->iDiffsCount;l++)
                {
                    //create one level bomreps

                    BNL_rp_create_onelevel_bomreport(pArray->ptDiffs[l].tObject, pszRevRule, pszEffDate,0);
                }

            }
        }

    }


    fprintf(rplog, "Freeing memory using BNL_rp_free_diff_struc in BNL_rp_web_bom\n");
    BNL_rp_free_diff_struc(pArray->iDiffsCount, pArray->ptDiffs);
    free(pArray);

    finish=clock();
    duration = (double)(finish - start) / CLOCKS_PER_SEC;
    fprintf(rplog, "BOM report creation took:[%2.1f]s\n",duration);

    return iRetCode;
}

int BNL_rp_web_succ_pred
(
  tag_t tProduct,
  char **pszXMLrep,
  t_XMLElement **pDocument
)
{
    int iRetCode        = 0;
    int iSucc           = 0;
    int iPred           = 0;
    int i               = 0;

    diffinfo_t *pPred   = NULL;
    diffinfo_t *pSucc   = NULL;

    tag_t *ptSuccObjs   = NULL;
    tag_t *ptPredObjs   = NULL;

    t_XMLElement *pProps   = NULL;
    array_t pArray;

    char szClass[WSO_name_size_c+1];

    clock_t start, finish;
    double  duration;

    fprintf(rplog, "BNL_rp_web_succ_pred executed.\n");

    start=clock();

    iRetCode = BNL_tb_determine_super_class(tProduct, szClass);
    if (strcmp(szClass, "ItemRevision") != 0)
    {
      int iRevs       = 0;
      tag_t *ptRevs   = NULL;

      // Get the first created ItemRevision
      iRetCode = ITEM_list_all_revs(tProduct, &iRevs, &ptRevs);

      if (iRevs > 0)
      {
        tProduct = ptRevs[0];
        MEM_free(ptRevs);
        iRevs = 0;
      }
    }

    iRetCode = BNL_rp_get_succ_rel_objs(tProduct, &iSucc, &ptSuccObjs);

    iRetCode = BNL_rp_get_pred_rel_objs(tProduct, &iPred, &ptPredObjs);

    pPred=(diffinfo_t *) MEM_realloc(pPred, iPred * sizeof(diffinfo_t));

    for (i= 0; i<iPred; i++)
    {
      pPred[i].tObject=tProduct;
      pPred[i].pszQuant="";
      pPred[i].pszSeq="";
      pPred[i].pszRefDes="";

      BNL_rp_create_xml_props(ptPredObjs[i], "predecessor",0, &pProps,NULL,NULL,NULL,NULL,NULL);
      pPred[i].pXMLprops=pProps;
      pPred[i].iChange= 0;
      pPred[i].pNext= NULL;
      pPred[i].pPrev= NULL;
    }

    pSucc=(diffinfo_t *) MEM_realloc(pSucc, iSucc * sizeof(diffinfo_t));
    for (i= 0; i<iSucc; i++)
    {
      pSucc[i].tObject=tProduct;
      pSucc[i].pszQuant="";
      pSucc[i].pszSeq="";
      pSucc[i].pszRefDes="";

      BNL_rp_create_xml_props(ptSuccObjs[i], "successor",0, &pProps,NULL,NULL,NULL,NULL,NULL);
      pSucc[i].pXMLprops=pProps;
      pSucc[i].iChange= 0;
      pSucc[i].pNext= NULL;
      pSucc[i].pPrev= NULL;
    }

    pArray.tPredecessor=NULL_TAG;
    pArray.tProduct=tProduct;

    BNL_rp_get_succ_pred_xml_struc(pArray, pSucc, iSucc, pPred, iPred, pszXMLrep, pDocument);

    finish=clock();
    duration = (double)(finish - start) / CLOCKS_PER_SEC;
    fprintf(rplog, "BOM report creation took:[%2.1f]s\n",duration);

    return iRetCode;
}
/*Returns the successor objects*/
int BNL_rp_get_succ_rel_objs(tag_t tProduct, int *iObj,tag_t **ptObjs)
{
   int iRetCode      = 0;
   int iValues       = 0;
   int k             = 0;
   int iCount        = 0;

   char szName[254];

   char *pszRelation = NULL;

   tag_t tItem       = NULL_TAG;
   tag_t tProp       = NULL_TAG;
   tag_t tObj        = NULL_TAG;
   tag_t tRelType    = NULL_TAG;

   tag_t *ptValues   = NULL;
   tag_t *ptTempObjs = NULL;

   iRetCode = ITEM_ask_item_of_rev(tProduct, &tItem);
   if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

   iRetCode = PREF_ask_char_value("BNL_succ_relation", 0, &pszRelation);
   if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

   if (pszRelation == NULL)
   {
     pszRelation = MEM_alloc(sizeof(char)*33);
     strcpy(pszRelation, DEFAULT_SUCCRELATION);
   }

   iRetCode = GRM_find_relation_type(pszRelation, &tRelType);
   if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

   //iRetCode = GRM_list_primary_objects_only(tItem, tRelType, &iValues, &ptValues);
   iRetCode = GRM_list_secondary_objects_only(tItem, tRelType, &iValues, &ptValues);
   if (BNL_em_error_handler("GRM_list_primary_objects", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

   iRetCode = ITEM_ask_id(tItem, szName);

   fprintf(rplog, "Item with id [%s] has [%d] successor(s).\n", szName, iValues);

   while(iValues == 1)
   {
     iCount++;
     ptTempObjs=(tag_t *) MEM_realloc(ptTempObjs, iCount * sizeof(tag_t));
     ptTempObjs[iCount-1] = ptValues[0];

     tObj = ptValues[0];
     iValues = 0;
     if (ptValues !=NULL) MEM_free(ptValues);

     iRetCode = ITEM_ask_id(tObj, szName);

     fprintf(rplog, "Successor has id [%s].\n", szName);

     //iRetCode = GRM_list_primary_objects_only(tObj, tRelType, &iValues, &ptValues);
     iRetCode = GRM_list_secondary_objects_only(tObj, tRelType, &iValues, &ptValues);
     if (BNL_em_error_handler("GRM_list_primary_objects", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

     fprintf(rplog, "Item with id [%s] has [%d] successor(s).\n", szName, iValues);
   }

   if (iValues == 0 )
   {
     fprintf(rplog, "Warning, Item with id [%s] has no successor relation.\n", szName);
     //return iRetCode;
   }
   if (iValues > 1)
   {
     fprintf(rplog, "Warning, Item with id [%s] has more than 1 successor relation.\n", szName);
    // return iRetCode;
   }

   *iObj = iCount;
   *ptObjs = ptTempObjs;

   if (pszRelation != NULL) MEM_free(pszRelation);

   return iRetCode;
}
/*Returns the predecessor objects*/
int BNL_rp_get_pred_rel_objs(tag_t tProduct, int *iObj,tag_t **ptObjs)
{
   int iRetCode      = 0;
   int iValues       = 0;
   int i             = 0;
   int iCount        = 0;
   int iRevs         = 0;

   char szName[254];

   char *pszRelation = NULL;

   tag_t tObj        = NULL_TAG;
   tag_t tItem       = NULL_TAG;
   tag_t tProp       = NULL_TAG;
   tag_t tRelType    = NULL_TAG;

   tag_t *ptRevs     = NULL;
   tag_t *ptValues   = NULL;
   tag_t *ptTempObjs = NULL;

   iRetCode = ITEM_ask_item_of_rev(tProduct, &tItem);
   if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

   // Get the first created ItemRevision
   iRetCode = ITEM_list_all_revs(tItem, &iRevs, &ptRevs);
   if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

   tObj = ptRevs[0];

   if (iRevs > 0)
   {
     iRevs = 0;
     MEM_free(ptRevs);
   }

   iRetCode = PREF_ask_char_value("BNL_pred_relation", 0, &pszRelation);
   if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

   if (pszRelation == NULL)
   {
     pszRelation = MEM_alloc(sizeof(char)*33);
     strcpy(pszRelation, DEFAULT_PREDRELATION);
   }

   iRetCode = GRM_find_relation_type(pszRelation, &tRelType);
   if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

   iRetCode = GRM_list_secondary_objects_only(tObj, tRelType, &iValues, &ptValues);
   if (BNL_em_error_handler("GRM_list_secondary_objects_only", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

   iRetCode = ITEM_ask_id(tItem, szName);

   fprintf(rplog, "Item with id [%s] has [%d] predecessor(s).\n", szName, iValues);

   while(iValues == 1)
   {
     iRetCode = ITEM_ask_item_of_rev(ptValues[0], &tItem);
     if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

     // Get the first created ItemRevision
     iRetCode = ITEM_list_all_revs(tItem, &iRevs, &ptRevs);
     if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

     tObj = ptRevs[0];

     if (iRevs > 0)
     {
       iRevs = 0;
       MEM_free(ptRevs);
     }

     iValues = 0;
     if (ptValues != NULL) MEM_free(ptValues);


     iCount++;
     ptTempObjs=(tag_t *) MEM_realloc(ptTempObjs, iCount * sizeof(tag_t));
     //ptTempObjs[iCount-1] = ptValues[0];
     ptTempObjs[iCount-1] = tItem;

     iRetCode = ITEM_ask_id(tItem, szName);

     fprintf(rplog, "Predecessor has id [%s].\n", szName);

     iRetCode = GRM_list_secondary_objects_only(tObj, tRelType, &iValues, & ptValues);
     if (BNL_em_error_handler("GRM_list_secondary_objects_only", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

     fprintf(rplog, "Item with id [%s] has [%d] predecessor(s).\n", szName, iValues);
   }

   if (iValues == 0 )
   {
     fprintf(rplog, "Warning, Item with id [%s] has no predecessor relation.\n", szName);
     //return iRetCode;
   }
   if (iValues > 1)
   {
     fprintf(rplog, "Warning, Item with id [%s] has more than 1 predecessor relation.\n", szName);
     //return iRetCode;
   }

   *iObj = iCount;
   *ptObjs = ptTempObjs;

   if (pszRelation > 0) MEM_free(pszRelation);

   return iRetCode;
}

int BNL_get_successors(tag_t tProduct, int *iSuccessors,tag_t **ptSuccessors)
{
    int iRetCode                = 0;
    int iPrims                  = 0;
    int s                       = 0;

    char *pszPredRelation       = NULL;

    tag_t tRelType              =NULL_TAG;
    tag_t *ptSuccs              = NULL;

    GRM_relation_t *pPrims      = NULL;


    iRetCode = PREF_ask_char_value("BNL_pred_relation",0, &pszPredRelation);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (pszPredRelation==NULL)
    {
        pszPredRelation=MEM_alloc(sizeof(char)*33);
        strcpy(pszPredRelation,DEFAULT_PREDRELATION);

    }

    iRetCode = GRM_find_relation_type(pszPredRelation, &tRelType);
    if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    iRetCode = GRM_list_primary_objects(tProduct,tRelType, &iPrims, &pPrims);
    if (BNL_em_error_handler("GRM_list_primary_objects", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    ptSuccs=MEM_alloc(sizeof(tag_t)*iPrims);

    for (s= 0;s<iPrims;s++)
    {

        ptSuccs[s]=pPrims[s].primary;

    }

    *iSuccessors=iPrims;
    *ptSuccessors=ptSuccs;

    if (iPrims > 0) MEM_free(pPrims);
    if (pszPredRelation != NULL) MEM_free(pszPredRelation);

    return iRetCode;
}

int BNL_rp_determine_latest_rev(tag_t tProduct,tag_t *tLatestRev)
{
    int iRetCode    = 0;
    int i           = 0;
    int iRevs       = 0;
    int iAllReleased=1;

    tag_t tItem     =NULL_TAG;
    tag_t *ptRevs   = NULL;

    char *pszProduct= NULL;
    char *pszLatest= NULL;



    iRetCode = ITEM_ask_item_of_rev(tProduct, &tItem);
    if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    AOM_refresh(tItem, false);

    //the returned array is in sequence of creation date, latest revision on creation date is the last in array (following documentation).
    iRetCode = ITEM_list_all_revs  (tItem, &iRevs, &ptRevs);
    if (BNL_em_error_handler("ITEM_list_all_revs", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;


    //check if all released
    for (i= 0;i<iRevs;i++)
    {
        if (!BNL_rp_is_released(ptRevs[i])) iAllReleased= 0;

    }

    if (iAllReleased)
    {
        date_t dHighestDate;
        date_t dReleased;


        for (i= 0;i<iRevs;i++)
        {
            iRetCode = BNL_tb_get_instance_field_datevalue(ptRevs[i], "date_released", &dReleased);
            if (BNL_em_error_handler("BNL_tb_get_instance_field_datevalue", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

            if (i== 0)
            {
                dHighestDate=dReleased;
                *tLatestRev=ptRevs[i];
            }
            /*
              dateOne == dateTwo   = 0
              dateOne > dateTwo    = 1
              dateOne < dateTwo    =-1
              */

            if (BNL_compare_dates(dHighestDate, dReleased)==-1)
            {

                 dHighestDate=dReleased;
                 *tLatestRev=ptRevs[i];
            }
        }
    }
    else
    {
        /*
        date_t dCreate;
        date_t dHighestDate;

        for (i= 0;i<iRevs;i++)
        {
            iRetCode = BNL_tb_get_instance_field_datevalue(ptRevs[i], "creation_date", &dCreate);

            if (i== 0)
            {
                dHighestDate=dCreate;
                *tLatestRev=ptRevs[i];
            }

            if (BNL_compare_dates(dHighestDate, dCreate)==-1)
            {
                dHighestDate=dCreate;
                *tLatestRev=ptRevs[i];
            }

        }*/

        *tLatestRev=ptRevs[iRevs-1];
    }

    if (iRevs > 0) MEM_free(ptRevs);

    pszProduct=BNL_rp_get_full_id(tProduct);
    pszLatest=BNL_rp_get_full_id(*tLatestRev);
    fprintf(rplog, "Determined [%s] as latest rev of [%s]\n", pszLatest, pszProduct);
    if (pszProduct != NULL) MEM_free(pszProduct);
    if (pszLatest != NULL) MEM_free(pszLatest);


    return iRetCode;

}

int BNL_id_is_higher
(
   char *pszPrev,
   char *pszNew
)
{
   int iRetCode     = 0;
   int iLenp        = 0;
   int iLenn        = 0;
   int iComp        = 0;
   int i            = 0;
   int iAlpha       = 0;
   char *pszNode;


   /*the higher subid consists of letters*/
   iLenp=(int)strlen(pszPrev);
   iLenn=(int)strlen(pszNew);

   pszNode=pszNew;
   for (i= 0;i<iLenn;i++)
   {
       if (!isdigit(*pszNode)) iAlpha=1;
       pszNode++;

   }

   if (iAlpha)
   {

   if (iLenp < iLenn) iComp=iLenp;
   if (iLenp > iLenn) iComp=iLenn;
   if (iLenp == iLenn) iComp=iLenn;

    for(i= 0;i<iComp;i++)
    {

        if (*pszPrev < *pszNew) iRetCode =  -1;
        if (*pszPrev == *pszNew) iRetCode =  0;
        if (*pszPrev > *pszNew) iRetCode =  1;

        if (iRetCode!=0) break;

        pszNew++;
        pszPrev++;
      }
   }
   else
   {
    if (iLenp < iLenn) iRetCode =  -1;
    if (iLenp == iLenn) iRetCode =  0;
    if (iLenp > iLenn) iRetCode =  1;

    if (iRetCode == 0)
    {
        for(i= 0;i<iLenp;i++)
        {

            if (*pszPrev < *pszNew) iRetCode =  -1;
            if (*pszPrev == *pszNew) iRetCode =  0;
            if (*pszPrev > *pszNew) iRetCode =  1;

            if (iRetCode!=0) break;

            pszNew++;
            pszPrev++;
        }
     }
   }



   return iRetCode;

}/*end of int BNL_id_is_higher*/

/*
returns NULL_TAG in case the status of the end product is obsolete.
*/
int BNL_rp_get_latest_productline_revision(tag_t tProduct, tag_t *tLatestRev)
{
    int iRetCode                = 0;
    int iSuccessors             = 0;

    tag_t *ptSuccessors         = NULL;

    char *pszTemp               = NULL;

    /*
    int i                       = 0;
    int iPrims                  = 0;
    int p                       = 0;
    tag_t tRelType              = 0;
    GRM_relation_t *pPrims      = NULL;
    char *pszPredRelation       = NULL;
    char *pszFullId             = NULL;
    char szStatus[33];
    tag_t tItem                 =NULL_TAG;
    */



    pszTemp = BNL_rp_get_full_id(tProduct);
    fprintf(rplog, "INFO: Getting latest revision of [%s]\n", pszTemp);
    if (pszTemp != NULL)
    {
      MEM_free(pszTemp);
      pszTemp = NULL;
    }



    iRetCode = BNL_get_successors(tProduct, &iSuccessors, &ptSuccessors);
    if (BNL_em_error_handler("BNL_get_successors", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    //fprintf(rplog, "DEBUG: tProduct [%d] iSuccessors [%d] *tLatestRev [%d]\n", tProduct, iSuccessors, *tLatestRev);

    if (iSuccessors== 0)
    {
        tag_t tPredecessor      =NULL_TAG;
        tag_t tLatest           =NULL_TAG;

        if (BNL_rp_is_obsolete(tProduct))
        {
             *tLatestRev=NULL_TAG;
            return iRetCode;
        }

        iRetCode = BNL_rp_get_predecessor(tProduct, &tPredecessor);
        if (BNL_em_error_handler("BNL_rp_get_predecessor", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

        if (tPredecessor==NULL_TAG)
        {
            tag_t tItem     =NULL_TAG;
            int iRevs       = 0;
            tag_t *ptRevs   = NULL;

            //get revision count
            iRetCode = ITEM_ask_item_of_rev(tProduct, &tItem);
            if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

            AOM_refresh(tItem, false);

            iRetCode = ITEM_list_all_revs  (tItem, &iRevs, &ptRevs);
            if (BNL_em_error_handler("ITEM_list_all_revs", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

            if (ptRevs!=NULL) MEM_free(ptRevs);

            if (iRevs==1)
            {
                *tLatestRev=tProduct;
                return iRetCode;

            }
            else
            {
                *tLatestRev=NULL_TAG;
                return iRetCode;

            }


        }

        iRetCode = BNL_rp_determine_latest_rev(tProduct,tLatestRev);
        if (BNL_em_error_handler("BNL_rp_determine_latest_rev", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

        if (tProduct!=*tLatestRev) BNL_rp_get_latest_productline_revision(*tLatestRev,tLatestRev);

        return iRetCode;


    }

    if (iSuccessors==1)
    {
        if (BNL_rp_is_obsolete(ptSuccessors[0]))
        {
            MEM_free(ptSuccessors);
            *tLatestRev=NULL_TAG;
            return iRetCode;
        }

        //recursive call
        iRetCode = BNL_rp_get_latest_productline_revision(ptSuccessors[0],tLatestRev);
        if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

        MEM_free(ptSuccessors);

        return iRetCode;

    }

    if (iSuccessors==2)
    {
        int iNextSuc;
        tag_t *ptNextSuc;

        iRetCode = BNL_get_successors(ptSuccessors[0], &iNextSuc, &ptNextSuc);
        if (BNL_em_error_handler("BNL_get_successors", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

        if (iNextSuc== 0)
        {

           iRetCode = BNL_get_successors(ptSuccessors[1], &iNextSuc, &ptNextSuc);
           if (BNL_em_error_handler("BNL_get_successors", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

           if (iNextSuc>0)
           {
               if (BNL_rp_is_obsolete(ptSuccessors[1]))
               {
                    MEM_free(ptNextSuc);
                    MEM_free(ptSuccessors);
                    *tLatestRev=NULL_TAG;
                    return iRetCode;
               }

               //recursive call
               iRetCode = BNL_rp_get_latest_productline_revision(ptSuccessors[1],tLatestRev);
               if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

               MEM_free(ptSuccessors);
               return iRetCode;


           }
        }

        if (iNextSuc>0)
        {
             if (BNL_rp_is_obsolete(ptSuccessors[0]))
             {
                 MEM_free(ptNextSuc);
                 MEM_free(ptSuccessors);
                 *tLatestRev=NULL_TAG;
                 return iRetCode;
             }

             //recursive call
             iRetCode = BNL_rp_get_latest_productline_revision(ptSuccessors[0],tLatestRev);
             if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

             MEM_free(ptSuccessors);
             return iRetCode;

        }
        else
        {

            if (BNL_rp_is_obsolete(ptSuccessors[0]) && BNL_rp_is_obsolete(ptSuccessors[1]) )
            {
                MEM_free(ptSuccessors);
                *tLatestRev=NULL_TAG;
                return iRetCode;

            }

            if (!BNL_rp_is_obsolete(ptSuccessors[0]) && !BNL_rp_is_obsolete(ptSuccessors[1]))
            {
                tag_t tItemSucOne   =NULL_TAG;
                tag_t tItemSucTwo   =NULL_TAG;
                tag_t tItem         =NULL_TAG;
                tag_t tLatest       =NULL_TAG;


               iRetCode = ITEM_ask_item_of_rev(ptSuccessors[0], &tItemSucOne);
               if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

               AOM_refresh(tItemSucOne, false);

               iRetCode = ITEM_ask_item_of_rev(ptSuccessors[1], &tItemSucTwo);
               if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

               AOM_refresh(tItemSucTwo, false);

               iRetCode = ITEM_ask_item_of_rev(tProduct, &tItem);
               if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

               AOM_refresh(tItem, false);

               if (tItemSucTwo==tItemSucOne)
               {

                   if (tItem==tItemSucOne)
                   {
                        iRetCode = BNL_rp_determine_latest_rev(tProduct, &tLatest);
                        if (BNL_em_error_handler("BNL_rp_determine_latest_rev", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
                   }
                   else
                   {
                       iRetCode = BNL_rp_determine_latest_rev(ptSuccessors[0], &tLatest);
                       if (BNL_em_error_handler("BNL_rp_determine_latest_rev", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

                   }

                   if (tLatest==NULL_TAG)
                   {
                       MEM_free(ptSuccessors);
                       *tLatestRev=NULL_TAG;
                       return iRetCode;
                   }
                   else
                   {
                       //recursive call
                       iRetCode = BNL_rp_get_latest_productline_revision(tLatest,tLatestRev);
                       if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

                       MEM_free(ptSuccessors);
                       return iRetCode;

                   }


               }
               else
               {

                   if ((tItem!=tItemSucTwo) && (tItem!=tItemSucOne))
                   {
                       char szSucOneId[33];
                       char szSucTwoId[33];
                       tag_t tSuccessor=NULL_TAG;

                       iRetCode = ITEM_ask_id(tItemSucOne,szSucOneId);
                       iRetCode = ITEM_ask_id(tItemSucTwo,szSucTwoId);


                       //take highest id
                       //                             <
                       if (BNL_id_is_higher(szSucOneId,szSucTwoId)==-1)
                       {
                           tSuccessor=ptSuccessors[1];

                       }
                       else
                       {
                           tSuccessor=ptSuccessors[0];

                       }


                       //recursive call
                      iRetCode = BNL_rp_get_latest_productline_revision(tSuccessor,tLatestRev);
                      if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

                      MEM_free(ptSuccessors);
                      return iRetCode;

                   }

                   if (tItem!=tItemSucOne)
                   {
                      //recursive call
                      iRetCode = BNL_rp_get_latest_productline_revision(ptSuccessors[0],tLatestRev);
                      if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

                      MEM_free(ptSuccessors);
                      return iRetCode;
                   }

                   if (tItem!=tItemSucTwo)
                   {
                       //recursive call
                       iRetCode = BNL_rp_get_latest_productline_revision(ptSuccessors[1],tLatestRev);
                       if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

                       MEM_free(ptSuccessors);
                       return iRetCode;

                   }
               }


            }

            if (!BNL_rp_is_obsolete(ptSuccessors[0]))
            {
                 //recursive call
                 iRetCode = BNL_rp_get_latest_productline_revision(ptSuccessors[0],tLatestRev);
                 if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

                 MEM_free(ptSuccessors);
                 return iRetCode;
            }

            if (!BNL_rp_is_obsolete(ptSuccessors[1]))
            {
                 //recursive call
                 iRetCode = BNL_rp_get_latest_productline_revision(ptSuccessors[0],tLatestRev);
                 if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

                 MEM_free(ptSuccessors);
                 return iRetCode;
            }




        }




    }

    if (iSuccessors>2)
    {
        MEM_free(ptSuccessors);

        //not supported
        *tLatestRev=NULL_TAG;
        return iRetCode;
    }

    return iRetCode;
}


int BNL_rp_is_obsolete(tag_t tObject)
{
    int iRetCode        = 0;
    char szStatus[33];


    iRetCode = BNL_tb_ask_object_status(tObject,szStatus);
    if (BNL_em_error_handler("BNL_tb_ask_object_status", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;


    if (strcmp(szStatus, BNL_OBSOLETE_STATUS)== 0) return 1;

    return iRetCode;
}


int BNL_rp_is_released(tag_t tObject)
{
    int iRetCode        = 0;
    int iFail           = 0;
    char szStatus[33];

    if (tObject!=NULL_TAG)
    {
      iFail=BNL_tb_ask_object_status(tObject,szStatus);
      if (BNL_em_error_handler("BNL_tb_ask_object_status", BNL_module, EMH_severity_error, iFail, false)) return iRetCode;

      if (strcmp(szStatus, "-")== 0)
      {
        iRetCode = 0;
        return iRetCode;
      }
      else
      {
        iRetCode = 1;
        return iRetCode;
      }
    }
    else
    {
        return 0;
    }

    return iRetCode;
}


int BNL_rp_add_whereused_list(logical lTopLevel, int iLevel, tag_t tProduct,wu_list_t **pWuList)
{
    int iRetCode            = 0;

    int i= 0;

    wu_list_t *pList        = NULL;
    wu_list_t *pNode        = NULL;
    wu_list_t *pNew         = NULL;

    tag_t tItem             =NULL_TAG;
    tag_t tLatestPrdRev     = NULL_TAG;

    char szItemId[33];

    //fprintf(rplog, "DEBUG:Start BNL_rp_add_whereused_list]\n");


    if (tProduct==NULL_TAG) return iRetCode;

    iRetCode = ITEM_ask_item_of_rev(tProduct, &tItem);
    if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (lTopLevel)
    {
      tLatestPrdRev = tProduct;
      fprintf(rplog, "DEBUG: Top level\n");
    }
    else
    {
      iRetCode = BNL_rp_get_latest_productline_revision(tProduct, &tLatestPrdRev);
      if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
    }

    AOM_refresh(tItem, false);

    iRetCode = ITEM_ask_id(tItem,szItemId);
    if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;;

    fprintf(rplog, "DEBUG: ITEM_ID [%s]\n", szItemId);

    pList=*pWuList;

    if (pList==NULL)
    {
        pList=malloc(sizeof(wu_list_t));
        pList->pNext= NULL;
        pList->pPrev= NULL;
        pList->tRevision=tProduct;
        pList->tLatestRevision = tLatestPrdRev;
        pList->iLevel=iLevel;
        pList->iTopLevel= 0;
        strcpy(pList->szItemId,szItemId);

        *pWuList=pList;



        return iRetCode;
    }
    pNode=pList;
    while (pNode)
    {
      if (pNode->tRevision==tProduct && pNode->iLevel==iLevel)
      {
        return iRetCode;
      }

      pNode=pNode->pNext;
    }


    pNode=pList;

    while (pNode)
    {
      // <
      if (BNL_id_is_higher(szItemId, pNode->szItemId)==-1)
      {
          pNew=malloc(sizeof(wu_list_t));
          strcpy(pNew->szItemId,szItemId);
          pNew->tRevision=tProduct;
          pNew->tLatestRevision = tLatestPrdRev;
          pNew->iLevel=iLevel;
          pNew->iTopLevel= 0;

          if (pNode->pPrev==NULL)
          {
              pNew->pNext=pNode;
              pNew->pPrev= NULL;
              pNode->pPrev=pNew;
              *pWuList=pNew;

              return iRetCode;
          }
          else
          {
              pNew->pNext=pNode;
              pNew->pPrev=pNode->pPrev;
              pNode->pPrev=pNew;
              pNew->pPrev->pNext=pNew;
          }


          return iRetCode;
      }

      // ==
      if (BNL_id_is_higher(szItemId, pNode->szItemId)== 0 && pNode->pNext!=NULL)
      {
          pNew=malloc(sizeof(wu_list_t));
          strcpy(pNew->szItemId,szItemId);
          pNew->tRevision=tProduct;
          pNew->tLatestRevision = tLatestPrdRev;
          pNew->iLevel=iLevel;
          pNew->iTopLevel= 0;

          pNew->pNext=pNode->pNext;
          pNode->pNext->pPrev=pNew;
          pNode->pNext=pNew;
          pNew->pPrev=pNode;

          return iRetCode;
      }

      if (pNode->pNext==NULL)
      {
        pNew=malloc(sizeof(wu_list_t));
        strcpy(pNew->szItemId,szItemId);
        pNew->tRevision=tProduct;
        pNew->tLatestRevision = tLatestPrdRev;
        pNew->iLevel=iLevel;
        pNew->iTopLevel= 0;

        pNode->pNext=pNew;
        pNew->pNext= NULL;
        pNew->pPrev=pNode;

        return iRetCode;

      }

      pNode=pNode->pNext;
    }


    return iRetCode;

}


int _BNL_rp_add_whereused_list(int iLevel, tag_t tProduct,wu_list_t **pWuList)
{
    int iRetCode            = 0;

    int i= 0;

    wu_list_t *pList        = NULL;
    wu_list_t *pNode        = NULL;
    wu_list_t *pNew         = NULL;

    tag_t tItem             =NULL_TAG;

    char szItemId[33];

    //fprintf(rplog, "DEBUG:Start BNL_rp_add_whereused_list\n");

    if (tProduct==NULL_TAG) return iRetCode;

    iRetCode = ITEM_ask_item_of_rev(tProduct, &tItem);
    if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    AOM_refresh(tItem, false);

    iRetCode = ITEM_ask_id(tItem,szItemId);
    if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;;

    pList=*pWuList;

    // Create list
    if (pList==NULL)
    {
        pList=malloc(sizeof(wu_list_t));
        pList->pNext= NULL;
        pList->pPrev= NULL;
        pList->iLevel=iLevel;
        pNew->iTopLevel= 0;
        pList->tRevision=tProduct;
        strcpy(pList->szItemId,szItemId);

        *pWuList=pList;



        return iRetCode;
    }

    pNode=pList;

    // Check if item already in list
    while (pNode)
    {

        if (pNode->tRevision==tProduct)
        {
            return iRetCode;
        }

        pNode=pNode->pNext;
    }


    pNode=pList;


    // Add item to list, first sort on level and then sort on itemid
    while (pNode)
    {
      //fprintf(rplog, "DEBUG: iLevel [%d], pNode->iLevel [%d]\n", iLevel, pNode->iLevel);
      // <
      if (iLevel == pNode->iLevel && BNL_id_is_higher(szItemId, pNode->szItemId)==-1)
      {
        pNew=malloc(sizeof(wu_list_t));
        strcpy(pNew->szItemId,szItemId);
        pNew->iLevel=iLevel;
        pNew->tRevision=tProduct;

        if (pNode->pPrev==NULL)
        {
          pNew->pNext=pNode;
          pNew->pPrev= NULL;
          pNode->pPrev=pNew;
          *pWuList=pNew;

          return iRetCode;
        }
        else
        {
          pNew->pNext=pNode;
          pNew->pPrev=pNode->pPrev;
          pNode->pPrev=pNew;
          pNew->pPrev->pNext=pNew;
        }
        return iRetCode;
      }

      // ==
      if (iLevel == pNode->iLevel &&
        (BNL_id_is_higher(szItemId, pNode->szItemId)== 0 || BNL_id_is_higher(szItemId, pNode->szItemId)==1)
        && pNode->pNext!=NULL)
      {
        pNew=malloc(sizeof(wu_list_t));
        strcpy(pNew->szItemId,szItemId);
        pNew->iLevel=iLevel;
        pNew->iTopLevel= 0;
        pNew->tRevision=tProduct;

        pNew->pNext=pNode->pNext;
        pNode->pNext->pPrev=pNew;
        pNode->pNext=pNew;
        pNew->pPrev=pNode;

        return iRetCode;
      }

      if (pNode->pNext==NULL)
      {
        pNew=malloc(sizeof(wu_list_t));
        strcpy(pNew->szItemId,szItemId);
        pNew->iLevel=iLevel;
        pNew->iTopLevel= 0;
        pNew->tRevision=tProduct;

        pNode->pNext=pNew;
        pNew->pNext= NULL;
        pNew->pPrev=pNode;

        return iRetCode;
      }
      //fprintf(rplog, "******* DEBUG: iLevel [%d], pNode->iLevel [%d]\n", iLevel, pNode->iLevel);
      pNode=pNode->pNext;
    }


    return iRetCode;

}

time_t BNL_rp_date_to_time(date_t dDate)
{
  struct tm tmTime;

  tmTime.tm_year = dDate.year - 1900;
  tmTime.tm_mon  = dDate.month;
  tmTime.tm_mday = dDate.day;
  tmTime.tm_hour = dDate.hour;
  tmTime.tm_min  = dDate.minute;
  tmTime.tm_sec  = dDate.second;

  return mktime(&tmTime);
}

int BNL_rp_get_latest_eco(int iEcos, tag_t *ptEcos, tag_t *ptEcoLatest)
{
  int iRetCode        = 0;
  int i               = 0;
  tag_t tLatest       = NULL_TAG;

  for (i = 0; i < iEcos; i++)
  {
    date_t dFirst;
    date_t dSecond;

    if (tLatest == NULL_TAG)
      tLatest = ptEcos[i];

    //fprintf(rplog, "DEBUG: i[%d]\n", ptEcos[i]);

    /* Get the creation dates of each object */
    iRetCode = AOM_ask_creation_date(tLatest, &dFirst);
    if (BNL_em_error_handler("AOM_ask_creation_date (1)", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    //fprintf(rplog, "INFO: Creation date first object: %04d-%02d-%02d %02d:%02d.\n", dFirst.year, dFirst.month, dFirst.day, dFirst.hour, dFirst.minute);


    if (i > 0 && i + 1 < iEcos)
    {
      //fprintf(rplog, "DEBUG: i+1[%d]\n", ptEcos[i+1]);

      iRetCode = AOM_ask_creation_date(ptEcos[i+1], &dSecond);
      if (BNL_em_error_handler("AOM_ask_creation_date (2)", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

      //fprintf(rplog, "INFO: Creation date second object: %04d-%02d-%02d %02d:%02d.\n", dSecond.year, dSecond.month, dSecond.day, dSecond.hour, dSecond.minute);

      if (BNL_rp_date_to_time(dFirst) >= BNL_rp_date_to_time(dSecond))
      {
        //fprintf(rplog, "INFO: First object will be used.\n");
        tLatest = ptEcos[i];
      }
      else
      {
        //fprintf(rplog, "INFO: Second object will be used.\n");
        tLatest = ptEcos[i + 1];
      }
    }
  }

  *ptEcoLatest = tLatest;

  return iRetCode;
}


int BNL_rp_get_eco_in_process(int iEcos, tag_t *ptEcos, tag_t *ptEcoInProcess)
{
  int iRetCode        = 0;
  int i               = 0;
  tag_t tEco          = NULL_TAG;


  for (i = 0; i < (iEcos) && tEco == NULL_TAG; i++)
  {
    char *pszState     = NULL;

    iRetCode = BNL_rp_get_process_state(ptEcos[i], &pszState);
    if (BNL_em_error_handler("BNL_rp_get_process_state", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (pszState != NULL)
    {
      //fprintf(rplog , "DEBUG: Eco [%d] state [%s]\n", ptEcos[i], pszState);
      tEco = ptEcos[i];
    }
    else
    {
      //fprintf(rplog , "DEBUG: Eco [%d] state [-]\n", ptEcos[i]);
    }

    if (pszState != NULL) free(pszState);
  }

  *ptEcoInProcess = tEco;

  return iRetCode;
}


int BNL_rp_get_eco_matching_status(char *pszStatus, int iEcos, tag_t *ptEcos, tag_t *ptEcoStatus)
{
  int iRetCode        = 0;
  int i               = 0;
  tag_t tEco          = NULL_TAG;


  for (i = 0; i < (iEcos) && tEco == NULL_TAG; i++)
  {
    char szStatusEco[33];

    iRetCode = BNL_tb_ask_object_status(ptEcos[i], szStatusEco);
    if (BNL_em_error_handler("BNL_tb_ask_object_status", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    //fprintf(rplog , "DEBUG: Status [%s] status eco [%s]\n", pszStatus, szStatusEco);

    if (strcmp(pszStatus, szStatusEco) == 0)
    {
      tEco = ptEcos[i];
    }
  }

  *ptEcoStatus = tEco;

  return iRetCode;
}


int BNL_rp_get_related_eco(tag_t tObject, int *iEcos,tag_t **ptEcos)
{
    int iRetCode                    = 0;
    int i                           = 0;
    int j                           = 0;
    int iSolEcs                     = 0;
    int iAffEcs                     = 0;

    tag_t tSolRelType               = NULL_TAG;
    tag_t tOldAffRelType            = NULL_TAG;
    tag_t tOldSolRelType            = NULL_TAG;


    GRM_relation_t *ptSolGrmEcos    = NULL;
    GRM_relation_t *ptAffGrmEcos    = NULL;

    tag_t *ptTempEcos               = NULL;
    tag_t *ptEcosFinal              = NULL;

    tag_t tEco                      = NULL_TAG;

    char *pszFullId;
    char *pszPuid;


    if (tObject==NULL_TAG)
    {
        *iEcos= 0;
        *ptEcos= NULL;
        return iRetCode;
    }

    //Get related ecos of EC_afffected_item_rel/CMHasImpactedItem and EC_solution_item_rel/CMHasSolutionItem

    iRetCode = POM_tag_to_uid(tObject, &pszPuid);
    if (BNL_em_error_handler("POM_tag_to_uid", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    pszFullId=BNL_rp_get_full_id(tObject);

    fprintf(rplog, "Get related ecos of [%d][%s][%s]\n",tObject, pszPuid, pszFullId);
    //assumed the object is the solution, and need this relation
    MEM_free(pszPuid);

    iRetCode = GRM_find_relation_type("CMHasSolutionItem", &tSolRelType);
    if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    // Get the old ECO details
    iRetCode = GRM_find_relation_type("EC_affected_item_rel", &tOldAffRelType);
    if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    // Get the old ECO details
    iRetCode = GRM_find_relation_type("EC_solution_item_rel", &tOldSolRelType);
    if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    fprintf(rplog, "Get ECO following solutions relation\n");
    iRetCode = GRM_list_primary_objects  (tObject,tSolRelType, &iSolEcs, &ptSolGrmEcos);
    if (BNL_em_error_handler("GRM_list_primary_objects", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;


    if( iSolEcs == 0 )
    {
        fprintf(rplog, "Get ECO following solutions relation - old EngChange\n");
        iRetCode = GRM_list_primary_objects  (tObject, tOldSolRelType, &iSolEcs, &ptSolGrmEcos);
        if (BNL_em_error_handler("GRM_list_primary_objects", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
    }
    if( iAffEcs == 0 )
    {
        fprintf(rplog, "Get ECO following affected relation - old EngChange\n");
        iRetCode = GRM_list_primary_objects  (tObject,tOldAffRelType, &iAffEcs, &ptAffGrmEcos);
        if (BNL_em_error_handler("GRM_list_primary_objects", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
    }

     fprintf(rplog, "Found [%d] ecos at solution relation\n", iSolEcs);
     fprintf(rplog, "Found [%d] ecos at affected relation\n", iAffEcs);

     ptTempEcos=MEM_alloc(sizeof(tag_t)* (iSolEcs+iAffEcs));
     ptEcosFinal = MEM_alloc(sizeof(tag_t)* (iSolEcs+iAffEcs));

     for (i= 0;i<iSolEcs;i++)
     {
       //WSOM_ask_name(ptSolGrmEcos[i].primary,szName);
       //fprintf(rplog, "Found sol ECO [%s] to add\n",szName);
       ptTempEcos[i]=ptSolGrmEcos[i].primary;
     }

     for (i= 0;i<iAffEcs;i++)
     {

       //WSOM_ask_name(ptAffGrmEcos[i].primary,szName);
       //fprintf(rplog, "Found aff ECO [%s] to add\n",szName);

       ptTempEcos[iSolEcs+i]=ptAffGrmEcos[i].primary;
     }

     //current_job
     //iRetCode = BNL_tb_ask_object_status(tObject,szStatus);
     //iRetCode = BNL_rp_get_process_state(tObject, &pszState);

     if (iSolEcs + iAffEcs > 1)
     {
       char szStatus[33];
       /*
       Take the EngChange Rev that matches in order:
	      - Working
	      - In Process
	      - EngChange Revision used to Apply most recent status
		      (Otherwise the most recent EngChange)
       */

       iRetCode = BNL_rp_get_eco_matching_status("-", iSolEcs+iAffEcs, ptTempEcos, &tEco);
       if (BNL_em_error_handler("BNL_rp_get_eco_matching_status", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

       //fprintf(rplog , "DEBUG: Working eco [%d]\n", tEco);

       if (tEco != NULL_TAG)
       {
         fprintf(rplog, "ECO Working found.\n");
       }
       else
       {
          iRetCode = BNL_rp_get_eco_in_process(iSolEcs+iAffEcs, ptTempEcos, &tEco);
          if (BNL_em_error_handler("BNL_rp_get_eco_in_process", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

          //fprintf(rplog , "DEBUG: Eco in process [%d]\n", tEco);

          if (tEco != NULL_TAG)
          {
            fprintf(rplog, "ECO In Process found.\n");
          }
          else
          {
            iRetCode = BNL_tb_ask_object_status(tObject, szStatus);
            if (BNL_em_error_handler("BNL_tb_ask_object_status", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

            iRetCode = BNL_rp_get_eco_matching_status(szStatus, iSolEcs+iAffEcs, ptTempEcos, &tEco);
            if (BNL_em_error_handler("BNL_rp_get_eco_matching_status", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

            //fprintf(rplog, "DEBUG: Eco with matching status [%d]\n", tEco);

            if (tEco != NULL_TAG)
            {
              fprintf(rplog, "ECO with status [%s] has been found.\n", szStatus);
            }
            else
            {
              iRetCode = BNL_rp_get_latest_eco(iSolEcs+iAffEcs, ptTempEcos, &tEco);
              if (BNL_em_error_handler("BNL_rp_get_latest_eco", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

              //fprintf(rplog , "DEBUG: Latest eco [%d]\n", tEco);
              fprintf(rplog, "Using latest ECO.\n");
            }
          }
       }
       ptEcosFinal[0] = tEco;
       j++;
     }

     for (i = 0; i < (iSolEcs+iAffEcs); i++)
     {
        if (ptTempEcos[i] != tEco)
          ptEcosFinal[j++] = ptTempEcos[i];
     }

     /*for (i = 0; i < (iSolEcs+iAffEcs); i++)
     {
       fprintf(rplog , "DEBUG: ECO final [%d]\n", ptEcosFinal[i]);
     }*/

     *iEcos=iSolEcs+iAffEcs;
     //*ptEcos=ptTempEcos;
     *ptEcos = ptEcosFinal;


     if (ptSolGrmEcos!=NULL) MEM_free(ptSolGrmEcos);
     if (ptAffGrmEcos!=NULL) MEM_free(ptAffGrmEcos);
     if ((iSolEcs + iAffEcs) > 0) MEM_free(ptTempEcos);

    return iRetCode;

}

int BNL_rp_create_ecos_xml(int iEcos,tag_t *ptEcos,t_XMLElement **pXmlEcos)
{
    int iRetCode                = 0;
    int e                       = 0;
    t_XMLElement *pEcos         = NULL;
    t_XMLElement *pEco          = NULL;
    t_XMLElement *pEcoProps     = NULL;

    /*
    <ecos>
    <eco>
      <props></props>
    </eco>
    </ecos>
    */

    pEcos=BNL_xml_element_create(XML_EL_ECOS,NULL);

    pEco=BNL_xml_element_create(XML_EL_ECO,NULL);
    BNL_xml_element_add_element(pEcos, pEco);

    if (iEcos== 0)
    {
        iRetCode = BNL_rp_create_xml_props(NULL_TAG, "whereusedeco",0, &pEcoProps,NULL,NULL,NULL,NULL,NULL);
        if (BNL_em_error_handler("BNL_rp_create_xml_props", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

        BNL_xml_element_add_element(pEco, pEcoProps);

    }
    else
    {
        for (e= 0;e<iEcos;e++)
        {


            iRetCode = BNL_rp_create_xml_props(ptEcos[e], "whereusedeco",0, &pEcoProps,NULL,NULL,NULL,NULL,NULL);
            if (BNL_em_error_handler("BNL_rp_create_xml_props", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

            BNL_xml_element_add_element(pEco, pEcoProps);

        }
    }





    *pXmlEcos=pEcos;

    return iRetCode;
}

int BNL_rp_create_whereused_xml(logical lTopLevel, wu_list_t *pWuList,t_XMLElement **pXmlProducts)
{
    int iRetCode        = 0;

    int iEcos           = 0;


    tag_t *ptEcos       = NULL;

    tag_t tEco          =NULL_TAG;
    tag_t tLatestPrdRev =NULL_TAG;


    t_XMLElement *pEcoProps     = NULL;
    t_XMLElement *pProduct      = NULL;
    t_XMLElement *pProductProps = NULL;
    t_XMLElement *pProducts     = NULL;
    t_XMLElement *pEcos         = NULL;

    wu_list_t *pNode= NULL;



/*

     <products>
       <product>
          <props></props>
          <ecos></ecos>
       </product>
     <products>
*/

    pProducts=BNL_xml_element_create(XML_EL_PRODUCTS,NULL);

    pNode=pWuList;

   while (pNode)
    {
      if ((lTopLevel && pNode->iTopLevel == 1) || !lTopLevel)
      {
        pProduct=BNL_xml_element_create(XML_EL_PRODUCT,NULL);
        BNL_xml_element_add_element(pProducts, pProduct);

        //iRetCode = BNL_rp_create_xml_props(pNode->tRevision, "whereused",0, &pProductProps,NULL);
        if (lTopLevel)
        {
          iRetCode = BNL_rp_create_xml_props(pNode->tRevision, "whereused_top", pNode->iLevel, &pProductProps,NULL,NULL,NULL,NULL,NULL);
        }
        else
        {
          iRetCode = BNL_rp_create_xml_props(pNode->tRevision, "whereused_top", pNode->iLevel, &pProductProps,NULL,NULL,NULL,NULL,NULL);
        }
        if (BNL_em_error_handler("BNL_rp_create_xml_props", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

        BNL_xml_element_add_element(pProduct, pProductProps);

        if (!lTopLevel)
        {
          fprintf(rplog, "!TOP LEVEL\n");
          iRetCode = BNL_rp_get_latest_productline_revision(pNode->tRevision, &tLatestPrdRev);
          if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

          //need to get eco also, accepts NULL_TAG
          iRetCode = BNL_rp_get_related_eco(tLatestPrdRev, &iEcos, &ptEcos);
          if (BNL_em_error_handler("BNL_rp_get_related_eco", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

          iRetCode = BNL_rp_create_ecos_xml(iEcos, ptEcos, &pEcos);

          BNL_xml_element_add_element(pProduct, pEcos);

          if (ptEcos!=NULL) MEM_free(ptEcos);
          ptEcos= NULL;
          iEcos= 0;
        }
      } // End of if ((lTopLevel && pNode->iTopLevel == 1) || !lTopLevel)
      pNode=pNode->pNext;
    }

    *pXmlProducts=pProducts;

    return iRetCode;
}

int BNL_rp_web_where_used_list(int iLevel, int iMaxLevel, char *pszFilterType, char *pszRevRule, tag_t tProduct, wu_list_t **pWuList)
{
  int iRetCode            = 0;
  int iWu                 = 0;
  int w                   = 0;
  int u                   = 0;
  int iPrdRevUsed         = 0;
  int iPredUsed           = 0;
  int iUsed               = 0;
  int iPrdReleased        = 0;
  int iPredReleased       = 0;
  int iMaxLevelTemp       = 0;

  int *piLevels           = NULL;

  tag_t *ptWuList         = NULL;

  tag_t tLatestPrdRev     = NULL_TAG;
  tag_t tPredecessor      = NULL_TAG;
  tag_t tRevrule          = NULL_TAG;

  //char *pszFullId       = NULL;
  char *pszTemp           = NULL;

  clock_t psstart, psfinish;
  double  psduration;

  wu_list_t *pList        = NULL;
  wu_list_t *pList1       = NULL;
  wu_list_t *pNode        = NULL;


  if (iMaxLevel == -2)
  {
    iMaxLevelTemp = -1;
  }
  else
  {
    iMaxLevelTemp = iMaxLevel;
  }

  if ((iLevel <= iMaxLevelTemp) || iMaxLevelTemp == -1)
  {
    /*
      Do a whereused and filter them.

      The whereused and filter mechanism is explained in a flowchart see documents
      Systemconcept_FEI_EWU_06.pdf or SystemConcept_FEI_Enhancements_2006_1_1 .pdf
    */

    psstart = clock();

    fprintf(rplog, "DEBUG: iLevel [%d] iMaxLevel [%d] iMaxLevelTemp [%d]\n", iLevel, iMaxLevel, iMaxLevelTemp);

    if (iMaxLevel == -2)
    {
      //char *pszRevRule = NULL;

      //iRetCode = PREF_ask_char_value("IMAN_config_rule_name", 0, &pszRevRule);
      //if (BNL_em_error_handler("CFM_find ", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

      fprintf(rplog, "DEBUG: Using revision rule [%s]\n", pszRevRule);

      iRetCode = CFM_find(pszRevRule, &tRevrule);
      if (BNL_em_error_handler("CFM_find ", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

      iRetCode = PS_where_used_configured(tProduct, tRevrule, 1, &iWu, &piLevels, &ptWuList);
      if (BNL_em_error_handler("PS_where_used_configured ", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

      //if (pszRevRule != NULL) MEM_free(pszRevRule);
    }
    else
    {
      iRetCode = PS_where_used_all(tProduct, 1, &iWu, &piLevels, &ptWuList);
      if (BNL_em_error_handler("PS_where_used_all", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;
    }

    for (w = 0; w < iWu; w++)
    {
      pszTemp = BNL_rp_get_full_id(ptWuList[w]);
      fprintf(rplog, "DEBUG: Object [%d] level [%d] [%s]\n", ptWuList[w], piLevels[w], pszTemp);
      MEM_free(pszTemp);
    }

    psfinish = clock();
    psduration = (double)(psfinish - psstart) / CLOCKS_PER_SEC;
    fprintf(rplog, "Where used all took: [%2.1f]s\n", psduration);

    fprintf(rplog, "Found %d where used targets.\n", iWu);

    for (w = 0; w < iWu; w++)
    {
      psstart = clock();

      if (iMaxLevel == -2)
      {
        fprintf(rplog, "TOP LEVEL\n");
        BNL_rp_add_whereused_list(true, iLevel, ptWuList[w], &pList);
      }
      else
      {
        fprintf(rplog, "INFO: Processing [%d] of [%d]\n", (w + 1), iWu);

        iRetCode = BNL_rp_get_latest_productline_revision(ptWuList[w], &tLatestPrdRev);
        if (BNL_em_error_handler("BNL_rp_get_latest_productline_revision", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

        psfinish = clock();
        psduration = (double)(psfinish - psstart) / CLOCKS_PER_SEC;
        fprintf(rplog, "Get latest revision in product line took: [%2.1f]s\n", psduration);


        /*
        pszTemp=BNL_rp_get_full_id(ptWuList[w]);
        pszFullId=BNL_rp_get_full_id(tLatestPrdRev);
        fprintf(rplog, "[%s] ->Latest revision in productline=[%s]\n", pszTemp, pszFullId);
        MEM_free(pszFullId);
        MEM_free(pszTemp);
        */

        iRetCode = BNL_rp_get_predecessor(tLatestPrdRev, &tPredecessor);
        iPredReleased = BNL_rp_is_released(tPredecessor);

        // Try to find a released predecessor
        while (!iPredReleased && tPredecessor != NULL_TAG)
        {
          iRetCode = BNL_rp_get_predecessor(tPredecessor, &tPredecessor);
          if (BNL_em_error_handler("BNL_rp_get_predecessor", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

          iPredReleased = BNL_rp_is_released(tPredecessor);
        }

        if (!iPredReleased)
        {
          iRetCode = BNL_rp_get_predecessor(tLatestPrdRev, &tPredecessor);
          iPredReleased = BNL_rp_is_released(tPredecessor);
        }

        //fprintf(rplog, "DEBUG: tPredecessor [%d]\n", tPredecessor);

        //check if used
        //JM: Needs to be updated when level != 1
        for (u = 0;u <iWu; u++)
        {
          if (tLatestPrdRev == ptWuList[u]) iPrdRevUsed = 1;
          if (tPredecessor == ptWuList[u]) iPredUsed = 1;
        }

        //note that predecessor can be NULL_TAG.
        iPrdReleased = BNL_rp_is_released(tLatestPrdRev);

        //iPredReleased = BNL_rp_is_released(tPredecessor);

        //fprintf(rplog, "Latestproduct used:%d released:%d\n", iPrdRevUsed, iPrdReleased);

        if (iPrdRevUsed && iPrdReleased)
        {
          BNL_rp_add_whereused_list(false, iLevel, tLatestPrdRev, &pList);
        }

        if (tPredecessor==NULL_TAG && !iPrdReleased && iPrdRevUsed)
        {
          BNL_rp_add_whereused_list(false, iLevel, tLatestPrdRev, &pList);
        }


        // everything is determined so compare
        if (iPredReleased && iPredUsed && !iPrdReleased && !iPrdRevUsed)
        {
          BNL_rp_add_whereused_list(false, iLevel, tPredecessor, &pList);
        }

        if (iPredReleased && iPredUsed && iPrdReleased && iPrdRevUsed)
        {
          BNL_rp_add_whereused_list(false, iLevel, tLatestPrdRev, &pList);
        }

        if (iPredReleased && iPredUsed && !iPrdReleased &&  iPrdRevUsed)
        {
          BNL_rp_add_whereused_list(false, iLevel, tPredecessor, &pList);
          BNL_rp_add_whereused_list(false, iLevel, tLatestPrdRev, &pList);
        }

        if (iPredReleased && !iPredUsed && !iPrdReleased &&  iPrdRevUsed)
        {
          BNL_rp_add_whereused_list(false, iLevel, tLatestPrdRev, &pList);
        }

        if (iPredReleased && !iPredUsed && iPrdReleased &&  iPrdRevUsed)
        {
          BNL_rp_add_whereused_list(false, iLevel, tLatestPrdRev, &pList);
        }

        iPrdRevUsed = 0;
        iPredUsed = 0;
        iPrdReleased = 0;
        iPredReleased = 0;
      } // End of else if (iMaxLevel == -2)
    }//end of for (w= 0;w<iWu;w++)

    if (iWu > 0)
    {
      MEM_free(piLevels);
      MEM_free(ptWuList);
    }

    pNode = pList;
    // Update list - move latest revisions
    while (pNode)
    {
      if (pNode->tRevision != pNode->tLatestRevision)
      {
        // Check if latest revision already om correct position
        if  (pNode->pNext != NULL && pNode->pNext->tRevision == pNode->tLatestRevision)
        {
          //fprintf(stdout, "Latest revision already in place.\n");
        }
        else
        {
          wu_list_t *pNodeTemp        = pList;

          // Check if latest revison is also in list
          while (pNodeTemp)
          {
            if (pNodeTemp->tRevision == pNode->tLatestRevision)
            {
               // This is the one, remove from current position
              if (pNodeTemp->pPrev != NULL)
                pNodeTemp->pPrev->pNext = pNodeTemp->pNext;
              else
              {
                // Reset pList
                if (pNodeTemp->pNext != NULL) pList = pNodeTemp->pNext;
              }
              if (pNodeTemp->pNext != NULL) pNodeTemp->pNext->pPrev = pNodeTemp->pPrev;

              // Now insert after pNode
              pNodeTemp->pNext = pNode->pNext;
              pNodeTemp->pPrev = pNode;

              if (pNode->pNext != NULL) pNode->pNext->pPrev = pNodeTemp;
              pNode->pNext = pNodeTemp;

              break;
            } // End of if (pNodeTemp->tRevision == pNode->tLatestRevision)

            pNodeTemp = pNodeTemp->pNext;
          } // End of while (pNodeTemp)
        } // End of else if  (pNode->pNext != NULL && pNode->pNext->tRevision == pNode->tLatestRevision))
      } // End if if (pNode->tRevision != pNode->tLatestRevision)
      pNode=pNode->pNext;
    }// End of while (pNode)

    pNode = pList;
    while (pNode)
    {
      tag_t tItem                = NULL_TAG;
      char szType[WSO_name_size_c + 1] = { "" };
      wu_list_t *pListNew        = NULL;
      wu_list_t *pNodeNew        = NULL;
      wu_list_t *pTemp           = NULL;
      logical lStop              = false;


      //fprintf(rplog, "tag revision [%d] id [%s] level [%d] prev [%d] next [%d]\n",
        //pNode->tRevision, pNode->szItemId, pNode->iLevel, pNode->pPrev, pNode->pNext);

      if (pszFilterType != NULL)
      {
        fprintf(rplog, "Filter type [%s], checking type...\n", pszFilterType);

        // Check the type, if type is filter type, then stop
        iRetCode = ITEM_ask_item_of_rev(pNode->tRevision, &tItem);
        BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true);

        if (tItem != NULL_TAG)
        {
          BNL_tb_determine_object_type(tItem, szType);
          fprintf(rplog, "Type is [%s]\n", szType);

          if (strcmp(szType, pszFilterType) == 0)
          //if (strcmp(szType, pszFilterType) == 0 || strstr(pszFilterType, szType) != NULL) //JM| This can be used when filtering on several ItemTypes
          {
            lStop = true;
            fprintf(rplog, "Match, stopping!\n");
          }
        }
      }

      if (!lStop)
      {
        BNL_rp_web_where_used_list((iLevel + 1), iMaxLevel, pszFilterType, pszRevRule, pNode->tRevision, &pListNew);
        if (pListNew == NULL)
        {
          fprintf(rplog, "DEBUG: List is empty, previous level was top level\n");
          pNode->iTopLevel = 1;
        }
      }
      else
      {
        fprintf(rplog, "DEBUG: Stopping by filter type, previous level was top level\n");
        pNode->iTopLevel = 1;
      }

      pTemp = pNode->pNext;

      if (pListNew != NULL)
      {
        // Insert
        pListNew->pPrev = pNode;

        pNodeNew = pListNew;
        while (pNodeNew)
        {
          if (pNodeNew->pNext == NULL)
          {
            if (pNode->pNext != NULL)
            {
              pNodeNew->pNext = pNode->pNext;
              pNode->pNext->pPrev = pNodeNew;
            }
            break;
          }
          pNodeNew = pNodeNew->pNext;
        }
        pNode->pNext = pListNew;
      }
      pNode = pTemp;
    }
  }// End of if ((iLevel <= iMaxLevel) || iMaxLevel == -1)

  //if (iMaxLevel == -2)
  //{
    //*pWuList = pList1;
  //}
  //else
  {
    *pWuList = pList;
  }

  return iRetCode;
}

int BNL_rp_web_where_used(int iLevel, int iMaxLevel, char *pszFilterType, char *pszRevRule, tag_t tProduct, char **pszXMLOut, t_XMLElement **pDocument)
{
  int iRetCode        = 0;
  int iBufSize        = 0;
  int iEcos           = 0;

  tag_t *ptEcos       =NULL_TAG;

  t_XMLElement *pXmlDocument  = NULL;
  t_XMLElement *pProductProps = NULL;
  t_XMLElement *pWhereUsed    = NULL;
  t_XMLElement *pEcos         = NULL;
  t_XMLElement *pProducts     = NULL;

  wu_list_t *pWuList      = NULL;
  wu_list_t *pNode        = NULL;

  clock_t start, finish;
  double  duration;

  fprintf(rplog, "BNL_rp_web_where_used executed.\n");

  start=clock();

  /* create following structure
    <document>
        <whereused>
            <props></props>
            <ecos>
                <eco>
                <props></props>
                </eco>
            </ecos>
            <products>
                <product>
                    <props></props>
                    <ecos>
                        <eco>
                            <props>    </props>
                        </eco>
                    </ecos>
                </product>
            </products>
        </whereused>
    </document>

*/


  pXmlDocument=BNL_xml_element_create(XML_EL_DOCUMENT,NULL);

  BNL_rp_web_where_used_list(iLevel, iMaxLevel, pszFilterType, pszRevRule, tProduct, &pWuList);

  if (iMaxLevel == -2)
  {
    int iTempLevel          = 1;
    logical lReset          = false;

    pNode = pWuList;
    // Update list - sort on level
    while (pNode)
    {
      if (pNode->iLevel > iTempLevel) lReset = true;

      if (pNode->iLevel == iTempLevel)
      {
          wu_list_t *pNodeTemp        = pWuList;

          while (pNodeTemp)
          {
            if (pNode->iLevel < pNodeTemp->iLevel)
            {
              if (pNode->pPrev != NULL) pNode->pPrev->pNext = pNode->pNext;
              if (pNode->pNext != NULL) pNode->pNext->pPrev = pNode->pPrev;

              if (pNodeTemp->pPrev != NULL) pNodeTemp->pPrev->pNext = pNode;

              pNode->pPrev = pNodeTemp->pPrev;
              pNode->pNext = pNodeTemp;

              pNodeTemp->pPrev = pNode;

              break;
            } // End of if (pNode->iLevel == iTempLevel)
            pNodeTemp = pNodeTemp->pNext;
          } // End of while (pNodeTemp)
        } // End of if (pNode->iLevel == iTempLevel)
      pNode = pNode->pNext;

      if (pNode == NULL && lReset)
      {
        lReset = false;
        pNode = pWuList;
        iTempLevel++;
      }
    }// End of while (pNode)
  } // End of if (iMaxLevel == -2)

  pNode=pWuList;
  while (pNode)
  {
    fprintf(rplog, "**********************************\n");
    fprintf(rplog, "tag revision [%d]\n", pNode->tRevision);
    fprintf(rplog, "tag last revision [%d]\n", pNode->tLatestRevision);
    fprintf(rplog, "id [%s] \n", pNode->szItemId);
    fprintf(rplog, "level [%d] \n", pNode->iLevel);
    fprintf(rplog, "top [%d] \n", pNode->iTopLevel);
    fprintf(rplog, "prev [%d]\n", pNode->pPrev);
    fprintf(rplog, "next [%d]\n", pNode->pNext);
    fprintf(rplog, "**********************************\n");

    pNode=pNode->pNext;
  }


  pWhereUsed=BNL_xml_element_create(XML_EL_WHEREUSED,NULL);
  if (iMaxLevel == -2)
  {
    iRetCode = BNL_rp_create_xml_props(tProduct, "whereused_topprod",0, &pProductProps,NULL,NULL,NULL,NULL,NULL);
  }
  else
  {
    iRetCode = BNL_rp_create_xml_props(tProduct, "whereusedprod",0, &pProductProps,NULL,NULL,NULL,NULL,NULL);
  }
  if (BNL_em_error_handler("BNL_rp_create_xml_props", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

  BNL_xml_element_add_element(pWhereUsed, pProductProps);

  if (iMaxLevel == -2)
  {
    fprintf(rplog, "TOP LEVEL, no need for related eco\n");
  }
  else
  {
    //need also the related ecos
    iRetCode = BNL_rp_get_related_eco(tProduct, &iEcos, &ptEcos);
    if (BNL_em_error_handler("BNL_rp_get_related_eco", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    BNL_rp_create_ecos_xml(iEcos, ptEcos, &pEcos);
    BNL_xml_element_add_element(pWhereUsed, pEcos);

    if (ptEcos!=NULL) MEM_free(ptEcos);
    ptEcos= NULL;
    iEcos= 0;
  }

  //addd from the real used products the props etc in the xml structure
  if (iMaxLevel == -2)
  {
    fprintf(rplog, "BNL_rp_create_whereused_xml, TOP LEVEL\n");
    iRetCode = BNL_rp_create_whereused_xml(true, pWuList, &pProducts);
  }
  else
  {
    fprintf(rplog, "BNL_rp_create_whereused_xml, !TOP LEVEL\n");
    iRetCode = BNL_rp_create_whereused_xml(false, pWuList, &pProducts);
  }
  if (BNL_em_error_handler("BNL_rp_create_whereused_xml", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

  BNL_xml_element_add_element(pWhereUsed, pProducts);
  BNL_xml_element_add_element(pXmlDocument, pWhereUsed);

  while(pWuList)
  {
    if (pWuList->pPrev!=NULL) free(pWuList->pPrev);
    if (pWuList->pNext==NULL)
    {
      free(pWuList);
      pWuList= NULL;
      break;
    }
    pWuList=pWuList->pNext;
  }

  //now we have a used list, create a report structure
  iRetCode = BNL_xml_convert_xml_to_buffer(pXmlDocument, &iBufSize, pszXMLOut);
  if (BNL_em_error_handler("BNL_xml_convert_xml_to_buffer", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

  //fprintf(rplog, "%s",*pszXMLOut);

  *pDocument=pXmlDocument;

  finish=clock();
  duration = (double)(finish - start) / CLOCKS_PER_SEC;
  fprintf(rplog, "Where used report creation took:[%2.1f]s\n",duration);

  return iRetCode;
}

int BNL_rp_open_user_report()
{
    int iRetCode        = 0;

    if (fpUserRep==NULL)
    {


        iRetCode = BNL_fm_open_file(&fpUserRep,gDocdownload_info.pszExportDir, "index.html", "w+t",1);

        fprintf(fpUserRep, "<html>\n");
        fprintf(fpUserRep, "<body>\n\n");

        fprintf(fpUserRep, "<h3>Overview Document Download</h3><br>\n");


        fprintf(fpUserRep, "<table  border=1>\n");
        fprintf(fpUserRep, "<tr><td><b>Product</b></td><td><b>Document</b></td><td><b>File</b></td><td><b>Error</b></td></tr>\n");
    }

    return iRetCode;
}

int BNL_rp_close_user_report(char *pszMainReport)
{
    int iRetCode        = 0;

    char *pszPath       = NULL;
    char *pszFileName   = NULL;
    char *pszExt        = NULL;

    fprintf(fpUserRep, "</table>\n");

     if (pszMainReport!=NULL)
     {
            BNL_fm_parse_path(pszMainReport, &pszPath, &pszFileName, &pszExt);
            fprintf(fpUserRep, "<br><b>Main report:</b><a href=\".\\%s.%s\">%s.%s</a><br><br>\n", pszFileName, pszExt, pszFileName, pszExt);
            free(pszPath);

            giReported=1;
     }

    fprintf(fpUserRep, "\n\n</body>\n");
    fprintf(fpUserRep, "</html>\n");

    BNL_fm_close_file(fpUserRep);

    if (!giReported) BNL_fm_delete_file(gDocdownload_info.pszExportDir, "index.html");

    fpUserRep= NULL;

    return iRetCode;
}

int BNL_report_a_line(char *pszItemId,char *pszDocId,char *pszFileName,char *pszState)
{
    int iRetCode        = 0;

    giReported=1;

    if (pszDocId==NULL)
    {
            //                   itemid      docid     fname       state
        fprintf(fpUserRep, "<tr><td>%s</td><td></td><td><a href=\".\\%s\">%s</a></td><td>%s</td></tr>\n", pszItemId, pszFileName, pszFileName, pszState);
    }
    else
    {
        //                       itemid     docid       fname     state
        fprintf(fpUserRep, "<tr><td>%s</td><td>%s</td><td><a href=\".\\%s\">%s</a></td><td>%s</td></tr>\n", pszItemId, pszDocId, pszFileName, pszFileName, pszState);
    }


    return iRetCode;

}

/**
int BNL_split_multiple_values
(
 char *pszValue,          <I>
 char chSeparator,        <I>
 int  *iCount,            <O>
 char ***pszValues        <OF>
)

  Description:

  Function to split string using the given separator

  Returns:

  0 on Ok

*/
int BNL_split_multiple_values(char *pszValue,char chSeparator, int  *iCount,char ***pszValues)
{
  int iRetCode                = 0;
  int i                       = 0;
  int iLen                    = 0;

  char *pszStart              = NULL;
  char *pszNode               = NULL;
  char *pszVal                = NULL;

  char **pszParsedVals        = NULL;

  logical lFound              = false;

  pszNode = pszValue;

  /* Count the values */
  while (*pszNode!='\0')
  {
    if (*pszNode == chSeparator) i++;
    pszNode++;
  }
  i++; /* The last value */

  pszParsedVals = (char **)malloc(sizeof(char*)*i);

  /* Reset */
  pszNode = pszValue;
  i = 0;
  pszStart = pszNode;
  while (*pszNode!='\0')
  {
    if (*pszNode == chSeparator)
    {
      *pszNode = '\0';
      iLen = (int)strlen(pszStart) + 1;
      pszVal = (char*)malloc(sizeof(char) * iLen);
      strcpy(pszVal, pszStart);
      pszParsedVals[i] = pszVal;
      pszStart = pszNode + 1;
      i++;
    }
    pszNode++;
  }

  /* Last not separated value */
  iLen = (int)strlen(pszStart) + 1;
  pszVal=(char*)malloc(sizeof(char) * iLen);
  strcpy(pszVal, pszStart);
  pszParsedVals[i] = pszVal;
  i++;

  *iCount=i;
  *pszValues=pszParsedVals;

  return iRetCode;
}/* End of BNL_FEI_split_multiple_values */


/**
int BNL_rp_web_comprep
(
  int iTargets,               <I>
  tag_t *ptTargets,           <I>
  int iWorkflow,              <I>
  char *pszConfig,            <I>
  char **pszXMLdiff,          <OF>
  t_XMLElement **pDocument,   <OF>
  int *piResult               <O>
)

  Description:

   Function to create compliance report

  Returns:

  0 on Ok

*/
int BNL_rp_web_comprep
(
  int iTargets,
  tag_t *ptTargets,
  int iWorkflow,
  char *pszConfig,
  char **pszXMLdiff,
  t_XMLElement **pDocument,
  int *piResult
)
{
  int iRetCode    = 0;
  int iRevs       = 0;
  int iRevsTemp   = 0;
  int t           = 0;
  int i           = 0;
  int d           = 0;
  int p           = 0;

  tag_t tEco      = NULL_TAG;
  tag_t *ptRevs   = NULL;
  tag_t *ptRevsTemp= NULL;

  char szType[WSO_name_size_c + 1];
  char szMsg[257];
  char szObjClass[WSO_name_size_c + 1];
  char szPref[256];

  char *pszConfigFile     = NULL;
  char *pszConfigFileTemp = NULL;

  array_t *pStatusDiffsArray;
  array_t *pAttrDiffsArray;
  array_t *pBomDiffsArray;

  t_XMLElement *pConfig           = NULL;

  t_XMLElement *pParent           = NULL;
  t_XMLElement *pXmlStatusdiffs   = NULL;
  t_XMLElement *pXmlAttrdiffs     = NULL;
  t_XMLElement *pXmlBomdiffs      = NULL;
  t_XMLElement *pComp             = NULL;
  t_XMLElement *pProd             = NULL;
  t_XMLElement *pDiff             = NULL;
  t_XMLElement *pProps            = NULL;
  t_XMLElement *pEcoProps         = NULL;
  t_XMLElement *pEcoInfo          = NULL;

  diffinfo_t *pDiffs;

  clock_t start, finish;
  double duration;

  char  **pszPrefValues   = NULL;
  int   iPrefValues       = 0;

  fprintf(rplog, "\nBNL_rp_web_comprep executed.\n");

  *piResult = 0;

  start = clock();

  // Get the configuration file
  sprintf(szPref, "FEI_compliance_%s", pszConfig);
  fprintf(rplog, "%s: Looking for preference [%s].\n", BNL_module, szPref);

  iRetCode = PREF_ask_char_value(szPref, 0, &pszConfigFileTemp);
  if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  EMH_clear_errors();

  if (pszConfigFileTemp == NULL)
  {
    sprintf(szMsg, "Failed to read preference [%s].", szPref);
    fprintf(rplog, "%s: %s\n", BNL_module, szMsg);

    iRetCode = BNL_error_message;
    EMH_store_error_s1(EMH_severity_error, iRetCode, szMsg);

    goto function_exit;
  }

  BNL_tb_replace_env_for_path(pszConfigFileTemp, &pszConfigFile);
  if (pszConfigFile == NULL)
  {
    pszConfigFile = pszConfigFileTemp;
  }
  else
  {
    MEM_free(pszConfigFileTemp);
  }

  fprintf(rplog, "%s: Configuration file [%s] will be used.\n", BNL_module, pszConfigFile);

  // Read the configuration file
  BNL_xml_load_document(pszConfigFile, &pConfig);
  if (pConfig == NULL)
  {
    sprintf(szMsg, "Error processing configuration file [%s].", pszConfigFile);
    fprintf(rplog, "%s: %s\n", BNL_module, szMsg);

    iRetCode = BNL_error_message;
    EMH_store_error_s1(EMH_severity_error, iRetCode, szMsg);

    goto function_exit;
  }

  fprintf(rplog, "%s: Configuration file [%s] has been read.\n", BNL_module, pszConfigFile);

  // Get the targets
  if (iWorkflow != 1 && iTargets == 1)
  {
    // Check if target is Engineering Change Revision or New change
    iRetCode = BNL_tb_pref_ask_char_values(BNL_REP_CHANGE_REV_TYPES_PREF, TC_preference_site, &iPrefValues, &pszPrefValues);
    if( iPrefValues == 0)
    {
      fprintf(rplog, "Error: There are no values specified in preference [%s].\n", BNL_REP_CHANGE_REV_TYPES_PREF);
      goto function_exit;
    }

    iRetCode = BNL_tb_determine_object_type(ptTargets[0], szType);
    if (BNL_tb_value_in_list(szType, iPrefValues, pszPrefValues))
    {
      int iAffRevs        = 0;
      int iSolRevs        = 0;

      tag_t *ptAffRevs    = NULL;
      tag_t *ptSolRevs    = NULL;

      fprintf(rplog, "%s: Target is [%s], getting affected and solution Items.\n", BNL_module, szType);

      tEco = ptTargets[0];
      AOM_refresh(tEco, false);

      if (strcmp(szType, BNL_ENGCHANGE_REVISION) != 0)
      {
        tag_t tRelType2 = NULL_TAG;

        iRetCode=GRM_find_relation_type("CMHasSolutionItem", &tRelType2);
        if (BNL_em_error_handler("GRM_find_relation_type",BNL_module,EMH_severity_error,iRetCode, true)) return iRetCode;

        iRetCode=GRM_list_secondary_objects_only(tEco,tRelType2,&iSolRevs, &ptSolRevs);
        if (BNL_em_error_handler("GRM_list_secondary_objects_only",BNL_module,EMH_severity_error,iRetCode, true)) return iRetCode;
        fprintf(rplog, "%s: Number of solution Items [%d].\n", BNL_module, iSolRevs);
      }
      else
      {
        iRetCode = ECM_get_contents(tEco, "affected_items", &iAffRevs, &ptAffRevs);
        if (BNL_em_error_handler("ECM_get_contents", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        fprintf(rplog, "%s: Number of affected Items [%d].\n", BNL_module, iAffRevs);

        iRetCode = ECM_get_contents(tEco, "solution_items", &iSolRevs, &ptSolRevs);
        if (BNL_em_error_handler("ECM_get_contents", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        fprintf(rplog, "%s: Number of solution Items [%d].\n", BNL_module, iSolRevs);
      }

      iRevsTemp = iAffRevs + iSolRevs;
      ptRevsTemp = MEM_alloc(sizeof(tag_t) * iRevsTemp);

      for (t = 0; t < iAffRevs; t++)
      {
        ptRevsTemp[t] = ptAffRevs[t];
      }
      for (t = 0; t < iSolRevs; t++)
      {
        ptRevsTemp[iAffRevs + t] = ptSolRevs[t];
      }

      if (iAffRevs > 0) MEM_free(ptAffRevs);
      if (iSolRevs > 0) MEM_free(ptSolRevs);
    } // End of if (BNL_tb_value_in_list(szType, iPrefValues, pszPrefValues))
    else
    {
      iRevsTemp = 1;
      ptRevsTemp = MEM_alloc(sizeof(tag_t) * 1);
      ptRevsTemp[0] = ptTargets[0];
    }
  }
  else
  {
    iRevsTemp = iTargets;
    ptRevsTemp = MEM_alloc(sizeof(tag_t) * iRevsTemp);

    for (t = 0; t < iRevsTemp; t++)
    {
      ptRevsTemp[t] = ptTargets[t];
    }
  } // End of if (iWorkflow != 1 && iTargets == 1)

  // Only process the ItemRevisions
  for (t = 0; t < iRevsTemp; t++)
  {
    // Check the class of the target
    if ((iRetCode = BNL_tb_determine_super_class(ptRevsTemp[t], szObjClass)) != 0) goto function_exit;

    if (strcmp(szObjClass, "ItemRevision") == 0)
    {
      iRevs++;
      ptRevs = (tag_t *)MEM_realloc(ptRevs, iRevs * sizeof(tag_t));

      ptRevs[iRevs - 1] = ptRevsTemp[t];
    }
    else
    {
      fprintf(rplog, "%s: Class of target [%s] is not ItemRevision, skipping...\n", BNL_module, szObjClass);
    }
  }

  // Initialize difference arrays
  pStatusDiffsArray = MEM_alloc(sizeof(array_t) * (iRevs));
  pAttrDiffsArray = MEM_alloc(sizeof(array_t) * (iRevs));
  pBomDiffsArray = MEM_alloc(sizeof(array_t) * (iRevs));

  // Process the target revision
  for (t = 0; t < iRevs; t++)
  {
    int iStatusCount               = 0;
    int iAttrCount                 = 0;
    int iBomCount                  = 0;

    t_XMLElement *pCheck           = NULL;
    t_XMLElement *pAttribute       = NULL;
    t_XMLElement *pBom             = NULL;

    diffinfo_t *pStatusDiffs        = NULL;
    diffinfo_t *pAttrDiffs          = NULL;
    diffinfo_t *pBomDiffs           = NULL;


    fprintf(rplog, "\n*** Processing target [%d] of [%d] ***\n", (t + 1), iRevs);

    // Refresh
    AOM_refresh(ptRevs[t], false);

    // WSO Status check
    pCheck = BNL_xml_find_first(pConfig, XML_EL_WSOSTATUS);
    if (pCheck != NULL)
    {
      fprintf(rplog, "*** WSO status check ***\n");
      if ((iRetCode = BNL_rp_wso_status_check(ptRevs[t], pCheck, &iStatusCount, &pStatusDiffs)) != 0) goto function_exit;
      fprintf(rplog, "*** End of WSO status check ***\n");
    }

    // Rel Status check
    pCheck = BNL_xml_find_first(pConfig, XML_EL_RELSTATUS);
    if (pCheck != NULL)
    {
      fprintf(rplog, "*** Relation status check ***\n");
      if ((iRetCode = BNL_rp_rel_status_check(ptRevs[t], pCheck, &iStatusCount, &pStatusDiffs)) != 0) goto function_exit;
      fprintf(rplog, "*** End of relation status check ***\n");
    }

    // Bvr Status check
    pCheck = BNL_xml_find_first(pConfig, XML_EL_BVRSTATUS);
    if (pCheck != NULL)
    {
      fprintf(rplog, "*** Bvr status check ***\n");

      fprintf(rplog, "DEBUG: iTargets [%d] ptRevs[t] [%d] ptTargets[0][%d]\n", iTargets, ptRevs[t], ptTargets[0]);
      if (iTargets == 1 && ptRevs[t] != ptTargets[0])
      {
        if ((iRetCode = BNL_rp_bvr_status_check(ptRevs[t], pCheck, iRevs, ptRevs, &iStatusCount, &pStatusDiffs)) != 0) goto function_exit;
      }
      else
      {
        if ((iRetCode = BNL_rp_bvr_status_check(ptRevs[t], pCheck, iTargets, ptTargets, &iStatusCount, &pStatusDiffs)) != 0) goto function_exit;
      }
      fprintf(rplog, "*** End of bvr status check ***\n");
    }

    // Attribute 1 check
    pCheck = BNL_xml_find_first(pConfig, XML_EL_ATTR1);
    if (pCheck != NULL)
    {
      pAttribute = BNL_xml_find_first(pCheck, XML_EL_ATTR);

      while (pAttribute != NULL)
      {
        fprintf(rplog, "*** Attribute-1 check ***\n");
        if ((iRetCode = BNL_rp_attr_1_check(ptRevs[t], pAttribute, &iAttrCount, &pAttrDiffs)) != 0) goto function_exit;
        fprintf(rplog, "*** End of attribute-1 check ***\n");

        pAttribute = pAttribute->pNextSibling;
      }
    }

    // Attribute 2 check
    pCheck = BNL_xml_find_first(pConfig, XML_EL_ATTR2);
    if (pCheck != NULL)
    {
      pAttribute = BNL_xml_find_first(pCheck, XML_EL_ATTR);

      while (pAttribute != NULL)
      {
        fprintf(rplog, "*** Attribute-2 check ***\n");
        if ((iRetCode = BNL_rp_attr_2_check(ptRevs[t], pAttribute, &iAttrCount, &pAttrDiffs)) != 0) goto function_exit;
        fprintf(rplog, "*** End of attribute-2 check ***\n");

        pAttribute = pAttribute->pNextSibling;
      }
    }

    // Attribute 3 check
    pCheck = BNL_xml_find_first(pConfig, XML_EL_ATTR3);
    if (pCheck != NULL)
    {
      fprintf(rplog, "*** Attribute-3 status check ***\n");
      if ((iRetCode = BNL_rp_attr_3_check(ptRevs[t], pCheck, &iAttrCount, &pAttrDiffs)) != 0) goto function_exit;
      fprintf(rplog, "*** End of attribute-3 status check ***\n");
    }

    // Bom check
    pCheck = BNL_xml_find_first(pConfig, XML_EL_BOM_CHECK);
    if (pCheck != NULL)
    {
      fprintf(rplog, "*** Bom check ***\n");
      if ((iRetCode = BNL_rp_bom_check(ptRevs[t], pCheck, &iBomCount, &pBomDiffs)) != 0) goto function_exit;
      fprintf(rplog, "*** End of bom check ***\n");
    }

    pStatusDiffsArray[t].tProduct = ptRevs[t];
    pStatusDiffsArray[t].ptDiffs = pStatusDiffs;
    pStatusDiffsArray[t].iDiffsCount = iStatusCount;

    pAttrDiffsArray[t].tProduct = ptRevs[t];
    pAttrDiffsArray[t].ptDiffs = pAttrDiffs;
    pAttrDiffsArray[t].iDiffsCount = iAttrCount;

    pBomDiffsArray[t].tProduct = ptRevs[t];
    pBomDiffsArray[t].ptDiffs = pBomDiffs;
    pBomDiffsArray[t].iDiffsCount = iBomCount;

    if (iStatusCount > 0 || iAttrCount > 0 || iBomCount > 0) *piResult = 1;

    fprintf(rplog, "*** End of processing target [%d] of [%d] ***\n\n", (t + 1), iRevs);
  } // End of for (t = 0; t < iRevs; t++)

  BNL_xml_close_document(pConfig);


  /* Create xml structure */

  pParent = BNL_xml_element_create(XML_EL_DOCUMENT, NULL);

  if (tEco != NULL_TAG)
  {
    pEcoInfo = BNL_xml_element_create(XML_EL_ECO,NULL);

    BNL_rp_create_xml_props(tEco, "eco_comprep", 0, &pEcoProps, NULL, NULL, NULL, NULL,NULL);

    BNL_xml_element_add_element(pEcoInfo, pEcoProps);

    BNL_xml_element_add_element(pParent, pEcoInfo);
  }

  // Status differences
  pXmlStatusdiffs = BNL_xml_element_create(XML_EL_STATUSDIFFS, NULL);

  for (i = 0; i < iRevs; i++)
  {
    pComp = BNL_xml_element_create(XML_EL_COMP, NULL);

    pProd = BNL_xml_element_create(XML_EL_PRODUCT, NULL);

    BNL_rp_create_xml_props(ptRevs[i], "prod_comprep", 0, &pProps, NULL, NULL, NULL, NULL,NULL);

    BNL_xml_element_add_element(pProd, pProps);

    BNL_xml_element_add_element(pComp, pProd);

    pDiffs = pStatusDiffsArray[i].ptDiffs;

    for (d = 0;d < pStatusDiffsArray[i].iDiffsCount; d++)
    {
      BNL_rp_create_xml_diff(pDiffs[d], &pDiff);
      BNL_xml_element_add_element(pComp, pDiff);
    }

    BNL_xml_element_add_element(pXmlStatusdiffs, pComp);
  }

  BNL_xml_element_add_element(pParent, pXmlStatusdiffs);

  // Attribute differences
  pXmlAttrdiffs = BNL_xml_element_create(XML_EL_ATTRIBUTESDIFFS, NULL);

  for (i = 0; i < iRevs; i++)
  {
    pComp = BNL_xml_element_create(XML_EL_COMP, NULL);

    pProd = BNL_xml_element_create(XML_EL_PRODUCT, NULL);

    BNL_rp_create_xml_props(ptRevs[i], "prod_comprep", 0, &pProps, NULL, NULL, NULL, NULL,NULL);

    BNL_xml_element_add_element(pProd, pProps);

    BNL_xml_element_add_element(pComp, pProd);

    pDiffs = pAttrDiffsArray[i].ptDiffs;

    for (d = 0;d < pAttrDiffsArray[i].iDiffsCount; d++)
    {
      BNL_rp_create_xml_diff(pDiffs[d], &pDiff);
      BNL_xml_element_add_element(pComp, pDiff);
    }

    BNL_xml_element_add_element(pXmlAttrdiffs, pComp);
  }

  BNL_xml_element_add_element(pParent, pXmlAttrdiffs);


  // Bom differences
  pXmlBomdiffs = BNL_xml_element_create(XML_EL_BOM, NULL);

  for (i = 0; i < iRevs; i++)
  {
    pComp = BNL_xml_element_create(XML_EL_COMP, NULL);

    pProd = BNL_xml_element_create(XML_EL_PRODUCT, NULL);

    BNL_rp_create_xml_props(ptRevs[i], "prod_comprep", 0, &pProps, NULL, NULL, NULL, NULL,NULL);

    BNL_xml_element_add_element(pProd, pProps);

    BNL_xml_element_add_element(pComp, pProd);

    pDiffs = pBomDiffsArray[i].ptDiffs;

    for (d = 0;d < pBomDiffsArray[i].iDiffsCount; d++)
    {
      BNL_rp_create_xml_diff(pDiffs[d], &pDiff);
      BNL_xml_element_add_element(pComp, pDiff);
    }

    BNL_xml_element_add_element(pXmlBomdiffs, pComp);
  }

  BNL_xml_element_add_element(pParent, pXmlBomdiffs);


  BNL_rp_xmlstuc_to_string(pParent, pszXMLdiff);

  fprintf(rplog, "Freeing memory using BNL_rp_free_array_struc in BNL_rp_web_comprep.\n");
  BNL_rp_free_array_struc(iRevs, pStatusDiffsArray);

  fprintf(rplog, "Freeing memory using BNL_rp_free_array_struc in BNL_rp_web_comprep\n");
  BNL_rp_free_array_struc(iRevs, pAttrDiffsArray);

  finish = clock();
  duration = (double)(finish - start) / CLOCKS_PER_SEC;
  fprintf(rplog, "Compliance report creation took:[%2.1f]s\n",duration);

  *pDocument = pParent;

function_exit:

  if (pszConfigFile != NULL) MEM_free(pszConfigFile);
  if (iRevs > 0) MEM_free(ptRevs);
  if (iRevsTemp > 0) MEM_free(ptRevsTemp);
  if (iPrefValues > 0) MEM_free(pszPrefValues);

  fprintf(rplog, "End of BNL_rp_web_comprep.\n\n");

  return iRetCode;
}

int BNL_rp_get_prop_type(tag_t tObject, char *pszAttrName, char *pszType, char **pszValue)
{
  int iRetCode          = 0;
  int iVals             = 0;
  int iValues           = 0;
  int i                 = 0;

  char *pszAttr         = NULL;
  char *pszPropValue    = NULL;

  char **pszValues      = NULL;

  tag_t tProp           = NULL_TAG;

  tag_t *ptValues       = NULL;


  BNL_tb_copy_string(pszAttrName, &pszAttr);

  BNL_split_multiple_values(pszAttr, '.', &iVals, &pszValues);

  if (iVals == 1)
  {
    BNL_tb_determine_object_type(tObject, pszType);

    iRetCode = BNL_tb_get_property(tObject, pszAttrName, &pszPropValue);
    if (BNL_em_error_handler("BNL_tb_get_property", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
  }
  else
  {
    for (i = 0; i < (iVals - 1) && tObject != NULL_TAG; i++)
    {
      iRetCode = PROP_ask_property_by_name(tObject, pszValues[i], &tProp);
      if (BNL_em_error_handler("PROP_ask_property_by_name", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      if (tProp != NULL_TAG)
      {
        iRetCode = PROP_ask_value_tags(tProp, &iValues, &ptValues);
        if (BNL_em_error_handler("BNL_tb_get_property", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
      }

      if (iValues > 0)
      {
        tObject = ptValues[0];
        MEM_free(ptValues);
        ptValues = NULL;
        iValues = 0;
      }
      else
      {
        tObject = NULL_TAG;
      }
    } // End of for (i = 0; i < (iVals - 1); i++)
    if (tObject != NULL_TAG)
    {
      BNL_tb_determine_object_type(tObject, pszType);

      iRetCode = BNL_tb_get_property(tObject, pszValues[iVals - 1], &pszPropValue);
      if (BNL_em_error_handler("BNL_tb_get_property", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
    }
  }

function_exit:

  if (pszPropValue == NULL)
  {
    pszPropValue = (char *)MEM_alloc(1);
    strcpy(pszPropValue, "");
  }

  *pszValue = pszPropValue;

  if (pszAttr != NULL) free(pszAttr);

  for (i = 0; i < iVals; i++)
  {
    free(pszValues[i]);
    pszValues[i] = NULL;
  }
  if(pszValues != NULL) free(pszValues);

  if (iValues > 0)
  {
    MEM_free(ptValues);
  }

  return iRetCode;
}

int BNL_rp_wso_status_check(tag_t tRev, t_XMLElement *pXml, int *piStatusCount, diffinfo_t **pStatusDiffs)
{
  int iRetCode          = 0;
  int iCount            = 0;
  int iTypes            = 0;
  int iStats            = 0;
  int i                 = 0;

  char szType[WSO_name_size_c + 1];

  char *pszTypeTemp     = NULL;
  char *pszStatusTemp   = NULL;
  char *pszStatus       = NULL;
  char *pszAllowedVal   = NULL;

  char **pszTypes       = NULL;
  char **pszStats       = NULL;

  diffinfo_t *pDiffs    = NULL;

  t_XMLElement *pType   = NULL;
  t_XMLElement *pStatus = NULL;
  t_XMLElement *pProps  = NULL;


  iCount = *piStatusCount;
  pDiffs = *pStatusDiffs;

  pType = BNL_xml_find_first(pXml, "type");
  pStatus = BNL_xml_find_first(pXml, "status");

  if (pType == NULL || pStatus == NULL)
  {
    fprintf(rplog, "%s: Warning, type and/or status were not found in configuration file.\n", BNL_module);
  }
  else
  {
    fprintf(rplog, "%s: type = [%s], status = [%s].\n", BNL_module, pType->pszElementValue, pStatus->pszElementValue);

    BNL_tb_copy_string(pType->pszElementValue, &pszTypeTemp);
    BNL_split_multiple_values(pszTypeTemp, ';', &iTypes, &pszTypes);

    BNL_tb_copy_string(pStatus->pszElementValue, &pszStatusTemp);
    BNL_split_multiple_values(pszStatusTemp, ';', &iStats, &pszStats);

    pszAllowedVal = BNL_rp_allowed_vals(iStats, pszStats);

    if ((iRetCode = BNL_tb_determine_object_type(tRev, szType)) != 0) goto function_exit;

    fprintf(rplog, "%s: Object is of type [%s].\n", BNL_module, szType);

    // Check the type
    if (BNL_tb_value_in_list(szType, iTypes, pszTypes) || strcmp(pType->pszElementValue, "*") == 0)
    {
      fprintf(rplog, "%s: Type [%s] matches, now check the status.\n", BNL_module, szType);

      if ((iRetCode = BNL_tb_get_property(tRev, "last_release_status", &pszStatus)) != 0) goto function_exit;

      fprintf(rplog, "%s: Object has status [%s].\n", BNL_module, pszStatus);

      if (!BNL_tb_value_in_list(pszStatus, iStats, pszStats))
      {
        fprintf(rplog, "%s: Error, status doesn't match.\n", BNL_module);

        iCount++;
        pDiffs = (diffinfo_t *) MEM_realloc(pDiffs, iCount * sizeof(diffinfo_t));

        BNL_rp_create_xml_props(tRev, "status_comprep", 0, &pProps, NULL, NULL, NULL, pszAllowedVal,NULL);

        pDiffs[iCount - 1].tObject = tRev;
        pDiffs[iCount - 1].pszQuant = BNL_strings_copy_string("");

        pDiffs[iCount - 1].pszSeq = BNL_strings_copy_string("");
        pDiffs[iCount - 1].pszRefDes = BNL_strings_copy_string("");
        pDiffs[iCount - 1].pXMLprops = pProps;
        pDiffs[iCount - 1].iChange = 0;

        pDiffs[iCount - 1].pNext = NULL;
        pDiffs[iCount - 1].pPrev = NULL;
      } // End of if (!BNL_tb_value_in_list(pszStatus, iStats, pszStats))
    } // End of if (BNL_tb_value_in_list(szType, iTypes, pszTypes))
  } // End of if (pType == NULL || pStatus == NULL)

function_exit:

  // Free memory
  for (i = 0; i < iTypes; i++)
  {
    free(pszTypes[i]);
  }
  free(pszTypes);

  for (i = 0; i < iStats; i++)
  {
    free(pszStats[i]);
  }
  free(pszStats);

  if (pszAllowedVal != NULL) MEM_free(pszAllowedVal);

  if (pszTypeTemp != NULL) free(pszTypeTemp);

  if (pszStatusTemp != NULL)free(pszStatusTemp);

  if (pszStatus != NULL) MEM_free(pszStatus);

  *piStatusCount = iCount;
  *pStatusDiffs = pDiffs;

  return iRetCode;
}

int BNL_rp_rel_status_check(tag_t tRev, t_XMLElement *pXml, int *piStatusCount, diffinfo_t **pStatusDiffs)
{
  int iRetCode          = 0;
  int iCount            = 0;
  int iTypes            = 0;
  int iStats            = 0;
  int i                 = 0;
  int iRelCount         = 0;

  char szType[WSO_name_size_c + 1];
  char szRelType[WSO_name_size_c + 1];

  char *pszTypeTemp     = NULL;
  char *pszStatusTemp   = NULL;
  char *pszStatus       = NULL;
  char *pszAllowedVal   = NULL;

  char **pszTypes       = NULL;
  char **pszStats       = NULL;

  tag_t tRelType        = NULL_TAG;

  tag_t *ptRelObjects   = NULL;

  diffinfo_t *pDiffs    = NULL;

  t_XMLElement *pType   = NULL;
  t_XMLElement *pStatus = NULL;
  t_XMLElement *pRelation= NULL;
  t_XMLElement *pProps  = NULL;


  iCount = *piStatusCount;
  pDiffs = *pStatusDiffs;

  pType = BNL_xml_find_first(pXml, "type");
  pStatus = BNL_xml_find_first(pXml, "status");
  pRelation = BNL_xml_find_first(pXml, "relation");


  if (pType == NULL || pStatus == NULL || pRelation == NULL)
  {
    fprintf(rplog, "%s: Warning, type, status and/or relation were not found in configuration file.\n", BNL_module);
  }
  else
  {
    fprintf(rplog, "%s: type = [%s], status = [%s], relation = [%s].\n", BNL_module, pType->pszElementValue, pStatus->pszElementValue, pRelation->pszElementValue);

    BNL_tb_copy_string(pType->pszElementValue, &pszTypeTemp);
    BNL_split_multiple_values(pszTypeTemp, ';', &iTypes, &pszTypes);

    BNL_tb_copy_string(pStatus->pszElementValue, &pszStatusTemp);
    BNL_split_multiple_values(pszStatusTemp, ';', &iStats, &pszStats);

    pszAllowedVal = BNL_rp_allowed_vals(iStats, pszStats);

    if ((iRetCode = BNL_tb_determine_object_type(tRev, szType)) != 0) goto function_exit;

    fprintf(rplog, "%s: Object is of type [%s].\n", BNL_module, szType);

    // Check the type
    if (BNL_tb_value_in_list(szType, iTypes, pszTypes) || strcmp(pType->pszElementValue, "*") == 0)
    {
      fprintf(rplog, "%s: Type [%s] matches, now get related objects.\n", BNL_module, szType);

      iRetCode = GRM_find_relation_type(pRelation->pszElementValue, &tRelType);
      if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      if (tRelType != NULL_TAG)
      {
        iRetCode = GRM_list_secondary_objects_only(tRev, tRelType, &iRelCount, &ptRelObjects);
        if (BNL_em_error_handler("GRM_list_secondary_objects_only", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        fprintf(rplog, "%s: Related objects [%d].\n", BNL_module, iRelCount);

        for (i = 0; i < iRelCount ; i++)
        {
          if ((iRetCode = BNL_tb_determine_object_type(ptRelObjects[i], szRelType)) != 0) goto function_exit;

          fprintf(rplog, "%s: Related object is of type [%s].\n", BNL_module, szRelType);

          if (BNL_tb_value_in_list(szRelType, iTypes, pszTypes))
          {
            fprintf(rplog, "%s: Type [%s] matches, now check the status.\n", BNL_module, szRelType);

            if ((iRetCode = BNL_tb_get_property(ptRelObjects[i], "last_release_status", &pszStatus)) != 0) goto function_exit;

            fprintf(rplog, "%s: Object has status [%s].\n", BNL_module, pszStatus);

            if (!BNL_tb_value_in_list(pszStatus, iStats, pszStats))
            {
              fprintf(rplog, "%s: Error, status doesn't match.\n", BNL_module);

              iCount++;
              pDiffs = (diffinfo_t *) MEM_realloc(pDiffs, iCount * sizeof(diffinfo_t));

              BNL_rp_create_xml_props(ptRelObjects[i], "status_comprep", 0, &pProps, NULL, NULL, NULL, pszAllowedVal,NULL);

              pDiffs[iCount - 1].tObject = ptRelObjects[i];
              pDiffs[iCount - 1].pszQuant = BNL_strings_copy_string("");

              pDiffs[iCount - 1].pszSeq = BNL_strings_copy_string("");
              pDiffs[iCount - 1].pszRefDes = BNL_strings_copy_string("");
              pDiffs[iCount - 1].pXMLprops = pProps;
              pDiffs[iCount - 1].iChange = 0;

              pDiffs[iCount - 1].pNext = NULL;
              pDiffs[iCount - 1].pPrev = NULL;
            } // End of if (!BNL_tb_value_in_list(pszStatus, iStats, pszStats))
            if (pszStatus != NULL)
            {
              MEM_free(pszStatus);
              pszStatus = NULL;
            }
          } // End of if (BNL_tb_value_in_list(szRelType, iTypes, pszTypes))
        } // End of for (i = 0; i < iRelCount ; i++)
      } // End of if (tRelType != NULL_TAG)
    } // End of if (BNL_tb_value_in_list(szType, iTypes, pszTypes))
  } // End of if (pType == NULL || pStatus == NULL || pRelation == NULL)

function_exit:

  // Free memory
  if (iRelCount > 0) MEM_free(ptRelObjects);

  for (i = 0; i < iTypes; i++)
  {
    free(pszTypes[i]);
  }
  free(pszTypes);

  for (i = 0; i < iStats; i++)
  {
    free(pszStats[i]);
  }
  free(pszStats);

  if (pszAllowedVal) MEM_free(pszAllowedVal);

  if (pszTypeTemp != NULL) free(pszTypeTemp);

  if (pszStatusTemp != NULL) free(pszStatusTemp);

  if (pszStatus != NULL) MEM_free(pszStatus);

  *piStatusCount = iCount;
  *pStatusDiffs = pDiffs;

  return iRetCode;
}

int BNL_rp_bvr_status_check(tag_t tRev, t_XMLElement *pXml, int iTargets, tag_t *ptTargets, int *piStatusCount, diffinfo_t **pStatusDiffs)
{
  int iRetCode          = 0;
  int iCount            = 0;
  int iTypes            = 0;
  int iStats            = 0;
  int i                 = 0;
  int iBomCount         = 0;
  int iItemRevCount     = 0;
  int t                 = 0;

  char szType[WSO_name_size_c + 1];
  char szBomType[WSO_name_size_c + 1];
  char szBvrType[WSO_name_size_c + 1];

  char *pszTypeTemp     = NULL;
  char *pszStatusTemp   = NULL;
  char *pszStatus       = NULL;
  char *pszAllowedVal   = NULL;

  char **pszTypes       = NULL;
  char **pszStats       = NULL;

  tag_t tItem           = NULL_TAG;
  tag_t tBomViewType    = NULL_TAG;
  tag_t tBomwindow      = NULL_TAG;
  tag_t tTopLine        = NULL_TAG;

  tag_t *ptItemRevs     = NULL;
  tag_t *ptBomViews     = NULL;

  diffinfo_t *pDiffs    = NULL;

  t_XMLElement *pType   = NULL;
  t_XMLElement *pStatus = NULL;
  t_XMLElement *pRevRule= NULL;
  t_XMLElement *pViewType= NULL;
  t_XMLElement *pLevel  = NULL;
  t_XMLElement *pProps  = NULL;

  logical lTarget       = true;


  iCount = *piStatusCount;
  pDiffs = *pStatusDiffs;

  pType = BNL_xml_find_first(pXml, "type");
  pStatus = BNL_xml_find_first(pXml, "status");
  pRevRule = BNL_xml_find_first(pXml, "revrule");
  pViewType = BNL_xml_find_first(pXml, "viewtype");
  pLevel = BNL_xml_find_first(pXml, "leveldepth");

  if (pType == NULL || pStatus == NULL || pRevRule == NULL || pViewType == NULL || pLevel == NULL)
  {
    fprintf(rplog, "%s: Warning, type, status, revrule, viewtype and/or leveldepth were not found in configuration file.\n", BNL_module);
  }
  else
  {
    fprintf(rplog, "%s: type = [%s], status = [%s], revrule = [%s], viewtype = [%s], level = [%s].\n",
      BNL_module, pType->pszElementValue, pStatus->pszElementValue, pRevRule->pszElementValue, pViewType->pszElementValue, pLevel->pszElementValue);

    BNL_tb_copy_string(pType->pszElementValue, &pszTypeTemp);
    BNL_split_multiple_values(pszTypeTemp, ';', &iTypes, &pszTypes);

    BNL_tb_copy_string(pStatus->pszElementValue, &pszStatusTemp);
    BNL_split_multiple_values(pszStatusTemp, ';', &iStats, &pszStats);

    pszAllowedVal = BNL_rp_allowed_vals(iStats, pszStats);

    if ((iRetCode = BNL_tb_determine_object_type(tRev, szType)) != 0) goto function_exit;

    fprintf(rplog, "%s: Object is of type [%s].\n", BNL_module, szType);

    // Check the type
    if (BNL_tb_value_in_list(szType, iTypes, pszTypes) || strcmp(pType->pszElementValue, "*") == 0)
    {
      fprintf(rplog, "%s: Type [%s] matches, now get bom.\n", BNL_module, szType);

      // Get the Item, get the bomviews
      iRetCode = ITEM_ask_item_of_rev(tRev, &tItem);
      if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      iRetCode = ITEM_list_bom_views(tItem, &iBomCount, &ptBomViews);
      if (BNL_em_error_handler("ITEM_list_bom_views", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      fprintf(rplog, "%s: Found [%d] bomviews at target.\n", BNL_module, iBomCount);

      /* check for the bomview type*/
      for (i = 0;i < iBomCount; i++)
      {
        if ((iRetCode = BNL_tb_determine_object_type(ptBomViews[i], szBomType)) != 0) goto function_exit;

        fprintf(rplog, "%s: Found view type [%s].\n", BNL_module, szBomType);

        if (strcmp(pViewType->pszElementValue, szBomType)== 0)
        {
          tBomViewType = ptBomViews[i];

          break;
        }
      } // for (i = 0;i < iBomCount; i++)

      if (tBomViewType != NULL_TAG)
      {
        fprintf(rplog, "%s: Processing view of type [%s].\n", BNL_module, szBomType);

        iRetCode = BOM_create_window(&tBomwindow);
        if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = BNL_tb_bom_apply_config_rule(tBomwindow, pRevRule->pszElementValue);
        if (BNL_em_error_handler("BNL_tb_bom_apply_config_rule", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = BOM_set_window_top_line(tBomwindow, NULL_TAG, tRev, tBomViewType, &tTopLine);
        if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = BNL_tb_bom_get_item_revs(tTopLine, 0, 0, atoi(pLevel->pszElementValue), &iItemRevCount, &ptItemRevs);
        if (BNL_em_error_handler("BNL_tb_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = BOM_close_window(tBomwindow);
        if (BNL_em_error_handler("BOM_close_window", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        for (i = 0; i < iItemRevCount ; i++)
        {
          if ((iRetCode = BNL_tb_determine_object_type(ptItemRevs[i], szBvrType)) != 0) goto function_exit;

          fprintf(rplog, "%s: Occurence is of type [%s].\n", BNL_module, szType);

          if (BNL_tb_value_in_list(szBvrType, iTypes, pszTypes))
          {
            fprintf(rplog, "%s: Type [%s] matches, now check the status.\n", BNL_module, szBvrType);

            if ((iRetCode = BNL_tb_get_property(ptItemRevs[i], "last_release_status", &pszStatus)) != 0) goto function_exit;

            fprintf(rplog, "%s: Object has status [%s].\n", BNL_module, pszStatus);

            if (!BNL_tb_value_in_list(pszStatus, iStats, pszStats))
            {
              fprintf(rplog, "%s: Error, status doesn't match.\n", BNL_module);

              // Could be object is one of the targets
              lTarget = false;
              for (t = 0; t < iTargets; t++)
              {
                if (ptItemRevs[i] == ptTargets[t])
                {
                  fprintf(rplog, "%s: Ignoring error, since object is target or attached to incoming change.\n", BNL_module);
                  lTarget = true;
                }
              }

              if (!lTarget)
              {
                iCount++;
                pDiffs = (diffinfo_t *) MEM_realloc(pDiffs, iCount * sizeof(diffinfo_t));

                BNL_rp_create_xml_props(ptItemRevs[i], "status_comprep", 0, &pProps, NULL, NULL, NULL, pszAllowedVal,NULL);

                pDiffs[iCount - 1].tObject = ptItemRevs[i];
                pDiffs[iCount - 1].pszQuant = BNL_strings_copy_string("");

                pDiffs[iCount - 1].pszSeq = BNL_strings_copy_string("");
                pDiffs[iCount - 1].pszRefDes = BNL_strings_copy_string("");
                pDiffs[iCount - 1].pXMLprops = pProps;
                pDiffs[iCount - 1].iChange = 0;

                pDiffs[iCount - 1].pNext = NULL;
                pDiffs[iCount - 1].pPrev = NULL;
              }// End of if (!lTarget)
            } // End of if (!BNL_tb_value_in_list(pszStatus, iStats, pszStats))

            if (pszStatus != NULL)
            {
              MEM_free(pszStatus);
              pszStatus = NULL;
            }
          } // End of if (BNL_tb_value_in_list(szBvrType, iTypes, pszTypes))
        } // End of for (i = 0; i < iItemRevCount ; i++)
      } // End of if (tBomViewType != NULL_TAG)
      else
      {
       fprintf(rplog, "%s: Viewtype [%s] not found.\n", BNL_module, pViewType->pszElementValue);
      }
    } // End of if (BNL_tb_value_in_list(szType, iTypes, pszTypes))
  } // End of if (pType == NULL || pStatus == NULL || pRelation == NULL)

function_exit:

  // Free memory
  if (iItemRevCount > 0) MEM_free(ptItemRevs);

  if (iBomCount > 0) MEM_free(ptBomViews);

  for (i = 0; i < iTypes; i++)
  {
    free(pszTypes[i]);
  }
  free(pszTypes);

  for (i = 0; i < iStats; i++)
  {
    free(pszStats[i]);
  }
  free(pszStats);

  if (pszAllowedVal != NULL) MEM_free(pszAllowedVal);

  if (pszTypeTemp != NULL) free(pszTypeTemp);

  if (pszStatusTemp != NULL) free(pszStatusTemp);

  if (pszStatus != NULL)MEM_free(pszStatus);

  *piStatusCount = iCount;
  *pStatusDiffs = pDiffs;

  return iRetCode;
}

int BNL_rp_attr_1_check(tag_t tRev, t_XMLElement *pXml, int *piAttrCount, diffinfo_t **pAttrDiffs)
{
  int iRetCode          = 0;
  int iCount            = 0;
  int iTypes            = 0;
  int iVals             = 0;
  int i                 = 0;

  char szType[WSO_name_size_c + 1];
  char szObjType[WSO_name_size_c + 1];

  char *pszTypeTemp     = NULL;
  char *pszValuesTemp   = NULL;
  char *pszAttrValue    = NULL;
  char *pszAllowedVal   = NULL;

  char **pszTypes       = NULL;
  char **pszVals        = NULL;

  diffinfo_t *pDiffs    = NULL;

  t_XMLElement *pType   = NULL;
  t_XMLElement *pName   = NULL;
  t_XMLElement *pDispName= NULL;
  t_XMLElement *pValue  = NULL;
  t_XMLElement *pProps  = NULL;


  iCount = *piAttrCount;
  pDiffs = *pAttrDiffs;

  pType = BNL_xml_find_first(pXml, "type");
  pName = BNL_xml_find_first(pXml, "name");
  pDispName = BNL_xml_find_first(pXml, "disp_name");
  pValue = BNL_xml_find_first(pXml, "value");

  if (pType == NULL || pName == NULL || pDispName == NULL || pValue == NULL)
  {
    fprintf(rplog, "%s: Warning, type, name, disp_name and/or value were not found in configuration file.\n", BNL_module);
  }
  else
  {
    fprintf(rplog, "%s: type = [%s], name = [%s], disp_name = [%s], value = [%s].\n",
      BNL_module, pType->pszElementValue, pName->pszElementValue, pDispName->pszElementValue, pValue->pszElementValue);

    BNL_tb_copy_string(pType->pszElementValue, &pszTypeTemp);
    BNL_split_multiple_values(pszTypeTemp, ';', &iTypes, &pszTypes);

    BNL_tb_copy_string(pValue->pszElementValue, &pszValuesTemp);
    BNL_split_multiple_values(pszValuesTemp, ';', &iVals, &pszVals);

    pszAllowedVal = BNL_rp_allowed_vals(iVals, pszVals);

    if ((iRetCode = BNL_tb_determine_object_type(tRev, szType)) != 0) goto function_exit;

    fprintf(rplog, "%s: Object is of type [%s].\n", BNL_module, szType);

    // Check the type
    if (BNL_tb_value_in_list(szType, iTypes, pszTypes) || strcmp(pType->pszElementValue, "*") == 0)
    {
      fprintf(rplog, "%s: Type [%s] matches, now check the property.\n", BNL_module, szType);

      if ((iRetCode = BNL_rp_get_prop_type(tRev, pName->pszElementValue, szObjType, &pszAttrValue)) != 0) goto function_exit;

      fprintf(rplog, "%s: Property has value [%s].\n", BNL_module, pszAttrValue);

      if ((strcmp(pValue->pszElementValue, "IS_NOT_NULL") == 0 && strlen(pszAttrValue) == 0)
        || (strcmp(pValue->pszElementValue, "IS_NOT_NULL") != 0 && !BNL_tb_value_in_list(pszAttrValue, iVals, pszVals)))
      {
        fprintf(rplog, "%s: Error, property doesn't match.\n", BNL_module);

        iCount++;
        pDiffs = (diffinfo_t *) MEM_realloc(pDiffs, iCount * sizeof(diffinfo_t));

        BNL_rp_create_xml_props(tRev, "attr_comprep", 0, &pProps, szObjType, pDispName->pszElementValue, pszAttrValue, pszAllowedVal,NULL);

        pDiffs[iCount - 1].tObject = tRev;
        pDiffs[iCount - 1].pszQuant = BNL_strings_copy_string("");

        pDiffs[iCount - 1].pszSeq = BNL_strings_copy_string("");
        pDiffs[iCount - 1].pszRefDes = BNL_strings_copy_string("");
        pDiffs[iCount - 1].pXMLprops = pProps;
        pDiffs[iCount - 1].iChange = 0;

        pDiffs[iCount - 1].pNext = NULL;
        pDiffs[iCount - 1].pPrev = NULL;
      } // End of if (!BNL_tb_value_in_list(pszStatus, iVals, pszValss))
    } // End of if (BNL_tb_value_in_list(szType, iTypes, pszTypes))
  } // End of if (pType == NULL || pName == NULL || pDispName == NULL || pValue == NULL)

function_exit:

  // Free memory
  for (i = 0; i < iTypes; i++)
  {
    free(pszTypes[i]);
  }
  free(pszTypes);

  for (i = 0; i < iVals; i++)
  {
    free(pszVals[i]);
  }
  free(pszVals);

  if (pszAllowedVal != NULL) MEM_free(pszAllowedVal);

  if (pszTypeTemp != NULL) free(pszTypeTemp);

  if (pszValuesTemp != NULL) free(pszValuesTemp);

  if (pszAttrValue != NULL) MEM_free(pszAttrValue);

  *piAttrCount = iCount;
  *pAttrDiffs = pDiffs;

  return iRetCode;
}

int BNL_rp_attr_2_check(tag_t tRev, t_XMLElement *pXml, int *piAttrCount, diffinfo_t **pAttrDiffs)
{
  int iRetCode          = 0;
  int iCount            = 0;
  int iTypes            = 0;
  int iVals             = 0;
  int iVals1            = 0;
  int i                 = 0;

  char szType[WSO_name_size_c + 1];
  char szObjType[WSO_name_size_c + 1];

  char *pszTypeTemp     = NULL;
  char *pszValuesTemp1  = NULL;
  char *pszValuesTemp   = NULL;
  char *pszAttrValue    = NULL;
  char *pszAttrValue1   = NULL;
  char *pszAllowedVal   = NULL;

  char **pszTypes       = NULL;
  char **pszVals        = NULL;
  char **pszVals1       = NULL;

  diffinfo_t *pDiffs    = NULL;

  t_XMLElement *pType   = NULL;
  t_XMLElement *pName   = NULL;
  t_XMLElement *pName1  = NULL;
  t_XMLElement *pDispName= NULL;
  t_XMLElement *pValue  = NULL;
  t_XMLElement *pValue1 = NULL;
  t_XMLElement *pProps  = NULL;
  t_XMLElement *pAttr   = NULL;


  iCount = *piAttrCount;
  pDiffs = *pAttrDiffs;

  pType = BNL_xml_find_first(pXml, "type");
  pName1 = BNL_xml_find_first(pXml, "name");
  pValue1 = BNL_xml_find_first(pXml, "value");

  if (pType == NULL || pName1 == NULL || pValue1 == NULL)
  {
    fprintf(rplog, "%s: Warning, type, name and/or value were not found in configuration file.\n", BNL_module);
  }
  else
  {
    fprintf(rplog, "%s: type = [%s], name = [%s], value = [%s].\n",
      BNL_module, pType->pszElementValue, pName1->pszElementValue, pValue1->pszElementValue);

    BNL_tb_copy_string(pType->pszElementValue, &pszTypeTemp);
    BNL_split_multiple_values(pszTypeTemp, ';', &iTypes, &pszTypes);

    BNL_tb_copy_string(pValue1->pszElementValue, &pszValuesTemp1);
    BNL_split_multiple_values(pszValuesTemp1, ';', &iVals1, &pszVals1);

    if ((iRetCode = BNL_tb_determine_object_type(tRev, szType)) != 0) goto function_exit;

    fprintf(rplog, "%s: Object is of type [%s].\n", BNL_module, szType);

    // Check the type
    if (BNL_tb_value_in_list(szType, iTypes, pszTypes) || strcmp(pType->pszElementValue, "*") == 0)
    {
      fprintf(rplog, "%s: Type [%s] matches, now check the property.\n", BNL_module, szType);

      if ((iRetCode = BNL_rp_get_prop_type(tRev, pName1->pszElementValue, szObjType, &pszAttrValue1)) != 0) goto function_exit;

      fprintf(rplog, "%s: Property has value [%s].\n", BNL_module, pszAttrValue1);

      //if (BNL_tb_value_in_list(pszAttrValue1, iVals1, pszVals1))
      if ((strcmp(pValue1->pszElementValue, "IS_NOT_NULL") == 0 && strlen(pszAttrValue1) > 0)
        || (strcmp(pValue1->pszElementValue, "IS_NOT_NULL") != 0 && BNL_tb_value_in_list(pszAttrValue1, iVals1, pszVals1)))
      {
        fprintf(rplog, "%s: Property values matches [%s].\n", BNL_module, pszAttrValue1);

        pAttr = BNL_xml_find_first(pXml, "attr");
        while (pAttr != NULL)
        {
          pName = BNL_xml_find_first(pAttr, "name");
          pDispName = BNL_xml_find_first(pAttr, "disp_name");
          pValue = BNL_xml_find_first(pAttr, "value");

          if (pName == NULL || pDispName == NULL || pValue == NULL)
          {
            fprintf(rplog, "%s: Warning, name, disp_name and/or value were not found in configuration file.\n", BNL_module);
          }
          else
          {
            fprintf(rplog, "%s: name = [%s], disp_name = [%s], value = [%s].\n",
              BNL_module, pName->pszElementValue, pDispName->pszElementValue, pValue->pszElementValue);

            BNL_tb_copy_string(pValue->pszElementValue, &pszValuesTemp);
            BNL_split_multiple_values(pszValuesTemp, ';', &iVals, &pszVals);

            pszAllowedVal = BNL_rp_allowed_vals(iVals, pszVals);

            // Get the property
            fprintf(rplog, "%s: Check the property.\n", BNL_module);

            if ((iRetCode = BNL_rp_get_prop_type(tRev, pName->pszElementValue, szObjType, &pszAttrValue)) != 0) goto function_exit;

            fprintf(rplog, "%s: Property has value [%s].\n", BNL_module, pszAttrValue);

            if ((strcmp(pValue->pszElementValue, "IS_NOT_NULL") == 0 && strlen(pszAttrValue) == 0)
              || (strcmp(pValue->pszElementValue, "IS_NOT_NULL") != 0 && !BNL_tb_value_in_list(pszAttrValue, iVals, pszVals)))
            {
              fprintf(rplog, "%s: Error, property doesn't match.\n", BNL_module);

              iCount++;
              pDiffs = (diffinfo_t *) MEM_realloc(pDiffs, iCount * sizeof(diffinfo_t));

              BNL_rp_create_xml_props(tRev, "attr_comprep", 0, &pProps, szObjType, pDispName->pszElementValue, pszAttrValue, pszAllowedVal,NULL);

              pDiffs[iCount - 1].tObject = tRev;
              pDiffs[iCount - 1].pszQuant = BNL_strings_copy_string("");

              pDiffs[iCount - 1].pszSeq = BNL_strings_copy_string("");
              pDiffs[iCount - 1].pszRefDes = BNL_strings_copy_string("");
              pDiffs[iCount - 1].pXMLprops = pProps;
              pDiffs[iCount - 1].iChange = 0;

              pDiffs[iCount - 1].pNext = NULL;
              pDiffs[iCount - 1].pPrev = NULL;
            } // End of if ((strcmp(pValue->pszElementValue, "IS_NOT_NULL") == 0 && strlen(pszAttrValue) == 0) || (!BNL_tb_value_in_list(pszAttrValue, iVals, pszVals)))
            for (i = 0; i < iVals; i++)
            {
              free(pszVals[i]);
            }
            free(pszVals);
            pszVals = NULL;
            iVals = 0;

            if (pszAllowedVal != NULL)
            {
              MEM_free(pszAllowedVal);
              pszAllowedVal = NULL;
            }

            if (pszAttrValue != NULL)
            {
              MEM_free(pszAttrValue);
              pszAttrValue = NULL;
            }
          } // End of if (pName == NULL || pDispName == NULL || pValue == NULL)
          pAttr = pAttr->pNextSibling;
        } // End of while (pAttr != NULL)
      } // End of if ((strcmp(pValue1->pszElementValue, "IS_NOT_NULL") == 0 && strlen(pszAttrValue1) > 0) || (strcmp(pValue1->pszElementValue, "IS_NOT_NULL") != 0 && BNL_tb_value_in_list(pszAttrValue1, iVals1, pszVals1)))
    } // End of if (BNL_tb_value_in_list(szType, iTypes, pszTypes))
  } // End of if (pType == NULL || pName == NULL || pDispName == NULL || pValue == NULL)

function_exit:

  // Free memory
  for (i = 0; i < iTypes; i++)
  {
    free(pszTypes[i]);
  }
  free(pszTypes);

  for (i = 0; i < iVals; i++)
  {
    free(pszVals[i]);
  }
  free(pszVals);

  if (pszAllowedVal != NULL) MEM_free(pszAllowedVal);

  for (i = 0; i < iVals1; i++)
  {
    free(pszVals1[i]);
  }
  free(pszVals1);

  if (pszTypeTemp != NULL) free(pszTypeTemp);

  if (pszValuesTemp != NULL) free(pszValuesTemp);

  if (pszValuesTemp1 != NULL) free(pszValuesTemp1);

  if (pszAttrValue != NULL) MEM_free(pszAttrValue);

  if (pszAttrValue1 != NULL) MEM_free(pszAttrValue1);

  *piAttrCount = iCount;
  *pAttrDiffs = pDiffs;

  return iRetCode;
}

int BNL_rp_attr_3_check(tag_t tRev, t_XMLElement *pXml, int *piAttrCount, diffinfo_t **pAttrDiffs)
{
  int iRetCode          = 0;
  int iCount            = 0;
  int iTypes            = 0;
  int iVals             = 0;
  int i                 = 0;
  int iBomCount         = 0;
  int iItemRevCount     = 0;

  tag_t tItem           = NULL_TAG;
  tag_t tBomViewType    = NULL_TAG;
  tag_t tBomwindow      = NULL_TAG;
  tag_t tTopLine        = NULL_TAG;

  char szType[WSO_name_size_c + 1];
  char szObjType[WSO_name_size_c + 1];
  char szBomType[WSO_name_size_c + 1];
  char szBvrType[WSO_name_size_c + 1];

  char *pszTypeTemp     = NULL;
  char *pszValuesTemp   = NULL;
  char *pszAttrValue    = NULL;
  char *pszAttrChild    = NULL;
  char *pszAllowedVal   = NULL;

  char **pszTypes       = NULL;
  char **pszVals        = NULL;

  diffinfo_t *pDiffs    = NULL;

  t_XMLElement *pType   = NULL;
  t_XMLElement *pName   = NULL;
  t_XMLElement *pRevRule= NULL;
  t_XMLElement *pDispName= NULL;
  t_XMLElement *pValue  = NULL;
  t_XMLElement *pViewType= NULL;
  t_XMLElement *pLevel  = NULL;
  t_XMLElement *pIgnoreRemote= NULL;
  t_XMLElement *pProps  = NULL;
  t_XMLElement *pAttribute= NULL;

  tag_t *ptItemRevs     = NULL;
  tag_t *ptBomViews     = NULL;

  WSO_description_t tDescription;

  iCount = *piAttrCount;
  pDiffs = *pAttrDiffs;

  pRevRule = BNL_xml_find_first(pXml, "revrule");
  pViewType = BNL_xml_find_first(pXml, "viewtype");
  pLevel = BNL_xml_find_first(pXml, "leveldepth");

  if (pRevRule == NULL || pViewType == NULL || pLevel == NULL)
  {
    fprintf(rplog, "%s: Warning, revrule, viewtype and/or leveldepth were not found in configuration file.\n", BNL_module);
  }
  else
  {
    fprintf(rplog, "%s: revrule = [%s], viewtype = [%s], level = [%s].\n",
      BNL_module, pRevRule->pszElementValue, pViewType->pszElementValue, pLevel->pszElementValue);

    pAttribute = BNL_xml_find_first(pXml, XML_EL_ATTR);

    while (pAttribute != NULL)
    {
      pType = BNL_xml_find_first(pAttribute, "type");
      pName = BNL_xml_find_first(pAttribute, "name");
      pDispName = BNL_xml_find_first(pAttribute, "disp_name");
      pValue = BNL_xml_find_first(pAttribute, "value");
      pIgnoreRemote = BNL_xml_find_first(pAttribute, "ignore_remote_items");

      if (pType == NULL || pName == NULL || pDispName == NULL || pValue == NULL || pIgnoreRemote == NULL)
      {
        fprintf(rplog, "%s: Warning, type, name, disp_name, value and/or ignore_remote_items were not found in configuration file.\n", BNL_module);
      }
      else
      {
        fprintf(rplog, "%s: type = [%s], name = [%s], disp_name = [%s], value = [%s], ignore_remote_items = [%s].\n",
          BNL_module, pType->pszElementValue, pName->pszElementValue, pDispName->pszElementValue, pValue->pszElementValue, pIgnoreRemote->pszElementValue);

        BNL_tb_copy_string(pType->pszElementValue, &pszTypeTemp);
        BNL_split_multiple_values(pszTypeTemp, ';', &iTypes, &pszTypes);

        BNL_tb_copy_string(pValue->pszElementValue, &pszValuesTemp);
        BNL_split_multiple_values(pszValuesTemp, ';', &iVals, &pszVals);

        pszAllowedVal = BNL_rp_allowed_vals(iVals, pszVals);

        if ((iRetCode = BNL_tb_determine_object_type(tRev, szType)) != 0) goto function_exit;

        fprintf(rplog, "%s: Object is of type [%s].\n", BNL_module, szType);

        if (BNL_tb_value_in_list(szType, iTypes, pszTypes) || strcmp(pType->pszElementValue, "*") == 0)
        {
          fprintf(rplog, "%s: Type [%s] matches, now check the property on the parent.\n", BNL_module, szType);

          if ((iRetCode = BNL_rp_get_prop_type(tRev, pName->pszElementValue, szObjType, &pszAttrValue)) != 0) goto function_exit;

          fprintf(rplog, "%s: Property has value [%s].\n", BNL_module, pszAttrValue);

          if ((strcmp(pValue->pszElementValue, "IS_NOT_NULL") == 0 && strlen(pszAttrValue) > 0)
              || (strcmp(pValue->pszElementValue, "IS_NOT_NULL") != 0 && BNL_tb_value_in_list(pszAttrValue, iVals, pszVals)))
          {
            fprintf(rplog, "%s: Property values matches [%s], now check its children.\n", BNL_module, pszAttrValue);

            // Get the Item, get the bomviews
            iRetCode = ITEM_ask_item_of_rev(tRev, &tItem);
            if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

            iRetCode = ITEM_list_bom_views(tItem, &iBomCount, &ptBomViews);
            if (BNL_em_error_handler("ITEM_list_bom_views", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

            fprintf(rplog, "%s: Found [%d] bomviews at target.\n", BNL_module, iBomCount);

            /* check for the bomview type*/
            for (i = 0;i < iBomCount; i++)
            {
              if ((iRetCode = BNL_tb_determine_object_type(ptBomViews[i], szBomType)) != 0) goto function_exit;

              fprintf(rplog, "%s: Found view type [%s].\n", BNL_module, szBomType);

              if (strcmp(pViewType->pszElementValue, szBomType)== 0)
              {
                tBomViewType = ptBomViews[i];
                break;
              }
            } // for (i = 0;i < iBomCount; i++)

            if (tBomViewType != NULL_TAG)
            {
              fprintf(rplog, "%s: Processing view of type [%s].\n", BNL_module, szBomType);

              iRetCode = BOM_create_window(&tBomwindow);
              if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              iRetCode = BNL_tb_bom_apply_config_rule(tBomwindow, pRevRule->pszElementValue);
              if (BNL_em_error_handler("BNL_tb_bom_apply_config_rule", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              iRetCode = BOM_set_window_top_line(tBomwindow, NULL_TAG, tRev, tBomViewType, &tTopLine);
              if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              iRetCode = BNL_tb_bom_get_item_revs(tTopLine, 0, 0, atoi(pLevel->pszElementValue), &iItemRevCount, &ptItemRevs);
              if (BNL_em_error_handler("BNL_tb_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              iRetCode = BOM_close_window(tBomwindow);
              if (BNL_em_error_handler("BOM_close_window", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              for (i = 0; i < iItemRevCount ; i++)
              {
                if ((iRetCode = BNL_tb_determine_object_type(ptItemRevs[i], szBvrType)) != 0) goto function_exit;

                fprintf(rplog, "%s: Occurence is of type [%s].\n", BNL_module, szType);

                if (BNL_tb_value_in_list(szBvrType, iTypes, pszTypes) || strcmp(pType->pszElementValue, "*") == 0)
                {
                  fprintf(rplog, "%s: Type [%s] matches.\n", BNL_module, szBvrType);

                  iRetCode = WSOM_describe(ptItemRevs[i], &tDescription);
                  if (BNL_em_error_handler("WSOM_describe", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

                  if (_stricmp(pIgnoreRemote->pszElementValue, "true") == 0
                    && tDescription.owning_site_name[0] != ' ')
                  {
                    fprintf(rplog, "%s: Owning site [%s], skipping...\n", BNL_module, tDescription.owning_site_name);
                  }
                  else
                  {
                    fprintf(rplog, "%s: Now check the property.\n", BNL_module);

                    if ((iRetCode = BNL_rp_get_prop_type(ptItemRevs[i], pName->pszElementValue, szObjType, &pszAttrChild)) != 0) goto function_exit;

                    fprintf(rplog, "%s: Property has value [%s].\n", BNL_module, pszAttrChild);

                    //if (strcmp(pszAttrValue, pszAttrChild) != 0)
                    if ((strcmp(pValue->pszElementValue, "IS_NOT_NULL") == 0 && strlen(pszAttrChild) == 0)
                     || (strcmp(pValue->pszElementValue, "IS_NOT_NULL") != 0 && !BNL_tb_value_in_list(pszAttrChild, iVals, pszVals)))
                    {
                      fprintf(rplog, "%s: Error, property doesn't match.\n", BNL_module);

                      iCount++;
                      pDiffs = (diffinfo_t *) MEM_realloc(pDiffs, iCount * sizeof(diffinfo_t));

                      BNL_rp_create_xml_props(ptItemRevs[i], "attr_comprep", 0, &pProps, szObjType, pDispName->pszElementValue, pszAttrChild, pszAllowedVal,NULL);

                      pDiffs[iCount - 1].tObject = ptItemRevs[i];
                      pDiffs[iCount - 1].pszQuant = BNL_strings_copy_string("");

                      pDiffs[iCount - 1].pszSeq = BNL_strings_copy_string("");
                      pDiffs[iCount - 1].pszRefDes = BNL_strings_copy_string("");
                      pDiffs[iCount - 1].pXMLprops = pProps;
                      pDiffs[iCount - 1].iChange = 0;

                      pDiffs[iCount - 1].pNext = NULL;
                      pDiffs[iCount - 1].pPrev = NULL;
                    } // End of if (strcmp(pszAttrValue, pszAttrChild) != 0)
                    if (pszAttrChild != NULL)
                    {
                      MEM_free(pszAttrChild);
                      pszAttrChild = NULL;
                    }
                  } // End of if (_stricmp(pIgnoreRemote->pszElementValue, "true") == 0 && tDescription.owning_site_name[0] != ' ')
                } // End of if (BNL_tb_value_in_list(szBvrType, iTypes, pszTypes))
              } // End of for (i = 0; i < iItemRevCount ; i++)
            } // End of if (tBomViewType != NULL_TAG)
            else
            {
            fprintf(rplog, "%s: Viewtype [%s] not found.\n", BNL_module, pViewType->pszElementValue);
            }
          } // if ((strcmp(pValue->pszElementValue, "IS_NOT_NULL") == 0 && strlen(pszAttrValue) > 0) || (BNL_tb_value_in_list(pszAttrValue, iVals, pszVals)))
        } // End of if (BNL_tb_value_in_list(szType, iTypes, pszTypes))
      } // End of if (pType == NULL || pName == NULL || pDispName == NULL || pValue == NULL || pIgnoreRemote == NULL)
      pAttribute = pAttribute->pNextSibling;
    } // End of while (pAttribute != NULL)
  } // End of if (pRevRule == NULL || pViewType == NULL || pLevel == NULL)


function_exit:

  // Free memory
  if (iItemRevCount > 0) MEM_free(ptItemRevs);

  if (iBomCount > 0) MEM_free(ptBomViews);

  for (i = 0; i < iTypes; i++)
  {
    free(pszTypes[i]);
  }
  free(pszTypes);

  for (i = 0; i < iVals; i++)
  {
    free(pszVals[i]);
  }
  free(pszVals);

  if (pszAllowedVal) MEM_free(pszAllowedVal);

  if (pszTypeTemp != NULL) free(pszTypeTemp);

  if (pszValuesTemp != NULL) free(pszValuesTemp);

  if (pszAttrValue != NULL) MEM_free(pszAttrValue);

  if (pszAttrChild != NULL) MEM_free(pszAttrChild);

  *piAttrCount = iCount;
  *pAttrDiffs = pDiffs;

  return iRetCode;
}


int BNL_rp_bom_check(tag_t tRev, t_XMLElement *pXml, int *piBomCount, diffinfo_t **pBomDiffs)
{
  int iRetCode          = 0;
  int iCount            = 0;
  int iTypes            = 0;
  int iVals             = 0;
  int ii                = 0;
  int jj                = 0;
  int i                 = 0;
  int iBomCount         = 0;
  //int iItemRevCount     = 0;
  int iBomLineCount     = 0;
  int iAttrRevId        = 0;

  tag_t tItem           = NULL_TAG;
  tag_t tBomViewType    = NULL_TAG;
  tag_t tBomwindow      = NULL_TAG;
  tag_t tTopLine        = NULL_TAG;
  tag_t tItemRev        = NULL_TAG;
  tag_t tItemRevOne     = NULL_TAG;

  char szType[WSO_name_size_c + 1];
  char szObjType[WSO_name_size_c + 1];
  char szBomType[WSO_name_size_c + 1];

  char *pszTypeTemp     = NULL;
  char *pszValuesTemp   = NULL;
  char *pszAttrValue    = NULL;
  char *pszAttrChild    = NULL;
  char *pszAllowedVal   = NULL;
  char *pszError        = NULL;

  char **pszTypes       = NULL;
  char **pszVals        = NULL;

  diffinfo_t *pDiffs    = NULL;

  t_XMLElement *pType   = NULL;
  t_XMLElement *pName   = NULL;
  t_XMLElement *pRevRule= NULL;
  t_XMLElement *pDispName= NULL;
  t_XMLElement *pValue  = NULL;
  t_XMLElement *pViewType= NULL;
  t_XMLElement *pLevel  = NULL;
  t_XMLElement *pIgnoreRemote= NULL;
  t_XMLElement *pProps  = NULL;
  t_XMLElement *pAttr   = NULL;

  t_XMLElement *pBomcheck= NULL;
  t_XMLElement *pParent  = NULL;
  t_XMLElement *pChild   = NULL;
  t_XMLElement *pChilds  = NULL;
  t_XMLElement *pOneChild= NULL;

  //tag_t *ptItemRevs     = NULL;
  tag_t *ptBomViews     = NULL;
  tag_t *ptBomLines     = NULL;

  logical lParentMatch  = false;
  logical lChildIsAllowed= false;


  iCount = *piBomCount;
  pDiffs = *pBomDiffs;

  pRevRule = BNL_xml_find_first(pXml, "revrule");
  pViewType = BNL_xml_find_first(pXml, "viewtype");
  pLevel = BNL_xml_find_first(pXml, "leveldepth");

  if (pRevRule == NULL || pViewType == NULL || pLevel == NULL)
  {
    fprintf(rplog, "%s: Warning, revrule, viewtype and/or leveldepth were not found in configuration file.\n", BNL_module);
  }
  else
  {
    fprintf(rplog, "%s: revrule = [%s], viewtype = [%s], level = [%s].\n",
      BNL_module, pRevRule->pszElementValue, pViewType->pszElementValue, pLevel->pszElementValue);

    pBomcheck = BNL_xml_find_first(pXml, XML_EL_BOMCHECK);

    while (pBomcheck != NULL)
    {
      lParentMatch = false;
      // First check the parent
      pParent = BNL_xml_find_first(pBomcheck, XML_EL_PARENT);

      if (pParent != NULL)
      {
        // Get the type
        pType = BNL_xml_find_first(pParent, "type");

        if (pType == NULL)
        {
          fprintf(rplog, "%s: Warning, parent type not found in configuration file.\n", BNL_module);
        }
        else
        {
          fprintf(rplog, "%s: parent type = [%s].\n", BNL_module, pType->pszElementValue);

          BNL_tb_copy_string(pType->pszElementValue, &pszTypeTemp);
          BNL_split_multiple_values(pszTypeTemp, ';', &iTypes, &pszTypes);

          if ((iRetCode = BNL_tb_determine_object_type(tRev, szType)) != 0) goto function_exit;

          fprintf(rplog, "%s: Object is of type [%s].\n", BNL_module, szType);

          if (BNL_tb_value_in_list(szType, iTypes, pszTypes) || strcmp(pType->pszElementValue, "*") == 0)
          {
            lParentMatch = true;
            fprintf(rplog, "%s: Type [%s] matches, now check the attributes on the parent.\n", BNL_module, szType);

            pAttr = BNL_xml_find_first(pParent, "attr");

            while (pAttr != NULL && strcmp(pAttr->pszElementname, "attr") == 0)
            {
              pName = BNL_xml_find_first(pAttr, "name");
              pValue = BNL_xml_find_first(pAttr, "value");

              if (pName == NULL || pValue == NULL)
              {
                fprintf(rplog, "%s: Warning, name and/or value were not found in configuration file.\n", BNL_module);
              }
              else
              {
                fprintf(rplog, "%s: name = [%s], value = [%s].\n", BNL_module, pName->pszElementValue, pValue->pszElementValue);

                BNL_tb_copy_string(pValue->pszElementValue, &pszValuesTemp);
                BNL_split_multiple_values(pszValuesTemp, ';', &iVals, &pszVals);

                if ((iRetCode = BNL_rp_get_prop_type(tRev, pName->pszElementValue, szObjType, &pszAttrValue)) != 0) goto function_exit;

                fprintf(rplog, "%s: Property has value [%s].\n", BNL_module, pszAttrValue);

                if ((strcmp(pValue->pszElementValue, "IS_NOT_NULL") == 0 && strlen(pszAttrValue) == 0)
                  || (strcmp(pValue->pszElementValue, "IS_NOT_NULL") != 0 && !BNL_tb_value_in_list(pszAttrValue, iVals, pszVals)))
                {
                  fprintf(rplog, "%s: Warning, property doesn't match. Skipping checks of children.\n", BNL_module);
                  lParentMatch = false;
                  break;
                }

                if (pszAttrValue != NULL)
                {
                  MEM_free(pszAttrValue);
                  pszAttrValue = NULL;
                }
                pAttr = pAttr->pNextSibling;

                if (pszValuesTemp != NULL)
                {
                  free(pszValuesTemp);
                  pszValuesTemp = NULL;
                }

                if (pszVals != NULL)
                {
                  for (i = 0; i < iVals; i++)
                  {
                    free(pszVals[i]);
                    pszVals[i] = NULL;
                  }
                  free(pszVals);
                  pszVals = NULL;
                }
              } // End of if (pName == NULL || pValue == NULL)
            } // End of while (pAttr != NULL)
          } // End of if (BNL_tb_value_in_list(szType, iTypes, pszTypes))
          if (pszTypeTemp != NULL)
          {
            free(pszTypeTemp);
            pszTypeTemp = NULL;
          }

          if (pszTypes != NULL)
          {
            for (i = 0; i < iTypes; i++)
            {
              free(pszTypes[i]);
              pszTypes[i] = NULL;
            }
            free(pszTypes);
            pszTypes = NULL;
          }
        } // End of if (pType != NULL)
      } // End of if (pParent != NULL)

      if (lParentMatch)
      {
        fprintf(rplog, "%s: The parent matches now check if its childs are allowed.\n", BNL_module);

        // Get the Item, get the bomviews
        iRetCode = ITEM_ask_item_of_rev(tRev, &tItem);
        if (BNL_em_error_handler("ITEM_ask_item_of_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        iRetCode = ITEM_list_bom_views(tItem, &iBomCount, &ptBomViews);
        if (BNL_em_error_handler("ITEM_list_bom_views", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

        fprintf(rplog, "%s: Found [%d] bomviews at target.\n", BNL_module, iBomCount);

        /* check for the bomview type*/
        for (i = 0;i < iBomCount; i++)
        {
          if ((iRetCode = BNL_tb_determine_object_type(ptBomViews[i], szBomType)) != 0) goto function_exit;

          fprintf(rplog, "%s: Found view type [%s].\n", BNL_module, szBomType);

          if (strcmp(pViewType->pszElementValue, szBomType)== 0)
          {
            tBomViewType = ptBomViews[i];
            break;
          }
        } // for (i = 0;i < iBomCount; i++)

        if (tBomViewType != NULL_TAG)
        {
          fprintf(rplog, "%s: Processing view of type [%s].\n", BNL_module, szBomType);

          iRetCode = BOM_create_window(&tBomwindow);
          if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

          iRetCode = BNL_tb_bom_apply_config_rule(tBomwindow, pRevRule->pszElementValue);
          if (BNL_em_error_handler("BNL_tb_bom_apply_config_rule", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

          iRetCode = BOM_set_window_top_line(tBomwindow, NULL_TAG, tRev, tBomViewType, &tTopLine);
          if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

          iRetCode = BOM_set_window_pack_all(tBomwindow, false);
          if (BNL_em_error_handler("BOM_set_window_pack_all", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

          iRetCode = BOM_line_ask_child_lines(tTopLine, &iBomLineCount, &ptBomLines);
          if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

          iRetCode = BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId);
          if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
        }

        if (iBomLineCount == 0)
        {
          fprintf(rplog, "%s: Warning, nothing to check since no bomlines have been found.\n", BNL_module);
        }
        else
        {
          // No check if the childs are allowed
          for (ii = 0; ii < iBomLineCount ; ii++)
          {
            char szErrorDescription[512]      = { "" };
            lChildIsAllowed = false;

            fprintf(rplog, "%s: Checking bomline [%d] of [%d].\n", BNL_module, ii+1, iBomLineCount);

            pChild = BNL_xml_find_first(pBomcheck, XML_EL_CHILD);

            while (pChild != NULL && !lChildIsAllowed)
            {
              iRetCode = BOM_line_ask_attribute_tag(ptBomLines[ii], iAttrRevId, &tItemRev);
              if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              // Get the type
              pType = BNL_xml_find_first(pChild, "type");

              if (pType == NULL)
              {
                fprintf(rplog, "%s: Warning, child type not found in configuration file.\n", BNL_module);
              }
              else
              {
                fprintf(rplog, "%s: child type = [%s].\n", BNL_module, pType->pszElementValue);

                BNL_tb_copy_string(pType->pszElementValue, &pszTypeTemp);
                BNL_split_multiple_values(pszTypeTemp, ';', &iTypes, &pszTypes);

                if ((iRetCode = BNL_tb_determine_object_type(tItemRev, szType)) != 0) goto function_exit;

                fprintf(rplog, "%s: Occurence is of type [%s].\n", BNL_module, szType);

                if (BNL_tb_value_in_list(szType, iTypes, pszTypes) || strcmp(pType->pszElementValue, "*") == 0)
                {
                  lChildIsAllowed = true;

                  fprintf(rplog, "%s: Type [%s] matches, now check the attributes on the child.\n", BNL_module, szType);

                  pAttr = BNL_xml_find_first(pChild, "attr");

                  //while (pAttr != NULL && lChildIsAllowed)
                  while (pAttr != NULL && strcmp(pAttr->pszElementname, "attr") == 0 && lChildIsAllowed)
                  {
                    pName = BNL_xml_find_first(pAttr, "name");
                    pDispName = BNL_xml_find_first(pAttr, "disp_name");
                    pValue = BNL_xml_find_first(pAttr, "value");

                    if (pName == NULL || pValue == NULL)
                    {
                      fprintf(rplog, "%s: Warning, name and/or value were not found in configuration file.\n", BNL_module);
                    }
                    else
                    {
                      fprintf(rplog, "%s: name = [%s], value = [%s].\n", BNL_module, pName->pszElementValue, pValue->pszElementValue);

                      BNL_tb_copy_string(pValue->pszElementValue, &pszValuesTemp);
                      BNL_split_multiple_values(pszValuesTemp, ';', &iVals, &pszVals);

                      pszAllowedVal = BNL_rp_allowed_vals(iVals, pszVals);

                      if ((iRetCode = BNL_rp_get_prop_type(tItemRev, pName->pszElementValue, szObjType, &pszAttrValue)) != 0)
                      {
                        fprintf(rplog, "%s: Warning, property [%s] does not exist on ItemRevision, checking bomline.\n", BNL_module, pName->pszElementValue);
                        EMH_clear_errors();

                        if ((iRetCode = BNL_rp_get_prop_type(ptBomLines[ii], pName->pszElementValue, szObjType, &pszAttrValue)) != 0) goto function_exit;
                      }

                      fprintf(rplog, "%s: Property has value [%s].\n", BNL_module, pszAttrValue);


                      // JM|15-05-2012: Workaround for IR 1894974
                      //if ((strcmp(pValue->pszElementValue, "IS_NOT_NULL") == 0 && strlen(pszAttrValue) > 0)
                      if ((strcmp(pValue->pszElementValue, "IS_NOT_NULL") == 0 && strcmp(pName->pszElementValue, "bl_real_quantity") != 0 && strlen(pszAttrValue) > 0)
                          || (strcmp(pValue->pszElementValue, "IS_NOT_NULL") == 0 && strcmp(pName->pszElementValue, "bl_real_quantity") == 0 && strlen(pszAttrValue) > 0 && strcmp(pszAttrValue, "0") != 0)
                          || (strcmp(pValue->pszElementValue, "IS_NOT_NULL") != 0 && BNL_tb_value_in_list(pszAttrValue, iVals, pszVals)))
                      {
                        fprintf(rplog, "%s: Property values matches [%s].\n", BNL_module, pszAttrValue);
                      }
                      else
                      {
                        fprintf(rplog, "%s: Warning, property doesn't match.\n", BNL_module);
                        lChildIsAllowed = false;
                        if (pDispName != NULL)
                        {
                          sprintf(szErrorDescription, "Value [%s] of property [%s] does not match allowed values [%s].", pszAttrValue, pDispName->pszElementValue, pszAllowedVal);
                        }
                        else
                        {
                          sprintf(szErrorDescription, "Value [%s] of property [%s] does not match allowed values [%s].", pszAttrValue, pName->pszElementValue, pszAllowedVal);
                        }
                        if (pszError == NULL)
                        {
                          pszError = (char *)MEM_realloc(pszError, ((int)strlen(szErrorDescription) + 1));
                          strcpy(pszError, szErrorDescription);
                        }
                        else
                        {
                          pszError = (char *)MEM_realloc(pszError, ((int)strlen(pszError) + (int)strlen(szErrorDescription) + 5));
                          sprintf(pszError, "%s<BR>%s", pszError, szErrorDescription);
                        }
                      }

                      if (pszAttrValue != NULL)
                      {
                        MEM_free(pszAttrValue);
                        pszAttrValue = NULL;
                      }
                      pAttr = pAttr->pNextSibling;

                      if (pszValuesTemp != NULL)
                      {
                        free(pszValuesTemp);
                        pszValuesTemp = NULL;
                      }

                      if (pszVals != NULL)
                      {
                        for (i = 0; i < iVals; i++)
                        {
                          free(pszVals[i]);
                          pszVals[i] = NULL;
                        }
                        free(pszVals);
                        pszVals = NULL;
                      }

                      if (pszAllowedVal != NULL)
                      {
                        MEM_free(pszAllowedVal);
                        pszAllowedVal = NULL;
                      }
                    } // End of if (pName == NULL || pValue == NULL)
                  } // End of while (pAttr != NULL)

                  if (lChildIsAllowed)
                  {
                    fprintf(rplog, "%s: Child is allowed, checking for [%s].\n", BNL_module, XML_EL_ONECHILD);
                    // XML_EL_ONECHILD ?
                    pOneChild = BNL_xml_find_first(pChild, XML_EL_ONECHILD);

                    if (pOneChild != NULL)
                    {
                      logical lOneChildFound = false;

                      fprintf(rplog, "%s: [%s] has been configured.\n", BNL_module, XML_EL_ONECHILD);

                      pName = BNL_xml_find_first(pOneChild, "name");
                      pDispName = BNL_xml_find_first(pOneChild, "disp_name");
                      pValue = BNL_xml_find_first(pOneChild, "value");

                      if (pName == NULL || pValue == NULL)
                      {
                        fprintf(rplog, "%s: Warning, name and/or value were not found in configuration file.\n", BNL_module);
                      }
                      else
                      {
                        fprintf(rplog, "%s: name = [%s], value = [%s].\n", BNL_module, pName->pszElementValue, pValue->pszElementValue);

                        BNL_tb_copy_string(pValue->pszElementValue, &pszValuesTemp);
                        BNL_split_multiple_values(pszValuesTemp, ';', &iVals, &pszVals);

                        pszAllowedVal = BNL_rp_allowed_vals(iVals, pszVals);

                        for (jj = 0; jj < iBomLineCount ; jj++)
                        {
                          iRetCode = BOM_line_ask_attribute_tag(ptBomLines[jj], iAttrRevId, &tItemRevOne);
                          if (BNL_em_error_handler("BOM_line_ask_attribute_tag", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

                          if ((iRetCode = BNL_rp_get_prop_type(tItemRevOne, pName->pszElementValue, szObjType, &pszAttrValue)) != 0)
                          {
                            fprintf(rplog, "%s: Warning, property [%s] does not exist.\n", BNL_module, pName->pszElementValue);
                            EMH_clear_errors();
                          }
                          else
                          {
                            fprintf(rplog, "%s: Property of bomline [%d] of [%d] has value [%s].\n", BNL_module, (jj + 1), iBomLineCount, pszAttrValue);

                            if ((strcmp(pValue->pszElementValue, "IS_NOT_NULL") == 0 && strlen(pszAttrValue) > 0)
                              || (strcmp(pValue->pszElementValue, "IS_NOT_NULL") != 0 && BNL_tb_value_in_list(pszAttrValue, iVals, pszVals)))
                            {
                              fprintf(rplog, "%s: Property values matches [%s].\n", BNL_module, pszAttrValue);

                              if (lOneChildFound)
                              {
                                fprintf(rplog, "%s: Already matching bomline found, not allowed. [%s] has failed.\n", BNL_module, XML_EL_ONECHILD);
                                lChildIsAllowed = false;

                                if (pDispName != NULL)
                                {
                                  sprintf(szErrorDescription, "More than one child found with property [%s] set to [%s].", pDispName->pszElementValue, pszAllowedVal);
                                }
                                else
                                {
                                  sprintf(szErrorDescription, "More than one child found with property [%s] set to [%s].", pName->pszElementValue, pszAllowedVal);
                                }
                                if (pszError == NULL)
                                {
                                  pszError = (char *)MEM_realloc(pszError, ((int)strlen(szErrorDescription) + 1));
                                  strcpy(pszError, szErrorDescription);
                                }
                                else
                                {
                                  pszError = (char *)MEM_realloc(pszError, ((int)strlen(pszError) + (int)strlen(szErrorDescription) + 5));
                                  sprintf(pszError, "%s<BR>%s", pszError, szErrorDescription);
                                }
                              }
                              else
                              {
                                // First child
                                lOneChildFound = true;
                              }
                            }
                          } // End of if ((strcmp(pValue->pszElementValue, "IS_NOT_NULL") == 0 && strlen(pszAttrValue) > 0) || (strcmp(pValue->pszElementValue, "IS_NOT_NULL") != 0 && BNL_tb_value_in_list(pszAttrValue, iVals, pszVals)))

                          if (pszAttrValue != NULL)
                          {
                            MEM_free(pszAttrValue);
                            pszAttrValue = NULL;
                          }
                        } // End of for (jj = 0; jj < iBomLineCount ; jj++)

                        if (!lOneChildFound)
                        {
                          fprintf(rplog, "%s: No matching childs found, not allowed. [%s] has failed.\n", BNL_module, XML_EL_ONECHILD);
                          lChildIsAllowed = false;
                          if (pDispName != NULL)
                          {
                            sprintf(szErrorDescription, "No child found with property [%s] set to [%s].", pDispName->pszElementValue, pszAllowedVal);
                          }
                          else
                          {
                            sprintf(szErrorDescription, "No child found with property [%s] set to [%s].", pName->pszElementValue, pszAllowedVal);
                          }
                          if (pszError == NULL)
                          {
                            pszError = (char *)MEM_realloc(pszError, ((int)strlen(szErrorDescription) + 1));
                            strcpy(pszError, szErrorDescription);
                          }
                          else
                          {
                            pszError = (char *)MEM_realloc(pszError, ((int)strlen(pszError) + (int)strlen(szErrorDescription) + 5));
                            sprintf(pszError, "%s<BR>%s", pszError, szErrorDescription);
                          }
                        }

                        if (pszValuesTemp != NULL)
                        {
                          free(pszValuesTemp);
                          pszValuesTemp = NULL;
                        }

                        if (pszVals != NULL)
                        {
                          for (i = 0; i < iVals; i++)
                          {
                            free(pszVals[i]);
                            pszVals[i] = NULL;
                          }
                          free(pszVals);
                          pszVals = NULL;
                        }

                        if (pszAllowedVal != NULL)
                        {
                          MEM_free(pszAllowedVal);
                          pszAllowedVal = NULL;
                        }
                      } // End of if (pName == NULL || pValue == NULL)
                    } // End of if (pOneChild != NULL)
                  } // End of if (lChildIsAllowed)
                } // End of if (BNL_tb_value_in_list(szType, iTypes, pszTypes))
                else
                {
                  lChildIsAllowed = false;
                  fprintf(rplog, "%s: Type [%s] does not match, child is not allowed.\n", BNL_module, szType);

                  sprintf(szErrorDescription, "Type child [%s] does not match allowed types [%s].", szType, pType->pszElementValue);

                  if (pszError == NULL)
                  {
                    pszError = (char *)MEM_realloc(pszError, ((int)strlen(szErrorDescription) + 1));
                    strcpy(pszError, szErrorDescription);
                  }
                  else
                  {
                    pszError = (char *)MEM_realloc(pszError, ((int)strlen(pszError) + (int)strlen(szErrorDescription) + 5));
                    sprintf(pszError, "%s<BR>%s", pszError, szErrorDescription);
                  }
                }

                if (pszTypeTemp != NULL)
                {
                  free(pszTypeTemp);
                  pszTypeTemp = NULL;
                }

                if (pszTypes != NULL)
                {
                  for (i = 0; i < iTypes; i++)
                  {
                    free(pszTypes[i]);
                    pszTypes[i] = NULL;
                  }
                  free(pszTypes);
                  pszTypes = NULL;
                }
              } // End of if (pType != NULL)
              pChild = pChild->pNextSibling;
            } // End of while (pChild != NULL)

            if (!lChildIsAllowed)
            {
              if (pszError == NULL || strlen(pszError) == 0)
              {
                strcpy(szErrorDescription, "Configuration error, check the log file for more details.");
                if (pszError == NULL)
                {
                  pszError = (char *)MEM_realloc(pszError, ((int)strlen(szErrorDescription) + 1));
                  strcpy(pszError, szErrorDescription);
                }
                else
                {
                  pszError = (char *)MEM_realloc(pszError, ((int)strlen(pszError) + (int)strlen(szErrorDescription) + 5));
                  sprintf(pszError, "%s<BR>%s", pszError, szErrorDescription);
                }
              }
              fprintf(rplog, "%s: Child is not allowed.\n", BNL_module);
              fprintf(rplog, "%s: Error description: %s\n", BNL_module, pszError);

              iCount++;
              pDiffs = (diffinfo_t *)MEM_realloc(pDiffs, iCount * sizeof(diffinfo_t));

              BNL_rp_create_xml_props(ptBomLines[ii], "bom_comprep", 0, &pProps, NULL, NULL, NULL, NULL, pszError);

              pDiffs[iCount - 1].tObject = ptBomLines[ii];
              pDiffs[iCount - 1].pszQuant = BNL_strings_copy_string("");

              pDiffs[iCount - 1].pszSeq = BNL_strings_copy_string("");
              pDiffs[iCount - 1].pszRefDes = BNL_strings_copy_string("");
              pDiffs[iCount - 1].pXMLprops = pProps;
              pDiffs[iCount - 1].iChange = 0;

              pDiffs[iCount - 1].pNext = NULL;
              pDiffs[iCount - 1].pPrev = NULL;
            } // End of if (!lChildIsAllowed)
            if (pszError != NULL)
            {
              MEM_free(pszError);
              pszError = NULL;
            }
          } // End of for (i = 0; i < iItemRevCount ; i++)
        } // End of if (iItemRevCount == 0)
        if (iBomLineCount > 0)
        {
          MEM_free(ptBomLines);
          iBomLineCount = 0;
        }

        if (iBomCount > 0)
        {
          MEM_free(ptBomViews);
          iBomCount = 0;
        }
      } // End of if (pParent != NULL)
      pBomcheck = pBomcheck->pNextSibling;
    } // End of while (pBomcheck != NULL)
  } // End of if (pRevRule == NULL || pViewType == NULL || pLevel == NULL)


function_exit:

  if (pszError != NULL) MEM_free(pszError);

  if (tBomwindow != NULL_TAG)
  {
    iRetCode = BOM_close_window(tBomwindow);
    if (BNL_em_error_handler("BOM_close_window", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;
  }

  // Free memory
  if (iBomLineCount > 0) MEM_free(ptBomLines);

  if (iBomCount > 0) MEM_free(ptBomViews);

  if (pszTypes != NULL)
  {
    for (i = 0; i < iTypes; i++)
    {
      free(pszTypes[i]);
    }
    free(pszTypes);
  }

  if (pszVals != NULL)
  {
    for (i = 0; i < iVals; i++)
    {
      free(pszVals[i]);
    }
    free(pszVals);
  }

  if (pszAllowedVal != NULL) MEM_free(pszAllowedVal);

  if (pszTypeTemp != NULL) free(pszTypeTemp);

  if (pszValuesTemp != NULL) free(pszValuesTemp);

  if (pszAttrValue != NULL) MEM_free(pszAttrValue);

  if (pszAttrChild != NULL) MEM_free(pszAttrChild);

  *piBomCount = iCount;
  *pBomDiffs = pDiffs;

  return iRetCode;
}

char *BNL_rp_allowed_vals(int iStats, char **pszStats)
{
  int i                     = 0;
  char *pszReturnString     = NULL;


  for (i = 0; i < iStats;i++)
  {
    if (strcmp(pszStats[i], "IS_NOT_NULL") == 0)
    {
      if (pszReturnString == NULL)
      {
        pszReturnString = (char *)MEM_realloc(pszReturnString, ((int)strlen("[Not empty]") + 1));
        strcpy(pszReturnString, "[Not empty]");
      }
      else
      {
        pszReturnString = (char *)MEM_realloc(pszReturnString, ((int)strlen(pszReturnString) + (int)strlen("[Not empty]") + 2));
        sprintf(pszReturnString, "%s;%s", pszReturnString, "[Not empty]");
      }
    }
    else if (strcmp(pszStats[i], "") == 0)
    {
      if (pszReturnString == NULL)
      {
        pszReturnString = (char *)MEM_realloc(pszReturnString, ((int)strlen("[Empty]") + 1));
        strcpy(pszReturnString, "[Empty]");
      }
      else
      {
        // JM: Prevent (;) => ([Empty];[Empty])
        if (strstr("[Empty]", pszReturnString) == NULL)
        {
          pszReturnString = (char *)MEM_realloc(pszReturnString, ((int)strlen(pszReturnString) + (int)strlen("[Empty]") + 2));
          sprintf(pszReturnString, "%s;%s", pszReturnString, "[Empty]");
        }
      }
    }
    else
    {
      if (pszReturnString == NULL)
      {
        pszReturnString = (char *)MEM_realloc(pszReturnString, ((int)strlen(pszStats[i]) + 1));
        strcpy(pszReturnString, pszStats[i]);
      }
      else
      {
        pszReturnString = (char *)MEM_realloc(pszReturnString, ((int)strlen(pszReturnString) + (int)strlen(pszStats[i]) + 2));
        sprintf(pszReturnString, "%s;%s", pszReturnString, pszStats[i]);
      }
    }
  }

  return pszReturnString;
}

/*
int BNL_rp_web_frulist_item
(
  tag_t tRev                 <I>
  char *pszRevRule,          <I>
  char **pszXMLrep,         <OF>
  t_XMLElement **pDocument   <OF>
)

 Description:
 Generates FRUList item report
 */
int BNL_rp_web_frulist_item
(
  tag_t tRev,
  char *pszRevRule,
  char **pszXMLrep,
  t_XMLElement **pDocument
)
{
    int iRetCode        = 0;
    array_t *pArray;


    clock_t start, finish;
    double  duration;

    fprintf(rplog, "BNL_rp_web_frulist_item .\n");

    start=clock();

    AOM_refresh(tRev, false);

    iRetCode = BNL_rp_get_frulist_item_report(tRev, pszRevRule, pszXMLrep, &pArray, pDocument);

    fprintf(rplog, "Freeing memory using BNL_rp_free_diff_struc in BNL_rp_web_bom\n");
    BNL_rp_free_diff_struc(pArray->iDiffsCount, pArray->ptDiffs);
    free(pArray);

    finish=clock();
    duration = (double)(finish - start) / CLOCKS_PER_SEC;
    fprintf(rplog, "FRUList item report creation took:[%2.1f]s\n",duration);

    return iRetCode;
}
/*
int BNL_rp_get_frulist_item_report
(
  tag_t tRev                 <I>
  char *pszRevRule,          <I>
  char **pszXMLrep ,         <OF>
  array_t **ppArray,         <OF>
  t_XMLElement **pDocument   <OF>
)

 Description:
 Generates FRUList item report
 */
int BNL_rp_get_frulist_item_report
(
  tag_t tRev,
  char *pszRevRule,
  char **pszXMLrep,
  array_t **ppArray,
  t_XMLElement **pDocument
 )
{
    int iRetCode        = 0;
    int iCount          = 0;
    int iLines          = 0;


    int imonth          = 0;
    int iday            = 0;
    int iyear           = 0;
    int ihour           = 0;
    int iminute         = 0;
    int isecond         = 0;
    int iRevs     = 0;


    char *pszViewType   = NULL;
    char *pszDateFormat = NULL;
    char *pszSort       = NULL;

    char *pszCommItem   = NULL;
    char *pszFRUListItem= NULL;

    char szType[256] = "";

    logical lFRUItem = false;
    logical lCommItem = false;


    tag_t tBomWindow     =NULL_TAG;
    tag_t tTopLine       =NULL_TAG;
    tag_t tItem          =NULL_TAG;
    tag_t tBom           =NULL_TAG;
    tag_t tPresetRule    =NULL_TAG;
    tag_t tCopyRule      =NULL_TAG;
    tag_t tDateEntry     =NULL_TAG;
    tag_t *ptRevs    = NULL;


    diffinfo_t *pLines   = NULL;
    array_t *pArray;

    PREF_ask_char_value(FEI_COMMERCIAL_ITEM, 0, &pszCommItem);
    if (pszCommItem == NULL)
    {
      fprintf(rplog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_COMMERCIAL_ITEM);
      goto exit;
    }
    else
    {
      if (strlen(pszCommItem)<1)
      {
          fprintf(rplog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_COMMERCIAL_ITEM);
          goto exit;
      }
    }
    fprintf(rplog, "%s: Preference [%s] has value [%s].\n\n", BNL_module, FEI_COMMERCIAL_ITEM, pszCommItem);

    PREF_ask_char_value(FEI_FRULIST_ITEM, 0, &pszFRUListItem);
    if (pszFRUListItem == NULL)
    {
      fprintf(rplog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_FRULIST_ITEM);
      goto exit;
    }
    else
    {
      if (strlen(pszFRUListItem)<1)
      {
          fprintf(rplog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_FRULIST_ITEM);
          goto exit;
      }
    }
    fprintf(rplog, "%s: Preference [%s] has value [%s].\n\n", BNL_module, FEI_FRULIST_ITEM, pszFRUListItem);


    pArray=malloc(sizeof(array_t));
    pArray->iDiffsCount= 0;
    pArray->ptDiffs= NULL;
    pArray->tPredecessor=NULL_TAG;
    pArray->tProduct=NULL_TAG;

    *ppArray=pArray;

    pArray->tProduct=tRev;

    if (pszViewType==NULL)
    {
        pszViewType=MEM_alloc(sizeof(char)*5);
        strcpy(pszViewType, "view");
    }

    iRetCode = BNL_rp_get_bomview(tRev, pszViewType, &tBom);
    if (BNL_em_error_handler("BNL_rep_get_bomview", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    MEM_free(pszViewType);

    if (tBom==NULL_TAG)
    {
        BNL_rp_xml_error("The chosen revision does not have a bomview revision", pszXMLrep, pDocument);
        return iRetCode;
    }

    iRetCode = CFM_find(pszRevRule, &tPresetRule);
    if (iRetCode == 710041)
    {
         BNL_rp_xml_error("The given revision rule does not exist", pszXMLrep, pDocument);
         return iRetCode;
    }

    if (BNL_em_error_handler("CFM_find", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    //get the item to check the type
    ITEM_ask_item_of_rev(tRev, &tItem);
    iRetCode = BNL_tb_determine_object_type(tItem, szType);
    fprintf(rplog, "szType[%s]\n", szType);

    if (strcmp(szType, pszFRUListItem)== 0)
    {
      lFRUItem = true;
    }

    if (strcmp(szType, pszCommItem)== 0)
    {
      lCommItem = true;
    }

    iRetCode = BOM_create_window(&tBomWindow);
    if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    //iRetCode = BOM_set_window_pack_all(tBomWindow,true);
    //if (BNL_em_error_handler("BOM_set_window_pack_all", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    /*set revision rule*/
    iRetCode = BOM_set_window_config_rule( tBomWindow, tPresetRule);
    if (BNL_em_error_handler("BNL_tb_bom_apply_config_rule", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    iRetCode = BOM_set_window_top_line(tBomWindow,NULL_TAG,tRev,tBom, &tTopLine);
    if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    iRetCode = BNL_rp_bom_frulist_item_lines(tTopLine, pszCommItem, pszFRUListItem,lCommItem,lFRUItem,0, &iLines, &pLines);
    if (BNL_em_error_handler("BNL_rp_bom_frulist_item_lines", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    fprintf(rplog, "iLines [%d]\n", iLines);

    iRetCode = BOM_close_window(tBomWindow);
    if (BNL_em_error_handler("BOM_close_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    pArray->iDiffsCount=iLines;
    pArray->ptDiffs=pLines;
    pArray->tPredecessor=NULL_TAG;

    iRetCode = PREF_ask_char_value("BNL_sort_bomlines",0, &pszSort);
    if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) return iRetCode;

    if (pszSort!=NULL)
    {
     fprintf(rplog, "Sorting bomlines on property [%s]\n", pszSort);
     BNL_rp_sort_on_prop(pszSort, iLines, pLines);
     MEM_free(pszSort);
    }

    BNL_rp_get_xml_struc("frulist", *pArray, pszXMLrep, pDocument);

    //for new report parser to html we add the info, it will not appear in the web structure.
    BNL_rp_store_configinfo(*pDocument, pszRevRule,NULL,-1);
exit:
    return iRetCode;
}

/*
int BNL_rp_bom_frulist_item_lines
(
  tag_t tBomLine             <I>
  char  *pszCommItem,        <I>
  char  *pszFRUListItem,     <I>
  logical    lCommParent,    <I>
  int    iLevel,             <I>
  int   *iRevCount ,         <OF>
  diffinfo_t **tItemRevs     <OF>
)

 Description:
 Generates FRUList item report
 */
int BNL_rp_bom_frulist_item_lines
(
  tag_t      tBomLine,
  char       *pszCommItem,
  char       *pszFRUListItem,
  logical    lCommParent,
  logical    lFRUParent,
  int        iLevel,
  int        *iRevCount,
  diffinfo_t **tItemRevs
)
{
    tag_t *tBomLines            = NULL;

    int iRetCode                =ITK_ok;

    int iBomLineCount           = 0;
    int l                       = 0;
    int i                       = 0;
    int iFound                  = 0;
    int iCount                  = 0;

    char szMode[20] = "frulist";
    char szFormType[100] = "";
    char szType[256] = "";

    logical lUnpack             = false;

    diffinfo_t *ptRevs=*tItemRevs;

    fprintf(rplog, "BNL_rp_bom_frulist_item_lines begin.\n");

    //initiliaze the form type
    sprintf(szFormType, "%s Master", pszCommItem);

    iCount=*iRevCount;

    /*if (iLevel == 0)
    {
        iCount++;
        ptRevs=(diffinfo_t *) MEM_realloc(ptRevs, iCount * sizeof(diffinfo_t));

        BNL_rp_bom_write_frulist_item_line(tBomLine, iLevel,szMode, iCount, &ptRevs);
    }*/

    iRetCode = BOM_line_ask_child_lines(tBomLine, &iBomLineCount, &tBomLines);
    if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return false;

    fprintf(rplog, "DEBUG: Mode [%s].\n", szMode);
    fprintf(rplog, "%s: Found at bomline at level [%d] [%d] bomlines lines\n  ", BNL_module,(iLevel+1), iBomLineCount);

    for (l= 0;l<iBomLineCount;l++)
    {
        tag_t tBomItem = NULL_TAG;
        tag_t tIMFObj  = NULL_TAG;

        //first check for commercial item
        if (BNL_rp_get_bom_item(tBomLines[l], &tBomItem)==true)
        {
            iRetCode = BNL_tb_determine_object_type(tBomItem, szType);
            fprintf(rplog, "szType[%s]\n", szType);

            if (strcmp(szType, pszCommItem) == 0 && lCommParent == false)
            {
                fprintf(rplog, "Comm item found\n");
                BNL_rp_get_related_object(tBomItem, "IMAN_master_form",szFormType, &tIMFObj);
                if (tIMFObj != NULL_TAG)
                {
                    if (BNL_rp_process_CommItem(tIMFObj) == true)
                    {
                        fprintf(rplog, "Comm item subclass validation ok!!, processing\n");

                        iCount++;
                        ptRevs=(diffinfo_t *) MEM_realloc(ptRevs, iCount * sizeof(diffinfo_t));

                        BNL_rp_bom_write_frulist_item_line(tBomLines[l], iLevel+1,szMode, iCount, &ptRevs);

                        if (lFRUParent == false)
                        {
                            iRetCode = BNL_rp_bom_frulist_item_lines(tBomLines[l], pszCommItem, pszFRUListItem,true,false, iLevel+1, &iCount, &ptRevs);
                            if (BNL_em_error_handler("BNL_tb_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) return false;
                        }
                        else
                        {
                            fprintf(rplog, "parent is frulist item, only 1 level processed\n");
                        }
                    }
                    else
                    {
                        fprintf(rplog, "Comm item subclass validation failed!!, not processing\n");
                    }
                }
            }
            else if (strcmp(szType, pszFRUListItem) == 0)
            {
                fprintf(rplog, "FRULIst item found!!, processing\n");

                iCount++;
                ptRevs=(diffinfo_t *) MEM_realloc(ptRevs, iCount * sizeof(diffinfo_t));

                BNL_rp_bom_write_frulist_item_line(tBomLines[l], iLevel+1,szMode, iCount, &ptRevs);

                if (lFRUParent == false)
                {
                    iRetCode = BNL_rp_bom_frulist_item_lines(tBomLines[l], pszCommItem, pszFRUListItem,false,true, iLevel+1, &iCount, &ptRevs);
                    if (BNL_em_error_handler("BNL_tb_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) return false;
                }
                else
                {
                    fprintf(rplog, "parent is frulist item, only 1 level processed\n");
                }
            }
            else if (lCommParent == false)
            {
                fprintf(rplog, "parent is not a Com item, processing bom line\n");

                iCount++;
                ptRevs=(diffinfo_t *) MEM_realloc(ptRevs, iCount * sizeof(diffinfo_t));

                BNL_rp_bom_write_frulist_item_line(tBomLines[l], iLevel+1,szMode, iCount, &ptRevs);

                if (lFRUParent == false)
                {
                    iRetCode = BNL_rp_bom_frulist_item_lines(tBomLines[l], pszCommItem, pszFRUListItem,false,false, iLevel+1, &iCount, &ptRevs);
                    if (BNL_em_error_handler("BNL_tb_bom_get_item_revs", BNL_module, EMH_severity_error, iRetCode, true)) return false;
                }
                else
                {
                    fprintf(rplog, "parent is frulist item, only 1 level processed\n");
                }
            }
        }
        else
        {
            fprintf(rplog, "Error getting bom item.\n");
        }
    }


    if (iBomLineCount>0) BNL_mm_free((void*)tBomLines);
    *tItemRevs=ptRevs;
    *iRevCount=iCount;

    return iRetCode;

}

/*
int BNL_rp_bom_write_frulist_item_line
(
  tag_t tBomLine             <I>
  int    iLevel,             <I>
  char  *szMode,             <I>
  int    iRevCount ,         <OF>
  diffinfo_t **tItemRevs     <OF>
)

 Description:
 Generates FRUList item report
 */
int BNL_rp_bom_write_frulist_item_line
(
  tag_t      tBomLine,
  int        iLevel,
  char       *szMode,
  int        iRevCount,
  diffinfo_t **tItemRevs
)
{
    int iRetCode                =ITK_ok;
    int iAttrRevId              = 0;
    int iAttrItemId             = 0;
    int iAttrQuantId            = 0;
    int iAttrTypeId             = 0;

    tag_t tItemRev              =NULL_TAG;
    tag_t tItem                 =NULL_TAG;

    char *pszQuantity           = NULL;
    char *pszSequence           = NULL;
    char *pszRefDes             = NULL;
    char *pszType               = NULL;

    t_XMLElement *pProps     = NULL;

    diffinfo_t *ptRevs=*tItemRevs;

    fprintf(rplog, "BNL_rp_bom_write_frulist_item_line begin.\n");

    ptRevs=(diffinfo_t *) MEM_realloc(ptRevs, iRevCount * sizeof(diffinfo_t));

    iRetCode = BOM_line_look_up_attribute(bomAttr_occQty, &iAttrQuantId);
    if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return false;

    iRetCode = BOM_line_ask_attribute_string(tBomLine, iAttrQuantId, &pszQuantity);
    if (BNL_em_error_handler("BOM_line_ask_attribute_string", BNL_module, EMH_severity_error, iRetCode, true)) return false;

    iRetCode = BOM_line_look_up_attribute(bomAttr_occSeqNo, &iAttrQuantId);
    if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return false;

    iRetCode = BOM_line_ask_attribute_string(tBomLine, iAttrQuantId, &pszSequence);
    if (BNL_em_error_handler("BOM_line_ask_attribute_string", BNL_module, EMH_severity_error, iRetCode, true)) return false;

    // REF DES
    BNL_tb_get_property(tBomLine, "REF DES", &pszRefDes);
    iRetCode = BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId);
    if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return false;

    iRetCode = BOM_line_look_up_attribute(bomAttr_lineItemTag, &iAttrItemId);
    if (BNL_em_error_handler("BOM_line_look_up_attribute", BNL_module, EMH_severity_error, iRetCode, true)) return false;

    iRetCode = BOM_line_ask_attribute_tag(tBomLine, iAttrRevId, &tItemRev);
    if (BNL_em_error_handler("BOM_line_ask_attribute_tag", BNL_module, EMH_severity_error, iRetCode, true)) return false;

    iRetCode = BOM_line_ask_attribute_tag(tBomLine, iAttrItemId, &tItem);
    if (BNL_em_error_handler("BOM_line_ask_attribute_tag", BNL_module, EMH_severity_error, iRetCode, true)) return false;

    if(tItemRev!=NULL_TAG)
    {
        AOM_refresh(tItemRev, false);
        AOM_refresh(tItem, false);

        ptRevs[iRevCount-1].tObject=tItemRev;
        //ptRevs[iRevCount-1].pszQuant=pszQuantity;
        ptRevs[iRevCount-1].pszQuant=BNL_strings_copy_string(pszQuantity);

        //ptRevs[iRevCount-1].pszSeq=pszSequence;
        ptRevs[iRevCount-1].pszSeq=BNL_strings_copy_string(pszSequence);
        //ptRevs[iCount-1].pszRefDes=pszRefDes;
        ptRevs[iRevCount-1].pszRefDes=BNL_strings_copy_string(pszRefDes);

        BNL_rp_create_xml_props(tBomLine,szMode, iLevel, &pProps,NULL,NULL,NULL,NULL,NULL);
        ptRevs[iRevCount-1].pXMLprops=pProps;
        ptRevs[iRevCount-1].iChange= 0;
        ptRevs[iRevCount-1].pNext= NULL;
        ptRevs[iRevCount-1].pPrev= NULL;
    }
    else
    {
        /*this happens when the revision rule is set and there are occurence wich not
          which is not a allowed occurence the (???) occurence.
          EN quik and dirty implementation 21-05-2001 for Philips EMT
        */

        fprintf(rplog, "%s:Unconfigured occurences found\n", BNL_module);

        ptRevs[iRevCount-1].tObject=tItem;
        //ptRevs[iRevCount-1].pszQuant=pszQuantity;
        ptRevs[iRevCount-1].pszQuant=BNL_strings_copy_string(pszQuantity);


        //ptRevs[iRevCount-1].pszSeq=pszSequence;
        ptRevs[iRevCount-1].pszSeq=BNL_strings_copy_string(pszSequence);
        //ptRevs[iCount-1].pszRefDes=pszRefDes;
        ptRevs[iRevCount-1].pszRefDes=BNL_strings_copy_string(pszRefDes);

        BNL_rp_create_xml_props(tBomLine,szMode, iLevel, &pProps,NULL,NULL,NULL,NULL,NULL);
        ptRevs[iRevCount-1].pXMLprops=pProps;
        ptRevs[iRevCount-1].iChange= 0;
        ptRevs[iRevCount-1].pNext= NULL;
        ptRevs[iRevCount-1].pPrev= NULL;
    }
    if (pszRefDes != NULL)
    {
       MEM_free(pszRefDes);
       pszRefDes = NULL;
    }

    *tItemRevs = ptRevs;
    return iRetCode;
}

/**
logical BNL_rp_process_CommItem

  Description:

    Processes the Commercial item

  Returns:

    true or false
)
*/
logical BNL_rp_process_CommItem(tag_t tIMFObj)
{
  int iRetCode = 0;

  int k              = 0;
  int iFRUCommSubClasses = 0;

  logical lProcess = false;

  char *pszCommSubclass = NULL;
  char *pszAttrVal      = NULL;

  char **pszFRUCommSubClasses = NULL;

  fprintf(rplog, "\nBegin ... BNL_rp_process_CommItem\n");

  PREF_ask_char_value(FEI_COMMERCIAL_SUBCLASS, 0, &pszCommSubclass);
  if (pszCommSubclass == NULL)
  {
      fprintf(rplog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_COMMERCIAL_SUBCLASS);
      goto exit;
  }
  else
  {
      if (strlen(pszCommSubclass)<1)
      {
          fprintf(rplog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_COMMERCIAL_SUBCLASS);
          goto exit;
      }
  }
  fprintf(rplog, "%s: Preference [%s] has value [%s].\n\n", BNL_module, FEI_COMMERCIAL_SUBCLASS, pszCommSubclass);

  PREF_ask_char_values(FEI_FRU_COMMERCIAL_SUBCLASS, &iFRUCommSubClasses, &pszFRUCommSubClasses);
  fprintf(rplog, "%s: Found types [%d] using preference [%s].\n", BNL_module, iFRUCommSubClasses,FEI_FRU_COMMERCIAL_SUBCLASS);

  if (iFRUCommSubClasses == 0)
  {
    fprintf(rplog, "%s: Warning, no values have been defined.\n\n", BNL_module);
    lProcess = false;
  }
  else
  {
      iRetCode = AOM_UIF_ask_value(tIMFObj, pszCommSubclass, &pszAttrVal);
      if (iRetCode !=ITK_ok)
      {
          char *pszmessage    = NULL;
          EMH_ask_error_text(iRetCode, &pszmessage);

          fprintf(rplog, " %s: xx- Error(AOM_UIF_ask_value [%s]) [%s] \n", BNL_module, pszCommSubclass, pszmessage);
          if (pszmessage) MEM_free(pszmessage);
      }
      else
      {
          if (pszAttrVal != NULL)
          {
              for (k= 0; k<iFRUCommSubClasses; k++)
              {
                  //fprintf(feilog, "%s: Found type [%s].\n", BNL_module, pszItemRevTypes[k]);
                  if (strcmp(pszAttrVal, pszFRUCommSubClasses[k]) == 0)
                  {
                      fprintf(rplog, "%s: value [%s] is listed.\n", BNL_module, pszAttrVal);
                      lProcess = true;
                      break;
                  }
              }
          }
      }
  }

  fprintf(rplog, "End ... BNL_rp_process_CommItem\n");
exit:
  if (iFRUCommSubClasses > 0) MEM_free(pszFRUCommSubClasses);
  if (pszAttrVal != NULL) MEM_free(pszAttrVal);
  return lProcess;
}

/**
logical BNL_rp_get_bom_item

  Description:

    returns the item of the bomline

  Returns:

    true or false
)
*/
logical BNL_rp_get_bom_item
(
  tag_t tBomLine,
  tag_t *tBomItem
)
{
  int iRetCode = 0;

  tag_t tProperty = NULL_TAG;
  tag_t tBItem = NULL_TAG;

  logical lFound = false;

  //fprintf(rplog, "\nBegin ... BNL_rp_get_bom_item\n");

   //get itemrev
  iRetCode = PROP_ask_property_by_name(tBomLine, "bl_item", &tProperty);
  if (iRetCode == ITK_ok)
  {
    iRetCode = PROP_ask_value_tag(tProperty, &tBItem);
    if(iRetCode == ITK_ok && tBItem !=NULL_TAG)
    {
      lFound = true;
      *tBomItem = tBItem;
    }
  }

  //fprintf(rplog, "End ... BNL_rp_get_bom_item\n");
  return lFound;
}
