/**
 (C) copyright by Siemens PLM Software

 Description	:

 Created for	: FEI


 Comments		  :

 Filename     :	BNL_fei_rep_parser.c

 Version info : release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 reason /description							Version     date
 ----------------------------------------------------------------------------------------------------------
 Creation                         0.1         13-08-2008
 Added ebom reports               0.2         13-08-2008
 Added new reports                0.3         03-09-2009
 Updated for dealing with entity
  characters                      0.4         01-12-2009
 Added BOMREPMFG                  0.5         05-01-2010
 Added WHEREUSEDTOP               0.6         19-02-2010
 Fixed BR for compliance report   0.7         01-03-2010
 Added FRUList item report        0.8         26-03-2010
 Updated for uom is each          0.9         29-02-2012

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <BNL_rp_module.h>
#include <BNL_strings_module.h>



void BNL_fei_write_xmlhtml
(
   FILE *fp,
   char *pszMode,
   char *pszXMLOut,
   int iMode,
   char *pszScript,
   char *pszRevRule,
   char *pszEffDate
)
{
  	fprintf(fp,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
	fprintf(fp,"<HTML><HEAD><META http-equiv=Content-Type content=\"text/html; charset=unicode\">\n");

	/*create extra xml island with type, its a bit double but we don't want to
	  edit the rp file to change a lot of things for just adding an attribute in the
	  document tag. tried to add some extra tag in the document but javascript does not accept
	  that.
	*/
	fprintf(fp,"<document type=\"%s\"/></xml>\n",pszMode);

	fprintf(fp,"<xml id=\"report\">");

	fprintf(fp,"%s",pszXMLOut);

	/*close the xml tag*/
	fprintf(fp,"</xml>\n");

	/*in case of bomrep extra configrule island*/
	if (iMode==BOMREP || iMode==BOMREPEXT || iMode==EBOMPACKEDREP || iMode==EBOMREP || iMode==BOMREPMFG)
	{
	  fprintf(fp,"<xml id=\"bomconfig\"><document>\n");
	  fprintf(fp,"<revrule>%s</revrule>\n",pszRevRule);
	  fprintf(fp,"<effdate>%s</effdate>\n",pszEffDate);
	  fprintf(fp,"</document></xml>\n");
	}

	fprintf(fp,"<SCRIPT src=\"%s\" type=text/javascript></SCRIPT>",pszScript);
}

void BNL_fei_xml_write_document
(
   FILE *fp,
   char *pszXMLOut,
   int   iMode,
   char *pszMode,
   char *pszRevRule,
   char *pszEffDate
)
{
  	fprintf(fp,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");

	// IR 1645275: Make it XML compatible
  //fprintf(fp,"<document type=\"%s\"/>\n",pszMode);
  fprintf(fp,"<document type=\"%s\">\n",pszMode);

	fprintf(fp,"<xml id=\"report\">");

	fprintf(fp,"%s",pszXMLOut);

	/*close the xml tag*/
	fprintf(fp,"</xml>\n");

	/*in case of bomrep extra configrule island*/
	if (iMode==BOMREP || iMode==BOMREPEXT || iMode==EBOMPACKEDREP || iMode==EBOMREP || iMode==BOMREPMFG)
	{
	  fprintf(fp,"<xml id=\"bomconfig\">\n");
	  fprintf(fp,"<revrule>%s</revrule>\n",pszRevRule);
	  fprintf(fp,"<effdate>%s</effdate>\n",pszEffDate);
	  fprintf(fp,"</xml>\n");
	}

  fprintf(fp,"</document>");
}


void BNL_get_element_props(t_XMLElement *pElement,int *iProps, char ***pszProps)
{

    t_XMLElement    *pProduct    =NULL;
    t_XMLElement    *pProp       =NULL;
    t_XMLElement    *pProps      =NULL;

    int             iCols        =0;
    char            **pszCols    =NULL;
    char            *pszColName  =NULL;

    /*
     <product>
        <props>
            <prop attr="id" colname="Product">000073/A</prop>
            <prop attr="object_desc" colname="Description">000073</prop>
            <table>border=&quot;1&quot;</table>
        </props>
    </product>
    <predecessor>
        <props>
            <prop attr="id" colname="Predecessor"> </prop>
            <table>border=&quot;1&quot;</table>
        </props>
    </predecessor>
    <diff>
        <change>o</change>
        <props>
            <prop attr="object_name" colname="Name">000073-A</prop>
            <prop attr="object_desc" colname="Description"></prop>
            <table>border=&quot;1&quot;</table>
        </props>
    </diff>
    ...
    */

    /*we use the product element to get the defined props*/


    pProps=BNL_xml_find_first(pElement,XML_EL_PROPS);

    if (pProps!=NULL)
    {
        pProp=BNL_xml_find_first(pProps,XML_EL_PROP);

        while (pProp)
        {
            iCols++;
            /*get colname attribute value*/
            BNL_xml_element_get_attribute_value(pProp,XML_ATTR_COLNAME,&pszColName);

            if (pszColName==NULL)
            {
                BNL_xml_element_get_attribute_value(pProp,XML_ATTR_ATTR,&pszColName);
            }

            if (pszCols==NULL)
            {
                pszCols=malloc(sizeof(char*));
                pszCols[iCols-1]=pszColName;

            }
            else
            {
                pszCols=realloc(pszCols,sizeof(char*)*iCols);
                pszCols[iCols-1]=pszColName;
            }

            pProp=BNL_xml_find_next(pProp,XML_EL_PROP);
        }
    }

   *iProps=iCols;
   *pszProps=pszCols;

}


void BNL_get_elements_of_name(t_XMLElement *pDocument,char *pszElement,int *iElements, t_XMLElement ***pElements)
{
    int iE=0;
    t_XMLElement **pE       =NULL;
    t_XMLElement *pElement  =NULL;

    pElement=BNL_xml_find_first(pDocument,pszElement);

    while (pElement)
    {
        iE++;
        if (pE==NULL)
        {
            pE=malloc(sizeof(t_XMLElement*)*iE);
            pE[iE-1]=pElement;
        }
        else
        {
            pE=realloc(pE,sizeof(t_XMLElement*)*iE);
            pE[iE-1]=pElement;
        }

        pElement=BNL_xml_find_next(pElement,pszElement);
    }

    *pElements=pE;
    *iElements=iE;

}

void BNL_get_table_format(t_XMLElement *pProduct,char **pszTableFormat)
{

    t_XMLElement *pProps        =NULL;
    t_XMLElement *pTableFormat  =NULL;


     pProps=BNL_xml_find_first(pProduct,XML_EL_PROPS);
     pTableFormat=BNL_xml_find_first(pProps,XML_EL_TABLE);

     if (pTableFormat!=NULL)
     {
       *pszTableFormat=BNL_strings_copy_string(pTableFormat->pszElementValue);
     }
     else
     {
         *pszTableFormat=BNL_strings_copy_string("");
     }

}


void BNL_write_table_header(FILE *fp,int iProps, char **pszProps)
{
    int p= 0;

    fprintf(fp,"<tr>");
    for(p=0;p<iProps;p++)
    {
        fprintf(fp,"<td><B>%s</B></td>",pszProps[p]);

    }

    fprintf(fp,"</tr>\n");

}

void BNL_get_uom(int iProps,t_XMLElement **pProps,char **pszUom)
{
    int   p             =0;
    char *pszAttr       =NULL;

    for (p=0;p<iProps;p++)
    {
        BNL_xml_element_get_attribute_value(pProps[p],XML_ATTR_ATTR,&pszAttr);
        if (strcmp(pszAttr,"bl_item_uom_tag")==0)
        {
            if (pProps[p]->pszElementValue!=NULL && pProps[p]->pszElementValue[0]=='\0')
            {
                *pszUom=NULL;
            }
            else
            {
              *pszUom=BNL_strings_copy_string(pProps[p]->pszElementValue);
            }
        }

        free(pszAttr);
    }

}

void BNL_write_table_row(FILE *fp,int iAsDiff,t_XMLElement *pProduct)
{
    int             p           =0;
    int             iProps      =0;
    t_XMLElement    *pEProps    =NULL;
    t_XMLElement    **pProps    =NULL;
    t_XMLElement    *pChange    =NULL;

    char *pszUom                =NULL;
    char *pszAttr               =NULL;
    char *pszTemp               =NULL;

    pEProps=BNL_xml_find_first(pProduct,XML_EL_PROPS);
    BNL_get_elements_of_name(pEProps,XML_EL_PROP,&iProps,&pProps);



    //totodo check in the props for a uom
    BNL_get_uom(iProps,pProps,&pszUom);


    fprintf(fp,"<tr>");

    if (iAsDiff)
    {
      //find the change
      pChange=BNL_xml_find_first(pProduct,XML_EL_CHANGE);

      BNL_xml_replace_chars_to_enitity_reference(pChange->pszElementValue, &pszTemp);
      //fprintf(fp,"<td>%s</td>",pChange->pszElementValue);
      fprintf(fp,"<td>%s</td>", pszTemp);

      if (pszTemp != NULL)
      {
        free(pszTemp);
        pszTemp = NULL;
      }
    }

    for (p=0;p<iProps;p++)
    {
      BNL_xml_replace_chars_to_enitity_reference(pProps[p]->pszElementValue, &pszTemp);

      /*todo if bl_quantity and there is no or empty uom and the value is null or empty then quantity is 1*/
      // JM|29-02-2012: Updated for uom is each
      BNL_xml_element_get_attribute_value(pProps[p],XML_ATTR_ATTR,&pszAttr);
      if((strcmp(pszAttr,"bl_quantity")==0) && (pszUom==NULL || strcmp(pszUom, "each") == 0) && (pProps[p]->pszElementValue==NULL || pProps[p]->pszElementValue[0]=='\0'))
      {
        fprintf(fp,"<td>1</td>");
      }
      else if((strcmp(pszAttr,"predquant")==0) && (pszUom==NULL || strcmp(pszUom, "each") == 0) && (pProps[p]->pszElementValue==NULL || pProps[p]->pszElementValue[0]=='\0'))
      {
        fprintf(fp,"<td>1</td>");
      }
      else
      {
        // For handling <BR> (compliance report)
        if (strstr(pszTemp, "&lt;BR&gt;") != NULL)
        {
          char *pszTemp1 = BNL_strings_replace_string(pszTemp, "&lt;BR&gt;", "<BR>");

          fprintf(fp,"<td>%s</td>", pszTemp1);

          if (pszTemp != NULL) free(pszTemp1);
        }
        else
        {
          fprintf(fp,"<td>%s</td>", pszTemp);
        }
      }

      free(pszAttr);

      if (pszTemp != NULL)
      {
        free(pszTemp);
        pszTemp = NULL;
      }
    }
    fprintf(fp,"</tr>\n");

    free(pProps);
    if (pszUom!=NULL) free(pszUom);
}

void BNL_create_table(FILE *fp,int iAsDiff,int iTableElements,t_XMLElement **pTableElements)
{
    int i                       =0;
    int iProps                  =0;
    int p                       =0;

    char *pszTableFormat        =NULL;
    char **pszProps             =NULL;
    char **pszAddProps          =NULL;

    if (iTableElements>0)
    {

            BNL_get_table_format(pTableElements[0],&pszTableFormat);

            if (iAsDiff)
            {

                pszProps=malloc(sizeof(char*));
                pszProps[0]=BNL_strings_copy_string("+\\-");
                BNL_get_element_props(pTableElements[0],&iProps,&pszAddProps);

                iProps++;

                pszProps=realloc(pszProps,sizeof(char*)*iProps);

                for (p=1;p<iProps;p++)
                {
                pszProps[p]=pszAddProps[p-1];

                }

                free(pszAddProps);
            }
            else
            {

            BNL_get_element_props(pTableElements[0],&iProps,&pszProps);

            }

            fprintf(fp,"<table %s>\n",pszTableFormat);

            BNL_write_table_header(fp,iProps,pszProps);

            for (i=0;i<iProps;i++)
            {
                free(pszProps[i]);
            }

            free(pszProps);

            for (i=0;i<iTableElements;i++)
            {
                BNL_write_table_row(fp,iAsDiff,pTableElements[i]);
            }

            fprintf(fp,"</table>\n");
    }
}

void BNL_create_prod_pred_table(FILE *fp,t_XMLElement *pProd, t_XMLElement *pPred)
{
    int p                           =0;
    int iProdProps                  =0;
    int iPredProps                  =0;
    char **pszProdProps             =NULL;
    char **pszPredProps             =NULL;
    t_XMLElement **pProdProps       =NULL;
    t_XMLElement **pPredProps       =NULL;
    t_XMLElement *pEProdProps       =NULL;
    t_XMLElement *pEPredProps       =NULL;
    char *pszTableFormat            =NULL;
    char *pszTemp                   =NULL;

    BNL_get_table_format(pProd,&pszTableFormat);

    BNL_get_element_props(pProd,&iProdProps,&pszProdProps);
    BNL_get_element_props(pPred,&iPredProps,&pszPredProps);


    fprintf(fp,"<table %s>\n",pszTableFormat);

    //head
    fprintf(fp,"<tr>");
     for (p=0;p<iProdProps;p++)
     {
         fprintf(fp,"<td><B>%s</B></td>",pszProdProps[p]);
         free(pszProdProps[p]);
     }

     for (p=0;p<iPredProps;p++)
     {
         fprintf(fp,"<td><B>%s</B></td>",pszPredProps[p]);
         free(pszPredProps[p]);
     }

     free(pszProdProps);
     free(pszPredProps);

    fprintf(fp,"</tr>\n");


    pEProdProps=BNL_xml_find_first(pProd,XML_EL_PROPS);
    pEPredProps=BNL_xml_find_first(pPred,XML_EL_PROPS);

    BNL_get_elements_of_name(pEProdProps,XML_EL_PROP,&iProdProps,&pProdProps);
    BNL_get_elements_of_name(pEPredProps,XML_EL_PROP,&iPredProps,&pPredProps);

    fprintf(fp,"<tr>");

    for (p=0;p<iProdProps;p++)
    {
      BNL_xml_replace_chars_to_enitity_reference(pProdProps[p]->pszElementValue, &pszTemp);

      //fprintf(fp,"<td>%s</td>",pProdProps[p]->pszElementValue);
      fprintf(fp,"<td>%s</td>", pszTemp);

      if (pszTemp != NULL)
      {
        free(pszTemp);
        pszTemp = NULL;
      }
    }

    for (p=0;p<iPredProps;p++)
    {
      BNL_xml_replace_chars_to_enitity_reference(pPredProps[p]->pszElementValue, &pszTemp);

      //fprintf(fp,"<td>%s</td>",pPredProps[p]->pszElementValue);

      fprintf(fp,"<td>%s</td>", pszTemp);

      if (pszTemp != NULL)
      {
        free(pszTemp);
        pszTemp = NULL;
      }
    }

    fprintf(fp,"</tr>\n");

    free(pProdProps);
    free(pPredProps);

    fprintf(fp,"</table>\n");
}

int BNL_check_for_error(FILE *fp,t_XMLElement *pDocument)
{
    t_XMLElement *pError        =NULL;


    pError=BNL_xml_find_first(pDocument,XML_EL_ERROR);

    if (pError!=NULL)
    {
        fprintf(fp,"<H1>Error<H1><BR>\n");
        fprintf(fp,"%s\n",pError->pszElementValue);
        return 1;
    }

    return 0;
}

/*------------------------------------------------------------------------------------------------------*/
void BNL_create_docs_rep(FILE *fp,t_XMLElement *pDocument)
{

    int iProps              =0;
    int iDiffs              =0;

    char *pszProps          =NULL;
    t_XMLElement **pDiffs    =NULL;

    t_XMLElement * pProduct =NULL;



    pProduct=BNL_xml_find_first(pDocument,XML_EL_PRODUCT);



    BNL_get_elements_of_name(pDocument,XML_EL_DIFF,&iDiffs,&pDiffs);

    fprintf(fp,"<html>\n<head></head>\n");
    fprintf(fp,"<H2> Documents of product </H2> <BR>\n");

    BNL_create_table(fp,0,1,&pProduct);

    fprintf(fp,"<BR>\n\n");

    BNL_create_table(fp,0,iDiffs,pDiffs);

    fprintf(fp,"</html>\n");

    free(pDiffs);
}

void BNL_create_attrdiff(FILE *fp, t_XMLElement *pDocument)
{

    int iProps              = 0;
    int iDiffs              = 0;

    char *pszProps          = NULL;
    t_XMLElement **pDiffs   = NULL;

    t_XMLElement * pProduct = NULL;
    t_XMLElement * pPredecessor = NULL;


    pProduct = BNL_xml_find_first(pDocument, XML_EL_PRODUCT);
    pPredecessor = BNL_xml_find_first(pDocument, XML_EL_PREDECESSOR);

    BNL_get_elements_of_name(pDocument, XML_EL_DIFF, &iDiffs, &pDiffs);

    fprintf(fp, "<html>\n<head></head>\n");
    fprintf(fp, "<H2> Change of Product Attributes </H2> <BR>\n");

    BNL_create_prod_pred_table(fp, pProduct, pPredecessor);

    fprintf(fp, "<BR>\n\n");

    if (iDiffs > 0)
    {
      BNL_create_table(fp, 0, iDiffs, pDiffs);

     fprintf(fp, "</html>\n");

      free(pDiffs);
    }
}

void BNL_create_attr_rep(FILE *fp, t_XMLElement *pDocument)
{

    int iProps              = 0;
    int iDiffs              = 0;

    char *pszProps          = NULL;
    t_XMLElement **pDiffs   = NULL;

    t_XMLElement * pProduct = NULL;



    pProduct = BNL_xml_find_first(pDocument, XML_EL_PRODUCT);

    BNL_get_elements_of_name(pDocument, XML_EL_DIFF, &iDiffs, &pDiffs);

    fprintf(fp, "<html>\n<head></head>\n");
    fprintf(fp, "<H2> Product Attributes </H2> <BR>\n");

    BNL_create_table(fp, 0, 1, &pProduct);

    fprintf(fp, "<BR>\n\n");

    if (iDiffs > 0)
    {
      BNL_create_table(fp, 0, iDiffs, pDiffs);

     fprintf(fp, "</html>\n");

      free(pDiffs);
    }
}

void BNL_create_succpred_rep(FILE *fp,t_XMLElement *pDocument)
{
  int iProps              = 0;
  int iSuccs              = 0;
  int iPreds              = 0;

  char *pszProps          = NULL;
  t_XMLElement **pSuccs   = NULL;
  t_XMLElement **pPreds   = NULL;

  t_XMLElement * pProduct =NULL;


  pProduct = BNL_xml_find_first(pDocument, XML_EL_PRODUCT);


  BNL_get_elements_of_name(pDocument, XML_EL_SUCC, &iSuccs, &pSuccs);
  BNL_get_elements_of_name(pDocument, XML_EL_PRED, &iPreds, &pPreds);

  fprintf(fp, "<html>\n<head></head>\n");
  fprintf(fp, "<H2> Successor/Predecessor </H2> <BR>\n");

  BNL_create_table(fp, 0, 1, &pProduct);

  fprintf(fp, "<BR>\n\n");

  if (iSuccs > 0)
  {
    fprintf(fp, "<H3> Successors</H3>\n");
    BNL_create_table(fp, 0, iSuccs, pSuccs);
    free(pSuccs);
  }

  if (iPreds > 0)
  {
    fprintf(fp, "<H3> Predessors</H3>\n");
    BNL_create_table(fp, 0, iPreds, pPreds);
    free(pPreds);
  }

  fprintf(fp, "</html>\n");
}

void BNL_create_doc_diff(FILE *fp, t_XMLElement *pDocument )
{
    int iProps                      =0;
    int iDiffs                      =0;
    char *pszProps                  =NULL;
    t_XMLElement **pDiffs           =NULL;
    t_XMLElement *pProduct          =NULL;
    t_XMLElement *pPredecessor      =NULL;



    pProduct=BNL_xml_find_first(pDocument,XML_EL_PRODUCT);
    pPredecessor=BNL_xml_find_first(pDocument,XML_EL_PREDECESSOR);

    BNL_get_elements_of_name(pDocument,XML_EL_DIFF,&iDiffs,&pDiffs);

    fprintf(fp,"<html>\n<head></head>\n");
    fprintf(fp,"<H2> Change of Documents</H2> <BR>\n");

    BNL_create_prod_pred_table(fp,pProduct,pPredecessor);

    fprintf(fp,"<BR>\n");

    BNL_create_table(fp,1,iDiffs,pDiffs);

    fprintf(fp,"</html>\n");

    free(pDiffs);

}


void BNL_create_bom_rep(char *pszTitle,FILE *fp,t_XMLElement *pDocument)
{

    int iProps              =0;
    int iDiffs              =0;

    char *pszProps                  =NULL;
    t_XMLElement **pDiffs           =NULL;

    t_XMLElement *pProduct          =NULL;
    t_XMLElement *pBomConfig        =NULL;
    t_XMLElement *pRevRule          =NULL;
    t_XMLElement *pEffDate          =NULL;

    pProduct=BNL_xml_find_first(pDocument,XML_EL_PRODUCT);
    pBomConfig=BNL_xml_find_first(pDocument,XML_EL_BOMCONFIG);
    pRevRule=BNL_xml_find_first(pBomConfig,XML_EL_REVRULE);
    pEffDate=BNL_xml_find_first(pBomConfig,XML_EL_EFFDATE);

    BNL_get_elements_of_name(pDocument,XML_EL_DIFF,&iDiffs,&pDiffs);

    fprintf(fp,"<html>\n<head></head>\n");
    fprintf(fp,"<H2> %s </H2> <BR>\n", pszTitle);

    fprintf(fp,"<B>Revision Rule: </B>%s<BR>\n",pRevRule->pszElementValue);

    if (pEffDate->pszElementValue!=NULL && strlen(pEffDate->pszElementValue) > 0)
    {
      fprintf(fp,"<B>Effective date: </B>%s<BR>\n",pEffDate->pszElementValue);
    }

    fprintf(fp,"<BR>\n");

    BNL_create_table(fp,0,1,&pProduct);

    fprintf(fp,"<BR>\n\n");

    BNL_create_table(fp,0,iDiffs,pDiffs);

    fprintf(fp,"</html>\n");

    free(pDiffs);

}


void BNL_create_eco_diff(FILE *fp,t_XMLElement *pDocument, int iMode)
{
    int c                   =0;
    int iDocComps           =0;
    int iBomComps           =0;
    int iAttrComps           =0;
    int iDocDiffs           =0;
    int iBomDiffs           =0;
    int iAttrDiffs           =0;

    t_XMLElement *pEco      =NULL;
    t_XMLElement *pEDocDiffs =NULL;
    t_XMLElement *pEBomDiffs =NULL;
    t_XMLElement *pEAttrDiffs =NULL;
    t_XMLElement *pProduct     =NULL;
    t_XMLElement *pPredecessor =NULL;
    t_XMLElement **pDocComps=NULL;
    t_XMLElement **pBomComps=NULL;
    t_XMLElement **pAttrComps=NULL;
    t_XMLElement **pDocDiffs =NULL;
    t_XMLElement **pBomDiffs =NULL;
    t_XMLElement **pAttrDiffs =NULL;




    fprintf(fp,"<html><head></head>\n");
    fprintf(fp,"<H2>Change Report</H2><BR>\n");


    pEco=BNL_xml_find_first(pDocument,XML_EL_ECO);
    pEDocDiffs=BNL_xml_find_first(pDocument,XML_EL_DOCDIFFS);
    pEBomDiffs=BNL_xml_find_first(pDocument,XML_EL_BOMDIFFS);
    pEAttrDiffs=BNL_xml_find_first(pDocument,XML_EL_ATTRDIFFS);


    //report the eco
    BNL_create_table(fp,0,1,&pEco);

    BNL_get_elements_of_name(pEDocDiffs,XML_EL_COMP,&iDocComps,&pDocComps);
    BNL_get_elements_of_name(pEBomDiffs,XML_EL_COMP,&iBomComps,&pBomComps);
    BNL_get_elements_of_name(pEAttrDiffs,XML_EL_COMP,&iAttrComps,&pAttrComps);

    /*there are always the same count of doc compares and bomcompares and they are in sync*/

    for (c=0;c<iDocComps;c++)
    {
        fprintf(fp,"<H3>Product change</H3>");

        //product and predeccesor info
        pProduct=BNL_xml_find_first(pDocComps[c],XML_EL_PRODUCT);
        pPredecessor=BNL_xml_find_first(pDocComps[c],XML_EL_PREDECESSOR);

        BNL_create_prod_pred_table(fp,pProduct,pPredecessor);

        BNL_get_elements_of_name(pDocComps[c],XML_EL_DIFF,&iDocDiffs,&pDocDiffs);
        BNL_get_elements_of_name(pBomComps[c],XML_EL_DIFF,&iBomDiffs,&pBomDiffs);
        BNL_get_elements_of_name(pAttrComps[c],XML_EL_DIFF,&iAttrDiffs,&pAttrDiffs);

        fprintf(fp,"<BR>\n");
        fprintf(fp,"<H3>Change of Document Lines</H3>\n");

        BNL_create_table(fp,1,iDocDiffs,pDocDiffs);
        fprintf(fp,"<H3>Changes of BOM Lines</H3>\n");
        BNL_create_table(fp,1,iBomDiffs,pBomDiffs);

        if (iMode == 1)
        {
          fprintf(fp,"<H3>Change of Product Attributes</H3>\n");
          BNL_create_table(fp, 0, iAttrDiffs, pAttrDiffs);
        }

        free(pDocDiffs);
        free(pBomDiffs);
        free(pAttrDiffs);

        fprintf(fp,"<BR><HR>\n");
    }

    free(pDocComps);
    free(pBomComps);
}


void BNL_create_comprep(FILE *fp, t_XMLElement *pDocument, char *pszConfig)
{
  int c                         = 0;
  int iStatusComps              = 0;
  int iAttrComps                = 0;
  int iBomComps                 = 0;
  int iStatusDiffs              = 0;
  int iAttrDiffs                = 0;
  int iBomDiffs                 = 0;

  t_XMLElement *pEco            = NULL;
  t_XMLElement *pStatus         = NULL;
  t_XMLElement *pAttr           = NULL;
  t_XMLElement *pBom            = NULL;
  t_XMLElement *pProduct        = NULL;

  t_XMLElement **pStatusComps   = NULL;
  t_XMLElement **pAttrComps     = NULL;
  t_XMLElement **pBomComps      = NULL;
  t_XMLElement **pStatusDiffs   = NULL;
  t_XMLElement **pAttrDiffs     = NULL;
  t_XMLElement **pBomDiffs      = NULL;


  fprintf(fp, "<html><head></head>\n");
  fprintf(fp, "<H2>Compliance Report</H2><BR>\n");

  fprintf(fp, "<B>Validation configuration: %s</B><BR><BR>\n", pszConfig);

  pEco = BNL_xml_find_first(pDocument,XML_EL_ECO);
  pStatus = BNL_xml_find_first(pDocument, XML_EL_STATUS);
  pAttr = BNL_xml_find_first(pDocument, XML_EL_ATTR);
  pBom = BNL_xml_find_first(pDocument, XML_EL_BOM);

  // Report the eco
  if (pEco)
  {
    BNL_create_table(fp, 0, 1, &pEco);
  }

  BNL_get_elements_of_name(pStatus, XML_EL_COMP, &iStatusComps, &pStatusComps);
  BNL_get_elements_of_name(pAttr, XML_EL_COMP, &iAttrComps, &pAttrComps);
  BNL_get_elements_of_name(pBom, XML_EL_COMP, &iBomComps, &pBomComps);

  /* There are always the same count and they are in sync*/

  for (c = 0; c < iStatusComps; c++)
  {
    fprintf(fp, "<H3>Product information</H3>");

    // Product info
    pProduct=BNL_xml_find_first(pStatusComps[c], XML_EL_PRODUCT);

    BNL_create_prod_pred_table(fp, pProduct, NULL);

    BNL_get_elements_of_name(pStatusComps[c], XML_EL_DIFF, &iStatusDiffs, &pStatusDiffs);
    BNL_get_elements_of_name(pAttrComps[c], XML_EL_DIFF, &iAttrDiffs, &pAttrDiffs);
    BNL_get_elements_of_name(pBomComps[c], XML_EL_DIFF, &iBomDiffs, &pBomDiffs);

    fprintf(fp, "<BR>\n");

    fprintf(fp, "<H3>Overview failing status checks</H3>\n");
    BNL_create_table(fp, 0, iStatusDiffs, pStatusDiffs);

    fprintf(fp, "<H3>Overview failing attribute checks</H3>\n");
    BNL_create_table(fp, 0, iAttrDiffs, pAttrDiffs);

    fprintf(fp, "<H3>Overview failing bom checks</H3>\n");
    BNL_create_table(fp, 0, iBomDiffs, pBomDiffs);

    free(pStatusDiffs);
    free(pAttrDiffs);

    fprintf(fp, "<BR><HR>\n");
  }
  free(pStatusComps);
  free(pAttrComps);
}


void BNL_create_bomdiff(FILE *fp, t_XMLElement *pDocument )
{
    int iProps                      =0;
    int iDiffs                      =0;
    char *pszProps                  =NULL;
    t_XMLElement **pDiffs           =NULL;
    t_XMLElement *pProduct          =NULL;
    t_XMLElement *pPredecessor      =NULL;

    pProduct=BNL_xml_find_first(pDocument,XML_EL_PRODUCT);
    pPredecessor=BNL_xml_find_first(pDocument,XML_EL_PREDECESSOR);

    BNL_get_elements_of_name(pDocument,XML_EL_DIFF,&iDiffs,&pDiffs);

    fprintf(fp,"<html>\n<head></head>\n");
    fprintf(fp,"<H2> Changes of Product Structure</H2> <BR>\n");

    BNL_create_prod_pred_table(fp,pProduct,pPredecessor);

    fprintf(fp,"<BR>\n");

    BNL_create_table(fp,1,iDiffs,pDiffs);

    fprintf(fp,"</html>\n");

    free(pDiffs);

}

int BNL_write_product_row(FILE *fp,int iHeader,t_XMLElement *pProduct)
{
    int iRetCode                =0;
    int p                       =0;
    int e                       =0;

    int iProdProps              =0;
    char **pszProdProps         =NULL;
    t_XMLElement **pProdProps   =NULL;

    int iEcos                   =0;
    t_XMLElement **pEcos        =NULL;

    int iEcoProps               =0;
    char **pszEcoProps          =NULL;
    t_XMLElement **pEcoProps    =NULL;

    t_XMLElement *pEecos        =NULL;
    t_XMLElement *pEProps       =NULL;

    char *pszTemp               =NULL;


     pEecos=BNL_xml_find_first(pProduct,XML_EL_ECOS);
     BNL_get_elements_of_name(pEecos,XML_EL_ECO,&iEcos,&pEcos);

     if (iHeader)
     {
        //header
        fprintf(fp,"<tr>");
            BNL_get_element_props(pProduct,&iProdProps, &pszProdProps);
            for(p=0;p<iProdProps;p++)
            {
                fprintf(fp,"<td><b>%s</b></td>",pszProdProps[p]);
                free(pszProdProps[p]);
            }
            free(pszProdProps);

            //now the header props of eco
            if (iEcos>0)
            {
                BNL_get_element_props(pEcos[0],&iEcoProps, &pszEcoProps);
                for(p=0;p<iEcoProps;p++)
                {
                fprintf(fp,"<td><b>%s</b></td>",pszEcoProps[p]);
                free(pszEcoProps[p]);
                }
                free(pszEcoProps);
            }
        fprintf(fp,"</tr>\n");
     }


     //now the values
     fprintf(fp,"<tr>");

        pEProps=BNL_xml_find_first(pProduct,XML_EL_PROPS);
        BNL_get_elements_of_name(pEProps,XML_EL_PROP,&iProdProps,&pProdProps);
        for (p=0;p<iProdProps;p++)
        {
          BNL_xml_replace_chars_to_enitity_reference(pProdProps[p]->pszElementValue, &pszTemp);

          //fprintf(fp,"<td>%s</td>",pProdProps[p]->pszElementValue);
          fprintf(fp,"<td>%s</td>", pszTemp);

          if (pszTemp != NULL)
          {
            free(pszTemp);
            pszTemp = NULL;
          }
        }

        //eco
        for (e=0;e<iEcos;e++)
        {
            pEProps=BNL_xml_find_first(pEcos[e],XML_EL_PROPS);
            BNL_get_elements_of_name(pEProps,XML_EL_PROP,&iEcoProps,&pEcoProps);

            if (e==0)
            {
                for (p=0;p<iEcoProps;p++)
                {
                  BNL_xml_replace_chars_to_enitity_reference(pEcoProps[p]->pszElementValue, &pszTemp);
                  //fprintf(fp,"<td>%s</td>",pEcoProps[p]->pszElementValue);
                  fprintf(fp,"<td>%s</td>", pszTemp);

                  if (pszTemp != NULL)
                  {
                    free(pszTemp);
                    pszTemp = NULL;
                  }
                }
            }
            else
            {
                for (p=0;p<iProdProps;p++)
                {
                    fprintf(fp,"<td></td>");

                }

                for (p=0;p<iEcoProps;p++)
                {
                  BNL_xml_replace_chars_to_enitity_reference(pEcoProps[p]->pszElementValue, &pszTemp);
                  //fprintf(fp,"<td>%s</td>",pEcoProps[p]->pszElementValue);
                  fprintf(fp,"<td>%s</td>", pszTemp);

                  if (pszTemp != NULL)
                  {
                    free(pszTemp);
                    pszTemp = NULL;
                  }
                }

            } //end of if e==0

            free(pEcoProps);
            iEcoProps=0;
        }//end of for e


    fprintf(fp,"</tr>\n");

    free(pProdProps);
    free(pEcos);

    return iRetCode;
}

int BNL_create_whereused_table(FILE *fp,t_XMLElement *pWhereUsed)
{
    int iRetCode            =0;
    int p                   =0;
    int iProducts           =0;



    t_XMLElement *pEproducts   =NULL;
    t_XMLElement **pProducts   =NULL;

    char *pszTableFormat        =NULL;


    /*incomming structure
     <whereused>
            <props></props>
            <ecos>
                <eco>
                <props></props>
                </eco>
            </ecos>
            <products>
                <product>
                    <props></props>
                    <ecos>
                        <eco>
                            <props>    </props>
                        </eco>
                    </ecos>
                </product>
            </products>
        </whereused>

        create table

         _______________________
        |    12nc   | -         |
        |-----------|-----------|
        |     -     |  WU       |
        |___________|___________|





    */

   // fprintf(fp,"<table>\n"); //main table

    //fprintf(fp,"<tr>\n"); //main table 12 nc

       // fprintf(fp,"<td>\n"); //main table 12 nc


            BNL_get_table_format(pWhereUsed,&pszTableFormat);
            fprintf(fp,"<table %s>\n",pszTableFormat);

            BNL_write_product_row(fp,1,pWhereUsed);
            fprintf(fp,"</table>\n");
        //fprintf(fp,"</td>\n"); //main table 12 nc

        //fprintf(fp,"<td>\n"); //main table 12 nc
        //empty
        //fprintf(fp,"</td>\n"); //main table 12 nc

    //fprintf(fp,"</tr>\n"); //main table 12 nc


    //fprintf(fp,"<tr>\n"); //main table wu

      //  fprintf(fp,"<td>\n"); //main table wu
        ///empty
        //fprintf(fp,"</td>\n"); //main table wu

        //fprintf(fp,"<td>\n"); //main table wu
            pEproducts=BNL_xml_find_first(pWhereUsed,XML_EL_PRODUCTS);
            BNL_get_elements_of_name(pEproducts,XML_EL_PRODUCT,&iProducts,&pProducts);

            fprintf(fp,"<br>\n");
            fprintf(fp,"<table %s>\n",pszTableFormat);
            for (p=0;p<iProducts;p++)
            {
                if (p==0)
                {
                  BNL_write_product_row(fp,1,pProducts[p]);
                }
                else
                {
                     BNL_write_product_row(fp,0,pProducts[p]);
                }
            }

            fprintf(fp,"</table>\n");
            free(pProducts);
            free(pszTableFormat);

        //fprintf(fp,"</td>\n"); //main table wu

    //fprintf(fp,"</tr>\n"); //main table wu

    //fprintf(fp,"</table>\n"); //main table

   return iRetCode;
}


int BNL_create_whereused(FILE *fp, t_XMLElement *pDocument, int iLevel, char *pszFilterType, char *pszRevRule)
{
    int iRetCode                 =0;
    int iProps                   =0;
    int iUsed                    =0;
    int u                        =0;

    t_XMLElement **pUsed         =NULL;
    t_XMLElement *pProps         =NULL;
    char *pszProps               =NULL;


    /* incomming structure
    <document>
        <whereused>
            <props></props>
            <ecos>
                <eco>
                <props></props>
                </eco>
            </ecos>
            <products>
                <product>
                    <props></props>
                    <ecos>
                        <eco>
                            <props>    </props>
                        </eco>
                    </ecos>
                </product>
            </products>
        </whereused>
    </document>

*/
    BNL_get_elements_of_name(pDocument,XML_EL_WHEREUSED,&iUsed,&pUsed);

    fprintf(fp,"<html>\n<head></head>\n");
    if (iLevel == -2)
    {
      fprintf(fp,"<H2>Where used top level</H2>\n");
      fprintf(fp,"<H3>Revision rule: %s</H3>\n", pszRevRule);
    }
    else
    {
      fprintf(fp,"<H2>Where used report</H2>\n");
      fprintf(fp,"<H3>Number of levels (-1 is all): %d</H3>\n", iLevel);
    }

    if (pszFilterType != NULL)
    {
      fprintf(fp,"<H3>Filter by Item Type: %s</H3><BR>\n", pszFilterType);
    }
    else
    {
      fprintf(fp,"<H3>Filter by Item Type: -</H3><BR>\n");
    }

    printf("Level [%d]\n", iLevel);

    for (u=0;u<iUsed;u++)
    {
        BNL_create_whereused_table(fp,pUsed[u]);

        fprintf(fp,"<BR>\n");

    }


    /*
    <document>
        <whereused>
        <props></props>
        <product>
         <props></props>
           <eco>
            <props>    </props>
           </eco>
        </product>
        </whereused>
    </document>

*/


    fprintf(fp,"</html>\n");


    return iRetCode;

}





/*
Function to parse the xml reports into HTML.
First it was setup in javascript.
*/
void BNL_fei_rep_parse_html
(
   FILE *fpReport,
   t_XMLElement *pDocument,
   int iMode,
   int iLevel,
   char *pszConfig,
   char *pszFilterType,
   char *pszRevRule
)
{
    if (BNL_check_for_error(fpReport,pDocument)) return;

    switch(iMode)
    {
        case BOMREP:
            BNL_create_bom_rep("Bill Of Material",fpReport,pDocument);
        break;

        case BOMREPEXT:
            BNL_create_bom_rep("Bill Of Material extended",fpReport,pDocument);
        break;

        case BOMREPMFG:
            BNL_create_bom_rep("Bill Of Material Compare",fpReport,pDocument);
        break;

        case EBOMPACKEDREP:
            BNL_create_bom_rep("Packed Electrical BOM",fpReport,pDocument);
        break;

        case EBOMREP:
            BNL_create_bom_rep("Unpacked Electrical BOM",fpReport,pDocument);
        break;

        case ECODIFF:
            BNL_create_eco_diff(fpReport,pDocument,0);
        break;

        case ECODIFF_EXT:
            BNL_create_eco_diff(fpReport,pDocument,1);
        break;

        case DOCSREP:
            BNL_create_docs_rep(fpReport,pDocument);
        break;

        case DOCDIFF:
            BNL_create_doc_diff(fpReport,pDocument);
        break;

        case BOMDIFF:
            BNL_create_bomdiff(fpReport,pDocument);
        break;

        case WHEREUSED:
        case WHEREUSEDTOP:
            BNL_create_whereused(fpReport,pDocument,iLevel,pszFilterType,pszRevRule);
        break;

        case ATTRDIFF:
          BNL_create_attrdiff(fpReport, pDocument);
        break;

        case ATTRREP:
           BNL_create_attr_rep(fpReport, pDocument);
        break;

        case SUCCPRED:
            BNL_create_succpred_rep(fpReport, pDocument);
        break;

        case COMPREP:
            BNL_create_comprep(fpReport, pDocument, pszConfig);
        break;

        case FRULISTITEM:
            BNL_create_bom_rep("FRUList Item Report",fpReport,pDocument);
        break;

        default:
        break;
    }

}/*end of int BNL_fei_rep_parse_html*/
