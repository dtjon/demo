/**
 (C) copyright by EDS PLM Solutions

 Description	:  Replaces the given new itemrevision with the given
                   old revision in all bomviews.


 Created for	:   EDS PLM Benelux


 Comments		:

 Filename       :   $Id: BNL_rp_module.h 456 2016-06-28 09:06:20Z tjonkonj $

 Version info   :   release.pointrelease (versions of the modules are stored in the module itself.

 History of changes
 reason /description                            Version By                  date
 ----------------------------------------------------------------------------------------------------------
 Creation                                       0.0                         04-07-2003
 Just updated after set first
  test proto                                    0.1                         29-07-2003
 Adde ebom reports                              0.2                         13-08-2008
 Updated BNL_rp_web_bomdiff and
 BNL_rp_web_eco_diffs                           0.3                         20-05-2009
 Added attrdiff report                          0.4                         11-06-2009
 Added level to BNL_rp_web_where_used           0.5                         12-06-2009
 Added product attributes report                0.6                         15-06-2009
 Added constants                                0.7                         15-06-2009
 Added successor/predecessor report             0.8                         16-06-2009
 Added DEFAULT_SUCCRELATION                     0.9                         18-06-2009
 Added new reports                              1.0                         08-03-2009
 Updated where_used                             1.1                         10-09-2009
 Added BNL_rp_web_comprep                       1.2                         21-09-2009
 Added BNL_rp_allowed_vals                      1.3                         10-11-2009
 Added BOMREPMFG                                1.4                         05-01-2010
 Added FRUList item report                      1.5                         26-03-2010
 Changed DEFAULT_PREDRELATION                   1.6                         04-11-2015
 Update for TC 10.1.*                           1.7     D.Tjon              31-05-2016
*/

#ifndef BNL_RP_H
#define BNL_RP_H

#ifdef  TC10_1
#include <tc/iman.h>
#else
#include <iman.h>
#endif

#include <BNL_xml_module.h>

#define DATASET "Dataset"
#define BOMVIEWREVISION "PSBOMViewRevision"
#define ITEMREVISION "ItemRevision"

#define BNL_FEI_REPORT_ATTR      "\\web\\htdocs\\iman\\fei\\fei_report_attr.xml"

#define BOMREP        1
#define ECODIFF       2
#define DOCSREP       3
#define DOCDIFF       4
#define BOMDIFF       5
#define WHEREUSED     6
#define EBOMPACKEDREP 7
#define EBOMREP       8
#define ATTRDIFF      10
#define BOMREPEXT     11
#define ATTRREP       12
#define SUCCPRED      13
#define ECODIFF_EXT   14
#define COMPREP       15
#define BOMREPMFG     16
#define WHEREUSEDTOP  17
#define FRULISTITEM   18

#define XML_EL_PRODUCT      "product"
#define XML_EL_PREDECESSOR  "predecessor"
#define XML_EL_PROP         "prop"
#define XML_ATTR_COLNAME    "colname"
#define XML_EL_PROPS        "props"
#define XML_EL_PROP         "prop"
#define XML_EL_TABLE        "table"
#define XML_EL_DIFFS        "diffs"
#define XML_EL_DIFF         "diff"
#define XML_EL_CHANGE       "change"
#define XML_EL_BOMCONFIG    "bomconfig"
#define XML_EL_REVRULE      "revrule"
#define XML_EL_EFFDATE      "effdate"
#define XML_ATTR_ATTR       "attr"
#define XML_EL_ECO          "eco"
#define XML_EL_DOCDIFFS     "docdiffs"
#define XML_EL_BOMDIFFS     "bomdiffs"
#define XML_EL_ATTRDIFFS    "attrdiffs"
#define XML_EL_COMP         "comp"
#define XML_EL_ERROR        "error"
#define XML_EL_DOCUMENT     "document"
#define XML_EL_LEVEL        "level"
#define XML_EL_WHEREUSED    "whereused"
#define XML_EL_ECOS         "ecos"
#define XML_EL_PRODUCTS     "products"
#define XML_EL_PACKED       "packed"
#define XML_EL_SUCC         "succ"
#define XML_EL_PRED         "pred"
#define XML_EL_STATUSDIFFS  "status"
#define XML_EL_ATTRIBUTESDIFFS "attribute"
#define XML_EL_STATUS       "status"
#define XML_EL_ATTR         "attribute"
#define XML_EL_BOM          "bom"
#define XML_EL_WSOSTATUS    "check-status-of-wso"
#define XML_EL_RELSTATUS    "check-relations-status"
#define XML_EL_BVRSTATUS    "check-status-in-bvr"
#define XML_EL_ATTR1        "check-attribute-1"
#define XML_EL_ATTR2        "check-attribute-2"
#define XML_EL_ATTR3        "check-attribute-3"
#define XML_EL_BOM_CHECK    "check-bom"
#define XML_EL_BOMCHECK     "bomcheck"
#define XML_EL_PARENT       "parent"
#define XML_EL_CHILD        "child"
#define XML_EL_ONECHILD     "one_child_check"


#define DEFAULT_PREDRELATION "CMSolutionToImpacted"
#define DEFAULT_SUCCRELATION "FEI_Successor_relation"
#define DEFAULT_VIEWTYPE     "view"
#define DEFAULT_REVRULE      "Latest Working"
#define DEFAULT_LEVEL        "-1"
#define ITEMREVISION_CLASS   "ItemRevision"
#define ITEM_CLASS           "Item"
#define DATASET_CLASS        "Dataset"
#define IMANFILE_CLASS       "ImanFile"
#define RELATION_2D3D        "IMAN_specification"
#define TYPE_2D              "UGPART"
#define TYPE_3D              "UGMASTER"
#define BNL_OBSOLETE_STATUS  "Obsolete"
#define BNL_RELEASED_STATUS  "Released"
#define BNL_SERVICE_STATUS   "Service"
#define BNL_PRODUCTION_STATUS "Production"
#define BNL_TECHOBS_STATUS   "TechOBS"
#define BNL_DIRECTMODEL_TYPE "DirectModel"

#define FEI_COMMERCIAL_SUBCLASS      "FEI_FRU_Commercial_Subclass"
#define FEI_FRU_COMMERCIAL_SUBCLASS  "FEI_FRU_Commercial_Subclass_values"
#define FEI_COMMERCIAL_ITEM          "FEI_FRU_Commercial_Item"
#define FEI_FRULIST_ITEM             "FEI_FRU_FRUList_Item"

#define STR_XMLHTML          "xmlhtml"
#define STR_XML              "xml"
#define STR_HTML             "html"

#define BNL_XMLHTML          1
#define BNL_XML              2
#define BNL_HTML             3

typedef struct sDiff
{
    tag_t  tObject;
    char *pszQuant;
    char *pszSeq;
    char *pszRefDes;
    int iChange;
    t_XMLElement *pXMLprops;
    struct sDiff *pPrev;
    struct sDiff *pNext;
}diffinfo_t;


typedef struct sArray
{
    tag_t tProduct;
    tag_t tPredecessor;
    diffinfo_t *ptDiffs;
    int iDiffsCount;
}array_t;


int BNL_rp_initialise
(
  FILE *pLog
);

int BNL_rp_set_log
(
  FILE *fpLog
);

int BNL_rp_get_documents
(
  tag_t tRev,
  char  *pszMode,
  int   *iDocCount,
  diffinfo_t **ptDocuments
);

int BNL_rp_get_bomview
(tag_t tRev,
 char *pszViewType ,
 tag_t *tBomView
);


int BNL_rp_bom_get_item_revs
(
  char       *pszMode,
  tag_t      tBomLine,
  int        iLevel,
  int        iMaxLevel,
  int        *iRevCount,
  diffinfo_t **tItemRevs

);

int BNL_rp_set_docdownload_extfilter
(
   int iExtTypes,
   char **pszExtTypes
);

int BNL_rp_set_docdownload_dsnamefilter
(
   int iDsNames,
   char **pszDsNames
);

int BNL_rp_set_downloadrevrule(char *pszRevRule);

int BNL_rp_compare_arrays
(
  int iCountOne,diffinfo_t *pArrayOne,
  int iCountTwo,diffinfo_t *pArrayTwo,
  int *iDiffCount,
  diffinfo_t  **pDiffs
);

int BNL_rp_bom_diff
(
  char *pszMode,
  tag_t tProduct,
  tag_t tPredecessor,
  char *pszViewType,
  char *pszRevRule,
  int  iLevel,
  int  *iDiffCount,
  diffinfo_t **pDiffs
);

int BNL_rp_document_diff
(
  char *pszMode,
  tag_t tProduct,
  tag_t tPredecessor,
  int   *iDiffCount,
  diffinfo_t **pDiffs
);

int BNL_rp_eco_diffs
(
   tag_t tEco,
   int  iLevel,
   char *pszRevRule,
   char *pszViewType,
   int *iAffDocCount,
   array_t **pAffDocDiffs,
   int *iAffBomCount,
   array_t **pAffBomDiffs,
   int *iAffAttrCount,
   array_t **pAffAttrDiffs
);

char* BNL_rp_get_full_id
(
  tag_t tRevision
);

int BNL_set_pred_quant
(
  t_XMLElement *pProps,
  char *pszOldQuant
);


int BNL_rp_get_predecessor
(
  tag_t tProduct,
  tag_t *tPredecessor
);

int BNL_rp_create_xml_diff
(
  diffinfo_t pDiff,
  t_XMLElement **pXMLDiff
);

int BNL_rp_create_xml_props
(
  tag_t tObject,
  char *pszTemplate,
  int iLevel,
  t_XMLElement **pDiff,
  char *pszObjType,
  char *pszAttrName,
  char *pszAttrValues,
  char *pszAllowedVal,
  char *pszErrorDescription
);

int BNL_rp_free_diff_struc
(
  int iCount,
  diffinfo_t *pDiff
);

int BNL_rp_free_array_struc
(
  int iArrays,
  array_t *ptArray
);

/*if pszParamter is NULL then a * is returned as parameter.*/
int BNL_rp_parse_parameter
(
 char *pszParameter,  //<I>
 int *iParameters,    //<O>
 char ***pszParameters //<OF>
);

int BNL_rp_set_docdownload_mode
(
  int iLogical
);

int BNL_rp_set_docdownload_skip_export
(
  int iLogical
);

int BNL_rp_set_pcb_skip_target
(
  int iLogical
);


/*do not free the given array after calling this func*/
int BNL_rp_set_docdownload_types
(
  int iDsTypes,char **pszDsTypes
);

/*do not free the given array after calling this func*/
int BNL_rp_set_docdownload_nrs
(
  int iNrs,char **pszNrs
);

/*do not free the given array after calling this func*/
int BNL_rp_set_docdownload_rels
(
  int iRels,char **pszRelations
);

/*do not free the given string after calling this func*/
int BNL_rp_set_downloaddir(char *pszDowloadDir);

int BNL_rp_set_onelevel_bomreps(int iLogical);


//***********The main functions to get the report in xml structure or xml string format

//iExportdocs to prevent be able not to export docs in case of docdownload,always set to 1.
//if 1 and docdownload ode then all is exported.
//iTop is only 1 in case of the first initiation.
int BNL_rp_web_bom
(
  int iMode,
  tag_t tRev,
  char *pszRevRule,
  char *pszEffDate,
  int iLevel,
  int iExportDocs,
  char **pszXMLrep,
  t_XMLElement **pDocument,
  int iTop
);

int BNL_rp_web_where_used
(
  int iLevel,
  int iMaxLevel,
  char *pszFilterType,
  char *pszRevRule,
  tag_t tProduct,
  char **pszXMLOut,
  t_XMLElement **pDocument
);
int BNL_rp_web_prod_attr
(
  tag_t tProduct,
  char **pszXMLdiff,
  t_XMLElement **pDocument
);
int BNL_rp_web_succ_pred
(
  tag_t tRev,
  char **pszXMLrep,
  t_XMLElement **pDocument
);
int BNL_rp_web_docdiff
(
  tag_t tProduct,
  tag_t tPredecessor,
  char **pszXMLdiff,
  t_XMLElement **pDocument
);
int BNL_rp_web_attrdiff
(
  tag_t tProd,
  tag_t tPred,
  char **pszXMLdiff,
  t_XMLElement **pDocument
);
int BNL_rp_web_bomdiff
(
  tag_t tProduct,
  tag_t tPredecessor,
  char *pszRevRule,
  char **pszXMLdiff,
  t_XMLElement **pDocument
);

int BNL_rp_web_docs
(
  tag_t tRev,
  char **pszXMLDiff,
  t_XMLElement **pDocument
);

int BNL_rp_web_eco_diffs
(
  tag_t tEco,
  char *pszRevRule,
  char **pszXMLdiff,
  t_XMLElement **pDocument
);

int BNL_rp_web_comprep
(
  int iTargets,
  tag_t *ptTargets,
  int iWorkflow,
  char *pszConfig,
  char **pszXMLdiff,
  t_XMLElement **pDocument,
  int *iResult
);

int BNL_rp_web_frulist_item
(
  tag_t tRev,
  char *pszRevRule,
  char **pszXMLrep,
  t_XMLElement **pDocument
);
int BNL_rp_get_frulist_item_report
(
  tag_t tRev,
  char *pszRevRule,
  char **pszXMLrep,
  array_t **ppArray,
  t_XMLElement **pDocument
 );
int BNL_rp_bom_frulist_item_lines
(
  tag_t      tBomLine,
  char       *pszCommItem,
  char       *pszFRUListItem,
  logical    lCommParent,
  logical    lFRUParent,
  int        iLevel,
  int        *iRevCount,
  diffinfo_t **tItemRevs
);
logical BNL_rp_get_bom_item
(
  tag_t tBomLine,
  tag_t *tBomItem
);
logical BNL_rp_process_CommItem
(
 tag_t IMFObj
 );
int BNL_rp_bom_write_frulist_item_line
(
  tag_t      tBomLine,
  int        iLevel,
  char       *szMode,
  int        iRevCount,
  diffinfo_t **tItemRevs
);


int BNL_rp_open_user_report();

int BNL_rp_close_user_report(char *pszMainReport);

int BNL_rp_get_bom_structure
(
  int iMode,
  tag_t tRev,
  char *pszRevRule,
  char *pszEffDate,
  int  iLevel,
  char **pszXMLrep,
  array_t **ppArray,
  t_XMLElement **pDocument
 );

int BNL_rp_is_released(tag_t tObject);

int BNL_rp_get_prop_type(tag_t tRev, char *pszAttrName, char *pszType, char **pszValue);

char *BNL_rp_allowed_vals(int iStats, char **pszStats);

#endif BNL_RP_H
