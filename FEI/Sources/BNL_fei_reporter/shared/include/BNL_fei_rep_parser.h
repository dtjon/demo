/**
 (C) copyright by UGS Solutions

 Filename       :   $Id: BNL_fei_rep_parser.h 426 2015-11-27 09:44:43Z tjonkonj $

 History of changes
 reason /description                            date
 ----------------------------------------------------------------------------------------------------------
 add BNL_ENGCHANGE_REVISION for IR-7542280      26-11-2015
**/

#include <bnl_xml_module.h>

#define BNL_ENGCHANGE_REVISION        "EngChange Revision"
#define BNL_REP_CHANGE_REV_TYPES_PREF "BNL_rep_change_rev_types"

void BNL_fei_write_xmlhtml
(
   FILE *fp,
   char *pszMode,
   char *pszXMLOut,
   int iMode,
   char *pszScript,
   char *pszRevRule,
   char *pszEffDate
);

void BNL_fei_xml_write_document
(
   FILE *fp,
   char *BNL_fei_write_xml,
   int  iMode,
   char *pszMode,
   char *pszRevRule,
   char *pszEffDate
);

void BNL_fei_rep_parse_html
(
   FILE *fpReport,
   t_XMLElement *pDocument,
   int iMode,
   int iLevel,
   char *pszConfig,
   char *pszFilterType,
   char *pszRevRule
);

void BNL_get_elements_of_name
(
   t_XMLElement *pDocument,
   char *pszElement,
   int *iElements,
   t_XMLElement ***pElements
);

void BNL_create_comprep(FILE *fp,t_XMLElement *pDocument, char *pszConfig);
