 /***********************************************************************************************************************
 (C) copyright by SIEMENS PLM

 Created by     :   J. Wijnja

 Description    :   This utility (un)sets the fru attribute
 Created for    :   FEI

 Comments       :   Every new procedure or function starts with FEI_

 Filename       :   $Id: FEI_process_fru_attr.c 471 2016-06-28 09:22:27Z tjonkonj $

 Version info   :   The number consist of the iman release and then the release of the handler
                    <release>.<pointrelease>


 History of changes
 reason /description                                                    Version     By          date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                                0.1        JWO         16-02-2010
 Always get the all secondary objs from services relation                0.2        JWO         05-03-2010
 if the secondary obj of the service relation is a comm item, the
   target is added to the FRUList of the service relation secondary
   item. Otherwise get the parent of the secondary item.                 0.3        JWO         11-03-2010

 Add all bomlines of the input item as input for the process             0.4        JWO         18-03-2010
 Use single level parent of the target item for all bomlines             0.5        JWO         31-03-2010
 Changed functionality --> if bomlines nas FRU not set than check
 the FRU of service primary object. if FRU yes, add it.                  0.6        JWO         09-04-2010
 The Seqno was left empty after the bomline is added.                    0.7        JWO         16-04-2010
 Added check if the target item is Comm item. If it is where used is
 not performed.                                                          0.7        JWO         16-04-2010
 If a new bom view is created the owning user is set to owning user
 of FRUList revision                                                     0.8        JWO         21-04-2010
 get the nex level parents till the commercial item parent found         0.9        JWO         14-05-2010
 Added mail preferences                                                  1.0        JWO         11-06-2010
 Do not collect comm item bomlines as input item                         1.0        JWO         11-06-2010
 process remove item from FRULIST parent and service relation check
 for all input items                                                     1.0        JWO         11-06-2010
 If the input item is comm item, it is not needed to perform where used
 to find comm item parent.                                               1.0        JWO         11-06-2010
 Set 0 to the bl_quantity of the new bomline                             1.1        JWO         19-06-2010
 Collect all parent Commercial items                                     1.2        JWO         01-07-2010
 when processing the commercial item the description check on
   FRUList item is processed. If the descriptions are not same the
   FRUList item description is updated.                                  1.3        JWO         08-07-2010
 Added check when adding to list                                         1.4        JM          22-07-2010
 Fixed issue with FEI_ITK_InputArgs                                      1.5        JM          27-07-2010
 Updated FEI_get_FRUList_bomlines                                        1.6        JM          27-07-2010
 Updated FEI_add_bomline                                                 1.7        JM          27-07-2010
 Error handling is updated                                               1.8        JWO         20-08-2010
 Added -bypass param                                                     1.8        JWO         20-08-2010
 Write access check added                                                1.8        JWO         20-08-2010
 New FRUList item revision is created if the latest rev has a status     1.8        JWO         20-08-2010
 Added changeownership and new status to the new FRUList revision        1.9        JWO         27-08-2010
 Added predecessor bomline check while collecting bomlines as input      1.9        JWO         27-08-2010
 Service relation check is also done if FRU attr is not set.             1.9        JWO         27-08-2010
 FRUList bomlines are not proccessed in collect inputs.                  2.0        JWO         03-09-2010
 FRUList item should be revised once                                     2.0        JWO         03-09-2010
 FEI_SERVICE_ITEM preference can have more values                        2.1        JWO         23-09-2010
 Bug fixing                                                              2.2        JWO         01-10-2010
 Bug fixing (status of the new FRUList item revision)                    2.3        JWO         07-10-2010
 Added functionality to create csp file for FRUItem revisions            2.3.1      JWO         01-11-2010
 Commented out FEI_remove_from_parent_FRUList                            2.3.2      JM          12-11-2010
 Updated FEI_check_comm_and_predecessor_status                           2.3.3      JM          12-11-2010
 Added functionality to assign a Project                                 2.4        JWO         12-10-2010
 Added preference FEI_FRU_obsolete_status_commercial                     2.5        JM          01-04-2011
 Reset attribute defined by pref FEI_MDR_MDRProcessed during revise      2.6        JM          06-12-2011
 Use TEMP instead IMAN_TMP_DIR                                           2.7        JM          16-01-2012
 changed define FEI_PREDECESSOR_REL                                      2.8        DTJ         04-11-2015
 ***********************************************************************************************************************/


#define ITK

#ifdef  TC10_1
#include <tccore/aom_prop.h>
#include <tccore/project.h>
#else
#include <item.h>
#include <sa.h>
#include <aom_prop.h>
#include <grm.h>
#include <pom.h>
#include <iman_date.h>
#include <bom.h>
#include <imantype.h>
#include <imanfile.h>
#include <project.h>
#endif

#include <BNL_fm_module.h>
#include <BNL_tb_module.h>
#include <BNL_csp_module.h>
#include <BNL_mm_module.h>
#include <BNL_em_module.h>
#include <BNL_register_bnllibs.h>

#define FEI_ITK_PARAM_ITEMID         "-itemid="
#define FEI_ITK_PARAM_REVID          "-revid="
#define FEI_ITK_PARAM_BOMLINES       "-bomlines="
#define FEI_ITK_PARAM_MAILRECP       "-mail_recipient="
#define FEI_ITK_PARAM_BYPASS         "-bypass="


#define FEI_FRU                      "FEI_FRU_FRUFlag"
#define FEI_MDRPROCESSED             "FEI_MDR_MDRProcessed"
#define FEI_SERVICE_ITEM             "FEI_FRU_Service_Item"
#define FEI_COMMERCIAL_ITEM          "FEI_FRU_Commercial_Item"
#define FEI_SERVICE_REL              "FEI_FRU_Service_Relation"
#define FEI_FRU_REVISION_RULE        "FEI_FRU_revision_rule"
#define FEI_FRULIST_ITEM             "FEI_FRU_FRUList_Item"
#define FEI_COMMERCIAL_SUBCLASS      "FEI_FRU_Commercial_Subclass"
#define FEI_FRU_COMMERCIAL_SUBCLASS  "FEI_FRU_Commercial_Subclass_values"
#define FEI_FRU_COMMERCIAL_STATUS    "FEI_FRU_allowed_status_commercial"
#define FEI_FRU_COMMERCIAL_OBSOLETE  "FEI_FRU_obsolete_status_commercial"
#define FEI_FRU_FUNCTIONAL_EMAIL     "FEI_FRU_Functional_email"

#define FEI_FRU_HAS_NO_FRULIST_MAIL_TEXT           "FEI_FRU_Has_No_FRUList_Mail_text"
#define FEI_FRU_HAS_NO_FRULIST_TEXT                "Items do not have a FRUList item"
#define FEI_FRU_NO_FRU_MAIL_SUBJECT                "FEI_FRU_No_FRUList_Mail_Subject"

#define FEI_ASSIGN_PROJECT           "FEI_FRU_Project"

#define FEI_IRMF_REL                 "IMAN_master_form"
#define FEI_IMF_REL                  "IMAN_master_form"
#define FEI_OWNING_SITE_ATTR         "owning_site"

#define FEI_PREDECESSOR_REL          "CMSolutionToImpacted"


FILE *feilog;
FILE *gfpErrorMail = NULL;

char **gpszFRU = NULL;
int giFRU = 0;

tag_t gtRevRule    = NULLTAG;
char *gpszRevRule = NULL;

char *gpszMDR = NULL;
char *gpszFRUListItem = NULL;
char *gpszCommItem = NULL;
char *gpszCommSubclass = NULL;
char *gpszErrorFile = NULL;

char *gpszMailNoFRUList = NULL;
logical glNoFRUMail = false;

int giFRUCommSubClasses = 0;
char **gpszFRUCommSubClasses = NULL;

int giFRUCommAllowedStatus = 0;
char **gpszFRUCommAllowedStatus = NULL;

int giFRUCommObsoleteStatus = 0;
char **gpszFRUCommObsoleteStatus = NULL;

char gszItemid[SA_name_size_c + 1] = "";
logical glBomlines = false;
logical glBypass = false;

tag_t *gptItems = NULL;
int  giItems    = 0;

/*FRUList item should be revised once.*/
tag_t* gptnewFRUListRev = NULLTAG;
int    ginewFRUListRev  = 0;

tag_t* gptoldFRUListRev = NULLTAG;
int    gioldFRUListRev  = 0;

char ** gppszStatus = NULL;
int giStatus = 0;

/* Version */
char BNL_module[] ="FEI_process_fru_attr 2.8";

char DLL_module[] ="FEI_process_fru_attr";

char DLL_logfilename[] = "Feifru";


static int FEI_ITK_Usage( void );
int FEI_ITK_InputArgs( int argc, char **ppszArgs, char *pszUser, char *pszGroup, char *pszPasswd, char *pszItemid, char *pszBomlines, char *pszMailRecp);
int FEI_get_secObj(tag_t tObj, char *pszRel, tag_t *tRelObj);
int FEI_get_bvr(tag_t tItemRev,tag_t tBv,tag_t *ptBvr);
logical FEI_get_bom_revision(tag_t tBomLine,tag_t *tBomRev);
int FEI_start_process(char *pszItemid, char *pszMailRecp);
int FEI_get_FRU_attr_value(tag_t tItem, char **pszFRUvalue);
int FEI_remove_from_parent_FRUList(tag_t tItem,char *pszFRU, tag_t *ptTagParents,int iParents);
int FEI_remove_service_item_from_parent_FRUList(tag_t tItem,int iLevel);
int FEI_item_has_service_rel(tag_t tObj, char *pszRel, int *iRel, tag_t **ptServiceObj, logical *lReturn, logical *lExist);
int FEI_get_FRUList_bomlines(tag_t tComItem,tag_t **ptFRUListBomLines, int *iFRUListBomLines);
int FEI_get_MDR_attr_value(tag_t tItem, logical *lMDRvalue);
logical FEI_check_comm_item_to_process(tag_t tComObj);
int FEI_process_item(tag_t tServiceItem,tag_t ttargetItem);
logical FEI_is_value_in_list(char *pszValue, int iCount, char **pszValues);
int FEI_add_bomline(tag_t tBv,tag_t parentItem,tag_t tObj);
int FEI_remove_bomline(tag_t tBv,tag_t parentItem,tag_t tObj);
int FEI_split_multiple_values(char *pszValue,char chSeparator,int  *iCount,char ***pszValues);
int FEI_send_mail(char *pszMailRecp,char *pszSub);
int FEI_create_body_text(char *pszBody, char **pszBodyFile);
int BNL_FEI_create_plot_request(tag_t tItemRev, int iExtraCols, char **pszExtraCols);
int BNL_create_dispatcher_request(tag_t tItemRev, char *pszPrefName, int iExtraCols, char **pszExtraCols);
int FEI_collect_input_items(tag_t tBomLine, tag_t tPredecessor);
int FEI_item_has_service_rel_primary(tag_t tObj, char *pszRel, int *iRel,tag_t **ptPrimObjs, logical *lReturn, logical *lExist);
int FEI_find_comm_parents(tag_t tItemRev, tag_t **tCommParents, int *iCommParents);
int FEI_send_error_mail(char *pszMailRecp);
int FEI_write_to_error_file(char *pszText);
int FEI_check_FRUListItem_to_revise(tag_t tFRUListRev, tag_t tItemTofind, char *pszFRU, logical *lRevise);
int FEI_check_comm_and_predecessor_status(tag_t tCommItem, logical *lStatus);
int FEI_send_revise_mail(tag_t tFRUListItem);
int FEI_find_comm_parent(tag_t tItemRev, tag_t *tCommParent);
int FEI_get_form(tag_t tObject, char *pszRel, tag_t *tForm);
int FEI_assign_to_project(tag_t tItem);
int FEI_reset_mdr_attr_value(tag_t tRev);

/**
int ITK_user_main()

  Description:

  Returns:

    0 on Ok

*/
int ITK_user_main( int iArgc , char **ppszArgs )
{
    int    i = 0;
    int iRetCode = 0;

    char szUser[SA_name_size_c + 1] = "";
    char szGroup[SA_name_size_c + 1] = "";
    char szPassword[SA_password_size_c + 1] = "";
    char szRevid[SA_name_size_c + 1] = "";
    char szBomLines[SA_name_size_c + 1] = "";
    char szMailRecp[SA_full_path_size_c+ 1] = "";

    tag_t   tagGroup    = NULLTAG;
    tag_t   tagRole     = NULLTAG;
    tag_t   tagUser     = NULLTAG;

    printf("\nBegin ... itk main\n");

    BNL_register_bnllibs(DLL_module,BNL_module,DLL_logfilename,&feilog);

    // check to see if the user passed no arguments or
    // asks for help before initializing ITK
    if( 2 == iArgc )
    {
        if( strcmp(ppszArgs[1], "-h") == 0 )
        {
            FEI_ITK_Usage();
            return 0;
        }
    }

    // Check for the commandline parameters
    iRetCode = FEI_ITK_InputArgs( iArgc, ppszArgs, szUser, szPassword, szGroup, gszItemid, szBomLines,szMailRecp);
    if( iRetCode != 0 )
    {
        FEI_ITK_Usage();
        return 0;
    }

    if (strcmp(szBomLines,"y") == 0)
    {
      glBomlines = true;
    }
    ITK_initialize_text_services(0);

    // Login into iMAN
#if defined(TC8000_3_0) || defined(TC10_1)
    iRetCode = TC_auto_login();
#else
    iRetCode = ITK_auto_login();
#endif
    if (BNL_em_error_handler("ITK_auto_login", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    if (glBypass == true)
    {
      iRetCode = ITK_set_bypass(true);
      if (iRetCode != ITK_ok)
      {
        fprintf(feilog,"Bypass not set !\n");
      }
      else
      {
        fprintf(feilog,"Bypass set !\n");
      }
    }

    //iRetCode=ITK_set_bypass (true);
    //if (BNL_em_error_handler("ITK_set_bypass",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;
    //fprintf(feilog,"Bypass set !\n");


    iRetCode = FEI_start_process(gszItemid,szMailRecp);
    if (BNL_em_error_handler("FEI_start_process", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    ITK_exit_module (TRUE);

    printf("End ... itk main\n");

    return iRetCode;
}    /* END MAIN */


/**
int FEI_ITK_Usage()

  Description:
    Prints out information on how to run the program.

  Returns:
    0 on Ok

*/
int FEI_ITK_Usage( void )
{
    printf( "\nProgram Syntax:\n\n" );
    printf( "fei_process_fru        [-h] [-u=<user> -p=<password> -g=<group>]\n" );
    printf( "                       -itemid=<item_id>  -bypass=<y|n> -bomlines=<y|n>  -mail_recipient=<Mail adress> \n" );
    printf( "Where:\n" );
    printf( "  -h                                       Display this help message\n" );
    printf( "  -u=<user_id>                             iMAN user ID. In most cases the iMAN user ID will\n" );
    printf( "                                           be infodba  or another iMAN  user ID with  system\n" );
    printf( "                                           administration privileges\n" );
    printf( "  -p=<password>                            iMAN password associated with the -u=<user_id>\n" );
    printf( "  -g=<group>                               iMAN group associated with the -u=<user_id>\n" );
    printf( "  -itemid=<item id>                        a valid item id\n" );
    printf( "  -bypass=<y|n>                            If y bypass will be set.\n");
    printf( "  -bomlines=<y|n>                          y or n. If y all level bomlines are also processed.\n" );
    printf( "  -mail_recipient=<Mail adress>            email adress.\n" );
    printf( "Notes:\n" );
    printf( "   1. -itemid is required.\n\n" );
    return ITK_ok;
}

/**
int FEI_ITK_InputArgs()

  Description:
    Checks out the input arguments to the program.

  Returns:
    0 on Ok

*/
int FEI_ITK_InputArgs( int argc, char **ppszArgs,
   char *pszUser, char *pszGroup, char *pszPasswd, char *pszItemid, char *pszBomLines,char *pszMailRecp)
{
   int iArg = 0;
   int iTempArgCount = 0;
   int iFoundArgCount = 0;
   char *pszArg;
   logical bProblem = FALSE;

   fprintf(feilog, "\nBegin ... FEI_ITK_InputArgs\n");

   // Get the argument for the type of processing.
   pszArg = ITK_ask_cli_argument( FEI_ITK_PARAM_ITEMID );
   //printf("pszArg %s\n", pszArg);
   if( pszArg != NULL )
   {
     strncpy(pszItemid, pszArg, SA_name_size_c );
     iFoundArgCount++;
   }


   if (iFoundArgCount < 1)
   {
     printf("\nRequired Arguments not passed ");
     printf("(%s is required)\n", FEI_ITK_PARAM_ITEMID);
     bProblem = TRUE;
     goto exit;
   }

   pszArg = ITK_ask_cli_argument(FEI_ITK_PARAM_BYPASS);
   //printf("pszArg %s\n", pszArg);
   if(pszArg != NULL)
   {
     if (_stricmp(pszArg,"y") == 0)
     {
       glBypass = true;
     }
   }

   // Get the argument for the type of processing.
   pszArg = ITK_ask_cli_argument( FEI_ITK_PARAM_BOMLINES );
   //printf("pszArg %s\n", pszArg);
   if( pszArg != NULL )
   {
     strncpy(pszBomLines, pszArg, SA_name_size_c );
     iFoundArgCount++;
   }

   // Get the mail recipient  parameter.
   pszArg = ITK_ask_cli_argument( FEI_ITK_PARAM_MAILRECP );
   if( pszArg != NULL )
   {
      strncpy(pszMailRecp, pszArg, SA_full_path_size_c );
      iFoundArgCount++;
   }


   // Get the user, group and password.
   pszArg = ITK_ask_cli_argument( "-u=" );
   if( pszArg != NULL )
   {
      strncpy(pszUser, pszArg, SA_name_size_c );
      iFoundArgCount++;
   }
   pszArg = ITK_ask_cli_argument( "-p=" );
   if( pszArg != NULL )
   {
      strncpy(pszPasswd, pszArg, SA_name_size_c );
      iFoundArgCount++;
   }
   pszArg = ITK_ask_cli_argument( "-g=" );
   if( pszArg != NULL )
   {
      strncpy(pszGroup, pszArg, SA_name_size_c );
      iFoundArgCount++;
   }



   // Check that the user, group and password arguments are correct.
   if( strcmp(pszUser, "") != 0 && (strcmp(pszGroup, "") == 0 || strcmp(pszPasswd, "") == 0 ) )
   {
      printf("\nIf userid (-u) is passed, both password and group (-p, -g) are also required.\n");
      bProblem = TRUE;
   }
   if( strcmp(pszUser, "") == 0 && (strcmp(pszGroup, "") != 0 || strcmp(pszPasswd, "") != 0 ) )
   {
      printf("\nIf group and pasword are passed (-g, -p), userid (-u) is also required.\n");
      bProblem = TRUE;
   }

   // Check for weird/typo'd arguments.
   /*if( argc > (iFoundArgCount + 1) )
   {
      printf("\nUnknown argument passed.\n");
      bProblem = TRUE;
   }*/

   fprintf(feilog, "End ... FEI_ITK_InputArgs\n");
exit:
   /* If there were problems detected, return 1. */
   if( bProblem )
   {
      return 1;
   }

   //if (pszArg != NULL) free(pszArg);
   return ITK_ok;
}

/**
int FEI_start_process()

  Description:
    start of the process

  Returns:
    0 on Ok

*/
int FEI_start_process(char *pszItemid, char *pszMailRecp)
{
  int iRetCode       = 0;
  int k              = 0;
  int it             = 0;
  int iParents       = 0;
  int iCommParents   = 0;

  int* piLevels      = 0;

  int iLenMail      = 0;
  int iServiceItem  = 0;

//  char szName[1024];
  char szObjType[1024];

  char **pszServiceItem = NULL;
  char *pszServiceRel = NULL;

  char *pszNOFRUListMailPrefText = NULL;
  char *pszNoFRUMailSub = NULL;

  char *pszTemp = NULL;

  tag_t tItemRev  = NULLTAG;
  tag_t tItem = NULLTAG;

  tag_t* ptTagParents = NULL;
  tag_t* ptCommParents = NULL;


  logical lFruvalue = false;
  char *pszFruvalue = NULL;

  logical lInputCommItem = false;

  fprintf(feilog, "\nBegin ... FEI_start_process\n");

  PREF_set_search_scope(IMAN_preference_site);

  iRetCode = PREF_ask_char_value(FEI_FRU, 0, &pszTemp);
  if (pszTemp != NULL)
  {
    fprintf(feilog, "%s: [%s] has value [%s]\n",BNL_module,FEI_FRU,pszTemp);
    FEI_split_multiple_values(pszTemp , ':', &giFRU, &gpszFRU);
  }
  else
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined. Exiting!!\n\n", BNL_module, FEI_FRU);
    goto exit;
  }

  iRetCode = PREF_ask_char_value(FEI_MDRPROCESSED, 0, &gpszMDR);
  if (gpszMDR != NULL)
  {
    fprintf(feilog, "%s: [%s] has value [%s]\n",BNL_module,FEI_MDRPROCESSED,gpszMDR);
  }
  else
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined. Exiting!!\n\n", BNL_module, FEI_MDRPROCESSED);
    goto exit;
  }

  PREF_ask_char_value(FEI_COMMERCIAL_ITEM, 0, &gpszCommItem);
  if (gpszCommItem == NULL)
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_COMMERCIAL_ITEM);
    goto exit;
  }
  else
  {
    if (strlen(gpszCommItem)<1)
    {
      fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_COMMERCIAL_ITEM);
      goto exit;
    }
  }
  fprintf(feilog, "%s: Preference [%s] has value [%s].\n\n", BNL_module, FEI_COMMERCIAL_ITEM, gpszCommItem);

  PREF_ask_char_values(FEI_SERVICE_ITEM, &iServiceItem, &pszServiceItem);
  fprintf(feilog, "%s: Found types [%d] using preference [%s].\n", BNL_module, iServiceItem,FEI_SERVICE_ITEM);

  if (iServiceItem == 0)
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_SERVICE_ITEM);
    goto exit;
  }

  /*PREF_ask_char_value(FEI_SERVICE_ITEM, 0, &pszServiceItem);
  if (pszServiceItem == NULL)
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_SERVICE_ITEM);
    goto exit;
  }
  else
  {
    if (strlen(pszServiceItem)<1)
    {
      fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_SERVICE_ITEM);
      goto exit;
    }
  }

  fprintf(feilog, "%s: Preference [%s] has value [%s].\n\n", BNL_module, FEI_SERVICE_ITEM, pszServiceItem);*/

  PREF_ask_char_value(FEI_SERVICE_REL, 0, &pszServiceRel);
  if (pszServiceRel == NULL)
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_SERVICE_REL);
    goto exit;
  }
  else
  {
    if (strlen(pszServiceRel)<1)
    {
      fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_SERVICE_REL);
      goto exit;
    }
  }
  fprintf(feilog, "%s: Preference [%s] has value [%s].\n\n", BNL_module, FEI_SERVICE_REL, pszServiceRel);

  PREF_ask_char_value(FEI_FRU_REVISION_RULE, 0, &gpszRevRule);
  if (gpszRevRule == NULL)
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_FRU_REVISION_RULE);
    goto exit;
  }
  else
  {
    if (strlen(gpszRevRule)<1)
    {
      fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_FRU_REVISION_RULE);
      goto exit;
    }
  }
  fprintf(feilog, "%s: Preference [%s] has value [%s].\n\n", BNL_module, FEI_FRU_REVISION_RULE, gpszRevRule);

  //getting revision rule
  iRetCode = CFM_find(gpszRevRule,&gtRevRule);
  if (gtRevRule == NULLTAG)
  {
    fprintf(feilog, "%s: Error, revision rule [%s] not found.\n\n", BNL_module, gpszRevRule);
    goto exit;
  }

  PREF_ask_char_value(FEI_FRULIST_ITEM, 0, &gpszFRUListItem);
  if (gpszFRUListItem == NULL)
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_FRULIST_ITEM);
  }
  else
  {
    if (strlen(gpszFRUListItem)<1)
    {
      fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, gpszFRUListItem);
    }
  }
  fprintf(feilog, "%s: Preference [%s] has value [%s].\n\n", BNL_module, FEI_FRULIST_ITEM, gpszFRUListItem);

  PREF_ask_char_value(FEI_COMMERCIAL_SUBCLASS, 0, &gpszCommSubclass);
  if (gpszCommSubclass == NULL)
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_COMMERCIAL_SUBCLASS);
  }
  else
  {
    if (strlen(gpszCommSubclass)<1)
    {
      fprintf(feilog, "%s: Warning, preference [%s] is not defined.\n\n", BNL_module, FEI_COMMERCIAL_SUBCLASS);
    }
  }
  fprintf(feilog, "%s: Preference [%s] has value [%s].\n\n", BNL_module, FEI_COMMERCIAL_SUBCLASS, gpszCommSubclass);

  //getting mail prefs
  PREF_ask_char_value(FEI_FRU_HAS_NO_FRULIST_MAIL_TEXT, 0, &pszNOFRUListMailPrefText);
  if (pszNOFRUListMailPrefText == NULL)
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined. Using [%s] \n\n", BNL_module, FEI_FRU_HAS_NO_FRULIST_MAIL_TEXT,FEI_FRU_HAS_NO_FRULIST_TEXT);
    //goto exit;
  }
  else
  {
    if (strlen(pszNOFRUListMailPrefText)<1)
    {
      fprintf(feilog, "%s: Warning, preference [%s] is not defined. Using [%s] \n\n", BNL_module, FEI_FRU_HAS_NO_FRULIST_MAIL_TEXT,FEI_FRU_HAS_NO_FRULIST_TEXT);

    }
  }

  PREF_ask_char_value(FEI_FRU_NO_FRU_MAIL_SUBJECT, 0, &pszNoFRUMailSub);
  if (pszNoFRUMailSub == NULL)
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined. Using [Items] \n\n", BNL_module, FEI_FRU_NO_FRU_MAIL_SUBJECT);
    //goto exit;
  }
  else
  {
    if (strlen(pszNoFRUMailSub)<1)
    {
      fprintf(feilog, "%s: Warning, preference [%s] is not defined. Using [Items] \n\n", BNL_module, FEI_FRU_NO_FRU_MAIL_SUBJECT);
    }
  }

  if (pszNOFRUListMailPrefText != NULL && strlen(pszNOFRUListMailPrefText) > 0)
  {
    iLenMail = (int)strlen(pszNOFRUListMailPrefText);
    gpszMailNoFRUList = MEM_alloc((sizeof(char)*(iLenMail+2)));
    strcpy(gpszMailNoFRUList, pszNOFRUListMailPrefText);
    strcat(gpszMailNoFRUList, "\n");
  }
  else
  {
    iLenMail = (int)strlen(FEI_FRU_HAS_NO_FRULIST_TEXT);
    gpszMailNoFRUList = MEM_alloc((sizeof(char)*(iLenMail+2)));
    strcpy(gpszMailNoFRUList, FEI_FRU_HAS_NO_FRULIST_TEXT);
    strcat(gpszMailNoFRUList, "\n");
  }


  PREF_ask_char_values(FEI_FRU_COMMERCIAL_SUBCLASS, &giFRUCommSubClasses, &gpszFRUCommSubClasses);
  fprintf(feilog, "%s: Found types [%d] using preference [%s].\n", BNL_module, giFRUCommSubClasses,FEI_FRU_COMMERCIAL_SUBCLASS);

  PREF_ask_char_values(FEI_FRU_COMMERCIAL_STATUS, &giFRUCommAllowedStatus, &gpszFRUCommAllowedStatus);
  fprintf(feilog, "%s: Found types [%d] using preference [%s].\n", BNL_module, giFRUCommAllowedStatus,FEI_FRU_COMMERCIAL_STATUS);

  PREF_ask_char_values(FEI_FRU_COMMERCIAL_OBSOLETE, &giFRUCommObsoleteStatus, &gpszFRUCommObsoleteStatus);
  fprintf(feilog, "%s: Found types [%d] using preference [%s].\n", BNL_module, giFRUCommObsoleteStatus, FEI_FRU_COMMERCIAL_OBSOLETE);

  EMH_clear_errors();

  //check if the itemid is given as input
  if (strlen(pszItemid) > 0)
  {
    iRetCode =  ITEM_find_item(pszItemid, &tItem);
    if (BNL_em_error_handler("ITEM_find_item", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
  }
  else
  {
    fprintf(feilog, "%s: Item id is empty !! \n",BNL_module);
    goto exit;
  }

  //collect the input
  if (tItem != NULLTAG)
  {
    char *psconf = NULL;

    char szId[256] ="";

    int iBvrs = 0;
    int i     = 0;

    tag_t tBview     = NULLTAG;
    tag_t tBomWindow = NULLTAG;
    tag_t tTopLine   = NULLTAG;

    tag_t *ptBvrs = NULL;

    fprintf(feilog, "%s: Item [%s] found \n",BNL_module, pszItemid);

    gptItems = (tag_t*) MEM_realloc(gptItems, sizeof(tag_t) * 1);
    giItems++;
    gptItems[giItems-1] = tItem;

    //check if the target item is commercial item. if yes do not get the parents
    iRetCode = BNL_tb_determine_object_type(tItem, szObjType);
    if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    iRetCode = CFM_item_ask_configured(gtRevRule, tItem,&tItemRev, &psconf);
    if (BNL_em_error_handler("CFM_item_ask_configured", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    if (strcmp(szObjType,gpszCommItem)==0)
    {
      ptTagParents = (tag_t*) MEM_realloc(ptTagParents, sizeof(tag_t) * 1);
      iParents++;
      ptTagParents[iParents-1] = tItemRev;
      lInputCommItem = true;
    }
    else
    {
      //get where used

      if (tItemRev != NULLTAG)
      {
        iRetCode = PS_where_used_configured(tItemRev, gtRevRule,1,&iParents,&piLevels,&ptTagParents);
        fprintf(feilog, "%s: Target Item [%s] is used in [%d] parents \n",BNL_module, pszItemid,iParents);

        iRetCode = FEI_find_comm_parents(tItemRev, &ptCommParents, &iCommParents);
        if (iRetCode !=ITK_ok)
        {
          char *pszmessage    = NULL;
          char szError[5012] = "";

          EMH_ask_error_text(iRetCode, &pszmessage);

          fprintf(feilog, "%s: Error (FEI_find_comm_parents) %s.\n", BNL_module, pszmessage);

          sprintf(szError,"Error occured processing FEI_find_comm_parents function : %s", pszmessage);

          iRetCode = FEI_write_to_error_file(szError);
          if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

          if (pszmessage) MEM_free(pszmessage);
        }
        fprintf(feilog, " Found [%d] Commercial item parents .\n", iCommParents);
      }
    }
    if (glBomlines == true)
    {
      if (tItemRev != NULLTAG)
      {
        iRetCode = ITEM_ask_id(tItem, szId);
        if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

        iRetCode = ITEM_rev_list_bom_view_revs(tItemRev, &iBvrs, &ptBvrs);
        if (BNL_em_error_handler("ITEM_rev_list_bom_view_revs", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

        fprintf(feilog, "\niBvrs [%d].\n", iBvrs);

        for (i=0; i<iBvrs; i++)
        {
          iRetCode = BNL_tb_determine_object_type(ptBvrs[i], szObjType);
          if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

          if (strcmp(szObjType,"view")==0)
          {
            tBview = ptBvrs[i];
            break;
          }
        }

        if (tBview != NULLTAG)
        {
          tag_t  tRel  = NULLTAG;
          tag_t  tPred = NULLTAG;

          tag_t* ptObjects = NULL;

          int iNum = 0;


          iRetCode = BOM_create_window(&tBomWindow);
          if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

          iRetCode = BOM_set_window_top_line(tBomWindow, NULLTAG, tItemRev, tBview , &tTopLine);
          if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

          /*Get the predecessor*/
          iRetCode = GRM_find_relation_type(FEI_PREDECESSOR_REL, &tRel);
          if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

          iRetCode =  GRM_list_secondary_objects_only(tItemRev, tRel, &iNum, &ptObjects);
          if (BNL_em_error_handler("GRM_list_secondary_objects_only", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

          fprintf(feilog, "%s: iNum [%d] \n",BNL_module, iNum);

          if (iNum > 0)
          {
            tPred = ptObjects[0];

            fprintf(feilog, "%s: Predecessor found \n",BNL_module);
          }

          iRetCode = FEI_collect_input_items(tTopLine,tPred);
          if (iRetCode != ITK_ok)
          {
            char *pszmessage    = NULL;
            char szError[5012] = "";

            EMH_ask_error_text(iRetCode, &pszmessage);

            fprintf(feilog, " %s: xx- Error(FEI_collect_input_items) [%s] \n", BNL_module);

            sprintf(szError,"Error occured collecting bomlines of item %s : %s", szId, pszmessage);

            iRetCode = FEI_write_to_error_file(szError);
            if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

            if (pszmessage) MEM_free(pszmessage);
          }

        }
      }

      if ( iBvrs>0) BNL_mm_free(ptBvrs);
    }
    else
    {
      fprintf(feilog, "%s: Processing only Item [%s] \n",BNL_module, pszItemid);
    }


    if ( psconf != NULL) BNL_mm_free(psconf);

  }
  else
  {
    fprintf(feilog, "%s: Item [%s] not found\n",BNL_module, pszItemid);
    goto exit;
  }

  fprintf(feilog, "%s: Found [%d] input items\n",BNL_module, giItems);

  for (it=0; it<giItems;it++)
  {
    char szId[256] = "";

    iRetCode = ITEM_ask_id(gptItems[it], szId);

    fprintf(feilog, "%s: processing [%d] input item id [%s] \n",BNL_module, (it + 1), szId);

    iRetCode = BNL_tb_determine_object_type(gptItems[it], szObjType);
    if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    fprintf(feilog, "%s: Getting FRU attr of item [%s] \n",BNL_module, szId);
    iRetCode = FEI_get_FRU_attr_value(gptItems[it], &pszFruvalue);
    if (iRetCode!= 0)
    {
      char *pszmessage    = NULL;
      char szError[5012] = "";

      EMH_ask_error_text(iRetCode, &pszmessage);

      fprintf(feilog, " %s: xx- Error(FEI_get_FRU_attr_value) [%s] \n", BNL_module);

      sprintf(szError,"Error occured processing item %s : %s", szId, pszmessage);

      iRetCode = FEI_write_to_error_file(szError);
      if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

      if (pszmessage) MEM_free(pszmessage);
    }
    else/*if (iRetCode !=ITK_ok) FEI_get_FRU_attr_value*/
    {
      if (pszFruvalue != NULL && (strcmp(pszFruvalue,gpszFRU[1]) == 0))
      {
        fprintf(feilog,"%s:Item [%s] has [%s] value true, checking item type\n", BNL_module, szId, gpszFRU[0]);
        //check if the item is service item
        //if (strcmp(szObjType, pszServiceItem)==0)
        if (FEI_is_value_in_list(szObjType, iServiceItem, pszServiceItem) == true)
        {
          tag_t *ptServiceItems = NULL;
          logical lReturn = false;
          logical lExist = false;
          int iRels = 0;

          fprintf(feilog, "%s: Item [%s] has type [%s] \n",BNL_module, szId, szObjType);

          //check service rel
          fprintf(feilog, "%s: Getting service relation of item [%s] \n",BNL_module, szId);

          if ((iRetCode = FEI_item_has_service_rel(gptItems[it], pszServiceRel, &iRels, &ptServiceItems, &lReturn, &lExist)) != 0)
          {
            char *pszmessage    = NULL;
            char szError[5012] = "";

            EMH_ask_error_text(iRetCode, &pszmessage);

            fprintf(feilog, "%s: Error (FEI_item_has_service_rel) %s.\n", BNL_module, pszmessage);

            sprintf(szError,"Error occured processing item %s : %s",szId, pszmessage);

            iRetCode = FEI_write_to_error_file(szError);
            if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

            if (pszmessage) MEM_free(pszmessage);
          }
          if (!lExist)
          {
            //fprintf(feilog, "%s: service relation of item [%s] not found, removing target item from single level where used FRUList\n",BNL_module, szId);
            fprintf(feilog, "%s: service relation of item [%s] not found.\n",BNL_module, szId);
            /*iRetCode = FEI_remove_from_parent_FRUList(gptItems[it],NULL,ptTagParents,iParents);
            if (iRetCode !=ITK_ok)
            {
              char *pszmessage    = NULL;
              char szError[5012] = "";

              EMH_ask_error_text(iRetCode, &pszmessage);

              fprintf(feilog, "%s: Error (FEI_remove_from_parent_FRUList) %s.\n", BNL_module, pszmessage);

              sprintf(szError,"Error occured processing item %s : %s",szId, pszmessage);

              iRetCode = FEI_write_to_error_file(szError);
              if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

              if (pszmessage) MEM_free(pszmessage);
            }  */
          }
          else
          {
            int is = 0;
            char szServType[256]="";
            fprintf(feilog, "%s: Found [%d] Service relation items. Processing service relation items.\n",BNL_module, iRels);

            fprintf(feilog, "%s: Checking type of service relation items.\n",BNL_module);
            for (is=0;is<iRels;is++)
            {
              char szCommId[256]="";
              tag_t tServItemRev = NULLTAG;
              char szServiceItemName[256]="";
              char *psconf = NULL;
              int iPs = 0;
              tag_t *ptTParents   = NULL;
              int *piLevels2    = NULL;
              int i = 0;

              iRetCode =ITEM_ask_id(ptServiceItems[is],szCommId);
              if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;
              //check if found service relation item is a commercial item.
              iRetCode = BNL_tb_determine_object_type(ptServiceItems[is], szServType);
              if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;
              if (strcmp(szServType,gpszCommItem)==0)
              {
                fprintf(feilog, "%s: Commercial type service relation item found, process commercial item [%s].\n",BNL_module,szCommId);
                iRetCode =FEI_process_item(ptServiceItems[is],gptItems[it]);
                if (iRetCode !=ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (FEI_process_item) %s.\n", BNL_module, pszmessage);

                  sprintf(szError,"Error occured processing item %s : %s",szCommId, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                }
              }
              else
              {
                fprintf(feilog, " %s: Service relation item [%s] is not a [%s].\n", BNL_module,szCommId,gpszCommItem);
                //get the name of the service item
                iRetCode =ITEM_ask_id(ptServiceItems[is],szServiceItemName);
                if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                fprintf(feilog, "Getting where used of service relation item [%s].\n",szServiceItemName);
                //get item revision with revision rule
                iRetCode = CFM_item_ask_configured(gtRevRule, ptServiceItems[is],&tServItemRev, &psconf);
                if (BNL_em_error_handler("CFM_item_ask_configured", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;
                if (tServItemRev != NULLTAG)
                {
                  // Perform all Level Where-Used
                  iRetCode = PS_where_used_configured(tServItemRev, gtRevRule,-1,&iPs,&piLevels2,&ptTParents);
                  if (BNL_em_error_handler("PS_where_used_configured", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;
                  if (iPs > 0)
                  {
                    tag_t tParentItem = NULLTAG;

                    fprintf(feilog, "%s: Found [%d] parents of item [%s].\n", BNL_module, iParents, szServiceItemName);

                    for (i=0; i<iPs; i++)
                    {
                      char szPId[256]="";

                      tParentItem = NULLTAG;
                      //get the item
                      iRetCode = ITEM_ask_item_of_rev(ptTParents[i], &tParentItem);
                      if (tParentItem != NULLTAG)
                      {
                        iRetCode = ITEM_ask_id(tParentItem, szPId);
                        if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                        iRetCode = BNL_tb_determine_object_type(tParentItem, szObjType);
                        if (iRetCode != ITK_ok)
                        {
                          fprintf(feilog, "%s: Error by parents BNL_tb_determine_object_type.\n", BNL_module);
                        }
                        else
                        {
                          fprintf(feilog, " %s: szObjType [%s]\n", BNL_module, szObjType);
                          if (strcmp(szObjType,gpszCommItem)==0)
                          {
                            fprintf(feilog, " Commercial item found., checking it to process\n");
                            iRetCode =FEI_process_item(tParentItem,gptItems[it]);
                            if (iRetCode !=ITK_ok)
                            {
                              char *pszmessage    = NULL;
                              char szError[5012] = "";

                              EMH_ask_error_text(iRetCode, &pszmessage);

                              fprintf(feilog, "%s: Error (FEI_process_item) %s.\n", BNL_module, pszmessage);

                              sprintf(szError,"Error occured processing item %s : %s",szPId, pszmessage);

                              iRetCode = FEI_write_to_error_file(szError);
                              if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                              if (pszmessage) MEM_free(pszmessage);
                            }
                          }
                          else
                          {
                            fprintf(feilog, " %s: Parent item [%d] is not a [%s]\n", BNL_module, i, gpszCommItem);
                          }
                        }
                      }
                      else
                      {
                        fprintf(feilog, "%s: Error by parents ITEM_ask_item_of_rev .\n", BNL_module);
                      }
                    }
                  }
                  else
                  {
                    fprintf(feilog, "%s: No parents found for item[%s].\n", BNL_module, szServiceItemName);
                  }
                  if ( ptTParents != NULL) BNL_mm_free(ptTParents);
                  if ( piLevels2 != NULL) BNL_mm_free(piLevels2);
                  if ( psconf != NULL) BNL_mm_free(psconf);

                }

              }
            }
          }
          if (iRels>0) MEM_free(ptServiceItems);

        }/*if (strcmp(szObjType, pszServiceItem)==0)*/
        //else
        {
          char szTargetItemName[256]="";
          int i = 0;
          int iRels =0;
          tag_t *ptServicePrimItems = NULL;

          logical lReturn = false;
          logical lExist = false;

          //fprintf(feilog, "%s: Item is not one of the items in preference  [%s], processing item\n",BNL_module, FEI_SERVICE_ITEM);
      fprintf(feilog, "%s: Processing item...\n",BNL_module);

          iRetCode = WSOM_ask_name(tItem,szTargetItemName);
          if (BNL_em_error_handler("WSOM_ask_name", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

          fprintf(feilog, "Getting where used of item [%s].\n",szTargetItemName);

          if (tItemRev != NULLTAG)
          {
            //perform where used till find a commercial parent

            int iC           = 0;

            tag_t tParentItem    = NULLTAG;
            char szPid[256] = "";

            /*if the input item is a comm item, it is not needed to perform where use to find comm item parent.*/

            if (lInputCommItem == true)
            {
              tParentItem = NULLTAG;
              fprintf(feilog, " The input item is Commercial item, using it as parent.\n");

              //get the item
              iRetCode = ITEM_ask_item_of_rev(ptTagParents[i], &tParentItem);
              if (tParentItem != NULLTAG)
              {
                iRetCode = ITEM_ask_id(tParentItem, szPid);
                if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                iRetCode =FEI_process_item(tParentItem,gptItems[it]);
                if (iRetCode !=ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (FEI_process_item) %s.\n", BNL_module, pszmessage);

                  sprintf(szError,"Error occured processing item %s : %s",szPid, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                }
              }
            }
            else
            {
              fprintf(feilog, "%s: perform where used till find a [%s] parent.\n", BNL_module, gpszCommItem);

              for (iC=0; iC<iCommParents; iC++)
              {
                char szPid[256] = "";

                iRetCode = ITEM_ask_id(ptCommParents[iC], szPid);
                if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                iRetCode =FEI_process_item(ptCommParents[iC],gptItems[it]);
                if (iRetCode !=ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (FEI_process_item) %s.\n", BNL_module, pszmessage);

                  sprintf(szError,"Error occured processing item %s : %s",szPid, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                }
              }
            }

            // check if item is a prim obj of services rel
            // yes, check fru of the secondaty obj
            // fru yes --> add the secondaty obj to the frulist component of the comm parent
            // fru no --> do nothing
            fprintf(feilog, "%s: Getting service relation primary objects of item [%s] \n",BNL_module, szId);
            if ((iRetCode = FEI_item_has_service_rel_primary(gptItems[it],pszServiceRel, &iRels,&ptServicePrimItems, &lReturn, &lExist)) != 0)
            {
              char *pszmessage    = NULL;
              char szError[5012] = "";

              EMH_ask_error_text(iRetCode, &pszmessage);

              fprintf(feilog, "%s: Error (FEI_item_has_service_rel_primary) %s.\n", BNL_module, pszmessage);

              sprintf(szError,"Error occured processing item %s : %s",szId, pszmessage);

              iRetCode = FEI_write_to_error_file(szError);
              if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

              if (pszmessage) MEM_free(pszmessage);
            }
            else
            {
              int is = 0;
              char szServType[256]="";
              fprintf(feilog, "%s: Found [%d] Service relation primary items of item [%s]. Processing items.\n",BNL_module, iRels, szId);

              if (iRels > 0)
              {
                fprintf(feilog, "%s: Checkin FRU \n",BNL_module);
              }
              for (is=0;is<iRels;is++)
              {
                char szPrimId[256] = "";

                iRetCode = ITEM_ask_id(ptServicePrimItems[is], szPrimId);

                fprintf(feilog, "%s: processing [%d] primary object item id [%s] \n",BNL_module, is, szPrimId);

                fprintf(feilog, "%s: Getting FRU attr of item [%s] \n",BNL_module, szPrimId);
                iRetCode = FEI_get_FRU_attr_value(ptServicePrimItems[is], &pszFruvalue);
                if (iRetCode !=ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (FEI_get_FRU_attr_value) %s.\n", BNL_module, pszmessage);

                  sprintf(szError,"Error occured processing item %s : %s",szPrimId, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                  //goto exit;
                }
                else/*if (iRetCode !=ITK_ok) FEI_get_FRU_attr_value*/
                {
                  if (pszFruvalue != NULL && (strcmp(pszFruvalue,gpszFRU[1]) == 0))
                  {
                    char szTargetItemName[256]="";
                    fprintf(feilog,"%s:Item [%s] has [%s] value true, adding item to the FRUList item component of the commercial item parent \n", BNL_module, szPrimId, gpszFRU[0]);

                    WSOM_ask_name(tItem,szTargetItemName);
                    fprintf(feilog, "Getting where used of item [%s].\n",szTargetItemName);

                    if (tItemRev != NULLTAG)
                    {
                      // Perform a One Level Where-Used
                      if (iParents > 0)
                      {
                        tag_t tParentItem = NULLTAG;
                        int i = 0;
                        char szPid[256] = "";

                        fprintf(feilog, "%s: Found [%d] parents of item [%s].\n", BNL_module, iParents, szTargetItemName);
                        for (i=0; i<iParents; i++)
                        {
                          tParentItem = NULLTAG;
                          //get the item
                          iRetCode = ITEM_ask_item_of_rev(ptTagParents[i], &tParentItem);
                          if (tParentItem != NULLTAG)
                          {
                            iRetCode = ITEM_ask_id(tParentItem, szPid);
                              if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                            iRetCode = BNL_tb_determine_object_type(tParentItem, szObjType);
                            if (iRetCode != ITK_ok)
                            {
                              fprintf(feilog, "%s: Error by parents BNL_tb_determine_object_type.\n", BNL_module);
                            }
                            else
                            {
                              fprintf(feilog, " %s: szObjType [%s]\n", BNL_module, szObjType);
                              if (strcmp(szObjType,gpszCommItem)==0)
                              {
                                fprintf(feilog, " Commercial item found.\n");
                                iRetCode =FEI_process_item(tParentItem,ptServicePrimItems[is]);
                                if (iRetCode !=ITK_ok)
                                {
                                  char *pszmessage    = NULL;
                                  char szError[5012] = "";

                                  EMH_ask_error_text(iRetCode, &pszmessage);

                                  fprintf(feilog, "%s: Error (FEI_process_item) %s.\n", BNL_module, pszmessage);

                                  sprintf(szError,"Error occured processing item %s : %s",szPid, pszmessage);

                                  iRetCode = FEI_write_to_error_file(szError);
                                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                                  if (pszmessage) MEM_free(pszmessage);
                                }
                              }
                              else
                              {
                                fprintf(feilog, " %s: Parent item [%d] is not a [%s]\n", BNL_module, i, gpszCommItem);
                              }
                            }
                          }
                          else
                          {
                            fprintf(feilog, "%s: Error by parents ITEM_ask_item_of_rev .\n", BNL_module);
                          }
                        }
                      }
                    }
                  }
                  else
                  {
                    fprintf(feilog, "%s: primary object item id [%s] has FRU flag not set\n",BNL_module, szPrimId);
                  }
                }
              } /*for (is=0;is<iRels;is++)*/
            }
          }
        }

      }
      else/*if (lFruvalue == true)*/
      {
        tag_t *ptServicePrimItems = NULL;
        logical lReturn = false;
        logical lExist = false;
        int iRels = 0;

        fprintf(feilog, "%s: [%s] is false, removing item from single level where used FRUList parent\n",BNL_module, gpszFRU[0]);

        if (iParents > 0)
        {
          char szPid[256] = "";

          iRetCode = ITEM_ask_id(gptItems[it], szPid);
          if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

          iRetCode = FEI_remove_from_parent_FRUList(gptItems[it],NULL,ptTagParents,iParents);

          if (iRetCode !=ITK_ok)
          {
            char *pszmessage    = NULL;
            char szError[5012] = "";

            EMH_ask_error_text(iRetCode, &pszmessage);

            fprintf(feilog, "%s: Error (FEI_remove_from_parent_FRUList) %s.\n", BNL_module, pszmessage);

            sprintf(szError,"Error occured processing FEI_remove_from_parent_FRUList function of item %s : %s",szPid, pszmessage);

            iRetCode = FEI_write_to_error_file(szError);
            if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

            if (pszmessage) MEM_free(pszmessage);
          }
        }


        // check if item is a prim obj of services rel
        // yes, check fru of the secondaty obj
        // fru yes --> add the secondaty obj to the frulist component of the comm parent
        // fru no --> do nothing

        fprintf(feilog, "%s: Getting service relation secondary objects of item [%s] \n",BNL_module, szId);
        if ((iRetCode = FEI_item_has_service_rel_primary(gptItems[it],pszServiceRel, &iRels,&ptServicePrimItems, &lReturn, &lExist)) != 0)
        {
          char *pszmessage    = NULL;
          char szError[5012] = "";

          EMH_ask_error_text(iRetCode, &pszmessage);

          fprintf(feilog, "%s: Error (FEI_item_has_service_rel_primary) %s.\n", BNL_module, pszmessage);

          sprintf(szError,"Error occured processing item %s : %s",szId, pszmessage);

          iRetCode = FEI_write_to_error_file(szError);
          if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

          if (pszmessage) MEM_free(pszmessage);
        }
        else
        {
          int is = 0;
          char szServType[256]="";
          fprintf(feilog, "%s: Found [%d] Service relation secondary items of item [%s]. Processing items.\n",BNL_module, iRels, szId);

          fprintf(feilog, "%s: Checkin FRU \n",BNL_module);
          for (is=0;is<iRels;is++)
          {
            char szPrimId[256] = "";

            iRetCode = ITEM_ask_id(ptServicePrimItems[is], szPrimId);

            fprintf(feilog, "%s: processing [%d] secondary object item id [%s] \n",BNL_module, is, szPrimId);

            fprintf(feilog, "%s: Getting FRU attr of item [%s] \n",BNL_module, szPrimId);
            iRetCode = FEI_get_FRU_attr_value(ptServicePrimItems[is], &pszFruvalue);
            if (iRetCode !=ITK_ok)
            {
              char *pszmessage    = NULL;
              char szError[5012] = "";

              EMH_ask_error_text(iRetCode, &pszmessage);

              fprintf(feilog, "%s: Error (FEI_get_FRU_attr_value) %s.\n", BNL_module, pszmessage);

              sprintf(szError,"Error occured processing item %s : %s",szPrimId, pszmessage);

              iRetCode = FEI_write_to_error_file(szError);
              if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

              if (pszmessage) MEM_free(pszmessage);
              //goto exit;
            }
            else/*if (iRetCode !=ITK_ok) FEI_get_FRU_attr_value*/
            {
              if (pszFruvalue != NULL && (strcmp(pszFruvalue,gpszFRU[1]) == 0))
              {
                char szTargetItemName[256]="";
                fprintf(feilog,"%s:Item [%s] has [%s] value true, adding item to the FRUList item component of the commercial item parent \n", BNL_module, szPrimId, gpszFRU[0]);

                WSOM_ask_name(tItem,szTargetItemName);
                fprintf(feilog, "Getting where used of item [%s].\n",szTargetItemName);

                if (tItemRev != NULLTAG)
                {
                  if (lInputCommItem == true)
                  {
                    char szPid[256] = "";
                    tag_t tParentItem = NULLTAG;
                    fprintf(feilog, " The input item is Commercial item, using it as parent.\n");

                    //get the item
                    iRetCode = ITEM_ask_item_of_rev(ptTagParents[0], &tParentItem);
                    if (tParentItem != NULLTAG)
                    {
                      iRetCode = ITEM_ask_id(tParentItem, szPid);
                      if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                      iRetCode =FEI_process_item(tParentItem,ptServicePrimItems[is]);
                      if (iRetCode !=ITK_ok)
                      {
                        char *pszmessage    = NULL;
                        char szError[5012] = "";

                        EMH_ask_error_text(iRetCode, &pszmessage);

                        fprintf(feilog, "%s: Error (FEI_process_item) %s.\n", BNL_module, pszmessage);

                        sprintf(szError,"Error occured processing item %s : %s",szPid, pszmessage);

                        iRetCode = FEI_write_to_error_file(szError);
                        if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                        if (pszmessage) MEM_free(pszmessage);
                      }
                    }
                    else
                    {
                      fprintf(feilog, "%s: Error by comm parents .\n", BNL_module);
                    }
                  }
                  else
                  {
                    // perform for all comparents
                    if (iCommParents > 0)
                    {
                      tag_t tParentItem = NULLTAG;
                      int i = 0;
                      char szPid[256] = "";

                      fprintf(feilog, "%s: Found [%d] commercial parents of item [%s].\n", BNL_module, iCommParents, szTargetItemName);
                      for (i=0; i<iCommParents; i++)
                      {
                        tParentItem = NULLTAG;
                        //get the item
                        tParentItem = ptCommParents[i];

                        if (tParentItem != NULLTAG)
                        {
                          iRetCode = ITEM_ask_id(tParentItem, szPid);
                            if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                          /*iRetCode = BNL_tb_determine_object_type(tParentItem, szObjType);
                          if (iRetCode != ITK_ok)
                          {
                            fprintf(feilog, "%s: Error by parents BNL_tb_determine_object_type.\n", BNL_module);
                          }
                          else
                          {
                            fprintf(feilog, " %s: szObjType [%s]\n", BNL_module, szObjType);
                            if (strcmp(szObjType,gpszCommItem)==0)
                            {
                              fprintf(feilog, " Commercial item found.\n");*/
                          iRetCode =FEI_process_item(tParentItem,ptServicePrimItems[is]);
                          if (iRetCode !=ITK_ok)
                          {
                            char *pszmessage    = NULL;
                            char szError[5012] = "";

                            EMH_ask_error_text(iRetCode, &pszmessage);

                            fprintf(feilog, "%s: Error (FEI_process_item) %s.\n", BNL_module, pszmessage);

                            sprintf(szError,"Error occured processing item %s : %s",szPid, pszmessage);

                            iRetCode = FEI_write_to_error_file(szError);
                            if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                            if (pszmessage) MEM_free(pszmessage);
                          }
                        /* }
                         else
                         {
                           fprintf(feilog, " %s: Parent item [%d] is not a [%s]\n", BNL_module, i, gpszCommItem);
                         }
                       }*/
                        }
                        else
                        {
                          fprintf(feilog, "%s: Error by comm parents .\n", BNL_module);
                        }
                      }
                    }
                  }
                }
              }
              else
              {
                fprintf(feilog, "%s: primary object item id [%s] has FRU flag not set\n",BNL_module, szPrimId);
              }
            }
          } /*for (is=0;is<iRels;is++)*/
        }
      }
    }
  }

  /*set status to the new revisions*/
  for (k=0; k<ginewFRUListRev; k++)
  {
    tag_t tNewStatus = NULLTAG;
    tag_t tITem  = NULLTAG;
    tag_t tUser  = NULLTAG;
    tag_t tGroup = NULLTAG;
    tag_t tBview = NULLTAG;

    char szItID[256] = "";

    int iBvrs  = 0;
    int i      = 0;

    tag_t* ptBvrs = NULL;

    iRetCode=ITEM_ask_item_of_rev(gptnewFRUListRev[k], &tITem);
    if (BNL_em_error_handler("BNL_tb_create_new_status", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    iRetCode=ITEM_ask_id(tITem, szItID);
    if (BNL_em_error_handler("BNL_tb_create_new_status", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    fprintf(feilog, "%s: szItID) %s.\n", BNL_module, szItID);

    iRetCode = BNL_tb_load_object(gptnewFRUListRev[k],POM_modify_lock);
    if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    /*Do change ownership*/
    iRetCode=POM_ask_owner(gptoldFRUListRev[k], &tUser, &tGroup);
    if (BNL_em_error_handler("POM_ask_owner",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

    iRetCode=AOM_set_ownership(gptnewFRUListRev[k], tUser, tGroup);
    if (iRetCode != ITK_ok)
    {
      char *pszmessage    = NULL;
      char szError[5012] = "";

      EMH_ask_error_text(iRetCode, &pszmessage);

      fprintf(feilog, "%s: Error (AOM_set_ownership) %s.\n", BNL_module, pszmessage);

      sprintf(szError,"Error occured changing ownership of item %s : %s",szItID, pszmessage);

      iRetCode = FEI_write_to_error_file(szError);
      if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

      if (pszmessage) MEM_free(pszmessage);
    }

    iRetCode=AOM_save(gptnewFRUListRev[k]);
    if (iRetCode != ITK_ok)
    {
      char *pszmessage    = NULL;
      char szError[5012] = "";

      EMH_ask_error_text(iRetCode, &pszmessage);

      fprintf(feilog, "%s: Error (AOM_set_ownership) %s.\n", BNL_module, pszmessage);

      sprintf(szError,"Error occured settigng status to item %s : %s",szItID, pszmessage);

      iRetCode = FEI_write_to_error_file(szError);
      if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

      if (pszmessage) MEM_free(pszmessage);
    }

    iRetCode = ITEM_rev_list_bom_view_revs(gptnewFRUListRev[k], &iBvrs, &ptBvrs);
    if (BNL_em_error_handler("ITEM_rev_list_bom_view_revs", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    for (i=0; i<iBvrs; i++)
    {
      iRetCode = BNL_tb_determine_object_type(ptBvrs[i], szObjType);
      if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

      if (strcmp(szObjType,"view")==0)
      {
        tBview = ptBvrs[i];
        break;
      }
    }

    iRetCode = BNL_tb_load_object(tBview,POM_modify_lock);
    if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    iRetCode=AOM_set_ownership(tBview, tUser, tGroup);
    if (iRetCode != ITK_ok)
    {
      char *pszmessage    = NULL;
      char szError[5012] = "";

      EMH_ask_error_text(iRetCode, &pszmessage);

      fprintf(feilog, "%s: Error (AOM_set_ownership) %s.\n", BNL_module, pszmessage);

      sprintf(szError,"Error occured changing ownership of bom revision of item %s : %s",szItID, pszmessage);

      iRetCode = FEI_write_to_error_file(szError);
      if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

      if (pszmessage) MEM_free(pszmessage);
    }

    iRetCode=AOM_save(tBview);
    if (iRetCode != ITK_ok)
    {
      char *pszmessage    = NULL;
      char szError[5012] = "";

      EMH_ask_error_text(iRetCode, &pszmessage);

      fprintf(feilog, "%s: Error (AOM_set_ownership) %s.\n", BNL_module, pszmessage);

      sprintf(szError,"Error occured changing ownership of bom revision of item %s : %s",szItID, pszmessage);

      iRetCode = FEI_write_to_error_file(szError);
      if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

      if (pszmessage) MEM_free(pszmessage);
    }

    iRetCode = BNL_tb_load_object(tBview, POM_no_lock);
    if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;


    if (strcmp(gppszStatus[k],"-") == 0)
    {
      fprintf(feilog, "%s: New FRUList revision will not get a status.\n", BNL_module);
    }
    else
    {
      char **pszExtraCols  = NULL;
      int iExtraCols = 0;

      fprintf(feilog, "%s: New FRUList revision will get  status [%s].\n", BNL_module, gppszStatus[k]);

      /*Setting status*/
      iRetCode=BNL_tb_create_new_status(gppszStatus[k], &tNewStatus);
      if (BNL_em_error_handler("BNL_tb_create_new_status", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;


      iRetCode=BNL_tb_add_new_status(gptnewFRUListRev[k], tNewStatus, 999, true);
      if (iRetCode != ITK_ok)
      {
        char *pszmessage    = NULL;
        char szError[5012] = "";

        EMH_ask_error_text(iRetCode, &pszmessage);

        fprintf(feilog, "%s: Error (BNL_tb_add_new_status) %s.\n", BNL_module, pszmessage);

        sprintf(szError,"Error occured setting status to item %s : %s",szItID, pszmessage);

        iRetCode = FEI_write_to_error_file(szError);
        if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

        if (pszmessage) MEM_free(pszmessage);
      }

      iExtraCols = 1;
      pszExtraCols = (char **)malloc(sizeof(char*)*iExtraCols);
      BNL_tb_copy_string(szItID, &pszExtraCols[0]);
//      BNL_tb_copy_string(pszOwningSite, &pszExtraCols[1]);
      //BNL_tb_copy_string("veldhoven", &pszExtraCols[1]);

      // Do the thing
      fprintf(feilog, "%s: creating csp file.\n", BNL_module);
      if ((iRetCode = BNL_FEI_create_plot_request(gptnewFRUListRev[k], iExtraCols, pszExtraCols)) != 0) goto exit;

      for (i = 0; i < iExtraCols; i++)
        free(pszExtraCols[i]);
      if (iExtraCols > 0) free(pszExtraCols);

    }
    iRetCode = BNL_tb_load_object(gptnewFRUListRev[k], POM_no_lock);
    if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    if (ptBvrs != NULL) MEM_free(ptBvrs);

  }
  if (glNoFRUMail == true)
  {
    //send mail
    fprintf(feilog, "%s: gpszMailNoFRUList [%s] \n",BNL_module, gpszMailNoFRUList);
    FEI_send_mail(pszMailRecp, pszNoFRUMailSub);
  }

exit:
  fprintf(feilog, "End ... FEI_start_process\n");

  if ( piLevels != NULL) BNL_mm_free(piLevels);
  if ( ptTagParents != NULL) BNL_mm_free(ptTagParents);

  if (pszServiceItem != NULL) MEM_free(pszServiceItem);
  //if (pszServiceRel != NULL) MEM_free(pszServiceRel);
  if (gpszRevRule != NULL) MEM_free(gpszRevRule);
  if (gpszCommItem != NULL) MEM_free(gpszCommItem);
  if (gpszFRUListItem != NULL) MEM_free(gpszFRUListItem);
  if (gpszCommSubclass != NULL) MEM_free(gpszCommSubclass);
  if (gpszFRUCommSubClasses != NULL) MEM_free(gpszFRUCommSubClasses);
  if (gpszFRUCommAllowedStatus != NULL) MEM_free(gpszFRUCommAllowedStatus);
  if (gpszFRUCommObsoleteStatus != NULL) MEM_free(gpszFRUCommObsoleteStatus);
  if ( gptItems != NULL) BNL_mm_free(gptItems);

  return iRetCode;
}

/**
logical FEI_is_remote_obj()

    Description :

        returns if the obj remote is or not.

    Returns     :

        False or True;

*/
logical FEI_is_remote_obj(tag_t tObj,char **pszOwningSite)
{
  char *pszOwnSiteVal = NULL;
  logical lReturn = true;

  AOM_UIF_ask_value(tObj, FEI_OWNING_SITE_ATTR, &pszOwnSiteVal);
  fprintf(feilog,"%s: Owning site [%s]\n", BNL_module, pszOwnSiteVal);
  if (pszOwnSiteVal!=NULL && strlen(pszOwnSiteVal)==0)
  {
    lReturn =false;
    *pszOwningSite = pszOwnSiteVal;
  }

  return lReturn;
}


/**
int FEI_get_FRU_attr_value()

    Description :

        returns the value of the FRU attr of the item.

    Returns     :

        False or True;

*/
int FEI_get_FRU_attr_value(tag_t tItem, char **pszFRUvalue)
{
  int    iRetCode =0;
  int    iCount   =0;

  tag_t tRelType             =NULLTAG;
  tag_t tIMFObj             =NULLTAG;
  tag_t *ptSecondaryObjs =NULL;

  logical lReturn  = false;

  char *pszTemp = NULL;

  fprintf(feilog, "\nBegin ... FEI_get_FRU_attr_value\n");

  iRetCode=GRM_find_relation_type(FEI_IMF_REL,&tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type",BNL_module,EMH_severity_error,iRetCode, true)) goto exit;
  if (tRelType == NULLTAG)
  {
    fprintf(feilog, "%s: Relation type %s is not found.\n", BNL_module, FEI_IMF_REL);
    iRetCode = -1;
    goto exit;
  }

  iRetCode=GRM_list_secondary_objects_only(tItem,tRelType,&iCount,&ptSecondaryObjs);
  if (BNL_em_error_handler("GRM_list_secondary_objects",BNL_module,EMH_severity_error,iRetCode, true)) goto exit;

  if (iCount >0)
  {
    fprintf(feilog, "%s: Found [%d] object with relation type %s. The first one will be used\n", BNL_module,iCount, FEI_IMF_REL);
    tIMFObj = ptSecondaryObjs[0];
    lReturn = true;
  }
  else
  {
    fprintf(feilog, "%s: Found [%d] object with relation type %s.\n", BNL_module,iCount, FEI_IMF_REL);
  }

  iRetCode = AOM_UIF_ask_value(tIMFObj, gpszFRU[0], pszFRUvalue);
  if (BNL_em_error_handler("AOM_UIF_ask_value",BNL_module,EMH_severity_error,iRetCode, true)) goto exit;

  /*else
  {
    *pszFRUvalue = pszTemp;
  }*/

exit:
  fprintf(feilog, "End ... FEI_get_FRU_attr_value\n");

  if (ptSecondaryObjs != NULL) MEM_free(ptSecondaryObjs);
  if (pszTemp != NULL) MEM_free(pszTemp);
  return iRetCode;
}/*Enf of FEI_get_FRU_attr_value*/

/**
logical FEI_get_MDR_attr_value()

    Description :

        returns the value of the MDR attr of the item.

    Returns     :

        False or True;

*/
int FEI_get_MDR_attr_value(tag_t tItem, logical *lMDRvalue)
{
  int    iRetCode =0;
  int    iCount   =0;

  tag_t tRelType             =NULLTAG;
  tag_t tIMFObj             =NULLTAG;
  tag_t *ptSecondaryObjs =NULL;

  logical lReturn  = false;

  fprintf(feilog, "\nBegin ... FEI_get_MDR_attr_value\n");

  iRetCode=GRM_find_relation_type(FEI_IRMF_REL,&tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type",BNL_module,EMH_severity_error,iRetCode, true)) goto exit;
  if (tRelType == NULLTAG)
  {
    fprintf(feilog, "%s: Relation type %s is not found.\n", BNL_module, FEI_IRMF_REL);
    iRetCode = -1;
    goto exit;
  }

  iRetCode=GRM_list_secondary_objects_only(tItem,tRelType,&iCount,&ptSecondaryObjs);
  if (BNL_em_error_handler("GRM_list_secondary_objects",BNL_module,EMH_severity_error,iRetCode, true)) goto exit;

  if (iCount >0)
  {
    fprintf(feilog, "%s: Found [%d] object with relation type %s. The first one will be used\n", BNL_module,iCount, FEI_IRMF_REL);
    tIMFObj = ptSecondaryObjs[0];
    lReturn = true;
  }
  else
  {
    fprintf(feilog, "%s: Found [%d] object with relation type %s.\n", BNL_module,iCount, FEI_IRMF_REL);
  }

  iRetCode = AOM_ask_value_logical(tIMFObj, gpszMDR, &lReturn);
  if (BNL_em_error_handler("AOM_ask_value_logical",BNL_module,EMH_severity_error,iRetCode, true)) goto exit;

  *lMDRvalue = lReturn;

exit:
  fprintf(feilog, "End ... FEI_get_MDR_attr_value\n");

  if (ptSecondaryObjs != NULL) MEM_free(ptSecondaryObjs);
  return iRetCode;
}/*Enf of FEI_get_MDR_attr_value*/


int FEI_reset_mdr_attr_value(tag_t tRev)
{
  int    iRetCode        = 0;
  int    iCount          = 0;

  tag_t tRelType             = NULL_TAG;
  tag_t tIRMFObj           = NULL_TAG;
  tag_t *ptSecondaryObjs = NULL;

  logical lReturn  = false;

  fprintf(feilog, "\nBegin ... FEI_reset_mdr_attr_value\n");

  iRetCode = GRM_find_relation_type(FEI_IRMF_REL, &tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type", BNL_module ,EMH_severity_error, iRetCode, true)) goto exit;

  if (tRelType == NULL_TAG)
  {
    fprintf(feilog, "%s: Relation type %s is not found.\n", BNL_module, FEI_IRMF_REL);
    iRetCode = -1;
      goto exit;
  }

  iRetCode = GRM_list_secondary_objects_only(tRev, tRelType, &iCount, &ptSecondaryObjs);
  if (BNL_em_error_handler("GRM_list_secondary_objects", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

  if (iCount > 0)
  {
    fprintf(feilog, "%s: Found [%d] object with relation type %s. The first one will be used.\n", BNL_module,iCount, FEI_IRMF_REL);
    tIRMFObj = ptSecondaryObjs[0];
  }

  if (tIRMFObj == NULL_TAG)
  {
    fprintf(feilog, "%s: IRMF not found.\n", BNL_module);
    iRetCode = -1;
      goto exit;
  }
  else
  {
    iRetCode = AOM_refresh(tIRMFObj, true);
    if (BNL_em_error_handler("AOM_refresh", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    iRetCode = AOM_set_value_logical(tIRMFObj, gpszMDR, false);
    if (BNL_em_error_handler("AOM_set_value_logical", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    iRetCode = AOM_save(tIRMFObj);
    if (BNL_em_error_handler("AOM_save", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;
  }

exit:

  fprintf(feilog, "End ... FEI_reset_mdr_attr_value\n");

  if (ptSecondaryObjs != NULL) MEM_free(ptSecondaryObjs);

  return iRetCode;
}/*Enf of FEI_reset_mdr_attr_value*/


/**
logical FEI_get_secObj()

    Description :

        Find the secobj of the given ItemRevision.

    Returns     :

        False or True;

*/
int FEI_get_secObj(tag_t tObj, char *pszRel, tag_t *tRelObj)
{
  int    iRetCode =0;
  int    iCount   =0;

  tag_t tRelType             =NULLTAG;
  tag_t *ptSecondaryObjs =NULL;

  logical lReturn  = false;

  fprintf(feilog, "\nBegin ... FEI_get_secObj\n");

  iRetCode=GRM_find_relation_type(pszRel,&tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type",BNL_module,EMH_severity_error,iRetCode, true)) goto exit;
  if (tRelType == NULLTAG)
  {
    fprintf(feilog, "%s: Relation type %s is not found.\n", BNL_module, pszRel);
    goto exit;
  }

  iRetCode=GRM_list_secondary_objects_only(tObj,tRelType,&iCount,&ptSecondaryObjs);
  if (BNL_em_error_handler("GRM_list_secondary_objects",BNL_module,EMH_severity_error,iRetCode, true)) goto exit;

  if (iCount > 0)
  {
    fprintf(feilog, "%s: Found [%d] object with relation type %s. The first one will be used\n", BNL_module,iCount, pszRel);
    *tRelObj = ptSecondaryObjs[0];
    lReturn = true;
  }
exit:
  fprintf(feilog, "End ... FEI_get_secObj\n");

  if (ptSecondaryObjs != NULL) MEM_free(ptSecondaryObjs);
  return iRetCode;
}/*Enf of FEI_get_IRMF*/


/**
logical FEI_get_bom_revision

  Description:

    returns the item revision of the bomline

  Returns:

    true or false
)
*/
logical FEI_get_bom_revision
(
  tag_t tBomLine,
  tag_t *tBomRev
)
{
  int iRetCode = 0;

  tag_t tPropertyRev = NULLTAG;
  tag_t tBRev = NULLTAG;

  logical lFound = false;

  fprintf(feilog, "\nBegin ... FEI_get_bom_revision\n");

   //get itemrev
  iRetCode = PROP_ask_property_by_name(tBomLine, "bl_revision", &tPropertyRev);
  if (iRetCode == ITK_ok)
  {
    iRetCode = PROP_ask_value_tag(tPropertyRev, &tBRev);
    if(iRetCode == ITK_ok && tBRev !=NULLTAG)
    {
      lFound = true;
      *tBomRev = tBRev;
    }
  }
  else
  {
    char *pszmessage    = NULL;
    char szError[5012] = "";

    EMH_ask_error_text(iRetCode, &pszmessage);

    fprintf(feilog, "%s: Error (FEI_get_bom_revision) %s.\n", BNL_module, pszmessage);

    sprintf(szError,"Error occured : %s", pszmessage);

    iRetCode = FEI_write_to_error_file(szError);
    if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    if (pszmessage) MEM_free(pszmessage);
  }
exit:
  fprintf(feilog, "End ... FEI_get_bom_revision\n");
  return lFound;
}

/**
logical FEI_get_bom_item

  Description:

    returns the item of the bomline

  Returns:

    true or false
)
*/
logical FEI_get_bom_item
(
  tag_t tBomLine,
  tag_t *tBomItem
)
{
  int iRetCode = 0;

  tag_t tProperty = NULLTAG;
  tag_t tBItem = NULLTAG;

  logical lFound = false;

  fprintf(feilog, "\nBegin ... FEI_get_bom_item\n");

   //get itemrev
  iRetCode = PROP_ask_property_by_name(tBomLine, "bl_item", &tProperty);
  if (iRetCode == ITK_ok)
  {
    iRetCode = PROP_ask_value_tag(tProperty, &tBItem);
    if(iRetCode == ITK_ok && tBItem !=NULLTAG)
    {
      lFound = true;
      *tBomItem = tBItem;
    }
  }
  else
  {
    char *pszmessage    = NULL;
    char szError[5012] = "";

    EMH_ask_error_text(iRetCode, &pszmessage);

    fprintf(feilog, "%s: Error (FEI_get_bom_item) %s.\n", BNL_module, pszmessage);

    sprintf(szError,"Error occured : %s", pszmessage);

    iRetCode = FEI_write_to_error_file(szError);
    if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    if (pszmessage) MEM_free(pszmessage);
  }
exit:
  fprintf(feilog, "End ... FEI_get_bom_item\n");
  return lFound;
}



/**
int FEI_get_bvr

  Description:

    get the bvr

  Returns:

    0 on Ok
)
*/
int FEI_get_bvr
(
  tag_t tItemRev,
  tag_t tBv,
  tag_t *ptBvr
)
{
  int iRetCode = 0;
  int iBomLineCount = 0;
  int iBvrs = 0;
  int i     = 0;

  tag_t tBomWindow  = NULLTAG;
  tag_t tTopLine    = NULLTAG;
  tag_t tBview      = NULLTAG;

  tag_t tUser       = NULLTAG;
  tag_t tGroup      = NULLTAG;

  tag_t *ptBvrs         = NULL;
  tag_t *ptBoms         = NULL;

  char szObjType[256] = "";

  fprintf(feilog, "\nBegin ... FEI_get_bvr\n");

  iRetCode = ITEM_rev_list_bom_view_revs(tItemRev, &iBvrs, &ptBvrs);
  if (BNL_em_error_handler("ITEM_rev_list_bom_view_revs", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

  fprintf(feilog, "\niBvrs [%d].\n", iBvrs);

  for (i=0; i<iBvrs; i++)
  {
    iRetCode = BNL_tb_determine_object_type(ptBvrs[i], szObjType);
    if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    if (strcmp(szObjType,"view")==0)
    {
      tBview = ptBvrs[i];
      break;
    }
  }

  if (tBview == NULLTAG)
  {
    // Lock the itemrev
    iRetCode = BNL_tb_load_object(tItemRev, POM_modify_lock);
    if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    iRetCode=PS_create_bvr(tBv,NULL,NULL,false,tItemRev,&tBview);
    if (BNL_em_error_handler("PS_create_bvr",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

    /*iRetCode=POM_ask_owner(tItemRev, &tUser, &tGroup);
    if (BNL_em_error_handler("POM_ask_owner",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

    iRetCode=AOM_set_ownership(tBview, tUser, tGroup);
    if (BNL_em_error_handler("AOM_set_ownership",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;*/

    iRetCode=AOM_save(tBview);
    if (BNL_em_error_handler("AOM_save of Bomview Revision",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

    iRetCode=AOM_save(tItemRev);
    if (BNL_em_error_handler("AOM_save of ItemRevision",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

    iRetCode = BNL_tb_load_object(tItemRev, POM_no_lock);
    if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
  }

  *ptBvr = tBview;


exit:
  fprintf(feilog, "End ... FEI_get_bvr\n");
 // if ( ptBoms != NULL) BNL_mm_free(ptBoms);
  if ( ptBvrs != NULL) BNL_mm_free(ptBvrs);

  return iRetCode;
}

/**
int FEI_create_bv

  Description:

    create bv

  Returns:

    0 on Ok
)
*/
int FEI_create_bv
(
  tag_t tItem,
  tag_t tRev,
  tag_t *ptBv,
  tag_t *ptBvr
)
{
  int iRetCode = 0;
  int iBomLineCount = 0;
  int iBvrs = 0;
  int i     = 0;

  tag_t tBomWindow  = NULLTAG;
  tag_t tViewType    = NULLTAG;
  tag_t tBomView   = NULLTAG;
  tag_t tUser      = NULLTAG;
  tag_t tGroup      = NULLTAG;
  tag_t tBvr         = NULLTAG;
  tag_t *ptBoms         = NULL;

  char szObjType[256] = "";

  fprintf(feilog, "\nBegin ... FEI_create_bv\n");

  // Lock the item
  iRetCode = BNL_tb_load_object(tItem, POM_modify_lock);
  if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode=PS_find_view_type( "view", &tViewType );
  if (BNL_em_error_handler("PS_find_view_type",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

  iRetCode=PS_create_bom_view(tViewType,NULL,NULL,tItem,&tBomView);
  if (BNL_em_error_handler("PS_create_bom_view",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

  iRetCode=POM_ask_owner(tItem, &tUser, &tGroup);
  if (BNL_em_error_handler("POM_ask_owner",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

  iRetCode=AOM_set_ownership(tBomView, tUser, tGroup);
  if (BNL_em_error_handler("AOM_set_ownership",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

  iRetCode=AOM_save(tBomView);
  if (BNL_em_error_handler("AOM_save of Bomview",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

  iRetCode=AOM_save(tItem);
  if (BNL_em_error_handler("AOM_save of Item",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

  ITEM_ask_latest_rev(tItem, &tRev);
  if (tRev != NULLTAG)
  {
    // Lock the itemrev
    iRetCode = BNL_tb_load_object(tRev, POM_modify_lock);
    if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    iRetCode=PS_create_bvr(tBomView,NULL,NULL,false,tRev,&tBvr);
    if (BNL_em_error_handler("PS_create_bvr",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

    /*iRetCode=POM_ask_owner(tRev, &tUser, &tGroup);
    if (BNL_em_error_handler("POM_ask_owner",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

    iRetCode=AOM_set_ownership(tBvr, tUser, tGroup);
    if (BNL_em_error_handler("AOM_set_ownership",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;*/

    iRetCode=AOM_save(tBvr);
    if (BNL_em_error_handler("AOM_save of Bomview Revision",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

    iRetCode=AOM_save(tRev);
    if (BNL_em_error_handler("AOM_save of ItemRevision",BNL_module,EMH_severity_error,iRetCode , true)) return iRetCode;

    iRetCode = BNL_tb_load_object(tRev, POM_no_lock);
    if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
  }

  iRetCode = BNL_tb_load_object(tItem, POM_no_lock);
  if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  *ptBv = tBomView;
  *ptBvr = tBvr;

//exit:
  fprintf(feilog, "End ... FEI_create_bv\n");
 // if ( ptBoms != NULL) BNL_mm_free(ptBoms);

  return iRetCode;
}

/**
int FEI_remove_service_item_from_parent_FRUList

  Description:

    get the where used FRUList parents and remove the item from their structure

  Returns:

    0 on Ok

  PS: not used
)
*/
int FEI_remove_service_item_from_parent_FRUList(tag_t tItem,int iLevel)
{
  int iRetCode = 0;
  int iParents     = 0;
  int i     = 0;

  int *piLevels    = NULL;

  tag_t tItemRev    = NULLTAG;
  tag_t tRevRule    = NULLTAG;

  tag_t *ptTagParents   = NULL;

  char szObjType[256] = "";
  char szName[1024] = "";

  char *psconf = NULL;

  fprintf(feilog, "\nBegin ... FEI_remove_service_item_from_parent_FRUList\n");

  fprintf(feilog, "Getting where used.\n");

  //get item revision with revision rule
  iRetCode = CFM_find(gpszRevRule,&tRevRule);
  if (tRevRule != NULLTAG)
  {
    iRetCode = CFM_item_ask_configured(tRevRule, tItem,&tItemRev, &psconf);
    if (tItemRev != NULLTAG)
    {
      // Perform a One Level Where-Used
      iRetCode = PS_where_used_configured(tItemRev, tRevRule,iLevel,&iParents,&piLevels,&ptTagParents);
      if (iParents > 0)
      {
        tag_t tParentItem = NULLTAG;

        fprintf(feilog, "%s: Found [%d] parents.\n", BNL_module, iParents);
        for (i=0; i<iParents; i++)
        {
          tParentItem = NULLTAG;
          //get the item
          iRetCode = ITEM_ask_item_of_rev(ptTagParents[i], &tParentItem);
          if (tParentItem != NULLTAG)
          {
            iRetCode = BNL_tb_determine_object_type(tParentItem, szObjType);
            if (iRetCode != ITK_ok)
            {
              fprintf(feilog, "%s: Error by parents BNL_tb_determine_object_type.\n", BNL_module);
            }
            else
            {
              if (strcmp(szObjType,gpszFRUListItem)==0)
              {
                char *pszOwningSite = NULL;
                fprintf(feilog, "%s: Found [%s] item in parents.\n", BNL_module, gpszFRUListItem);
                //get the name
                WSOM_ask_name(tParentItem,szName);
                if (FEI_is_remote_obj(tParentItem, &pszOwningSite) == false)
                {
                  int iBvs = 0;
                  tag_t *ptBvs = NULL;
                  //found FRUList item
                  iRetCode = ITEM_list_bom_views(tParentItem, &iBvs, &ptBvs);
                  if (iRetCode == ITK_ok)
                  {
                    int k=0;
                    char szBviewType[256] = "";

                    fprintf(feilog, "\n found item [%s] iBvs [%d].\n", szName, iBvs);

                    for (k=0; k<iBvs; k++)
                    {
                      iRetCode = BNL_tb_determine_object_type(ptBvs[k], szBviewType);
                      if (strcmp(szBviewType,"view")==0)
                      {
                        tag_t tTopLine = NULLTAG;
                        tag_t tBomWindow = NULLTAG;
                        int iBomLineCount = 0;
                        int iBomLines = 0;
                        tag_t *ptBoms = NULL;
                        char *pszPUID1 = NULL;

                        //found bom view
                        //remove the target item from bom
                        fprintf(feilog, "%s: Removing target from bom view.\n", BNL_module);
                        //jale
                        iRetCode = FEI_remove_bomline(ptBvs[k],tParentItem,tItem);
                        if (iRetCode != ITK_ok)
                        {
                          char *pszmessage    = NULL;
                          EMH_ask_error_text(iRetCode, &pszmessage);

                          fprintf(feilog, " %s: xx- Error(FEI_remove_bomline ) [%s] \n",BNL_module, pszmessage);
                          if (pszmessage) MEM_free(pszmessage);
                        }
                      }
                      else
                      {
                        fprintf(feilog, "%s: No bom view found  [%s].\n", BNL_module,szName);
                      }
                    }

                    if ( ptBvs != NULL) BNL_mm_free(ptBvs);
                    ptBvs = NULL;
                  }
                  else/*if (iRetCode == ITK_ok) ITEM_list_bom_views*/
                  {
                    fprintf(feilog, "%s: Error by parents ITEM_list_bom_views of item [%s].\n", BNL_module,szName);
                  }
                }
                else/*if (FEI_is_remote_obj(tParentItem) == false)*/
                {
                  /*char **pszExtraCols  = NULL;
                  int iExtraCols = 0;
                  char szId[ITEM_id_size_c + 1];*/

                  fprintf(feilog, "%s: item [%s] is a remote item, target will not be removed .\n", BNL_module, szName);
                  /*fprintf(feilog, "%s: Owning site of item [%s] is [%s]. Creating csp file.\n", BNL_module, szName,pszOwningSite);

                  iRetCode = ITEM_ask_id(tItem, szId);
                  if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  iExtraCols = 2;
                  pszExtraCols = (char **)malloc(sizeof(char*)*iExtraCols);
                  BNL_tb_copy_string(szId, &pszExtraCols[0]);
                  BNL_tb_copy_string(pszOwningSite, &pszExtraCols[1]);
                  //BNL_tb_copy_string("veldhoven", &pszExtraCols[1]);

                  // Do the thing
                  if ((iRetCode = BNL_FEI_create_plot_request(tItem, iExtraCols, pszExtraCols)) != 0) goto exit;

                  for (i = 0; i < iExtraCols; i++)
                    free(pszExtraCols[i]);
                  if (iExtraCols > 0) free(pszExtraCols);*/

                }

              }
              else
              {
                fprintf(feilog, "%s: Parent item [%d] is not a [%s].\n", BNL_module, i,gpszFRUListItem);
              }
            }
          }
          else
          {
            fprintf(feilog, "%s: Error by parents ITEM_ask_item_of_rev .\n", BNL_module);
          }
        }
      }
      else
      {
        fprintf(feilog, "%s: No parents found (PS_where_used_configured).\n", BNL_module);
      }
    }
    else
    {
      fprintf(feilog, "%s: Revision with rule [%s] not found.\n", BNL_module, gpszRevRule);
    }
  }
  else
  {
    fprintf(feilog, "%s: Revision rule [%s] not found.\n", BNL_module, gpszRevRule);
    //iRetCode = -1;
  }

//exit:
  fprintf(feilog, "End ... FEI_remove_service_item_from_parent_FRUList\n");
  if ( piLevels != NULL) BNL_mm_free(piLevels);
  if ( psconf != NULL) BNL_mm_free(psconf);
  if ( ptTagParents != NULL) BNL_mm_free(ptTagParents);

  return iRetCode;
}


/**
int FEI_remove_from_parent_FRUList

  Description:

    get the where used FRUList parents and remove the item from their structure

  Returns:

    0 on Ok
)
*/
int FEI_remove_from_parent_FRUList(tag_t tItem,char *pszFRU,tag_t *ptTagParents,int iParents)
{
  int iRetCode = 0;
  int i     = 0;


  char szObjType[256] = "";
  char szID[1024] = "";

  char *psconf = NULL;

  tag_t tParentItem = NULLTAG;

  fprintf(feilog, "\nBegin ... FEI_remove_from_parent_FRUList\n");

  fprintf(feilog, "%s: Using single level parents of target item.Found [%d] parents.\n", BNL_module, iParents);
  for (i=0; i<iParents; i++)
  {
    tParentItem = NULLTAG;
    //get the item
    iRetCode = ITEM_ask_item_of_rev(ptTagParents[i], &tParentItem);
    if (tParentItem != NULLTAG)
    {
      iRetCode = BNL_tb_determine_object_type(tParentItem, szObjType);
      if (iRetCode != ITK_ok)
      {
        fprintf(feilog, "%s: Error by parents BNL_tb_determine_object_type.\n", BNL_module);
      }
      else
      {
        if (strcmp(szObjType,gpszFRUListItem)==0)
        {
          char *pszOwningSite = NULL;
          logical lFRUAcces = false;

          fprintf(feilog, "%s: Found [%s] item in parents.\n", BNL_module, gpszFRUListItem);
          //get the name
          ITEM_ask_id(tParentItem,szID);

          iRetCode = AM_check_privilege(tParentItem, "WRITE", &lFRUAcces);
          if (iRetCode != ITK_ok)
          {
            char *pszmessage    = NULL;
            char szError[5012] = "";

            EMH_ask_error_text(iRetCode, &pszmessage);

            fprintf(feilog, "%s: Error (AM_check_privilege) %s.\n", BNL_module, pszmessage);

            sprintf(szError,"Error occured AM_check_privilege of FRUItem %s : %s",szID, pszmessage);

            iRetCode = FEI_write_to_error_file(szError);
            if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

            if (pszmessage) MEM_free(pszmessage);
          }

          if (lFRUAcces == true)
          {
            if (FEI_is_remote_obj(tParentItem, &pszOwningSite) == false)
            {
              char szFRUItemId[256] = "";

              tag_t tFRUListRev = NULLTAG;
              tag_t tnewFRUListRev = NULLTAG;
              tag_t tCommParent    = NULLTAG;

              /*check if it is needed to revise FRUList item*/
              //get the latest rev (creation date)
              iRetCode = ITEM_ask_id(tParentItem, szFRUItemId);
              if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

              fprintf(feilog, " getting latest rev (creation date) of fru item[%s]\n", szFRUItemId);

              iRetCode =ITEM_ask_latest_rev(tParentItem, &tFRUListRev);
              if (BNL_em_error_handler("ITEM_ask_latest_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

              // assign to the project
              iRetCode = FEI_assign_to_project(tParentItem);
              if (iRetCode != ITK_ok)
              {
                char *pszmessage    = NULL;
                char szError[5012] = "";

		        EMH_ask_error_text(iRetCode, &pszmessage);

			    fprintf(feilog, "%s: Error (FEI_assign_to_project) %s.\n", BNL_module, pszmessage);

	      	    sprintf(szError,"Error occured assigning item %s to project: %s",szFRUItemId, pszmessage);

			    iRetCode = FEI_write_to_error_file(szError);
			    if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;
	            if (pszmessage) MEM_free(pszmessage);
              }
              else
              {
                fprintf(feilog, "%s: FRU item [%s] is assigned to project.\n", BNL_module, szFRUItemId );
              }

              /*get the comm item parent of the FRUList Item*/
              iRetCode = FEI_find_comm_parent(tFRUListRev, &tCommParent);
              if (iRetCode != ITK_ok)
              {
                char *pszmessage    = NULL;
                char szError[5012] = "";

                EMH_ask_error_text(iRetCode, &pszmessage);

                fprintf(feilog, "%s: Error (FEI_find_comm_parent) %s.\n", BNL_module, pszmessage);

                  sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

                iRetCode = FEI_write_to_error_file(szError);
                if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;
                if (pszmessage) MEM_free(pszmessage);
              }
              else
              {
                logical lRevise = false;

                iRetCode = FEI_check_FRUListItem_to_revise(tFRUListRev, tItem, pszFRU, &lRevise);
                if (iRetCode != ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (FEI_check_FRUListItem_to_revise) %s.\n", BNL_module, pszmessage);

                    sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;
                  if (pszmessage) MEM_free(pszmessage);
                }
                else
                {
                  if (lRevise == false)
                  {
                    fprintf(feilog, "%s: FRUITem revision will not be revised.\n", BNL_module);
                    tnewFRUListRev = tFRUListRev;
                  }
                  else
                  {
                    char szStatus[256] = "";

                    iRetCode = BNL_tb_ask_object_status(tFRUListRev, szStatus);
                    if (iRetCode != ITK_ok)
                    {
                      char *pszmessage    = NULL;
                      char szError[5012] = "";

                      EMH_ask_error_text(iRetCode, &pszmessage);

                      fprintf(feilog, "%s: Error (BNL_tb_ask_object_status) %s.\n", BNL_module, pszmessage);

                      sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

                      iRetCode = FEI_write_to_error_file(szError);
                      if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                      if (pszmessage) MEM_free(pszmessage);
                    }
                    else
                    {
                      if (strcmp(szStatus,"-") == 0)
                      {
                        fprintf(feilog, "%s: FRUITem revision has no status, using the latest revision.\n", BNL_module);
                        tnewFRUListRev = tFRUListRev;
                      }
                      else
                      {
                        logical lStatus = false;

                        iRetCode = FEI_check_comm_and_predecessor_status(tCommParent, &lStatus);
                        if (iRetCode != ITK_ok)
                        {
                          char *pszmessage    = NULL;
                          char szError[5012] = "";

                          EMH_ask_error_text(iRetCode, &pszmessage);

                          fprintf(feilog, "%s: Error (FEI_check_comm_and_predecessor_status) %s.\n", BNL_module, pszmessage);

                            sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

                          iRetCode = FEI_write_to_error_file(szError);
                          if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                          if (pszmessage) MEM_free(pszmessage);
                        }
                        else
                        {

                          //fprintf(feilog, " FRUITem revision has status, creating new revision\n");
                          fprintf(feilog, "FEI_check_comm_and_predecessor_status returned [%d], creating new revision\n", lStatus);
                          //create a new revision
                          iRetCode = ITEM_copy_rev(tFRUListRev, NULL, &tnewFRUListRev);
                          if (iRetCode != ITK_ok)
                          {
                            char *pszmessage    = NULL;
                            char szError[5012] = "";

		                    EMH_ask_error_text(iRetCode, &pszmessage);

		  	                fprintf(feilog, "%s: Error (ITEM_copy_rev) %s.\n", BNL_module, pszmessage);

	      	                sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

			                iRetCode = FEI_write_to_error_file(szError);
			                if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

			                if (pszmessage) MEM_free(pszmessage);
                          }
                          else
                          {
                            int iDeepcount = 0;
                            tag_t *ptDeepcopiedobjects = NULL;

                            tag_t tUser       = NULL_TAG;
                            tag_t tGroup      = NULL_TAG;
                            tag_t tNewStatus  = NULL_TAG;

                            iRetCode =ITEM_perform_deepcopy(tnewFRUListRev,ITEM_revise_operation,tFRUListRev,&iDeepcount,&ptDeepcopiedobjects);
                            if (iRetCode != ITK_ok)
                            {
                              char *pszmessage    = NULL;
                              char szError[5012] = "";

		                      EMH_ask_error_text(iRetCode, &pszmessage);

	                          fprintf(feilog, "%s: Error (ITEM_perform_deepcopy) %s.\n", BNL_module, pszmessage);

			                  sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

			                  iRetCode = FEI_write_to_error_file(szError);
			                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

			                  if (pszmessage) MEM_free(pszmessage);
                            }

                            fprintf(feilog, "%s: iDeepcount [%d].\n", BNL_module,iDeepcount);
                            if (ptDeepcopiedobjects != NULL) MEM_free(ptDeepcopiedobjects);

                            // JM|06122011: Get IRMF and reset attribute defined by pref FEI_MDR_MDRProcessed
                            iRetCode = FEI_reset_mdr_attr_value(tnewFRUListRev);
                            if (iRetCode != ITK_ok)
                            {
                                char *pszmessage    = NULL;
                                char szError[5012] = "";

                                EMH_ask_error_text(iRetCode, &pszmessage);

                                fprintf(feilog, "%s: Error (FEI_reset_mdr_attr_value) %s.\n", BNL_module, pszmessage);
                                sprintf(szError ,"Error occured processing item %s : %s",szFRUItemId, pszmessage);
                                iRetCode = FEI_write_to_error_file(szError);
                                if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                                if (pszmessage) MEM_free(pszmessage);
                            }

                            /*send mail that FRUList revision is revised*/
                            FEI_send_revise_mail(tParentItem);


                            /*collect the FRUListItem revs that is revised*/
                            ginewFRUListRev ++;
                            gptnewFRUListRev =( tag_t* )MEM_realloc( gptnewFRUListRev , (sizeof( tag_t ) * ginewFRUListRev ));
                            gptnewFRUListRev [ginewFRUListRev-1] = tnewFRUListRev;

                            gptoldFRUListRev =( tag_t* )MEM_realloc( gptoldFRUListRev , (sizeof( tag_t ) * ginewFRUListRev ));
                            gptoldFRUListRev [ginewFRUListRev-1] = tFRUListRev;

                            if (lStatus == false)
                            {
                              fprintf(feilog, "new FRUList Item revision will not get status.\n");

                              giStatus ++;
                              gppszStatus =( char** )MEM_realloc( gppszStatus , (sizeof( char * ) * (giStatus * sizeof(char*))));
                              gppszStatus [giStatus-1] = MEM_alloc(sizeof(char)* (int)strlen("-")+1);
                              strcpy(gppszStatus [giStatus-1], "-");
                            }
                            else
                            {
                              giStatus ++;
                              gppszStatus =( char** )MEM_realloc( gppszStatus , (sizeof( char * ) * (giStatus * sizeof(char*))));
                              gppszStatus [giStatus-1] = MEM_alloc(sizeof(char)* (int)strlen(szStatus)+1);
                              strcpy(gppszStatus [giStatus-1], szStatus);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              if (tnewFRUListRev != NULLTAG)
              {
                int iBvs = 0;
                tag_t *ptBvs = NULL;
                //found FRUList item
                iRetCode = ITEM_list_bom_views(tParentItem, &iBvs, &ptBvs);
                if (iRetCode == ITK_ok)
                {
                  int k=0;
                  char szBviewType[256] = "";

                  fprintf(feilog, "\n found item [%s] iBvs [%d].\n", szID, iBvs);

                  for (k=0; k<iBvs; k++)
                  {
                    iRetCode = BNL_tb_determine_object_type(ptBvs[k], szBviewType);
                    if (strcmp(szBviewType,"view")==0)
                    {
                      tag_t tTopLine = NULLTAG;
                      tag_t tBomWindow = NULLTAG;
                      int iBomLineCount = 0;
                      int iBomLines = 0;
                      tag_t *ptBoms = NULL;
                      char *pszPUID1 = NULL;

                      //found bom view
                      //remove the target item from bom

                      fprintf(feilog, "%s: Removing target from bom view.\n", BNL_module);
                      iRetCode = FEI_remove_bomline(ptBvs[k],tParentItem,tItem);
                      if (iRetCode != ITK_ok)
                      {
                        char *pszmessage    = NULL;
                        char szError[5012] = "";

                        EMH_ask_error_text(iRetCode, &pszmessage);

                        fprintf(feilog, "%s: Error (BOM_line_cut) %s.\n", BNL_module, pszmessage);

                        sprintf(szError,"Error occured removing bomlines from item %s : %s",szID, pszmessage);

                        iRetCode = FEI_write_to_error_file(szError);
                        if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                        if (pszmessage) MEM_free(pszmessage);
                      }
                    }
                    else
                    {
                      fprintf(feilog, "%s: No bom view found  [%s].\n", BNL_module,szID);
                    }
                  }
                  if ( ptBvs != NULL) BNL_mm_free(ptBvs);
                  ptBvs = NULL;
                }
                else/*if (iRetCode == ITK_ok) ITEM_list_bom_views*/
                {
                  fprintf(feilog, "%s: Error by parents ITEM_list_bom_views of item [%s].\n", BNL_module,szID);
                }
              }
            }
            else/*if (FEI_is_remote_obj(tParentItem) == false)*/
            {
              /*char **pszExtraCols  = NULL;
              int iExtraCols = 0;
              char szId[ITEM_id_size_c + 1];*/

              fprintf(feilog, "%s: item [%s] is a remote item, target will not be removed .\n", BNL_module, szID);
              /*fprintf(feilog, "%s: Owning site of item [%s] is [%s]. Creating csp file.\n", BNL_module, szID,pszOwningSite);

              iRetCode = ITEM_ask_id(tParentItem, szId);
              if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

              iExtraCols = 2;
              pszExtraCols = (char **)malloc(sizeof(char*)*iExtraCols);
              BNL_tb_copy_string(szId, &pszExtraCols[0]);
              BNL_tb_copy_string(pszOwningSite, &pszExtraCols[1]);
              //BNL_tb_copy_string("veldhoven", &pszExtraCols[1]);

              // Do the thing
              if ((iRetCode = BNL_FEI_create_plot_request(tItem, iExtraCols, pszExtraCols)) != 0) goto exit;
              for (i = 0; i < iExtraCols; i++)
                free(pszExtraCols[i]);
              if (iExtraCols > 0) free(pszExtraCols);*/

            }
          }
          else
          {
            char szError[5012] = "";
            fprintf(feilog, "%s: Processing user has no write access on item  %s.\n", BNL_module, szID);

            sprintf(szError,"Processing user has no write access on item  %s.",szID);

            iRetCode = FEI_write_to_error_file(szError);
            if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

          }
        }
        else
        {
          fprintf(feilog, "%s: Parent item [%d] is not a [%s].\n", BNL_module, i,gpszFRUListItem);
        }
      }
    }
    else
    {
      fprintf(feilog, "%s: Error by parents ITEM_ask_item_of_rev .\n", BNL_module);
    }
 }


exit:
  fprintf(feilog, "End ... FEI_remove_from_parent_FRUList\n");

  return iRetCode;
}

/**
logical FEI_item_has_service_rel

  Description:

    check if the item has service relation

  Returns:

    true or false
)
*/
int FEI_item_has_service_rel(tag_t tObj, char *pszRel, int *iRel, tag_t **ptServiceObj, logical *lReturn, logical *lExist)
{
  int    iRetCode        = 0;
  int    iCount          = 0;

  tag_t tRelType             = NULL_TAG;

  tag_t *ptSecondaryObjs = NULL;


  *lExist = false;
  *lReturn = false;

  //fprintf(feilog, "\nBegin ... FEI_item_has_service_rel\n");

  iRetCode = GRM_find_relation_type(pszRel, &tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

  if (tRelType == NULL_TAG)
  {
    fprintf(feilog, "%s: Relation type %s is not found.\n", BNL_module, pszRel);
    goto exit;
  }

  iRetCode = GRM_list_secondary_objects_only(tObj,tRelType, &iCount, &ptSecondaryObjs);
  if (BNL_em_error_handler("GRM_list_secondary_objects", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

  if (iCount > 0)
  {
    fprintf(feilog, "%s: Found [%d] object with relation type %s.\n", BNL_module,iCount, pszRel);
    *ptServiceObj = ptSecondaryObjs;
    *iRel = iCount;
    *lExist = true;
    *lReturn = true;
  }
  else
  {
    fprintf(feilog, "%s: Found [%d] object with relation type %s.\n", BNL_module, iCount, pszRel);
    *lReturn = true;
  }


exit:
  fprintf(feilog, "End ... FEI_item_has_service_rel\n");

  //if (ptSecondaryObjs != NULL) MEM_free(ptSecondaryObjs);
 return iRetCode;
}/*Enf of FEI_item_has_service_rel*/

/**
logical FEI_item_has_service_rel_primary

  Description:

    check if the item has service relation and return the primary objects

  Returns:

    true or false
)
*/
int FEI_item_has_service_rel_primary(tag_t tObj, char *pszRel, int *iRel,tag_t **ptPrimObjs, logical *lReturn, logical *lExist)
{
  int    iRetCode        = 0;
  int    iCount          = 0;

  tag_t tRelType             = NULL_TAG;

  tag_t *ptPrimaryObjs   = NULL;


  *lExist = false;
  *lReturn = false;

  //fprintf(feilog, "\nBegin ... FEI_item_has_service_rel_primary\n");

  iRetCode = GRM_find_relation_type(pszRel,&tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

  if (tRelType == NULL_TAG)
  {
    fprintf(feilog, "%s: Relation type %s is not found.\n", BNL_module, pszRel);
    goto exit;
  }

  iRetCode = GRM_list_primary_objects_only(tObj, tRelType, &iCount, &ptPrimaryObjs);
  if (BNL_em_error_handler("GRM_list_secondary_objects", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

  if (iCount > 0)
  {
    fprintf(feilog, "%s: Found [%d] object with relation type %s.\n", BNL_module, iCount, pszRel);
    *ptPrimObjs = ptPrimaryObjs;
    *iRel = iCount;
    *lExist = true;
    *lReturn = true;
  }
  else
  {
    fprintf(feilog, "%s: Found [%d] object with relation type %s.\n", BNL_module, iCount, pszRel);
    *lReturn = true;
  }

exit:

  //fprintf(feilog, "End ... FEI_item_has_service_rel_primary\n");

  //if (ptSecondaryObjs != NULL) MEM_free(ptSecondaryObjs);
  return iRetCode;
}/*Enf of FEI_item_has_service_rel_primary*/

/**
int FEI_process_item

  Description:

    process of the service item

  Returns:

    0 on Ok
)
*/
int FEI_process_item
(
  tag_t tCommItem,
  tag_t tTargetItem
)
{
  int iRetCode = 0;

  int i     = 0;

  tag_t tItemRev    = NULLTAG;

  char szObjType[256] = "";
  char szName[1024] = "";
  char szServiceItemName[1024] = "";

  char szCommId[256] = "";
  char szTargetId[256] = "";
  char szCommDesc[256] = "";


  fprintf(feilog, "\nBegin ... FEI_process_item\n");

  ITEM_ask_id(tTargetItem,szTargetId);

  /*Skip the comm item self*/
  if (tCommItem == tTargetItem)
  {
    fprintf(feilog, "target item same as commercial item\n");
    goto exit;
  }

  if (FEI_check_comm_item_to_process(tCommItem) == true)
  {
    tag_t *ptFRUListBomLines = NULL;
    int iFRUListBomLines = 0;

    iRetCode = ITEM_ask_id(tCommItem,szCommId);
    if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    iRetCode = ITEM_ask_description(tCommItem, szCommDesc);
    if (BNL_em_error_handler("ITEM_ask_description", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    fprintf(feilog, " Getting FRUList bom lines of comm item [%s]\n",szCommId);
    iRetCode = FEI_get_FRUList_bomlines(tCommItem, &ptFRUListBomLines, &iFRUListBomLines);
    if (iRetCode != ITK_ok)
    {
      char *pszmessage    = NULL;
      char szError[5012] = "";

      EMH_ask_error_text(iRetCode, &pszmessage);

      fprintf(feilog, "%s: Error (FEI_get_FRUList_bomlines) %s.\n", BNL_module, pszmessage);

      sprintf(szError,"Error occured processing item %s : %s",szCommId, pszmessage);

      iRetCode = FEI_write_to_error_file(szError);
      if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

      if (pszmessage) MEM_free(pszmessage);
    }
    else
    {
      int ifru = 0;
      fprintf(feilog, " found [%d] FRULIst bom items of comm item [%s]\n" ,iFRUListBomLines, szCommId);
      if (iFRUListBomLines == 0)
      {
        fprintf(feilog, " Adding commercial item id [%s] to the list to send mail\n", szCommId);

        if (gpszMailNoFRUList == NULL)
        {
          gpszMailNoFRUList = NULL;
          gpszMailNoFRUList = MEM_alloc((sizeof(char)* (int)strlen(szCommId) + 1 ));
          strcpy(gpszMailNoFRUList, szCommId);
          glNoFRUMail = true;
        }
        else
        {
          gpszMailNoFRUList= MEM_realloc(gpszMailNoFRUList,( sizeof(char)*(int)strlen(gpszMailNoFRUList)+ (int)strlen(szCommId) + 2 ) );
          strcat(gpszMailNoFRUList, szCommId);
          strcat(gpszMailNoFRUList, "\n");
          glNoFRUMail = true;
        }
      }
      for (ifru = 0; ifru<iFRUListBomLines; ifru++)
      {
        char *pszOwningSite = NULL;

        //get the id
        iRetCode = ITEM_ask_id(ptFRUListBomLines[ifru],szName);
        if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

        fprintf(feilog, " Checking owning site of fru bomline item revision[%s]\n", szName);
        if (FEI_is_remote_obj(ptFRUListBomLines[ifru],&pszOwningSite) == false)
        {
          tag_t tFRUListItem = ptFRUListBomLines[ifru];
          tag_t tFRUListRev = NULLTAG;

          /*ensure that the FRUList item is revised once*/
          tag_t tnewFRUListRev = NULLTAG;

          logical lMDRvalue = false;
          logical lFRUvalue = false;
          logical lFRUAcces = false;

          char *pszFRUvalue            = NULL;

          char szFRUItemId[1024];


          //if(FEI_get_bom_item(ptFRUListBomLines[ifru],&tFRUListItem)==true)
          //{

          fprintf(feilog, " getting FRU attr of item [%s]\n", szTargetId);
          iRetCode = FEI_get_FRU_attr_value(tTargetItem, &pszFRUvalue);
          if (iRetCode != ITK_ok)
          {
            char *pszmessage    = NULL;
            char szError[5012] = "";

            EMH_ask_error_text(iRetCode, &pszmessage);

            fprintf(feilog, "%s: Error (ITEM_list_bom_views) %s.\n", BNL_module, pszmessage);
            sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

            iRetCode = FEI_write_to_error_file(szError);
            if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

            if (pszmessage) MEM_free(pszmessage);
          }

          //get the id
          iRetCode = ITEM_ask_id(tFRUListItem,szFRUItemId);
          if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

          //get the latest rev (creation date)
          fprintf(feilog, " gettign latest rev (creation date) of fru bomline item[%s]\n", szFRUItemId);

          iRetCode =ITEM_ask_latest_rev(tFRUListItem, &tFRUListRev);
          if (BNL_em_error_handler("ITEM_ask_latest_rev", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

          iRetCode = AM_check_privilege(tFRUListItem, "WRITE", &lFRUAcces);
          if (iRetCode != ITK_ok)
          {
            char *pszmessage    = NULL;
            char szError[5012] = "";

            EMH_ask_error_text(iRetCode, &pszmessage);

            fprintf(feilog, "%s: Error (AM_check_privilege) %s.\n", BNL_module, pszmessage);

            sprintf(szError,"Error occured AM_check_privilege of FRUItem %s : %s",szFRUItemId, pszmessage);

            iRetCode = FEI_write_to_error_file(szError);
            if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

            if (pszmessage) MEM_free(pszmessage);
          }


          if (tFRUListRev != NULLTAG)
          {
            char szFRUDesc[256] = "";
            char szStatus[WSO_release_status_size_c + 1];

            logical lAccess = false;

            iRetCode =ITEM_ask_rev_description(tFRUListRev, szFRUDesc);
            if (BNL_em_error_handler("ITEM_ask_rev_description", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

            //check the description of the FRU item if it is same as Comm item desc.
            // If it is not same copy comm desc to FRU list item
            if (strcmp(szFRUDesc, szCommDesc) != 0)
            {
              fprintf(feilog, " Updating FRUList item[%s] description with value [%s]\n", szFRUItemId, szCommDesc);

              iRetCode = AM_check_privilege(tFRUListRev, "WRITE", &lAccess);
              if (iRetCode != ITK_ok)
              {
                char *pszmessage    = NULL;
                char szError[5012] = "";

                EMH_ask_error_text(iRetCode, &pszmessage);

                fprintf(feilog, "%s: Error (AM_check_privilege) %s.\n", BNL_module, pszmessage);

                sprintf(szError,"Error occured AM_check_privilege of item(updating the description) %s : %s",szFRUItemId, pszmessage);

                iRetCode = FEI_write_to_error_file(szError);
                if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                if (pszmessage) MEM_free(pszmessage);
              }

              if (lAccess == true)
              {
                iRetCode = BNL_tb_load_object(tFRUListRev, POM_modify_lock);
                if (iRetCode != ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (BNL_tb_load_object) %s.\n", BNL_module, pszmessage);

                  sprintf(szError,"Error occured updating description of item %s : %s",szFRUItemId, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                }

                iRetCode = BNL_tb_load_object(tFRUListItem, POM_modify_lock);
                if (iRetCode != ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (BNL_tb_load_object) %s.\n", BNL_module, pszmessage);

                  sprintf(szError,"Error occured updating description of item %s : %s",szFRUItemId, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                }

                iRetCode = ITEM_set_rev_description(tFRUListRev,szCommDesc ) ;
                if (iRetCode != ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (ITEM_set_rev_description) %s.\n", BNL_module, pszmessage);

                  sprintf(szError,"Error occured updating description of item %s : %s",szFRUItemId, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                }
                else
                {
                  iRetCode = AOM_save(tFRUListRev);
                  if (iRetCode != ITK_ok)
                  {
                    char *pszmessage    = NULL;
                    char szError[5012] = "";

                    EMH_ask_error_text(iRetCode, &pszmessage);

                    fprintf(feilog, "%s: Error (AOM_save) %s.\n", BNL_module, pszmessage);

                    sprintf(szError,"Error occured updating description of item %s : %s",szFRUItemId, pszmessage);

                    iRetCode = FEI_write_to_error_file(szError);
                    if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                    if (pszmessage) MEM_free(pszmessage);
                  }
                }
                iRetCode = ITEM_set_description(tFRUListItem,szCommDesc ) ;
                if (iRetCode != ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (ITEM_set_description) %s.\n", BNL_module, pszmessage);

                  sprintf(szError,"Error occured updating description of item %s : %s",szFRUItemId, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                }
                else
                {
                  iRetCode = AOM_save(tFRUListItem);
                  if (iRetCode != ITK_ok)
                  {
                    char *pszmessage    = NULL;
                    char szError[5012] = "";

                    EMH_ask_error_text(iRetCode, &pszmessage);

                    fprintf(feilog, "%s: Error (AOM_save) %s.\n", BNL_module, pszmessage);

                    sprintf(szError,"Error occured updating description of item %s : %s",szFRUItemId, pszmessage);

                    iRetCode = FEI_write_to_error_file(szError);
                    if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                    if (pszmessage) MEM_free(pszmessage);
                  }
                }
                iRetCode = BNL_tb_load_object(tFRUListRev, POM_no_lock);
                if (iRetCode != ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (BNL_tb_load_object) %s.\n", BNL_module, pszmessage);

                  sprintf(szError,"Error occured updating description of item %s : %s",szFRUItemId, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                }
                iRetCode = BNL_tb_load_object(tFRUListItem, POM_no_lock);
                if (iRetCode != ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (BNL_tb_load_object) %s.\n", BNL_module, pszmessage);

                  sprintf(szError,"Error occured updating description of item %s : %s",szFRUItemId, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                }
              }
            }

            /*JWO: MDR attr check is not done anymore. Status check is added*/
            /*fprintf(feilog, " gettign MDR attr of latest rev of fru bomline item[%s]\n", szFRUItemId);
            iRetCode = FEI_get_MDR_attr_value(tFRUListRev, &lMDRvalue);
            if (iRetCode == ITK_ok)
            {
              fprintf(feilog, " MDR attr has value [%d]\n", lMDRvalue);
              if (lMDRvalue == true)
              {
                fprintf(feilog, " MDR attr has value true, creating new revision\n");
                //create a new revision
                iRetCode = ITEM_copy_rev(tFRUListRev, NULL, &tnewFRUListRev);
                if (iRetCode != ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (ITEM_copy_rev) %s.\n", BNL_module, pszmessage);

                  sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                }
                else
                {
                  int iDeepcount = 0;
                  tag_t *ptDeepcopiedobjects = NULL;
                  iRetCode =ITEM_perform_deepcopy(tnewFRUListRev,ITEM_revise_operation,tFRUListRev,&iDeepcount,&ptDeepcopiedobjects);
                  if (iRetCode != ITK_ok)
                  {
                    char *pszmessage    = NULL;
                    char szError[5012] = "";

                    EMH_ask_error_text(iRetCode, &pszmessage);

                    fprintf(feilog, "%s: Error (ITEM_perform_deepcopy) %s.\n", BNL_module, pszmessage);

                    sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

                    iRetCode = FEI_write_to_error_file(szError);
                    if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                    if (pszmessage) MEM_free(pszmessage);
                  }

                  fprintf(feilog, "%s: iDeepcount [%d].\n", BNL_module,iDeepcount);
                  if (ptDeepcopiedobjects != NULL) MEM_free(ptDeepcopiedobjects);
                }
              }
              else
              {
                fprintf(feilog, "%s: MDR value is false, using latest rev.\n", BNL_module);
                tnewFRUListRev = tFRUListRev;
              }*/

            if (lFRUAcces ==false)
            {
              char szError[5012] = "";
              fprintf(feilog, "%s: Processing user has no write access on item  %s.\n", BNL_module, szFRUItemId);

              sprintf(szError,"Processing user has no write access on item  %s.",szFRUItemId);

              iRetCode = FEI_write_to_error_file(szError);
              if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;
              continue;
            }

            // assign to the project
            iRetCode = FEI_assign_to_project(tFRUListItem);
            if (iRetCode != ITK_ok)
            {
              char *pszmessage    = NULL;
              char szError[5012] = "";

              EMH_ask_error_text(iRetCode, &pszmessage);

              fprintf(feilog, "%s: Error (FEI_assign_to_project) %s.\n", BNL_module, pszmessage);

                sprintf(szError,"Error occured assigning item %s to project: %s",szFRUItemId, pszmessage);

              iRetCode = FEI_write_to_error_file(szError);
              if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;
              if (pszmessage) MEM_free(pszmessage);
            }
            else
            {
              fprintf(feilog, "%s: FRU item [%s] is assigned to project.\n", BNL_module, szFRUItemId );
            }

            iRetCode = BNL_tb_ask_object_status(tFRUListRev, szStatus);
            if (iRetCode != ITK_ok)
            {
              char *pszmessage    = NULL;
              char szError[5012] = "";

              EMH_ask_error_text(iRetCode, &pszmessage);

              fprintf(feilog, "%s: Error (BNL_tb_ask_object_status) %s.\n", BNL_module, pszmessage);

              sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

              iRetCode = FEI_write_to_error_file(szError);
              if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

              if (pszmessage) MEM_free(pszmessage);
            }
            else
            {
              if (strcmp(szStatus,"-") == 0)
              {
                fprintf(feilog, "%s: FRUITem revision has no status, using the latest revision.\n", BNL_module);
                tnewFRUListRev = tFRUListRev;
              }
              else
              {
                logical lAllreadyInList      = false;
                logical lRevise              = false;
                int iFRURev                  = 0;

                /*check if it is needed to revise*/
                iRetCode = FEI_check_FRUListItem_to_revise(tFRUListRev,tTargetItem, pszFRUvalue, &lRevise);
                if (iRetCode != ITK_ok)
                {
                  char *pszmessage    = NULL;
                  char szError[5012] = "";

                  EMH_ask_error_text(iRetCode, &pszmessage);

                  fprintf(feilog, "%s: Error (FEI_check_FRUListItem_to_revise) %s.\n", BNL_module, pszmessage);

                    sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                }
                else
                {
                  if (lRevise == false)
                  {
                    fprintf(feilog, "%s: FRUITem revision will not be revised.\n", BNL_module);
                    tnewFRUListRev = tFRUListRev;
                  }
                  else
                  {
                    logical lStatus = false;

                    iRetCode = FEI_check_comm_and_predecessor_status(tCommItem, &lStatus);
                    if (iRetCode != ITK_ok)
                    {
                      char *pszmessage    = NULL;
                      char szError[5012] = "";

                      EMH_ask_error_text(iRetCode, &pszmessage);

                      fprintf(feilog, "%s: Error (FEI_check_comm_and_predecessor_status) %s.\n", BNL_module, pszmessage);

                        sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

                      iRetCode = FEI_write_to_error_file(szError);
                      if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                      if (pszmessage) MEM_free(pszmessage);
                    }
                    else
                    {

                      //fprintf(feilog, " FRUITem revision has status, creating new revision\n");
                      fprintf(feilog, "_FEI_check_comm_and_predecessor_status returned [%d], creating new revision\n", lStatus);
                      //create a new revision
                      iRetCode = ITEM_copy_rev(tFRUListRev, NULL, &tnewFRUListRev);
                      if (iRetCode != ITK_ok)
                      {
                        char *pszmessage    = NULL;
                        char szError[5012] = "";

		                EMH_ask_error_text(iRetCode, &pszmessage);

		  	            fprintf(feilog, "%s: Error (ITEM_copy_rev) %s.\n", BNL_module, pszmessage);

	      	            sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

			            iRetCode = FEI_write_to_error_file(szError);
			            if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

			            if (pszmessage) MEM_free(pszmessage);
                      }
                      else
                      {
                        int iDeepcount = 0;
                        tag_t *ptDeepcopiedobjects = NULL;

                        tag_t tUser       = NULL_TAG;
                        tag_t tGroup      = NULL_TAG;
                        tag_t tNewStatus  = NULL_TAG;

                        iRetCode =ITEM_perform_deepcopy(tnewFRUListRev,ITEM_revise_operation,tFRUListRev,&iDeepcount,&ptDeepcopiedobjects);
                        if (iRetCode != ITK_ok)
                        {
                          char *pszmessage    = NULL;
                          char szError[5012] = "";

		                  EMH_ask_error_text(iRetCode, &pszmessage);

	                      fprintf(feilog, "%s: Error (ITEM_perform_deepcopy) %s.\n", BNL_module, pszmessage);

			              sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

			              iRetCode = FEI_write_to_error_file(szError);
			              if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

			              if (pszmessage) MEM_free(pszmessage);
                        }

                        fprintf(feilog, "%s: iDeepcount [%d].\n", BNL_module,iDeepcount);
                        if (ptDeepcopiedobjects != NULL) MEM_free(ptDeepcopiedobjects);

                        // JM|06122011: Get IRMF and reset attribute defined by pref FEI_MDR_MDRProcessed
                        iRetCode = FEI_reset_mdr_attr_value(tnewFRUListRev);
                        if (iRetCode != ITK_ok)
                        {
                            char *pszmessage    = NULL;
                            char szError[5012] = "";

                            EMH_ask_error_text(iRetCode, &pszmessage);

                            fprintf(feilog, "%s: Error (FEI_reset_mdr_attr_value) %s.\n", BNL_module, pszmessage);
                            sprintf(szError ,"Error occured processing item %s : %s",szFRUItemId, pszmessage);
                            iRetCode = FEI_write_to_error_file(szError);
                            if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                            if (pszmessage) MEM_free(pszmessage);
                         }


                        /*send mail that FRUList revision is revised*/
                        FEI_send_revise_mail(tFRUListItem);


                        /*collect the FRUListItem revs that is revised*/
                        ginewFRUListRev ++;
                        gptnewFRUListRev =( tag_t* )MEM_realloc( gptnewFRUListRev , (sizeof( tag_t ) * ginewFRUListRev ));
                        gptnewFRUListRev [ginewFRUListRev-1] = tnewFRUListRev;

                        gptoldFRUListRev =( tag_t* )MEM_realloc( gptoldFRUListRev , (sizeof( tag_t ) * ginewFRUListRev ));
                        gptoldFRUListRev [ginewFRUListRev-1] = tFRUListRev;

                        if (lStatus == false)
                        {
                          fprintf(feilog, "new FRUList Item revision will not get status.\n");

                          giStatus ++;
                          gppszStatus =( char** )MEM_realloc( gppszStatus , (sizeof( char * ) * (giStatus * sizeof(char*))));
                          gppszStatus [giStatus-1] = MEM_alloc(sizeof(char)* (int)strlen("-")+1);
                          strcpy(gppszStatus [giStatus-1], "-");
                        }
                        else
                        {
                          giStatus ++;
                          gppszStatus =( char** )MEM_realloc( gppszStatus , (sizeof( char * ) * (giStatus * sizeof(char*))));
                          gppszStatus [giStatus-1] = MEM_alloc(sizeof(char)* (int)strlen(szStatus)+1);
                          strcpy(gppszStatus [giStatus-1], szStatus);
                        }
                      }
                    }
                  }
                }
              }
            }
            if (tnewFRUListRev!=NULLTAG)
            {
              char szID[256] = "";
              int iBvs = 0;
              tag_t *ptBvs = NULL;
              tag_t tBv = NULLTAG;
              tag_t tBvr = NULLTAG;

              iRetCode = ITEM_ask_id(tTargetItem, szID);
              if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;


              fprintf(feilog, " getting bom views  of fru bomline item[%s]\n", szFRUItemId);
              iRetCode = ITEM_list_bom_views(tFRUListItem, &iBvs, &ptBvs);
              if (iRetCode == ITK_ok)
              {
                int k=0;
                char szBviewType[256] = "";
                fprintf(feilog, "\n found item [%s] iBvs [%d].\n", szName, iBvs);

                for (k=0; k<iBvs; k++)
                {
                  iRetCode = BNL_tb_determine_object_type(ptBvs[k], szBviewType);
                  if (strcmp(szBviewType,"view")==0)
                  {
                    tBv = ptBvs[k];
                    break;
                  }
                }
                if (tBv == NULLTAG)
                {
                  //create bv
                  fprintf(feilog, " No bom view found creating\n");

                  iRetCode = FEI_create_bv(tFRUListItem,tnewFRUListRev,&tBv,&tBvr);
                  if (iRetCode != ITK_ok)
                  {
                    char *pszmessage    = NULL;
                    char szError[5012] = "";

                    EMH_ask_error_text(iRetCode, &pszmessage);

                    fprintf(feilog, "%s: Error (FEI_create_bv) %s.\n", BNL_module, pszmessage);

                    sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

                    iRetCode = FEI_write_to_error_file(szError);
                    if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                    if (pszmessage) MEM_free(pszmessage);
                  }
                }
                else
                {
                  //create bvr
                  fprintf(feilog, " getting bom view revision\n");

                  iRetCode = FEI_get_bvr(tnewFRUListRev,tBv, &tBvr);
                  if (iRetCode != ITK_ok)
                  {
                    char *pszmessage    = NULL;
                    char szError[5012] = "";

                    EMH_ask_error_text(iRetCode, &pszmessage);

                    fprintf(feilog, "%s: Error (FEI_get_bvr) %s.\n", BNL_module, pszmessage);

                    sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

                    iRetCode = FEI_write_to_error_file(szError);
                    if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                   if (pszmessage) MEM_free(pszmessage);
                }
                }


                if (pszFRUvalue != NULL && strcmp(pszFRUvalue,gpszFRU[1]) == 0)
                {
                  fprintf(feilog, " FRU attr is true of item [%s], adding item as bomline to FRUList item [%s]\n",szTargetId,szFRUItemId);
                  //add the target item to the FRUlist item bom
                  //iRetCode = FEI_add_bomline(tBv,tFRUListItem,tTargetItem);

                  iRetCode = FEI_add_bomline(tBvr,tFRUListItem,tTargetItem);
                  if (iRetCode != ITK_ok)
                  {
                    char *pszmessage    = NULL;
                    char szError[5012] = "";

                    EMH_ask_error_text(iRetCode, &pszmessage);

                     fprintf(feilog, "%s: Error (FEI_add_bomline) %s.\n", BNL_module, pszmessage);

                    sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

                    iRetCode = FEI_write_to_error_file(szError);
                    if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                    if (pszmessage) MEM_free(pszmessage);
                  }
                }
                else
                {
                  fprintf(feilog, " FRU attr is false of item[%s], removing item as bomline\n", szTargetId);
                  //remove the target item from FRUList item bom

                  iRetCode = FEI_remove_bomline(tBv,tFRUListItem,tTargetItem);
                  if (iRetCode != ITK_ok)
                  {
                    char *pszmessage    = NULL;
                    char szError[5012] = "";

                    EMH_ask_error_text(iRetCode, &pszmessage);

                    fprintf(feilog, "%s: Error (FEI_remove_bomline) %s.\n", BNL_module, pszmessage);

                    sprintf(szError,"Error occured processing item %s : %s",szFRUItemId, pszmessage);

                    iRetCode = FEI_write_to_error_file(szError);
                    if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                    if (pszmessage) MEM_free(pszmessage);
                  }
                }
              }
              else
              {
                char *pszmessage    = NULL;
                char szError[5012] = "";

                EMH_ask_error_text(iRetCode, &pszmessage);

                fprintf(feilog, "%s: Error (FEI_get_FRU_attr_value) %s.\n", BNL_module, pszmessage);

                sprintf(szError,"Error occured processing item %s : %s",szID, pszmessage);

                  iRetCode = FEI_write_to_error_file(szError);
                  if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

                  if (pszmessage) MEM_free(pszmessage);
                }

            }
          }
        }
        else
        {
          fprintf(feilog, "%s: [%s] is a remote item,.\n", BNL_module, szName);
          /*char **pszExtraCols  = NULL;
          int iExtraCols = 0;
          char szId[ITEM_id_size_c + 1];

          //fprintf(feilog, "%s: item [%s] is a remote item, target will not be removed .\n", BNL_module, szName);
          fprintf(feilog, "%s: Owning site of item [%s] is [%s]. Creating csp file.\n", BNL_module, szName,pszOwningSite);
          iRetCode = ITEM_ask_id(tTargetItem, szId);
          if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

          iExtraCols = 2;
          pszExtraCols = (char **)malloc(sizeof(char*)*iExtraCols);
          BNL_tb_copy_string(szId, &pszExtraCols[0]);
          BNL_tb_copy_string(pszOwningSite, &pszExtraCols[1]);
          //BNL_tb_copy_string("veldhoven", &pszExtraCols[1]);

          // Do the thing
          if ((iRetCode = BNL_FEI_create_plot_request(tTargetItem, iExtraCols, pszExtraCols)) != 0) goto exit;

          for (i = 0; i < iExtraCols; i++)
            free(pszExtraCols[i]);
          if (iExtraCols > 0) free(pszExtraCols);*/
        }
      }
      if (iFRUListBomLines > 0) MEM_free(ptFRUListBomLines);
    }
  }
  else/*iif (FEI_check_comm_item_to_process(tParentItem) == true)*/
  {
    fprintf(feilog, "%s: Commercial item will not be processed .\n", BNL_module);
  }

exit:
  fprintf(feilog, "End ... FEI_process_item\n");
  return iRetCode;
}

/**
logical FEI_check_comm_item_to_process

  Description:

    checks if the comm item is to process

  Returns:

    true or false
)
*/
logical FEI_check_comm_item_to_process(tag_t tComObj)
{
  int iRetCode = 0;

  tag_t tIMFObj = NULLTAG;

  logical lProcess = false;

  char *pszAttrVal = NULL;
  char szStatus[256];
  char szType[256];
  char szID[256];

  fprintf(feilog, "Begin ... FEI_check_comm_item_to_process\n");

  iRetCode = ITEM_ask_id(tComObj, szID);
  if (BNL_em_error_handler("ITEM_ask_id", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BNL_tb_determine_object_type(tComObj, szType);
  if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  fprintf(feilog, "Item type [%s]\n", szType);

  iRetCode = FEI_get_secObj(tComObj, FEI_IMF_REL, &tIMFObj);
  if (iRetCode !=ITK_ok)
  {
    char *pszmessage    = NULL;
    char szError[5012] = "";

    EMH_ask_error_text(iRetCode, &pszmessage);

    fprintf(feilog, "%s: Error (FEI_get_secObj) %s.\n", BNL_module, pszmessage);

    sprintf(szError,"Error occured processing item %s : %s",szID, pszmessage);

    iRetCode = FEI_write_to_error_file(szError);
    if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

    if (pszmessage) MEM_free(pszmessage);
  }
  else
  {
    iRetCode = BNL_tb_determine_object_type(tIMFObj, szType);
    if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    fprintf(feilog, "IMF type [%s]\n", szType);

    fprintf(feilog, " %s: Getting [%s] attr \n",BNL_module, gpszCommSubclass);
    iRetCode = AOM_UIF_ask_value(tIMFObj, gpszCommSubclass, &pszAttrVal);
    if (iRetCode !=ITK_ok)
    {
      char *pszmessage    = NULL;
      char szError[5012] = "";

      EMH_ask_error_text(iRetCode, &pszmessage);

      fprintf(feilog, "%s: Error (AOM_UIF_ask_value) %s.\n", BNL_module, pszmessage);

      sprintf(szError,"Error occured processing item %s : %s",szID, pszmessage);

      iRetCode = FEI_write_to_error_file(szError);
      if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

      if (pszmessage) MEM_free(pszmessage);
    }
    else
    {
      if (pszAttrVal != NULL)
      {
        fprintf(feilog, " %s:[%s] has value [%s] \n",BNL_module, gpszCommSubclass,pszAttrVal);
        if (FEI_is_value_in_list(pszAttrVal, giFRUCommSubClasses, gpszFRUCommSubClasses)==true)
        {
          //attr value found, check the status now
          fprintf(feilog, " %s: [%s] has value, value found in the list, OK to process \n",BNL_module, gpszCommSubclass, pszAttrVal);

          fprintf(feilog, " %s: checking status\n", BNL_module);
          iRetCode = BNL_tb_ask_object_status(tComObj, szStatus);
          if (iRetCode !=ITK_ok)
          {
            char *pszmessage    = NULL;
            char szError[5012] = "";

            EMH_ask_error_text(iRetCode, &pszmessage);

            fprintf(feilog, "%s: Error (BNL_tb_ask_object_status) %s.\n", BNL_module, pszmessage);

            sprintf(szError,"Error occured processing item %s : %s",szID, pszmessage);

            iRetCode = FEI_write_to_error_file(szError);
            if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

            if (pszmessage) MEM_free(pszmessage);

          }
          else
          {
            if (FEI_is_value_in_list(szStatus, giFRUCommAllowedStatus, gpszFRUCommAllowedStatus)==true)
            {
              //status found, ok to process
              fprintf(feilog, " %s: Status [%s] found, OK to process \n",BNL_module, szStatus);
              lProcess = true;
            }
            else
            {
              //status not found, not ok to process
              fprintf(feilog, " %s: Status [%s] not found, NOT OK to process \n",BNL_module, szStatus);
              lProcess = false;
            }
          }
        }
        else
        {
          //attr value not found, not ok to process
          fprintf(feilog, " %s: [%s] value [%s] not found in the [%s], NOT OK to process \n",BNL_module, gpszCommSubclass,pszAttrVal,FEI_FRU_COMMERCIAL_SUBCLASS);
          lProcess = false;
        }
      }
      else
      {
        fprintf(feilog, " %s: [%s] has value null, will not be processed \n",BNL_module, gpszCommSubclass);
      }
    }
  }

exit:
  fprintf(feilog, "End ... FEI_check_comm_item_to_process\n");
  if (pszAttrVal != NULL) MEM_free(pszAttrVal);
  return lProcess;

}

/**
logical FEI_is_value_in_list

  Description:

    checks if the given value occurs in the list

  Returns:

    true or false
)
*/
logical FEI_is_value_in_list(char *pszValue, int iCount, char **pszValues)
{
  int iRetCode = 0;

  int k              = 0;

  logical lProcess = false;

  fprintf(feilog, "\nBegin ... FEI_is_value_in_list\n");

  if (iCount == 0)
  {
    fprintf(feilog, "%s: Warning, no values have been defined in FEI_is_value_in_list.\n\n", BNL_module);
    lProcess = false;
  }
  else
  {
    //fprintf(feilog, "%s: Found types using preference [%d].\n", BNL_module, iItemRevTypes);

    for (k=0; k<iCount; k++)
    {
      //fprintf(feilog, "%s: Found type [%s].\n", BNL_module, pszItemRevTypes[k]);
      if (strcmp(pszValue,pszValues[k]) == 0)
      {
        fprintf(feilog, "%s: value [%s] is listed.\n", BNL_module, pszValue);
        lProcess = true;
        break;
      }
    }
  }

  fprintf(feilog, "End ... FEI_is_value_in_list\n");
  return lProcess;
}

/**
int FEI_get_FRUList_bomlines

  Description:

    gettign fru bomlines

  Returns:

    0 on Ok
)
*/
int FEI_get_FRUList_bomlines(tag_t tComItem,tag_t **ptFRUListBomLines, int *iFRUListBomLines)
{
  int iRetCode = 0;
  int iCount = 0;
  int iBvs = 0;
  int iBvrs = 0;
  int k=0;
  int iBomLineCount = 0;
  int iBomLines = 0;

  char szBviewType[256] = "";
  char szObjType[256] = "";
  char szName[1024];
  char *pszConf       = NULL;

  tag_t tComItemRev   = NULL_TAG;
  tag_t tTopLine = NULLTAG;
  tag_t tBomWindow = NULL_TAG;

  tag_t *ptFRULists = NULL;
  tag_t *ptBvs = NULL;
  tag_t *ptBvrs = NULL;
  tag_t *ptBoms = NULL;

  fprintf(feilog, "\nBegin ... FEI_get_FRUList_bomlines\n");

  iRetCode = WSOM_ask_name(tComItem,szName);
  if (BNL_em_error_handler("WSOM_ask_name", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = CFM_item_ask_configured(gtRevRule, tComItem, &tComItemRev, &pszConf);
  if (BNL_em_error_handler("CFM_item_ask_configured", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  if (tComItemRev != NULL_TAG)
  {
    iRetCode = ITEM_rev_list_bom_view_revs(tComItemRev, &iBvrs, &ptBvrs);
    if (iRetCode == ITK_ok)
    {
      fprintf(feilog, "\n found itemrev [%s] iBvrs [%d].\n", szName, iBvrs);

      for (k=0; k<iBvrs; k++)
      {
        iRetCode = BNL_tb_determine_object_type(ptBvrs[k], szBviewType);
        if (strcmp(szBviewType,"view")==0)
        {
          iRetCode = BOM_create_window(&tBomWindow);
          if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

          iRetCode = BOM_set_window_top_line(tBomWindow, NULLTAG, tComItemRev, ptBvrs[k], &tTopLine);
          if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

          iRetCode = BOM_line_ask_child_lines(tTopLine, &iBomLineCount, &ptBoms);
          if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

          fprintf(feilog, "%s: Found [%d] bomlines.\n", BNL_module, iBomLineCount);
        }
      }
    }
    else
    {
      return iRetCode;
    }
  }
  else
  {
    iRetCode = ITEM_list_bom_views(tComItem, &iBvs, &ptBvs);
    if (iRetCode == ITK_ok)
    {
      fprintf(feilog, "\n found item [%s] iBvs [%d].\n", szName, iBvs);

      for (k=0; k<iBvs; k++)
      {
        iRetCode = BNL_tb_determine_object_type(ptBvs[k], szBviewType);
        if (strcmp(szBviewType,"view")==0)
        {
          //found bom view

          iRetCode = BOM_create_window(&tBomWindow);
          if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

          iRetCode = BOM_set_window_top_line(tBomWindow, tComItem, NULLTAG, ptBvs[k] , &tTopLine);
          if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

          iRetCode = BOM_line_ask_child_lines(tTopLine, &iBomLineCount, &ptBoms);
          if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

          fprintf(feilog, "%s: Found [%d] bomlines.\n", BNL_module, iBomLineCount);

          break;
        }
        else
        {
          fprintf(feilog, "%s: no bom view found.\n", BNL_module);
        }
      }
    }
    else
    {
      return iRetCode;
    }

  }

  // walk throuh the bomlines and find the FRUList items
  for (iBomLines=0; iBomLines<iBomLineCount; iBomLines++)
  {
    tag_t tBomItem = NULLTAG;

    if (FEI_get_bom_item(ptBoms[iBomLines],&tBomItem) == true)
    {
      iRetCode = BNL_tb_determine_object_type(tBomItem, szObjType);
      if (BNL_em_error_handler("BNL_tb_determine_object_type", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      fprintf(feilog, "%s: szObjType [%s] found.\n", BNL_module, szObjType);
      if (strcmp(szObjType,gpszFRUListItem)==0)
      {
        //get the name
        WSOM_ask_name(tBomItem,szName);
        fprintf(feilog, "%s: FRUList item [%s] found.\n", BNL_module, szName);
        iCount ++;
        ptFRULists = (tag_t *) MEM_realloc(ptFRULists, sizeof(tag_t) *iCount);
        ptFRULists[iCount -1] = tBomItem;
        // iCount ++;
      }
    }
  }

  if (tBomWindow != NULL_TAG) BOM_close_window(tBomWindow);

  if (iCount ==0)
  {
    fprintf(feilog, "%s: No [%s] bomlines found.\n", BNL_module, gpszFRUListItem);
  }

  *iFRUListBomLines = iCount;
  *ptFRUListBomLines = ptFRULists;

  if (pszConf != NULL) MEM_free(pszConf);
  if (ptBvs != NULL) MEM_free(ptBvs);
  if (ptBvrs != NULL) MEM_free(ptBvrs);

  fprintf(feilog, "\nEnd ... FEI_get_FRUList_bomlines\n");

  return iRetCode;
}

/**
int FEI_remove_bomline

  Description:

    removes a line

  Returns:

    0 on Ok
)
*/
int FEI_remove_bomline(tag_t tBv,tag_t parentItem, tag_t tObj)
{
  tag_t tTopLine = NULLTAG;
  tag_t tBomWindow = NULLTAG;

  int iBomLineCount = 0;
  int iBomLines = 0;
  int iRetCode = 0;

  tag_t *ptBoms = NULL;

  char *pszPUID1 = NULL;

  char szID[1024];

  fprintf(feilog, "\nBegin ... FEI_remove_bomline\n");

  // Lock the bvr
  iRetCode = BNL_tb_load_object(tBv, POM_modify_lock);
  if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BOM_create_window(&tBomWindow);
  if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  /*iRetCode = BNL_tb_bom_apply_config_rule(tBomWindow, gpszRevRule);
  if (BNL_em_error_handler("BNL_tb_bom_apply_config_rule", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
*/

  iRetCode = BOM_set_window_top_line(tBomWindow, parentItem,NULLTAG, tBv , &tTopLine);
  if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BOM_line_ask_child_lines(tTopLine, &iBomLineCount, &ptBoms);
  if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  fprintf(feilog, "%s: Found [%d] bomlines (FEI_remove_bomline).\n", BNL_module, iBomLineCount);

  //walk throuh the bomlines and find the target item and remove it
  POM_tag_to_string(tObj, &pszPUID1);
  for (iBomLines=0; iBomLines<iBomLineCount; iBomLines++)
  {
    char *pszPUID2 = NULL;
    tag_t tBomItem = NULLTAG;

    //get bom item
    if (FEI_get_bom_item(ptBoms[iBomLines],&tBomItem) == true)
    {
      POM_tag_to_string(tBomItem, &pszPUID2);
      if (strcmp(pszPUID1,pszPUID2)==0)
      {
        ITEM_ask_id(tBomItem,szID);
        fprintf(feilog, "%s: Found target item in FRUList of [%s].\n", BNL_module, szID);
        iRetCode = BOM_line_cut(ptBoms[iBomLines]);
        if (iRetCode != ITK_ok)
        {
          char *pszmessage    = NULL;
          char szError[5012] = "";

          EMH_ask_error_text(iRetCode, &pszmessage);

          fprintf(feilog, "%s: Error (BOM_line_cut) %s.\n", BNL_module, pszmessage);

          sprintf(szError,"Error occured removing bomline %s : %s",szID, pszmessage);

          iRetCode = FEI_write_to_error_file(szError);
          if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

          if (pszmessage) MEM_free(pszmessage);
        }
        else
        {
          fprintf(feilog, "%s:Target item removed.\n", BNL_module);
        }
      }
      if ( pszPUID2 != NULL) MEM_free(pszPUID2);
      pszPUID2 = NULL;
    }
  }
  if ( pszPUID1 != NULL) MEM_free(pszPUID1);
  pszPUID1 = NULL;

  if ( ptBoms != NULL) BNL_mm_free(ptBoms);
  ptBoms = NULL;

  iRetCode = BOM_save_window(tBomWindow);
  if (BNL_em_error_handler("BOM_save_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = AOM_save(tBv);
  if (BNL_em_error_handler("AOM_save", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BOM_close_window(tBomWindow);
  if (BNL_em_error_handler("BOM_close_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BNL_tb_load_object(tBv, POM_no_lock);
  if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  fprintf(feilog, "\nEnd ... FEI_remove_bomline\n");
exit:
  return iRetCode;

}

/**
int FEI_add_bomline

  Description:

   adds a line

  Returns:

    0 on Ok
)
*/
int FEI_add_bomline(tag_t tBv,tag_t tObj,tag_t tTargetItem)
{
  tag_t tTopLine = NULLTAG;
  tag_t tBomWindow = NULLTAG;
  tag_t tNewBomline = NULLTAG;
  tag_t tTargetRev = NULLTAG;

  int iBomLineCount = 0;
  int iBomLines = 0;
  int iRetCode = 0;

  tag_t *ptBoms = NULL;

  char *pszPUID1 = NULL;
  char *psconf = NULL;

  char szName[1024];

  logical lFound = false;

  char      szitem_id[ITEM_id_size_c+1];

  fprintf(feilog, "\nBegin ... FEI_add_bomline\n");

  ITEM_ask_id(tObj,szitem_id);
  fprintf(feilog, " tObj item id [%s] \n", szitem_id);


  iRetCode = CFM_item_ask_configured(gtRevRule, tTargetItem,&tTargetRev, &psconf);
  if (BNL_em_error_handler("CFM_item_ask_configured", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
  // Lock the bvr
  iRetCode = BNL_tb_load_object(tBv, POM_modify_lock);
  if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BOM_create_window(&tBomWindow);
  if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BOM_set_window_top_line(tBomWindow, tObj, NULLTAG, tBv , &tTopLine);
  if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  iRetCode = BOM_line_ask_child_lines(tTopLine, &iBomLineCount, &ptBoms);
  if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  fprintf(feilog, "%s: Found [%d] bomlines (FEI_add_bomline).\n", BNL_module, iBomLineCount);

  //walk throuh the bomlines and find the target item and remove it
  POM_tag_to_string(tTargetItem, &pszPUID1);
  for (iBomLines=0; iBomLines<iBomLineCount&&lFound==false; iBomLines++)
  {
    char *pszPUID2 = NULL;
    tag_t tBomItem = NULLTAG;

    //get bom item
    if (FEI_get_bom_item(ptBoms[iBomLines],&tBomItem) == true)
    {
      POM_tag_to_string(tBomItem, &pszPUID2);
      if (strcmp(pszPUID1,pszPUID2)==0)
      {
        WSOM_ask_name(tBomItem,szName);
        fprintf(feilog, "%s: Found target item in FRUList of [%s], will not be added.\n", BNL_module, szName);
        lFound = true;
      }
      if ( pszPUID2 != NULL) MEM_free(pszPUID2);
      pszPUID2 = NULL;
    }
  }
  if ( pszPUID1 != NULL) MEM_free(pszPUID1);
  pszPUID1 = NULL;

  if ( ptBoms != NULL) BNL_mm_free(ptBoms);
  ptBoms = NULL;

  if (lFound == false)
  {
    char *pszSeq = NULL;
    int iAttrSeqId = 0;
    int iAttrQty = 0;
    tag_t tTargetItemRev = NULL_TAG;
    char *pszConf = NULL;

    ITEM_ask_id(tTargetItem,szitem_id);
    fprintf(feilog, " tTargetItem item id [%s] \n", szitem_id);

    CFM_item_ask_configured(gtRevRule, tTargetItem, &tTargetItemRev, &pszConf);

    if (pszConf != NULL) MEM_free(pszConf);

    /*iRetCode = PS_create_occurrences(tBv,tTargetRev,NULLTAG,1,&ptOccs);
    if (BNL_em_error_handler("PS_create_occurrences", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    iRetCode = AOM_save(tBv);
    if (BNL_em_error_handler("AOM_save", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;*/

    //iRetCode = BOM_line_add(tTopLine,tTargetItem,NULLTAG,NULLTAG, &tNewBomline);
    if (tTargetItemRev != NULL_TAG)
    {
      fprintf(feilog, "Adding bomline [%d]...\n", tTargetItemRev);

      iRetCode = BOM_line_add(tTopLine,NULLTAG,tTargetItemRev,NULLTAG, &tNewBomline);
      if (BNL_em_error_handler("BOM_line_add", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      //set the sequence
      iRetCode=BOM_line_look_up_attribute(bomAttr_occSeqNo,&iAttrSeqId);
      if (BNL_em_error_handler("BOM_line_look_up_attribute",BNL_module,EMH_severity_error, iRetCode, true)) return false;

        //set the quantity
      iRetCode=BOM_line_look_up_attribute(bomAttr_occQty,&iAttrQty);
      if (BNL_em_error_handler("BOM_line_look_up_attribute",BNL_module,EMH_severity_error, iRetCode, true)) return false;

        iRetCode=BOM_line_set_attribute_string(tNewBomline,iAttrQty,"0");
      if (BNL_em_error_handler("BOM_line_set_attribute_string",BNL_module,EMH_severity_error, iRetCode, true)) return false;

      iRetCode=USER_ask_new_seqno(tBv,tTargetItem,&pszSeq);
      fprintf(feilog, " Sequence no [%s] \n", pszSeq);

      if (pszSeq == NULL)
      {
        pszSeq = USER_ask_for_new_sequence_no(tBv);
        fprintf(feilog, " New Sequence no [%s] \n", pszSeq);
      }

      if (pszSeq != NULL)
      {
        iRetCode=BOM_line_set_attribute_string(tNewBomline,iAttrSeqId,pszSeq);
        if (BNL_em_error_handler("BOM_line_set_attribute_string",BNL_module,EMH_severity_error, iRetCode, true)) return false;

        if (pszSeq != NULL) MEM_free(pszSeq);
      }


      iRetCode = BOM_save_window(tBomWindow);
      if (BNL_em_error_handler("BOM_save_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      iRetCode = AOM_save(tBv);
      if (BNL_em_error_handler("AOM_save", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }

    iRetCode = BOM_close_window(tBomWindow);
    if (BNL_em_error_handler("BOM_close_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    iRetCode = BNL_tb_load_object(tBv, POM_no_lock);
    if (BNL_em_error_handler("BNL_tb_load_object", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    fprintf(feilog, "\nEnd ... target is added as bomline\n");
  }

  fprintf(feilog, "\nEnd ... FEI_add_bomline\n");

  return iRetCode;

}

/**
int FEI_check_comm_and_predecessor_status

  Description:

   checks if the status od the commercial item revision same as his predecessor

  Returns:

    0 on Ok
)
*/
int FEI_check_comm_and_predecessor_status(tag_t tCommItem, logical *lStatus)
{
  int iRetCode  = 0;
  int iNum      = 0;

  tag_t tCommRev = NULLTAG;
  tag_t tPred    = NULLTAG;
  tag_t tRel     = NULLTAG;

  tag_t *ptObjects = NULL;

  char *psconf = NULL;

  char szStatus[256] = "";
  char szPredStatus[256] = "";

  fprintf(feilog, "\nBegin ... FEI_check_comm_and_predecessor_status\n");


  *lStatus = false;

  iRetCode = CFM_item_ask_configured(gtRevRule, tCommItem,&tCommRev, &psconf);
  if (BNL_em_error_handler("CFM_item_ask_configured", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  if (tCommRev == NULLTAG)
  {
    fprintf(feilog, "No revision found with revision, getting latest revision\n");
      ITEM_ask_latest_rev(tCommItem,&tCommRev );
  }

  iRetCode = BNL_tb_ask_object_status(tCommRev, szStatus);
  if (BNL_em_error_handler("BNL_tb_ask_object_status", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  if (FEI_is_value_in_list(szStatus, giFRUCommObsoleteStatus, gpszFRUCommObsoleteStatus))
  {
    fprintf(feilog, " %s: Status [%s] found (obsolete).\n", BNL_module, szStatus);
    *lStatus = true;
    goto function_exit;
  }
  else
  {
    fprintf(feilog, " %s: Status [%s] not found (obsolete).\n", BNL_module, szStatus);
  }

  if (strcmp(szStatus,"-") == 0)
  {
      fprintf(feilog, "%s: Commercial item has no status \n",BNL_module);
  }
  else
  {
    /*Get the predecessor*/
    iRetCode = GRM_find_relation_type(FEI_PREDECESSOR_REL, &tRel);
    if (BNL_em_error_handler("GRM_find_relation_type", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    iRetCode =  GRM_list_secondary_objects_only(tCommRev, tRel, &iNum, &ptObjects);
    if (BNL_em_error_handler("GRM_list_secondary_objects_only", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    //fprintf(feilog, "%s: iNum [%d] \n",BNL_module, iNum);

    if (iNum == 0)
    {
      fprintf(feilog, "%s: No predecessors have been found.\n",BNL_module);

        /*if (strcmp(szStatus,"-") == 0)
        {
          fprintf(feilog, "%s: Commercial item has no status \n",BNL_module);
        }
        else
        {
          *lStatus = true;
        }*/
    }
    if (iNum > 0)
    {
      tPred = ptObjects[0];

        fprintf(feilog, "%s: Predecessor found \n",BNL_module);
        iRetCode = BNL_tb_ask_object_status(tPred, szPredStatus);
      if (BNL_em_error_handler("BNL_tb_ask_object_status", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      if (strcmp(szPredStatus,szStatus) == 0)
      {
        *lStatus = true;
      }
    }
  }

function_exit:

  fprintf(feilog, "\nEnd ... FEI_check_comm_and_predecessor_status\n");


  if (ptObjects != NULL) MEM_free(ptObjects);
  if (psconf != NULL) MEM_free(psconf);

  return iRetCode;
}
/**
int FEI_check_FRUListItem_to_revise

  Description:

   checks if it is needed to revise FRUListItem revision

  Returns:

    0 on Ok
)
*/
int FEI_check_FRUListItem_to_revise(tag_t tFRUListRev, tag_t tItemTofind, char *pszFRU, logical *lRevise)
{
  int iRetCode      = 0;
  int iBvrs         = 0;
  int i             = 0;
  int iBomLineCount = 0;

  tag_t tBview      = NULLTAG;
  tag_t tBomWindow  = NULLTAG;
  tag_t tTopLine    = NULLTAG;

  tag_t *ptBvrs = NULL;
  tag_t *ptBoms = NULL;

  char szBviewType[256] = "";
  char szItemId[256]    = "";

  char *pszPUID1 = NULL;

  logical lFound = false;

  fprintf(feilog, "\nBegin ... FEI_check_FRUListItem_to_revise\n");


  *lRevise = false;

  iRetCode = ITEM_rev_list_all_bom_view_revs(tFRUListRev, &iBvrs, &ptBvrs);
  if (BNL_em_error_handler("ITEM_rev_list_all_bom_view_revs", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  if (iBvrs == 0) *lRevise = true;
  for (i=0; i<iBvrs; i++)
  {
    iRetCode = BNL_tb_determine_object_type(ptBvrs[i], szBviewType);
    if (strcmp(szBviewType,"view")==0)
    {
      tBview = ptBvrs[i];

      break;
    }
  }

  if (tBview != NULLTAG)
  {
    iRetCode = BOM_create_window(&tBomWindow);
    if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    iRetCode = BOM_set_window_top_line(tBomWindow, NULLTAG,tFRUListRev, tBview, &tTopLine);
    if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    iRetCode = BOM_line_ask_child_lines(tTopLine, &iBomLineCount, &ptBoms);
    if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    POM_tag_to_string(tItemTofind, &pszPUID1);
    ITEM_ask_id(tItemTofind, szItemId);

    for (i=0; i<iBomLineCount; i++)
    {
      char *pszPUID2 = NULL;
      tag_t tBomItem = NULLTAG;

      //get bom item
      if (FEI_get_bom_item(ptBoms[i],&tBomItem) == true)
      {
        POM_tag_to_string(tBomItem, &pszPUID2);
        if (strcmp(pszPUID1,pszPUID2)==0)
        {
          lFound = true;

          if (pszFRU != NULL && strcmp(pszFRU,gpszFRU[1]) == 0)
          {
            fprintf(feilog, " FRU attr is true,target item ia a bomline of FRUList item, not needed to revise\n");
            *lRevise = false;
          }
          else
          {
            fprintf(feilog, " FRU attr is false,target item ia a bomline of FRUList item(should be removed), needed to revise\n");
            *lRevise = true;
          }

        }
        if ( pszPUID2 != NULL) MEM_free(pszPUID2);
        pszPUID2 = NULL;
      }
    }
    if ( pszPUID1 != NULL) MEM_free(pszPUID1);
    pszPUID1 = NULL;

    if ( ptBoms != NULL) BNL_mm_free(ptBoms);
    ptBoms = NULL;

    if (lFound == false)
    {
      if (pszFRU != NULL && strcmp(pszFRU,gpszFRU[1]) == 0)
      {
        fprintf(feilog, " FRU attr is true,target item is not a bomline of FRUList item, need to revise\n");
        *lRevise = true;
      }
      else
      {
        fprintf(feilog, " FRU attr is false,target item is not a bomline of FRUList item(should be removed), not needed to revise\n");
        *lRevise = false;
      }
    }
  }

  if ( ptBvrs != NULL) BNL_mm_free(ptBvrs);
  ptBvrs = NULL;

fprintf(feilog, "\nEnd ... FEI_check_FRUListItem_to_revise\n");
  return iRetCode;
}

/**
int FEI_split_multiple_values
(
 char *pszValue,          <I>
 char chSeparator,        <I>
 int  *iCount,            <O>
 char ***pszValues        <OF>
)

  Description:

    Function to split string using the given separator

  Returns:

    0 on Ok

*/
int FEI_split_multiple_values
(
 char *pszValue,
 char chSeparator,
 int  *iCount,
 char ***pszValues
)
{
  int iRetCode                = 0;
  int i                       = 0;
  int iLen                    = 0;

  char *pszStart              = NULL;
  char *pszNode               = NULL;
  char *pszVal                = NULL;

  char **pszParsedVals        = NULL;


  pszNode = pszValue;

  /* Count the values */
  while (*pszNode!='\0')
  {
    if (*pszNode == chSeparator) i++;
    pszNode++;
  }
  i++; /* The last value */

  pszParsedVals = (char **)malloc(sizeof(char*)*i);

  /* Reset */
  pszNode = pszValue;
  i = 0;
  pszStart = pszNode;
  while (*pszNode!='\0')
  {
    if (*pszNode == chSeparator)
    {
      *pszNode = '\0';
      iLen = (int)strlen(pszStart) + 1;
      pszVal = (char*)malloc(sizeof(char) * iLen);
      strcpy(pszVal, pszStart);
      pszParsedVals[i] = pszVal;
      pszStart = pszNode + 1;
      i++;
    }
    pszNode++;
  }

  /* Last not separated value */
  iLen = (int)strlen(pszStart) + 1;
  pszVal=(char*)malloc(sizeof(char) * iLen);
  strcpy(pszVal, pszStart);
  pszParsedVals[i] = pszVal;
  i++;

  *iCount=i;
  *pszValues=pszParsedVals;

  return iRetCode;
}/* End of FEI_split_multiple_values */


/**
void FEI_send_mail

  Description:

  sends a mail
)
*/
int FEI_send_mail(char *pszMailRecp,char *pszSub)
{
  int iRetCode              = 0;
  int i                     = 0;

  char *pszReceiver         = NULL;
  char *pszMailServer       = NULL;
  char *pszBodyFile         = NULL;

  tag_t tEnv                = NULL_TAG;
  char szCommand[400];


  fprintf(feilog, "\nBegin ... FEI_send_mail\n");

  /*get the mail server name from iman env*/
  iRetCode=PREF_ask_char_value("Mail_server_name",0,&pszMailServer);
  if (BNL_em_error_handler("PREF_ask_char_value",BNL_module,EMH_severity_error,iRetCode, false)) goto exit;

  iRetCode =FEI_create_body_text(gpszMailNoFRUList, &pszBodyFile);
  //iRetCode =FEI_create_body_text("test jale\n testjale1\ntestjale2\n", &pszBodyFile);
  if (BNL_em_error_handler("FEI_create_body_text", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

  if (pszMailRecp == NULL || strlen(pszMailRecp)<1)
  {

    PREF_ask_char_value(FEI_FRU_FUNCTIONAL_EMAIL, 0, &pszReceiver);
    if (pszReceiver == NULL || strlen(pszReceiver) <1)
    {
      fprintf(feilog, "%s: Warning, no mail adress defined using preference [%s].\n\n", BNL_module, FEI_FRU_FUNCTIONAL_EMAIL);
      goto exit;
    }

    sprintf(szCommand,"tc_mail_smtp -to=\"%s\" -subject=\"%s\" -server=%s -body=\"%s\"",pszReceiver,pszSub,pszMailServer,pszBodyFile);

    fprintf(feilog,"%s: Sending:[%s] \n",BNL_module,szCommand);
  }
  else
  {
    sprintf(szCommand,"tc_mail_smtp -to=\"%s\" -subject=\"%s\" -server=%s -body=\"%s\"",pszMailRecp,pszSub,pszMailServer,pszBodyFile);

    fprintf(feilog,"%s: Sending:[%s] \n",BNL_module,szCommand);
  }

  system(szCommand);

  fprintf(feilog, "End ... FEI_send_mail\n");

exit:
  if (pszBodyFile != NULL) MEM_free(pszBodyFile);
  if (pszMailServer != NULL) MEM_free(pszMailServer);
  if (pszReceiver != NULL) MEM_free(pszReceiver);
  return iRetCode;
}

/**
int FEI_create_body_text
(
 char *pszBody,          <I>
 char **pszBodyFile  <OF>
)

  Description:

    writes the string in to a file

  Returns:

    true on Ok

*/
int FEI_create_body_text(char *pszBody, char **pszBodyFile)
{
  int iRetCode        =0;
  int iLen            =0;

  char *pszFileName   =NULL;
  char *pszPath       =NULL;

  FILE *fpToMail      =NULL;

  /*
  create a new body file in the iman temp directory
 */

 // fprintf(ugslog, "%s: body [%s].\n", BNL_module, pszBody);

  /*create a unique file name*/
  pszFileName=USER_new_file_name("mail","txt","txt",1);
  //pszPath=getenv("IMAN_TMP_DIR");
  pszPath=getenv("TEMP");

  iRetCode=BNL_fm_open_file(&fpToMail,pszPath,pszFileName,"w+t",1);
  if (BNL_em_error_handler("BNL_fm_open_file",BNL_module,EMH_severity_error,iRetCode, true)) return iRetCode;

  if (pszBody != NULL)
  {
    fprintf(fpToMail,"%s\n",pszBody);
  }


  fprintf(fpToMail,"\n\n");

  iRetCode=BNL_fm_close_file(fpToMail);
  BNL_em_error_handler("BNL_CloseFile",BNL_module,EMH_severity_error,iRetCode, false);

  iLen=( (int)strlen(pszPath) + (int)strlen(pszFileName) + 2 );
  *pszBodyFile=MEM_alloc(sizeof(char)*iLen);

  sprintf(*pszBodyFile,"%s%s%s",pszPath,FILESEP,pszFileName);

  return iRetCode;
}

/**************************************************************************************************
 * int BNL_FEI_create_plot_request:
 *
 * History of changes
 * Date        Author           Description
 * ----------- ---------------- -------------------------------------------------------------------
 * 06-Jun-2016 DTJ              Obsolete. From Tc10 replaced by BNL_create_dispatcher_request
 **************************************************************************************************/
int BNL_FEI_create_plot_request(tag_t tItemRev, int iExtraCols, char **pszExtraCols)
{
  int iRetCode            = 0;

  iRetCode = BNL_create_dispatcher_request(tItemRev, "FEI_FRU_CSP_INI_FRULIST", iExtraCols, pszExtraCols);
  if (BNL_em_error_handler( "BNL_create_dispatcher_request", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

function_exit:

  return iRetCode;
}
/*
int BNL_FEI_create_plot_request(tag_t tItemRev,  int iExtraCols, char **pszExtraCols)
{
  int iRetCode            = 0;
  int iTypesCount         = 0;
  int iScanFolder         = 0;

  char szPrefName[256]    = {""};

  char *pszIniFile        = NULL;
  char *pszTempFile       = NULL;
  char *pszTemp           = NULL;
  char *pszDevice         = NULL; // Freed by csp module
  char *pszScanDirectory  = NULL;
  char *pszScanFolder     = NULL;
  char *pszOutputTemp     = getenv("TEMP");

  tag_t tFolder           = NULL_TAG;

  char **pszObjectTypes   = NULL; // Freed by csp module

  BNL_csp_initialise(feilog, NULL);

  PREF_set_search_scope(IMAN_preference_site);

  iRetCode = PREF_ask_char_value("FEI_FRU_CSP_INI_FRULIST", 0, &pszTempFile);
  if (pszTempFile == NULL)
  {
    fprintf(feilog, "%s: Preference FEI_FRU_CSP_INI_FRULIST is not defined. No PlotRequest is being created!\n", BNL_module);
  }
  else
  {
    BNL_tb_replace_env_for_path(pszTempFile, &pszIniFile);
    if (pszIniFile == NULL)
      pszIniFile = pszTempFile;
    else
    {
      MEM_free(pszTempFile);
      pszTempFile = NULL;
    }

    fprintf(feilog, "%s: Using ini file [%s]\n", BNL_module, pszIniFile);

    if (!BNL_fm_check_file_or_dir(pszIniFile))
    {
      fprintf(feilog, "%s: Error, the given csv inifile [%s] not found.\n" , BNL_module, pszIniFile);
      goto function_exit;
    }

    BNL_csp_set_ini(pszIniFile);

    // Scanfolder?
    if (BNL_ini_read_ini_value(pszIniFile, "scan", "scanfolder", &pszScanFolder) == 0)
    {
      iRetCode = BNL_tb_find_folder(pszScanFolder, &tFolder);
      if (BNL_em_error_handler( "BNL_tb_find_folder", BNL_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      // check existence
      if (tFolder == NULL_TAG)
      {
        fprintf(feilog, "%s: Error, scan folder [%s] not found.\n", BNL_module, pszScanFolder);
        goto function_exit;
      }
      iScanFolder = 1;
    }
    else
    {
      if (BNL_ini_read_ini_value(pszIniFile, "scan", "scandir", &pszTemp) == 0)
      {
        BNL_tb_replace_all_env_for_path(pszTemp, &pszScanDirectory);
        if (pszScanDirectory == NULL)
        {
          pszScanDirectory = MEM_alloc(sizeof(char)*((int)strlen(pszTemp) + 1));
          strcpy(pszScanDirectory, pszTemp);
        }
        free(pszTemp);

        // check existence
        if (BNL_fm_check_file_or_dir( pszScanDirectory) == 0)
        {
          fprintf( feilog, "%s: Error, scan directory [%s] not found.\n", BNL_module, pszScanDirectory);
          goto function_exit;
        }
      }
      else
      {
        fprintf( feilog, "%s: Error, no scanfolder or scandir has been defined.\n", BNL_module);
        goto function_exit;
      } // End of if (BNL_ini_read_ini_value(pszIniFile, SCAN_CONFIG, "scandir", &pszTemp) == 0)
    } // End of if (BNL_ini_read_ini_value(pszInFile, SCAN_CONFIG, "scanfolder", &pszScanFolder) == 0)

    // Get the object types
    if (BNL_ini_get_multiple_values(pszIniFile, CREATOR_CONFIG, "object_types", ';', &iTypesCount, &pszObjectTypes) != 0)
    {
      fprintf( feilog, "%s: Error, object_types has not been defined.\n", BNL_module);
      goto function_exit;
    }
    else
    {
      BNL_csp_set_objecttypes(iTypesCount, pszObjectTypes);
    }

    // Get the device
    if (BNL_ini_read_ini_value(pszIniFile, CREATOR_CONFIG, "device", &pszDevice) != 0)
    {
      fprintf( feilog, "%s: Error, device has not been defined.\n", BNL_module);
      goto function_exit;
    }
    else
    {
      BNL_csp_set_device(pszDevice);
    }

    // Set the output directory
    if (pszOutputTemp != NULL)
    {
      BNL_csp_set_csp_out_dir(pszOutputTemp);
    }
    else
    {
      fprintf( feilog, "%s: Error, setting [TEMP] has not been defined.\n", BNL_module);
      goto function_exit;
    }

    // Extra col
    if (iExtraCols > 0)
    {
      BNL_csp_set_extracols(iExtraCols, pszExtraCols);
    }

    // Create csp file in temporary directory
    if ((iRetCode = BNL_create_csp_file(tItemRev)) != 0) goto function_exit;

    if (iScanFolder)
    {
      if ((iRetCode = store_csv_file()) != 0) goto function_exit;
      if ((iRetCode = send_plotrequest(NULL_TAG, CUT_AND_PASTE, pszScanFolder)) != 0) goto function_exit;
    }
    else
    {
      if ((iRetCode = send_csv_file(pszScanDirectory)) != 0) goto function_exit;
    }
  } // End of if (pszTemp == NULL)

function_exit:

  BNL_csp_exit();

  if (pszIniFile != NULL) MEM_free(pszIniFile);
  if (pszScanFolder != NULL) free(pszScanFolder);
  if (pszScanDirectory != NULL) MEM_free(pszScanDirectory);

  return iRetCode;
}
*/

/**
int FEI_collect_input_items

  Description:

    collect input items for process

  Returns:

    true or false
)
*/
int FEI_collect_input_items(tag_t tBomLine, tag_t tPredecessor)
{
  int iRetCode      = 0;
  int iBomLineCount = 0;
  int iBom          = 0;
  int i             = 0;

  int iCount        = 0;

  tag_t *ptBoms = NULL;

  tag_t tBomItem = NULLTAG;

  char szObjType[256] ="";


  /*if tPredecessor is NULLTAG than it is not the first level bomlines */
  if (tPredecessor != NULLTAG)
  {
    tag_t *ptBvrs = NULL;
    tag_t *ptBomLinesPred = NULL;
    tag_t *ptBomLines = NULL;

    tag_t tBomWindow = NULLTAG;
    tag_t tTopLine   = NULLTAG;
    tag_t tBview     = NULLTAG;
    tag_t tBomItemPred = NULLTAG;

    char szBviewType[256] = "";

    int iBvrs         = 0;
    int iBomLinesPred = 0;
    int iBomLines     = 0;
    int k             = 0;

    fprintf(feilog, "%s: Predecessor processing \n",BNL_module);

    iRetCode = BOM_line_ask_child_lines(tBomLine, &iBomLines, &ptBomLines);
    if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    /*Get the predecessor bomlines*/
    iRetCode = ITEM_rev_list_bom_view_revs(tPredecessor, &iBvrs, &ptBvrs);
    if (BNL_em_error_handler("ITEM_rev_list_bom_view_revs", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    for (k=0; k<iBvrs; k++)
    {
      iRetCode = BNL_tb_determine_object_type(ptBvrs[k], szBviewType);
      if (strcmp(szBviewType,"view")==0)
      {
        tBview = ptBvrs[k];

        break;
      }
    }

    if (tBview != NULL_TAG)
    {
      iRetCode = BOM_create_window(&tBomWindow);
      if (BNL_em_error_handler("BOM_create_window", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      iRetCode = BOM_set_window_top_line(tBomWindow, NULLTAG, tPredecessor, tBview , &tTopLine);
      if (BNL_em_error_handler("BOM_set_window_top_line", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      iRetCode = BOM_line_ask_child_lines(tTopLine, &iBomLinesPred, &ptBomLinesPred);
      if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

      fprintf(feilog, "%s:Predecessor has [%d] bomlines.\n", BNL_module,iBomLinesPred );

      if (iBomLinesPred == 0)
      {
        iRetCode = BOM_line_ask_child_lines(tBomLine, &iBomLineCount, &ptBoms);
        if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
      }
      else
      {
        for (k=0; k<iBomLines; k++)
        {
          for (i=0; i<iBomLinesPred; i++)
          {
            FEI_get_bom_item(ptBomLines[k], &tBomItem);
            FEI_get_bom_item(ptBomLinesPred[i], &tBomItemPred);
            if (tBomItemPred == tBomItem)
            {
              fprintf(feilog, "%s: Bomline found in Predecessor, skipping.\n", BNL_module);
            }
            else
            {
              iRetCode = BNL_tb_determine_object_type(tBomItem, szObjType);
              if (iRetCode != ITK_ok)
              {
                fprintf(feilog, "%s: Error by FEI_collect_input_items BNL_tb_determine_object_type.\n", BNL_module);
              }
              else
              {
                if (strcmp(szObjType, gpszFRUListItem) == 0)
                {
                  fprintf(feilog, "%s: Bomline [%s] found, skipping.\n", BNL_module,gpszFRUListItem);
                }
                else
                {
                  iBomLineCount ++;
                  ptBoms =( tag_t* )MEM_realloc( ptBoms , (sizeof( tag_t ) * iBomLineCount ));
                  ptBoms [iBomLineCount-1] = ptBomLines[k];
                }
              }
            }
          }
        }
      }
    }
    else
    {
      iRetCode = BOM_line_ask_child_lines(tBomLine, &iBomLineCount, &ptBoms);
      if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
    }
  }
  else
  {
    iRetCode = BOM_line_ask_child_lines(tBomLine, &iBomLineCount, &ptBoms);
    if (BNL_em_error_handler("BOM_line_ask_child_lines", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;
  }

  fprintf(feilog, "%s: Found [%d] bomlines.\n", BNL_module, iBomLineCount);

  //walk throuh the bomlines
  for (iBom=0; iBom<iBomLineCount; iBom++)
  {
    if (FEI_get_bom_item(ptBoms[iBom], &tBomItem) == true)
    {
      iRetCode = BNL_tb_determine_object_type(tBomItem, szObjType);
      if (iRetCode != ITK_ok)
      {
        fprintf(feilog, "%s: Error by FEI_collect_input_items BNL_tb_determine_object_type.\n", BNL_module);
      }
      else
      {
        fprintf(feilog, "%s: szObjType [%s] found.\n", BNL_module, szObjType);
        if ((strcmp(szObjType,gpszCommItem)==0) || (strcmp(szObjType,gpszFRUListItem)==0))
        {
          fprintf(feilog, "%s: Bomline [%s] found skipping.\n", BNL_module, szObjType);
        }
        else
        {
          logical lAllreadyInList      = false;

          // JM|22-07-2010: Skip if already in list
          for (i = 0; i < giItems; i++)
          {
            if (tBomItem == gptItems[i])
            {
              fprintf(feilog, "%s: Item already in list, skipping....\n", BNL_module);
              lAllreadyInList = true;
              break;
            }
          }

          if (!lAllreadyInList)
          {
            giItems++;
            gptItems = (tag_t*) MEM_realloc(gptItems, sizeof(tag_t) * giItems);
            gptItems[giItems-1] = tBomItem;

            if ((iRetCode = FEI_collect_input_items(ptBoms[iBom], NULLTAG)) != 0)
            {
              char *pszmessage    = NULL;
              char szError[5012] = "";

              EMH_ask_error_text(iRetCode, &pszmessage);

              fprintf(feilog, "%s: Error (FEI_collect_input_items) %s.\n", BNL_module, pszmessage);

              sprintf(szError,"Error occured collecting bomlines of item %s : %s",gszItemid, pszmessage);

              iRetCode = FEI_write_to_error_file(szError);
              if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

              if (pszmessage) MEM_free(pszmessage);
            }
          }
        }
      }
    }
  }
exit:
  if (iBomLineCount>0) MEM_free(ptBoms);

  return iRetCode;
}

/**
int FEI_find_comm_parents

  Description:

    collect commercial parents

  Returns:

    true or false
)
*/
int FEI_find_comm_parents(tag_t tItemRev, tag_t **ptCParents, int *iCParents)
{
  int iRetCode      = 0;
  int iPs           = 0;
  int i             = 0;
  int j             = 0;
  int iComms        = 0;

  int *piLevels    = NULL;

  tag_t tParentItem = NULLTAG;

  tag_t *ptTParentsTemp   = NULL;
  tag_t *ptComms          = NULL;

  char szObjType[256] ="";

  fprintf(feilog, "Begin ... FEI_find_comm_parents\n");

  ptComms = *ptCParents;
  iComms = *iCParents;

  iRetCode = PS_where_used_configured(tItemRev, gtRevRule,1,&iPs,&piLevels,&ptTParentsTemp);
  if (BNL_em_error_handler("PS_where_used_configured", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  fprintf(feilog, "%s: Found [%d] parents.\n", BNL_module, iPs);

  for (i=0; i<iPs; i++)
  {
    //get the item
    iRetCode = ITEM_ask_item_of_rev(ptTParentsTemp[i], &tParentItem);
    if (tParentItem != NULLTAG)
    {
      BNL_tb_determine_object_type(tParentItem, szObjType);
      fprintf(feilog, " %s: szObjType [%s]\n", BNL_module, szObjType);
      if (strcmp(szObjType,gpszCommItem)==0)
      {
        logical lAllreadyInList = false;

        fprintf(feilog, "Commercial parent item found.\n");

        //fprintf(feilog, "iComms %d.\n", iComms);

        // JM|22-07-2010: Skip if already in list
        for (j = 0; j < iComms; j++)
        {
          if (tParentItem == ptComms[j])
          {
            fprintf(feilog, "%s: Commercial Item already in list, skipping....\n", BNL_module);
            lAllreadyInList = true;
            break;
          }
        }

        if (!lAllreadyInList)
        {
            iComms++;
          ptComms = (tag_t *) MEM_realloc(ptComms, sizeof(tag_t) * iComms);
          ptComms[iComms-1] = tParentItem;

          fprintf(feilog, "Commercial parent added to the list.\n");
        }
      }
      else
      {
        iRetCode = FEI_find_comm_parents(ptTParentsTemp[i], &ptComms, &iComms);
        if (iRetCode != ITK_ok)
        {
          char *pszmessage    = NULL;
          char szError[5012] = "";

          EMH_ask_error_text(iRetCode, &pszmessage);

          fprintf(feilog, "%s: Error (FEI_find_comm_parents) %s.\n", BNL_module, pszmessage);

          sprintf(szError,"Error occured processing FEI_find_comm_parents function : %s", pszmessage);

          iRetCode = FEI_write_to_error_file(szError);
          if (BNL_em_error_handler("FEI_write_to_error_file", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;

          if (pszmessage) MEM_free(pszmessage);
        }
      }
    }
  }

  *ptCParents = ptComms;
  *iCParents = iComms;

exit:
 // if ( ptTParentsTemp != NULL) BNL_mm_free(ptTParentsTemp);
  if ( piLevels != NULL) BNL_mm_free(piLevels);

  fprintf(feilog, "End ... FEI_find_comm_parents\n");

  return iRetCode;
}

/**
int FEI_find_comm_parent

  Description:

    collect commercial parent

  Returns:

    true or false
)
*/
int FEI_find_comm_parent(tag_t tItemRev, tag_t *tCommParent)
{
  int iRetCode      = 0;
  int iPs           = 0;
  int i             = 0;
  int j             = 0;
  int iComms        = 0;

  int *piLevels    = NULL;

  tag_t tParentItem = NULLTAG;

  tag_t *ptTParentsTemp   = NULL;
  tag_t *ptComms          = NULL;

  char szObjType[256] ="";

  fprintf(feilog, "Begin ... FEI_find_comm_parent\n");

  iRetCode = PS_where_used_configured(tItemRev, gtRevRule,1,&iPs,&piLevels,&ptTParentsTemp);
  if (BNL_em_error_handler("PS_where_used_configured", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

  fprintf(feilog, "%s: Found [%d] parents.\n", BNL_module, iPs);

  for (i=0; i<iPs; i++)
  {
    //get the item
    iRetCode = ITEM_ask_item_of_rev(ptTParentsTemp[i], &tParentItem);
    if (tParentItem != NULLTAG)
    {
      BNL_tb_determine_object_type(tParentItem, szObjType);
      fprintf(feilog, " %s: szObjType [%s]\n", BNL_module, szObjType);
      if (strcmp(szObjType,gpszCommItem)==0)
      {

           fprintf(feilog, "Commercial parent found .\n");
        *tCommParent = ptTParentsTemp[i];
        break;

      }
    }
  }

//exit:
 // if ( ptTParentsTemp != NULL) BNL_mm_free(ptTParentsTemp);
  if ( piLevels != NULL) BNL_mm_free(piLevels);

  fprintf(feilog, "End ... FEI_find_comm_parent\n");

  return iRetCode;
}

/**
int FEI_create_body_text
(
 char *pszBody,          <I>
 char **pszBodyFile  <OF>
)

  Description:

    writes the string in to a file

  Returns:

    true on Ok

*/
int FEI_write_to_error_file(char *pszText)
{
  int iRetCode        =0;
  int iLen            =0;

  char *pszFileName   =NULL;
  char *pszPath       =NULL;

  /*
  create a new body file in the iman temp directory
 */

 // fprintf(ugslog, "%s: body [%s].\n", BNL_module, pszBody);

  /*create a unique file name*/
  pszFileName = USER_new_file_name("mail","txt","txt",1);
  pszPath = getenv("TEMP");

  if (gfpErrorMail == NULL)
  {
    iRetCode = BNL_fm_open_file(&gfpErrorMail, pszPath, pszFileName,"w+t",1);
    if (BNL_em_error_handler("BNL_fm_open_file", BNL_module, EMH_severity_error, iRetCode, true)) return iRetCode;

    iLen=((int)strlen(pszPath) + (int)strlen(pszFileName) + 2);
    gpszErrorFile=MEM_alloc(sizeof(char)*iLen);

    sprintf(gpszErrorFile, "%s%s%s", pszPath, FILESEP, pszFileName);

  }

  if (pszText != NULL)
  {
    fprintf(gfpErrorMail,"%s\n", pszText);
  }
  return iRetCode;
}

int FEI_send_revise_mail(tag_t tFRUListItem)
{
  int iRetCode              = 0;
  int i                     = 0;

  char *pszReceiver         = NULL;
  char *pszMailServer       = NULL;
  char *pszBodyFile         = NULL;

  char szMsg[5000]          = "";
  char szItemId[256]        = "";
  char szDesc[2000]         = "";


  tag_t tEnv                = NULL_TAG;
  char szCommand[400];


  //fprintf(feilog, "\nBegin ... FEI_send_revise_mail\n");

  /*get the mail server name from iman env*/
  iRetCode = PREF_ask_char_value("Mail_server_name", 0,& pszMailServer);
  if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) goto exit;

  PREF_ask_char_value(FEI_FRU_FUNCTIONAL_EMAIL, 0, &pszReceiver);
  if (pszReceiver == NULL || strlen(pszReceiver) <1)
  {
    fprintf(feilog, "%s: Warning, no mail adress defined using preference [%s].\n\n", BNL_module, FEI_FRU_FUNCTIONAL_EMAIL);
    goto exit;
  }

  ITEM_ask_id(tFRUListItem,szItemId);

  ITEM_ask_description(tFRUListItem,szDesc);

  sprintf(szMsg,"FRUList item %s with description %s has been revised.", szItemId,szDesc);

  //FEI_create_body_text("", &pszBodyFile);


  //sprintf(szCommand,"tc_mail_smtp -to=\"%s\" -subject=\"%s\" -server=%s -body=\"%s\"", pszReceiver, szMsg, pszMailServer, gpszErrorFile);
  sprintf(szCommand,"tc_mail_smtp -to=\"%s\" -subject=\"%s\" -server=%s ", pszReceiver, szMsg, pszMailServer);

  //sprintf(szCommand,"tc_mail_smtp -to=\"%s\" -subject=\"%s\" -server=%s -body=\"%s\"","jale79@hotmail.com", pszSub,"nl-smtp.asml.com", pszBodyFile);


  fprintf(feilog, "%s: Sending:[%s] \n", BNL_module, szCommand);


  system(szCommand);

  //fprintf(feilog, "End ... FEI_send_mail\n");

exit:
  if (pszBodyFile != NULL) MEM_free(pszBodyFile);
  if (pszMailServer != NULL) MEM_free(pszMailServer);
  if (pszReceiver != NULL) MEM_free(pszReceiver);

  return iRetCode;
}

int FEI_send_error_mail(char *pszMailRecp)
{
  int iRetCode              = 0;
  int i                     = 0;

  char *pszReceiver         = NULL;
  char *pszMailServer       = NULL;
  char *pszBodyFile         = NULL;

  tag_t tEnv                = NULL_TAG;
  char szCommand[400];


  //fprintf(feilog, "\nBegin ... FEI_send_mail\n");

  /*get the mail server name from iman env*/
  iRetCode = PREF_ask_char_value("Mail_server_name", 0,& pszMailServer);
  if (BNL_em_error_handler("PREF_ask_char_value", BNL_module, EMH_severity_error, iRetCode, false)) goto exit;

  if (pszMailRecp == NULL || strlen(pszMailRecp)<1)
  {

    PREF_ask_char_value(FEI_FRU_FUNCTIONAL_EMAIL, 0, &pszReceiver);
    if (pszReceiver == NULL || strlen(pszReceiver) <1)
    {
      fprintf(feilog, "%s: Warning, no mail adress defined using preference [%s].\n\n", BNL_module, FEI_FRU_FUNCTIONAL_EMAIL);
     // goto exit;
    }

    sprintf(szCommand,"tc_mail_smtp -to=\"%s\" -subject=\"%s\" -server=%s -body=\"%s\"", pszReceiver, "Error occured during execution healthcheck", pszMailServer, gpszErrorFile);

    //sprintf(szCommand,"tc_mail_smtp -to=\"%s\" -subject=\"%s\" -server=%s -body=\"%s\"","jale79@hotmail.com", pszSub,"nl-smtp.asml.com", pszBodyFile);

    fprintf(feilog, "%s: Sending:[%s] \n", BNL_module, szCommand);
  }
  else
  {
    sprintf(szCommand,"tc_mail_smtp -to=\"%s\" -subject=\"%s\" -server=%s -body=\"%s\"", pszMailRecp, "Error occured during execution healthcheck", pszMailServer, gpszErrorFile);

    fprintf(feilog, "%s: Sending:[%s] \n", BNL_module, szCommand);
  }

  iRetCode = BNL_fm_close_file(gfpErrorMail);
  BNL_em_error_handler("BNL_CloseFile", BNL_module, EMH_severity_error, iRetCode, false);

  system(szCommand);

  //fprintf(feilog, "End ... FEI_send_mail\n");

exit:
  if (pszBodyFile != NULL) MEM_free(pszBodyFile);
  if (pszMailServer != NULL) MEM_free(pszMailServer);
  if (pszReceiver != NULL) MEM_free(pszReceiver);

  return iRetCode;
}

/**
int FEI_assign_to_project
(
 tag_t tItem,          <I>
 tag_t tRev,           <I>
)

  Description:

    writes the string in to a file

  Returns:

    true on Ok

*/
int FEI_assign_to_project(tag_t tItem)
{
  int iRetCode        =0;

  tag_t tProject      = NULLTAG;

  tag_t *ptObjs       = NULL;
  tag_t *ptBvrs       = NULL;
  tag_t *ptProjects   = NULL;
  char *pszProject    = NULL;

  int i               = 0;
  int iObjs           = 1;
  int iBvrs           = 0;

  char szObjType[256] = "";

  iRetCode = PREF_ask_char_value(FEI_ASSIGN_PROJECT, 0, &pszProject);
  if (pszProject != NULL)
  {
    fprintf(feilog, "%s: [%s] has value [%s]\n",BNL_module,FEI_ASSIGN_PROJECT,pszProject);
  }
  else
  {
    fprintf(feilog, "%s: Warning, preference [%s] is not defined. Exiting!!\n\n", BNL_module, FEI_ASSIGN_PROJECT);
    goto exit;
  }

  iRetCode = PROJ_find(pszProject, &tProject);
  if (BNL_em_error_handler("PROJ_find", BNL_module, EMH_severity_error, iRetCode, false)) goto exit;

  if (tProject != NULLTAG)
  {
    fprintf(feilog, "%s: Assiging to project [%s]\n",BNL_module,pszProject);

    ptProjects = MEM_alloc(sizeof(tag_t) * 1);
    ptProjects[0] = tProject;

    ptObjs = MEM_alloc(sizeof(tag_t) * iObjs);
    ptObjs[0] =  tItem;

    iRetCode = PROJ_assign_objects(1,ptProjects,iObjs,ptObjs);
    if (BNL_em_error_handler("PROJ_assign_objects", BNL_module, EMH_severity_error, iRetCode, true)) goto exit;
  }


exit:

  if ( ptBvrs != NULL) BNL_mm_free(ptBvrs);
  if ( ptProjects != NULL) BNL_mm_free(ptProjects);
  if ( ptObjs != NULL) BNL_mm_free(ptObjs);

  return iRetCode;

}

/**
int FEI_get_form()

    Description :

        Find the form of the given object.

    Returns     :

        False or True;


*/
int FEI_get_form(tag_t tObject, char *pszRel, tag_t *tForm)
{
  int    iRetCode =0;
  int    iCount   =0;

  tag_t tRelType             =NULLTAG;
  tag_t *ptSecondaryObjs =NULL;

  logical lReturn  = false;
  logical lExist   = false;

  iRetCode=GRM_find_relation_type(pszRel,&tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type",BNL_module,EMH_severity_error,iRetCode, true)) goto exit;
  if (tRelType == NULLTAG)
  {
    fprintf(feilog, "Relation type %s is not found.\n", pszRel);
    goto exit;
  }

  iRetCode=GRM_list_secondary_objects_only(tObject,tRelType,&iCount,&ptSecondaryObjs);
  if (BNL_em_error_handler("GRM_list_secondary_objects",BNL_module,EMH_severity_error,iRetCode, true)) goto exit;

  if (iCount >0)
  {
    *tForm = ptSecondaryObjs[0];
  }

exit:

  if (ptSecondaryObjs != NULL) MEM_free(ptSecondaryObjs);
  return iRetCode;
}
