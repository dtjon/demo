/**
 (C) copyright by Siemens PLM Software


 Description    :   FEI Electrical Design Upload Tool

 Created for    :   FEI

 Comments       :

 Filename       :   $Id: FEI_edu_tool.c 462 2016-06-28 09:14:59Z tjonkonj $

 Version info   :   release.pointrelease

 History of changes
 reason /description                                        By      Version     Date
 ----------------------------------------------------------------------------------------------------------
 Creation                                                   JM      0.1         19-01-2007
 Added extra logging and update the check for checkouts     JM      0.2         17-04-2008
 Replace PROP_UIF_ask_property_by_name and added the update
 of the dataset attributes                                  JM      0.3         18-04-2008
 Also check the Item for checkouts and implement the
 possibility to substitute bomlines                         JM      0.4         22-05-2008
 Exit with error when file to be imported does not exist
  and get latest form/dataset using creation_date           JM      0.5         13-08-2008
 Only update the attributes of newly created objects        JM      0.6         08-10-2008
 Ignore error PS_ask_occurrence_note_text                   JM      0.7         30-10-2009
 Added FEI_reset_mdr_attr_value                             JM      0.8         21-03-2012
 Updated FEI_reset_mdr_attr_value                           JM      0.9         14-05-2012
 Update for TC 10.1.*                                       D.Tjon  1.0         01-06-2016

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "time.h"

#ifdef  TC10_1
#include <tc/iman.h>
#include <tccore/imantype.h>
#include <form/form.h>
#include <res/res_itk.h>
#include <tccore/aom_prop.h>
#else
#include <iman.h>
#include <reservation.h>
#include <aom_prop.h>
#include <form.h>
#endif

#include <BNL_fm_module.h>
#include <BNL_xml_module.h>
#include <BNL_ini_module.h>
#include <BNL_em_module.h>
#include <BNL_tb_module.h>
#include <BNL_csv_module.h>
#include <BNL_debug.h>

FILE *fpLog         = NULL;
FILE *fpOutputfile  = NULL;
FILE *fpErrorfile   = NULL;

/* Made this global due to quick fix */
tag_t gtRule        = NULL_TAG;

static char FEI_module[] = "FEI_edu_tool 1.0";

#define FEI_EDU_INI        "FEI_edu_tool.ini"

/* XML element name definitions */
#define XML_ITEMS          "items"
#define XML_ITEM           "item"
#define XML_RELATIONS      "relations"
#define XML_RELATION       "relation"
#define XML_OBJECT         "object"
#define XML_NAMEDREFS      "namedreferences"
#define XML_NAMEDREF       "namedref"
#define XML_OCCURENCES     "occurrences"
#define XML_OCCURENCE      "occurrence"
#define XML_REVISION       "revision"

#define XML_REL_TYPE       "type"
#define XML_TYPE           "type"
#define XML_ID             "id"
#define XML_QUANTITY       "quantity"
#define XML_SEQ            "sequencenum"
#define XML_ATTR           "attribute"
#define XML_FILE           "file"
#define XML_ATTRFORM       "attributeform"
#define XML_CLASS          "class"
#define XML_ATTR           "attribute"
#define XML_ATTR           "attribute"
#define XML_NAME           "name"

#define XML_VAL_REL_BOM    "structure_revisions"

#define INI_PARTSLIST      "Partslist"
#define INI_LOG            "logfile"
#define INI_REVRULE        "revisionrule"
#define INI_TYPES          "types"
#define INI_ITEM_DEFAULT   "default_type"
#define INI_ITEM_CREATE    "create_allowed"
#define INI_ITEM_REVISE    "revise_statusallowed"
#define INI_BOM_INDENTIFY  "bomline_identify"
#define INI_CHECK_NOTE     "checknote"
#define INI_REFDES         "refdes"
#define INI_GENERAL        "General"


//
int FEI_perform_upload(char *pszConfigFile, t_XMLElement *pXMLstruc);
int FEI_check_bomlines(char *pszConfigFile, t_XMLElement *pXMLitem);
int FEI_upload(char *pszConfigFile, t_XMLElement *pXMLitem);
int FEI_update_object(logical lCreated, char *pszConfigFile, tag_t tObject, t_XMLElement *pXMLitem);
int FEI_create_revise_dataset(tag_t tParent, char *pszRelName, char *pszDStype, char *pszDSname, char *pszDSdesc, logical *plCreated, tag_t *ptDataset);
int FEI_create_form(tag_t tParent, char *pszRelName, char *pszClass, char *pszFormtype, char *pszFormname, logical *plCreated, tag_t *ptForm);
int FEI_set_prop_value(tag_t tObject, char *pszAttrName, char *pszAttrValue);
int FEI_update_attributes(logical lUpdateDefaultAttr, char *pszConfigFile, char *pszMainKey, t_XMLElement *pXMLtop, tag_t tObject);
int FEI_update_bomattributes(logical lUpdateDefaultAttr, char *pszConfigFile, tag_t tBvr, char *pszMainKey, t_XMLElement *pXMLtop, tag_t tLine);
int FEI_update_bom(char *pszConfigFile, tag_t tItem, tag_t tRev, t_XMLElement *pXMLrev);
int FEI_find_bvr(tag_t tVwType, tag_t tRev, tag_t *ptBvr);
int FEI_find_bv(tag_t tVwType, tag_t tItem, tag_t *ptBv);
t_XMLElement *FEI_find_firstrelation(t_XMLElement *pXMLtop, char *pszRelName);
char *FEI_get_upload_dir(char *pszConfigFile);
int FEI_object_has_checkouts(tag_t tObject, int *piCheckedOut);
int FEI_get_attr_value(char *pszConfigFile, char *pszMainKey, t_XMLElement *pXMLocc, char *pszProperty, char **pszValue);

time_t BNL_date_to_time(date_t dDate);
int BNL_get_latest_object(tag_t tFirst, tag_t tSecond, tag_t *ptLatest);
int FEI_reset_mdr_attr_value(tag_t tRev);


void welcome()
{
  printf("\nSiemens PLM Software Benelux (c) 2008-2011\n");
  printf("\n");
  printf("%s\n", FEI_module);
  printf("\n");
} /* end of welcome */

void usage()
{
  fprintf(stderr, "\nUsage:\n");
  fprintf(stderr, "FEI_edu_tool [-u=<user> p=<password> -g=group] -f=<inputfile> [-c=<configfile>] [-l=<logfile>] [-b]\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "<inputfile>: The name of input file.\n");
  fprintf(stderr, "<configfile>: The name of the config file, default is [%s].\n", FEI_EDU_INI);
  fprintf(stderr, "<logfile>: The name of log file, This overrules the default from the configuration.\n");
  fprintf(stderr, "\nWhen -b is provided, the bypass will be set.\n");
  fprintf(stderr, "\n");
} /* End of usage */


/* Main */
int ITK_user_main(int argc, char *argv[])
{
  int iRetCode             = 0;

  char *pszInputFile       = NULL;
  char *pszConfigFile      = NULL;
  char *pszLogFile         = NULL;
  char *pszLogFileName     = NULL;
  char *pszBypass          = NULL;

  FILE *fpInputfile        = NULL;

  t_XMLElement *pXMLstruc  = NULL;


  ITK_initialize_text_services(0);

  /* Print welcome message */
  welcome();

  if (ITK_ask_cli_argument("-h") != NULL)
  {
    usage();
    return 0;
  }

  /* Get the arguments */
  pszInputFile = ITK_ask_cli_argument("-f=");
  pszConfigFile = ITK_ask_cli_argument("-c=");
  pszLogFile = ITK_ask_cli_argument("-l=");
  pszBypass = ITK_ask_cli_argument("-b");

  /* Check the given arguments */
  if (pszInputFile == NULL)
  {
    fprintf(stderr, "WARNING: No input file has been supplied.\n");
    usage();
    return 0;
  }

  if (pszConfigFile == NULL)
  {
    pszConfigFile = FEI_EDU_INI;
  }

  /* Open logfile */
  BNL_fm_initialise(stdout);
  BNL_ini_initialise(stdout, NULL);

  if (pszLogFile != NULL)
  {
    if (BNL_fm_open_file(&fpLog, NULL, pszLogFile, "w", 1) != 0)
    {
      fprintf(stderr, "ERROR: Failed to open log file [%s].\n",  pszLogFile);
      iRetCode = 1;
      goto program_exit;
    }
    else
      fprintf(stdout, "INFO: Using log file [%s].\n",  pszLogFile);
  }
  else
  {
    if ((iRetCode = BNL_ini_read_ini_value(pszConfigFile, INI_GENERAL, INI_LOG, &pszLogFileName)) != 0)
      goto program_exit;

    if (BNL_fm_open_file(&fpLog, NULL, pszLogFileName, "w", 1) != 0)
    {
      fprintf(stderr, "ERROR: Failed to open log file [%s].\n", pszLogFileName);
      iRetCode = 1;
      goto program_exit;
    }
    else
      fprintf(stdout, "INFO: Using log file [%s].\n",  pszLogFileName);
  }
  BNL_fm_set_log(fpLog);
  BNL_ini_set_log(fpLog);

  fprintf(fpLog, "************************************************************\n");
	fprintf(fpLog, "\nElectrical Design Upload Tool\n");
  fprintf(fpLog, "%s\n\n", FEI_module);
  fprintf(fpLog, "************************************************************\n\n");

  /* Check for the input file */
  if (BNL_fm_check_file_or_dir(pszInputFile) != 1)
  {
    fprintf(fpLog, "ERROR: Failed to find the input file [%s].\n",  pszInputFile);
    fprintf(stderr, "ERROR: Failed to find the input file [%s].\n",  pszInputFile);
    iRetCode = 1;
    goto program_exit;
  }

  /* Check for config file */
  if (pszConfigFile == NULL)
  {
    pszConfigFile = FEI_EDU_INI;
  }
  if (BNL_fm_check_file_or_dir(pszConfigFile) != 1)
  {
    fprintf(fpLog, "ERROR: Failed to find the configuration file [%s].\n",  pszConfigFile);
    fprintf(stderr, "ERROR: Failed to find configuration input file [%s].\n",  pszConfigFile);
    iRetCode = 1;
    goto program_exit;
  }

  /* Login the user.*/
#if defined(TC8000_3_0) || defined(TC10_1)
  iRetCode = TC_auto_login();
#else
  iRetCode = ITK_auto_login();
#endif
  if (iRetCode != ITK_ok)
  {
    fprintf(fpLog, "ERROR: Login failed with [%d].\n");
    iRetCode = 2;
    goto program_exit;
  }

  /* BYPASS */
  if (pszBypass != NULL)
  {
    iRetCode = ITK_set_bypass(true);
    if (iRetCode != ITK_ok)
    {
      fprintf(fpLog, "ERROR: Set bypass failed with [%d].\n", iRetCode);
      iRetCode = 2;
      goto program_exit;
    }
    fprintf(fpLog, "INFO: BYPASS mode activated.\n");
  }

  /* Initialize the modules */
  BNL_mm_initialise(fpLog);
  BNL_em_initialise(fpLog);
  BNL_fm_initialise(fpLog);
  BNL_tb_initialise(fpLog);
  BNL_xml_initialise(fpLog, NULL);
  fprintf(fpLog, "Modules initialized.\n\n");

  /* Open the input file */
  iRetCode = BNL_xml_load_document(pszInputFile, &pXMLstruc);
  if (iRetCode != ITK_ok)
  {
    fprintf(fpLog, "ERROR: Failed to parse contents of file [%s].\n", pszInputFile);
    iRetCode = 3;
    goto program_exit;
  }

  iRetCode = FEI_perform_upload(pszConfigFile, pXMLstruc);
  if (iRetCode == 0)
  {
    fprintf(fpLog, "Upload done without errors.\n\n");
    fprintf(stderr, "Upload done without errors.\n\n");
  }
  else
  {
    fprintf(fpLog, "Upload done with errors!\n\n");
    fprintf(stderr, "Upload done with errors! Check the logfile for more details.\n\n");
  }


program_exit:

  if (pXMLstruc != NULL) BNL_xml_close_document(pXMLstruc);
  if (pszLogFileName != NULL) free(pszLogFileName);

  /* Logout */
  if (ITK_exit_module(true) != ITK_ok)
  {
    printf("Error logging out...\n");
  }

  /* Close modules */
  BNL_ini_exit();
  BNL_mm_exit();
  BNL_em_exit();
  BNL_tb_exit();
  BNL_fm_exit();

  /* Close the opened files */
  BNL_fm_close_file(fpLog);

  printf("Program closed.\n");

  return iRetCode;
} /* End of main */


int FEI_perform_upload(char *pszConfigFile, t_XMLElement *pXMLstruc)
{
  int iRetCode                    = 0;

  t_XMLElement *pXMLitem          = NULL;
  t_XMLElement *pXMLrev           = NULL;
  t_XMLElement *pXMLpartslist     = NULL;


  BNL_ENTER("EDU", "FEI_perform_upload");

  /* Cycle all the mono items */
  pXMLitem = BNL_xml_find_first(pXMLstruc, XML_ITEM);
  while (pXMLitem != NULL)
  {
    /* Check parts list data */
    pXMLrev = BNL_xml_find_first(pXMLitem, XML_REVISION);
    pXMLpartslist = FEI_find_firstrelation(pXMLrev, XML_VAL_REL_BOM);
    if (pXMLpartslist == NULL)
    {
      fprintf(fpLog, "INFO: **** Working with new item (mono).\n");

      if ((iRetCode = FEI_upload(pszConfigFile, pXMLitem)) != 0) goto upload_exit;

      fprintf(fpLog, "INFO: **** Ended without errors.\n\n");
    }

    /* Get next item */
    pXMLitem = BNL_xml_find_next(pXMLitem, XML_ITEM);
  }

  /* Now process the assemblies */
  pXMLitem = BNL_xml_find_first(pXMLstruc, XML_ITEM);
  while (pXMLitem != NULL)
  {
    /* Check parts list data */
    pXMLrev = BNL_xml_find_first(pXMLitem, XML_REVISION);
    pXMLpartslist = FEI_find_firstrelation(pXMLrev, XML_VAL_REL_BOM);

    if (pXMLpartslist != NULL)
    {
      fprintf(fpLog, "INFO: **** Working with new item (partslist).\n");

      if ((iRetCode = FEI_check_bomlines(pszConfigFile, pXMLitem)) != 0) goto upload_exit;

      if ((iRetCode = FEI_upload(pszConfigFile, pXMLitem)) != 0) goto upload_exit;

      fprintf(fpLog, "INFO: **** Ended without errors.\n\n");
    }

    /* Get next item */
    pXMLitem = BNL_xml_find_next(pXMLitem, XML_ITEM);
  }


upload_exit:

  BNL_LEAVE("EDU", "FEI_perform_upload");

  return iRetCode;
}

// FEI_upl_checkBomLines
int FEI_check_bomlines(char *pszConfigFile, t_XMLElement *pXMLitem)
{
  int iRetCode                 = 0;
  int iNotFound                = 0;
  int iStatusNotOk             = 0;

  tag_t tItem                  = NULL_TAG;
  tag_t tRev                   = NULL_TAG;

  char *pszRevRule             = NULL;

  t_XMLElement *pXMLrev        = NULL;
  t_XMLElement *pXMLpartslist  = NULL;
  t_XMLElement *pXMLoccs       = NULL;
  t_XMLElement *pXMLocc        = NULL;
  t_XMLElement *pXMLcoo        = NULL;
  t_XMLElement *pXMLid         = NULL;

  BNL_ENTER("EDU", "FEI_check_bomlines");

  /* Get start of parts list data */
  pXMLrev = BNL_xml_find_first(pXMLitem, XML_REVISION);
  pXMLpartslist = FEI_find_firstrelation(pXMLrev, XML_VAL_REL_BOM);
  pXMLoccs = BNL_xml_find_first(pXMLpartslist, XML_OCCURENCES);
  pXMLocc = BNL_xml_find_first(pXMLoccs, XML_OCCURENCE);

  if ((iRetCode = BNL_ini_read_ini_value(pszConfigFile, INI_PARTSLIST, INI_REVRULE, &pszRevRule)) != 0)
  {
    fprintf(fpLog, "ERROR: The revision rule has not been defined in settings file.\n");
    iRetCode = 10;
    goto check_bom_exit;
  }
  else
  {
    iRetCode = CFM_find(pszRevRule, &gtRule);
    if (BNL_em_error_handler("CFM_find", FEI_module, EMH_severity_user_error, iRetCode , true)) goto check_bom_exit;

    if (gtRule == NULL_TAG)
    {
      fprintf(fpLog, "ERROR: Did not find revision rule [%s] in database.", pszRevRule);
      iRetCode = 10;
      goto check_bom_exit;
    }
  }

  /* Loop through all BOM lines */
  while (pXMLocc != NULL)
  {
    pXMLid = BNL_xml_find_first(pXMLocc, XML_ID);

    iRetCode = ITEM_find_item(pXMLid->pszElementValue, &tItem);
    if (BNL_em_error_handler("ITEM_find_item", FEI_module, EMH_severity_user_error, iRetCode, true)) goto check_bom_exit;

    if (tItem == NULLTAG)
    {
        fprintf(fpLog, "WARNING: Item [%s] from Parts list not found in TcEng.\n", pXMLid->pszElementValue);
        iNotFound++;
    }
    else
    {
      int iCount            = 0;
      int iTypes            = 0;
      int i                 = 0;
      int iFound            = 0;
      int iStatusCheck      = 0;
      char *pszDummy        = NULL;
      char **pszValues      = NULL;
      char **pszTypes       = NULL;
      char szStatusKey[65]  = {""};
      char szType[IMANTYPE_name_size_c+1] = {""};
      char szStatus[WSO_release_status_size_c + 1] = {""};


      /* Get the ItemRevision using revision rule */
      iRetCode = CFM_item_ask_configured(gtRule, tItem, &tRev, &pszDummy);
      if (BNL_em_error_handler("CFM_item_ask_configured", FEI_module, EMH_severity_user_error, iRetCode , true)) goto check_bom_exit;

      if (pszDummy) MEM_free(pszDummy);

      iRetCode = BNL_tb_determine_object_type(tItem, szType);
      if (BNL_em_error_handler("BNL_tb_determine_object_type", FEI_module, EMH_severity_user_error, iRetCode , true)) goto check_bom_exit;

      /* Does the status needs to be checked */
      if ((iRetCode = BNL_ini_get_multiple_values(pszConfigFile, INI_PARTSLIST, INI_TYPES, ';', &iTypes, &pszTypes)) != 0)
      {
        fprintf(fpLog, "ERROR: Failed to get the list of the types which status must be checked.\n");
        iRetCode = 10;
        goto check_bom_exit;
      }

      for (i = 0; i < iTypes; i++)
      {
        if (strcmp(pszTypes[i], szType) == 0)
        {
          iStatusCheck = 1;
          free(pszTypes[i]);
        }
      }
      if (iTypes > 0) free(pszTypes);

      if (iStatusCheck)
      {
        /* Get the allowed statuses */
        sprintf(szStatusKey, "%s_statusallowed", szType);
        if ((iRetCode = BNL_ini_get_multiple_values(pszConfigFile, INI_PARTSLIST, szStatusKey, ';', &iCount, &pszValues)) != 0)
        {
          fprintf(fpLog, "ERROR: Failed to get the list of allowed statuses for type [%s].\n", szType);
          iRetCode = 10;
          goto check_bom_exit;
        }

        /* Get the status */
        if ((iRetCode = BNL_tb_ask_object_status(tRev, szStatus)) != 0)
        {
          fprintf(fpLog, "ERROR: Failed to get status.\n", szStatus);
          iRetCode = 10;
          goto check_bom_exit;
        }

        /* Check the status */
        for (i = 0; i < iCount; i++)
        {
          if (strcmp(pszValues[i], szStatus) == 0)
          {
            iFound = 1;
            free(pszValues[i]);
          }
        }
        if (iCount > 0) free(pszValues);

        if (!iFound)
        {
          fprintf(fpLog, "WARNING: Item [%s] of type [%s] has an unconfigured status [%s].\n", pXMLid->pszElementValue, szType, szStatus);
          iStatusNotOk++;
        }
      }
    }
    /* get next BOM line */
    pXMLocc = BNL_xml_find_next(pXMLocc, XML_OCCURENCE);
  }

  if (iNotFound > 0)
  {
    fprintf(fpLog, "ERROR: [%d] Item(s) from parts list not found in TCENG. Upload canceled.\n", iNotFound);
    iRetCode = 1;
  }
  if (iStatusNotOk > 0)
  {
    fprintf(fpLog, "ERROR: [%d] Item(s) from parts list did not have the correct status. Upload canceled.\n", iStatusNotOk);
    iRetCode = 2;
  }

check_bom_exit:

  if (pszRevRule != NULL) free(pszRevRule);

  BNL_LEAVE("EDU", "FEI_check_bomlines");

  return iRetCode;
} /* End of FEI_check_bomlines */


t_XMLElement *FEI_find_firstrelation(t_XMLElement *pXMLtop, char *pszRelType)
{
  t_XMLElement *pXMLrels   = NULL;
  t_XMLElement *pXMLrel    = NULL;
  t_XMLElement *pXMLname   = NULL;

  BNL_ENTER("EDU", "FEI_find_firstrelation");

  pXMLrels = BNL_xml_find_first(pXMLtop, XML_RELATIONS);
  pXMLrel  = BNL_xml_find_first(pXMLrels, XML_RELATION);
  while (pXMLrel != NULL)
  {
    pXMLname = BNL_xml_find_first(pXMLrel, XML_REL_TYPE);
    if (strcmp(pXMLname->pszElementValue, pszRelType) == 0)
      return BNL_xml_find_first(pXMLrel, XML_OBJECT);

    pXMLrel = BNL_xml_find_next(pXMLrel, XML_RELATION);
  }

  BNL_LEAVE("EDU", "FEI_find_firstrelation");

  return NULL;
} /* End of FEI_find_firstrelation */

int FEI_upload(char *pszConfigFile, t_XMLElement *pXMLitem)
{
  int iRetCode             = 0;
  int i                    = 0;
  int iRels                = 0;
  int iRevise              = 0;

  logical lItemCreated     = false;
  logical lRevCreated      = false;

  char *pszType            = NULL;

  char szItemType[IMANTYPE_name_size_c+1] = {""};
  char szStatus[WSO_release_status_size_c + 1] = {""};

  tag_t tItem              = NULL_TAG;
  tag_t tRev               = NULL_TAG;

  t_XMLElement *pXMLid     = NULL;
  t_XMLElement *pXMLtype   = NULL;
  t_XMLElement *pXMLrev    = NULL;

  GRM_relation_t *ptRels   = NULL;


  BNL_ENTER("EDU", "FEI_upload");

  pXMLid = BNL_xml_find_first(pXMLitem, XML_ID);
  pXMLtype = BNL_xml_find_first(pXMLitem, XML_TYPE);

  fprintf(fpLog, "INFO: Id [%s] \n", pXMLid->pszElementValue);

  if (pXMLtype == NULL || strlen(pXMLtype->pszElementValue) == 0)
  {
    // Get the type from the ini file
    if ((iRetCode = BNL_ini_read_ini_value(pszConfigFile, INI_GENERAL, INI_ITEM_DEFAULT, &pszType)) != 0)
    {
      fprintf(fpLog, "ERROR: The default item type to use has not been defined in settings file.\n");
      iRetCode = 10;
      goto upload_exit;
    }
    else
    {
      strcpy(szItemType, pszType);
      free(pszType);
    }
  }
  else
  {
    strcpy(szItemType, pXMLtype->pszElementValue);
  }
  //fprintf(fpLog, "INFO: Item type [%s] will be used when not given in the input file.\n", szItemType);

  iRetCode = ITEM_find_item(pXMLid->pszElementValue, &tItem);
  if (BNL_em_error_handler("ITEM_find_item", FEI_module, EMH_severity_user_error, iRetCode, true)) goto upload_exit;

  if (tItem == NULL_TAG)
  {
    int iCreateAllowed = 0;
    char *pszCreate    = NULL;
    char szKey[128]     = { "" };

    sprintf(szKey, "%s_%s", szItemType, INI_ITEM_CREATE);

    /* Check if Item creation is allowed */
    if ((iRetCode = BNL_ini_read_ini_value(pszConfigFile, INI_GENERAL, szKey, &pszCreate)) != 0)
    {
      iCreateAllowed = 0;
    }
    else if (pszCreate[0] == 'Y' || pszCreate[0] == 'y')
    {
      iCreateAllowed = 1;
    }

    if (iCreateAllowed)
    {
      char *pszUser            = NULL;
      char *pszAttrValue       = NULL;

      tag_t tUser              = NULL_TAG;
      tag_t tNewstuff          = NULL_TAG;

      fprintf(fpLog, "INFO: Item with [%s] of type [%s] will be created.\n", pXMLid->pszElementValue, szItemType);

      lItemCreated = true;
      lRevCreated = true;

      /* Create the item and insert in NewStuff folder */
      iRetCode = ITEM_create_item(pXMLid->pszElementValue, pXMLid->pszElementValue, szItemType, NULL, &tItem, &tRev);
      if (BNL_em_error_handler("ITEM_create_item", FEI_module, EMH_severity_user_error, iRetCode, true)) goto upload_exit;

      /* Get User Information */
		  iRetCode = POM_get_user(&pszUser, &tUser);
      if (BNL_em_error_handler("POM_get_user", FEI_module, EMH_severity_user_error, iRetCode, true)) goto upload_exit;

	    if (pszUser != NULL) MEM_free(pszUser);

		  /* Retrieve the newstuff folder for the given user */
		  iRetCode = SA_ask_user_newstuff_folder (tUser, &tNewstuff);
      if (BNL_em_error_handler("SA_ask_user_newstuff_folder", FEI_module, EMH_severity_user_error, iRetCode, true)) goto upload_exit;

  		/* Save the item into the Newstuff folder */
		  iRetCode = FL_insert(tNewstuff, tItem, -999);
		  if (iRetCode != ITK_ok)
		  {
			  if(iRetCode = 6007)
				  iRetCode = ITK_ok;
			  else
			  {
				  BNL_em_error_handler("FL_insert", FEI_module, EMH_severity_user_error, iRetCode, true);
				  goto upload_exit;
			  }
		  }

      if ((iRetCode = BNL_tb_save_and_unlock_object(tNewstuff)) != 0)
        goto upload_exit;
    }
    else
    {
      fprintf(fpLog, "ERROR: Item creation is not allowed for type [%s].\n", szItemType);
      iRetCode = 3;
      goto upload_exit;
    }
  }
  else
  {
    /* Check Item for Checkouts */
    int iCheckedOut              = 0;

    fprintf(fpLog, "INFO: Checking Item for checkouts.\n");

    if ((iRetCode = FEI_object_has_checkouts(tItem, &iCheckedOut)) != 0)
      goto upload_exit;

    if (iCheckedOut)
    {
      fprintf(fpLog, "ERROR: Checkouts detected on Item.\n");
      iRetCode = 4;
      goto upload_exit;
    }
    else
    {
      fprintf(fpLog, "INFO: No checkouts detected on Item.\n");
    }

    /* Get the last revision */
    iRetCode = ITEM_ask_latest_rev(tItem, &tRev);
    if (BNL_em_error_handler( "ITEM_ask_latest_rev", FEI_module, EMH_severity_user_error, iRetCode, true)) goto upload_exit;

    if ((iRetCode = BNL_tb_ask_object_status(tRev, szStatus)) != 0)
      goto upload_exit;

    /* If this revision has a status (not "-") and this status is not allowed
       then revise this revision and remove all datasets */
    if (strcmp(szStatus, "-") == 0)
    {
      iRevise = 0;
      fprintf(fpLog, "INFO: No status, so no need to revise.\n");
    }
    else
    {
      int iReviseCount                  = 0;
      char **pszStatusValues            = NULL;
      char szReviseStatusKey[128]       = { "" };

      iRevise = 1;

      sprintf(szReviseStatusKey, "%s_%s", szItemType, INI_ITEM_REVISE);
      if ((iRetCode = BNL_ini_get_multiple_values(pszConfigFile, INI_GENERAL, szReviseStatusKey, ';', &iReviseCount, &pszStatusValues)) != 0)
      {
        fprintf(fpLog, "ERROR: Failed to get the list of allowed statuses for revise of type [%s].\n", szItemType);
        iRetCode = 10;
        goto upload_exit;
      }

      for (i = 0; i < iReviseCount; i++)
      {
        if (strcmp(szStatus, pszStatusValues[i]) == 0)
        {
          iRevise = 0;
          free(pszStatusValues[i]);
        }
      }
      if (iReviseCount > 0) free(pszStatusValues);

      if (iRevise)
      {
        fprintf(fpLog, "INFO: ItemRevision of type [%s] with status [%s] will be revised.\n", szItemType, szStatus);
      }
      else
      {
        fprintf(fpLog, "INFO: ItemRevision of type [%s] with status [%s] will not be revised.\n", szItemType, szStatus);
      }
    }


    if (iRevise)
    {
      lRevCreated = true;

      /* Revise */
      iRetCode = ITEM_copy_rev(tRev, NULL, &tRev);
      if (BNL_em_error_handler("ITEM_copy_rev", FEI_module, EMH_severity_user_error, iRetCode, true)) goto upload_exit;

      iRetCode = FEI_reset_mdr_attr_value(tRev);
      if (BNL_em_error_handler("FEI_reset_mdr_attr_value", FEI_module, EMH_severity_user_error, iRetCode, true)) goto upload_exit;

      /* Cut all datasets */
      iRetCode = GRM_list_secondary_objects(tRev, NULL_TAG, &iRels, &ptRels);
      if (BNL_em_error_handler("GRM_list_secondary_objects_only", FEI_module, EMH_severity_user_error, iRetCode, true)) goto upload_exit;

      for (i = 0; i < iRels; i++)
      {
        char szClass[IMANTYPE_class_name_size_c + 1] = {""};

        iRetCode = BNL_tb_determine_super_class(ptRels[i].secondary, szClass);
        if (BNL_em_error_handler("BNL_tb_determine_super_class", FEI_module, EMH_severity_user_error, iRetCode , true)) goto upload_exit;

        if (strcmp(szClass, "Dataset") == 0)
        {
          fprintf(fpLog, "INFO: Dataset will be removed.\n");

          iRetCode = GRM_delete_relation(ptRels[i].the_relation);
          if (BNL_em_error_handler("GRM_delete_relation", FEI_module, EMH_severity_user_error, iRetCode , true)) goto upload_exit;
        }
      }
      if ((iRetCode = BNL_tb_save_and_unlock_object(tRev)) != 0)
        goto upload_exit;
    }
    else
    {
      int iCheckedOut              = 0;

      fprintf(fpLog, "INFO: Checking ItemRevision for checkouts.\n");

      if ((iRetCode = FEI_object_has_checkouts(tRev, &iCheckedOut)) != 0)
        goto upload_exit;

      if (iCheckedOut)
      {
        fprintf(fpLog, "ERROR: Checkouts detected on ItemRevision.\n");
        iRetCode = 4;
        goto upload_exit;
      }
      else
      {
        fprintf(fpLog, "INFO: No checkouts detected on ItemRevision.\n");
      }
    }
  }

  /* Now we have Item and ItemRevision */

  // Update Item
  if (lItemCreated)
  {
    fprintf(fpLog, "INFO: Item created, update of default attributes will be performed.\n");
  }
  if ((iRetCode = FEI_update_object(lItemCreated, pszConfigFile, tItem, pXMLitem)) != 0)
    goto upload_exit;

  // Update ItemRevision
  pXMLrev = BNL_xml_find_first(pXMLitem, XML_REVISION);
  if (lRevCreated)
  {
    fprintf(fpLog, "INFO: ItemRevision created, update of default attributes will be performed.\n");
  }
  if ((iRetCode = FEI_update_object(lRevCreated, pszConfigFile, tRev, pXMLrev)) != 0)
    goto upload_exit;

  // Update BOM
  if ((iRetCode = FEI_update_bom(pszConfigFile, tItem, tRev, pXMLrev)) != 0)
    goto upload_exit;

upload_exit:

  if (iRels > 0) MEM_free(ptRels);

  BNL_LEAVE("EDU", "FEI_upload");

  return iRetCode;
}


int FEI_update_object(logical lObjectCreated, char *pszConfigFile, tag_t tObject, t_XMLElement *pXMLitem)
{
  int iRetCode              = 0;

  tag_t tDataset            = NULL_TAG;
  tag_t tForm               = NULL_TAG;

  char szType[IMANTYPE_name_size_c+1]  = {""};
  char szMainKey[65]                   = {""};

  t_XMLElement *pXMLrel         = NULL;
  t_XMLElement *pXMLtype        = NULL;
  t_XMLElement *pXMLobject      = NULL;
  t_XMLElement *pXMLobjectclass = NULL;
  t_XMLElement *pXMLobjecttype  = NULL;
  t_XMLElement *pXMLobjectname  = NULL;
  t_XMLElement *pXMLnr          = NULL;
  t_XMLElement *pXMLnrfile      = NULL;
  t_XMLElement *pXMLnrtype      = NULL;
  t_XMLElement *pXMLattrform    = NULL;


  BNL_ENTER("EDU", "FEI_update_object");

  iRetCode = AOM_refresh(tObject, 1);
  if (BNL_em_error_handler("AOM_refresh", FEI_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  iRetCode = BNL_tb_determine_object_type(tObject, szType);
  if (BNL_em_error_handler("BNL_tb_determine_object_type", FEI_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;

  /* Form the main for getting the default attributes */
  sprintf(szMainKey, "%s_defaultattributes", szType);

  if (lObjectCreated)
  {
    iRetCode = FEI_update_attributes(true, pszConfigFile, szMainKey, pXMLitem, tObject);
  }
  else
  {
    iRetCode = FEI_update_attributes(false, pszConfigFile, szMainKey, pXMLitem, tObject);
  }
  if (iRetCode != 0) goto function_exit;

  /* Update the related objects */
  pXMLrel = BNL_xml_find_first(BNL_xml_find_first(pXMLitem, XML_RELATIONS), XML_RELATION);
  while (pXMLrel != NULL)
  {
    pXMLtype = BNL_xml_find_first(pXMLrel, XML_REL_TYPE);

    if (strcmp(pXMLtype->pszElementValue, XML_VAL_REL_BOM) != 0)
    {
      //fprintf(fpLog, "type: [%s]\n", pXMLtype->pszElementValue);

      pXMLobject = BNL_xml_find_first(pXMLrel, XML_OBJECT);
      // class and type is mandatory
      pXMLobjectclass = BNL_xml_find_first(pXMLobject, XML_CLASS);
      pXMLobjecttype = BNL_xml_find_first(pXMLobject, XML_TYPE);
      pXMLobjectname = BNL_xml_find_first(pXMLobject, XML_NAME);

      //if (pXMLobjectclass == NULL || pXMLobjecttype == NULL || pXMLobjectname == NULL)
      if (pXMLobjectclass == NULL || pXMLobjecttype == NULL)
      {
        //fprintf(fpLog, "WARNING: Not all mandatory fields have been supplied (class, type, name).\n" );
        fprintf(fpLog, "WARNING: Not all mandatory fields have been supplied (class and type).\n" );
      }

      if (strcmp(pXMLobjectclass->pszElementValue, "Form") == 0)
      {
        logical lFormCreated      = false;

        if (pXMLobjectname != NULL)
        {
          iRetCode = FEI_create_form(tObject, pXMLtype->pszElementValue, pXMLobjectclass->pszElementValue, pXMLobjecttype->pszElementValue, pXMLobjectname->pszElementValue, &lFormCreated, &tForm);
        }
        else
        {
          iRetCode = FEI_create_form(tObject, pXMLtype->pszElementValue, pXMLobjectclass->pszElementValue, pXMLobjecttype->pszElementValue, NULL, &lFormCreated, &tForm);
        }
        if (iRetCode != 0) goto function_exit;

        iRetCode = BNL_tb_determine_object_type(tForm, szType);
        if (BNL_em_error_handler("BNL_tb_determine_object_type", FEI_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;

        /* Form the mainkey for getting the default attributes */
        szMainKey[0] = '\0';
        sprintf(szMainKey, "%s_defaultattributes", szType);

        iRetCode = AOM_refresh(tForm, true);
        if (BNL_em_error_handler("AOM_refresh (form 1)", FEI_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;

        /* Now update the form */
        if (lFormCreated)
        {
          fprintf(fpLog, "INFO: Form created, update of default attributes will be performed.\n");
          iRetCode = FEI_update_attributes(true, pszConfigFile, szMainKey, pXMLobject, tForm);
        }
        else
        {
          iRetCode = FEI_update_attributes(false, pszConfigFile, szMainKey, pXMLobject, tForm);
        }
        if (iRetCode != 0) goto function_exit;

        iRetCode = AOM_save(tForm);
        if (BNL_em_error_handler("AOM_save (form)", FEI_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;

        AOM_refresh(tForm, false);
        if (BNL_em_error_handler("AOM_refresh (form 2)", FEI_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;
      }
      else if (strcmp(pXMLobjectclass->pszElementValue, "Dataset") == 0)
      {
        logical lDatasetCreated   = false;

        if (pXMLobjectname != NULL)
        {
          iRetCode = FEI_create_revise_dataset(tObject, pXMLtype->pszElementValue, pXMLobjecttype->pszElementValue, pXMLobjectname->pszElementValue, NULL, &lDatasetCreated, &tDataset);
        }
        else
        {
          iRetCode = FEI_create_revise_dataset(tObject, pXMLtype->pszElementValue, pXMLobjecttype->pszElementValue, NULL, NULL, &lDatasetCreated, &tDataset);
        }
        if (iRetCode != 0) goto function_exit;

        /* Now update the dataset */
        if (lDatasetCreated)
        {
          fprintf(fpLog, "INFO: Dataset created, update of default attributes will be performed.\n");
          iRetCode = FEI_update_attributes(true, pszConfigFile, szMainKey, pXMLobject, tDataset);
        }
        else
        {
          iRetCode = FEI_update_attributes(false, pszConfigFile, szMainKey, pXMLobject, tDataset);
        }
        if (iRetCode != 0) goto function_exit;

        /* Now update the named references */
        pXMLnr = BNL_xml_find_first(BNL_xml_find_first(pXMLobject, XML_NAMEDREFS), XML_NAMEDREF);

        while (pXMLnr != NULL)
        {
          int iRefCount                = 0;
          int iFileType                = 0;
          tag_t tDStype                = NULL_TAG;
          tag_t tFile                  = NULL_TAG;
          tag_t tAtt                   = NULL_TAG;
          char *pszUploadDir           = NULL;
          char **ppszRefTemplates      = NULL;
          char **ppszRefFormats        = NULL;
          char szFileName[MAX_PATH]    = { "" };
          IMF_file_t sFileDescriptor;
          AE_reference_type_t sRefType;


          pXMLnrfile = BNL_xml_find_first(pXMLnr, XML_FILE);
          pXMLnrtype = BNL_xml_find_first(pXMLnr, XML_TYPE);
          pXMLattrform = BNL_xml_find_first(pXMLnr, XML_ATTRFORM);

          if (pXMLattrform != NULL)
          {
            /*
            <namedref>
                <attributeform name="UGPART-ATTR">
                  <attribute name="title">EDI</attribute>
                </attributeform>
            </namedref>
            */
            iRetCode = AE_ask_dataset_named_ref(tDataset, pXMLattrform->pAttributes->pszValue, &sRefType, &tAtt);
            if (BNL_em_error_handler( "AE_ask_dataset_named_ref", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

            if (tAtt == NULLTAG)
            {
              fprintf(fpLog, "WARNING: Attribute Form [%s] not found in dataset\n", pXMLattrform->pAttributes->pszValue);
            }
            else
            {
              iRetCode = BNL_tb_determine_object_type(tAtt, szType);
              if (BNL_em_error_handler("BNL_tb_determine_object_type", FEI_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;

              /* Form the main for getting the default attributes */
              szMainKey[0] = '\0';
              sprintf(szMainKey, "%s_defaultattributes", szType);

              iRetCode = AOM_refresh(tAtt, true);
              if (BNL_em_error_handler("AOM_refresh (att 1)", FEI_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;

              /* Now update the form */
              iRetCode = FEI_update_attributes(false, pszConfigFile, szMainKey, pXMLattrform, tAtt);
              if (iRetCode != 0) goto function_exit;

              iRetCode = AOM_save(tAtt);
              if (BNL_em_error_handler("AOM_save (att)", FEI_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;

              iRetCode = AOM_refresh(tAtt, false);
              if (BNL_em_error_handler("AOM_refresh (att 2)", FEI_module, EMH_severity_user_error, iRetCode , true)) goto function_exit;
            }
          }

          else
          {
            iRetCode = AE_find_datasettype(pXMLobjecttype->pszElementValue, &tDStype);
            if (BNL_em_error_handler("AE_find_datasettype", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

            iRetCode = AE_ask_datasettype_file_refs(tDStype, pXMLnrtype->pszElementValue, &iRefCount, &ppszRefTemplates, &ppszRefFormats);
            if (BNL_em_error_handler("AE_ask_datasettype_file_refs", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

            if (iRefCount == 0)
            {
              fprintf(fpLog, "ERROR: No named references of type [%s] found in datasettype [%s].\n",pXMLnrtype->pszElementValue, pXMLobjecttype->pszElementValue);
              iRetCode = 10;
              goto function_exit;
            }

            if (strcmp(ppszRefFormats[0], TEXT_REF) == 0)
              iFileType = SS_TEXT;
            else if (strcmp( ppszRefFormats[0], BINARY_REF) == 0)
              iFileType = SS_BINARY;
            else
              iFileType = SS_OBJECT;

            MEM_free(ppszRefTemplates);
            MEM_free(ppszRefFormats);

            /* Remove possible named reference with the same refName, if any */
            AE_remove_dataset_named_ref(tDataset, pXMLnrtype->pszElementValue);

            /* Compose file name */
            pszUploadDir = FEI_get_upload_dir(pszConfigFile);
            sprintf(szFileName, "%s%s%s", pszUploadDir, FILESEP, pXMLnrfile->pszElementValue);
            if (pszUploadDir != NULL) free(pszUploadDir);

            /* Check for the file */
            if (!BNL_fm_check_file_or_dir(szFileName))
            {
              fprintf(fpLog, "ERROR: File [%s] does not exist.\n", szFileName);
              iRetCode = 5;
              goto function_exit;
            }
            else
            {
              fprintf(fpLog, "INFO: File [%s] will be imported.\n", szFileName);

              iRetCode = IMF_import_file(szFileName, NULL, iFileType, &tFile, &sFileDescriptor);
              if (BNL_em_error_handler("IMF_import_file", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              iRetCode = IMF_close_file(sFileDescriptor);
              if (BNL_em_error_handler("IMF_close_file", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

              iRetCode = BNL_tb_save_and_unlock_object(tFile);

              /* Add the Named Ref to the dataset */
              iRetCode = AE_add_dataset_named_ref(tDataset, pXMLnrtype->pszElementValue, AE_PART_OF, tFile);
              if (BNL_em_error_handler("AE_add_dataset_named_ref", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;
            }
          }

          /* Get next named reference */
          pXMLnr = BNL_xml_find_next(pXMLnr, XML_NAMEDREF);
        }
        /* Save dataset */
        iRetCode = BNL_tb_save_and_unlock_object(tDataset);
        if (iRetCode != 0) goto function_exit;
      }
      else
      {
        fprintf(fpLog, "WARNING: An unsupported related object has been found [%s]\n", pXMLobjectclass->pszElementValue);
      }
    }

    /* Get next relation */
    pXMLrel = BNL_xml_find_next(pXMLrel, XML_RELATION);
  }

  iRetCode = AOM_save(tObject);
  if (BNL_em_error_handler("AOM_save", FEI_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  iRetCode = AOM_refresh(tObject, 0);
  if (BNL_em_error_handler("AOM_refresh", FEI_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;


function_exit:

  BNL_LEAVE("EDU", "FEI_update_object");

  return iRetCode;
}


int FEI_create_revise_dataset
(
  tag_t tParent,
  char *pszRelName,
  char *pszDStype,
  char *pszDSname,
  char *pszDSdesc,
  logical *plCreated,
  tag_t *ptDataset
)
{
  int iRetCode            = 0;
  int iRefCount           = 0;
  int i                   = 0;

  tag_t tDStype           = NULL_TAG;
  tag_t tDatasettype      = NULL_TAG;
  tag_t tRevAnch          = NULL_TAG;
  tag_t tRelType          = NULL_TAG;
  tag_t tRelCreated       = NULL_TAG;

  tag_t *ptObjects        = NULL;

  char szName[WSO_name_size_c + 1] = { "" };
  char szClassName[IMANTYPE_name_size_c+1] = { "" };

  *ptDataset = NULLTAG;

  if (pszDSname != NULL)
  {
    fprintf(fpLog, "INFO: Checking for dataset of type [%s] with name [%s] attached with relation [%s].\n", pszDStype, pszDSname, pszRelName);
  }
  else
  {
    fprintf(fpLog, "INFO: Checking for dataset of type [%s] attached with relation [%s].\n", pszDStype, pszRelName);
  }

  AE_find_datasettype(pszDStype, &tDStype);
  if (BNL_em_error_handler("AE_find_datasettype", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  iRetCode = GRM_find_relation_type(pszRelName, &tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  /* Find all datasets with given name with the given relation */
  iRetCode = GRM_list_secondary_objects_only(tParent, tRelType, &iRefCount, &ptObjects);
  if (BNL_em_error_handler( "GRM_list_secondary_objects_only", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  for (i = 0; i < iRefCount; i ++)
  {
    /* For each attachment check if this is a dataset */
    iRetCode = BNL_tb_determine_super_class(ptObjects[i], szClassName);
    if (iRetCode != ITK_ok) goto function_exit;

    if (strcmp(szClassName, "Dataset")==0 )
    {
      /* Get the corresponding datasettype_tag */
      iRetCode = AE_ask_dataset_datasettype(ptObjects[i], &tDatasettype);
      if (BNL_em_error_handler("AE_ask_dataset_datasettype", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

      /* Compare the datasettype with the expected */
      if (tDatasettype == tDStype)
      {
        if (pszDSname != NULL)
        {
          iRetCode = WSOM_ask_name(ptObjects[i], szName);
          if (BNL_em_error_handler("WSOM_ask_name", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

          if (strcmp(szName, pszDSname) == 0)
          {
            fprintf(fpLog, "INFO: Dataset with name [%s] already exists.\n", szName);
            if (*ptDataset == NULL_TAG)
            {
              *ptDataset = ptObjects[i];
            }
            else
            {
              BNL_get_latest_object(*ptDataset, ptObjects[i], ptDataset);
            }
          }
        }
        else
        {
          fprintf(fpLog, "INFO: Dataset already exists.\n");
          if (*ptDataset == NULL_TAG)
          {
            *ptDataset = ptObjects[i];
          }
          else
          {
            BNL_get_latest_object(*ptDataset, ptObjects[i], ptDataset);
          }
        }
      }
    }
  } /* End loop for each attachment */

  /* If tag is set: revise else create dataset */
  if (*ptDataset == NULL_TAG)
  {
    fprintf(fpLog, "INFO: Dataset will be created.\n");

    *plCreated = true;

    iRetCode = BNL_tb_create_dataset(pszDSname, pszDSdesc, tDStype, ptDataset);
    if (iRetCode != 0) goto function_exit;

    iRetCode = AOM_save(*ptDataset);
    if (BNL_em_error_handler("AOM_save", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    /* Attach the dataset to the parent */
    iRetCode = GRM_create_relation(tParent, *ptDataset, tRelType, NULL_TAG, &tRelCreated);
    if (BNL_em_error_handler("GRM_create_relation", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

		iRetCode = GRM_save_relation(tRelCreated);
    if (BNL_em_error_handler("GRM_save_relation", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    iRetCode = AOM_refresh(*ptDataset, true);
    if (BNL_em_error_handler("AOM_refresh", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;
  }
  else    /* revise */
  {
    iRetCode = WSOM_ask_name(*ptDataset, szName);
    if (BNL_em_error_handler("WSOM_ask_name", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    fprintf(fpLog, "INFO: Dataset with name [%s] will be revised.\n", szName);

    /*  Create the new version of the dataset. */
    iRetCode = AOM_refresh(*ptDataset, true);
    if (BNL_em_error_handler("AOM_refresh", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

    /* Get rev anchor */
    iRetCode = AE_ask_dataset_anchor(*ptDataset, &tRevAnch);
    if(iRetCode == ITK_ok)
    {
      /* Get rev0 dataset tag = latest revision */
      iRetCode = AE_ask_anchor_revision(tRevAnch, 0, ptDataset);
      if (BNL_em_error_handler("AE_ask_anchor_revision", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;
    }
  }

function_exit:

  if (ptObjects) MEM_free(ptObjects);

  return iRetCode;
}

int FEI_create_form
(
  tag_t tParent,
  char *pszRelName,
  char *pszClass,
  char *pszFormtype,
  char *pszFormname,
  logical *plCreated,
  tag_t *ptForm
)
{
  int iRetCode            = 0;
  int iRefCount           = 0;
  int i                   = 0;

  tag_t tRelType          = NULL_TAG;
  tag_t tRelCreated       = NULL_TAG;

  tag_t *ptObjects        = NULL;

  char szClassName[IMANTYPE_name_size_c+1] = { "" };
  char szType[IMANTYPE_name_size_c+1] = { "" };
  char szName[WSO_name_size_c + 1] = { "" };

  *ptForm = NULLTAG;

  if (pszFormname != NULL)
  {
    fprintf(fpLog, "INFO: Checking for form of type [%s] with name [%s] attached with relation [%s].\n", pszFormtype, pszFormname, pszRelName);
  }
  else
  {
    fprintf(fpLog, "INFO: Checking for form of type [%s] attached with relation [%s].\n", pszFormtype, pszFormname, pszRelName);
  }

  iRetCode = GRM_find_relation_type(pszRelName, &tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type", FEI_module, EMH_severity_error, iRetCode, true)) goto create_form_exit;

  /* Find all forms with given name with the given relation */
  iRetCode = GRM_list_secondary_objects_only(tParent, tRelType, &iRefCount, &ptObjects);
  if (BNL_em_error_handler( "GRM_list_secondary_objects_only", FEI_module, EMH_severity_error, iRetCode, true)) goto create_form_exit;

  for (i = 0; i < iRefCount; i ++)
  {
    /* For each attachment check if this is a dataset */
    iRetCode = BNL_tb_determine_super_class(ptObjects[i], szClassName);
    if (iRetCode != ITK_ok)  goto create_form_exit;

    if (strcmp(szClassName, pszClass)==0 )
    {
      iRetCode = BNL_tb_determine_object_type(ptObjects[i], szType);
      if (iRetCode != ITK_ok)  goto create_form_exit;

      if (strcmp(szType, pszFormtype) == 0)
      {
        if (pszFormname != NULL)
        {
          iRetCode = WSOM_ask_name(ptObjects[i], szName);
          if (BNL_em_error_handler("WSOM_ask_name", FEI_module, EMH_severity_error, iRetCode, true)) goto create_form_exit;

          if (strcmp(szName, pszFormname) == 0)
          {
            fprintf(fpLog, "INFO: Form with name [%s] already exists.\n", pszFormname);
            if (*ptForm == NULL_TAG)
            {
              *ptForm = ptObjects[i];
            }
            else
            {
              BNL_get_latest_object(*ptForm, ptObjects[i], ptForm);
            }
          }
        }
        else
        {
          fprintf(fpLog, "INFO: Form already exists.\n");
          if (*ptForm == NULL_TAG)
          {
            *ptForm = ptObjects[i];
          }
          else
          {
            BNL_get_latest_object(*ptForm, ptObjects[i], ptForm);
          }
        }
      }
    }
  } /* End loop for each attachment */

  /* If not found then create form */
  if (*ptForm != NULL_TAG)
  {
    iRetCode = WSOM_ask_name(*ptForm, szName);
    if (BNL_em_error_handler("WSOM_ask_name", FEI_module, EMH_severity_error, iRetCode, true)) goto create_form_exit;

    fprintf(fpLog, "INFO: Form with name [%s] will be updated.\n", szName);
  }
  else
  {
    fprintf(fpLog, "INFO: Form will be created.\n");

    *plCreated = true;

    iRetCode = FORM_create(pszFormname, NULL, pszFormtype, ptForm);
    if (BNL_em_error_handler("FORM_create", FEI_module, EMH_severity_error, iRetCode, true)) goto create_form_exit;

    iRetCode = AOM_save(*ptForm);
    if (BNL_em_error_handler("AOM_save", FEI_module, EMH_severity_error, iRetCode, true)) goto create_form_exit;

    /* Attach the form to the parent */
    iRetCode = GRM_create_relation(tParent, *ptForm, tRelType, NULL_TAG, &tRelCreated);
    if (BNL_em_error_handler("GRM_create_relation", FEI_module, EMH_severity_error, iRetCode, true)) goto create_form_exit;

		iRetCode = GRM_save_relation(tRelCreated);
    if (BNL_em_error_handler("GRM_save_relation", FEI_module, EMH_severity_error, iRetCode, true)) goto create_form_exit;
  }


create_form_exit:

  if (ptObjects) MEM_free(ptObjects);

  return iRetCode;

}

/**
char    *GetUploadDir(void)

  Description:

    Builds the upload directory. If the UpDir setting
    starts with "/" (UNIX) or the second char = ":" (Windows)
    then the path is treated as absolute, else a absolute path
    is build with the user's home directory as

  Returns:

    The upload directory

*/
char *FEI_get_upload_dir(char *pszConfigFile)
{
  char *pszDir   = NULL;
  char szFullDir[MAX_PATH];

  if (BNL_ini_read_ini_value(pszConfigFile, INI_GENERAL, "uploaddir", &pszDir) != 0)
  {
    pszDir = (char *)malloc(2);
    strcpy(pszDir, ".");
  }

#ifdef WIN32
  if (pszDir[1] == ':')
    return pszDir;
  else
  {
    sprintf(szFullDir, "%s%s%s%s", getenv("HOMEDRIVE"), getenv("HOMEPATH"), FILESEP, pszDir);
  }
#else
  if (pszDir[0] == '/')
    return pszDir;
  else
  {
    sprintf(szFullDir, "%s%s%s", getenv("HOME"), FILESEP, pszDir);
  }
#endif

  free(pszDir);

  pszDir = (char *)malloc( strlen(szFullDir) + 1);
  strcpy(pszDir, szFullDir);

  return pszDir;
}


int FEI_set_prop_value(tag_t tObject, char *pszAttrName, char *pszAttrValue)
{
  int iRetCode          = 0;

  tag_t tAttr           = NULL_TAG;

  logical lMod          = false;


  fprintf(fpLog, "INFO: Setting property [%s] to value [%s].\n", pszAttrName, pszAttrValue);

/*
  iRetCode = PROP_UIF_ask_property_by_name(tObject, pszAttrName, &tAttr);
  if (BNL_em_error_handler("PROP_UIF_ask_property_by_name", FEI_module, EMH_severity_user_error, iRetCode, true)) goto set_prop_exit;
*/

  iRetCode = PROP_ask_property_by_name(tObject, pszAttrName, &tAttr);
  if (BNL_em_error_handler("PROP_ask_property_by_name", FEI_module, EMH_severity_user_error, iRetCode, true)) goto set_prop_exit;

  iRetCode = PROP_ask_property_by_name(tObject, pszAttrName, &tAttr);
  if (BNL_em_error_handler("PROP_ask_property_by_name", FEI_module, EMH_severity_user_error, iRetCode, true)) goto set_prop_exit;

  iRetCode = PROP_is_modifiable(tAttr, &lMod);
  if (BNL_em_error_handler("PROP_is_modifiable", FEI_module, EMH_severity_user_error, iRetCode, true)) goto set_prop_exit;

  if (!lMod)
  {
    fprintf(fpLog, "WARNING: Attribute [%s] not modifiable.\n", pszAttrName);
  }
  else
  {
    if ((strcmp(pszAttrName, "uom_tag") == 0) && (_stricmp(pszAttrValue, "ST") == 0))
    {
      // If UOM=ST then no UOM in TcEng
      fprintf(fpLog, "INFO: Value is of uom is [%s], clearing it.\n", pszAttrValue);
      iRetCode = PROP_UIF_set_value(tAttr, "");
    }
    else
      iRetCode = PROP_UIF_set_value(tAttr, pszAttrValue);

    if (BNL_em_error_handler("PROP_UIF_set_value", FEI_module, EMH_severity_user_error, iRetCode, true)) goto set_prop_exit;
  }

set_prop_exit:

  return iRetCode;
}


int FEI_update_attributes(logical lUpdateDefaultAttr, char *pszConfigFile, char *pszMainKey, t_XMLElement *pXMLtop, tag_t tObject)
{
  int iRetCode        = 0;
  int i               = 0;
  int iCount          = 0;

  t_XMLElement *pXMLattr;

  inivaluepair_t **pValuepairs;


  if (lUpdateDefaultAttr)
  {
    BNL_ini_read_ini_array(pszConfigFile, pszMainKey, &iCount, &pValuepairs);

    for (i = 0; i < iCount; i++)
    {
      int iFound        = 0;

      /* Check if attribute is given in input file */
      pXMLattr = BNL_xml_find_first(pXMLtop, XML_ATTR);
      while (pXMLattr != NULL)
      {
        if (pXMLattr->iAttributes != 0)
        {
          if (strcmp(pXMLattr->pAttributes->pszValue, pValuepairs[i]->pszKey) == 0)
          {
            iFound = 1;
            break;
          }
        }
        /* Get next attribute */
        pXMLattr = BNL_xml_find_next(pXMLattr, XML_ATTR);
      }
      if (iFound == 0)
      {
        fprintf(fpLog, "INFO: Updating default attribute [%s] with value [%s].\n", pValuepairs[i]->pszKey, pValuepairs[i]->pszValue);

        iRetCode = FEI_set_prop_value(tObject, pValuepairs[i]->pszKey, pValuepairs[i]->pszValue);
        if (iRetCode != 0) goto update_attr_exit;
      }
      else
      {
        //fprintf(fpLog, "INFO: Skipping default attribute [%s].\n", pValuepairs[i]->pszKey);
      }
    }
  }

  /* Get the attributes from the input file */
  pXMLattr = BNL_xml_find_first(pXMLtop, XML_ATTR);

  while (pXMLattr != NULL)
  {
    if (pXMLattr->iAttributes != 0)
    {
      fprintf(fpLog, "INFO: Updating attribute from input file [%s] with value [%s].\n", pXMLattr->pAttributes->pszValue, pXMLattr->pszElementValue);

      iRetCode = FEI_set_prop_value(tObject, pXMLattr->pAttributes->pszValue, pXMLattr->pszElementValue);
      if (iRetCode != 0) goto update_attr_exit;
    }
    /* Get next attribute */
    pXMLattr = BNL_xml_find_next(pXMLattr, XML_ATTR);
  }

update_attr_exit:

  for (i = 0; i < iCount; i++)
  {
    if (pValuepairs[i]->pszKey != NULL) free(pValuepairs[i]->pszKey);
    if (pValuepairs[i]->pszValue != NULL) free(pValuepairs[i]->pszValue);
  }
  if (iCount > 0) free(pValuepairs);

  return iRetCode;
}


int FEI_update_bomattributes(logical lUpdateDefaultAttr, char *pszConfigFile, tag_t tBvr, char *pszMainKey, t_XMLElement *pXMLtop, tag_t tLine)
{
  int iRetCode        = 0;
  int i               = 0;
  int iCount          = 0;
  tag_t tNote         = NULL_TAG;

  t_XMLElement *pXMLattr;

  inivaluepair_t **pValuepairs;


  if (lUpdateDefaultAttr)
  {
    BNL_ini_read_ini_array(pszConfigFile, pszMainKey, &iCount, &pValuepairs);

    for (i = 0; i < iCount; i++)
    {
      int iFound        = 0;

      /* Check if attribute is given in input file */
      pXMLattr = BNL_xml_find_first(pXMLtop, XML_ATTR);
      while (pXMLattr != NULL)
      {
        if (pXMLattr->iAttributes != 0)
        {
          if (strcmp(pXMLattr->pAttributes->pszValue, pValuepairs[i]->pszKey) == 0)
          {
            iFound = 1;
            break;
          }
        }
        /* Get next attribute */
        pXMLattr = BNL_xml_find_next(pXMLattr, XML_ATTR);
      }
      if (iFound == 0)
      {
        fprintf(fpLog, "INFO: Updating default bom attribute [%s] with value [%s].\n", pValuepairs[i]->pszKey, pValuepairs[i]->pszValue);

        /* Check if note */
        PS_find_note_type(pValuepairs[i]->pszKey, &tNote);
        if (tNote != NULL_TAG)
        {
          if (strlen(pValuepairs[i]->pszValue) > 0)
          {
            iRetCode = PS_set_occurrence_note_text(tBvr, tLine, tNote, pValuepairs[i]->pszValue);
            if (BNL_em_error_handler("PS_set_occurrence_note_text", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_attr_exit;
          }
          else
          {
            iRetCode = PS_delete_occurrence_note(tBvr, tLine, tNote);
            if (BNL_em_error_handler("PS_delete_occurrence_note", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_attr_exit;
          }
        }
        else
        {
          // This is not working for occurrences
          //iRetCode = FEI_set_prop_value(tLine, pValuepairs[i]->pszKey, pValuepairs[i]->pszValue);
          //if (iRetCode != 0) goto update_attr_exit;
        }
      }
      else
      {
        //fprintf(fpLog, "INFO: Skipping default bom attribute [%s].\n", pValuepairs[i]->pszKey);
      }
    }
  }

  /* Get the attributes from the input file */
  pXMLattr = BNL_xml_find_first(pXMLtop, XML_ATTR);

  while (pXMLattr != NULL)
  {
    if (pXMLattr->iAttributes != 0)
    {
      fprintf(fpLog, "INFO: Updating bom attribute from input file [%s] with value [%s].\n", pXMLattr->pAttributes->pszValue, pXMLattr->pszElementValue);

      /* Check if note */
      PS_find_note_type(pXMLattr->pAttributes->pszValue, &tNote);
      if (tNote != NULL_TAG)
      {
        if (strlen(pXMLattr->pszElementValue) > 0)
        {
          iRetCode = PS_set_occurrence_note_text(tBvr, tLine, tNote, pXMLattr->pszElementValue);
          if (BNL_em_error_handler("PS_set_occurrence_note_text", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_attr_exit;
        }
        else
        {
          iRetCode = PS_delete_occurrence_note(tBvr, tLine, tNote);
          if (BNL_em_error_handler("PS_delete_occurrence_note", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_attr_exit;
        }
      }
      else
      {
        // This is not working for occurrences
        //iRetCode = FEI_set_prop_value(tLine, pXMLattr->pAttributes->pszValue, pXMLattr->pszElementValue);
        //if (iRetCode != 0) goto update_attr_exit;
      }
    }
    /* Get next attribute */
    pXMLattr = BNL_xml_find_next(pXMLattr, XML_ATTR);
  }

update_attr_exit:

  for (i = 0; i < iCount; i++)
  {
    if (pValuepairs[i]->pszKey != NULL) free(pValuepairs[i]->pszKey);
    if (pValuepairs[i]->pszValue != NULL) free(pValuepairs[i]->pszValue);
  }
  if (iCount > 0) free(pValuepairs);

  return iRetCode;
}


// FEI_update_bom
int FEI_update_bom(char *pszConfigFile, tag_t tItem, tag_t tRev, t_XMLElement *pXMLrev)
{
  int iRetCode                 = 0;
  int i                        = 0;
  int j                        = 0;
  int iOccs                    = 0;
  int iCount                   = 0;
  int iIdentifyMode            = 0;

  tag_t tBvr                   = NULL_TAG;
  tag_t tBv                    = NULL_TAG;
  tag_t tVwType                = NULL_TAG;
  tag_t tNoteType              = NULL_TAG;

  tag_t *ptOccs                = NULL;

  char szMainKey[65]           = {""};
  char szKey[256]              = {""};

  char *pszCheckNote           = NULL;
  char *pszCheckNoteValue      = NULL;
  char *pszNote                = NULL;

  char **pszBomProps           = NULL;
  char **pszBomValues          = NULL;

  t_XMLElement *pXMLpartslist  = NULL;
  t_XMLElement *pXMLobjectclass= NULL;
  t_XMLElement *pXMLobjecttype = NULL;
  t_XMLElement *pXMLobjectname = NULL;
  t_XMLElement *pXMLoccs       = NULL;
  t_XMLElement *pXMLocc        = NULL;
  t_XMLElement *pXMLcoo        = NULL;
  t_XMLElement *pXMLid         = NULL;
  t_XMLElement *pXMLquantity   = NULL;
  t_XMLElement *pXMLseq        = NULL;

  BNL_ENTER("EDU", "FEI_update_bom");


  /* Get start of parts list data */
  pXMLpartslist = FEI_find_firstrelation(pXMLrev, XML_VAL_REL_BOM);
  if (pXMLpartslist != NULL)
  {
    BNL_ini_get_multiple_values(pszConfigFile, INI_PARTSLIST, INI_BOM_INDENTIFY, ';', &iCount, &pszBomProps);
    if (iCount == 0)
    {
      fprintf(fpLog, "INFO: BOM will be updated by adding and removing bomlines.\n");
      iIdentifyMode = 0;
    }
    else
      iIdentifyMode = 1;

    /* Get the note and its for indentifying the bomlines to handle */
    if ((iRetCode = BNL_ini_read_ini_value(pszConfigFile, INI_PARTSLIST, INI_CHECK_NOTE, &pszCheckNote)) != 0)
      goto update_bom_exit;
    if ((iRetCode = BNL_ini_read_ini_value(pszConfigFile, INI_PARTSLIST, pszCheckNote, &pszCheckNoteValue)) != 0)
      goto update_bom_exit;

    fprintf(fpLog, "INFO: Checking for note [%s]\n", pszCheckNote);
    iRetCode = PS_find_note_type(pszCheckNote, &tNoteType);
    if (BNL_em_error_handler("PS_find_note_type", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

    // Class and type is mandatory (name will ignored)
    pXMLobjectclass = BNL_xml_find_first(pXMLpartslist, XML_CLASS);
    pXMLobjecttype = BNL_xml_find_first(pXMLpartslist, XML_TYPE);
    //pXMLobjectname = BNL_xml_find_first(pXMLpartslist, XML_NAME);

    //fprintf(fpLog, "class: [%s]\n", pXMLobjectclass->pszElementValue);
    //fprintf(fpLog, "type: [%s]\n", pXMLobjecttype->pszElementValue);
    //fprintf(fpLog, "name: [%s]\n", pXMLobjectname->pszElementValue);

    pXMLoccs = BNL_xml_find_first(pXMLpartslist, XML_OCCURENCES);
    pXMLocc = BNL_xml_find_first(pXMLoccs, XML_OCCURENCE);

    iRetCode = PS_find_view_type(pXMLobjecttype->pszElementValue, &tVwType);
    if (BNL_em_error_handler("PS_find_view_type", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

    /* Form the main for getting the default attributes */
    sprintf(szMainKey, "%s_defaultattributes", pXMLobjecttype->pszElementValue);

    /* Find the bvr */
    iRetCode = FEI_find_bvr(tVwType, tRev, &tBvr);
    if (iRetCode != 0)
      goto update_bom_exit;

    if (tBvr == NULL_TAG)
    {
      /* If no BVR there still may be a BV so first check for BV */
      iRetCode = FEI_find_bv(tVwType, tItem, &tBv);
      if (iRetCode != 0)
        goto update_bom_exit;

      iRetCode = AOM_refresh(tItem, true);
      if (BNL_em_error_handler("AOM_refresh (item 1)", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

      iRetCode = AOM_refresh(tRev, true);
      if (BNL_em_error_handler("AOM_refresh (rev 1)", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

      /* Create a BV if not yet available */
      if (tBv == NULL_TAG)
      {
        iRetCode = PS_create_bom_view(tVwType, NULL, NULL, tItem, &tBv);
        if (BNL_em_error_handler("PS_create_bom_view", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;
      }
      else
      {
        iRetCode = AOM_refresh(tBv, true);
        if (BNL_em_error_handler("AOM_refresh (bv 1)", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;
      }

      /* Create a BVR */
      iRetCode = PS_create_bvr(tBv, NULL, NULL, FALSE, tRev, &tBvr);
      if (BNL_em_error_handler("PS_create_bvr", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

      iRetCode = AOM_save(tBv);
      if (BNL_em_error_handler("AOM_save (bv)", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

      iRetCode = AOM_refresh(tBv, false);
      if (BNL_em_error_handler("AOM_refresh (bv 2)", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

      iRetCode = AOM_save(tItem);
      if (BNL_em_error_handler( "AOM_save (item)", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

      iRetCode = AOM_refresh(tItem, false);
      if (BNL_em_error_handler("AOM_refresh (item 2)", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

      iRetCode = AOM_save(tBvr);
      if (BNL_em_error_handler( "AOM_save (bvr)", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

      iRetCode = AOM_save(tRev);
      if (BNL_em_error_handler("AOM_save (rev)", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

      iRetCode = AOM_refresh(tRev, false);
      if (BNL_em_error_handler("AOM_refresh (rev 2)", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;
    }
    else
    {
      iRetCode = PS_list_occurrences_of_bvr(tBvr, &iOccs, &ptOccs);
      if (BNL_em_error_handler("PS_list_occurrences_of_bvr", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;
    }

    iRetCode = AOM_refresh(tBvr, true);
    if (BNL_em_error_handler("AOM_refresh (bvr 1)", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

    /* Loop through all BOM lines */
    while (pXMLocc != NULL)
    {
      tag_t tOccItem              = NULL_TAG;
      tag_t tOccLine              = NULL_TAG;
      double dQty                 = .0;
      logical lUpdateDefaultAttr  = false;

      pXMLid = BNL_xml_find_first(pXMLocc, XML_ID);
      pXMLquantity = BNL_xml_find_first(pXMLocc, XML_QUANTITY);
      pXMLseq = BNL_xml_find_first(pXMLocc, XML_SEQ);

      if (iIdentifyMode == 1)
      {
        pszBomValues = (char **)malloc(sizeof(char*) * iCount);

        /* Get the required properties */
        for (i = 0; i < iCount; i++)
        {
          /* Reserved */
          if (strcmp(pszBomProps[i], "sequencenum") == 0)
          {
            pszBomValues[i] = (char *)malloc(sizeof(char) * (strlen(pXMLseq->pszElementValue) + 1));
            strcpy(pszBomValues[i], pXMLseq->pszElementValue);
          }
          else
          {
            FEI_get_attr_value(pszConfigFile, szMainKey, pXMLocc, pszBomProps[i], &pszBomValues[i]);
          }

          if (pszBomValues[i] == NULL)
          {
            fprintf(fpLog, "WARNING: Failed to determine the value of the identify property [%s].\n", pszBomProps[i]);
            fprintf(fpLog, "INFO: BOM will be updated by adding and removing bomlines.\n");
            iIdentifyMode = 0;
            break;
          }

          //fprintf(fpLog, "DEBUG: %s: %s\n", pszBomProps[i], pszBomValues[i]);
        }
      }

      //fprintf(fpLog, "ID: %s\n", pXMLid->pszElementValue);
      //fprintf(fpLog, "Quantity: %s\n", pXMLquantity->pszElementValue);
      //fprintf(fpLog, "Seq: %s\n", pXMLseq->pszElementValue);

      /* Find the Item */
      iRetCode = ITEM_find_item(pXMLid->pszElementValue, &tOccItem);
      if (BNL_em_error_handler("ITEM_find_item", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

      /* Check if the bomline already exists */
      for (i = 0; i < iOccs; i++)
      {
        // Check if already been processed
        if (ptOccs[i] != NULL_TAG)
        {
          tag_t tOccObject        = NULL_TAG;
          tag_t tChildBomView     = NULL_TAG;

          logical lPrecise;

          iRetCode = PS_ask_is_bvr_precise(tBvr, &lPrecise);
          if (BNL_em_error_handler("PS_ask_is_bvr_precise", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

          iRetCode = PS_ask_occurrence_child(tBvr, ptOccs[i], &tOccObject, &tChildBomView);
          if (BNL_em_error_handler("PS_ask_occurrence_child", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

          if (lPrecise)
          {
            /* Get the belonging Item */
            iRetCode = ITEM_ask_item_of_rev(tOccObject, &tOccObject);
            if (BNL_em_error_handler("ITEM_ask_item_of_rev", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;
          }

          if (iIdentifyMode == 1)
          {
            int iNoMatch        = 0;

            fprintf(fpLog, "INFO: BOM lines will be identified by the configured properties.\n");

            for (j = 0; j < iCount; j++)
            {
              char *pszCheckValue   = NULL;

              /* Check the bomlines properties */
              if (strcmp(pszBomProps[j], "sequencenum") == 0)
              {
                iRetCode = PS_ask_seq_no(tBvr, ptOccs[i], &pszCheckValue);
                if (BNL_em_error_handler("PS_ask_seq_no", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;
              }
              else
              {
                tag_t tNote           = NULL_TAG;

                PS_find_note_type(pszBomProps[j], &tNote);
                if (tNote != NULL_TAG)
                {
                  PS_ask_occurrence_note_text(tBvr, ptOccs[i], tNote, &pszCheckValue);
                }
              }
              //fprintf(fpLog, "DEBUG: Value of %s: %s\n", pszBomProps[j], pszCheckValue);

              if (pszCheckValue == NULL || strcmp(pszBomValues[j], pszCheckValue) != 0)
              {
                //fprintf(fpLog, "DEBUG: NO MATCH!\n");
                iNoMatch = 1;
              }

              if (pszCheckValue != NULL) MEM_free(pszCheckValue);
            }
            if (!iNoMatch)
            {
              /* This is the one, check if it needs to be substituted */
              //fprintf(fpLog, "DEBUG: MATCH! Check if it needs to be sustituded.\n");

              // Compare Item
              if (tOccItem == tOccObject)
              {
                /* Match */
                fprintf(fpLog, "INFO: Occurrence is already present, no need to susbstitute.\n");
              }
              else
              {
                fprintf(fpLog, "INFO: Occurrence will be susbstituted.\n");

                if (lPrecise)
                {
                  char *pszDummy        = NULL;

                  iRetCode = CFM_item_ask_configured(gtRule, tOccItem, &tOccItem, &pszDummy);
                  if (BNL_em_error_handler("CFM_item_ask_configured", FEI_module, EMH_severity_user_error, iRetCode , true)) goto update_bom_exit;

                  if (pszDummy) MEM_free(pszDummy);
                }

                iRetCode = PS_set_occurrence_child(tBvr, ptOccs[i], tOccItem, tChildBomView);
                if (BNL_em_error_handler("PS_set_occurrence_child", FEI_module, EMH_severity_user_error, iRetCode , true)) goto update_bom_exit;
              }

              /* Reset the occurence tag */
              tOccLine = ptOccs[i];
              ptOccs[i] = NULL_TAG;

              break;
            }
          }
          else
          {
            // Compare Item
            if (tOccItem == tOccObject)
            {
              /* Match */
              //fprintf(fpLog, "FOUND!\n");
              fprintf(fpLog, "INFO: Occurrence is already present.\n");

              /* Reset the occurence tag */
              tOccLine = ptOccs[i];
              ptOccs[i] = NULL_TAG;

              break;
            }
          }
        }
      }

      if (tOccLine == NULL_TAG)
      {
        tag_t *ptOccCreated       = NULL;

        fprintf(fpLog, "INFO: New occurrence will be created.\n");

        lUpdateDefaultAttr = true;
        fprintf(fpLog, "INFO: Bomline created, update of default attributes will be performed.\n");

        /* Create the occurence line */
        iRetCode = PS_create_occurrences(tBvr, tOccItem, NULL_TAG, 1, &ptOccCreated);
        if (BNL_em_error_handler("PS_create_occurrences", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

        tOccLine = ptOccCreated[0];

        MEM_free(ptOccCreated);
      }

      /* Update quantity and sequence number */
      sscanf(pXMLquantity->pszElementValue, "%lf", &dQty);
      iRetCode = PS_set_occurrence_qty(tBvr, tOccLine, dQty);
      if (BNL_em_error_handler("PS_set_occurrence_qty", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

      iRetCode = PS_set_seq_no(tBvr, tOccLine, pXMLseq->pszElementValue );
      if (BNL_em_error_handler("PS_set_seq_no", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

      /* Now update all the give attributes */
      iRetCode = FEI_update_bomattributes(lUpdateDefaultAttr, pszConfigFile, tBvr, szMainKey, pXMLocc, tOccLine);
      if (iRetCode != 0)
        goto update_bom_exit;

      /* get next BOM line */
      pXMLocc = BNL_xml_find_next(pXMLocc, XML_OCCURENCE);
    }

    /* Check the occurrences that have been skipped */
    for (i = 0; i < iOccs; i++)
    {
      // Check if already been processed
      if (ptOccs[i] != NULL_TAG)
      {
        /* Check if it has to be removed */
        PS_ask_occurrence_note_text(tBvr, ptOccs[i], tNoteType, &pszNote);

        /* Only remove the occurrences with note Type CodeOfOrigin=EDI */
        if (pszNote != NULL)
        {
          if (strcmp(pszNote, pszCheckNoteValue) == 0)
          {
            fprintf(fpLog, "INFO: Occurrence will be removed.\n");

            iRetCode = PS_delete_occurrence(tBvr, ptOccs[i]);
            if (BNL_em_error_handler("PS_delete_occurrence", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;
          }
          MEM_free(pszNote);
          pszNote = NULL;
        }
      }
    }

    iRetCode = AOM_save(tBvr);
    if (BNL_em_error_handler("AOM_save", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;

    iRetCode = AOM_refresh(tBvr, false);
    if (BNL_em_error_handler("AOM_refresh (bvr 2)", FEI_module, EMH_severity_user_error, iRetCode, true)) goto update_bom_exit;
  }

update_bom_exit:

  if (iOccs > 0) MEM_free(ptOccs);
  if (pszCheckNote != NULL) free(pszCheckNote);
  if (pszCheckNoteValue != NULL) free(pszCheckNoteValue);

  for (i = 0; i < iCount; i++)
  {
    if (pszBomProps[i] != NULL) free(pszBomProps[i]);
    if (pszBomValues[i] != NULL) free(pszBomValues[i]);
  }
  if (pszBomProps != NULL) free(pszBomProps);
  if (pszBomValues != NULL) free(pszBomValues);

  BNL_LEAVE("EDU", "FEI_update_bom");

  return iRetCode;
} /* End of FEI_update_bom */


int FEI_find_bvr(tag_t tVwType, tag_t tRev, tag_t *ptBvr)
{
  int iRetCode        = 0;
  int iBvrs           = 0;
  int i               = 0;
  tag_t tBv           = NULL_TAG;
  tag_t tBvType       = NULL_TAG;
  tag_t *ptBvrs       = NULL;


  BNL_ENTER("EDU", "FEI_find_bvr");

  *ptBvr = NULL_TAG;

  iRetCode = ITEM_rev_list_bom_view_revs(tRev, &iBvrs, &ptBvrs);
  if (BNL_em_error_handler("ITEM_rev_list_bom_view_revs", FEI_module, EMH_severity_user_error, iRetCode, true)) goto get_bvr_exit;

  if (iBvrs > 0)
  {
    for (i = 0; i < iBvrs; i++)
    {
      iRetCode = PS_ask_bom_view_of_bvr(ptBvrs[i], &tBv);
      if (BNL_em_error_handler("PS_ask_bom_view_of_bvr", FEI_module, EMH_severity_user_error, iRetCode, true)) goto get_bvr_exit;

      iRetCode = PS_ask_bom_view_type(tBv, &tBvType);
      if (BNL_em_error_handler("PS_ask_bom_view_of_bvr", FEI_module, EMH_severity_user_error, iRetCode, true)) goto get_bvr_exit;

      if (tBvType == tVwType)
      {
        *ptBvr = ptBvrs[i];
        break;
      }
    }
  }

get_bvr_exit:

  if (iBvrs > 0) MEM_free(ptBvrs);

  BNL_LEAVE("EDU", "FEI_find_bvr");

  return iRetCode;
}

int FEI_find_bv(tag_t tVwType, tag_t tItem, tag_t *ptBv)
{
  int iRetCode        = 0;
  int iBvs            = 0;
  int i               = 0;
  tag_t tBv           = NULL_TAG;
  tag_t tBvType       = NULL_TAG;
  tag_t *ptBvs        = NULL;


  BNL_ENTER("EDU", "FEI_find_bv");

  *ptBv = NULLTAG;

  iRetCode = ITEM_list_bom_views(tItem, &iBvs, &ptBvs);
  if (BNL_em_error_handler( "ITEM_list_bom_views", FEI_module, EMH_severity_user_error, iRetCode, true)) goto get_bv_exit;

  if (iBvs > 0)
  {
    for (i = 0; i < iBvs; i++)
    {
      iRetCode = PS_ask_bom_view_type( ptBvs[i], &tBvType);
      if (tBvType == tVwType)
      {
        *ptBv = ptBvs[i];
        break;
      }
    }
  }

get_bv_exit:

  if (iBvs > 0) MEM_free(ptBvs);

  BNL_LEAVE("EDU", "FEI_find_bv");

  return iRetCode;
}


int FEI_object_has_checkouts(tag_t tObject, int *piCheckedOut)
{
  int iRetCode           = 0;
  int iRels              = 0;
  int iCount             = 0;
  int i                  = 0;

  char szClass[IMANTYPE_class_name_size_c + 1] = {""};

  GRM_relation_t *ptRels = NULL;

  tag_t tRelType          = NULL_TAG;

  tag_t *ptBoms           = NULL;

  logical lCheckedOut    = false;


  BNL_ENTER("FEI_object_has_checkouts", "FEI_upload");

  iRetCode = RES_is_checked_out(tObject, &lCheckedOut);
  if (BNL_em_error_handler("RES_is_checked_out", FEI_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

  if (lCheckedOut)
  {
    *piCheckedOut = 1;
  }
  else
  {
    /* Check bv/bvr */
    if ((iRetCode = BNL_tb_determine_super_class(tObject, szClass)) != 0)  goto function_exit;

    if (_stricmp(szClass, "Item") == 0)
    {
      iRetCode = ITEM_list_bom_views(tObject, &iCount, &ptBoms);
      if (BNL_em_error_handler("ITEM_list_bom_views", FEI_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;
    }
    else
    {
      iRetCode = ITEM_rev_list_bom_view_revs(tObject, &iCount, &ptBoms);
      if (BNL_em_error_handler("ITEM_rev_list_bom_view_revs", FEI_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;
    }

    for (i = 0; i < iCount && !*piCheckedOut; i++)
    {
      lCheckedOut = false;
      if (RES_is_checked_out(ptBoms[i], &lCheckedOut) == ITK_ok)
      {
        if (lCheckedOut)
          *piCheckedOut = 1;
      }
    }

    if (!lCheckedOut)
    {
      iRetCode = GRM_list_secondary_objects(tObject, NULL_TAG, &iRels, &ptRels);
      if (BNL_em_error_handler("GRM_list_secondary_objects_only", FEI_module, EMH_severity_user_error, iRetCode, true)) goto function_exit;

      for (i = 0; i < iRels && !*piCheckedOut; i++)
      {
        lCheckedOut = false;
        if (RES_is_checked_out(ptRels[i].secondary, &lCheckedOut) == ITK_ok)
        {
          if (lCheckedOut)
            *piCheckedOut = 1;
        }
      }
    }
  }

function_exit:

  if (iRels > 0) MEM_free(ptRels);
  if (iCount > 0) MEM_free(ptBoms);

  BNL_LEAVE("FEI_object_has_checkouts", "FEI_upload");

  return iRetCode;
}


int FEI_get_attr_value(char *pszConfigFile, char *pszMainKey, t_XMLElement *pXMLtop, char *pszProperty, char **pszPropValue)
{
  int iRetCode        = 0;
  int i               = 0;
  int iCount          = 0;
  int iFound          = 0;

  char *pszValue      = NULL;

  t_XMLElement *pXMLattr;

  inivaluepair_t **pValuepairs;


  /* Check if attribute is given in input file */
  pXMLattr = BNL_xml_find_first(pXMLtop, XML_ATTR);
  while (pXMLattr != NULL)
  {
    if (pXMLattr->iAttributes != 0)
    {
      if (strcmp(pXMLattr->pAttributes->pszValue, pszProperty) == 0)
      {
        pszValue = (char *)malloc(sizeof(char) * (strlen(pXMLattr->pszElementValue) + 1));
        strcpy(pszValue, pXMLattr->pszElementValue);
        iFound = 1;
        break;
      }
    }
    /* Get next attribute */
    pXMLattr = BNL_xml_find_next(pXMLattr, XML_ATTR);
  }

  if (iFound == 0)
  {
    BNL_ini_read_ini_array(pszConfigFile, pszMainKey, &iCount, &pValuepairs);

    for (i = 0; i < iCount; i++)
    {
      if (strcmp(pValuepairs[i]->pszKey, pszProperty) == 0)
      {
        pszValue = (char *)malloc(sizeof(char) * (strlen(pValuepairs[i]->pszValue) + 1));
        strcpy(pszValue, pValuepairs[i]->pszValue);
        iFound = 1;
        break;
      }
    }

    for (i = 0; i < iCount; i++)
    {
      if (pValuepairs[i]->pszKey != NULL) free(pValuepairs[i]->pszKey);
      if (pValuepairs[i]->pszValue != NULL) free(pValuepairs[i]->pszValue);
    }
    if (iCount > 0) free(pValuepairs);
  }

  *pszPropValue = pszValue;

  return iRetCode;
}


time_t BNL_date_to_time(date_t dDate)
{
  struct tm tmTime;

  tmTime.tm_year = dDate.year - 1900;
  tmTime.tm_mon  = dDate.month;
  tmTime.tm_mday = dDate.day;
  tmTime.tm_hour = dDate.hour;
  tmTime.tm_min  = dDate.minute;
  tmTime.tm_sec  = dDate.second;

  return mktime(&tmTime);
}

int BNL_get_latest_object(tag_t tFirst, tag_t tSecond, tag_t *ptLatest)
{
  int iRetCode          = 0;

  date_t dFirst;
  date_t dSecond;


  /* Get the creation dates of each object */
  iRetCode = AOM_ask_creation_date(tFirst, &dFirst);
  if (BNL_em_error_handler("AOM_ask_creation_date", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(fpLog, "INFO: Creation date first object: %04d-%02d-%02d %02d:%02d.\n", dFirst.year, dFirst.month, dFirst.day, dFirst.hour, dFirst.minute);

  iRetCode = AOM_ask_creation_date(tSecond, &dSecond);
  if (BNL_em_error_handler("AOM_ask_creation_date", FEI_module, EMH_severity_error, iRetCode, true)) goto function_exit;

  fprintf(fpLog, "INFO: Creation date second object: %04d-%02d-%02d %02d:%02d.\n", dSecond.year, dSecond.month, dSecond.day, dSecond.hour, dSecond.minute);

  if (BNL_date_to_time(dFirst) >= BNL_date_to_time(dSecond))
  {
    fprintf(fpLog, "INFO: First object will be used.\n");
    *ptLatest = tFirst;
  }
  else
  {
    fprintf(fpLog, "INFO: Second object will be used.\n");
    *ptLatest = tSecond;
  }

function_exit:

  return iRetCode;
}


int FEI_reset_mdr_attr_value(tag_t tRev)
{
  int    iRetCode        = 0;
  int    iCount          = 0;

  tag_t tRelType         = NULL_TAG;
  tag_t tIRMFObj         = NULL_TAG;

  tag_t *ptSecondaryObjs = NULL;

  char *pszMDR           = NULL;

  logical lHasBypass     = false;


  fprintf(fpLog, "\nBegin ... FEI_reset_mdr_attr_value\n");

  ITK_ask_bypass(&lHasBypass);

  if (!lHasBypass)
  {
    fprintf(fpLog, "User has no bypass, no need to reset MDR attribute.\n");

    goto exit;
  }

  PREF_ask_char_value("FEI_MDR_MDRProcessed", 0, &pszMDR);
  if (pszMDR == NULL)
  {
    fprintf(fpLog, "Warning, preference [FEI_MDR_MDRProcessed] has not been set.\n");
    goto exit;
  }

  iRetCode = GRM_find_relation_type("IMAN_master_form", &tRelType);
  if (BNL_em_error_handler("GRM_find_relation_type", FEI_module ,EMH_severity_error, iRetCode, true)) goto exit;

  if (tRelType == NULL_TAG)
  {
    fprintf(fpLog, "%s: Relation type [IMAN_master_form] is not found.\n", FEI_module);
    iRetCode = -1;
      goto exit;
  }

  iRetCode = GRM_list_secondary_objects_only(tRev, tRelType, &iCount, &ptSecondaryObjs);
  if (BNL_em_error_handler("GRM_list_secondary_objects", FEI_module, EMH_severity_error, iRetCode, true)) goto exit;

  if (iCount > 0)
  {
    fprintf(fpLog, "%s: Found [%d] object with relation type [IMAN_master_form]. The first one will be used.\n", FEI_module,iCount);
    tIRMFObj = ptSecondaryObjs[0];
  }

  if (tIRMFObj == NULL_TAG)
  {
    fprintf(fpLog, "%s: IRMF not found.\n", FEI_module);
    iRetCode = -1;
      goto exit;
  }
  else
  {
    iRetCode = AOM_refresh(tIRMFObj, true);
    if (BNL_em_error_handler("AOM_refresh", FEI_module, EMH_severity_error, iRetCode, true)) goto exit;

    iRetCode = AOM_set_value_logical(tIRMFObj, pszMDR, false);
    if (BNL_em_error_handler("AOM_set_value_logical", FEI_module, EMH_severity_error, iRetCode, true)) goto exit;

    iRetCode = AOM_save(tIRMFObj);
    if (BNL_em_error_handler("AOM_save", FEI_module, EMH_severity_error, iRetCode, true)) goto exit;
  }

exit:

  fprintf(fpLog, "End ... FEI_reset_mdr_attr_value\n");

  if (ptSecondaryObjs != NULL) MEM_free(ptSecondaryObjs);
  if (pszMDR != NULL) MEM_free(pszMDR);

  return iRetCode;
}/*End of FEI_reset_mdr_attr_value*/
