@echo off
rem program: FEI_MDR_XML.bat
rem
rem This scripts generates XML files
rem It is called by the UPM device fileFEI_MDR_XML.xml
rem
rem ===========================================================================
rem rev	date		name 		description
rem ---------------------------------------------------------------------------
rem 001	27-Dec-2009	GRDR / Siemens PLM	Initial version for FEI Company
rem ===========================================================================

rem -----------------------------
rem setting environment
rem -----------------------------
set FEI_FRU_HOME=%BNL_TOOLS%\FRU
set FEI_FRU_WORK=%FEI_FRU_HOME%\work
set FEI_FRU_STAGE=%FEI_FRU_HOME%\stage

rem UPM scan folders for FEI Production Environment
set FEI_UPM_SCANDIR_BRNO=\\Brno890u\BNL_TOOLS\UPM\scan
set FEI_UPM_SCANDIR_HBO=\\hltuup03\bnl_tools\upm\scan
set FEI_UPM_SCANDIR_ACHT=\\ach5367\BNL_tools\UPM\scan

IF %TCENGPLANT%==ACHT set USER=upm
IF %TCENGPLANT%==ACHT set PASSWRD=upm4acht
IF %TCENGPLANT%==ACHT set GROUP=dba
IF %TCENGPLANT%==BRNO set USER=upm
IF %TCENGPLANT%==BRNO set PASSWRD=upm4brno
IF %TCENGPLANT%==BRNO set GROUP=dba
IF %TCENGPLANT%==HBO set USER=upm
IF %TCENGPLANT%==HBO set PASSWRD=upm
IF %TCENGPLANT%==HBO set GROUP=dba

rem -----------------------------
rem taking arguments from UPM_job
rem ------------------------------
set ITEM_PRIM=%~1
set ITEM_SEC=%~2
set REL_NAME=%~3
set REL_ACTION=%~4
set SEC_OWNING_SITE=%~5
set SI_OWNING_SITE=%~6
set FRU_START_SITE=%~7

set CONFIG=%~8
set REV_ID=%~9

rem -----------------------------
rem debugging info
rem ------------------------------
echo ITEM_PRIM=%ITEM_PRIM%                   >echo.txt
echo ITEM_SEC=%ITEM_SEC%                    >>echo.txt
echo REL_NAME=%REL_NAME%                    >>echo.txt
echo REL_ACTION=%REL_ACTION%                >>echo.txt
echo SEC_OWNING_SITE=%SEC_OWNING_SITE%      >>echo.txt
echo SI_OWNING_SITE=%SI_OWNING_SITE%        >>echo.txt
echo TCENGPLANT=%TCENGPLANT%                >>echo.txt

echo CONFIG=%CONFIG%                        >>echo.txt
echo REV_ID=%REV_ID%                        >>echo.txt

rem set FEI_FUNCTIONAL_EMAIL=wmulders

rem -----------------------------
rem Defining the local site based on TCENGPLANT setting
rem ------------------------------

rem set FRU_START_SITE=%FRU_START_SITE:"=%
rem set TCENGPLANT=%TCENGPLANT:"= %
echo FRU_START_SITE=%FRU_START_SITE%>>echo.txt
echo TCENGPLANT=%TCENGPLANT%>>echo.txt
echo %FRU_START_SITE%==%TCENGPLANT%>>echo.txt


:CheckRelation
IF DEFINED ITEM_PRIM goto :CreateRelation
IF NOT DEFINED ITEM_PRIM goto :DistributeSecondaryItem

:CreateRelation
echo Creating relation...                   >>echo.txt
echo %FEI_FRU_HOME%\bin\BNL_fei_rel.exe upg -item_prim="%ITEM_PRIM%" -item_sec="%ITEM_SEC%" -rel_name="%REL_NAME%" -m="%REL_ACTION%" -bypass>>echo.txt
%FEI_FRU_HOME%\bin\BNL_fei_rel.exe %upg% -item_prim="%ITEM_PRIM%" -item_sec="%ITEM_SEC%" -rel_name="%REL_NAME%" -m="%REL_ACTION%" -bypass
echo relation has been processed >>echo.txt

rem IF /I SEC_OWNING_SITE=="" goto :DistributePrimaryItem
rem echo pipo>>echo.txt
rem goto :CheckPrimaryItem

:CheckStartSite
IF /I "%FRU_START_SITE%" NEQ "%TCENGPLANT%"  (
echo since "%FRU_START_SITE%"=="%TCENGPLANT%" I will go ProcessFRU >>echo.txt
goto :ProcessFRU
)

IF /I "%FRU_START_SITE%" EQU "%TCENGPLANT%"  (
echo since "%FRU_START_SITE%"=="%TCENGPLANT%" I will go :DistributeSecondaryItem >>echo.txt
goto :DistributeSecondaryItem
)

echo since nothing works I go to eof
goto :EOF


:DistributeSecondaryItem
echo Starting to distribute secondary item "%ITEM_SEC%">>echo.txt
echo %TC_BIN%\data_share -f=send -item_id="%ITEM_SEC%" -site="FEIC_BRNO" -latest_working_or_any >>echo.txt
rem IF /I "%SEC_OWNING_SITE%"=="" set SEC_OWNING_SITE="%FRU_START_SITE%"
rem set SEC_OWNING_SITE="%FRU_START_SITE%"
rem IF /I %SEC_OWNING_SITE%=="FEIC_ACHT" %TC_BIN%\data_share %upg% -f=send -item_id="%ITEM_SEC%" -site="FEIC_BRNO" -latest_working_or_any
rem IF /I %SEC_OWNING_SITE%=="FEIC_ACHT" %TC_BIN%\data_share %upg% -f=send -item_id="%ITEM_SEC%" -site="FEIC_HBO" -latest_working_or_any
rem IF /I %SEC_OWNING_SITE%=="FEIC_BRNO" %TC_BIN%\data_share %upg% -f=send -item_id="%ITEM_SEC%" -site="FEIC_ACHT" -latest_working_or_any
rem IF /I %SEC_OWNING_SITE%=="FEIC_BRNO" %TC_BIN%\data_share %upg% -f=send -item_id="%ITEM_SEC%" -site="FEIC_HBO" -latest_working_or_any
rem IF /I %SEC_OWNING_SITE%=="FEIC_HBO" %TC_BIN%\data_share %upg% -f=send -item_id="%ITEM_SEC%" -site="FEIC_ACHT" -latest_working_or_any
rem IF /I %SEC_OWNING_SITE%=="FEIC_HBO" %TC_BIN%\data_share %upg% -f=send -item_id="%ITEM_SEC%" -site="FEIC_BRNO" -latest_working_or_any
echo FRU_START_SITE=%FRU_START_SITE%>>echo.txt
IF /I %FRU_START_SITE%==ACHT %TC_BIN%\data_share -u=%USER% -p=%PASSWRD% -g=%GROUP% -f=send -item_id=%ITEM_SEC% -site="FEIC_BRNO" -latest_working_or_any
IF /I %FRU_START_SITE%==ACHT %TC_BIN%\data_share -u=%USER% -p=%PASSWRD% -g=%GROUP% -f=send -item_id=%ITEM_SEC% -site="FEIC_HBO" -latest_working_or_any
IF /I %FRU_START_SITE%==BRNO %TC_BIN%\data_share -u=%USER% -p=%PASSWRD% -g=%GROUP% -f=send -item_id=%ITEM_SEC% -site="FEIC_ACHT" -latest_working_or_any
IF /I %FRU_START_SITE%==BRNO %TC_BIN%\data_share -u=%USER% -p=%PASSWRD% -g=%GROUP% -f=send -item_id=%ITEM_SEC% -site="FEIC_HBO" -latest_working_or_any
IF /I %FRU_START_SITE%==HBO %TC_BIN%\data_share -u=%USER% -p=%PASSWRD% -g=%GROUP% -f=send -item_id=%ITEM_SEC% -site="FEIC_ACHT" -latest_working_or_any
IF /I %FRU_START_SITE%==HBO %TC_BIN%\data_share -u=%USER% -p=%PASSWRD% -g=%GROUP% -f=send -item_id=%ITEM_SEC% -site="FEIC_BRNO" -latest_working_or_any

rem goto :DistributeCSP
goto :DistributePrimaryItem

:CheckPrimaryItem
echo 1 PRIM_OWNING_SITE="%PRIM_OWNING_SITE%">>echo.txt
IF /I "%PRIM_OWNING_SITE%"=="" goto :DistributePrimaryItem
echo pipo2>>echo.txt

goto :DistributeCSP

:DistributePrimaryItem
echo pipo3>>echo.txt
echo Starting to distribute primary item "%ITEM_PRIM%">>echo.txt
echo %TC_BIN%\data_share -f=send -item_id="%ITEM_PRIM%" -site="FEIC_BRNO" -latest_working_or_any>>echo.txt
IF NOT DEFINED PRIM_OWNING_SITE set PRIM_OWNING_SITE="%FRU_START_SITE%"
echo pipo4>>echo.txt
set PRIM_OWNING_SITE="%FRU_START_SITE%">>echo.txt
echo 2 PRIM_OWNING_SITE="%FRU_START_SITE%">>echo.txt
IF /I %FRU_START_SITE%==ACHT %TC_BIN%\data_share -u=%USER% -p=%PASSWRD% -g=%GROUP% -f=send -item_id="%ITEM_PRIM%" -site="FEIC_BRNO" -latest_working_or_any
IF /I %FRU_START_SITE%==ACHT %TC_BIN%\data_share -u=%USER% -p=%PASSWRD% -g=%GROUP% -f=send -item_id="%ITEM_PRIM%" -site="FEIC_HBO" -latest_working_or_any
IF /I %FRU_START_SITE%==BRNO %TC_BIN%\data_share -u=%USER% -p=%PASSWRD% -g=%GROUP% -f=send -item_id="%ITEM_PRIM%" -site="FEIC_ACHT" -latest_working_or_any
IF /I %FRU_START_SITE%==BRNO %TC_BIN%\data_share -u=%USER% -p=%PASSWRD% -g=%GROUP% -f=send -item_id="%ITEM_PRIM%" -site="FEIC_HBO" -latest_working_or_any
IF /I %FRU_START_SITE%==HBO %TC_BIN%\data_share -u=%USER% -p=%PASSWRD% -g=%GROUP% -f=send -item_id="%ITEM_PRIM%" -site="FEIC_ACHT" -latest_working_or_any
IF /I %FRU_START_SITE%==HBO %TC_BIN%\data_share -u=%USER% -p=%PASSWRD% -g=%GROUP% -f=send -item_id="%ITEM_PRIM%" -site="FEIC_BRNO" -latest_working_or_any

:DistributeCSP
echo Starting to distribute CSPs >>echo.txt
IF NOT EXIST "%FEI_FRU_STAGE%\FEIC_ACHT" mkdir "%FEI_FRU_STAGE%\FEIC_ACHT"
IF NOT EXIST "%FEI_FRU_STAGE%\FEIC_BRNO" mkdir "%FEI_FRU_STAGE%\FEIC_BRNO"
IF NOT EXIST "%FEI_FRU_STAGE%\FEIC_HBO" mkdir "%FEI_FRU_STAGE%\FEIC_HBO"

IF NOT EXIST "%FEI_FRU_STAGE%\FEIC_ACHT" %TC_BIN%\tc_mail_smtp.exe -to=%FEI_FUNCTIONAL_EMAIL% -subject="FRU Staging directory is missing" -server=%FEI_MAIL_SERVER%
IF NOT EXIST "%FEI_FRU_STAGE%\FEIC_BRNO" %TC_BIN%\tc_mail_smtp.exe -to=%FEI_FUNCTIONAL_EMAIL% -subject="FRU Staging directory is missing" -server=%FEI_MAIL_SERVER%
IF NOT EXIST "%FEI_FRU_STAGE%\FEIC_HBO" %TC_BIN%\tc_mail_smtp.exe -to=%FEI_FUNCTIONAL_EMAIL% -subject="FRU Staging directory is missing" -server=%FEI_MAIL_SERVER%

REM replace with call SOA-call
REM IF /I %FRU_START_SITE%==ACHT copy /v /d *.csp "%FEI_FRU_STAGE%\FEIC_BRNO"
REM IF /I %FRU_START_SITE%==ACHT copy /v /d *.csp "%FEI_FRU_STAGE%\FEIC_HBO"
REM IF /I %FRU_START_SITE%==BRNO copy /v /d *.csp "%FEI_FRU_STAGE%\FEIC_ACHT"
REM IF /I %FRU_START_SITE%==BRNO copy /v /d *.csp "%FEI_FRU_STAGE%\FEIC_HBO"
REM IF /I %FRU_START_SITE%==HBO copy /v /d *.csp "%FEI_FRU_STAGE%\FEIC_ACHT"
REM IF /I %FRU_START_SITE%==HBO copy /v /d *.csp "%FEI_FRU_STAGE%\FEIC_BRNO"
rem Possible need to do something with other site
c:\TC101\bnl\dep\create_dp_request.bat "%ITEM_SEC%" %REV_ID% "%CONFIG%" "%ITEM_PRIM%" "%FRU_START_SITE%"

IF NOT EXIST "%FEI_UPM_SCANDIR_ACHT%" %TC_BIN%\tc_mail_smtp.exe -to=%FEI_FUNCTIONAL_EMAIL% -subject="FRU cannot reach UPM SCAN in ACHT" -server=%FEI_MAIL_SERVER%
IF NOT EXIST "%FEI_UPM_SCANDIR_BRNO%" %TC_BIN%\tc_mail_smtp.exe -to=%FEI_FUNCTIONAL_EMAIL% -subject="FRU cannot reach UPM SCAN in BRNO" -server=%FEI_MAIL_SERVER%
IF NOT EXIST "%FEI_UPM_SCANDIR_HBO%" %TC_BIN%\tc_mail_smtp.exe -to=%FEI_FUNCTIONAL_EMAIL% -subject="FRU cannot reach UPM SCAN in HBO" -server=%FEI_MAIL_SERVER%

REM replace with call SOA-call
REM IF EXIST "%FEI_UPM_SCANDIR_ACHT%" move /y "%FEI_FRU_STAGE%\FEIC_ACHT\*.csp" %FEI_UPM_SCANDIR_ACHT%
REM IF EXIST "%FEI_UPM_SCANDIR_BRNO%" move /y "%FEI_FRU_STAGE%\FEIC_BRNO\*.csp" %FEI_UPM_SCANDIR_BRNO%
REM IF EXIST "%FEI_UPM_SCANDIR_HBO%" move /y "%FEI_FRU_STAGE%\FEIC_HBO\*.csp" %FEI_UPM_SCANDIR_HBO%
rem Possible need to do something with other site
c:\TC101\bnl\dep\create_dp_request.bat "%ITEM_SEC%" %REV_ID% "%CONFIG%" "%ITEM_PRIM%" "%FRU_START_SITE%"

IF EXIST "%FEI_FRU_STAGE%\FEIC_ACHT\*.csp" %TC_BIN%\tc_mail_smtp.exe -to=%FEI_FUNCTIONAL_EMAIL% -subject="FRU could not copy CSP to UPM_ACHT" -server=%FEI_MAIL_SERVER%
IF EXIST "%FEI_FRU_STAGE%\FEIC_BRNO\*.csp" %TC_BIN%\tc_mail_smtp.exe -to=%FEI_FUNCTIONAL_EMAIL% -subject="FRU could not copy CSP to UPM_BRNO" -server=%FEI_MAIL_SERVER%
IF EXIST "%FEI_FRU_STAGE%\FEIC_HBO\*.csp" %TC_BIN%\tc_mail_smtp.exe -to=%FEI_FUNCTIONAL_EMAIL% -subject="FRU could not copy CSP to UPM_HBO" -server=%FEI_MAIL_SERVER%



:ProcessFRU
echo Processing fru...                   >>echo.txt
rem %FEI_FRU_HOME%\bin\fei_process_fru.exe -itemid="%ITEM_SEC%" -revid=%REVID% -bomlines=n -bypass=y
echo %FEI_FRU_HOME%\bin\fei_process_fru.exe -u=%USER% -p=*** -g=%GROUP% -itemid=%ITEM_SEC% -bomlines=n -mail_recipient=%MAIL_USER% -bypass=y >>echo.txt
rem %FEI_FRU_HOME%\bin\fei_process_fru.exe -u=%USER% -p=%PASSWRD% -g=%GROUP% -itemid=%ITEM_SEC% -bomlines=n -mail_recipient=%MAIL_USER% -bypass=y
%FEI_FRU_HOME%\bin\fei_process_fru.exe -u=%USER% -p=%PASSWRD% -g=%GROUP% -itemid=%ITEM_SEC% -bomlines=n -bypass=y

:EOF
