/*
$Id: BnlCloneConnector.java 153 2016-05-25 07:34:18Z tjonkonj $

REVISION HISTORY
Version Date        Author           Description
------- ----------- ---------------- -----------------------------------------------------
0.0.0.1 20-jul-2016 D. Tjon Kon Joe  Created
*/
package com.bnl.dep;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
//import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
//import java.util.Hashtable;
//import java.util.Iterator;
//import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;

import com.bnl.tc.soa.BnlSoa;
import com.bnl.tc.soa.client.BnlSession;
//import com.bnl.tc.soa.lib.BnlTools;

import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.strong.core.DataManagementService;
import com.teamcenter.services.strong.core.DispatcherManagementService;
import com.teamcenter.services.strong.core.SessionService;
import com.teamcenter.services.strong.core._2008_06.DispatcherManagement.CreateDispatcherRequestArgs;
import com.teamcenter.services.strong.core._2008_06.DispatcherManagement.CreateDispatcherRequestResponse;
import com.teamcenter.services.strong.core._2008_06.DispatcherManagement.KeyValueArguments;

import com.teamcenter.soa.client.model.ModelObject;
import com.teamcenter.soa.client.model.Property;
//import com.teamcenter.soa.client.model.PropertyDescription;
import com.teamcenter.soa.client.model.strong.BOMLine;
import com.teamcenter.soa.client.model.strong.BOMWindow;
import com.teamcenter.soa.client.model.strong.Dataset;
import com.teamcenter.soa.client.model.strong.Item;
import com.teamcenter.soa.client.model.strong.ItemRevision;
import com.teamcenter.soa.client.model.strong.PSBOMView;
import com.teamcenter.soa.client.model.strong.PSBOMViewRevision;
import com.teamcenter.soa.client.model.strong.Folder;
import com.teamcenter.soa.client.model.strong.PSViewType;
import com.teamcenter.soa.common.ObjectPropertyPolicy;
//import com.teamcenter.soa.exceptions.NotLoadedException;

public class BnlCreateTranslationRequest {

	static String VERSION = "0.0.0.1";
	static Logger logger = Logger.getLogger(BnlCreateTranslationRequest.class.getCanonicalName() + " " + VERSION);
	static DataManagementService dmService = null;
	static BnlSession session = null;
	static BnlDepRequest bnlDepRequest = new BnlDepRequest();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();

		// Welcome message
		System.out.println("");
		System.out.println(logger.getName() + ": (C) 2013-2016 Siemens Industry Software BV");
		System.out.println("Log file: " + ((FileAppender) Logger.getRootLogger().getAppender("SoaAppLogFile")).getFile());
		System.out.println("");

		if (args.length > 0) {
			if (args[0].equals("-help") || args[0].equals("-h")) {
				printHelp();
			}

			// 1. read tool arguments
			for (int i = 0; i < args.length; i++) {
				String arg = args[i];
				if (arg.startsWith("-item_id=")) {
					String itemId = arg.substring(arg.indexOf("=") + 1, arg.length());
					bnlDepRequest.itemTable.put("item_id", itemId);
				} else if (arg.startsWith("-rev_id=")) {
					bnlDepRequest.revId = arg.substring(arg.indexOf("=") + 1, arg.length());
				} else if (arg.startsWith("-config=")) {
					bnlDepRequest.pszIni = arg.substring(arg.indexOf("=") + 1, arg.length());
				} else if (arg.startsWith("-ta1=")) {
					bnlDepRequest.extArg1 = arg.substring(arg.indexOf("=") + 1, arg.length());
				} else if (arg.startsWith("-ta2=")) {
					bnlDepRequest.extArg2 = arg.substring(arg.indexOf("=") + 1, arg.length());
				} else if (arg.startsWith("-ta3=")) {
					bnlDepRequest.extArg3 = arg.substring(arg.indexOf("=") + 1, arg.length());
				} else if (arg.startsWith("-u=")) {
					bnlDepRequest.theUsername = arg.substring(arg.indexOf("=") + 1, arg.length());
				} else if (arg.startsWith("-p=")) {
					bnlDepRequest.thePassword = arg.substring(arg.indexOf("=") + 1, arg.length());
				} else if (arg.startsWith("-g=")) {
					bnlDepRequest.theGroup = arg.substring(arg.indexOf("=") + 1, arg.length());
				} else if (arg.startsWith("-r=")) {
					bnlDepRequest.theRole = arg.substring(arg.indexOf("=") + 1, arg.length());
				} else if (arg.startsWith("-host=")) {
					bnlDepRequest.theHostname = arg.substring(arg.indexOf("=") + 1, arg.length());
				} else if (arg.equals("-bypass")) {
					bnlDepRequest.bypassFlag = true;
				} else if (arg.startsWith("-policy=")) {
					bnlDepRequest.policyFile = arg.substring(arg.indexOf("=") + 1, arg.length());
				} else if (arg.startsWith("-") && arg.indexOf("=") != -1) {
					// TODO add to config file
					// Item key attribute
					String keyAttrName = arg.substring(1, arg.indexOf("="));
					String keyAttrvalue = arg.substring(arg.indexOf("=") + 1, arg.length());

					if (bnlDepRequest.itemTable.get(keyAttrName) == null) {
						logger.info("Adding key attribute [" + keyAttrName + "] with value [" + keyAttrvalue + "].");
						bnlDepRequest.itemTable.put(keyAttrName, keyAttrvalue);
					}
				}
			}

			// 2. read the ini file
			readConfigIniFile(bnlDepRequest.pszIni);

			// 3. validate arguments
			bnlDepRequest.validateArgs();

			try {
				// 4. Connect to Teamcenter
				session = new BnlSession(bnlDepRequest.theHostname, bnlDepRequest.theUsername,
						bnlDepRequest.thePassword, bnlDepRequest.theGroup, bnlDepRequest.theRole,
						bnlDepRequest.bypassFlag);
System.out.println("Connect to Teamcenter");

				// 4. Optional load policyFile
				if (bnlDepRequest.policyFile != null && bnlDepRequest.policyFile.length() > 0) {
					String policyPath = BnlSoa.getBnlTools().replaceEnvironmentVariables(bnlDepRequest.policyFile);

					logger.debug("policy file " + policyPath);
					ObjectPropertyPolicy policy = new ObjectPropertyPolicy();
					policy.load(policyPath);

					SessionService session = SessionService.getService(BnlSession.getConnection());
					session.setObjectPropertyPolicy(policy);

					logger.info("Policy file [" + policyPath + "] has been loaded.");
System.out.println("Policy file [" + policyPath + "] has been loaded.");
				}

				dmService = DataManagementService.getService(BnlSession.getConnection());

			} catch (Exception e) {
				System.out.println("Error during log in");
				logger.error("Error during log in " + e.toString());
				e.printStackTrace();
				System.exit(-1);
			}

			// 5. create_translation_requests for each targets
			// get targets
			bnlDepRequest.BNL_ask_attachments();
			// TODO : pszlistname
			// Loop through the targets and process them
			for (int i = 0; i < bnlDepRequest.ptTargets.length; i++) {
				bnlDepRequest.tCurrentTarget = null;
				bnlDepRequest.tItem = null;
				bnlDepRequest.tItemRev = null;
				bnlDepRequest.tDataset = null;
				bnlDepRequest.tNamedRef = null;
				try {
					process_target(bnlDepRequest.ptTargets[i]);
					// TODO: procesoneline
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.error(e.getMessage());
				}
			}

			// Disconnect from Teamcenter
			if (session != null) {
				session.logout();
				session = null;
			}
		} else // No arguments
		{
			printHelp();
		}

		long endTime = System.currentTimeMillis();
		System.out.println("Duration [" + (endTime - startTime) / 1000 + "] seconds");

		logger.info("End");

		System.out.println("");
		System.out.println("End");
	}

	public static void BNL_create_request(ModelObject tPrimary, ModelObject tSecondary) {
		DispatcherManagementService dispService;
		if (BnlSession.getConnection() != null) {
			dispService = DispatcherManagementService.getService(BnlSession.getConnection());
		} else {
			// throw new Exception("No Teamcenter connection");
			return;
		}

		CreateDispatcherRequestArgs[] arg1 = new CreateDispatcherRequestArgs[1];
		CreateDispatcherRequestArgs zArg = new CreateDispatcherRequestArgs();
		CreateDispatcherRequestResponse zRes;
		KeyValueArguments[] keyArgs = new KeyValueArguments[bnlDepRequest.extraArgKeys.size()];

		ModelObject[] zCreatedReq = null;
		try {
			zArg.priority = bnlDepRequest.iPriority;
			zArg.providerName = bnlDepRequest.pszProvider;
			zArg.serviceName = bnlDepRequest.pszTranslator;

			ModelObject primObjs[] = new ModelObject[1];
			primObjs[0] = tPrimary;
			zArg.primaryObjects = primObjs;

			if (tSecondary != null) {
				ModelObject secObjs[] = new ModelObject[1];
				secObjs[0] = tSecondary;
				zArg.secondaryObjects = secObjs;
			}

			if (bnlDepRequest.extraArgKeys.size() > 0) {
				for (int i = 0; i < bnlDepRequest.extraArgKeys.size(); i++) {
					keyArgs[i] = new KeyValueArguments();
					keyArgs[i].key = bnlDepRequest.extraArgKeys.elementAt(i);
					try {
						keyArgs[i].value = replace_var_list(bnlDepRequest.extraArgValues.elementAt(i));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				zArg.keyValueArgs = keyArgs;
			}

			arg1[0] = zArg;

			zRes = dispService.createDispatcherRequest(arg1);
			zCreatedReq = zRes.requestsCreated;
		} catch (ServiceException e) {
			e.printStackTrace();
			System.out.println("Failed to create test Dispatcher Request");
		}

		if (zCreatedReq == null || zCreatedReq.length == 0) {
			System.out.println("Failed to create test Dispatcher Request");
		}

		// return zCreatedReq;
	}

	public static void process_object(ModelObject obj) {
		String szType = "ItemRevision";
		// iRetCode = WSOM_ask_object_type(tObject, szType);
		BNL_create_request(obj, null);
	}

	public static int process_target(ModelObject tObject) throws Exception {
		String szType = "";
		int iRetCode = 0;

		bnlDepRequest.tCurrentTarget = tObject;
		bnlDepRequest.currentTargetType = BnlSoa.getBnlTools().getPropertyValue(tObject, "object_type");

		if (tObject instanceof Folder) {
			// TODO
		} else if (tObject instanceof Item) {
			bnlDepRequest.tItem = (Item) tObject;
			if (bnlDepRequest.lUseBomView == true) {

				PSBOMView[] ptBomViews = (PSBOMView[]) bnlDepRequest.tItem.get_bom_view_tags();
				for (int b = 0; b < ptBomViews.length; b++) {
					iRetCode = process_target(ptBomViews[b]);
					if (iRetCode == -1)
						return -1;
				}
			} else if (bnlDepRequest.lRecursive == true) {
				ModelObject[] theRevs = ((Item) tObject).get_revision_list();
				for (int i = 0; i < theRevs.length; i++) {
					process_target(theRevs[i]);
				}
			}
		} else if (tObject instanceof ItemRevision) {
			PSBOMViewRevision tBvr = null;
			if (bnlDepRequest.lUseBomView == true && !bnlDepRequest.lIsOccOfBomView) {
				//dmService.getProperties(new ModelObject[] { tObject }, new String[] { "structure_revisions" });
				for (int b = 0; b < ((ItemRevision) tObject).get_structure_revisions().length; b++) {
					PSBOMViewRevision bvrFound = ((ItemRevision) tObject).get_structure_revisions()[b];
					dmService.getProperties(new ModelObject[] { bvrFound }, new String[] { "bom_view" });
					PSBOMView view = bvrFound.get_bom_view();
					dmService.getProperties(new ModelObject[] { view }, new String[] { "view_type" });
					PSViewType vType = bvrFound.get_bom_view().get_view_type();
					dmService.getProperties(new ModelObject[] { vType }, new String[] { "name" });
					if (bvrFound.get_bom_view().get_view_type().get_name().equals(bnlDepRequest.pszBomview)) {
						tBvr = bvrFound;
					}
				}
			} // End of if (if (psDEPdata->lUseBomView &&
				// psDEPdata->lIsOccOfBomView)

			// This determines if there is bvr to handle
			if (bnlDepRequest.lUseBomView && !bnlDepRequest.lIsOccOfBomView && tBvr != null) {
				bnlDepRequest.tItem = BnlSoa.getBnlItem().getItemOfRev((ItemRevision) tObject);
				iRetCode = process_target(tBvr);
				if (iRetCode == -1)
					return -1;
			} else if (bnlDepRequest.lRecursive || bnlDepRequest.lIsOccOfBomView) {
				int iFlag = 0;
				bnlDepRequest.tItemRev = (ItemRevision) tObject;

				szType = BnlSoa.getBnlTools().getPropertyValue(tObject, "object_type");
				logger.info("Object is of type [" + szType + "].");
				if (bnlDepRequest.pszObjectsTypes.contains("-")
						|| bnlDepRequest.pszObjectsTypes.contains(bnlDepRequest.currentTargetType)) {
					logger.info("Object type [" + bnlDepRequest.currentTargetType + "] matches...");
					process_object(tObject);
					iFlag = 1;
				}

				szType = "";

				if (iFlag == 0) {
					// Loop through datasets
					ModelObject attached_objects[] = BnlSoa.getBnlTools().getRelatedObjs(tObject, "");
					logger.info("Found [" + attached_objects.length + "] revision attachments.");

					for (int i = 0; i < attached_objects.length; i++) {
						/* For each attachment */
						String szClassName = attached_objects[i].getTypeObject().getClassName();
						logger.info("[" + i + "] Attachment class [" + szClassName + "]");

						if (szClassName.equals("Dataset")) {
							process_target(attached_objects[i]);
						}

					} /* End of loop for each attachment */
				}
			}
		} else if (tObject instanceof PSBOMView) {
			if (bnlDepRequest.lUseBomView) {
				if (bnlDepRequest.tItem == null) {
					ModelObject revObj = ((PSBOMView) tObject).get_item_revision();
					bnlDepRequest.tItem = BnlSoa.getBnlItem().getItemOfRev((ItemRevision) revObj);
				}

				String szBomType = tObject.getTypeObject().getName();
				if (szBomType.equals(bnlDepRequest.pszBomview)) {
					logger.info("Processing bomview of type [" + szBomType + "].");
					iRetCode = process_bomview(tObject);
					if (iRetCode == -1)
						return -1;
				}

			}
		} else if (tObject instanceof PSBOMViewRevision) {
			if (bnlDepRequest.lUseBomView) {
				if (bnlDepRequest.tItem == null) {
					ModelObject revObj = ((PSBOMView) tObject).get_item_revision();
					bnlDepRequest.tItem = BnlSoa.getBnlItem().getItemOfRev((ItemRevision) revObj);
				}
				String szBomType = ((PSBOMViewRevision)tObject).get_bom_view().get_view_type().get_name();
				if (szBomType.equals(bnlDepRequest.pszBomview)) {
					logger.info("Processing bomview revision of type  [" + szBomType + "].");
					iRetCode = process_bomview(tObject);
					if (iRetCode == -1)
						return -1;
				}
			}
		} else if (tObject instanceof Dataset) {
			/* Loop through named refs elements */
			bnlDepRequest.tDataset = (Dataset) tObject;

			szType = tObject.getTypeObject().getName();
			for (int t = 0; t < bnlDepRequest.pszObjectsTypes.size(); t++) {
				if (bnlDepRequest.pszObjectsTypes.get(t).equals("-")
						|| szType.equals(bnlDepRequest.pszObjectsTypes.get(t))) {
					bnlDepRequest.tItemRev = null;
					ModelObject tItemRevision = BNL_lookup_item_revision(bnlDepRequest.tDataset);
					if (tItemRevision != null) {
						process_object(tItemRevision);
					}
				}

			}

			szType = "";

			/* Get the dataset type of the found dataset */
			String szDsType = bnlDepRequest.tDataset.getTypeObject().getName();
			/* Find the requested named references in this dataset */
			for (int i = 0; i < bnlDepRequest.pszDStypes.size(); i++) {
				/* Check first if datasettype is known to avoid errors */
				if (szDsType.equals(bnlDepRequest.pszDStypes.get(i))) {
					ModelObject refs[] = bnlDepRequest.tDataset.get_ref_list();
					String[] refTypes = bnlDepRequest.tDataset.get_ref_names();

					String szDsName = BnlSoa.getBnlTools().getPropertyValue(bnlDepRequest.tDataset, "object_name");
					for (int j = 0; j < refs.length; j++) {
						/*
						 * check for named refs in this dataset of this
						 * datasettype
						 */
						for (int k = 0; k < bnlDepRequest.pszNamedRefs.size(); k++) {
							if (refTypes[j].equals(bnlDepRequest.pszNamedRefs.get(k))) {
								bnlDepRequest.tNamedRef = refs[j];
								process_namedref(bnlDepRequest.pszNamedRefs.get(k));
							}

						}
					}
					break;
				}
			}

		} else {
			szType = BnlSoa.getBnlTools().getPropertyValue(tObject, "object_type");
			logger.info("Object is of type [" + szType + "].");
			if (bnlDepRequest.pszObjectsTypes.contains("-")
					|| bnlDepRequest.pszObjectsTypes.contains(bnlDepRequest.currentTargetType)) {
				logger.info("Object type [" + bnlDepRequest.currentTargetType + "] matches...");
				process_object(tObject);
			} else {
				logger.info("WARNING: unsupported object [" + bnlDepRequest.currentTargetType + "], rejected.");
			}
		}

		return iRetCode;
	}

	public static int process_bomview(ModelObject tObject) throws Exception {
		PSBOMViewRevision tBomViewRev = null;
		PSBOMView tBomView = null;
		BOMWindow tBomWindow = null;
		int iItemRevCount = 0;
		int iRetCode = 0;
		Vector<ModelObject> ptItemRevsVector = null;
		ModelObject[] ptItemRevs = null;

		BOMLine tTopLine = null;

		String szClass = tObject.getTypeObject().getClassName();

		if (szClass.equals("PSBOMViewRevision")) {
			tBomViewRev = (PSBOMViewRevision) tObject;
			tBomView = tBomViewRev.get_bom_view();
		} else {
			tBomView = (PSBOMView) tObject;
		}

		if (tBomViewRev != null) {
			ItemRevision itemRev = (ItemRevision) tBomViewRev.get_item_revision();
			if (bnlDepRequest.pszRevisionRule != null) {
				tBomWindow = BnlSoa.getBnlBom().createBOMWindow(itemRev, tBomView, bnlDepRequest.pszRevisionRule);
			} else {
				tBomWindow = BnlSoa.getBnlBom().createBOMWindow(itemRev, tBomView, "");

			}
			// TODO : set the topline precise
			/*
			 * BOMLine bl = BnlSoa.getBnlBom().getTopLine(tBomWindow); bl.
			 * tBomViewRev.get_is_precise()
			 */

			tTopLine = BnlSoa.getBnlBom().getTopLine(tBomWindow);
		} else {
			ItemRevision itemRev = (ItemRevision) tBomView.get_item_revision();
			Item item = BnlSoa.getBnlItem().getItemOfRev(itemRev);
			if (bnlDepRequest.pszRevisionRule != null) {
				tBomWindow = BnlSoa.getBnlBom().createBOMWindow(item, tBomView, bnlDepRequest.pszRevisionRule);
			} else {
				tBomWindow = BnlSoa.getBnlBom().createBOMWindow(item, tBomView, "");

			}

			tTopLine = BnlSoa.getBnlBom().getTopLine(tBomWindow);
		}

		// 1=true, 0=false
		if (bnlDepRequest.iExcludeNote != 0)// false
		{
			/* Get relevant item revs from bom */
			/*
			 * We fool the BNL_tb_bom_get_item_revs by setting startlevel on 1
			 * to force the top level of the bom to be added. Therefore we need
			 * to increment the maxlevel too (if not -1)
			 */
			if (bnlDepRequest.iBomLevel != -1)
				bnlDepRequest.iBomLevel++;

			ptItemRevs = null;
			ModelObject revTmp = null;
			if (bnlDepRequest.lIgnoreUnconfigBom) {
				// TODO: expand to bepaald level like 2
				ptItemRevs = BnlSoa.getBnlBom().expandPSOneLevelItemRevs(revTmp, tBomWindow, null, "false");
			} else {
				// TODO: expand to bepaald level like 2
				ptItemRevs = BnlSoa.getBnlBom().expandPSOneLevelItemRevs(revTmp, tBomWindow, null, "true");
			}

			/* Loop through item revisions */
			bnlDepRequest.lIsOccOfBomView = true;
			for (int i = 0; i < ptItemRevs.length; i++)
				process_target(ptItemRevs[i]);

			bnlDepRequest.lIsOccOfBomView = false;
		} else {
			logger.info("Filter on note type [" + bnlDepRequest.pszNoteType + "] with value [" + bnlDepRequest.pszNote
					+ "].");

			ModelObject revTmp = null;
			ModelObject[] ptOccs = BnlSoa.getBnlBom().expandPSAllLevel(revTmp, tBomWindow, null, null);

			if (bnlDepRequest.iBomLevel != -1)
				bnlDepRequest.iBomLevel++;
			ptItemRevs = null;

			iRetCode = BNL_get_non_note_excluded_revs(tTopLine, 1, ptItemRevsVector);
			if (iRetCode == -1) {
				logger.error("There where unconfigured occurences, no plotrequest created.");
				return iRetCode;
			} else if (iRetCode != 0) {
				return iRetCode;
			}

			/*
			 * Filter out doubles something stupid because counter did not work
			 * in BNL_get_non_note_excluded_revs function so filter doubles
			 * here.
			 */

			ptItemRevs = new ModelObject[ptItemRevsVector.size()];

			for (int d = 0; d < ptItemRevsVector.size(); d++) {
				for (int t = (d + 1); t < ptItemRevsVector.size(); t++) {
					if (ptItemRevsVector.get(d) != null) {
						if (ptItemRevsVector.get(d) == ptItemRevsVector.get(t)) {
							ptItemRevs[t] = null;
						} else {
							ptItemRevs[t] = ptItemRevsVector.get(t);
						}
					}
				}
			}
			/* Loop through item revisions */
			bnlDepRequest.lIsOccOfBomView = true;
			for (int i = 0; i < ptItemRevs.length; i++) {
				if (ptItemRevs[i] != null)
					process_target(ptItemRevs[i]);
			}
			bnlDepRequest.lIsOccOfBomView = false;
		}

		BnlSoa.getBnlBom().closeBOMWindow(tBomWindow);

		return 0;
	}

	public static void readConfigIniFile(String inifile) {
		if (inifile.length() == 0) {
			System.out.println("-config= argument is required");
			System.exit(-1);
		}

		try {
			BufferedReader inifileReader = new BufferedReader(new FileReader(inifile));
			BnlIniConfigFile iniConfig = new BnlIniConfigFile(inifileReader);
			// iniConfig.readConfigIniFile();
		} catch (FileNotFoundException fe) {
			System.out.println("Given ini file " + inifile + "does not exits");
			logger.error("Given ini file " + inifile + "does not exits, exiting");
			fe.printStackTrace();
			System.exit(-1);
		}
	}

	private static void printHelp() {
		System.out.println("Usage: java com.bnl.dep.BnnCreateTranslationRequest ");
		System.out.println("                 [-item_id = Item Id]");
		System.out.println("                 [-rev_id = Revision Id]");
		System.out.println("                 [-item multi field attribute(s) = values(s)]");
		System.out.println("                 -config = path to ini file");
		System.out.println("                 [-u = TC user id]");
		System.out.println("                 [-p = TC password]");
		System.out.println("                 [-g = TC group ]");
		System.out.println("                 [-r = TC role]");
		System.out.println("                 [-bypass]");
		System.out.println("                 [-host = connection host name]");
		System.out.println("                 [-ta1 = extra argument for translation requets]");
		System.out.println("                 [-ta2 = extra argument for translation requets]");
		System.out.println("                 [-ta3 = extra argument for translation requets]");
		System.out.println("");
		System.out.println("Example:");
		System.out.println(
				"java com.bnl.dep.BnnCreateTranslationRequest -item_id=000087 -rev_id=A -config=c:\\dep\\dep.ini -ta1=\"site=eindhoven\" ");
		System.exit(0);
	}

	public static String replace_var_list(String pszVarList) throws Exception {
		String pszStart = pszVarList;
		/*
		 * TODO
		 *
		 * while ((pszDel = strchr(pszStart, '|')) != NULL) { pszDel = '\0';
		 * //get the value, if any iRetCode = replace_var(psDEPdata, pszStart,
		 * pszValue); if (iRetCode == 0 && *pszValue != NULL) return iRetCode;
		 *
		 * pszDel = '|'; pszStart = pszDel + 1; }
		 */

		/* get the last value */
		return replace_var(pszStart);
	}

	public static String replace_var(String argVal) throws Exception {
		String translatedVal = "";
		String dateFormat = "";
		String tempArgVal = argVal;
		if (tempArgVal.contains(";")) {
			// date format is given
			dateFormat = tempArgVal.substring(tempArgVal.indexOf(";") + 1, tempArgVal.length());
			tempArgVal = tempArgVal.substring(0, tempArgVal.indexOf(";"));
		}
		String[] tempVals = tempArgVal.split(":");
		if (tempVals.length == 2) {
			if (tempVals[0].equalsIgnoreCase("nr")) {
				boolean lfound = false;
				ModelObject tObj = null;

				/*
				 * get the named reference object by given nr-->
				 * nr(PDF_reference)
				 */
				int index1 = tempVals[0].indexOf('(') + 1;
				int index2 = tempVals[0].indexOf(')');
				String pRef = tempVals[0].substring(index1, index2);

				/* now we have the namedref name */
				logger.info("Getting object of namedref [" + pRef + "]");

				/* if there is a namedref there should be a dataset tag. */
				if (bnlDepRequest.tDataset != null) {
					/*
					 * Get the named refrences from the dataset and select the
					 * one out needed
					 */
					/* done because the ITK documentation says to do it */
					String[] refTypes = bnlDepRequest.tDataset.get_ref_names();
					ModelObject[] refList = bnlDepRequest.tDataset.get_ref_list();
					for (int i = 0; i < refTypes.length && lfound == false; i++) {
						if (refTypes[i].equals(pRef)) {
							lfound = true;
							tObj = refList[i];
						}
					}

					if (lfound == false) {
						logger.info("Named reference [" + pRef + "] not found in dataset");
						return "";
					}

					/*
					 * the tObj can be a form first try to get the value in the
					 * form. If not found then try to get it from the object
					 * itself
					 */

					String szClass = tObj.getTypeObject().getClassName();

					if (szClass.equals("Form")) {
						logger.info("Asking form value [" + tempVals[1] + "].");
						translatedVal = BnlSoa.getBnlTools().getPropertyValue(tObj, tempVals[1]);
					}

					if (translatedVal == null) {
						/* then try to get the property */
						if (tempVals[1].equalsIgnoreCase("puid")) {
							logger.debug("Retrieving uid.");
							translatedVal = tObj.getUid();
						} else {
							translatedVal = get_prop_date(tObj, tempVals[1], dateFormat);
						}
					}
				} else {
					logger.info("%s: Cant get he the forced base object of the dataset.");
					return "";
				}
			} else if (tempVals[0].equalsIgnoreCase("ds")) {
				if (bnlDepRequest.tDataset != null) {
					if (tempVals[1].equalsIgnoreCase("puid")) {
						logger.debug("Retrieving uid.");
						translatedVal = bnlDepRequest.tDataset.getUid();
					} else  {
						translatedVal = get_prop_date(bnlDepRequest.tDataset, tempVals[1], dateFormat);
					}
				} else {
					logger.debug("input object is not a dataset, skipping value of " + argVal);
				}
			} else if (tempVals[0].equalsIgnoreCase("target")) {
				// TODO
			} else if (tempVals[0].equalsIgnoreCase("rv")) {
				if (bnlDepRequest.tItemRev != null) {
					if (tempVals[1].equalsIgnoreCase("puid")) {
						logger.debug("Retrieving uid.");
						translatedVal = bnlDepRequest.tItemRev.getUid();
					} else {
						translatedVal = get_prop_date(bnlDepRequest.tItemRev, tempVals[1], dateFormat);
					}
				} else {
					logger.debug("input object is not a ItemRevision, skipping value of " + argVal);
				}
			} else if (tempVals[0].equalsIgnoreCase("it")) {
				if (bnlDepRequest.tItem != null) {
					if (tempVals[1].equalsIgnoreCase("puid")) {
						logger.debug("Retrieving uid.");
						translatedVal = bnlDepRequest.tItem.getUid();
					} else {
						translatedVal = get_prop_date(bnlDepRequest.tItemRev, tempVals[1], dateFormat);
					}
				} else {
					logger.debug("input object is not a Item, skipping value of " + argVal);
				}
			} else if (tempVals[0].equalsIgnoreCase("irm")) {
				translatedVal = get_master_form_value(tempVals[1]);
			} else if (tempVals[0].equalsIgnoreCase("job")) {
				// TODO
				// ook met date
			} else if (tempVals[0].equalsIgnoreCase("task")) {
				// TODO
				// ook met date
			} else if (tempVals[0].equalsIgnoreCase("roottask")) {
				// TODO
				// ook met date

			} else if (tempVals[0].equalsIgnoreCase("fixed")) {
				translatedVal = BnlSoa.getBnlTools().replaceEnvironmentVariables(tempVals[1]);
			} else if (tempVals[0].equalsIgnoreCase("pref")) {
				translatedVal = BnlSoa.getBnlTools().getPreferenceValue(tempVals[1], "site");
			} else {
				// Use value as-is
				translatedVal = tempArgVal;
			}
		}

		if (translatedVal == null)
			translatedVal = "";

		if (translatedVal != null && translatedVal.length() == 0)
			translatedVal = tempArgVal;

		return translatedVal;
	}

	public static ModelObject BNL_lookup_item_revision(ModelObject tDataset) throws Exception {
		int iRetCode = 0;
		int i = 0;
		int j = 0;
		int iCount = 0;

		String szTargetClass = "";

		ModelObject[] ptPrimaryList = null;

		ModelObject ptRev = null;

		logger.info("Checking if primary ItemRevision is also target.\n");

		ptPrimaryList = BnlSoa.getBnlTools().getPrimaryRelatedObjs(tDataset, "");

		for (i = 0; i < ptPrimaryList.length; i++) {
			// Check if ItemRevision
			szTargetClass = ptPrimaryList[i].getTypeObject().getClassName();

			if (szTargetClass.equals("ItemRevision")) {
				// Check if target
				for (j = 0; j < bnlDepRequest.ptTargets.length; j++) {
					if (ptPrimaryList[i] == bnlDepRequest.ptTargets[j]) {
						ptRev = ptPrimaryList[i];
						logger.info("Found!.");
						return ptRev;
					}
				}
			}
		}

		logger.info("No matching ItemRevision has been found, using property item_revision");

		Property prop = tDataset.getPropertyObject("item_revision");
		if (prop != null) {
			ptRev = prop.getModelObjectValue();
		}

		return ptRev;
	}

	public static void process_namedref(String pszNamedRef) throws Exception {
		/*
		 * If the Item Revision Tag is not yet available, try to extract it from
		 * the properties of the dataset
		 */
		if (bnlDepRequest.tItemRev == null) {
			Property prop = bnlDepRequest.tDataset.getPropertyObject("item_revision");
			if (prop != null) {
				bnlDepRequest.tItemRev = (ItemRevision) prop.getModelObjectValue();
			}
		}
		/*
		 * If no reference to the owning ItemRevision is stored in the
		 * properties of the Dataset, skip this named reference
		 */
		if (bnlDepRequest.tItemRev == null) {
			logger.info(
					"Error, Dataset has no reference to the owning Item Revision. This named reference is skipped.");
			return;
		}

		BNL_create_request(bnlDepRequest.tDataset, bnlDepRequest.tItemRev);
	}

	public static String get_master_form_value(String pszVar) throws Exception {
		String pVal = "";
		ModelObject[] ptRels = BnlSoa.getBnlTools().getRelatedObjs(bnlDepRequest.tItemRev, "IMAN_master_form");
		if (ptRels.length > 0) /* only one Master Form should be found */
		{
			Property prop = ptRels[0].getPropertyObject(pszVar);
			if (prop != null) {
				pVal = prop.getDisplayableValue();
				logger.info("read formvalue [" + pszVar + "]=[" + pVal + "].");
			} else {
				logger.info("Could not get value [" + pszVar + "] from Master Form");
			}

		}
		return pVal;
	}

	public static String get_prop_date(ModelObject tObj, String propname, String dateFormat) throws Exception {
		String translatedVal = "";
		if (dateFormat.length() > 0) {
			Property prop = tObj.getPropertyObject(propname);
			if (prop != null) {
				Calendar cal = prop.getCalendarValue();
				Date dateTime = cal.getTime();
				SimpleDateFormat dt = new SimpleDateFormat(dateFormat);
				translatedVal = dt.format(dateTime);
			}
		} else {
			translatedVal = BnlSoa.getBnlTools().getPropertyValue(bnlDepRequest.tItem, propname);
		}

		return translatedVal;
	}

	public static int BNL_get_non_note_excluded_revs(BOMLine tBomLine, int iLevel, Vector<ModelObject> ptRevs)
			throws Exception {
		String pszOccValue = null;
		ModelObject tItemRev = null;
		int iSuppressed = 0;
		int iRetCode = 0;

		ModelObject[] tItemRevs;

		if (iLevel > 0) {
			pszOccValue = BnlSoa.getBnlTools().getPropertyValue(tBomLine, bnlDepRequest.pszNoteType);

			Property prop = tBomLine.getPropertyObject("item_revision");
			if (prop != null) {
				tItemRev = prop.getModelObjectValue();
			}

			if (tItemRev != null) {
				/* Check if it is supressed */
				if (pszOccValue.equals(bnlDepRequest.pszNote))
					iSuppressed = 1;

				// 0=true, 1=false
				if (iSuppressed != 0) {
					/*
					 * Check for doubles, did it here but for some reason it
					 * will not pick up the counter
					 */
					ptRevs.add(tItemRev);
				}
			} else {
				/*
				 * This happens when the revision rule is set and there are
				 * occurence wich not which is not a allowed occurence the (???)
				 * occurence. EN quik and dirty implementation 21-05-2001 for
				 * Philips EMT
				 */

				logger.info(" Unconfigured occurence found.");
				return -1;
			}
		} else {
			tItemRevs = null;
		}

		if ((iLevel < bnlDepRequest.iBomLevel || bnlDepRequest.iBomLevel == -1) && iSuppressed == 0) {
			ModelObject[] tBomLines = tBomLine.get_bl_child_lines();

			logger.info(
					" Found at bomline at level [" + (iLevel + 1) + "] [" + tBomLines.length + "] bomlines lines.  ");

			for (int l = 0; l < tBomLines.length; l++) {
				iRetCode = BNL_get_non_note_excluded_revs((BOMLine) tBomLines[l], (iLevel + 1), ptRevs);

				if (iRetCode == -1) {
					if (bnlDepRequest.lIgnoreUnconfigBom) {
						logger.info(" Skipped unconfigured occurence.\n");
						iRetCode = 0;
					} else {
						return -1;
					}
				}
				logger.info("  Found at bomline [" + l + "] of level [" + (iLevel + 1) + "] [" + ptRevs.size()
						+ " ] child lines. ");
			}
		}

		return iRetCode;
	} /* End of BNL_get_non_note_excluded_revs */

}
