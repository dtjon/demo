/*
$Id: BnlCloneConnector.java 153 2016-05-25 07:34:18Z tjonkonj $

REVISION HISTORY
Version Date        Author           Description
------- ----------- ---------------- -----------------------------------------------------
0.0.0.1 20-jul-2016 D. Tjon Kon Joe  Created
*/
package com.bnl.dep;

import com.teamcenter.soa.client.model.ModelObject;

import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.teamcenter.soa.client.model.strong.Dataset;
import com.teamcenter.soa.client.model.strong.Item;
import com.teamcenter.soa.client.model.strong.ItemRevision;

import com.bnl.tc.soa.BnlSoa;

public class BnlDepRequest {
	String VERSION = "0.0.0.1";
	private Logger logger = Logger.getLogger(BnlDepRequest.class.getCanonicalName() + " " + VERSION);

	// [TOOL]-arguments
	public Hashtable<String, String> itemTable = new Hashtable<String, String>();
	public String revId = "";
	public String extArg1 = "";
	public String extArg2 = "";
	public String extArg3 = "";

	public String theUsername = "";
	public String thePassword = "";
	public String theGroup = "";
	public String theRole = "";
	boolean bypassFlag = false;
	public String theHostname = "";

	public String policyFile = "";

	public BnlDepRequest() {
		super();
	}

	// [Request]-arguments
	public String pszIni = "";
	public String pszTranslator = "";
	public String pszProvider = "";
	public Vector<String> pszNamedRefs = new Vector<String>();
	public Vector<String> pszDStypes = new Vector<String>();
	public Vector<String> pszObjectsTypes = new Vector<String>();
	public String pszNoteType = "";
	public String pszNote = "";
	public int iPriority = 3; // The schedule priority. If not given, the default will be 3.
	// public int namedRefCount = 0;
	// public int dstypesCount = 0;
	// public int objectTypes = 0;
	public int iProcessOneLine = 0;
	public Vector<String> extraArgKeys = new Vector<String>();
	public Vector<String> extraArgValues = new Vector<String>();
	public String pszListName = "";
	public String pszBomview = "";
	public String pszRevisionRule = "";
	public int iBomLevel = 0;
	public int iExcludeNote = 0;
	public ModelObject tCurrentTarget = null;
	public Item tItem = null;
	public ItemRevision tItemRev = null;
	public Dataset tDataset = null;
	public ModelObject tNamedRef = null;
	public ModelObject tRootTask = null;
	public boolean lRecursive = true; // true=default process all targets
	public boolean lUseBomView = false;
	public boolean lIgnoreUnconfigBom = false;
	public boolean lIsOccOfBomView = false;
	public boolean lTranslationRequestCreated = false;
	// public int numTargets;
	public ModelObject[] ptTargets;
	public String pszTaskPuid = "";

	public String currentTargetType = "";

	public void validateArgs() {
		boolean lError = false;
		// validate [Tool] section
		if (theUsername.length() == 0) {
			System.out.println("-u= argument is required");
			lError = true;
		}
		if (thePassword.length() == 0) {
			System.out.println("-p= argument is required");
			lError = true;
		}
		if (theHostname.length() == 0) {
			System.out.println("-host= argument is required");
			lError = true;
		}

		/* policy file is optional
		if (policyFile.length() == 0) {
			System.out.println("-policy= argument is required");
			lError = true;
		} */

		if (pszTaskPuid.length() == 0) {
			if (itemTable.get("item_id").length() == 0) {
				System.out.println("-item_id= argument is required");
				lError = true;
			}
		}

		// if bomviewtype was given, revrule and leveldepth are required arguments
		if (lUseBomView) {
			if (pszRevisionRule.length() == 0) {
				System.out.println("-revrule= argument is required");
				lError = true;
			}
		}

		// init the extra args
		if (extArg1.length() > 0) {
			String[] tempKey = extArg1.split("=");
			if (tempKey.length != 2) {
				System.out.println("Given value for -ta1 has wrong format, skipping");
				logger.error("Given value for -ta1 has wrong format, skipping");
			} else {
				extraArgKeys.add(tempKey[0]);
				extraArgValues.add(tempKey[1]);
			}
		}
		if (extArg2.length() > 0) {
			String[] tempKey = extArg2.split("=");
			if (tempKey.length != 2) {
				System.out.println("Given value for -ta2 has wrong format, skipping");
				logger.error("Given value for -ta2 has wrong format, skipping");
			} else {
				extraArgKeys.add(tempKey[0]);
				extraArgValues.add(tempKey[1]);
			}
		}
		if (extArg3.length() > 0) {
			String[] tempKey = extArg3.split("=");
			if (tempKey.length != 2) {
				System.out.println("Given value for -ta3 has wrong format, skipping");
				logger.error("Given value for -ta3 has wrong format, skipping");
			} else {
				extraArgKeys.add(tempKey[0]);
				extraArgValues.add(tempKey[1]);
			}
		}

		if (lError == true) {
			System.exit(-1);
		}
	}

	public void BNL_ask_attachments() {
		if (pszTaskPuid.length() == 0) {
			ItemRevision itemRev;
			try {
				itemRev = BnlSoa.getBnlItem().findItemRevision(itemTable, revId);
				ptTargets = new ModelObject[] { itemRev };
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			// TODO
		}

		// Log arguments
		logger.info("[Tool]");
		logger.info("-u=[" + theUsername + "]");
		logger.info("-p=[" + thePassword + "]");
		logger.info("-g=[" + theGroup + "]");
		logger.info("-r=[" + theRole + "]");
		logger.info("-bypass=[" + bypassFlag + "]");
		logger.info("-host=[" + theHostname + "]");
		logger.info("-config=[" + pszIni + "]");
		logger.info("-policy=[" + policyFile + "]");
		logger.info("-item_id=[" + itemTable.get("item_id") + "]");
		logger.info("-rev_id=[" + revId + "]");
		logger.info("-ta1=[" + extArg1 + "]");
		logger.info("-ta2=[" + extArg2 + "]");
		logger.info("-ta3=[" + extArg3 + "]");

		logger.info("[Request]");
		logger.info("-dataset_types=[" + pszDStypes + "]");
		logger.info("-named_references=[" + pszNamedRefs + "]");
		logger.info("-object_types=[" + pszObjectsTypes + "]");
		logger.info("-translator=[" + pszTranslator + "]");
		logger.info("-provider=[" + pszProvider + "]");
		logger.info("-priority=[" + iPriority + "]");
		logger.info("-recursive=[" + lRecursive + "]");
		logger.info("-listname=[" + pszListName + "]");
		logger.info("-oneline_process=[" + iProcessOneLine + "]");

		logger.info("-bomviewtype=[" + lUseBomView + "]");
		logger.info("-revrule=[" + pszRevisionRule + "]");
		logger.info("-leveldepth=[" + iBomLevel + "]");
		logger.info("-ignore_unconfigured=[" + lIgnoreUnconfigBom + "]");
		logger.info("-excludednote=[" + pszNoteType + ":" + pszNote + "]");
		logger.info("-iExcludeNote=[" + iExcludeNote + "]");
	}
}
