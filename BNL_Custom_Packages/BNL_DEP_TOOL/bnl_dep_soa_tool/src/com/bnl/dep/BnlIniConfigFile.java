/*
$Id: BnlCloneConnector.java 153 2016-05-25 07:34:18Z tjonkonj $

REVISION HISTORY
Version Date        Author           Description
------- ----------- ---------------- -----------------------------------------------------
0.0.0.1 20-jul-2016 D. Tjon Kon Joe  Created
*/
package com.bnl.dep;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.SubnodeConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.log4j.Logger;
import com.bnl.dep.BnlCreateTranslationRequest;

public class BnlIniConfigFile {
	String VERSION = "0.0.0.1";
	private Logger logger = Logger.getLogger(BnlIniConfigFile.class.getCanonicalName() + " " + VERSION);

	/**
	 * @param args
	 */

	String SOA_SECTION = "Tool";
	String REQUEST_SECTION = "Request";
	String EXTRA_ARGS_SECTION = "Extra Arguments";
	String SEMICOLON_TOKENIZER = ";";
	String COLON_TOKENIZER = ":";

	BufferedReader inifileReader;

	public BnlIniConfigFile(BufferedReader inifileReader) {
		super();
		this.inifileReader = inifileReader;
		INIConfiguration iniconfig = new INIConfiguration();
		try {
			iniconfig.read(inifileReader);
			// read the SOA_SECTION
			SubnodeConfiguration subConfig = iniconfig.getSection(SOA_SECTION);
			if (subConfig == null) {
				System.out.println("Given ini file has to contain section " + SOA_SECTION + " !");
				System.exit(-1);
			}

			String keyValue;
			if (BnlCreateTranslationRequest.bnlDepRequest.theUsername.isEmpty()) {
				keyValue = (String) subConfig.getProperty("-u");
				if (keyValue != null) {
					BnlCreateTranslationRequest.bnlDepRequest.theUsername = keyValue;
				}
			}

			if (BnlCreateTranslationRequest.bnlDepRequest.thePassword.isEmpty()) {
				keyValue = (String) subConfig.getProperty("-p");
				if (keyValue != null) {
					BnlCreateTranslationRequest.bnlDepRequest.thePassword = keyValue;
				}
			}

			if (BnlCreateTranslationRequest.bnlDepRequest.theGroup.length() == 0) {
				keyValue = (String) subConfig.getProperty("-g");
				if (keyValue != null) {
					BnlCreateTranslationRequest.bnlDepRequest.theGroup = keyValue;
				}
			}

			if (BnlCreateTranslationRequest.bnlDepRequest.theRole.length() == 0) {
				keyValue = (String) subConfig.getProperty("-r");
				if (keyValue != null) {
					BnlCreateTranslationRequest.bnlDepRequest.theRole = keyValue;
				}
			}

			if (BnlCreateTranslationRequest.bnlDepRequest.theHostname.isEmpty()) {
				keyValue = (String) subConfig.getProperty("-host");
				if (keyValue != null) {
					BnlCreateTranslationRequest.bnlDepRequest.theHostname = keyValue;
				}
			}

			if (BnlCreateTranslationRequest.bnlDepRequest.extArg1.isEmpty()) {
				keyValue = (String) subConfig.getProperty("-ta1");
				if (keyValue != null) {
					BnlCreateTranslationRequest.bnlDepRequest.extArg1 = keyValue;
				}
			}

			if (BnlCreateTranslationRequest.bnlDepRequest.extArg2.isEmpty()) {
				keyValue = (String) subConfig.getProperty("-ta2");
				if (keyValue != null) {
					BnlCreateTranslationRequest.bnlDepRequest.extArg2 = keyValue;
				}
			}

			if (BnlCreateTranslationRequest.bnlDepRequest.extArg3.isEmpty()) {
				keyValue = (String) subConfig.getProperty("-ta3");
				if (keyValue != null) {
					BnlCreateTranslationRequest.bnlDepRequest.extArg3 = keyValue;
				}
			}
			if (BnlCreateTranslationRequest.bnlDepRequest.policyFile.isEmpty()) {
				keyValue = (String) subConfig.getProperty("-policy");
				if (keyValue != null) {
					BnlCreateTranslationRequest.bnlDepRequest.policyFile = keyValue;
				}
			}

			// read the REQUEST_SECTION
			subConfig = iniconfig.getSection(REQUEST_SECTION);
			if (subConfig == null) {
				logger.info("Given ini file has to contain section " + REQUEST_SECTION + " !");
				System.exit(-1);
			}

			keyValue = (String) subConfig.getProperty("-dataset_types");
			if (keyValue != null) {
				StringTokenizer st = new StringTokenizer(keyValue, SEMICOLON_TOKENIZER);
				while (st.hasMoreTokens()) {
					String nextToken = st.nextToken();
					BnlCreateTranslationRequest.bnlDepRequest.pszDStypes.add(nextToken);
				}
			}

			keyValue = (String) subConfig.getProperty("-named_references");
			if (keyValue != null) {
				StringTokenizer st = new StringTokenizer(keyValue, SEMICOLON_TOKENIZER);
				while (st.hasMoreTokens()) {
					String nextToken = st.nextToken();
					BnlCreateTranslationRequest.bnlDepRequest.pszNamedRefs.add(nextToken);
				}
			}

			keyValue = (String) subConfig.getProperty("-object_types");
			if (keyValue != null) {
				StringTokenizer st = new StringTokenizer(keyValue, SEMICOLON_TOKENIZER);
				while (st.hasMoreTokens()) {
					String nextToken = st.nextToken();
					BnlCreateTranslationRequest.bnlDepRequest.pszObjectsTypes.add(nextToken);
				}
			}

			keyValue = (String) subConfig.getProperty("-translator");
			if (keyValue != null) {
				BnlCreateTranslationRequest.bnlDepRequest.pszTranslator = keyValue;
			}

			keyValue = (String) subConfig.getProperty("-provider");
			if (keyValue != null) {
				BnlCreateTranslationRequest.bnlDepRequest.pszProvider = keyValue;
			}

			// keyValue = (String)subConfig.getProperty("-inifile");

			keyValue = (String) subConfig.getProperty("-priority");
			if (keyValue != null) {
				BnlCreateTranslationRequest.bnlDepRequest.iPriority = Integer.parseInt(keyValue);
			}

			keyValue = (String) subConfig.getProperty("-recursive");
			if (keyValue != null) {
				if (keyValue.equalsIgnoreCase("false"))
					BnlCreateTranslationRequest.bnlDepRequest.lRecursive = false;
				else
					BnlCreateTranslationRequest.bnlDepRequest.lRecursive = true;
			}

			keyValue = (String) subConfig.getProperty("-listname");
			if (keyValue != null) {
				BnlCreateTranslationRequest.bnlDepRequest.pszListName = keyValue;
			}

			// keyValue = (String)subConfig.getProperty("-no_info");

			keyValue = (String) subConfig.getProperty("-oneline_process");
			if (keyValue != null) {
				if (keyValue.equalsIgnoreCase("false"))
					BnlCreateTranslationRequest.bnlDepRequest.lRecursive = false;
				else
					BnlCreateTranslationRequest.bnlDepRequest.lRecursive = true;
			}

			keyValue = (String) subConfig.getProperty("-bomviewtype");
			if (keyValue != null) {
				BnlCreateTranslationRequest.bnlDepRequest.pszBomview = keyValue;
				BnlCreateTranslationRequest.bnlDepRequest.lUseBomView = true;
			}
			if (BnlCreateTranslationRequest.bnlDepRequest.lUseBomView == true) {
				keyValue = (String) subConfig.getProperty("-revrule");
				if (keyValue != null) {
					BnlCreateTranslationRequest.bnlDepRequest.pszRevisionRule = keyValue;
				}

				keyValue = (String) subConfig.getProperty("-leveldepth");
				if (keyValue != null) {
					BnlCreateTranslationRequest.bnlDepRequest.iBomLevel = Integer.parseInt(keyValue);
				}

				keyValue = (String) subConfig.getProperty("-excludednote");
				if (keyValue != null) {
					StringTokenizer st = new StringTokenizer(keyValue, COLON_TOKENIZER);
					if (st.hasMoreTokens()) {
						String nextToken = st.nextToken();
						BnlCreateTranslationRequest.bnlDepRequest.pszNoteType = nextToken;
						if (st.hasMoreTokens()) {
							nextToken = st.nextToken();
							BnlCreateTranslationRequest.bnlDepRequest.pszNote = nextToken;
						}
					}
				}
			}

			// read the EXTRA_ARGS_SECTION
			subConfig = iniconfig.getSection(EXTRA_ARGS_SECTION);
			if (subConfig != null) {
				Iterator<?> extArgsIter = subConfig.getKeys();
				while (extArgsIter.hasNext()) {
					String keyArg = (String) extArgsIter.next();
					BnlCreateTranslationRequest.bnlDepRequest.extraArgKeys.add(keyArg);
					keyValue = (String) subConfig.getProperty(keyArg);
					BnlCreateTranslationRequest.bnlDepRequest.extraArgValues.add(keyValue);
				}
			}

		} catch (ConfigurationException e) {
			System.out.println("Given ini file has config error!");
			logger.error("Given ini file has config error!");
			e.printStackTrace();
			System.exit(-1);
		} catch (IOException e) {
			System.out.println("Cannot open ini file!");
			logger.error("Cannot open ini file!");
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
